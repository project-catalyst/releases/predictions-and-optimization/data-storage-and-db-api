# Data Storage and DB API

This component implements the CATALYST data model and stores the following type of data: DC main sub-system characteristics, thermal and energy monitored data, prediction outcomes and optimization action plans.