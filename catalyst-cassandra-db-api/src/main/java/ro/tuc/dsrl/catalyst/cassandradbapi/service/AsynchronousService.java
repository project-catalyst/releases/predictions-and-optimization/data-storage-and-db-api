package ro.tuc.dsrl.catalyst.cassandradbapi.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
@Async
public class AsynchronousService {

    @Value("${fast.foward}")
    private boolean fastFoward;

    @Value("${async.aggregation.periodicity}")
    private int asyncPeriodicity;

    private static final Log LOGGER = LogFactory.getLog(AsynchronousService.class);

    private final ApplicationContext applicationContext;
    private final ScheduledExecutorService threadPoolExecutor;

    @Autowired
    public AsynchronousService(ApplicationContext applicationContext,
                               ScheduledThreadPoolExecutor threadPoolExecutor){
        this.applicationContext = applicationContext;
        this.threadPoolExecutor = threadPoolExecutor;
    }

    public void executeAsynchronously() {

        InsertAggregatedMonitoredValuesInMySQL insertAggregatedMonitoredValuesInMySQL = applicationContext.
                getBean(InsertAggregatedMonitoredValuesInMySQL.class);
        ScheduledFuture<?> f;

        if(fastFoward) {
            f = threadPoolExecutor.scheduleAtFixedRate(insertAggregatedMonitoredValuesInMySQL,
                    0,
                    asyncPeriodicity, //each 5 milliseconds
                    TimeUnit.SECONDS);
            
        }
        else {
            f = threadPoolExecutor.scheduleAtFixedRate(insertAggregatedMonitoredValuesInMySQL,
                    0,
                    asyncPeriodicity, //each 5 minutes
                    TimeUnit.MINUTES);
        }

        try {
            f.get();
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }


}
