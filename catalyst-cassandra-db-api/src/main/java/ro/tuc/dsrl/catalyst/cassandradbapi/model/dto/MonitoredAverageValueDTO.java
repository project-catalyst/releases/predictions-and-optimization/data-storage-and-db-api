package ro.tuc.dsrl.catalyst.cassandradbapi.model.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class MonitoredAverageValueDTO {

    private UUID deviceId;
    private UUID deviceMeasurementId;
    private Long timestamp;
    private Double monitoredValue;

    public MonitoredAverageValueDTO(UUID deviceId, UUID deviceMeasurementId, Long timestamp, Double monitoredValue) {
        this.deviceId = deviceId;
        this.deviceMeasurementId = deviceMeasurementId;
        this.timestamp = timestamp;
        this.monitoredValue = monitoredValue;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public UUID getDeviceMeasurementId() {
        return deviceMeasurementId;
    }

    public void setDeviceMeasurementId(UUID deviceMeasurementId) {
        this.deviceMeasurementId = deviceMeasurementId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getMonitoredValue() {
        return monitoredValue;
    }

    public void setMonitoredValue(Double monitoredValue) {
        this.monitoredValue = monitoredValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonitoredAverageValueDTO that = (MonitoredAverageValueDTO) o;
        return Objects.equals(deviceId, that.deviceId) &&
            Objects.equals(deviceMeasurementId, that.deviceMeasurementId) &&
            Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, deviceMeasurementId, timestamp);
    }
}
