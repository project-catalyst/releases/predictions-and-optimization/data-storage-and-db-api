package ro.tuc.dsrl.catalyst.cassandradbapi.utils;

public class Constants {

    private Constants(){

    }

    public static final String INCORRECT_PARAMS = "Incorrect Parameters";

    public static final String AGGREGATION_GRANULARITY_MYSQL = "5min";
}
