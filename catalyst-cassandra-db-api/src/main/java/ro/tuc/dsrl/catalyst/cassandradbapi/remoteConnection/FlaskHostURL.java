package ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection;

public enum FlaskHostURL implements ApiURL  {

    PREPROCESS_DATA("preprocessing/data_preprocess/{granularity}");

    private String value;

    FlaskHostURL(String url) {
        this.value = url;
    }

    public String value() {
        return value;
    }

}
