package ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.ParameterizedTypeReference;

import java.util.Map;

@Component
public class RestAPIClient {

    private final RestAPIConfig restAPIConfig;

    @Autowired
    public RestAPIClient(RestAPIConfig restAPIConfig) {
        this.restAPIConfig = restAPIConfig;
    }

    public <T> T postObject(
            FlaskHostURL url, Object o, ParameterizedTypeReference<T> reference, Map<String, ?> urlVariables) {
        return RestTemplateWrapper.postObject(
                restAPIConfig.getFlaskAPIHost() + url.value(), o, reference, urlVariables);
    }

    public <T> T postObject(MySqlHostURL url, Object o, Class<T> clazz) {
        return RestTemplateWrapper.postObject(restAPIConfig.getMysqlDBAPIHost() + url.value(), o, clazz);
    }

    private static class RestTemplateWrapper {

        public static <T> T postObject(
                String url, Object o, ParameterizedTypeReference<T> reference, Map<String, ?> urlVariables) {

            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            return rt.exchange(url, HttpMethod.POST, new HttpEntity<>(o), reference, urlVariables).getBody();
        }

        public static <T> T postObject(String url, Object o, Class<T> clazz) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.postForObject(url, o, clazz);
        }

        public static <T> T postObject(String url, Object o, Class<T> clazz, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.postForObject(url, o, clazz, urlVariables);
        }

        public static <T> T postObject(String url, Object o, ParameterizedTypeReference<T> reference) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.exchange(url, HttpMethod.POST, new HttpEntity<>(o), reference).getBody();
        }

        public static <T> T getObject(String url, Class<T> responseType, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.getForObject(url, responseType, urlVariables);
        }

        public static <T> ResponseEntity<T> getObject(String url, HttpHeaders headers,
                                                      Class<T> responseType, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            HttpEntity httpEntity = new HttpEntity(headers);
            return rt.exchange(url, HttpMethod.GET, httpEntity, responseType, urlVariables);
        }

        public static <T> T getObject(String url, Class<T> responseType) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.getForObject(url, responseType);
        }

        public static <T> ResponseEntity<T> getObject(String url, HttpHeaders headers,
                                                      Class<T> responseType) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            HttpEntity httpEntity = new HttpEntity(headers);
            return rt.exchange(url, HttpMethod.GET, httpEntity, responseType);
        }

    }
}
