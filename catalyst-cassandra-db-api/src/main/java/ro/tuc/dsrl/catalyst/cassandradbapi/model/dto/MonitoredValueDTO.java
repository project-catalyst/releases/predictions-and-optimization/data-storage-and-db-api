package ro.tuc.dsrl.catalyst.cassandradbapi.model.dto;

import java.util.UUID;

public class MonitoredValueDTO {

    private UUID id;
    private Long timestamp;
    private UUID deviceId;
    private UUID deviceMeasurementId;
    private Double monitoredValue;
    private boolean isProcessed;


    public MonitoredValueDTO() {

    }

    public MonitoredValueDTO(Long timestamp, UUID deviceId, UUID deviceMeasurementId, Double monitoredValue, boolean isProcessed) {
        this.timestamp = timestamp;
        this.deviceId = deviceId;
        this.deviceMeasurementId = deviceMeasurementId;
        this.monitoredValue = monitoredValue;
        this.isProcessed = isProcessed;
    }

    public MonitoredValueDTO(UUID id, Long timestamp, UUID deviceId, UUID deviceMeasurementId, Double monitoredValue, boolean isProcessed) {
        this.id = id;
        this.timestamp = timestamp;
        this.deviceId = deviceId;
        this.deviceMeasurementId = deviceMeasurementId;
        this.monitoredValue = monitoredValue;
        this.isProcessed = isProcessed;
    }

    public UUID getId() {
        return id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public UUID getDeviceMeasurementId() {
        return deviceMeasurementId;
    }

    public Double getMonitoredValue() {
        return monitoredValue;
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public void setDeviceMeasurementId(UUID deviceMeasurementId) {
        this.deviceMeasurementId = deviceMeasurementId;
    }

    public void setMonitoredValue(Double monitoredValue) {
        this.monitoredValue = monitoredValue;
    }

    public void setProcessed(boolean processed) {
        isProcessed = processed;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MonitoredValueDTO{" +
                "timestamp=" + timestamp +
                ", deviceId=" + deviceId +
                ", deviceMeasurementId=" + deviceMeasurementId +
                ", monitoredValue=" + monitoredValue +
                ", isProcessed=" + isProcessed +
                '}';
    }

    public boolean checkIfEmpty() {
        return (this.timestamp == null && this.deviceId == null &&
                this.deviceMeasurementId == null && this.deviceId == null);
    }

    public boolean checkIfNotValid(){
        return (this.timestamp == null || this.deviceId == null ||
                this.deviceMeasurementId == null || this.isProcessed == true);
    }
}
