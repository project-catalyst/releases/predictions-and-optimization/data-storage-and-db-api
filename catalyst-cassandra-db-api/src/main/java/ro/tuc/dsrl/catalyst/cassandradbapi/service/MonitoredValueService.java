package ro.tuc.dsrl.catalyst.cassandradbapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.entity.MonitoredValuesEntity;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.builder.MonitoredValueDTOBuilder;
import ro.tuc.dsrl.catalyst.cassandradbapi.repository.MonitoredValueRepository;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.validators.MonitoredValueValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class MonitoredValueService {

    private final MonitoredValueRepository monitoredValueRepository;
    private static final Log LOGGER = LogFactory.getLog(MonitoredValueService.class);

    @Autowired
    public MonitoredValueService(MonitoredValueRepository monitoredValueRepository) {
        this.monitoredValueRepository = monitoredValueRepository;
    }

    public List<MonitoredValueDTO> findAll(){
        return toDTOList(monitoredValueRepository.findAll());
    }

    public List<MonitoredValueDTO> findAllByTimestampInterval(LocalDateTime start,
                                                              LocalDateTime end){

        MonitoredValueValidator.validateTimestampInterval(start, end);

        List<MonitoredValuesEntity> monitoredValuesEntityList = monitoredValueRepository.findAllByTimestampInterval(start, end);
        return toDTOList(monitoredValuesEntityList);
    }

    public UUID insert(MonitoredValueDTO monitoredValueDTO) {

        MonitoredValueValidator.validateInsert(monitoredValueDTO);
        MonitoredValuesEntity m = MonitoredValueDTOBuilder.generateEntityFromDTO(monitoredValueDTO);
        return monitoredValueRepository.save(m).getId();
    }

    public UUID updateFlag(MonitoredValueDTO monitoredValueDTO, boolean flag){

        MonitoredValueValidator.validateInsert(monitoredValueDTO);

        MonitoredValuesEntity m = monitoredValueRepository.findByUUID(monitoredValueDTO.getId());
        if (m == null) {
            throw new ResourceNotFoundException(MonitoredValuesEntity.class.getSimpleName());
        }
        m.setProcessed(flag);
        return monitoredValueRepository.save(m).getId();
    }


    private List<MonitoredValueDTO> toDTOList(List<MonitoredValuesEntity> monitoredValues) {

        List<MonitoredValueDTO> monitoredValueDTOS = new ArrayList<>();
        for(MonitoredValuesEntity m: monitoredValues) {
            MonitoredValueDTO mDTO = MonitoredValueDTOBuilder.generateDTOFromEntity(m);
            mDTO.setId(m.getId());
            monitoredValueDTOS.add(mDTO);

        }
        return monitoredValueDTOS;
    }

    public List<MonitoredValueDTO> findAllByDeviceId(UUID deviceId) {

        List<MonitoredValuesEntity> monitoredValuesEntityList = monitoredValueRepository.findAllByDeviceId(deviceId);
        return toDTOList(monitoredValuesEntityList);
    }

    public List<MonitoredValueDTO> findAllByMeasurementId(UUID measurementId) {

        List<MonitoredValuesEntity> monitoredValuesEntityList = monitoredValueRepository.findAllByMeasurementId(measurementId);
        return toDTOList(monitoredValuesEntityList);
    }

    public List<MonitoredValueDTO> findAllByTimestampIntervalForDeviceAndMeasurement(LocalDateTime start,
                                                                                     LocalDateTime end,
                                                                                     UUID deviceId,
                                                                                     UUID measurementId) {

        MonitoredValueValidator.validateTimestampInterval(start, end);

        List<MonitoredValuesEntity> monitoredValuesEntityList = monitoredValueRepository.findAllByTimestampIntervalForDeviceAndMeasurement(start,
                end,
                deviceId,
                measurementId);
        return toDTOList(monitoredValuesEntityList);

    }

    public List<MonitoredValueDTO> findAllUnproccesedValues() {

        List<MonitoredValuesEntity> monitoredValuesEntityList = monitoredValueRepository.findAllUnprocessedData();
        return toDTOList(monitoredValuesEntityList);
    }
}
