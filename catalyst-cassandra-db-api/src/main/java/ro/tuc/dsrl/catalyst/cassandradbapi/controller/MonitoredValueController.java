package ro.tuc.dsrl.catalyst.cassandradbapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.cassandradbapi.controller.preconditions.MonitoredValuePreconditions;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.MonitoredValueService;
import ro.tuc.dsrl.catalyst.cassandradbapi.utils.DateUtils;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/monitored-values")
public class MonitoredValueController {

    private final MonitoredValueService monitoredValueService;

    @Autowired
    public MonitoredValueController(MonitoredValueService monitoredValueService) {
        this.monitoredValueService = monitoredValueService;
    }

    @GetMapping(value = "/")
    public String sayHello(){

        return "Hello";
    }

    @GetMapping(value = "/all")
    public List<MonitoredValueDTO> getAll(){

        return monitoredValueService.findAll();
    }

    @GetMapping(value = "/byTimestampInterval/{start}/{end}")
    public List<MonitoredValueDTO> getAllByTimestampInterval(@PathVariable("start") Long start,
                                                    @PathVariable("end") Long end) {

        List<String> errors = MonitoredValuePreconditions.validateDateInterval(start, end);
        MonitoredValuePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLDT = DateUtils.millisToUTCLocalDateTime(start);
        LocalDateTime endLDT = DateUtils.millisToUTCLocalDateTime(end);

        return monitoredValueService.findAllByTimestampInterval(startLDT, endLDT);
    }

    @GetMapping(value = "/byDeviceId/{deviceId}")
    public List<MonitoredValueDTO> getAllForDeviceId(@PathVariable("deviceId") UUID deviceId) {

        List<String> errors = MonitoredValuePreconditions.validateUUID(deviceId.toString());
        MonitoredValuePreconditions.throwIncorrectParameterException(errors);

        return monitoredValueService.findAllByDeviceId(deviceId);
    }

    @GetMapping(value = "/byMeasurementId/{measurementId}")
    public List<MonitoredValueDTO> getAllForMeasurementId(@PathVariable("measurementId") UUID measurementId) {

        List<String> errors = MonitoredValuePreconditions.validateUUID(measurementId.toString());
        MonitoredValuePreconditions.throwIncorrectParameterException(errors);

        return monitoredValueService.findAllByMeasurementId(measurementId);
    }

    @GetMapping(value = "/byTimestampIntervalForDeviceAndMeasurement/{start}/{end}/{deviceId}/{measurementId}")
    public List<MonitoredValueDTO> getAllByTimestampForDeviceAndForMeasurement(
            @PathVariable("start") Long start,
            @PathVariable("end") Long end,
            @PathVariable("deviceId") UUID deviceId,
            @PathVariable("measurementId") UUID measurementId) {

        List<String> errors = MonitoredValuePreconditions.validateParams(start, end, deviceId.toString(), measurementId.toString());
        MonitoredValuePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLDT = DateUtils.millisToUTCLocalDateTime(start);
        LocalDateTime endLDT = DateUtils.millisToUTCLocalDateTime(end);

        return monitoredValueService.findAllByTimestampIntervalForDeviceAndMeasurement(startLDT, endLDT, deviceId, measurementId);
    }

    @PostMapping(value = "/insert")
    public UUID insertMonitoredValue(@RequestBody MonitoredValueDTO monitoredValueDTO) {

        return monitoredValueService.insert(monitoredValueDTO);
    }


    @PutMapping(value = "/update/{flag}")
    public UUID insertMonitoredValue(@RequestBody MonitoredValueDTO monitoredValueDTO,
                                     @PathVariable("flag") boolean flag) {

        return monitoredValueService.updateFlag(monitoredValueDTO, flag);
    }


}
