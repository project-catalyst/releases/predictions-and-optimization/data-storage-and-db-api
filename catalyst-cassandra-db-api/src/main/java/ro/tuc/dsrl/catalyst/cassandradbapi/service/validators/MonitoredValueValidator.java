package ro.tuc.dsrl.catalyst.cassandradbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.MonitoredValueService;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MonitoredValueValidator {

    private static final Log LOGGER = LogFactory.getLog(MonitoredValueValidator.class);

    private MonitoredValueValidator(){

    }

    public static void validateTimestampInterval(LocalDateTime start, LocalDateTime end) {

        List<String> errors = new ArrayList<>();

        if(start == null){
            errors.add("start timestamp is null");
        }else {

            if (LocalDateTime.of(2000, 1, 1, 0, 0).isAfter(start)) {
                errors.add("start timestamp is not valid");
            }

            if(start.isAfter(end)){
                errors.add("start timestamp is after end timestamp");
            }
        }

        if(end == null){
            errors.add("end timestamp is null");
        }else{
            if (LocalDateTime.of(2000, 1, 1, 0, 0).isAfter(end)) {
                errors.add("end timestamp is not valid");
            }
        }

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException(MonitoredValueService.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(MonitoredValueDTO monitoredValueDTO){

        List<String> errors = new ArrayList<>();

        if(monitoredValueDTO == null){
            errors.add("monitored value is null");
            throw new IncorrectParameterException(MonitoredValueService.class.getSimpleName(), errors);
        }

        if(monitoredValueDTO.getDeviceId() == null){
            errors.add("device ID is null");
        }

        if(monitoredValueDTO.getDeviceMeasurementId() == null){
            errors.add("device measurement ID is null");
        }

        if(monitoredValueDTO.getTimestamp() == null){
            errors.add("timestamp is null");
        }

        if(monitoredValueDTO.isProcessed() == true){
            errors.add("isProcessed cannot be true");
        }

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException(MonitoredValueService.class.getSimpleName(), errors);
        }

    }
}
