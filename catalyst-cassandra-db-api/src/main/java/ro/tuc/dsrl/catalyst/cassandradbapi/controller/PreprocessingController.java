package ro.tuc.dsrl.catalyst.cassandradbapi.controller;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.cassandradbapi.controller.preconditions.MonitoredValuePreconditions;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.entity.MonitoredValuesEntity;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.MonitoredValueService;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.PreprocessingService;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.timers.PreprocessingTimerTask;
import ro.tuc.dsrl.catalyst.cassandradbapi.utils.DateUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@CrossOrigin
@RequestMapping(value = "/preprocessing")
public class PreprocessingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreprocessingController.class);
    private static boolean STARTED = false;

    private final PreprocessingService preprocessingService;
    private final PreprocessingTimerTask preprocessingTimerTask;

    @Autowired
    public PreprocessingController(PreprocessingService preprocessingService,
                                   PreprocessingTimerTask preprocessingTimerTask) {
        this.preprocessingService = preprocessingService;
        this.preprocessingTimerTask = preprocessingTimerTask;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "Preprocessing";
    }

    @RequestMapping(value = "/start-pilot", method = RequestMethod.GET)
    public boolean startPilotSimulation() {
        int OPT_RUNNING_PERIOD = 5*60*1000;//5 secunde //5 minutes in miliseconds
        if (STARTED) {
            return false;
        }
        Runnable run = () -> {
            DateTime systemTime = new DateTime();
            DateTime newHour = null;
            int mins = -1;
            if(systemTime.getMinuteOfHour() >= 54){
                newHour = systemTime.plusHours(1);
                mins = 5;
            }else{
                newHour = systemTime.plusHours(0);
                mins = systemTime.getMinuteOfHour();
                mins = (mins / 5 + 1) * 5;
            }
            DateTime startTime = new DateTime(newHour.getYear(),
                newHour.getMonthOfYear(),
                newHour.getDayOfMonth(),
                newHour.getHourOfDay(),
                mins);
            System.out.println(startTime);
            System.out.println(mins);
            long initialDelay = startTime.getMillis() -
                systemTime.getMillis();
            System.out.println(DateUtils.millisToUTCLocalDateTime(initialDelay));

            ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(1);
            ScheduledFuture<?> f = stpe.scheduleAtFixedRate(preprocessingTimerTask, initialDelay,
                OPT_RUNNING_PERIOD, TimeUnit.MILLISECONDS);
            try {
                f.get();
            } catch (Exception ex) {
                LOGGER.error("", ex);
            }
        };
        new Thread(run).start();
        STARTED = true;
        return true;
        /*
        int OPT_RUNNING_PERIOD = 5*60*1000; //5 minutes in miliseconds
        DateTimeZone.setDefault(DateTimeZone.UTC);
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        if (STARTED) {
            return false;
        }
        Runnable run = () -> {
            LOGGER.info("Optimizer Started....");
            DateTime systemTime = new DateTime();
            DateTime startTime = new DateTime(systemTime.getYear(),
                systemTime.getMonthOfYear(),
                systemTime.getDayOfMonth(), 19, 50);
            long initialDelay= startTime.getMillis() -
                systemTime.getMillis();

            OptimizerTimerTask instance = OptimizerTimerTask.getInstance(startTime);
            ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(1);
            ScheduledFuture<?> f = stpe.scheduleAtFixedRate(instance, initialDelay, OPT_RUNNING_PERIOD,
                TimeUnit.MILLISECONDS);
            try {
                f.get();
            } catch (Exception ex) {
                LOGGER.error("", ex);
            }
        };
        new Thread(run).start();
        STARTED = true;
        return true;
        */
    }

}
