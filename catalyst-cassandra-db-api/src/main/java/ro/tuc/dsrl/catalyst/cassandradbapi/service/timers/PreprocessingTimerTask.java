package ro.tuc.dsrl.catalyst.cassandradbapi.service.timers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredAverageValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.entity.MonitoredValuesEntity;
import ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection.MySqlHostURL;
import ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection.RestAPIClient;
import ro.tuc.dsrl.catalyst.cassandradbapi.repository.MonitoredValueRepository;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.MonitoredValueService;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.PreprocessingService;
import ro.tuc.dsrl.catalyst.cassandradbapi.utils.DateUtils;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection.RestAPIClient;


import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimerTask;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PreprocessingTimerTask extends TimerTask {

    private final MonitoredValueRepository monitoredValueRepository;
    private final RestAPIClient accessPoint;

    @Autowired
    public PreprocessingTimerTask(MonitoredValueRepository monitoredValueRepository,
                                  RestAPIClient accessPoint) {
        this.monitoredValueRepository = monitoredValueRepository;
        this.accessPoint = accessPoint;
    }

    @Override
    public void run() {
        List<MonitoredValuesEntity> monitoredValues = monitoredValueRepository.findAllUnprocessedData();
        List<MonitoredValuesEntity> savedMonitoredValues = new ArrayList<>(monitoredValues);
        List<MonitoredAverageValueDTO> filteredValues = monitoredValues.parallelStream()
            .map(x -> new MonitoredAverageValueDTO(
                      x.getDeviceId(),
                      x.getDeviceMeasurementId(),
                      (x.getTimestampInt() / (5 * 60 * 1000) * (5 * 60 * 1000)),
                      x.getMonitoredValue()))
            .collect(
                Collectors.groupingBy(y -> y, Collectors.averagingDouble(MonitoredAverageValueDTO::getMonitoredValue)))
            .entrySet()
            .parallelStream()
            .map(t-> new MonitoredAverageValueDTO(
                t.getKey().getDeviceId(),
                t.getKey().getDeviceMeasurementId(),
                t.getKey().getTimestamp(),
                t.getValue()))
            .sorted(Comparator.comparing(MonitoredAverageValueDTO::getTimestamp))
            .collect(Collectors.toList());
        if(filteredValues.size() > 0) {
            Long lastTimestamp = filteredValues.get(filteredValues.size() - 1).getTimestamp();
            for (MonitoredAverageValueDTO filteredValue : filteredValues) {
                if (filteredValue.getTimestamp() < lastTimestamp) {
                    MonitoredValueDTO monitoredValue = new MonitoredValueDTO(
                        DateUtils.millisToUTCLocalDateTime(filteredValue.getTimestamp()),
                        filteredValue.getMonitoredValue(),
                        filteredValue.getDeviceId(),
                        filteredValue.getDeviceMeasurementId());

                    UUID insertedValueID = accessPoint.postObject(MySqlHostURL.INSERT_MONITORED_VALUES,
                        monitoredValue,
                        UUID.class);
                }
            }
            for (MonitoredValuesEntity monitoredValue : savedMonitoredValues) {
                if(monitoredValue.getTimestampInt() < lastTimestamp){
                    monitoredValueRepository.updateProcessedData(monitoredValue.getId());
                }
            }
        }
        /*
        for(MonitoredAverageValueDTO filteredValue : filteredValues){
            MonitoredValueDTO monitoredValue = new MonitoredValueDTO(
                DateUtils.millisToUTCLocalDateTime(filteredValue.getTimestamp()),
                filteredValue.getMonitoredValue(),
                filteredValue.getDeviceId(),
                filteredValue.getDeviceMeasurementId());

            UUID insertedValueID = accessPoint.postObject(MySqlHostURL.INSERT_MONITORED_VALUES,
                monitoredValue,
                UUID.class);
        }

        for(UUID id : uuidList){
            monitoredValueRepository.updateProcessedData(id);
        }
        */
    }
}
