package ro.tuc.dsrl.catalyst.cassandradbapi.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class DateUtils {
    
    private static final Log LOGGER = LogFactory.getLog(DateUtils.class);

    private DateUtils(){

    }

    public static long dateTimeToMillis(LocalDateTime localDateTime) {
        return localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    public static LocalDateTime millisToUTCLocalDateTime(Long millis){
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault());
    }


}
