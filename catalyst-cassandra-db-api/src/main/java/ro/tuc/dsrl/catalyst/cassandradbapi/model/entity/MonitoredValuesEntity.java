package ro.tuc.dsrl.catalyst.cassandradbapi.model.entity;

import com.datastax.driver.core.utils.*;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;
import java.util.UUID;


@Table("monitored_values")
public class MonitoredValuesEntity implements Serializable, Comparable {

    private static final long serialVersionUID = -7788619177798333712L;

    @PrimaryKey
    private UUID id;
    @Column("device_id")
    private UUID deviceId;
    @Column("device_measurement_id")
    private UUID deviceMeasurementId;
    @Column("monitored_value")
    private Double monitoredValue;
    @Column("timestamp")
    private LocalDateTime timestamp;
    @Column("is_processed")
    private boolean isProcessed;

    public MonitoredValuesEntity(){

    }

    public MonitoredValuesEntity(
            UUID id,
            UUID deviceId,
            UUID deviceMeasurementId,
            Double monitoredValue,
            LocalDateTime timestamp,
            boolean isProcessed) {
        this.id = id;
        this.deviceId = deviceId;
        this.deviceMeasurementId = deviceMeasurementId;
        this.monitoredValue = monitoredValue;
        this.timestamp = timestamp;
        this.isProcessed = isProcessed;
    }
    public MonitoredValuesEntity(
            UUID deviceId,
            UUID deviceMeasurementId,
            Double monitoredValue,
            LocalDateTime timestamp,
            boolean isProcessed) {
        this.id = UUIDs.timeBased();
        this.deviceId = deviceId;
        this.deviceMeasurementId = deviceMeasurementId;
        this.monitoredValue = monitoredValue;
        this.timestamp = timestamp;
        this.isProcessed = isProcessed;
    }

    public UUID getId() {
        return id;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public UUID getDeviceMeasurementId() {
        return deviceMeasurementId;
    }

    public Double getMonitoredValue() {
        return monitoredValue;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public Long getTimestampInt() {
        return timestamp.toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public void setDeviceMeasurementId(UUID deviceMeasurementId) {
        this.deviceMeasurementId = deviceMeasurementId;
    }

    public void setMonitoredValue(Double monitoredValue) {
        this.monitoredValue = monitoredValue;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setProcessed(boolean processed) {
        isProcessed = processed;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof  MonitoredValuesEntity){
            MonitoredValuesEntity that = (MonitoredValuesEntity) o;
            return this.timestamp.compareTo(that.timestamp);
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MonitoredValuesEntity)) return false;
        MonitoredValuesEntity that = (MonitoredValuesEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(deviceMeasurementId, that.deviceMeasurementId) &&
                Objects.equals(monitoredValue, that.monitoredValue) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, deviceId, deviceMeasurementId, monitoredValue, timestamp);
    }


}
