package ro.tuc.dsrl.catalyst.cassandradbapi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.ScheduledThreadPoolExecutor;

@Configuration
@EnableAsync
public class ThreadConfig {

    @Bean
    public ScheduledThreadPoolExecutor threadPoolExecutor() {
        return new ScheduledThreadPoolExecutor(10);
    }

}
