package ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.builder;

import ro.tuc.dsrl.catalyst.cassandradbapi.model.entity.MonitoredValuesEntity;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.utils.DateUtils;

public class MonitoredValueDTOBuilder {

    private MonitoredValueDTOBuilder(){

    }

    public static MonitoredValueDTO generateDTOFromEntity(MonitoredValuesEntity monitoredValue){

        MonitoredValueDTO m = new MonitoredValueDTO(monitoredValue.getId(),
                DateUtils.dateTimeToMillis(monitoredValue.getTimestamp()),
                monitoredValue.getDeviceId(),
                monitoredValue.getDeviceMeasurementId(),
                monitoredValue.getMonitoredValue(),
                monitoredValue.isProcessed());
        return m;

    }

    public static  MonitoredValuesEntity generateEntityFromDTO(MonitoredValueDTO monitoredValueDTO) {

        return new MonitoredValuesEntity(monitoredValueDTO.getDeviceId(),
                monitoredValueDTO.getDeviceMeasurementId(),
                monitoredValueDTO.getMonitoredValue(),
                DateUtils.millisToUTCLocalDateTime(monitoredValueDTO.getTimestamp()),
                monitoredValueDTO.isProcessed());
    }

}
