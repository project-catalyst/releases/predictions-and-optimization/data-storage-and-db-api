package ro.tuc.dsrl.catalyst.cassandradbapi.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

public class AggregatedValueDTO implements Serializable {

    private LocalDateTime timestamp;
    private double value;
    private UUID deviceId;
    private UUID measurementId;

    public AggregatedValueDTO() {

    }

    public AggregatedValueDTO(LocalDateTime timestamp, Double value, UUID deviceId, UUID measurementId) {
        this.timestamp = timestamp;
        this.value = value;
        this.deviceId = deviceId;
        this.measurementId = measurementId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public Double getValue() {
        return value;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public UUID getMeasurementId() {
        return measurementId;
    }

    @Override
    public String toString() {
        return "AggregatedValueDTO{" +
                "timestamp=" + timestamp +
                ", value=" + value +
                ", deviceId=" + deviceId +
                ", measurementId=" + measurementId +
                '}';
    }

}
