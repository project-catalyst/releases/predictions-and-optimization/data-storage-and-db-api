package ro.tuc.dsrl.catalyst.cassandradbapi.controller.preconditions;

import ro.tuc.dsrl.catalyst.cassandradbapi.service.validators.MonitoredValueValidator;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class MonitoredValuePreconditions {

    private static final Pattern VALID_UUID_REGEX =
            Pattern.compile("\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b",
                    Pattern.CASE_INSENSITIVE);

    private MonitoredValuePreconditions(){

    }

    public static List<String> validateDateInterval(Long startDate, Long endDate){

        List<String> errors = new ArrayList<>();

        if(startDate == null){
            errors.add("start time must not be null");
        }

        if(endDate == null){
            errors.add("end time must not be null");
        }

        return errors;
    }


    public static List<String> validateUUID(String uuid) {

        List<String> errors = new ArrayList<>();
        if (uuid == null) {
            errors.add("ID must not be null");
        } else {
            if (!VALID_UUID_REGEX.matcher(uuid).find()) {
                errors.add("ID does not have the correct format");
            }
        }
        return errors;
    }

    public static List<String> validateParams(Long startDate, Long endDate, String deviceId, String measurementId) {

        List<String> errors = new ArrayList<>();
        List<String> errorsDateInterval = validateDateInterval(startDate, endDate);
        if(!errorsDateInterval.isEmpty()){
            errors.addAll(errorsDateInterval);
        }
        if (deviceId == null) {
            errors.add("Device ID must not be null");
        } else {
            if (!VALID_UUID_REGEX.matcher(deviceId).find()) {
                errors.add("Device ID does not have the correct format");
            }
        }

        if (measurementId == null) {
            errors.add("Measurement ID must not be null");
        } else {
            if (!VALID_UUID_REGEX.matcher(deviceId).find()) {
                errors.add("Measurement ID does not have the correct format");
            }
        }

        return errors;
    }

    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException(MonitoredValueValidator.class.getSimpleName(), errors);
        }
    }
}
