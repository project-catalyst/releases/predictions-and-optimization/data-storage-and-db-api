package ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RestAPIConfig {

    @Value("${python.url}")
    private String flaskAPIHost;

    @Value("${dbapi.url}")
    private String mysqlDBAPIHost;

    public String getFlaskAPIHost() {
        return flaskAPIHost;
    }

    public String getMysqlDBAPIHost() {
        return mysqlDBAPIHost;
    }
}
