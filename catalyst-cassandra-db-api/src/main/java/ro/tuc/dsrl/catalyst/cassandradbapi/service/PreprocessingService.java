package ro.tuc.dsrl.catalyst.cassandradbapi.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.builder.MonitoredValueDTOBuilder;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.entity.MonitoredValuesEntity;
import ro.tuc.dsrl.catalyst.cassandradbapi.repository.MonitoredValueRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class PreprocessingService {

    private final MonitoredValueRepository monitoredValueRepository;

    @Autowired
    public PreprocessingService(MonitoredValueRepository monitoredValueRepository) {
        this.monitoredValueRepository = monitoredValueRepository;
    }

    public List<MonitoredValuesEntity> getGroupedUnlabeledMonitored() {
        return monitoredValueRepository.findAllUnprocessedData();
    }
}
