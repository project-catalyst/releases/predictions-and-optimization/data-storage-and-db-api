package ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection;

public interface ApiURL {

    String value();
}
