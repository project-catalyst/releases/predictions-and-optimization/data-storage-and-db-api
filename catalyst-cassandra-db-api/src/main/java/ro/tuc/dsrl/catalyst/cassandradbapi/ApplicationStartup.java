package ro.tuc.dsrl.catalyst.cassandradbapi;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.AsynchronousService;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private static final Log LOGGER = LogFactory.getLog(ApplicationStartup.class);

    private final AsynchronousService asynchronousService;

    @Value("${start.asynchronous.service}")
    private boolean startAsynchronousServiceFlag;

    @Autowired
    public ApplicationStartup(AsynchronousService asynchronousService) {
        this.asynchronousService = asynchronousService;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        if(startAsynchronousServiceFlag){
            LOGGER.info("Started asynchronous aggregation");
            asynchronousService.executeAsynchronously();
        }

    }

}
