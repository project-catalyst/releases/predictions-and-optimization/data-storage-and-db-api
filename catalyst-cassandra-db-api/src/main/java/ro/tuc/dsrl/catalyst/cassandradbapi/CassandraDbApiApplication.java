package ro.tuc.dsrl.catalyst.cassandradbapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.TimeZone;

@SpringBootApplication
public class CassandraDbApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(CassandraDbApiApplication.class, args);

    }

}

//@SpringBootApplication
//public class CassandraDbApiApplication {
//    public static void main(String[] args) {
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//        SpringApplication.run(CassandraDbApiApplication.class, args);
//    }
//}
