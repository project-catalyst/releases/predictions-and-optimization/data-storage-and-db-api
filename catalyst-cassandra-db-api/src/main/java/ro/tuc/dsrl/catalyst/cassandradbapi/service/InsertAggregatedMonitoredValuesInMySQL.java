package ro.tuc.dsrl.catalyst.cassandradbapi.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.AggregatedValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection.MySqlHostURL;
import ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection.RestAPIClient;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class InsertAggregatedMonitoredValuesInMySQL  implements Runnable{

    private static final Log LOGGER = LogFactory.getLog(InsertAggregatedMonitoredValuesInMySQL.class);

    private final MonitoredValueService monitoredValueService;
    private final RestAPIClient accessPoint;
    private boolean simulationOn;
    private boolean debugMode;
    private int dataGranularity;

    public InsertAggregatedMonitoredValuesInMySQL(MonitoredValueService monitoredValueService,
                                                  @Value("${data.granularity}") int dataGranularity,
                                                  RestAPIClient accessPoint, @Value("${simulated.time}") boolean simulationOn,
                                                  @Value("${debug.mode}") boolean debugMode){

        this.monitoredValueService = monitoredValueService;
        this.simulationOn = simulationOn;
        this.accessPoint = accessPoint;
        this.debugMode = debugMode;
        this.dataGranularity = dataGranularity;
    }

    @Override
    public void run() {

        long startAsync = System.currentTimeMillis();

        if(simulationOn){
            if(debugMode) {
                LOGGER.info("Running in simulation mode");
            }
        }

        if(debugMode) {
            LOGGER.info("Started new data chunk aggregation ...");

        }

        List<MonitoredValueDTO> monitoredValueDTOList = monitoredValueService.findAllUnproccesedValues();

        if(!monitoredValueDTOList.isEmpty()){

            // group monitorings by device measurement and by device
            Map<Pair<UUID, UUID>, List<MonitoredValueDTO>> monitoredValuesGroupedByDeviceAndDeviceMeasurement =
                    monitoredValueDTOList
                            .stream()
                            .sorted(Comparator.comparing(MonitoredValueDTO::getTimestamp))
                            .collect(Collectors.groupingBy(p -> Pair.of(p.getDeviceId(), p.getDeviceMeasurementId())));


            // aggregate values for each group
            for(Pair<UUID, UUID> key : monitoredValuesGroupedByDeviceAndDeviceMeasurement.keySet()){

                List<MonitoredValueDTO> rawEntries = monitoredValuesGroupedByDeviceAndDeviceMeasurement.get(key);

                //order raw entries by timestamp
                Comparator<MonitoredValueDTO> compareByTimestamp = (MonitoredValueDTO m1, MonitoredValueDTO m2) -> m1.getTimestamp().compareTo(m2.getTimestamp());
                Collections.sort(rawEntries, compareByTimestamp);

                // impute missing data on entries
                List<MonitoredValueDTO> entries = new ArrayList<>(rawEntries.size());
                if(!rawEntries.isEmpty() &&  rawEntries.size() > 2) {
                    entries = this.imputeMissingData(rawEntries);
                }else{
                    entries.addAll(rawEntries);
                }

                if(debugMode){
                    LOGGER.info("Raw data: " + rawEntries);
                    LOGGER.info("Imputed data: " + entries);
                }

                if(debugMode)
                {
                    LOGGER.info("List of timestamps to be aggregated: " + entries.stream()
                            .map(e -> Instant.ofEpochMilli(e.getTimestamp()).atZone(ZoneId.systemDefault()).toLocalDateTime())
                            .collect(Collectors.toList()));
                    LOGGER.info("List of values to be aggregated: " + entries.stream()
                            .map(e -> e.getMonitoredValue())
                            .collect(Collectors.toList()));
                }


                List<AggregatedValueDTO> aggregatedMonitoredValues = new ArrayList<>();

                if(entries.size() <= 1){

                    //do nothing as their is none or single value that will be passed to next run
                    if(debugMode) {
                        LOGGER.info("List of timestamps aggregated: " + aggregatedMonitoredValues);
                        LOGGER.info("List of values aggregated: " + aggregatedMonitoredValues.stream()
                                .map(e -> e.getValue())
                                .collect(Collectors.toList()));
                    }

                }else{

                    long startIntervalTime = entries.get(0).getTimestamp();
                    long endIntervalTime = entries.get(entries.size()-1).getTimestamp();
                    LocalDateTime startIntervalTimeLDT = Instant.ofEpochMilli(startIntervalTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
                    LocalDateTime endIntervalTimeLDT = Instant.ofEpochMilli(endIntervalTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
                    List<LocalDateTime> intervals = this.computeIntervals(startIntervalTimeLDT, endIntervalTimeLDT, dataGranularity);

                    if(debugMode){
                        LOGGER.info("Startime detected: " + startIntervalTimeLDT.toString());
                        LOGGER.info("Endtime detected: " + endIntervalTimeLDT.toString());
                        LOGGER.info("Intervals detected: " + intervals.toString());
                    }

                    if(intervals.size() == 1) {

                        // if there is one single interval detected
                        if(endIntervalTimeLDT.minusMinutes(intervals.get(0).getMinute()).getMinute() == dataGranularity){
                            // time interval is fully completed
                            // only one time window mediate all data inside
                            aggregatedMonitoredValues.add(sampleRegion(startIntervalTimeLDT, endIntervalTimeLDT, entries, true));
                        }

                    } else {

                            // more than one interval
                            Iterator<LocalDateTime> it = intervals.iterator();
                            LocalDateTime previous = null;

                            while(it.hasNext()) {

                                LocalDateTime current = it.next();

                                if(previous != null) {

                                    if(debugMode) {
                                        LOGGER.info("Current time index: " + current.toString());
                                        LOGGER.info("Previous time index: " + previous.toString());
                                    }

                                    aggregatedMonitoredValues.add(sampleRegion(previous, current, entries, false));

                                    if(!it.hasNext() && (current.isBefore(endIntervalTimeLDT) || current.isEqual(endIntervalTimeLDT))) {

                                        if( (endIntervalTimeLDT.getMinute() - current.getMinute()) == dataGranularity){
                                            // last interval is fully completed
                                            aggregatedMonitoredValues.add(sampleRegion(current, endIntervalTimeLDT, entries, true));
                                        }
                                    }
                                }
                                previous = current;
                            }
                        }

                    if(debugMode) {
                        LOGGER.info("List of timestamps aggregated: " + aggregatedMonitoredValues.stream()
                                .map(e -> e.getTimestamp())
                                .collect(Collectors.toList()));
                        LOGGER.info("List of values aggregated: " + aggregatedMonitoredValues.stream()
                                .map(e -> e.getValue())
                                .collect(Collectors.toList()));
                    }

                    if(!aggregatedMonitoredValues.isEmpty()) {
                        for (AggregatedValueDTO m : aggregatedMonitoredValues) {
                            // insert values in MySQL
                            UUID insertedValueID = accessPoint.postObject(MySqlHostURL.INSERT_MONITORED_VALUES,
                                    m,
                                    UUID.class);
                            if (debugMode){
                                LOGGER.info("Aggregated value inserted succesfully with UUID: " + insertedValueID.toString());
                            }
                        }
                    }
                }
            }

        }else{
            LOGGER.info("MonitoredValuesList empty");
        }

        long finishAsync = System.currentTimeMillis();
        long timeElapsed = finishAsync - startAsync;

        if (debugMode) {
            LOGGER.info("Time elapsed for the async service: " + timeElapsed);
        }
    }

    private List<LocalDateTime> computeIntervals(LocalDateTime startTime, LocalDateTime endTime, long frequency) {

        List<LocalDateTime> timeIndices = new ArrayList<>();

        for(LocalDateTime time = startTime; time.isBefore(endTime) || time.isEqual(endTime); time = time.plusMinutes(frequency)) {
            timeIndices.add(time);
        }

        return timeIndices;
    }

    private AggregatedValueDTO sampleRegion(LocalDateTime startTime, LocalDateTime endTime,
                                            List<MonitoredValueDTO> entries, boolean includeEnd){

        if(debugMode) {
            LOGGER.info("Sample region between " + startTime + " " + endTime);
        }

        double aggregatedSumPerRegion = 0.0;
        double noEntriesPerRegion = 0;
        List<MonitoredValueDTO> processedValues = new ArrayList();

        for(MonitoredValueDTO m:entries){
            LocalDateTime mTimestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(m.getTimestamp()), ZoneOffset.UTC);
            if(includeEnd) {
                if ((mTimestamp.isEqual(startTime) || mTimestamp.isAfter(startTime)) &&
                        (mTimestamp.isBefore(endTime) || (mTimestamp.isEqual(endTime)))){

                    aggregatedSumPerRegion += m.getMonitoredValue();
                    noEntriesPerRegion++;
                    processedValues.add(m);
                }
            }else {
                if ((mTimestamp.isEqual(startTime) || mTimestamp.isAfter(startTime)) && mTimestamp.isBefore(endTime)) {
                    aggregatedSumPerRegion += m.getMonitoredValue();
                    noEntriesPerRegion++;
                    processedValues.add(m);
                }
            }

        }

        List<UUID> processedUUIDs = this.updateProccesedValues(processedValues);
        if(debugMode){
            LOGGER.info("List of processed values: " + processedValues);
            LOGGER.info("Processed UUIDs " + processedUUIDs);
        }

		double aggregatedValuePerRegion = 0.0;
		if(noEntriesPerRegion != 0){
			aggregatedValuePerRegion = aggregatedSumPerRegion/noEntriesPerRegion;
		}

		AggregatedValueDTO aggregatedValueDTO = new AggregatedValueDTO(startTime,
                aggregatedValuePerRegion,
                entries.get(0).getDeviceId(),
                entries.get(0).getDeviceMeasurementId());
        if(debugMode) {

            LOGGER.info("Inserting AggregatedValueDTO: " + aggregatedValueDTO);
        }

        return aggregatedValueDTO;
    }

    private List<UUID> updateProccesedValues(List<MonitoredValueDTO> processedValues){

        List<UUID> addedUUIDs = new ArrayList<>();

        if(!processedValues.isEmpty()) {
            for (MonitoredValueDTO m : processedValues) {
                UUID updatedValueUUID = monitoredValueService.updateFlag(m, true);
                if(debugMode) {
                    LOGGER.info("Updated value: " + m);
                }
                addedUUIDs.add(updatedValueUUID);
            }
        }

        return addedUUIDs;
    }

    private List<MonitoredValueDTO> imputeMissingData(List<MonitoredValueDTO> rawEntries){

        List<MonitoredValueDTO> entries = new ArrayList<>(rawEntries.size());

        // find average of non-zero elements in rawEntries list
        double sumRawEntries = 0.0;
        for(int i=0; i<rawEntries.size(); i++){
            if(rawEntries.get(i).getMonitoredValue() != 0.0) {
                sumRawEntries += rawEntries.get(i).getMonitoredValue();
            }
        }
        double meanRawEntries = sumRawEntries/rawEntries.size();

        for(int i=0; i < rawEntries.size(); i++){
            if(rawEntries.get(i).getMonitoredValue() == 0.0){
                entries.add(new MonitoredValueDTO(rawEntries.get(i).getId(),
                        rawEntries.get(i).getTimestamp(),
                        rawEntries.get(i).getDeviceId(),
                        rawEntries.get(i).getDeviceMeasurementId(),
                        meanRawEntries,
                        false));
            }else{
                entries.add(new MonitoredValueDTO(rawEntries.get(i).getId(),
                        rawEntries.get(i).getTimestamp(),
                        rawEntries.get(i).getDeviceId(),
                        rawEntries.get(i).getDeviceMeasurementId(),
                        rawEntries.get(i).getMonitoredValue(),
                        false));
            }
        }
        return entries;
    }
}
