package ro.tuc.dsrl.catalyst.cassandradbapi.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.entity.MonitoredValuesEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface MonitoredValueRepository extends CassandraRepository<MonitoredValuesEntity, UUID> {

    @Query("SELECT * FROM catalyst.monitored_values")
    List<MonitoredValuesEntity> findAll();

    @Query("SELECT * FROM catalyst.monitored_values WHERE timestamp >= ?0 AND timestamp < ?1 ALLOW FILTERING")
    List<MonitoredValuesEntity> findAllByTimestampInterval(LocalDateTime start, LocalDateTime end);

    @Query("SELECT * FROM catalyst.monitored_values WHERE device_id = ?0 ALLOW FILTERING")
    List<MonitoredValuesEntity> findAllByDeviceId(UUID deviceId);

    @Query("SELECT * FROM catalyst.monitored_values WHERE device_measurement_id = ?0 ALLOW FILTERING")
    List<MonitoredValuesEntity> findAllByMeasurementId(UUID measurementId);

    @Query("SELECT * FROM catalyst.monitored_values WHERE timestamp >= ?0 AND timestamp < ?1 AND " +
            "device_id = ?2 AND device_measurement_id = ?3 ALLOW FILTERING")
    List<MonitoredValuesEntity> findAllByTimestampIntervalForDeviceAndMeasurement(LocalDateTime start, LocalDateTime end, UUID deviceId, UUID measurementId);

    @Query("SELECT * FROM catalyst.monitored_values WHERE is_processed = false ALLOW FILTERING")
    List<MonitoredValuesEntity> findAllUnprocessedData();

    @Query("SELECT * FROM catalyst.monitored_values WHERE id = ?0 ALLOW FILTERING")
    MonitoredValuesEntity findByUUID(UUID id);

    @Query("UPDATE catalyst.monitored_values SET is_processed = true WHERE id = ?0")
    MonitoredValuesEntity updateProcessedData(UUID uuid);
}
