package ro.tuc.dsrl.catalyst.cassandradbapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.dsrl.catalyst.cassandradbapi.controller.preconditions.MonitoredValuePreconditions;
import ro.tuc.dsrl.catalyst.cassandradbapi.exceptionHandler.RestResponseEntityErrorHandler;
import ro.tuc.dsrl.catalyst.cassandradbapi.model.dto.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.cassandradbapi.service.validators.MonitoredValueValidator;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@EnableRabbit
public class SensorMessageConsumer {

    private static final Log LOGGER = LogFactory.getLog(SensorMessageConsumer.class);

    private final MonitoredValueService monitoredValueService;
    private boolean debugMode;

    @Autowired
    public SensorMessageConsumer(MonitoredValueService monitoredValueRepository,
                                 @Value("${debug.mode}") boolean debugMode) {

        this.monitoredValueService = monitoredValueRepository;
        this.debugMode = debugMode;

    }

    @RabbitListener(queues = "${jsa.rabbitmq.genericqueue}")
    public void receiveMessage(final Message message) {

        message.getMessageProperties().setContentType("application/json");

        // Take the monitored values and insert them in Cassandra
        ObjectMapper mapper = new ObjectMapper();
        MonitoredValueDTO monitoredValueDTO = new MonitoredValueDTO();
        String messageBody = new String(message.getBody());
        if (messageBody != null && !messageBody.equals("")) {

            try {
                monitoredValueDTO = mapper.readValue(message.getBody(), MonitoredValueDTO.class);
            } catch (IOException e) {
                LOGGER.error(e);
            }

            if (monitoredValueDTO != null && !monitoredValueDTO.checkIfEmpty() && !monitoredValueDTO.checkIfNotValid()) {

                monitoredValueDTO.setProcessed(false);
                String receivedData = monitoredValueDTO.toString();

                if (debugMode) {
                        LOGGER.info("Received message " + receivedData);
                }

                // Insert new monitored value in Cassandra database
                UUID uuid = monitoredValueService.insert(monitoredValueDTO);
                if (debugMode) {

                    if (uuid == null) {
                        LOGGER.info("Monitored value could not be inserted in Cassandra DB " + receivedData);
                    } else {
                        LOGGER.info("Monitored value inserted in Cassandra DB " + receivedData);
                    }
                }
            }
        }
    }
}

