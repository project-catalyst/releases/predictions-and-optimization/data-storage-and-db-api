package ro.tuc.dsrl.catalyst.cassandradbapi.remoteConnection;

public enum MySqlHostURL implements ApiURL  {

    INSERT_MONITORED_VALUES("monitored-value/insert");

    private String value;

    MySqlHostURL(String url) {
        this.value = url;
    }

    public String value() {
        return value;
    }

}
