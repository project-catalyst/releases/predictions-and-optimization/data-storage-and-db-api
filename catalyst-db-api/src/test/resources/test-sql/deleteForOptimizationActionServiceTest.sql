SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE optimization_action;
TRUNCATE TABLE optimization_plans;


TRUNCATE TABLE measure_unit;
TRUNCATE TABLE value_type;
TRUNCATE TABLE device;
TRUNCATE TABLE device_type;


SET FOREIGN_KEY_CHECKS = 1;