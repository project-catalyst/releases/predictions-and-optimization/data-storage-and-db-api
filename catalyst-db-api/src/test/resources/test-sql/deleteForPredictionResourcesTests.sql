SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE monitored_value;
TRUNCATE TABLE device;
TRUNCATE TABLE measurement;
TRUNCATE TABLE device_type;
TRUNCATE TABLE property_source;
TRUNCATE TABLE measure_unit;
TRUNCATE TABLE value_type;
TRUNCATE TABLE predicted_value;
TRUNCATE TABLE prediction_job;
TRUNCATE TABLE trained_model;

SET FOREIGN_KEY_CHECKS = 1;