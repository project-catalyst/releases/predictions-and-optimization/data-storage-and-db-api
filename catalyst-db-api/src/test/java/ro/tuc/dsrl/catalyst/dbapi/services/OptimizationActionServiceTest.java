package ro.tuc.dsrl.catalyst.dbapi.services;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.dsrl.catalyst.dbapi.SpringBootTestConfig;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.enums.ActionTypeName;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.service.basic.OptimizationActionService;
import ro.tuc.dsrl.dbapi.service.basic.OptimizationPlanService;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
import ro.tuc.dsrl.geyser.datamodel.actions.ShiftDelayTolerantWorkload;
import ro.tuc.dsrl.geyser.datamodel.actions.WorkloadRelocation;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/insertForOptimizationActionServiceTest.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/deleteForOptimizationActionServiceTest.sql")
public class OptimizationActionServiceTest extends SpringBootTestConfig {

    @Autowired
    OptimizationActionService optimizationActionService;
    @Autowired
    OptimizationPlanService optimizationPlanService;

    private final Logger LOG = LoggerFactory.getLogger(OptimizationActionServiceTest.class);

    private final Date plansStartDate = new Date(1573430400000L); // 2019-11-11 00:00:00
    private final Date plansEndDate = new Date(1573444800000L); // 2019-11-11 00:04:00
    private final double planConfidenceLevel = 0.0;

    // ACTION 1 (COOLING)
    private final Date startTimeAction1 = new Date(1573430400000L);  // 2019-11-11 00:00:00
    private final Date endTimeAction1 = new Date(1573432200000L);  // 2019-11-11 00:30:00
    private final double coolingAction1Amount = 125.00;

    // ACTION 2 (SHIFT DELAY-TOLERANT WORKLOAD)
    private final Date startTimeAction2 = new Date(1573434000000L);  // 2019-11-11 01:00:00
    private final Date endTimeAction2 = new Date(1573435800000L); // 2019-11-11 02:00:00
    private final double  sdtwAction2Amount = 77.77;
    private final Date sdtwAction2FromTime = new Date(1573434000000L); // 2019-11-11 01:00:00
    private final Date sdtwAction2ToTime = new Date(1573435800000L); // 2019-11-11 02:00:00
    private final double percentageWorkloadDelayedAction2 = 1.0;

    // ACTION 3 (RELOCATE WORKLOAD)
    private final Date startTimeAction3 = new Date(1573441200000L); // 2019-11-11 03:00:00
    private final Date endTimeAction3 = new Date(1573443000000L); // 2019-11-11 03:30:00
    private double relocateWorkloadAction3Amount = 96.00;
    private double percentageOfWorkloadReallocatedAction3 = 1.0;

    private final Timeframe timeframe = Timeframe.INTRA_DAY;
    private final String DATACENTER_POZNAN = "DATACENTER_POZNAN";


    @Before
    public void setup() {

        LOG.info("Before running tests from OptimizationActionServiceTest");
    }

    @Test
    public void testInsertOptimizationPlanContainer(){
        OptimizationPlansContainer container = new OptimizationPlansContainer();

        List<EnergyEfficiencyOptimizationAction> actionPool = new ArrayList<>();

        // ADD ACTIONS TO OPTIMIZATION PLAN 1
        EnergyEfficiencyOptimizationAction coolingAction1 =
                new EnergyEfficiencyOptimizationAction(0, ActionTypeName.DYNAMIC_ADJUSTMENT_OF_COOLING_INTENSITY.getName(), startTimeAction1, endTimeAction1, coolingAction1Amount);

        ShiftDelayTolerantWorkload shiftDelayTolerantAction2 =
                new ShiftDelayTolerantWorkload(0, sdtwAction2Amount,  startTimeAction2, endTimeAction2, 0, sdtwAction2FromTime, sdtwAction2ToTime, percentageWorkloadDelayedAction2);

        WorkloadRelocation relocateWorkloadAction3 = new WorkloadRelocation(0, relocateWorkloadAction3Amount, startTimeAction3, endTimeAction3, percentageOfWorkloadReallocatedAction3, relocateWorkloadAction3Amount, "CURRENT_DC");

        actionPool.add(coolingAction1);
        actionPool.add(shiftDelayTolerantAction2);
        actionPool.add(relocateWorkloadAction3);

        OptimizerPlan plan1 = new OptimizerPlan(0, actionPool, planConfidenceLevel);

        List<OptimizerPlan> optimizations = new ArrayList<>();
        optimizations.add(plan1);

        // ADD OPTIMIZATION PLAN 1 TO OPTIMIZATION PLAN CONTAINER
        container.setTimeframe(timeframe.getName());
        container.setStartDate(plansStartDate);
        container.setEndDate(plansEndDate);
        container.setOptimizations(optimizations);
        container.setDataCenterName(DATACENTER_POZNAN);

        // INSERT OPTIMIZATION PLAN CONTAINER INTO THE DATABASE
        optimizationPlanService.insert(container);

        // CHECK IF ACTIONS WHERE INSERTED CORRECTLY
        LocalDateTime plan1StartTime  = DateUtils.millisToUTCLocalDateTime(plansStartDate.getTime());
        LocalDateTime plan1EndTime  = DateUtils.millisToUTCLocalDateTime(plansEndDate.getTime());
        List<OptimizationActionDTO> insertedActions = this.optimizationActionService.getAllActionsInInterval(plan1StartTime, plan1EndTime, planConfidenceLevel, timeframe, DATACENTER_POZNAN);

        assert insertedActions.size() == 3;


        List<OptimizationActionDTO> coolActions = this.optimizationActionService.getActionsInIntervalByType(DATACENTER_POZNAN,plan1StartTime, plan1EndTime, ActionTypeName.DYNAMIC_ADJUSTMENT_OF_COOLING_INTENSITY, planConfidenceLevel, timeframe );
        assert coolActions.size() == 1;
        assert coolActions.get(0).getAmount() == coolingAction1.getAmountOfEnergy();
        assert coolActions.get(0).getStartTime() == (coolingAction1.getStartTime().getTime());
        assert coolActions.get(0).getEndTime() == (coolingAction1.getEndTime().getTime());

        List<OptimizationActionDTO> sdtwActions = this.optimizationActionService.getActionsInIntervalByType(DATACENTER_POZNAN,plan1StartTime, plan1EndTime, ActionTypeName.SHIFT_DELAY_TOLERANT_WORKLOAD, planConfidenceLevel, timeframe );
        assert sdtwActions.size() == 1;
        assert sdtwActions.get(0).getAmount() == shiftDelayTolerantAction2.getAmountOfEnergy();
        assert sdtwActions.get(0).getStartTime() == (shiftDelayTolerantAction2.getStartTime().getTime());
        assert sdtwActions.get(0).getEndTime() == (shiftDelayTolerantAction2.getEndTime().getTime());
        assert sdtwActions.get(0).getMoveFromDate() == (shiftDelayTolerantAction2.getFromTime().getTime());
        assert sdtwActions.get(0).getMovePercentage() == (shiftDelayTolerantAction2.getPercentOfWorkloadDelayed());


        List<OptimizationActionDTO> relocActions = this.optimizationActionService.getActionsInIntervalByType(DATACENTER_POZNAN,plan1StartTime, plan1EndTime, ActionTypeName.RELOCATE_WORKLOAD, planConfidenceLevel, timeframe );
        assert relocActions.size() == 1;
        assert relocActions.get(0).getAmount() == relocateWorkloadAction3.getAmountOfEnergy();
        assert relocActions.get(0).getStartTime() == (relocateWorkloadAction3.getStartTime().getTime());
        assert relocActions.get(0).getEndTime() == (relocateWorkloadAction3.getEndTime().getTime());
        assert relocActions.get(0).getMovePercentage() == (relocateWorkloadAction3.getPercentOfWorkloadReallocated());
    }






    @After
    public void resetDb(){
        LOG.info("After running tests from OptimizationActionServiceTest");


    }

}