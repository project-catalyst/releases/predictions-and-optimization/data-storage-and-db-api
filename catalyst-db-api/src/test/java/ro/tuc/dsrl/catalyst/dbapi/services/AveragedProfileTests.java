package ro.tuc.dsrl.catalyst.dbapi.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.dsrl.catalyst.dbapi.SpringBootTestConfig;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.service.DataCSVReadService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/insertForDCResourcesTests.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/deleteForDCResourcesTests.sql")
public class AveragedProfileTests extends SpringBootTestConfig {
    private static final Log LOGGER = LogFactory.getLog(AveragedProfileTests.class);
    private static final String DATACENTER_NAME = "DATACENTER_POZNAN";
    private static final Double EPSILON = 0.01;

    @Autowired
    private EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    @Autowired
    private  DataCSVReadService dataCSVReadService;
    private static final ArrayList<Double> DA_BASELINE = new ArrayList<>(
            Arrays.asList(1024.077576,1021.510606,1020.39,1019.725455,1020.989394,1026.383939,1031.778485,
                    1037.524848,1043.310303, 1047.545152, 1050.020909, 1052.457576, 1053.91697, 1055.376364, 1056.301515,
                    1056.900909, 1057.343939,1057.161515,1056.979091,1057.565455,1058.412424,1060.171515,1063.715758, 1067.26));

    private static final ArrayList<Double> ID_BASELINE = new ArrayList<>(
            Arrays.asList(1024.077576,1024.077576,1021.510606, 1021.510606, 1020.39, 1020.39,  1019.725455,  1019.725455,
        1020.989394,   1020.989394, 1026.383939, 1026.383939, 1031.778485, 1031.778485, 1037.524848, 1037.524848,  1043.310303,
        1043.310303, 1047.545152, 1047.545152,  1050.020909, 1050.020909, 1052.457576,  1052.457576, 1053.91697, 1053.91697,
        1055.376364, 1055.376364, 1056.301515, 1056.301515, 1056.900909,1056.900909,  1057.343939, 1057.343939, 1057.161515,
        1057.161515,  1056.979091, 1056.979091,  1057.565455,  1057.565455, 1058.412424, 1058.412424, 1060.171515,
        1060.171515, 1063.715758,  1063.715758,    1067.26,    1067.26   ));

    @BeforeClass
    public static void config(){
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void testBaselineDA(){
        assert setupSignals();
        LocalDateTime start = LocalDateTime.of(2019, 6, 29, 0, 0, 0);
        LocalDateTime end = start.plusHours(24);
        List<AggregatedValuesDTO> dto = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(DATACENTER_NAME, start, end, PredictionGranularity.DAYAHEAD,
                MeasurementType.DATA_CENTER_BASELINE);
        checkProfiles(dto, DA_BASELINE);
    }

    @Test
    public void testBaselineID(){
        assert setupSignals();
        LocalDateTime start = LocalDateTime.of(2019, 6, 29,0, 0, 0);
        LocalDateTime end = LocalDateTime.of(2019, 6, 29, 4, 0, 0);
        List<AggregatedValuesDTO> dto = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(DATACENTER_NAME, start, end, PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_BASELINE);
        checkProfiles(dto, ID_BASELINE.subList(0,8));

         start = LocalDateTime.of(2019, 6, 29, 4, 0, 0);
         end = LocalDateTime.of(2019, 6, 29, 8, 0, 0);
         dto = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(DATACENTER_NAME, start, end, PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_BASELINE);
        checkProfiles(dto, ID_BASELINE.subList(8,16));

        start = LocalDateTime.of(2019, 6, 29, 8, 0, 0);
        end = LocalDateTime.of(2019, 6, 29, 12, 0, 0);
        dto = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(DATACENTER_NAME, start, end, PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_BASELINE);
        checkProfiles(dto, ID_BASELINE.subList(16, 24));

        start = LocalDateTime.of(2019, 6, 29, 12, 0, 0);
        end = LocalDateTime.of(2019, 6, 29, 16, 0, 0);
        dto = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(DATACENTER_NAME, start, end, PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_BASELINE);
        checkProfiles(dto, ID_BASELINE.subList(24, 32));

        start = LocalDateTime.of(2019, 6, 29, 16, 0, 0);
        end = LocalDateTime.of(2019, 6, 29, 20, 0, 0);
        dto = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(DATACENTER_NAME, start, end, PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_BASELINE);
        checkProfiles(dto, ID_BASELINE.subList(32, 40));

        start = LocalDateTime.of(2019, 6, 29, 20, 0, 0);
        end = LocalDateTime.of(2019, 6, 30, 0, 0, 0);
        dto = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(DATACENTER_NAME, start, end, PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_BASELINE);
        checkProfiles(dto, ID_BASELINE.subList(40, 48));
    }

    private void checkProfiles(List<AggregatedValuesDTO> profile, List<Double> refrence){

        assert profile!=null;
        assert profile.size() == refrence.size();
        for(int i =0; i<profile.size();i++){
            assertEquals("Profile value " + i, Math.abs(refrence.get(i) - profile.get(i).getValue()) < EPSILON, true);
        }
    }

    private boolean setupSignals(){

        String folder = "/test-data/scenario0";
        final String drSignalDA = folder+"/dr_signalDA.xls";
        final String drSignalID = folder+"/dr_signalID.xls";
        final String setTotalPower = folder+"/TotalPower.xls";
        final String setPricesDA = folder+"/PricesDA.xls";
        final String setPricesID = folder+"/PricesID.xls";
        final String setPredictedDA = folder+"/EnergyPredictionDA.xls";
        final String setPredictedID = folder+"/EnergyPredictionID.xls";
        final String baselineDA = folder+"/EnergyBaselineDA.xls";
        final String baselineID = folder+"/EnergyBaselineID.xls";

        LocalDateTime dt =  DateUtils.isoStringToLocalDateTime("2019-06-29T00:00:00Z");
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        boolean drID =  dataCSVReadService.populateDRSIGNAL(drSignalID,DATACENTER_NAME, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE DR SIGNAL INTRADAY " + drID);
        boolean drDA =dataCSVReadService.populateDRSIGNAL(drSignalDA,DATACENTER_NAME, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE DR SIGNAL DAYAHEAD " + drDA);
        boolean tp = dataCSVReadService.populatePreOptimizationProfiles(setTotalPower, DATACENTER_NAME, dt);
        LOGGER.info("POPULATE TOTAL POWER " + tp);
        boolean pricesID =dataCSVReadService.populatePriceData(setPricesID, DATACENTER_NAME, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE PRICE INTRADAY " + pricesID);
        boolean pricesDA =dataCSVReadService.populatePriceData(setPricesDA, DATACENTER_NAME, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE PRICE DAYAHEAD "+ pricesDA);
        boolean predID =dataCSVReadService.populatePredictedData(setPredictedID, DATACENTER_NAME, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30, MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION);
        LOGGER.info("POPULATE PREDICTED INTRADAY " + predID);
        boolean predDA =dataCSVReadService.populatePredictedData(setPredictedDA, DATACENTER_NAME, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION);
        LOGGER.info("POPULATE PREDICTED DAYAHEAD "+ predDA);
        boolean baseeID =dataCSVReadService.populateBaselineData(baselineID, DATACENTER_NAME, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE BASELINE INTRADAY "+baseeID);
        boolean baseDA = dataCSVReadService.populateBaselineData(baselineDA, DATACENTER_NAME, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE BASELINE DAYAHEAD "+ baseDA);
        return drID && drDA && tp && pricesID && pricesDA && predID && predDA && baseeID && baseDA;
    }
}
