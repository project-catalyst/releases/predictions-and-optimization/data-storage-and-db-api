package ro.tuc.dsrl.catalyst.dbapi.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.dsrl.catalyst.dbapi.SpringBootTestConfig;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.MonitoredValueService;
import ro.tuc.dsrl.geyser.datamodel.actions.EnergyConsumptionOptimization;
import ro.tuc.dsrl.geyser.datamodel.components.Component;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.HeatRecoveryInfrastructure;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.time.LocalDateTime;
import java.util.*;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/insertForDCResourcesTests.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/deleteForDCResourcesTests.sql")
public class DCResourcesTests extends SpringBootTestConfig {

    private static final String DATACENTER_POZNAN = "DATACENTER_POZNAN";
    private static final Double EPSILON = 0.01;
    private static final String UUID_SERVER_DEVICE_TYPE = "78C2A82C-C3AF-1C1B-4D3D-C2A87027C39F";
    private static final String UUID_DC_TYPE = "C29D2B6D-C3BD-C2B4-C38E-4329C2A4C3BE";
    private static final String UUID_DC_POZNAN = "C382C39F-C2A5-C2BE-C38E-5011C3A9C2AD";
    private static final String UUID_MAX_IT_MEASUREMENT = "F4E77279-0171-32EA-9673-00155DB12D03";
    private static final String UUID_E_D_Percentage = "316712B6-95A1-418E-B126-42E7912CE5B2";

    @Autowired
    DeviceService deviceService;

    @Autowired
    MonitoredValueService mvService;

    @Test
    public void testInsert() {

        //insert server device in another DC
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        DeviceDTO dto2 = new DeviceDTO(UUID.randomUUID(), "SERVER_OTHER_DC", UUID.fromString(UUID_SERVER_DEVICE_TYPE), parentID);
        deviceService.insert(dto2);

        List<ServerRoom> rooms = deviceService.getServerRoom(DATACENTER_POZNAN, LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        assert rooms.size() == 1;
        ServerRoom room = rooms.get(0);

        System.out.println("Room max energy: " + room.getMaxEnergyConsumption());
        assertEquals("MAX ENERGY", Math.abs(room.getMaxEnergyConsumption() - 1000.00) < EPSILON, true);
        assertEquals("ENERGY CONSUMPTION", Math.abs(room.getEnergyConsumption() - 734.729) < EPSILON, true);
        assertEquals("E_D_Percentage ", Math.abs(room.getEdPercentage() - 0.3) < EPSILON, true);

    }


    @Test(expected = IncorrectParameterException.class)
    public void testInsertFail() {
        DeviceDTO dto = new DeviceDTO(null, null, null, null);
        deviceService.insert(dto);
    }

    @Test(expected = EntityValidationException.class)
    public void testInsertFail2() {

        DeviceDTO dto = new DeviceDTO(UUID.randomUUID(), "SERVER_NEW", UUID.fromString("78C2A843-C3AF-1C1B-4D3D-C2A87027C39F"), UUID.fromString("78C2A843-C3AF-143B-4D3D-C2A87027C39F"));
        deviceService.insert(dto);
    }

    @Test
    public void testServerRooms() {
        List<ServerRoom> rooms = deviceService.getServerRoom(DATACENTER_POZNAN, LocalDateTime.of(2019, 9, 9, 3, 0, 0));
        assert rooms.size() == 1;
        ServerRoom room = rooms.get(0);

        assertEquals("MAX ENERGY", Math.abs(room.getMaxEnergyConsumption() - 1000) < EPSILON, true);
        assertEquals("ENERGY CONSUMPTION", Math.abs(room.getEnergyConsumption() - 734.729) < EPSILON, true);
        assertEquals("E_D_Percentage ", Math.abs(room.getEdPercentage() - 0.3) < EPSILON, true);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsInsertWrongParameter0() {
        ServerRoom serverRoom = new ServerRoom(null, 1.0, 1.0, 1.0, 1.0, 1.0);
        deviceService.insertServerRoom(serverRoom, "testServerRoom");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsInsertWrongParameter1() {
        ServerRoom serverRoom = new ServerRoom("", 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertServerRoom(serverRoom, "testServerRoom");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsInsertWrongParameter3() {
        ServerRoom serverRoom = new ServerRoom("testServerRoom", 1.0, -1.0, 1.0, 1.0, 1.0);

        deviceService.insertServerRoom(serverRoom, "testServerRoom");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsInsertWrongParameter4() {
        ServerRoom serverRoom = new ServerRoom("testServerRoom", 1.0, 1.0, -1.0, 1.0, 1.0);

        deviceService.insertServerRoom(serverRoom, "testServerRoom");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsInsertWrongParameter5() {
        ServerRoom serverRoom = new ServerRoom("testServerRoom", 1.0, 1.0, 1.0, -1.0, 1.0);

        deviceService.insertServerRoom(serverRoom, "testServerRoom");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsInsertWrongParameter6() {
        ServerRoom serverRoom = new ServerRoom("testServerRoom", 1.0, 1.0, 1.0, 1.0, -1.0);

        deviceService.insertServerRoom(serverRoom, "testServerRoom");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsInsertDuplicateLabel() {
        ServerRoom serverRoom = new ServerRoom("testServerRoomDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0);
        ServerRoom serverRoom2 = new ServerRoom("testServerRoomDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertServerRoom(serverRoom, "testServerRoomDuplicate");
        deviceService.insertServerRoom(serverRoom2, "testServerRoomDuplicate");
    }

    @Test
    public void testServerRoomsInsertCorrect() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        ServerRoom serverRoom = new ServerRoom("testServerRoom", 1.0, 1.0, 1.0, 1.0, 1.0);

        List<ServerRoom> insertedServerRooms = deviceService.insertServerRoom(serverRoom, "DC_NEW");

        assertEquals("SERVER ROOM ID", serverRoom.getDeviceId(), insertedServerRooms.get(0).getDeviceId());
        assertEquals("SERVER ROOM LABEL", serverRoom.getDeviceLabel(), insertedServerRooms.get(0).getDeviceLabel());
    }

    @Test
    public void testCoolingSystems() {
        List<CoolingSystem> cs = deviceService.getCoolingSystem(DATACENTER_POZNAN, LocalDateTime.of(2019, 9, 9, 3, 0, 0));
        assert cs.size() == 1;
        CoolingSystem coolingSystem = cs.get(0);

        assertEquals("MAX COOLING", Math.abs(coolingSystem.getMaxCoolingLoadKWh() - 400) < EPSILON, true);
        assertEquals("ENERGY CONSUMPTION", Math.abs(coolingSystem.getEnergyConsumption() - 378.731) < EPSILON, true);
        assertEquals("COP C", Math.abs(coolingSystem.getCopC() - 3.3) < EPSILON, true);
        assertEquals("COP H", Math.abs(coolingSystem.getCopH() - 2.3) < EPSILON, true);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testCoolingSystemInsertWrongParameter0() {
        CoolingSystem coolingSystem = new CoolingSystem(null, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertCoolingSystem(coolingSystem, "testCoolingSystem");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testCoolingSystemInsertWrongParameter1() {
        CoolingSystem coolingSystem = new CoolingSystem("", 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertCoolingSystem(coolingSystem, "testCoolingSystem");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testCoolingSystemInsertWrongParameter3() {
        CoolingSystem coolingSystem = new CoolingSystem("testCoolingSystem", 1.0, -1.0, 1.0, 1.0, 1.0);

        deviceService.insertCoolingSystem(coolingSystem, "testCoolingSystem");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testCoolingSystemInsertWrongParameter4() {
        CoolingSystem coolingSystem = new CoolingSystem("testCoolingSystem", 1.0, 1.0, -1.0, 1.0, 1.0);

        deviceService.insertCoolingSystem(coolingSystem, "testCoolingSystem");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testCoolingSystemInsertWrongParameter5() {
        CoolingSystem coolingSystem = new CoolingSystem("testCoolingSystem", 1.0, 1.0, 1.0, -1.0, 1.0);

        deviceService.insertCoolingSystem(coolingSystem, "testCoolingSystem");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testCoolingSystemInsertDuplicateLabel() {
        CoolingSystem coolingSystem = new CoolingSystem("testCoolingSystemDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0);
        CoolingSystem coolingSystem2 = new CoolingSystem("testCoolingSystemDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertCoolingSystem(coolingSystem, "testCoolingSystemDuplicate");
        deviceService.insertCoolingSystem(coolingSystem2, "testCoolingSystemDuplicate");
    }

    @Test
    public void testCoolingSystemInsertCorrect() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        CoolingSystem coolingSystem = new CoolingSystem("testCoolingSystem", 1.0, 1.0, 1.0, 1.0, 1.0);

        List<CoolingSystem> insertedCoolingSystems = deviceService.insertCoolingSystem(coolingSystem, "DC_NEW");

        assertEquals("COOLING SYSTEM ID", coolingSystem.getDeviceId(), insertedCoolingSystems.get(0).getDeviceId());
        assertEquals("COOLING SYSTEM LABEL", coolingSystem.getDeviceLabel(), insertedCoolingSystems.get(0).getDeviceLabel());
    }

    @Test
    public void testBatteries() {
        List<Battery> cs = deviceService.getBatteries(DATACENTER_POZNAN, LocalDateTime.of(2019, 9, 9, 3, 0, 0));
        assert cs.size() == 1;
        Battery battery = cs.get(0);

        assertEquals("ESD FACTOR", Math.abs(battery.getEsdFactor() - 1) < EPSILON, true);
        assertEquals("ACTUAL LOADED CAPACITY", Math.abs(battery.getActualLoadedCapacity() - 100) < EPSILON, true);
        assertEquals("MAX DISCHARGE RATE", Math.abs(battery.getMaxDischargeRate() - 300) < EPSILON, true);
        assertEquals("CHARGE LOSS RATE", Math.abs(battery.getChargeLossRate() - 1) < EPSILON, true);
        assertEquals("DISCHARGE LOSS RATE", Math.abs(battery.getDischargeLossRate() - 1) < EPSILON, true);
        assertEquals("MAXIMUM CAPACITY", Math.abs(battery.getMaximumCapacity() - 300) < EPSILON, true);
        assertEquals("DOD", Math.abs(battery.getDod() - 0.4) < EPSILON, true);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter0() {
        Battery battery = new Battery(null, null, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter1() {
        Battery battery = new Battery(null, "", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter2() {
        Battery battery = new Battery(null, "testBattery", -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter3() {
        Battery battery = new Battery(null, "testBattery", 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0,1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter4() {
        Battery battery = new Battery(null, "testBattery", 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter5() {
        Battery battery = new Battery(null, "testBattery", 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter6() {
        Battery battery = new Battery(null, "testBattery", 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter7() {
        Battery battery = new Battery(null, "testBattery", 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertWrongParameter8() {
        Battery battery = new Battery(null, "testBattery", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0);

        deviceService.insertBattery(battery, "testBattery");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteriesInsertDuplicateLabel() {
        Battery battery = new Battery(null, "testBatteryDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
        Battery battery2 = new Battery(null, "testBatteryDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertBattery(battery, "testBatteryDuplicate");
        deviceService.insertBattery(battery2, "testBatteryDuplicate");
    }

    @Test
    public void testBatteriesInsertCorrect() {
        Battery battery = new Battery(null, "testBattery", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        List<Battery> insertedBatteries = deviceService.insertBattery(battery, "DC_NEW");

        assertEquals("BATTERY ID", battery.getDeviceId(), insertedBatteries.get(0).getDeviceId());
        assertEquals("BATTERY LABEL", battery.getDeviceLabel(), insertedBatteries.get(0).getDeviceLabel());
    }

    @Test
    public void testTES() {
        List<ThermalEnergyStorage> cs = deviceService.getThermalEnergyStorages(DATACENTER_POZNAN, LocalDateTime.of(2019, 9, 9, 3, 0, 0));
        assert cs.size() == 1;
        ThermalEnergyStorage tes = cs.get(0);

        assertEquals("TES FACTOR", Math.abs(tes.getTesFactor() - 1) < EPSILON, true);
        assertEquals("ACTUAL LOADED CAPACITY", Math.abs(tes.getActualLoadedCapacity() - 0) < EPSILON, true);
        assertEquals("MAX DISCHARGE RATE", Math.abs(tes.getMaxDischargeRate() - 0) < EPSILON, true);
        assertEquals("MAX CHARGE RATE", Math.abs(tes.getMaxChargeRate() - 0) < EPSILON, true);
        assertEquals("ENERGY LOSS RATE", Math.abs(tes.getEnergyLossRate() - 1) < EPSILON, true);
        assertEquals("CHARGE LOSS RATE", Math.abs(tes.getChargeLossRate() - 1) < EPSILON, true);
        assertEquals("DISCHARGE LOSS RATE", Math.abs(tes.getDischargeLossRate() - 1) < EPSILON, true);
        assertEquals("MAXIMUM CAPACITY", Math.abs(tes.getMaximumCapacity() - 0) < EPSILON, true);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter0() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, null,1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter1() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter2() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTes", -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter3() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTes", 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter4() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTes", 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter5() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTes", 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter6() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTes", 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertWrongParameter7() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTes", 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTes");
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTesInsertDuplicateLabel() {
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTesDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
        ThermalEnergyStorage tes2 = new ThermalEnergyStorage(null, "testTesDuplicate", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        deviceService.insertThermalEnergyStorage(tes, "testTesDuplicate");
        deviceService.insertThermalEnergyStorage(tes2, "testTesDuplicate");
    }

    @Test
    public void testTesInsertCorrect() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        ThermalEnergyStorage tes = new ThermalEnergyStorage(null, "testTes", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        List<ThermalEnergyStorage> insertedTes = deviceService.insertThermalEnergyStorage(tes, "DC_NEW");

        assertEquals("TES ID", tes.getDeviceId(), insertedTes.get(0).getDeviceId());
        assertEquals("TES LABEL", tes.getDeviceLabel(), insertedTes.get(0).getDeviceLabel());
    }


    @Test
    public void testDC() {
        DataCentre dc = deviceService.getDatacenterProperties(DATACENTER_POZNAN, LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        assertEquals("CURRENT CONSUMPTION", Math.abs(dc.getEnergyConsumption() - (734.729 + 378.731)) < EPSILON, true);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testDCInsertWrongParameter0() {
        DataCentre dc = new DataCentre();
        dc.setLabel(null);

        deviceService.insertDatacentre(dc);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testDCInsertWrongParameter1() {
        DataCentre dc = new DataCentre();
        dc.setLabel("");

        deviceService.insertDatacentre(dc);
    }


    @Test(expected = IncorrectParameterException.class)
    public void testDCInsertDuplicateLabel() {
        DataCentre dc = new DataCentre();
        dc.setLabel("testDCDuplicate");
        dc.setFloorArea(1);
        dc.setDesignITLoadDensityWm2(1);
        dc.setActions(Collections.singletonList(new EnergyConsumptionOptimization()));
        dc.setComponents(Collections.singletonList(new Component()));

        DataCentre dc2 = new DataCentre();
        dc2.setLabel("testDCDuplicate");
        dc2.setFloorArea(1);
        dc2.setDesignITLoadDensityWm2(1);
        dc.setActions(Collections.singletonList(new EnergyConsumptionOptimization()));
        dc.setComponents(Collections.singletonList(new Component()));

        deviceService.insertDatacentre(dc);
        deviceService.insertDatacentre(dc2);
    }

    @Test
    public void testDCInsertCorrect() {
        DataCentre dc = new DataCentre();
        dc.setLabel("testDC");
        dc.setFloorArea(1);
        dc.setDesignITLoadDensityWm2(1);
        dc.setActions(Collections.singletonList(new EnergyConsumptionOptimization()));
        dc.setComponents(Collections.singletonList(new Component()));

        List<DataCentre> insertedDCs = deviceService.insertDatacentre(dc);
        assert insertedDCs.size() == 2;
        UUID insertedDC = deviceService.getDeviceIdFromLabel(dc.getLabel());
        assertEquals("DC ID", dc.getIdDataCentre(),insertedDC);
    }

    @Test
    public void testDelete(){
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentId = deviceService.insert(parent);
        DeviceDTO dto1 = new DeviceDTO(UUID.randomUUID(), "SERVER_NEW", UUID.fromString(UUID_SERVER_DEVICE_TYPE), parentId);
        UUID deviceId = deviceService.insert(dto1);

        MonitoredValueDTO mvDto = new MonitoredValueDTO(UUID.randomUUID(), LocalDateTime.of(2019, 9, 9, 3, 0, 0), 11, deviceId, UUID.fromString(UUID_MAX_IT_MEASUREMENT));
        mvService.insert(mvDto);
        MonitoredValueDTO mv2Dto = new MonitoredValueDTO(UUID.randomUUID(), LocalDateTime.of(2019, 9, 9, 3, 0, 0), 0.73, deviceId, UUID.fromString(UUID_E_D_Percentage));
        mvService.insert(mv2Dto);

        List<ServerRoom> rooms = deviceService.getServerRoom(parent.getLabel(), LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        assert rooms.size() == 1;
        ServerRoom room = rooms.get(0);

        assertEquals("MAX ENERGY", Math.abs(room.getMaxEnergyConsumption() - 11) < EPSILON, true);
        assertEquals("ENERGY CONSUMPTION", Math.abs(room.getEnergyConsumption() - 0) < EPSILON, true);
        assertEquals("E_D_Percentage ", Math.abs(room.getEdPercentage() - 0.73) < EPSILON, true);


        assertEquals("DELETE DEVICE 1",  deviceService.deleteDevice(dto1.getLabel()), true);

        rooms = deviceService.getServerRoom(parent.getLabel(), LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        assert rooms.size() == 0;
   }

    @Test(expected = ResourceNotFoundException.class)
    public void testDelete2() {
        DeviceDTO dto1 = new DeviceDTO(UUID.randomUUID(), "SERVER_NEW", UUID.fromString(UUID_SERVER_DEVICE_TYPE), UUID.fromString(UUID_DC_POZNAN));
        UUID deviceId = deviceService.insert(dto1);
        assertEquals("DELETE DEVICE 1",  deviceService.deleteDevice(dto1.getLabel()), true);
        dto1 = deviceService.findById(deviceId);
    }



    public void testDelete4() {
        DeviceDTO dto1 = new DeviceDTO(UUID.randomUUID(), "SERVER_NEW", UUID.fromString(UUID_SERVER_DEVICE_TYPE), UUID.fromString(UUID_DC_POZNAN));
        UUID deviceId = deviceService.insert(dto1);
        assertEquals("DELETE DEVICE 1",  deviceService.deleteDevice(dto1.getLabel()), true);
        assertEquals("FIN BY LABEL 1",  deviceService.getDeviceIdFromLabel(dto1.getLabel()), null);
    }


    @Test
    public void testDelete3() {
        DeviceDTO dto1 = new DeviceDTO(UUID.randomUUID(), "SERVER_NEW", UUID.fromString(UUID_SERVER_DEVICE_TYPE), UUID.fromString(UUID_DC_POZNAN));
        UUID deviceId = deviceService.insert(dto1);
        List<DeviceDTO> devicesPre = deviceService.findAll();
        assertEquals("DELETE DEVICE 1",  deviceService.deleteDevice(dto1.getLabel()), true);
        List<DeviceDTO> devicesPost = deviceService.findAll();
        assert devicesPost.size() == devicesPre.size() -1;

    }

    @Test
    public void testServerRoomsUpdateCorrect() {
        ServerRoom serverRoom = new ServerRoom("testServerRoom", 1.0, 1.0, 1.0, 1.0, 1.0);
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        List<ServerRoom> insertedServerRooms = deviceService.insertServerRoom(serverRoom, parent.getLabel());

        assert insertedServerRooms.size() == 1;
        ServerRoom sr = insertedServerRooms.get(0);
        assertEquals("SERVER ROOM", serverRoom, sr);

        sr.setDeviceLabel("updatedSR");
        serverRoom.setDeviceLabel("updatedSR");

        insertedServerRooms = deviceService.updateServerRoom(sr, parent.getLabel());

        assert insertedServerRooms.size() == 1;
        sr = insertedServerRooms.get(0);
        assertEquals("SERVER ROOM", serverRoom, sr);

        sr.setEdPercentage(0.3);
        serverRoom.setEdPercentage(0.3);

        insertedServerRooms = deviceService.updateServerRoom(sr, parent.getLabel());

        assert insertedServerRooms.size() == 1;
        sr = insertedServerRooms.get(0);
        assertEquals("SERVER ROOM", serverRoom, sr);
    }

    @Test
    public void testServerRoomsUpdateCorrectCheckFields() {
        ServerRoom serverRoom = new ServerRoom("testServerRoom", 1.0, 2.0, 0.3, 3.0, 4.0);
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        List<ServerRoom> insertedServerRooms = deviceService.insertServerRoom(serverRoom, parent.getLabel());

        assert insertedServerRooms.size() == 1;
        ServerRoom sr = insertedServerRooms.get(0);
        assertEquals("SERVER ROOM", serverRoom, sr);

        sr.setDeviceLabel("updatedSR");
        sr.setpMax(sr.getpMax() + 1);
        sr.setEdPercentage( sr.getEdPercentage() + 0.1);
        sr.setMaxHostLoad( sr.getMaxHostLoad() +1);
        sr.setMaxReallocLoad( sr.getMaxReallocLoad() +1);
        sr.setEnergyConsumption( sr.getEnergyConsumption() +1);
        sr.setMaxEnergyConsumption( sr.getMaxEnergyConsumption() +1);


        List<ServerRoom> updatedServerRooms = deviceService.updateServerRoom(sr, parent.getLabel());
        assert updatedServerRooms.size() == 1;
        ServerRoom  usr = updatedServerRooms.get(0);
        assertEquals("SERVER ROOM", sr, usr);
        
    }


    @Test(expected = IncorrectParameterException.class)
    public void testServerRoomsUpdateWithDuplicateName() {
        ServerRoom serverRoom1 = new ServerRoom("testServerRoom1", 1.0, 1.0, 1.0, 1.0, 1.0);
        ServerRoom serverRoom2 = new ServerRoom("testServerRoom2", 1.0, 1.0, 1.0, 1.0, 1.0);
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);

        List<ServerRoom> insertedServerRooms = deviceService.insertServerRoom(serverRoom1, parent.getLabel());
        assert insertedServerRooms.size() == 1;
        ServerRoom sr = insertedServerRooms.get(0);
        insertedServerRooms = deviceService.insertServerRoom(serverRoom2, parent.getLabel());
        assert insertedServerRooms.size() == 2;

        sr.setDeviceLabel("testServerRoom2");
        deviceService.updateServerRoom(sr, parent.getLabel());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testServerRoomsUpdateWithIncorrectID() {
        ServerRoom serverRoom1 = new ServerRoom("testServerRoom1", 1.0, 2.0, 0.3, 4.0, 5.0);
       DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);

        List<ServerRoom> insertedServerRooms = deviceService.insertServerRoom(serverRoom1, parent.getLabel());
        assert insertedServerRooms.size() == 1;
        ServerRoom sr = insertedServerRooms.get(0);
        sr.setDeviceId(UUID.randomUUID());
        deviceService.updateServerRoom(sr, parent.getLabel());
    }

    @Test
    public void testCoolingSystemUpdateCorrect() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        CoolingSystem refrenceCS = new CoolingSystem("testCoolingSystem", 1.0, 2.0, 3.0, 4.0, 5.0);

        List<CoolingSystem> insertedCoolingSystems = deviceService.insertCoolingSystem(refrenceCS, "DC_NEW");

        assert insertedCoolingSystems.size() == 1;

        CoolingSystem cs = insertedCoolingSystems.get(0);

        assertEquals("COOLING SYSTEM", refrenceCS, cs);

        cs.setDeviceLabel("updated cs");
        refrenceCS.setDeviceLabel("updated cs");
        insertedCoolingSystems = deviceService.updateCoolingSystem(cs, parent.getLabel());
        assert insertedCoolingSystems.size() == 1;
        cs = insertedCoolingSystems.get(0);
        assertEquals("COOLING SYSTEM", refrenceCS, cs);

        cs.setMaxCoolingLoadKWh(300.0);
        refrenceCS.setMaxCoolingLoadKWh(300.0);
        cs.setDeviceLabel("updated cs2");
        refrenceCS.setDeviceLabel("updated cs2");
        insertedCoolingSystems = deviceService.updateCoolingSystem(cs, parent.getLabel());
        assert insertedCoolingSystems.size() == 1;
        cs = insertedCoolingSystems.get(0);
        assertEquals("COOLING SYSTEM", refrenceCS, cs);

    }

    @Test
    public void testCoolingSystemUpdateCorrectCheckFields() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        CoolingSystem coolingSystem = new CoolingSystem("testCoolingSystem", 1.0, 2.0, 3.0, 4.0, 5.0);

        List<CoolingSystem> insertedCoolingSystems = deviceService.insertCoolingSystem(coolingSystem, "DC_NEW");
        assert insertedCoolingSystems.size() == 1;
        CoolingSystem cs = insertedCoolingSystems.get(0);
        assertEquals("COOLING SYSTEM INSERTED", coolingSystem, cs);

        cs.setDeviceLabel("updated cs");
        cs.setCoolingCapacityKWh(coolingSystem.getCoolingCapacityKWh()+1.0);
        cs.setMaxCoolingLoadKWh(coolingSystem.getMaxCoolingLoadKWh() + 1.0);
        cs.setCopC(coolingSystem.getCopC() +1.0);
        cs.setCopH(coolingSystem.getCopH() + 1.0);
        List<CoolingSystem> updatesCoolingSystem = deviceService.updateCoolingSystem(cs, parent.getLabel());
        assert updatesCoolingSystem.size() == 1;
        CoolingSystem ucs = updatesCoolingSystem.get(0);
        assertEquals("COOLING SYSTEM UPDATED", ucs, cs);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testCSUpdateWithDuplicateName() {
        CoolingSystem coolingSystem1 = new CoolingSystem("testCoolingSystem1", 1.0, 1.0, 1.0, 1.0, 1.0);
        CoolingSystem coolingSystem2 = new CoolingSystem("testCoolingSystem2", 1.0, 1.0, 1.0, 1.0, 1.0);
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);

        List<CoolingSystem> insertedCS = deviceService.insertCoolingSystem(coolingSystem1, parent.getLabel());
        assert insertedCS.size() == 1;
        CoolingSystem sr = insertedCS.get(0);
        insertedCS = deviceService.insertCoolingSystem(coolingSystem2, parent.getLabel());
        assert insertedCS.size() == 2;

        sr.setDeviceLabel("testCoolingSystem2");
        deviceService.updateCoolingSystem(sr, parent.getLabel());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testCSUpdateWithIncorrectID() {
        CoolingSystem coolingSystem1 = new CoolingSystem("testCoolingSystem1", 1.0, 1.0, 1.0, 1.0, 1.0);
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);

        List<CoolingSystem> insertedCS = deviceService.insertCoolingSystem(coolingSystem1, parent.getLabel());
        assert insertedCS.size() == 1;
        CoolingSystem sr = insertedCS.get(0);
        sr.setDeviceId(UUID.randomUUID());
        deviceService.updateCoolingSystem(sr, parent.getLabel());
    }

    @Test
    public void testBatteryUpdateCorrect() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        Battery battery = new Battery("testBattery", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        List<Battery> insertedBattery = deviceService.insertBattery(battery, "DC_NEW");

        assert insertedBattery.size() == 1;

        Battery cs = insertedBattery.get(0);

        assertEquals("BATTERY", battery, cs);

        cs.setDeviceLabel("updated cs");
        battery.setDeviceLabel("updated cs");
        insertedBattery = deviceService.updateBattery(cs, parent.getLabel());
        assert insertedBattery.size() == 1;
        cs = insertedBattery.get(0);
        assertEquals("BATTERY", battery, cs);

        cs.setDod(300.0);
        battery.setDod(300.0);
        cs.setDeviceLabel("updated cs2");
        battery.setDeviceLabel("updated cs2");
        insertedBattery = deviceService.updateBattery(cs, parent.getLabel());
        assert insertedBattery.size() == 1;
        cs = insertedBattery.get(0);
        assertEquals("BATTERY", battery, cs);

    }

    @Test
    public void testBatteryUpdateCorrectCheckFields() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        Battery refrenceESD = new Battery("testBattery", 1.0, 0.2, 3.0, 4.0, 0.5, 0.6, 7.0, 8.0);

        List<Battery> insertedBattery = deviceService.insertBattery(refrenceESD,  parent.getLabel());

        assert insertedBattery.size() == 1;

        Battery cs = insertedBattery.get(0);

        assertEquals("BATTERY", refrenceESD, cs);

        cs.setDeviceLabel("updated cs");
        cs.setDod(refrenceESD.getDod() +1);
        cs.setMaximumCapacity( refrenceESD.getMaximumCapacity() +1);
        cs.setEsdFactor(refrenceESD.getEsdFactor() +0.1);
        cs.setDischargeLossRate(refrenceESD.getDischargeLossRate() +0.1);
        cs.setChargeLossRate(refrenceESD.getChargeLossRate() +0.1);
        cs.setMaxChargeRate(refrenceESD.getMaxChargeRate() +1);
        cs.setMaxDischargeRate(refrenceESD.getMaxDischargeRate()+1);
        List<Battery> updatedBattery  = deviceService.updateBattery(cs, parent.getLabel());
        assert updatedBattery.size() == 1;
        Battery ucs = updatedBattery.get(0);
        assertEquals("BATTERY", ucs, cs);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testBatteryUpdateWithDuplicateName() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        Battery battery1 = new Battery("testBattery1", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
        Battery battery2 = new Battery("testBattery2", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        List<Battery> insertedBattery = deviceService.insertBattery(battery1, "DC_NEW");

        assert insertedBattery.size() == 1;
        Battery sr = insertedBattery.get(0);
        insertedBattery = deviceService.insertBattery(battery2, parent.getLabel());
        assert insertedBattery.size() == 2;

        sr.setDeviceLabel("testBattery2");
        deviceService.updateBattery(sr, parent.getLabel());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testBatteryUpdateWithIncorrectID() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        Battery battery = new Battery("testBattery", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        List<Battery> insertedBattery = deviceService.insertBattery(battery, "DC_NEW");

        assert insertedBattery.size() == 1;
        Battery sr = insertedBattery.get(0);
        sr.setDeviceId(UUID.randomUUID());
        deviceService.updateBattery(sr, parent.getLabel());
    }

    @Test
    public void testTESUpdateCorrect() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        ThermalEnergyStorage tes = new ThermalEnergyStorage("testTES", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        List<ThermalEnergyStorage> insertedTESs = deviceService.insertThermalEnergyStorage(tes, "DC_NEW");

        assert insertedTESs.size() == 1;

        ThermalEnergyStorage cs = insertedTESs.get(0);

        assertEquals("TES", tes, cs);

        cs.setDeviceLabel("updated cs");
        insertedTESs = deviceService.updateThermalEnergyStorage(cs, parent.getLabel());
        assert insertedTESs.size() == 1;
        cs = insertedTESs.get(0);
        assertEquals("TES", tes, cs);

        cs.setTesFactor(0.3);
        tes.setTesFactor(0.3);
        cs.setDeviceLabel("updated cs2");
        insertedTESs = deviceService.updateThermalEnergyStorage(cs, parent.getLabel());
        assert insertedTESs.size() == 1;
        cs = insertedTESs.get(0);
        assertEquals("TES", tes, cs);

    }

    @Test
    public void testTESUpdateCorrectCheckFields() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        ThermalEnergyStorage tes = new ThermalEnergyStorage("testTES", 1.0, 0.2, 3.0, 4.0, 0.5, 0.6, 7.0);

        List<ThermalEnergyStorage> insertedTESs = deviceService.insertThermalEnergyStorage(tes, parent.getLabel());

        assert insertedTESs.size() == 1;

        ThermalEnergyStorage cs = insertedTESs.get(0);

        assertEquals("TES", tes, cs);

        cs.setDeviceLabel("updated cs");
        cs.setTesFactor(tes.getTesFactor() + 0.1);
        cs.setMaxChargeRate(tes.getMaxChargeRate() +1.0);
        cs.setMaxDischargeRate(tes.getMaxDischargeRate() + 1.0);
        cs.setChargeLossRate(tes.getChargeLossRate() + 0.1);
        cs.setDischargeLossRate(tes.getDischargeLossRate() + 0.1);
        cs.setMaximumCapacity(tes.getMaximumCapacity() + 1.0);
        List<ThermalEnergyStorage> updatedTESs = deviceService.updateThermalEnergyStorage(cs, parent.getLabel());
        assert updatedTESs.size() == 1;
        ThermalEnergyStorage ucs = updatedTESs.get(0);
        assertEquals("TES", ucs, cs);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testTESUpdateWithDuplicateName() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        ThermalEnergyStorage tes1 = new ThermalEnergyStorage("testTES1", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
        ThermalEnergyStorage tes2 = new ThermalEnergyStorage("testTES2", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        List<ThermalEnergyStorage> insertedTESs = deviceService.insertThermalEnergyStorage(tes1, "DC_NEW");

        assert insertedTESs.size() == 1;
        ThermalEnergyStorage sr = insertedTESs.get(0);
        insertedTESs = deviceService.insertThermalEnergyStorage(tes2, parent.getLabel());
        assert insertedTESs.size() == 2;

        sr.setDeviceLabel("testTES2");
        deviceService.updateThermalEnergyStorage(sr, parent.getLabel());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testTESUpdateWithIncorrectID() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        ThermalEnergyStorage tes1 = new ThermalEnergyStorage("testTES1", 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);

        List<ThermalEnergyStorage> insertedTESs = deviceService.insertThermalEnergyStorage(tes1, "DC_NEW");

        assert insertedTESs.size() == 1;
        ThermalEnergyStorage sr = insertedTESs.get(0);
        sr.setDeviceId(UUID.randomUUID());
        deviceService.updateThermalEnergyStorage(sr, parent.getLabel());
    }

    @Test
    public void testHRIUpdateCorrect() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        HeatRecoveryInfrastructure refrenceHri = new HeatRecoveryInfrastructure("testTES", 1.0, 2.0, 3.0, 4.0);

        List<HeatRecoveryInfrastructure> insertedHRIs = deviceService.insertHeatRecoverySystems(refrenceHri, "DC_NEW");

        assert insertedHRIs.size() == 1;

        HeatRecoveryInfrastructure cs = insertedHRIs.get(0);

        assertEquals("HRI", refrenceHri, cs);

        cs.setDeviceLabel("updated cs");
        refrenceHri.setDeviceLabel("updated cs");
        insertedHRIs = deviceService.updateHeatRecoverySystems(cs, parent.getLabel());
        assert insertedHRIs.size() == 1;
        cs = insertedHRIs.get(0);
        assertEquals("HRI", refrenceHri, cs);

        cs.setCopH(0.3);
        refrenceHri.setCopH(0.3);
        cs.setDeviceLabel("updated cs2");
        refrenceHri.setDeviceLabel("updated cs2");
        insertedHRIs = deviceService.updateHeatRecoverySystems(cs, parent.getLabel());
        assert insertedHRIs.size() == 1;
        cs = insertedHRIs.get(0);
        assertEquals("HRI", refrenceHri, cs);

    }

    @Test
    public void testHRIUpdateCorrectCheckFields() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        HeatRecoveryInfrastructure hri = new HeatRecoveryInfrastructure("testTES", 1.0, 1.0, 1.0, 1.0);

        List<HeatRecoveryInfrastructure> insertedHRIs = deviceService.insertHeatRecoverySystems(hri, "DC_NEW");

        assert insertedHRIs.size() == 1;

        HeatRecoveryInfrastructure cs = insertedHRIs.get(0);

        assertEquals("HRI", hri, cs);

        cs.setDeviceLabel("updated cs");
        cs.setCopH(hri.getCopH() +1.0);
        cs.setHeatCapacityKWh(hri.getHeatCapacityKWh() + 1.0);
        cs.setMaxHeatLoadKWh(hri.getMaxHeatLoadKWh() +1.0);
        List<HeatRecoveryInfrastructure> updatedHRIs= deviceService.updateHeatRecoverySystems(cs, parent.getLabel());
        assert updatedHRIs.size() == 1;
        HeatRecoveryInfrastructure ucs = updatedHRIs.get(0);
        assertEquals("HRI", ucs, cs);
    }

    @Test(expected = IncorrectParameterException.class)
    public void testHRIUpdateWithDuplicateName() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        HeatRecoveryInfrastructure hri1 = new HeatRecoveryInfrastructure("testHRI1", 1.0, 1.0, 1.0, 1.0);
        HeatRecoveryInfrastructure hri2 = new HeatRecoveryInfrastructure("testHRI2", 1.0, 1.0, 1.0, 1.0);

        List<HeatRecoveryInfrastructure> insertedHRIs = deviceService.insertHeatRecoverySystems(hri1, "DC_NEW");

        assert insertedHRIs.size() == 1;
        HeatRecoveryInfrastructure sr = insertedHRIs.get(0);
        insertedHRIs = deviceService.insertHeatRecoverySystems(hri2, parent.getLabel());
        assert insertedHRIs.size() == 2;

        sr.setDeviceLabel("testHRI2");
        deviceService.updateHeatRecoverySystems(sr, parent.getLabel());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testHRIUpdateWithIncorrectID() {
        DeviceDTO parent = new DeviceDTO(UUID.randomUUID(), "DC_NEW", UUID.fromString(UUID_DC_TYPE), null);
        UUID parentID = deviceService.insert(parent);
        HeatRecoveryInfrastructure hri1 = new HeatRecoveryInfrastructure("testHRI2", 1.0, 1.0, 1.0, 1.0);

        List<HeatRecoveryInfrastructure> insertedHRIs = deviceService.insertHeatRecoverySystems(hri1, "DC_NEW");

        assert insertedHRIs.size() == 1;
        HeatRecoveryInfrastructure sr = insertedHRIs.get(0);
        sr.setDeviceId(UUID.randomUUID());
        deviceService.updateHeatRecoverySystems(sr, parent.getLabel());
    }

    @Test
    public void testDCUpdateCorrect() {

        DataCentre dc = new DataCentre("DC_NEW", 0.0); //TODO energy consumption is 0 when returned from db

        List<DataCentre> insertedDCs = deviceService.insertDatacentre(dc);

        assert insertedDCs.size() == 2;

        DataCentre cs = new DataCentre();
        for(DataCentre insertDC : insertedDCs){
            if(insertDC.getLabel().equals(dc.getLabel())){
                cs = insertDC;
            }
        }
        assertEquals("DC", dc, cs);

        cs.setLabel("updated cs");
        dc.setLabel("updated cs");
        insertedDCs = deviceService.updateDatacentre(cs);
        assert insertedDCs.size() == 2;
        for(DataCentre insertDC : insertedDCs){
            if(insertDC.getLabel().equals(dc.getLabel())){
                cs = insertDC;
            }
        }
        assertEquals("DC", dc, cs);

    }

    @Test(expected = IncorrectParameterException.class)
    public void testDCUpdateWithDuplicateName() {
        DataCentre dc1 = new DataCentre("DC_NEW1", 1.0);
        DataCentre dc2 = new DataCentre("DC_NEW2", 1.0);

        List<DataCentre> insertedDCs = deviceService.insertDatacentre(dc1);

        assert insertedDCs.size() == 2;
        DataCentre cs = new DataCentre();
        for(DataCentre insertDC : insertedDCs){
            if(insertDC.getLabel().equals(dc1.getLabel())){
                cs = insertDC;
            }
        }
        insertedDCs = deviceService.insertDatacentre(dc2);
        assert insertedDCs.size() == 3;

        cs.setLabel("DC_NEW2");
        deviceService.updateDatacentre(cs);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDCUpdateWithIncorrectID() {
        DataCentre dc1 = new DataCentre("DC_NEW2", 1.0);

        List<DataCentre> insertedDCs = deviceService.insertDatacentre(dc1);


        assert insertedDCs.size() == 2;
        DataCentre cs = new DataCentre();
        for(DataCentre insertDC : insertedDCs){
            if(insertDC.getLabel().equals(dc1.getLabel())){
                cs = insertDC;
            }
        }
        cs.setIdDataCentre(UUID.randomUUID());
        deviceService.updateDatacentre(cs);
    }
}
