package ro.tuc.dsrl.catalyst.dbapi.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.dsrl.catalyst.dbapi.SpringBootTestConfig;
import ro.tuc.dsrl.catalyst.model.dto.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.TrainedModelDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.repository.PredictedValueRepository;
import ro.tuc.dsrl.dbapi.repository.PredictionJobRepository;
import ro.tuc.dsrl.dbapi.service.basic.MonitoredValueService;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.dbapi.service.basic.PredictionJobService;
import ro.tuc.dsrl.dbapi.service.basic.TrainedModelService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.*;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/insertForPredictionResourcesTests.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/deleteForPredictionResourcesTests.sql")
public class PredictionResourcesTests extends SpringBootTestConfig {

    private static final String GRANULARITY = "DAYAHEAD";
    private static final String ALGO_TYPE = "ENSEMBLE";
    private static final String FLEXIBILITY_TYPE = "NONE";
    private static final String MEASUREMENT_ID = "F4E77279-0171-32EA-9673-00155DB52D03";
    private static final String DEVICE_ID = "C382C39F-C2A5-C2BE-C38E-5011C3A9C2AD";
    private static final String PREDICTION_JOB_ID = "0214E280-9A70-C39F-194A-4FE2809CC3A6";
    private static final String PREDICTED_VALUE_ID = "56971199-0178-11EA-9673-00155DB0AD01";
    private static final String MONITORED_VALUE_ID = "56971199-0178-11EA-9673-00155DB0AD01";
    private static final String COMPONENT_TYPE = "SERVER_ROOM";
    private static final String PATH = "D:/poznan/trained_models/intraday/COOLING_SYSTEM";
    private static final String SCENARIO = "POZNAN";
    private static final String STATUS = "PENDING";
    private static final String PREDICTION_TYPE = "DAYAHEAD";
    private static final String ENERGY_TYPE = "ELECTRICAL";
    private static final String AGGREGATION_GRANULARITY = "HOUR";
    private static final String TRAINED_MODEL_ID = "0214E280-9A70-C39F-194A-4FE2809CC3A7";
    private static final String SERVER_ROOM_ID = "78C2A82C-C3AF-1C1B-4D3D-C2A87027C39F";
    private static final String SERVER_ROOM_DEVICE_ID = "C2A6325C-17C3-8E54-11C3-A9C2AD1C0242";
    private static final String COOLING_SYSTEM_ID = "C3A755C2-B615-C38E-5411-C3A9C2AD1C02";
    private static final String DATACENTER_ID = "C382C39F-C2A5-C2BE-C38E-5011C3A9C2AD";

    @Autowired
    PredictionJobService predictionJobService;

    @Autowired
    PredictedValueService predictedValueService;

    @Autowired
    MonitoredValueService monitoredValueService;

    @Autowired
    TrainedModelService trainedModelService;

    @Autowired
    PredictionJobRepository predictionJobRepository;

    @Autowired
    EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;

    @Autowired
    PredictedValueRepository predictedValueRepository;


    @PostConstruct
    public void init() {
        // Setting Spring Boot SetTimeZone
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void testInsertPredictionJobOk() {

        List<PredictionJobDTO> beforeInsert = predictionJobService.findAll();
        PredictionJobDTO predictionJob = new PredictionJobDTO(
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                GRANULARITY,
                ALGO_TYPE,
                FLEXIBILITY_TYPE);
        predictionJobService.insert(predictionJob);
        List<PredictionJobDTO> afterInsert = predictionJobService.findAll();
        //then
        assertEquals("PREDICTION JOBS SIZE BEFORE", beforeInsert.size(), 2);
        assertEquals("PREDICTION JOBS SIZE AFTER", afterInsert.size(), 3);

    }


    @Test(expected = IncorrectParameterException.class)
    public void testInsertPredictionJobWrongParams() {

        // MIX PARAMS TO FORCE TEST TO CRASH
        PredictionJobDTO predictionJob = new PredictionJobDTO(
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                ALGO_TYPE,
                FLEXIBILITY_TYPE,
                GRANULARITY);
        predictionJobService.insert(predictionJob);

    }

    @Test(expected = IncorrectParameterException.class)
    public void testInsertPredictionJobFail() {

        PredictionJobDTO predictionJob = new PredictionJobDTO(null, null, null, null);
        predictionJobService.insert(predictionJob);

    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindByIdPredictionJobNotFound() {

        predictionJobService.findById(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3AF"));
    }

    @Test()
    public void testFindByIdPredictionJobFound() {

        PredictionJobDTO expected = new PredictionJobDTO(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3A7"),
                LocalDateTime.of(2019, 9, 9, 0, 0, 0),
                "DAYAHEAD",
                "ENSEMBLE",
                "NONE");
        PredictionJobDTO actual = predictionJobService.findById(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3A7"));
        assertEquals("PREDICTION JOB OBJECT ACTUAL VS EXPECTED", expected.toString(), actual.toString());
    }

    @Test
    public void testInsertPredictedValueOk() {

        List<PredictedValueDTO> beforeInsert = predictedValueService.findAll();
        PredictedValueDTO predictedValue = new PredictedValueDTO(
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                300.0,
                UUID.fromString(MEASUREMENT_ID),
                UUID.fromString(DEVICE_ID),
                UUID.fromString(PREDICTION_JOB_ID));
        predictedValueService.insert(predictedValue);
        List<PredictedValueDTO> afterInsert = predictedValueService.findAll();
        //then
        assertEquals("PREDICTED VALUES SIZE BEFORE", beforeInsert.size(), 25);
        assertEquals("PREDICTED VALUES SIZE AFTER", afterInsert.size(), 26);
    }


    @Test(expected = IncorrectParameterException.class)
    public void testInsertPredictedValueFail() {

        PredictedValueDTO predictedValue = new PredictedValueDTO(null, 0.0, null, null, null);
        predictedValueService.insert(predictedValue);

    }

    @Test(expected = EntityValidationException.class)
    public void testInsertPredictedValueWrongParams() {

        // MIX PARAMS TO FORCE TEST TO CRASH
        PredictedValueDTO predictedValue = new PredictedValueDTO(LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                10.0,
                UUID.fromString(DEVICE_ID),
                UUID.fromString(PREDICTION_JOB_ID),
                UUID.fromString(MEASUREMENT_ID));
        predictedValueService.insert(predictedValue);

    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindByIdPredictedValueNotFound() {

        predictedValueService.findById(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3AF"));
    }

    @Test()
    public void testFindByIdPredictedValueFound() {

        PredictedValueDTO expected = new PredictedValueDTO(UUID.fromString(PREDICTED_VALUE_ID),
                LocalDateTime.of(2019, 9, 9, 0, 0, 0),
                300.0,
                UUID.fromString(MEASUREMENT_ID),
                UUID.fromString(DEVICE_ID),
                UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3A7"));
        PredictedValueDTO actual = predictedValueService.findById(UUID.fromString(PREDICTED_VALUE_ID));
        assertEquals("PREDICTED VALUE OBJECT ACTUAL VS EXPECTED", expected.toString(), actual.toString());
    }

    @Test(expected = EntityValidationException.class)
    public void testInsertMonitoredValueNoTOk() {

        List<MonitoredValueDTO> beforeInsert = monitoredValueService.findAll();
        MonitoredValueDTO monitoredValue = new MonitoredValueDTO(
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                400.0,
                UUID.fromString(DEVICE_ID),
                UUID.fromString(MEASUREMENT_ID));
        monitoredValueService.insert(monitoredValue);
        List<MonitoredValueDTO> afterInsert = monitoredValueService.findAll();
        //then
        assertEquals("MONITORED VALUES SIZE BEFORE", beforeInsert.size(), 1);
        assertEquals("MONITORED VALUES SIZE AFTER", afterInsert.size(), 2);

    }


    @Test
    public void testInsertMonitoredValueOk() {

        List<MonitoredValueDTO> beforeInsert = monitoredValueService.findAll();
        MonitoredValueDTO monitoredValue = new MonitoredValueDTO(
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                400.0,
                UUID.fromString(SERVER_ROOM_DEVICE_ID),
                UUID.fromString(MEASUREMENT_ID));
        monitoredValueService.insert(monitoredValue);
        List<MonitoredValueDTO> afterInsert = monitoredValueService.findAll();
        //then
        assertEquals("MONITORED VALUES SIZE BEFORE", beforeInsert.size(), 1);
        assertEquals("MONITORED VALUES SIZE AFTER", afterInsert.size(), 2);

    }

    @Test(expected = EntityValidationException.class)
    public void testInsertMonitoredValueWrongParams() {

        // MIX PARAMS TO FORCE TEST TO CRASH
        MonitoredValueDTO monitoredValue = new MonitoredValueDTO(LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                10.0,
                UUID.fromString(MEASUREMENT_ID),
                UUID.fromString(PREDICTION_JOB_ID));
        monitoredValueService.insert(monitoredValue);

    }

    @Test(expected = IncorrectParameterException.class)
    public void testInsertMonitoredValueFail() {

        MonitoredValueDTO monitoredValue = new MonitoredValueDTO(null, 0.0, null, null);
        monitoredValueService.insert(monitoredValue);

    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindByIdMonitoredValueNotFound() {

        monitoredValueService.findById(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3AF"));
    }


    @Test()
    public void testFindByIdMonitoredValueFound() {

        MonitoredValueDTO expected = new MonitoredValueDTO(UUID.fromString(MONITORED_VALUE_ID),
                LocalDateTime.of(2019, 9, 9, 0, 0, 0),
                400.0,
                UUID.fromString(DEVICE_ID),
                UUID.fromString(MEASUREMENT_ID));
        MonitoredValueDTO actual = monitoredValueService.findById(UUID.fromString(MONITORED_VALUE_ID));
        assertEquals("MONITORED VALUE OBJECT ACTUAL VS EXPECTED", expected, actual);
    }


    @Test
    public void insertTrainedModelOk() {

        List<TrainedModelDTO> beforeInsert = trainedModelService.findAll();
        TrainedModelDTO trainedModel = new TrainedModelDTO(SCENARIO,
                GRANULARITY,
                ALGO_TYPE,
                COMPONENT_TYPE,
                PATH,
                STATUS,
                LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        trainedModelService.insert(trainedModel);
        List<TrainedModelDTO> afterInsert = trainedModelService.findAll();
        //then
        assertEquals("TRAINED MODELS SIZE BEFORE", beforeInsert.size(), 1);
        assertEquals("TRAINED MODELS SIZE AFTER", afterInsert.size(), 2);

    }

    @Test(expected = IncorrectParameterException.class)
    public void insertTrainedModelFail() {

        TrainedModelDTO trainedModelDTO = new TrainedModelDTO(null, null, null, null, null, null, null);
        trainedModelService.insert(trainedModelDTO);

    }

    @Test
    public void insertPredictedEnergyCurveEmpty() {

        EnergyProfileDTO energyProfile = new EnergyProfileDTO(null, null, null, null, null, null, null);
        boolean actual = predictedValueService.insertPredictedEnergyCurve(energyProfile, null);

        assertEquals("INSERT PREDICTED ENERGY CURVE", false, actual);

    }

    @Test
    public void insertPredictedEnergyCurveOK() {

        EnergySampleDTO energySampleDTO = new EnergySampleDTO(300.0,
                LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        List<EnergySampleDTO> energySampleDTOList = new ArrayList<>();
        energySampleDTOList.add(energySampleDTO);

        EnergyProfileDTO energyProfile = new EnergyProfileDTO(energySampleDTOList,
                UUID.fromString(DEVICE_ID),
                UUID.fromString(MEASUREMENT_ID),
                AggregationGranularity.HOUR,
                PredictionGranularity.DAYAHEAD,
                PredictionType.ENERGY_CONSUMPTION,
                EnergyType.ELECTRICAL);

        boolean actual = predictedValueService.insertPredictedEnergyCurve(energyProfile, UUID.fromString(PREDICTION_JOB_ID));

        assertEquals("INSERT PREDICTED ENERGY CURVE", true, actual);

    }

    @Test(expected = EntityValidationException.class)
    public void insertPredictedEnergyCurveBadParams() {

        EnergySampleDTO energySampleDTO = new EnergySampleDTO(300.0,
                LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        List<EnergySampleDTO> energySampleDTOList = new ArrayList<>();
        energySampleDTOList.add(energySampleDTO);

        // mix params to force crash
        EnergyProfileDTO energyProfile = new EnergyProfileDTO(energySampleDTOList,
                UUID.fromString(MEASUREMENT_ID),
                UUID.fromString(DEVICE_ID),
                AggregationGranularity.HOUR,
                PredictionGranularity.DAYAHEAD,
                PredictionType.ENERGY_CONSUMPTION,
                EnergyType.ELECTRICAL);

        boolean actual = predictedValueService.insertPredictedEnergyCurve(energyProfile, UUID.fromString(MEASUREMENT_ID));

        assertEquals("INSERT PREDICTED ENERGY CURVE", false, actual);

    }

    @Test(expected = EntityValidationException.class)
    public void insertPredictedEnergyCurveBadParams1() {

        EnergySampleDTO energySampleDTO = new EnergySampleDTO(300.0,
                LocalDateTime.of(2019, 9, 9, 3, 0, 0));

        List<EnergySampleDTO> energySampleDTOList = new ArrayList<>();
        energySampleDTOList.add(energySampleDTO);

        // mix params to force crash
        EnergyProfileDTO energyProfile = new EnergyProfileDTO(energySampleDTOList,
                UUID.fromString("F4E77279-0171-32EA-9673-00155DB52D01"),
                UUID.fromString("C382C39F-C2A5-C2BE-C38E-5011C3A9C2AE"),
                AggregationGranularity.HOUR,
                PredictionGranularity.DAYAHEAD,
                PredictionType.ENERGY_CONSUMPTION,
                EnergyType.ELECTRICAL);

        boolean actual = predictedValueService.insertPredictedEnergyCurve(energyProfile, UUID.fromString(MEASUREMENT_ID));

        assertEquals("INSERT PREDICTED ENERGY CURVE", false, actual);

    }

    @Test()
    public void getStatusOnLatestTrainedModelEmpty() {

        TrainedModelDTO trainedModelDTO = trainedModelService.getStatusOnLatestTrainedModel(SCENARIO, GRANULARITY, ALGO_TYPE, COMPONENT_TYPE);
        assertEquals("STATUS ON TRAINED MODEL", trainedModelDTO.getStatus(), "PENDING");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getStatusOnLatestTrainedModelBadParams() {

        TrainedModelDTO trainedModelDTO = trainedModelService.getStatusOnLatestTrainedModel("SERVER", GRANULARITY, ALGO_TYPE, COMPONENT_TYPE);
        assertEquals("STATUS ON TRAINED MODEL", trainedModelDTO.getStatus(), "PENDING");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getStatusOnLatestTrainedModelOk() {

        TrainedModelDTO trainedModelDTO = trainedModelService.getStatusOnLatestTrainedModel(null, null, null, null);
    }


    @Test(expected = IncorrectParameterException.class)
    public void updateStatusOnTrainedModelEmpty() {

        TrainedModelDTO trainedModelDTO = new TrainedModelDTO(null, null, null, null, null, null, null);
        trainedModelService.updateStatus(trainedModelDTO);
    }

    @Test()
    public void updateStatusOnTrainedModelFail() {

        TrainedModelDTO trainedModelDTO = new TrainedModelDTO(UUID.fromString(DEVICE_ID), SCENARIO, PREDICTION_TYPE, ALGO_TYPE, COMPONENT_TYPE, PATH, STATUS,
                LocalDateTime.of(2019, 9, 9, 3, 0, 0));
        UUID actual = trainedModelService.updateStatus(trainedModelDTO);
        assertEquals("UPDATE STATUS", null, actual);
    }

    @Test()
    public void updateStatusOnTrainedModelOk() {

        TrainedModelDTO trainedModelDTO = new TrainedModelDTO(UUID.fromString(TRAINED_MODEL_ID), SCENARIO, PREDICTION_TYPE, ALGO_TYPE, COMPONENT_TYPE, PATH, STATUS,
                LocalDateTime.of(2019, 9, 9, 3, 0, 0));
        UUID actual = trainedModelService.updateStatus(trainedModelDTO);
        assertEquals("UPDATE STATUS ON TRAINED MODEL", trainedModelDTO.getId(), actual);
    }

    @Test(expected = IncorrectParameterException.class)
    public void getPredictedConsumptionForITComponentFail() {
        energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(null,
                null,
                null,
                null,
                null,
                null);
    }

    @Test()
    public void getPredictedConsumptionForITComponentNoValues() {
        List<EnergyProfileDTO> expected = new ArrayList<>();
        expected.add(EnergyProfileDTO.getDefaultInstance(LocalDateTime.of(2019, 9, 9, 3, 0, 0), PredictionGranularity.DAYAHEAD));
        expected.add(EnergyProfileDTO.getDefaultInstance(LocalDateTime.of(2019, 9, 9, 3, 0, 0), PredictionGranularity.DAYAHEAD));
        List<EnergyProfileDTO> actual = energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(UUID.fromString(SERVER_ROOM_ID),
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                PredictionGranularity.DAYAHEAD,
                ENERGY_TYPE,
                PREDICTION_TYPE,
                FLEXIBILITY_TYPE);
        assertEquals("ENERGY PROFILE FOR IT COMPONENT", expected, actual);
    }

    @Test(expected = IncorrectParameterException.class)
    public void getPredictedConsumptionForCoolingComponentFail() {
        energyHistoricalPredictedValuesService.getPredictedConsumptionForCoolingSystem(null,
                null,
                null,
                null,
                null,
                null);
    }

    @Test()
    public void getPredictedConsumptionForCoolingSystemNoValues() {
        EnergyProfileDTO expected = EnergyProfileDTO.getDefaultInstance(LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                PredictionGranularity.DAYAHEAD);
        EnergyProfileDTO actual = energyHistoricalPredictedValuesService.getPredictedConsumptionForCoolingSystem(UUID.fromString(COOLING_SYSTEM_ID),
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                PredictionGranularity.DAYAHEAD,
                ENERGY_TYPE,
                PREDICTION_TYPE,
                FLEXIBILITY_TYPE);
        assertEquals("ENERGY PROFILE FOR COOLING COMPONENT", expected, actual);
    }


    @Test(expected = IncorrectParameterException.class)
    public void getPredictedConsumptionForEntireDCComponentFail() {
        energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(null,
                null,
                null,
                null,
                null,
                null);
    }

    @Test()
    public void getPredictedConsumptionForEntireDcNoValues() {
        EnergyProfileDTO expected = EnergyProfileDTO.getDefaultInstance(LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                PredictionGranularity.DAYAHEAD);
        EnergyProfileDTO actual = energyHistoricalPredictedValuesService.getPredictedConsumptionForEntireDC(UUID.fromString(DATACENTER_ID),
                LocalDateTime.of(2019, 9, 9, 3, 0, 0),
                PredictionGranularity.DAYAHEAD,
                ENERGY_TYPE,
                PREDICTION_TYPE,
                FLEXIBILITY_TYPE);
        assertEquals("ENERGY PROFILE FOR COOLING COMPONENT", expected, actual);
    }

//    @Test()
//    public void getPredictedConsumptionForITComponentOk()
//    {
//        LocalDateTime startTime = LocalDateTime.of(2019, 9, 9, 0, 0,0);
//
//        List<EnergySampleDTO> energycurve = new ArrayList<>();
//        for(int i=0; i<24; i++){
//            energycurve.add(new EnergySampleDTO(300.0, startTime.plusHours(i)));
//        }
//
//        EnergyProfileDTO expected = new EnergyProfileDTO(energycurve,
//                UUID.fromString(DEVICE_ID),
//                UUID.fromString(MEASUREMENT_ID),
//                AggregationGranularity.HOUR,
//                PredictionGranularity.DAYAHEAD,
//                PredictionType.ENERGY_CONSUMPTION,
//                EnergyType.ELECTRICAL);
//
//        EnergyProfileDTO actual = energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(UUID.fromString(DATACENTER_ID),
//                LocalDateTime.of(2019, 9, 9, 0, 0,0),
//                PredictionGranularity.DAYAHEAD,
//                ENERGY_TYPE,
//                PREDICTION_TYPE,
//                FLEXIBILITY_TYPE);
//        assertEquals("ENERGY PROFILE FOR ENTIRE DC", expected, actual);
//    }

//    @Test()
//    public void getPredictedConsumptionForEntireDcOk()
//    {
//        LocalDateTime startTime = LocalDateTime.of(2019, 9, 9, 0, 0,0);
//
//        List<EnergySampleDTO> energycurve = new ArrayList<>();
//        for(int i=0; i<24; i++){
//            energycurve.add(new EnergySampleDTO(300.0, startTime.plusHours(i)));
//        }
//
//        EnergyProfileDTO expected = new EnergyProfileDTO(energycurve,
//                UUID.fromString(DEVICE_ID),
//                UUID.fromString(MEASUREMENT_ID),
//                AggregationGranularity.HOUR,
//                PredictionGranularity.DAYAHEAD,
//                PredictionType.ENERGY_CONSUMPTION,
//                EnergyType.ELECTRICAL);
//
//        EnergyProfileDTO actual = energyHistoricalPredictedValuesService.getPredictedConsumptionForEntireDC(UUID.fromString(DATACENTER_ID),
//                LocalDateTime.of(2019, 9, 9, 0, 0,0),
//                PredictionGranularity.DAYAHEAD,
//                ENERGY_TYPE,
//                PREDICTION_TYPE,
//                FLEXIBILITY_TYPE);
//        assertEquals("ENERGY PROFILE FOR ENTIRE DC", expected, actual);
//    }

    @Test(expected = IncorrectParameterException.class)
    public void getPredictedConsumptionForServerComponentFail() {
        energyHistoricalPredictedValuesService.getPredictedConsumptionForServer(null,
                null,
                null,
                null,
                null);
    }

    @Test()
    public void findAllPredictionJobs() {
        List<PredictionJobDTO> expected = new ArrayList<>();
        expected.add(new PredictionJobDTO(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3A6"),
                LocalDateTime.of(2019, 9, 9, 0, 0, 0),
                GRANULARITY,
                ALGO_TYPE,
                FLEXIBILITY_TYPE));
        expected.add(new PredictionJobDTO(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3A7"),
                LocalDateTime.of(2019, 9, 9, 0, 0, 0),
                GRANULARITY,
                ALGO_TYPE,
                FLEXIBILITY_TYPE));
        List<PredictionJobDTO> actual = predictionJobService.findAll();
        assertEquals("ALL PREDICTION JOBS", expected, actual);
    }

    @Test()
    public void findLastPredictionJobDatetime() {
        LocalDateTime expected = LocalDateTime.of(2019, 9, 9, 0, 0, 0);

        LocalDateTime actual = predictionJobRepository.findJobDateTime(
                GRANULARITY,
                ALGO_TYPE,
                DeviceTypeEnum.SERVER_ROOM.type(),
                FLEXIBILITY_TYPE,
                LocalDateTime.of(2019, 9, 9, 0, 0, 0));
        assertEquals("ALL PREDICTION JOBS", expected, actual);
    }

//    @Test
//    public void findDataInIntervalForComponent(){
//
//        LocalDateTime startTime = LocalDateTime.of(2019, 9, 9, 3, 0,0);
//        List<PredictedValue> expected = new ArrayList<>();
//        for(int i=0; i<24; i++){
//            expected.add(new PredictedValue(startTime.plusHours(i),
//                    300.0,
//                    new Measurement(UUID.fromString(MEASUREMENT_ID)),
//                    new Device(UUID.fromString(DEVICE_ID)),
//                    new PredictionJob(UUID.fromString("0214E280-9A70-C39F-194A-4FE2809CC3A6"))));
//        }
//
//        List<PredictedValue> actual = predictedValueRepository.findDataInIntervalForComponent(UUID.fromString(DATACENTER_ID),
//                ENERGY_TYPE,
//                COMPONENT_TYPE,
//                PREDICTION_TYPE,
//                ALGO_TYPE,
//                GRANULARITY,
//                startTime,
//                FLEXIBILITY_TYPE);
//
//        for(int i=0; i<24; i++){
//            assertEquals("PREDICTED VALUE EXPECTED VS ACTUAL", expected.get(i).getValue(), actual.get(i).getValue());
//        }
//}
}
