CREATE DATABASE  IF NOT EXISTS `catalyst-psm` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `catalyst-psm`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: catalyst-psm
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_instance`
--

DROP TABLE IF EXISTS `action_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action_instance` (
  `id` binary(16) NOT NULL,
  `end_time` datetime NOT NULL,
  `label` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `action_type_id` binary(16) NOT NULL,
  `optimization_plan_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKesbdvmhp6l5jm6kqrlpihbd0p` (`action_type_id`),
  KEY `FKfp2qia8vqeaaee55m0b8syhc` (`optimization_plan_id`),
  CONSTRAINT `FKesbdvmhp6l5jm6kqrlpihbd0p` FOREIGN KEY (`action_type_id`) REFERENCES `action_type` (`id`),
  CONSTRAINT `FKfp2qia8vqeaaee55m0b8syhc` FOREIGN KEY (`optimization_plan_id`) REFERENCES `optimization_plans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_instance`
--

LOCK TABLES `action_instance` WRITE;
/*!40000 ALTER TABLE `action_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_property`
--

DROP TABLE IF EXISTS `action_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action_property` (
  `id` binary(16) NOT NULL,
  `property` varchar(255) NOT NULL,
  `observation` varchar(255) NOT NULL,
  `measure_unit_id` binary(16) NOT NULL,
  `value_type_id` binary(16) NOT NULL,
  `action_type_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`,`measure_unit_id`,`value_type_id`,`action_type_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_action_property_measure_unit1_idx` (`measure_unit_id`),
  KEY `fk_action_property_value_type1_idx` (`value_type_id`),
  KEY `fk_action_property_action_type1_idx` (`action_type_id`),
  CONSTRAINT `fk_action_property_action_type1` FOREIGN KEY (`action_type_id`) REFERENCES `action_type` (`id`),
  CONSTRAINT `fk_action_property_measure_unit1` FOREIGN KEY (`measure_unit_id`) REFERENCES `measure_unit` (`id`),
  CONSTRAINT `fk_action_property_value_type1` FOREIGN KEY (`value_type_id`) REFERENCES `value_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_property`
--

LOCK TABLES `action_property` WRITE;
/*!40000 ALTER TABLE `action_property` DISABLE KEYS */;
INSERT INTO `action_property` VALUES (_binary 'è‹|0‚H\�','reallocation_percentage','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary 'pëbÜ@žÚø'),(_binary '#ܐ��M�\�\�\�\�AF','buy_heat','EMPTY',_binary 'ñ3ä’E~¶¬',_binary 'Yè>ðüJ¥¨^^',_binary '_�4�\�UK8���=~��'),(_binary '”;¸D6CÓ•\�','discharge_battery_amount','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary '6º.Ë]Fí¢W\�'),(_binary '\Z«1bFµ¦UFÏ','charge_battery_amount','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary '}:ŸJýKFK„l_'),(_binary '%õ@BÙ¤z','discharge_TES_amount','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary '-øˆPl„Hã•'),(_binary '8ÖYMÎ@Œ‹:\�','cooling_amount','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary 'Mð¿þúMDì´\�'),(_binary 'YÛ%eŒÚK‘±\�','sdtw_end_time','EMPTY',_binary 'ƒÇÿ\0ØC†\�',_binary '±—Œ,\rWOb¬î',_binary '´U}wEÖG~¡ÿÀ'),(_binary '���9\Z\�N9�\�9ЦY;z','buy_energy','EMPTY',_binary 'ñ3ä’E~¶¬',_binary 'Yè>ðüJ¥¨^^',_binary 'æ�\�\�K8��0\�}-o�'),(_binary '���ʤM��\r���ϑ�','sell_heat','EMPTY',_binary 'ñ3ä’E~¶¬',_binary 'Yè>ðüJ¥¨^^',_binary '_�4�\�UK8���=~��'),(_binary 'y÷ÖÓóFƒ¥','sdtw_percentage','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary '´U}wEÖG~¡ÿÀ'),(_binary '¸ôèyJÄÓ','sdtw_amount','EMPTY',_binary 'ñ3ä’E~¶¬',_binary 'Yè>ðüJ¥¨^^',_binary '´U}wEÖG~¡ÿÀ'),(_binary 'Èç%Ë±ñJá\�','relocate_amount','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary 'pëbÜ@žÚø'),(_binary 'ïåH«PJq‚\�','charge_TES_amount','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary '½ë\\¢VjAf¯¯D'),(_binary 'òæ^Q€Dd²;$','sdtw_start_time','EMPTY',_binary 'ƒÇÿ\0ØC†\�',_binary '±—Œ,\rWOb¬î',_binary '´U}wEÖG~¡ÿÀ'),(_binary '\�|��\�<CQ�%�$�\�jo','host_workload_amount','EMPTY',_binary '˜Z:ïêF›',_binary 'Yè>ðüJ¥¨^^',_binary '�\�N��XG�ܾR�=+'),(_binary 'ӝ\0}q\�s\0\'}��','sell_flexibility','EMPTY',_binary 'ñ3ä’E~¶¬',_binary 'Yè>ðüJ¥¨^^',_binary 'ӝ\0}q\�s\0\'}��'),(_binary '�Bq\\d.C���օ$��X','sell_energy','EMPTY',_binary 'ñ3ä’E~¶¬',_binary 'Yè>ðüJ¥¨^^',_binary 'æ�\�\�K8��0\�}-o�');
/*!40000 ALTER TABLE `action_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_type`
--

DROP TABLE IF EXISTS `action_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action_type` (
  `id` binary(16) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_type`
--

LOCK TABLES `action_type` WRITE;
/*!40000 ALTER TABLE `action_type` DISABLE KEYS */;
INSERT INTO `action_type` VALUES (_binary 'pëbÜ@žÚø','Relocate Load (KWh)'),(_binary '-øˆPl„Hã•','Discharge TES (KWh)'),(_binary '6º.Ë]Fí¢W\�','Discharge Battery (KWh)'),(_binary 'Mð¿þúMDì´\�','Dynamic Adjustment of Cooling Intensity (KWh)'),(_binary '_�4�\�UK8���=~��','Sell Heat (KWh)'),(_binary '_�4�\�UK8���=~��','Buy Heat (KWh)'),(_binary '}:ŸJýKFK„l_','Charge Battery (KWh)'),(_binary '´U}wEÖG~¡ÿÀ','Shift delay-tolerant workload (KWh)'),(_binary '½ë\\¢VjAf¯¯D','Charge TES (KWh)'),(_binary 'æ�\�\�K8��0\�}-o�','Sell Energy (KWh)'),(_binary 'æ�\�\�K8��0\�}-o�','Buy Energy (KWh)'),(_binary 'ӝ\0}q\�s\0\'}��','Sell Flexibility (KWh)'),(_binary '�\�N��XG�ܾR�=+','Host Load (KWh)');
/*!40000 ALTER TABLE `action_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_value`
--

DROP TABLE IF EXISTS `action_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action_value` (
  `id` binary(16) NOT NULL,
  `value` double NOT NULL,
  `action_instance_id` binary(16) NOT NULL,
  `action_property_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK68higqhrns1m5k69alw8ebs3s` (`action_instance_id`),
  KEY `FK5qukk23cv5ufdoeyi6w3ecbjc` (`action_property_id`),
  CONSTRAINT `FK5qukk23cv5ufdoeyi6w3ecbjc` FOREIGN KEY (`action_property_id`) REFERENCES `action_property` (`id`),
  CONSTRAINT `FK68higqhrns1m5k69alw8ebs3s` FOREIGN KEY (`action_instance_id`) REFERENCES `action_instance` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_value`
--

LOCK TABLES `action_value` WRITE;
/*!40000 ALTER TABLE `action_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device` (
  `id` binary(16) NOT NULL,
  `label` varchar(255) NOT NULL,
  `device_type_id` binary(16) NOT NULL,
  `parent` binary(16) DEFAULT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`,`device_type_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `label_UNIQUE` (`label`),
  KEY `fk_device_device_type_idx` (`device_type_id`),
  KEY `fk_device_parent_idx` (`parent`),
  CONSTRAINT `fk_device_device_type` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`id`),
  CONSTRAINT `fk_device_parent` FOREIGN KEY (`parent`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES (_binary 'B\�,\�\�\0]��','MARKETPLACE_PILOT',_binary '.F��,�\�',_binary 'Âß¥¾ÎPé­',_binary '\0'),(_binary '}���SC��\��\�%\��<','PSM-server-room',_binary 'x¨,ïM=¨p\'ß',_binary 'Âß¥¾ÎPé­',_binary '\0'),(_binary '�\�6��Bg�(�-\�\�,','PSM-UPS',_binary 'yü¹IKæ¬ñ',_binary 'Âß¥¾ÎPé­',_binary '\0'),(_binary '�\�l9,H���kW�aO','PSM-cooling-system',_binary '– WSL1¢íÎ',_binary 'Âß¥¾ÎPé­',_binary '\0'),(_binary 'Âß¥¾ÎPé­','PILOT_DATACENTER',_binary '+mý´ÎC)¤þ',NULL,_binary '\0'),(_binary '\�@��y�Dc��;�[5%','PSM-virtual-TES',_binary '\0��p�s\0',_binary 'Âß¥¾ÎPé­',_binary '\0'),(_binary '\�z��\�@����-���\�','PSM-simulated-heat-pump',_binary 'M�\���\�P�r]yJ',_binary 'Âß¥¾ÎPé­',_binary '\0');
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_type`
--

DROP TABLE IF EXISTS `device_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device_type` (
  `id` binary(16) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_type`
--

LOCK TABLES `device_type` WRITE;
/*!40000 ALTER TABLE `device_type` DISABLE KEYS */;
INSERT INTO `device_type` VALUES (_binary '\0��p�s\0','THERMAL ENERGY STORAGE'),(_binary '%ÉŸ(¥€D:¾z','RACK'),(_binary '.F��,�\�','MARKETPLACE'),(_binary 'M�\���\�P�r]yJ','HEAT_RECOVERY_SYSTEM'),(_binary 'x¨,ïM=¨p\'ß','SERVER_ROOM'),(_binary 'yü¹IKæ¬ñ','BATTERY'),(_binary '+mý´ÎC)¤þ','DATACENTER'),(_binary '®þ@Na­å','HEAT_PUMP'),(_binary 'Ñi·óìA\�','VM'),(_binary '– WSL1¢íÎ','COOLING_SYSTEM'),(_binary '‘WŒèK¢´\�','SERVER');
/*!40000 ALTER TABLE `device_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `entry`
--

DROP TABLE IF EXISTS `entry`;
/*!50001 DROP VIEW IF EXISTS `entry`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `entry` AS SELECT 
 1 AS `id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `flexibility_strategy`
--

DROP TABLE IF EXISTS `flexibility_strategy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flexibility_strategy` (
  `id` binary(16) NOT NULL,
  `We` double NOT NULL DEFAULT '0',
  `Wl` double NOT NULL DEFAULT '0',
  `Wf` double NOT NULL DEFAULT '0',
  `Wt` double NOT NULL DEFAULT '0',
  `realloc_active` bit(1) NOT NULL DEFAULT b'0',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datacenter` binary(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flexibility_strategy`
--

LOCK TABLES `flexibility_strategy` WRITE;
/*!40000 ALTER TABLE `flexibility_strategy` DISABLE KEYS */;
/*!40000 ALTER TABLE `flexibility_strategy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure_unit`
--

DROP TABLE IF EXISTS `measure_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `measure_unit` (
  `id` binary(16) NOT NULL,
  `unit` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure_unit`
--

LOCK TABLES `measure_unit` WRITE;
/*!40000 ALTER TABLE `measure_unit` DISABLE KEYS */;
INSERT INTO `measure_unit` VALUES (_binary '€beÑ\\C« hK','CELSIUS'),(_binary 'ÉÖÁP$ÞOm\"\�','WATT'),(_binary 'ñ3ä’E~¶¬','KILOWATT'),(_binary 'ƒÇÿ\0ØC†\�','TIME'),(_binary '˜Z:ïêF›','#NUMBER'),(_binary '���Q,�','DOLLAR');
/*!40000 ALTER TABLE `measure_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurement`
--

DROP TABLE IF EXISTS `measurement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `measurement` (
  `id` binary(16) NOT NULL,
  `property` varchar(255) NOT NULL,
  `observation` varchar(255) NOT NULL,
  `device_type_id` binary(16) NOT NULL,
  `measure_unit_id` binary(16) NOT NULL,
  `property_source_id` binary(16) NOT NULL,
  `value_type_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`,`device_type_id`,`measure_unit_id`,`property_source_id`,`value_type_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `property_UNIQUE` (`property`),
  KEY `fk_measure_device_type1_idx` (`device_type_id`),
  KEY `fk_measure_measure_unit1_idx` (`measure_unit_id`),
  KEY `fk_measure_property_source1_idx` (`property_source_id`),
  KEY `fk_measure_value_type1_idx` (`value_type_id`),
  CONSTRAINT `fk_measure_device_type1` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`id`),
  CONSTRAINT `fk_measure_measure_unit1` FOREIGN KEY (`measure_unit_id`) REFERENCES `measure_unit` (`id`),
  CONSTRAINT `fk_measure_property_source1` FOREIGN KEY (`property_source_id`) REFERENCES `property_source` (`id`),
  CONSTRAINT `fk_measure_value_type1` FOREIGN KEY (`value_type_id`) REFERENCES `value_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurement`
--

LOCK TABLES `measurement` WRITE;
/*!40000 ALTER TABLE `measurement` DISABLE KEYS */;
INSERT INTO `measurement` VALUES (_binary 'd�B��\�','DR_SIGNAL','EMPTY',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '���-�','E_Price','Energy Price',_binary '.F��,�\�',_binary '���Q,�',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'Ê›û{Obš\�','COP_C','Cooling coefficient of performance on cooling system',_binary '– WSL1¢íÎ',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary ')L�p�s\0]','f_TES','TES factor',_binary '\0��p�s\0',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '.Lcp�s\0]\�','TES_R','TES charge loss factor',_binary '\0��p�s\0',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '2��p�s\0','TES_D','TES discharge loss factor',_binary '\0��p�s\0',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '7[ap\�s\0]��','MAX_CHR_TES','TES max charge per time unit',_binary '\0��p�s\0',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '7[ap\�s\0]��','HeatAdaptability','Heat adaptability',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '7[ap\�s\0]��','DCProfit','DC Profit',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '7[ap�s\0]\�','MAX_DIS_TES','TES max discharge per time unit',_binary '\0��p�s\0',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '1g���A��&B\�,\�','E_D_Percentage','Delay tolerant workload',_binary 'x¨,ïM=¨p\'ß',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary 'A�ĺ-��\0','L_Price','Workload Price',_binary '.F��,�\�',_binary '���Q,�',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'BW-��\0]','T_Price','Thermal Price',_binary '.F��,�\�',_binary '���Q,�',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'B�Y-��\0','D_Price','DR Incentive',_binary '.F��,�\�',_binary '���Q,�',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'RO{�s\0]\�','CURRENT CAPACITY_TES','TES Current Capacity',_binary '\0��p�s\0',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'R#	\�Il�NW,��B�','ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM_HOST_RELOCATE','Electrical power consumption host relocate for a server room',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'R#={�s\0]\�','CURRENT CAPACITY_ESD','ESD Current Capacity',_binary 'yü¹IKæ¬ñ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'lòˆÈºPK”0','ELECTRICAL_POWER_CONSUMPTION_COOLING_SYSTEM','Electrical power consumption for a cooling system',_binary '– WSL1¢íÎ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 's�ewr\�N��\��\�o','OPTIMIZED_DC','Optimized profile of DC',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '��f�\�P�r]yJ','COP_H_HEAT','Cooling coefficient of performance on heat recovery system',_binary 'M�\���\�P�r]yJ',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '���\�P�r]yJ','MAXIMUM_POWER_CONSUMPTION_HEAT_RECOVER_SYSTEM','MAX power consumption on heat recovery system',_binary 'M�\���\�P�r]yJ',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '�H`�\�P�r]yJ','MAXIMUM_CAPACITY_COOLING_SYSTEM','MAX capacity of heat recovery system',_binary 'M�\���\�P�r]yJ',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '���\�P�r]yJ','ELECTRICAL_POWER_CONSUMPTION_HEAT_RECOVER_SYSTEM','Current energy consumption of heat recovery system',_binary 'M�\���\�P�r]yJ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '��$��I���).���','OPTIMIZED_THERMAL','Data center optimized thermal profile',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '3‚ÁpGxµ\�','THERMAL_ENERGY_PRODUCTION','Thermal energy production for a server room',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'Â±RR‚BÜ°\"','ELECTRICAL_POWER_CONSUMPTION','Electrical power consumption for entire data center',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'Ò—:pOïCƒ\�','ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM','Electrical power consumption for a server room',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ä»Z«±\\N©’','MAXIMUM_POWER_CONSUMPTION_SERVER_ROOM','Maximum power consumption for a server room',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ŽX“Ùõ#Mœ£','MAXIMUM_POWER_CONSUMPTION_COOLING_SYSTEM','Maximum power consumption for a cooling system',_binary '– WSL1¢íÎ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '\�Z�n!�\�P�r]yJ','DATA_CENTER_CO2_BASELINE','Datacenter CO2 baseline',_binary '+mý´ÎC)¤þ',_binary '˜Z:ïêF›',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '\�\\5\\!�\�P�r]yJ','DATA_CENTER_HEAT_BASELINE','Datacenter heat baseline',_binary '+mý´ÎC)¤þ',_binary '˜Z:ïêF›',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '\�\\�0!�\�P�r]yJ','OPTIMIZED_CO2','Optimized profile of CO2',_binary '+mý´ÎC)¤þ',_binary '˜Z:ïêF›',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '\�]\�!�\�P�r]yJ','OPTIMIZED_COOLING','Optimized profile of cooling consumption',_binary '+mý´ÎC)¤þ',_binary '˜Z:ïêF›',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '̄K���M\0�D�!y��','OPTIMIZED_SERVER','Data center optimized energy profile',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'Ϻ�}r�s\0]','MAX_ESD','Battery max capacity',_binary 'yü¹IKæ¬ñ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '\�b5m\�s\"]��','ELECTRICAL_POWER_CONSUMPTION_IT_REALTIME_WORKLOAD','Electrical power consumption for IT realtime workload',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '\�c5m\�s\"]��','ELECTRICAL_POWER_CONSUMPTION_IT_DELAYTOLERABLE_WORKLOAD','Electrical power consumption for IT rdelay tolerable workload',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '\�}mVr\�s\0]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_HEAT_RECOVER_SYSTEM','PRE OPT Current energy consumption of heat recovery system',_binary 'M�\���\�P�r]yJ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝ\0}Vr\�s\0]��','PRE_OPT_CURRENT CAPACITY_ESD','PRE OP ESD Current Capacity',_binary '– WSL1¢íÎ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝ\0}q\�s\0]��','PUE','DC pue',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝ\0}q\�s\0]��$','ERE','DC ere',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝ\0}q\�s\0]��5','ReusePercent','DC ReusePercent',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝ}Vr\�s\0]��','PRE_OPT_CURRENT CAPACITY_TES','PRE OPT TES Current Capacity',_binary '\0��p�s\0',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝmVr\�s\0]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_COOLING_SYSTEM','PRE OPT Electrical power consumption for a cooling system',_binary '– WSL1¢íÎ',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝ�}C\�s\0]��','DATA_CENTER_BASELINE','Baseline consumption of the datacenter',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary 'ӝ�}s�s\0]','MAX_HOST','Maximum Incoming Workload processed on current DC',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary 'Ӱ*�s�s\0]','MAX_REALLOCATE','Maximum Workload to be reallocated',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '�b5m�s\0]','f_ESD','Battery factor',_binary 'yü¹IKæ¬ñ',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '���n�','MAX_DIS_ESD','Battery max discharge per time unit',_binary 'yü¹IKæ¬ñ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '��=yn�s\0','ESD_D','Battery discharge loss factor',_binary 'yü¹IKæ¬ñ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '��g�n�','DoD','Battery depth of discharge',_binary 'yü¹IKæ¬ñ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '��ryq�s\0','COP_H_COOLING','Cooling coefficient of performance on heating',_binary '– WSL1¢íÎ',_binary '˜Z:ïêF›',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '��� n�','ESD_R','Battery charge loss factor',_binary 'yü¹IKæ¬ñ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '����r\�','MAX_TES','TES max capacity',_binary '\0��p�s\0',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '�c5m\�s\"]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_IT_DELAYTOLERABLE_WORKLOAD','Electrical power consumption for IT delay tolerable workload',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '�C5m\�s\"]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM','Electrical power consumption for server room total workload',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '�c5m\�s\"]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_IT_REALTIME_WORKLOAD','Electrical power consumption for IT realtime workload',_binary 'x¨,ïM=¨p\'ß',_binary 'ñ3ä’E~¶¬',_binary '¿7”#2J ªs',_binary 'Yè>ðüJ¥¨^^'),(_binary '�\��n\�s\0]��','MAX_CHR_ESD','Battery max charge per time unit',_binary 'yü¹IKæ¬ñ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '�\��n\�s\0]��','DCAdapt','DC adaptability',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^'),(_binary '�\��n\�s\0]��','CO2emissions','CO2 emissions',_binary '+mý´ÎC)¤þ',_binary 'ñ3ä’E~¶¬',_binary 'í0—²INÖ°n',_binary 'Yè>ðüJ¥¨^^');
/*!40000 ALTER TABLE `measurement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metric`
--

DROP TABLE IF EXISTS `metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metric` (
  `id` binary(16) NOT NULL,
  `measurement_id` binary(16) NOT NULL,
  `device_id` binary(16) NOT NULL,
  `plan_id` binary(16) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `value` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_metric_measurement_idx` (`measurement_id`),
  KEY `FK_metric_device_idx` (`plan_id`),
  KEY `FKh01rhq9hgchl8k67jl74bd4y3` (`device_id`),
  CONSTRAINT `FK_metric_device` FOREIGN KEY (`plan_id`) REFERENCES `optimization_plans` (`id`),
  CONSTRAINT `FK_metric_measurement` FOREIGN KEY (`measurement_id`) REFERENCES `measurement` (`id`),
  CONSTRAINT `FKh01rhq9hgchl8k67jl74bd4y3` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metric`
--

LOCK TABLES `metric` WRITE;
/*!40000 ALTER TABLE `metric` DISABLE KEYS */;
/*!40000 ALTER TABLE `metric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitored_value`
--

DROP TABLE IF EXISTS `monitored_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `monitored_value` (
  `id` binary(16) NOT NULL,
  `timestamp` datetime NOT NULL,
  `value` double NOT NULL,
  `device_id` binary(16) NOT NULL,
  `measurement_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`,`device_id`,`measurement_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_monitored_value_device1_idx` (`device_id`),
  KEY `fk_monitored_value_measure1_idx` (`measurement_id`),
  CONSTRAINT `fk_monitored_value_device1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `fk_monitored_value_measure1` FOREIGN KEY (`measurement_id`) REFERENCES `measurement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitored_value`
--

LOCK TABLES `monitored_value` WRITE;
/*!40000 ALTER TABLE `monitored_value` DISABLE KEYS */;
INSERT INTO `monitored_value` VALUES (_binary '�	��\�Hߝo�d\�\�\�','2020-04-29 14:37:38',75,_binary '\�@��y�Dc��;�[5%',_binary 'RO{�s\0]\�'),(_binary '�?+KEk�m�^�/','2020-04-29 14:41:53',4.43,_binary '\�z��\�@����-���\�',_binary '��f�\�P�r]yJ'),(_binary 'e�\�sHs�$5\�\�[�','2020-04-29 14:37:38',1,_binary '\�@��y�Dc��;�[5%',_binary '.Lcp�s\0]\�'),(_binary ' \�mɝH֮vM�\��','2020-04-29 14:41:53',780,_binary '\�z��\�@����-���\�',_binary '���\�P�r]yJ'),(_binary '#\�\�\�)\�F}�?��	c\�','2020-04-29 14:36:45',790,_binary '�\�l9,H���kW�aO',_binary '�H`�\�P�r]yJ'),(_binary '%ͦ\'3K�O\�B\n5D','2020-04-29 14:36:45',6.05,_binary '�\�l9,H���kW�aO',_binary 'Ê›û{Obš\�'),(_binary '0\��\�\'=@ۧ#\�ݥ\�%\n','2020-04-29 14:37:38',1,_binary '\�@��y�Dc��;�[5%',_binary ')L�p�s\0]'),(_binary '9f�df�CɅ�� \�9B\�','2020-04-29 14:42:32',1600,_binary '�\�6��Bg�(�-\�\�,',_binary 'R#={�s\0]\�'),(_binary ';\�|�GN�H�\n\�\r','2020-04-29 14:37:38',1,_binary '\�@��y�Dc��;�[5%',_binary '2��p�s\0'),(_binary 'IO�1\�2A�:(7�\Z��','2020-04-29 14:41:53',450,_binary '\�z��\�@����-���\�',_binary '�H`�\�P�r]yJ'),(_binary 'a9\�W\�H&��\�D;�\�','2020-04-29 14:35:28',0,_binary '}���SC��\��\�%\��<',_binary 'Ò—:pOïCƒ\�'),(_binary 'j\�5\�/JқfWOfM�','2020-04-29 14:42:32',1,_binary '�\�6��Bg�(�-\�\�,',_binary '��� n�'),(_binary 'k$A7L��1J�/�','2020-04-29 14:42:32',0.4,_binary '�\�6��Bg�(�-\�\�,',_binary '��g�n�'),(_binary 'rW5j+\�KE��F\�=MA\�','2020-04-29 14:42:32',1600,_binary '�\�6��Bg�(�-\�\�,',_binary 'Ϻ�}r�s\0]'),(_binary '��A�/-D�\0o/G\�\�','2020-04-29 14:35:28',100,_binary '}���SC��\��\�%\��<',_binary 'ӝ�}s�s\0]'),(_binary '�5�\�U�J��n+uJ^�','2020-04-29 14:35:28',100,_binary '}���SC��\��\�%\��<',_binary 'Ӱ*�s�s\0]'),(_binary '�h7�,*D܈H�R(b�','2020-04-29 14:36:45',4.43,_binary '�\�l9,H���kW�aO',_binary '��ryq�s\0'),(_binary '�-P3uI��K��\�/\�','2020-04-29 14:37:38',75,_binary '\�@��y�Dc��;�[5%',_binary '7[ap�s\0]\�'),(_binary '��GX�Ji���#Ǥ\�','2020-04-29 14:42:32',1,_binary '�\�6��Bg�(�-\�\�,',_binary '�b5m�s\0]'),(_binary '�\nT\�}#H�\�@�ꍎ�','2020-04-29 14:36:45',790,_binary '�\�l9,H���kW�aO',_binary 'ŽX“Ùõ#Mœ£'),(_binary '�6�R\�#J�n��o\�\��','2020-04-29 14:37:38',75,_binary '\�@��y�Dc��;�[5%',_binary '����r\�'),(_binary 'ٗ��oGLv��\�-��\�O','2020-04-29 14:42:32',1000,_binary '�\�6��Bg�(�-\�\�,',_binary '�\��n\�s\0]��'),(_binary '\�x6\�\��OK�\�;�OT0F','2020-04-29 14:37:38',75,_binary '\�@��y�Dc��;�[5%',_binary '7[ap\�s\0]��'),(_binary '䌜Ȭ�H�����-\r�W','2020-04-29 14:36:45',300,_binary '�\�l9,H���kW�aO',_binary 'lòˆÈºPK”0'),(_binary '\�#4`\�Ḩ[g`�z�','2020-04-29 14:41:53',177,_binary '\�z��\�@����-���\�',_binary '���\�P�r]yJ'),(_binary '\�0S\0�nI$�2\Z&?\�','2020-04-29 14:42:32',1,_binary '�\�6��Bg�(�-\�\�,',_binary '��=yn�s\0'),(_binary '\�b\�!��H`��	\�{rq�','2020-04-29 14:35:28',0.1,_binary '}���SC��\��\�%\��<',_binary '1g���A��&B\�,\�'),(_binary '\�+Bɶ[Ľ��Z>','2020-04-29 14:42:32',1000,_binary '�\�6��Bg�(�-\�\�,',_binary '���n�'),(_binary '\�\�\�u[AA˕�uO��9','2020-04-29 14:35:28',780,_binary '}���SC��\��\�%\��<',_binary 'ä»Z«±\\N©’');
/*!40000 ALTER TABLE `monitored_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optimization_plans`
--

DROP TABLE IF EXISTS `optimization_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `optimization_plans` (
  `id` binary(16) NOT NULL,
  `carbon_savings` double DEFAULT NULL,
  `confidence_level` double DEFAULT NULL,
  `cost_savings` double DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `energy_savings` double DEFAULT NULL,
  `selected` bit(1) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `timeframe` varchar(255) DEFAULT NULL,
  `wl` double DEFAULT '0',
  `we` double DEFAULT '0',
  `wf` double DEFAULT '0',
  `wt` double DEFAULT '0',
  `realloc_active` bit(1) DEFAULT b'0',
  `datacenter` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7a3ekx2h1qac19ccndg3wex9u` (`datacenter`),
  CONSTRAINT `FK7a3ekx2h1qac19ccndg3wex9u` FOREIGN KEY (`datacenter`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optimization_plans`
--

LOCK TABLES `optimization_plans` WRITE;
/*!40000 ALTER TABLE `optimization_plans` DISABLE KEYS */;
/*!40000 ALTER TABLE `optimization_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predicted_value`
--

DROP TABLE IF EXISTS `predicted_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `predicted_value` (
  `id` binary(16) NOT NULL,
  `timestamp` datetime NOT NULL,
  `value` double NOT NULL,
  `measurement_id` binary(16) NOT NULL,
  `device_id` binary(16) NOT NULL,
  `prediction_job_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`,`measurement_id`,`device_id`,`prediction_job_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_predicted_value_measurement1_idx` (`measurement_id`),
  KEY `fk_predicted_value_device1_idx` (`device_id`),
  KEY `fk_predicted_value_prediction_job1_idx` (`prediction_job_id`),
  CONSTRAINT `fk_predicted_value_device1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `fk_predicted_value_measurement1` FOREIGN KEY (`measurement_id`) REFERENCES `measurement` (`id`),
  CONSTRAINT `FKmu4kdgc6469likviblfmjx10f` FOREIGN KEY (`prediction_job_id`) REFERENCES `prediction_job` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predicted_value`
--

LOCK TABLES `predicted_value` WRITE;
/*!40000 ALTER TABLE `predicted_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `predicted_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prediction_job`
--

DROP TABLE IF EXISTS `prediction_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prediction_job` (
  `id` binary(16) NOT NULL,
  `start_time` datetime NOT NULL,
  `granularity` varchar(45) NOT NULL,
  `algo_type` varchar(45) NOT NULL,
  `flexibility_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prediction_job`
--

LOCK TABLES `prediction_job` WRITE;
/*!40000 ALTER TABLE `prediction_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `prediction_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_source`
--

DROP TABLE IF EXISTS `property_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `property_source` (
  `id` binary(16) NOT NULL,
  `property_source` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_source`
--

LOCK TABLES `property_source` WRITE;
/*!40000 ALTER TABLE `property_source` DISABLE KEYS */;
INSERT INTO `property_source` VALUES (_binary '¿7”#2J ªs','DYNAMIC'),(_binary 'í0—²INÖ°n','STATIC');
/*!40000 ALTER TABLE `property_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trained_model`
--

DROP TABLE IF EXISTS `trained_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trained_model` (
  `id` binary(16) NOT NULL,
  `algo_type` varchar(255) NOT NULL,
  `component_type` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `prediction_type` varchar(255) NOT NULL,
  `scenario` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trained_model`
--

LOCK TABLES `trained_model` WRITE;
/*!40000 ALTER TABLE `trained_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `trained_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` binary(16) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (_binary 'k��B��+��n\�','dc_user','dc_user');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_to_dc`
--

DROP TABLE IF EXISTS `user_to_dc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_to_dc` (
  `id` binary(16) NOT NULL,
  `device_id` binary(16) DEFAULT NULL,
  `user_id` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3aikw7hfc586bq30jtgynvwfg` (`device_id`),
  KEY `FKk6lnrqhquad7okqhmkc42mwdu` (`user_id`),
  CONSTRAINT `FK3aikw7hfc586bq30jtgynvwfg` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FKk6lnrqhquad7okqhmkc42mwdu` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_to_dc`
--

LOCK TABLES `user_to_dc` WRITE;
/*!40000 ALTER TABLE `user_to_dc` DISABLE KEYS */;
INSERT INTO `user_to_dc` VALUES (_binary 'd\�*\�3AC4�\�\��*�e',_binary 'Âß¥¾ÎPé­',_binary 'k��B��+��n\�');
/*!40000 ALTER TABLE `user_to_dc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `value_type`
--

DROP TABLE IF EXISTS `value_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `value_type` (
  `id` binary(16) NOT NULL,
  `value_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `value_type`
--

LOCK TABLES `value_type` WRITE;
/*!40000 ALTER TABLE `value_type` DISABLE KEYS */;
INSERT INTO `value_type` VALUES (_binary '‘²Ÿ£OžT','STRING'),(_binary 'TI HZNÚºEÔsM','INTEGER'),(_binary 'Yè>ðüJ¥¨^^','DOUBLE'),(_binary '±—Œ,\rWOb¬î','LOCAL DATE TIME');
/*!40000 ALTER TABLE `value_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `entry`
--

/*!50001 DROP VIEW IF EXISTS `entry`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `entry` AS select `mv`.`id` AS `id` from (`monitored_value` `mv` join `measurement` `m` on((`mv`.`measurement_id` = `m`.`id`))) where ((`m`.`property` <> 'DR_SIGNAL') and (`m`.`property` <> 'ELECTRICAL_POWER_CONSUMPTION_COOLING_SYSTEM') and (`m`.`property` <> 'ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-29 20:38:54
