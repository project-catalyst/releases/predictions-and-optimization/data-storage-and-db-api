INSERT INTO device (id, label,device_type_id, parent) 
values (UNHEX("AC4EEBDA016C11EA967300155DB0AD02"), "BATTERY_POZNAN",unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("C382C39FC2A61BC38E5011C3A9C2AD1C"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("D2621635016D11EA967300155DB0AD02"), "f_ESD", "Battery factor",  unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("CB9C5A123AC3AFC3AA46E280BAC29005") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("FAACEE20016E11EA967300155DB0AD02"), "ESD_R", "Battery charge loss factor",  unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("FAC33D79016E11EA967300155DB0AD02"), "ESD_D", "Battery discharge loss factor",  unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("FAC707A4016E11EA967300155DB0AD02"), "MAX_DIS_ESD", "Battery max discharge per time unit",  unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("CFBAC67D017211EA967300155DB0AD02"), "MAX_ESD", "Battery max capacity",  unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("FAC967EA016E11EA967300155DB0AD02"), "DoD", "Battery depth of discharge",  unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("52230C3D017B11EA967300155DB0AD02"), "CURRENT CAPACITY", "ESD Current Capacity",  unhex("7911C3BC1FC2B9494BC3A6C2AC0BC3B1"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C2BF0237E2809D23324AC2A0C2AA730E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO device_type (id, type) 
values (UNHEX("00F2F506017011EA967300155DB0AD02"), "THERMAL ENERGY STORAGE");

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("521C1D4F017B11EA967300155DB0AD02"), "CURRENT CAPACITY", "TES Current Capacity",  unhex("00F2F506017011EA967300155DB0AD02"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C2BF0237E2809D23324AC2A0C2AA730E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO device (id, label,device_type_id, parent) 
values (UNHEX("1E125590017011EA967300155DB0AD02"), "TES_POZNAN",unhex("00F2F506017011EA967300155DB0AD02"), unhex("C382C39FC2A61BC38E5011C3A9C2AD1C"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("1E294CEC017011EA967300155DB0AD02"), "f_TES", "TES factor",  unhex("00F2F506017011EA967300155DB0AD02"), unhex("CB9C5A123AC3AFC3AA46E280BAC29005") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("1E2E4C63017011EA967300155DB0AD02"), "TES_R", "TES charge loss factor",  unhex("00F2F506017011EA967300155DB0AD02"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("1E3288DB017011EA967300155DB0AD02"), "TES_D", "TES discharge loss factor",  unhex("00F2F506017011EA967300155DB0AD02"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("1E375B61017011EA967300155DB0AD02"), "MAX_DIS_TES", "TES max discharge per time unit",  unhex("00F2F506017011EA967300155DB0AD02"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("F9D4E2F5017211EA967300155DB0AD02"), "MAX_TES", "TES max capacity",  unhex("00F2F506017011EA967300155DB0AD02"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("D39D997D017311EA967300155DB0AD02"), "MAX_HOST", "Maximum Incoming Workload processed on current DC",  unhex("C29D2B6DC3BDC2B4C38E4329C2A4C3BE"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("D3B02AC9017311EA967300155DB0AD02"), "MAX_REALLOCATE", "Maximum Workload to be reallocated",  unhex("C29D2B6DC3BDC2B4C38E4329C2A4C3BE"), unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("F4E77279017111EA967300155DB0AD02"), "COP_H", "Cooling coefficient of performance on heating",  unhex("E280930620570F534C31C2A2C3ADC38E"), unhex("CB9C5A123AC3AFC3AA46E280BAC29005") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


UPDATE measurement 
set property = "COP_C", observation = "Cooling coefficient of performance on cooling system"
where id = UNHEX("13C38AE280BAC3BB7BC28F4F62C5A1E2");


--  VALUES ESD


 -- f_ESD
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("51F7F928017611EA967300155DB0AD02"), "2019-09-09 00:00:00", 1,  unhex("AC4EEBDA016C11EA967300155DB0AD02"), unhex("D2621635016D11EA967300155DB0AD02"));

 -- ESD_R
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("5201FB63017611EA967300155DB0AD02"), "2019-09-09 00:00:00", 1,  unhex("AC4EEBDA016C11EA967300155DB0AD02"), unhex("FAACEE20016E11EA967300155DB0AD02"));

 -- ESD_D
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("52056D00017611EA967300155DB0AD02"), "2019-09-09 00:00:00", 1,  unhex("AC4EEBDA016C11EA967300155DB0AD02"), unhex("FAC33D79016E11EA967300155DB0AD02"));

 -- MAX_DIS_ESD
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("52089306017611EA967300155DB0AD02"), "2019-09-09 00:00:00", 300,  unhex("AC4EEBDA016C11EA967300155DB0AD02"), unhex("FAC707A4016E11EA967300155DB0AD02"));


 -- DOD
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("520B8A93017611EA967300155DB0AD02"), "2019-09-09 00:00:00", 0.4,  unhex("AC4EEBDA016C11EA967300155DB0AD02"), unhex("FAC967EA016E11EA967300155DB0AD02"));

 -- ESD_MAX
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("520E0777017611EA967300155DB0AD02"), "2019-09-09 00:00:00", 300,  unhex("AC4EEBDA016C11EA967300155DB0AD02"), unhex("CFBAC67D017211EA967300155DB0AD02"));


-- CURRENT_ESD
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("DD3E59DE017B11EA967300155DB0AD02"), "2019-09-09 00:00:00", 100,  unhex("AC4EEBDA016C11EA967300155DB0AD02"), unhex("52230C3D017B11EA967300155DB0AD02"));


-- VALUES TES

 -- f_TES
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("356AF018017711EA967300155DB0AD02"), "2019-09-09 00:00:00", 1,  unhex("1E125590017011EA967300155DB0AD02"), unhex("1E294CEC017011EA967300155DB0AD02"));

 -- TES_R
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("3573DDB1017711EA967300155DB0AD02"), "2019-09-09 00:00:00", 1,  unhex("1E125590017011EA967300155DB0AD02"), unhex("1E2E4C63017011EA967300155DB0AD02"));


 -- TES_D
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("3578B439017711EA967300155DB0AD02"), "2019-09-09 00:00:00", 1,  unhex("1E125590017011EA967300155DB0AD02"), unhex("1E3288DB017011EA967300155DB0AD02"));


 -- MAX_DIS_TES
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("357BDEC5017711EA967300155DB0AD02"), "2019-09-09 00:00:00", 0,  unhex("1E125590017011EA967300155DB0AD02"), unhex("1E375B61017011EA967300155DB0AD02"));


 -- MAX_TES
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("357FBBFE017711EA967300155DB0AD02"), "2019-09-09 00:00:00", 0,  unhex("1E125590017011EA967300155DB0AD02"), unhex("F9D4E2F5017211EA967300155DB0AD02"));

-- CURRENT_TES
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("DD28EE44017B11EA967300155DB0AD02"), "2019-09-09 00:00:00", 0,  unhex("1E125590017011EA967300155DB0AD02"), unhex("521C1D4F017B11EA967300155DB0AD02"));


 -- VALUES datacenetr 
 
 -- MAX_HOST
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("567C8BBF017811EA967300155DB0AD02"), "2019-09-09 00:00:00", 500,  unhex("C382C39FC2A5C2BEC38E5011C3A9C2AD"), unhex("D39D997D017311EA967300155DB0AD02"));

 -- MAX_REALLOC
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("56824A4A017811EA967300155DB0AD02"), "2019-09-09 00:00:00", 500,  unhex("C382C39FC2A5C2BEC38E5011C3A9C2AD"), unhex("D3B02AC9017311EA967300155DB0AD02"));

 -- VALUES cooling system 
 
 -- COP_C
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("5685343C017811EA967300155DB0AD02"), "2019-09-09 00:00:00", 3.3,  unhex("C3A755C2B615C38E5411C3A9C2AD1C02"), unhex("13C38AE280BAC3BB7BC28F4F62C5A1E2"));

 -- COP_H
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("568C38F7017811EA967300155DB0AD02"), "2019-09-09 00:00:00", 2.3,  unhex("C3A755C2B615C38E5411C3A9C2AD1C02"), unhex("F4E77279017111EA967300155DB0AD02"));

 -- MAX_COOL
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("5692BB41017811EA967300155DB0AD02"), "2019-09-09 00:00:00", 400,  unhex("C3A755C2B615C38E5411C3A9C2AD1C02"), unhex("C5BD58E2809CC399C3B5234DC593C2A3"));

 -- VALUES server_room
 
 -- MAX_IT
INSERT INTO monitored_value(id, timestamp , value, device_id, measurement_id)
values (UNHEX("56971199017811EA967300155DB0AD02"), "2019-09-09 00:00:00", 1000,  unhex("C2A6325C17C38E5411C3A9C2AD1C0242"), unhex("C3A4C2BB5AC2ABC2B15C4EC2A9E28099"));




INSERT INTO device_type (id, type) 
values (unhex("2E46AF94052C11EAB4D200155DB0AD02"), "MARKETPLACE");

INSERT INTO device (id, label,device_type_id, parent) 
values (UNHEX("420F13D2052C11EAB4D200155DB0AD02"), "MARKETPLACE_POZNAN",unhex("2E46AF94052C11EAB4D200155DB0AD02"), unhex("C382C39FC2A5C2BEC38E5011C3A9C2AD"));

INSERT INTO measure_unit (id, unit) 
values (UNHEX("C1BE8A51052C11EAB4D200155DB0AD02"), "DOLLAR");


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("07A3F5E7052D11EAB4D200155DB0AD02"), "E_Price", "Energy Price",  unhex("2E46AF94052C11EAB4D200155DB0AD02"), unhex("C1BE8A51052C11EAB4D200155DB0AD02") , unhex("C2BF0237E2809D23324AC2A0C2AA730E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));



INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("41EFC4BA052D11EAB4D200155DB0AD02"), "L_Price", "Workload Price",  unhex("2E46AF94052C11EAB4D200155DB0AD02"), unhex("C1BE8A51052C11EAB4D200155DB0AD02") , unhex("C2BF0237E2809D23324AC2A0C2AA730E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("4207570C052D11EAB4D200155DB0AD02"), "T_Price", "Thermal Price",  unhex("2E46AF94052C11EAB4D200155DB0AD02"), unhex("C1BE8A51052C11EAB4D200155DB0AD02") , unhex("C2BF0237E2809D23324AC2A0C2AA730E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX("420CEF59052D11EAB4D200155DB0AD02"), "D_Price", "DR Incentive",  unhex("2E46AF94052C11EAB4D200155DB0AD02"), unhex("C1BE8A51052C11EAB4D200155DB0AD02") , unhex("C2BF0237E2809D23324AC2A0C2AA730E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO device_type (id, type) 
values (UNHEX(REPLACE(uuid(), '-', '')), "HEAT_RECOVERY_SYSTEM");

select *, hex(id) from device_type;

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX(REPLACE(uuid(), '-', '')), "COP_H_HEAT", "Cooling coefficient of performance on heat recovery system",  unhex("2813CBE615B811EABAE700155DB0AD02"), unhex("CB9C5A123AC3AFC3AA46E280BAC29005") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX(REPLACE(uuid(), '-', '')), "MAXIMUM_POWER_CONSUMPTION_HEAT_RECOVER_SYSTEM", "MAX power consumption on heat recovery system",  unhex("2813CBE615B811EABAE700155DB0AD02"), unhex("CB9C5A123AC3AFC3AA46E280BAC29005") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));


INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX(REPLACE(uuid(), '-', '')), "MAXIMUM_CAPACITY_COOLING_SYSTEM", "MAX capacity of heat recovery system",  unhex("2813CBE615B811EABAE700155DB0AD02"), unhex("CB9C5A123AC3AFC3AA46E280BAC29005") , unhex("C3AD30E28094C2B210494EC396C2B06E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

INSERT INTO measurement(id, property , observation, device_type_id, measure_unit_id, property_source_id, value_type_id)
values (UNHEX(REPLACE(uuid(), '-', '')), "ELECTRICAL_POWER_CONSUMPTION_HEAT_RECOVER_SYSTEM", "Current energy consumption of heat recovery system",  unhex("2813CBE615B811EABAE700155DB0AD02"),  unhex("C3B1081F33C3A4E28099457EC2B6C2AC") , unhex("C2BF0237E2809D23324AC2A0C2AA730E") , unhex("5901C3A83EC3B0C3BC4AC2A5C2A85E5E"));

