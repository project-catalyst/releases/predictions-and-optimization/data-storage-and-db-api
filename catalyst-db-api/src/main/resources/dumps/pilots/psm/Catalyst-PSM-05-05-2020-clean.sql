
CREATE DATABASE  IF NOT EXISTS `catalyst-psm` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `catalyst-psm`;

-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: catalyst-psm
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `device` (
  `id` binary(16) NOT NULL,
  `label` varchar(255) NOT NULL,
  `device_type_id` binary(16) NOT NULL,
  `parent` binary(16) DEFAULT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`,`device_type_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `label_UNIQUE` (`label`),
  KEY `fk_device_device_type_idx` (`device_type_id`),
  KEY `fk_device_parent_idx` (`parent`),
  CONSTRAINT `fk_device_device_type` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`id`),
  CONSTRAINT `fk_device_parent` FOREIGN KEY (`parent`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES ('B\�,\�\�\0]��','MARKETPLACE_PILOT','.F��,�\�','Âß¥¾ÎPé­','\0'),('X:\�\�\�0AO�#�5fP�O','PSM-COOLING-SYSTEM','– WSL1¢íÎ','Âß¥¾ÎPé­','\0'),('Âß¥¾ÎPé­','PILOT_DATACENTER','+mý´ÎC)¤þ',NULL,'\0'),('\�|�k��G8�@vA�pD','PSM-VIRTUAL-TES','\0��p�s\0','Âß¥¾ÎPé­','\0'),('\�N�mɃFY�x~3�^B','PSM-simulated-heat-pump','M�\���\�P�r]yJ','Âß¥¾ÎPé­','\0'),('\�E�j<Dϲ���j\�y�','PSM-SERVER-ROOM','x¨,ïM=¨p\'ß','Âß¥¾ÎPé­','\0'),('��\� \�\'G����\"5\�[','PSM-UPS','yü¹IKæ¬ñ','Âß¥¾ÎPé­','\0');
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_type`
--

DROP TABLE IF EXISTS `device_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `device_type` (
  `id` binary(16) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_type`
--

LOCK TABLES `device_type` WRITE;
/*!40000 ALTER TABLE `device_type` DISABLE KEYS */;
INSERT INTO `device_type` VALUES ('\0��p�s\0','THERMAL ENERGY STORAGE'),('%ÉŸ(¥€D:¾z','RACK'),('.F��,�\�','MARKETPLACE'),('M�\���\�P�r]yJ','HEAT_RECOVERY_SYSTEM'),('x¨,ïM=¨p\'ß','SERVER_ROOM'),('yü¹IKæ¬ñ','BATTERY'),('+mý´ÎC)¤þ','DATACENTER'),('®þ@Na­å','HEAT_PUMP'),('Ñi·óìA\�','VM'),('– WSL1¢íÎ','COOLING_SYSTEM'),('‘WŒèK¢´\�','SERVER');
/*!40000 ALTER TABLE `device_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `entry`
--

DROP TABLE IF EXISTS `entry`;
/*!50001 DROP VIEW IF EXISTS `entry`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `entry` AS SELECT 
 1 AS `id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `flexibility_strategy`
--

DROP TABLE IF EXISTS `flexibility_strategy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `flexibility_strategy` (
  `id` binary(16) NOT NULL,
  `We` double NOT NULL DEFAULT '0',
  `Wl` double NOT NULL DEFAULT '0',
  `Wf` double NOT NULL DEFAULT '0',
  `Wt` double NOT NULL DEFAULT '0',
  `realloc_active` bit(1) NOT NULL DEFAULT b'0',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datacenter` binary(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flexibility_strategy`
--

LOCK TABLES `flexibility_strategy` WRITE;
/*!40000 ALTER TABLE `flexibility_strategy` DISABLE KEYS */;
/*!40000 ALTER TABLE `flexibility_strategy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure_unit`
--

DROP TABLE IF EXISTS `measure_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `measure_unit` (
  `id` binary(16) NOT NULL,
  `unit` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure_unit`
--

LOCK TABLES `measure_unit` WRITE;
/*!40000 ALTER TABLE `measure_unit` DISABLE KEYS */;
INSERT INTO `measure_unit` VALUES ('€beÑ\\C« hK','CELSIUS'),('ÉÖÁP$ÞOm\"\�','WATT'),('ñ3ä’E~¶¬','KILOWATT'),('ƒÇÿ\0ØC†\�','TIME'),('˜Z:ïêF›','#NUMBER'),('���Q,�','DOLLAR');
/*!40000 ALTER TABLE `measure_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurement`
--

DROP TABLE IF EXISTS `measurement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `measurement` (
  `id` binary(16) NOT NULL,
  `property` varchar(255) NOT NULL,
  `observation` varchar(255) NOT NULL,
  `device_type_id` binary(16) NOT NULL,
  `measure_unit_id` binary(16) NOT NULL,
  `property_source_id` binary(16) NOT NULL,
  `value_type_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`,`device_type_id`,`measure_unit_id`,`property_source_id`,`value_type_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `property_UNIQUE` (`property`),
  KEY `fk_measure_device_type1_idx` (`device_type_id`),
  KEY `fk_measure_measure_unit1_idx` (`measure_unit_id`),
  KEY `fk_measure_property_source1_idx` (`property_source_id`),
  KEY `fk_measure_value_type1_idx` (`value_type_id`),
  CONSTRAINT `fk_measure_device_type1` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`id`),
  CONSTRAINT `fk_measure_measure_unit1` FOREIGN KEY (`measure_unit_id`) REFERENCES `measure_unit` (`id`),
  CONSTRAINT `fk_measure_property_source1` FOREIGN KEY (`property_source_id`) REFERENCES `property_source` (`id`),
  CONSTRAINT `fk_measure_value_type1` FOREIGN KEY (`value_type_id`) REFERENCES `value_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurement`
--

LOCK TABLES `measurement` WRITE;
/*!40000 ALTER TABLE `measurement` DISABLE KEYS */;
INSERT INTO `measurement` VALUES ('d�B��\�','DR_SIGNAL','EMPTY','+mý´ÎC)¤þ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('���-�','E_Price','Energy Price','.F��,�\�','���Q,�','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('Ê›û{Obš\�','COP_C','Cooling coefficient of performance on cooling system','– WSL1¢íÎ','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),(')L�p�s\0]','f_TES','TES factor','\0��p�s\0','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('.Lcp�s\0]\�','TES_R','TES charge loss factor','\0��p�s\0','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('2��p�s\0','TES_D','TES discharge loss factor','\0��p�s\0','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('7[ap\�s\0]��','MAX_CHR_TES','TES max charge per time unit','\0��p�s\0','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('7[ap\�s\0]��','HeatAdaptability','Heat adaptability','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('7[ap\�s\0]��','DCProfit','DC Profit','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('7[ap�s\0]\�','MAX_DIS_TES','TES max discharge per time unit','\0��p�s\0','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('1g���A��&B\�,\�','E_D_Percentage','Delay tolerant workload','x¨,ïM=¨p\'ß','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('A�ĺ-��\0','L_Price','Workload Price','.F��,�\�','���Q,�','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('BW-��\0]','T_Price','Thermal Price','.F��,�\�','���Q,�','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('B�Y-��\0','D_Price','DR Incentive','.F��,�\�','���Q,�','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('RO{�s\0]\�','CURRENT CAPACITY_TES','TES Current Capacity','\0��p�s\0','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('R#	\�Il�NW,��B�','ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM_HOST_RELOCATE','Electrical power consumption host relocate for a server room','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('R#={�s\0]\�','CURRENT CAPACITY_ESD','ESD Current Capacity','yü¹IKæ¬ñ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('lòˆÈºPK”0','ELECTRICAL_POWER_CONSUMPTION_COOLING_SYSTEM','Electrical power consumption for a cooling system','– WSL1¢íÎ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('s�ewr\�N��\��\�o','OPTIMIZED_DC','Optimized profile of DC','+mý´ÎC)¤þ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('��f�\�P�r]yJ','COP_H_HEAT','Cooling coefficient of performance on heat recovery system','M�\���\�P�r]yJ','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('���\�P�r]yJ','MAXIMUM_POWER_CONSUMPTION_HEAT_RECOVER_SYSTEM','MAX power consumption on heat recovery system','M�\���\�P�r]yJ','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('�H`�\�P�r]yJ','MAXIMUM_CAPACITY_COOLING_SYSTEM','MAX capacity of heat recovery system','M�\���\�P�r]yJ','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('���\�P�r]yJ','ELECTRICAL_POWER_CONSUMPTION_HEAT_RECOVER_SYSTEM','Current energy consumption of heat recovery system','M�\���\�P�r]yJ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('��$��I���).���','OPTIMIZED_THERMAL','Data center optimized thermal profile','+mý´ÎC)¤þ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('3‚ÁpGxµ\�','THERMAL_ENERGY_PRODUCTION','Thermal energy production for a server room','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('Â±RR‚BÜ°\"','ELECTRICAL_POWER_CONSUMPTION','Electrical power consumption for entire data center','+mý´ÎC)¤þ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('Ò—:pOïCƒ\�','ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM','Electrical power consumption for a server room','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('ä»Z«±\\N©’','MAXIMUM_POWER_CONSUMPTION_SERVER_ROOM','Maximum power consumption for a server room','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('ŽX“Ùõ#Mœ£','MAXIMUM_POWER_CONSUMPTION_COOLING_SYSTEM','Maximum power consumption for a cooling system','– WSL1¢íÎ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('\�Z�n!�\�P�r]yJ','DATA_CENTER_CO2_BASELINE','Datacenter CO2 baseline','+mý´ÎC)¤þ','˜Z:ïêF›','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('\�\\5\\!�\�P�r]yJ','DATA_CENTER_HEAT_BASELINE','Datacenter heat baseline','+mý´ÎC)¤þ','˜Z:ïêF›','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('\�\\�0!�\�P�r]yJ','OPTIMIZED_CO2','Optimized profile of CO2','+mý´ÎC)¤þ','˜Z:ïêF›','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('\�]\�!�\�P�r]yJ','OPTIMIZED_COOLING','Optimized profile of cooling consumption','+mý´ÎC)¤þ','˜Z:ïêF›','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('̄K���M\0�D�!y��','OPTIMIZED_SERVER','Data center optimized energy profile','+mý´ÎC)¤þ','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('Ϻ�}r�s\0]','MAX_ESD','Battery max capacity','yü¹IKæ¬ñ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('\�b5m\�s\"]��','ELECTRICAL_POWER_CONSUMPTION_IT_REALTIME_WORKLOAD','Electrical power consumption for IT realtime workload','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('\�c5m\�s\"]��','ELECTRICAL_POWER_CONSUMPTION_IT_DELAYTOLERABLE_WORKLOAD','Electrical power consumption for IT rdelay tolerable workload','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('ӝ\0}q\�s\0]��','PUE','DC pue','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('ӝ\0}q\�s\0]��$','ERE','DC ere','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('ӝ\0}q\�s\0]��5','ReusePercent','DC ReusePercent','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('ӝ�}C\�s\0]��','DATA_CENTER_BASELINE','Baseline consumption of the datacenter','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('ӝ�}s�s\0]','MAX_HOST','Maximum Incoming Workload processed on current DC','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('Ӱ*�s�s\0]','MAX_REALLOCATE','Maximum Workload to be reallocated','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('�b5m�s\0]','f_ESD','Battery factor','yü¹IKæ¬ñ','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('���n�','MAX_DIS_ESD','Battery max discharge per time unit','yü¹IKæ¬ñ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('��=yn�s\0','ESD_D','Battery discharge loss factor','yü¹IKæ¬ñ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('��g�n�','DoD','Battery depth of discharge','yü¹IKæ¬ñ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('��ryq�s\0','COP_H_COOLING','Cooling coefficient of performance on heating','– WSL1¢íÎ','˜Z:ïêF›','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('��� n�','ESD_R','Battery charge loss factor','yü¹IKæ¬ñ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('����r\�','MAX_TES','TES max capacity','\0��p�s\0','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('�c5m\�s\"]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_IT_DELAYTOLERABLE_WORKLOAD','Electrical power consumption for IT delay tolerable workload','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('�C5m\�s\"]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM','Electrical power consumption for server room total workload','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('�c5m\�s\"]��','PRE_OPT_ELECTRICAL_POWER_CONSUMPTION_IT_REALTIME_WORKLOAD','Electrical power consumption for IT realtime workload','x¨,ïM=¨p\'ß','ñ3ä’E~¶¬','¿7”#2J ªs','Yè>ðüJ¥¨^^'),('�\��n\�s\0]��','MAX_CHR_ESD','Battery max charge per time unit','yü¹IKæ¬ñ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('�\��n\�s\0]��','DCAdapt','DC adaptability','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^'),('�\��n\�s\0]��','CO2emissions','CO2 emissions','+mý´ÎC)¤þ','ñ3ä’E~¶¬','í0—²INÖ°n','Yè>ðüJ¥¨^^');
/*!40000 ALTER TABLE `measurement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metric`
--

DROP TABLE IF EXISTS `metric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `metric` (
  `id` binary(16) NOT NULL,
  `measurement_id` binary(16) NOT NULL,
  `device_id` binary(16) NOT NULL,
  `plan_id` binary(16) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `value` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_metric_measurement_idx` (`measurement_id`),
  KEY `FK_metric_device_idx` (`plan_id`),
  KEY `FKh01rhq9hgchl8k67jl74bd4y3` (`device_id`),
  CONSTRAINT `FK_metric_device` FOREIGN KEY (`plan_id`) REFERENCES `optimization_plans` (`id`),
  CONSTRAINT `FK_metric_measurement` FOREIGN KEY (`measurement_id`) REFERENCES `measurement` (`id`),
  CONSTRAINT `FKh01rhq9hgchl8k67jl74bd4y3` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metric`
--

LOCK TABLES `metric` WRITE;
/*!40000 ALTER TABLE `metric` DISABLE KEYS */;
/*!40000 ALTER TABLE `metric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitored_value`
--

DROP TABLE IF EXISTS `monitored_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `monitored_value` (
  `id` binary(16) NOT NULL,
  `timestamp` datetime NOT NULL,
  `value` double NOT NULL,
  `device_id` binary(16) NOT NULL,
  `measurement_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`,`device_id`,`measurement_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_monitored_value_device1_idx` (`device_id`),
  KEY `fk_monitored_value_measure1_idx` (`measurement_id`),
  CONSTRAINT `fk_monitored_value_device1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `fk_monitored_value_measure1` FOREIGN KEY (`measurement_id`) REFERENCES `measurement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitored_value`
--

LOCK TABLES `monitored_value` WRITE;
/*!40000 ALTER TABLE `monitored_value` DISABLE KEYS */;
INSERT INTO `monitored_value` VALUES ('y��KG�\"0\�\�\�$','2020-05-05 09:33:03',177,'\�N�mɃFY�x~3�^B','���\�P�r]yJ'),('m\�cQ�A��\�;\r\�\�','2020-05-05 09:33:03',177,'\�N�mɃFY�x~3�^B','���\�P�r]yJ'),('\ZF_y\�\�B��\�\�b�W�','2020-05-05 09:21:29',1,'\�|�k��G8�@vA�pD','.Lcp�s\0]\�'),('�ǅ`�K;�\�^\�ژ�','2020-05-05 09:28:26',4.43,'X:\�\�\�0AO�#�5fP�O','��ryq�s\0'),(' \�a9��A��0U�z���','2020-05-05 09:41:10',1,'��\� \�\'G����\"5\�[','��=yn�s\0'),('!��sIܭ}�\�ڵ0*','2020-05-05 09:21:29',75,'\�|�k��G8�@vA�pD','RO{�s\0]\�'),('$L>a+�Dό��j���c','2020-05-05 09:19:06',0.4,'��\� \�\'G����\"5\�[','��g�n�'),('*\0��7�BԊ5Q^ָb�','2020-05-05 09:19:06',1,'��\� \�\'G����\"5\�[','��� n�'),('*MQ۱I�\�BuR\�\��','2020-05-05 09:41:10',1600,'��\� \�\'G����\"5\�[','Ϻ�}r�s\0]'),('3\"rGD@Dh��/\r�O�','2020-05-05 09:41:10',1000,'��\� \�\'G����\"5\�[','�\��n\�s\0]��'),('6\�\�9\�H��P:�\�iܭ','2020-05-05 09:28:26',6.05,'X:\�\�\�0AO�#�5fP�O','Ê›û{Obš\�'),('^>ȥu!Da������B','2020-05-05 09:19:06',1000,'��\� \�\'G����\"5\�[','�\��n\�s\0]��'),('g\�\�\�mCQ�ɇ\�~x�','2020-05-05 09:33:03',4.43,'\�N�mɃFY�x~3�^B','��f�\�P�r]yJ'),('m\�?��Cϑ!\�k��','2020-05-05 09:41:10',1000,'��\� \�\'G����\"5\�[','���n�'),('o\�SFN���.\�䚍y','2020-05-05 09:21:29',1,'\�|�k��G8�@vA�pD','2��p�s\0'),('q����\�I�\�q�|��d','2020-05-05 09:41:10',0.3,'��\� \�\'G����\"5\�[','��g�n�'),('q\�\��݊@��\�}�\�b\�x','2020-05-05 09:19:06',1600,'��\� \�\'G����\"5\�[','R#={�s\0]\�'),('su\�`x�K~�����A','2020-05-05 09:23:01',0.1,'\�E�j<Dϲ���j\�y�','1g���A��&B\�,\�'),('w\�0�2�A��A���R9\�','2020-05-05 09:33:03',450,'\�N�mɃFY�x~3�^B','�H`�\�P�r]yJ'),('|\ZR꨷@@���pr��','2020-05-05 09:23:01',100,'\�E�j<Dϲ���j\�y�','Ӱ*�s�s\0]'),('� D\�CM-�ZG8h\�\�?','2020-05-05 09:21:29',1,'\�|�k��G8�@vA�pD',')L�p�s\0]'),('��\�\�S�G\�\�)\�\�GR','2020-05-05 09:23:01',0,'\�E�j<Dϲ���j\�y�','Ò—:pOïCƒ\�'),('�\�;�\n^B��\����HY','2020-05-05 09:19:06',1000,'��\� \�\'G����\"5\�[','���n�'),('�	��\�UD�����:\r��','2020-05-05 09:28:26',300,'X:\�\�\�0AO�#�5fP�O','ŽX“Ùõ#Mœ£'),('���(\�A��hA\���\r','2020-05-05 09:28:26',790,'X:\�\�\�0AO�#�5fP�O','�H`�\�P�r]yJ'),('��p\�<nG0��\�A5�Y','2020-05-05 09:19:06',1,'��\� \�\'G����\"5\�[','��=yn�s\0'),('��J3B���]�8\���','2020-05-05 09:21:29',75,'\�|�k��G8�@vA�pD','7[ap\�s\0]��'),('��\�\�|\�D��gn���','2020-05-05 09:21:29',75,'\�|�k��G8�@vA�pD','7[ap�s\0]\�'),('�c��hoN��\�E\�\��','2020-05-05 09:41:10',1600,'��\� \�\'G����\"5\�[','R#={�s\0]\�'),('�\�G	�2Ba�n\�\n6','2020-05-05 09:23:01',780,'\�E�j<Dϲ���j\�y�','ä»Z«±\\N©’'),('�k8��Kǧ␻Lk�','2020-05-05 09:41:10',1,'��\� \�\'G����\"5\�[','�b5m�s\0]'),('\��uAY�\��k�\\^','2020-05-05 09:23:01',100,'\�E�j<Dϲ���j\�y�','ӝ�}s�s\0]'),('Ǆ̲7�HԣD�XLxA','2020-05-05 09:19:06',1600,'��\� \�\'G����\"5\�[','Ϻ�}r�s\0]'),('ӟ��ѹC\��M�\�m�\\G','2020-05-05 09:28:26',300,'X:\�\�\�0AO�#�5fP�O','lòˆÈºPK”0'),('ݧ���\�O��K�!i�','2020-05-05 09:41:10',1,'��\� \�\'G����\"5\�[','��� n�'),('\�0\��A��܃\"$�+�','2020-05-05 09:19:06',1,'��\� \�\'G����\"5\�[','�b5m�s\0]'),('�����@�\��/e�\�','2020-05-05 09:21:29',75,'\�|�k��G8�@vA�pD','����r\�');
/*!40000 ALTER TABLE `monitored_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optimization_action`
--

DROP TABLE IF EXISTS `optimization_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `optimization_action` (
  `id` binary(16) NOT NULL,
  `start_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(45) NOT NULL DEFAULT '"NONE"',
  `move_from_date` datetime DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `move_percentage` double DEFAULT '0',
  `optimization_plan_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK-opt-plan_idx` (`optimization_plan_id`),
  CONSTRAINT `FK-opt-plan` FOREIGN KEY (`optimization_plan_id`) REFERENCES `optimization_plans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optimization_action`
--

LOCK TABLES `optimization_action` WRITE;
/*!40000 ALTER TABLE `optimization_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `optimization_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optimization_plans`
--

DROP TABLE IF EXISTS `optimization_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `optimization_plans` (
  `id` binary(16) NOT NULL,
  `carbon_savings` double DEFAULT NULL,
  `confidence_level` double DEFAULT NULL,
  `cost_savings` double DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `energy_savings` double DEFAULT NULL,
  `selected` bit(1) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `timeframe` varchar(255) DEFAULT NULL,
  `wl` double DEFAULT '0',
  `we` double DEFAULT '0',
  `wf` double DEFAULT '0',
  `wt` double DEFAULT '0',
  `realloc_active` bit(1) DEFAULT b'0',
  `datacenter` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7a3ekx2h1qac19ccndg3wex9u` (`datacenter`),
  CONSTRAINT `FK7a3ekx2h1qac19ccndg3wex9u` FOREIGN KEY (`datacenter`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optimization_plans`
--

LOCK TABLES `optimization_plans` WRITE;
/*!40000 ALTER TABLE `optimization_plans` DISABLE KEYS */;
/*!40000 ALTER TABLE `optimization_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predicted_value`
--

DROP TABLE IF EXISTS `predicted_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `predicted_value` (
  `id` binary(16) NOT NULL,
  `timestamp` datetime NOT NULL,
  `value` double NOT NULL,
  `measurement_id` binary(16) NOT NULL,
  `device_id` binary(16) NOT NULL,
  `prediction_job_id` binary(16) NOT NULL,
  PRIMARY KEY (`id`,`measurement_id`,`device_id`,`prediction_job_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_predicted_value_measurement1_idx` (`measurement_id`),
  KEY `fk_predicted_value_device1_idx` (`device_id`),
  KEY `fk_predicted_value_prediction_job1_idx` (`prediction_job_id`),
  CONSTRAINT `FKmu4kdgc6469likviblfmjx10f` FOREIGN KEY (`prediction_job_id`) REFERENCES `prediction_job` (`id`),
  CONSTRAINT `fk_predicted_value_device1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `fk_predicted_value_measurement1` FOREIGN KEY (`measurement_id`) REFERENCES `measurement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predicted_value`
--

LOCK TABLES `predicted_value` WRITE;
/*!40000 ALTER TABLE `predicted_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `predicted_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prediction_job`
--

DROP TABLE IF EXISTS `prediction_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `prediction_job` (
  `id` binary(16) NOT NULL,
  `start_time` datetime NOT NULL,
  `granularity` varchar(45) NOT NULL,
  `algo_type` varchar(45) NOT NULL,
  `flexibility_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prediction_job`
--

LOCK TABLES `prediction_job` WRITE;
/*!40000 ALTER TABLE `prediction_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `prediction_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_source`
--

DROP TABLE IF EXISTS `property_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `property_source` (
  `id` binary(16) NOT NULL,
  `property_source` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_source`
--

LOCK TABLES `property_source` WRITE;
/*!40000 ALTER TABLE `property_source` DISABLE KEYS */;
INSERT INTO `property_source` VALUES ('¿7”#2J ªs','DYNAMIC'),('í0—²INÖ°n','STATIC');
/*!40000 ALTER TABLE `property_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trained_model`
--

DROP TABLE IF EXISTS `trained_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `trained_model` (
  `id` binary(16) NOT NULL,
  `algo_type` varchar(255) NOT NULL,
  `component_type` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `prediction_type` varchar(255) NOT NULL,
  `scenario` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trained_model`
--

LOCK TABLES `trained_model` WRITE;
/*!40000 ALTER TABLE `trained_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `trained_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` binary(16) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('k��B��+��n\�','dc_user','dc_user');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_to_dc`
--

DROP TABLE IF EXISTS `user_to_dc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_to_dc` (
  `id` binary(16) NOT NULL,
  `device_id` binary(16) DEFAULT NULL,
  `user_id` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3aikw7hfc586bq30jtgynvwfg` (`device_id`),
  KEY `FKk6lnrqhquad7okqhmkc42mwdu` (`user_id`),
  CONSTRAINT `FK3aikw7hfc586bq30jtgynvwfg` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `FKk6lnrqhquad7okqhmkc42mwdu` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_to_dc`
--

LOCK TABLES `user_to_dc` WRITE;
/*!40000 ALTER TABLE `user_to_dc` DISABLE KEYS */;
INSERT INTO `user_to_dc` VALUES ('d\�*\�3AC4�\�\��*�e','Âß¥¾ÎPé­','k��B��+��n\�');
/*!40000 ALTER TABLE `user_to_dc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `value_type`
--

DROP TABLE IF EXISTS `value_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `value_type` (
  `id` binary(16) NOT NULL,
  `value_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `value_type`
--

LOCK TABLES `value_type` WRITE;
/*!40000 ALTER TABLE `value_type` DISABLE KEYS */;
INSERT INTO `value_type` VALUES ('‘²Ÿ£OžT','STRING'),('TI HZNÚºEÔsM','INTEGER'),('Yè>ðüJ¥¨^^','DOUBLE'),('±—Œ,\rWOb¬î','LOCAL DATE TIME');
/*!40000 ALTER TABLE `value_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `entry`
--

/*!50001 DROP VIEW IF EXISTS `entry`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `entry` AS select `mv`.`id` AS `id` from (`monitored_value` `mv` join `measurement` `m` on((`mv`.`measurement_id` = `m`.`id`))) where ((`m`.`property` <> 'DR_SIGNAL') and (`m`.`property` <> 'ELECTRICAL_POWER_CONSUMPTION_COOLING_SYSTEM') and (`m`.`property` <> 'ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-05 12:42:03
