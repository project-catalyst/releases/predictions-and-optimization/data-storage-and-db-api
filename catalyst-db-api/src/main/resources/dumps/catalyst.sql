-- MySQL dump 10.13  Distrib 8.0.12, for Linux (x86_64)
--
-- Host: 192.168.253.163    Database: catalyst
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `device` (
  `id` binary(16) NOT NULL,
  `label` varchar(225) DEFAULT NULL,
  `device_type_id` binary(16) DEFAULT NULL,
  `topology_id` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topology.device_idx` (`topology_id`),
  KEY `device_type.device_idx` (`device_type_id`),
  CONSTRAINT `device_type.device` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`id`),
  CONSTRAINT `topology.device` FOREIGN KEY (`topology_id`) REFERENCES `topology` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_type`
--

DROP TABLE IF EXISTS `device_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `device_type` (
  `id` binary(16) NOT NULL,
  `type` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_type`
--

LOCK TABLES `device_type` WRITE;
/*!40000 ALTER TABLE `device_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `energy_consumption`
--

DROP TABLE IF EXISTS `energy_consumption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `energy_consumption` (
  `id` binary(16) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `value` double NOT NULL,
  `topology_id` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhh34uq89lc9ynvtex6xovmlrv` (`topology_id`),
  CONSTRAINT `FKhh34uq89lc9ynvtex6xovmlrv` FOREIGN KEY (`topology_id`) REFERENCES `topology` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `energy_consumption`
--

LOCK TABLES `energy_consumption` WRITE;
/*!40000 ALTER TABLE `energy_consumption` DISABLE KEYS */;
/*!40000 ALTER TABLE `energy_consumption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `energy_prediction`
--

DROP TABLE IF EXISTS `energy_prediction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `energy_prediction` (
  `id` binary(16) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `value` double NOT NULL,
  `topology_id` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKh2wyyejia0da7e2y1byl92ura` (`topology_id`),
  CONSTRAINT `FKh2wyyejia0da7e2y1byl92ura` FOREIGN KEY (`topology_id`) REFERENCES `topology` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `energy_prediction`
--

LOCK TABLES `energy_prediction` WRITE;
/*!40000 ALTER TABLE `energy_prediction` DISABLE KEYS */;
/*!40000 ALTER TABLE `energy_prediction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `measure` (
  `id` binary(16) NOT NULL,
  `property` varchar(225) DEFAULT NULL,
  `observation` varchar(225) DEFAULT NULL,
  `device_type_id` binary(16) DEFAULT NULL,
  `measure_unit_id` binary(16) DEFAULT NULL,
  `property_source_id` binary(16) DEFAULT NULL,
  `measured_value_type_id` binary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_type.measure_idx` (`device_type_id`),
  KEY `measure_unit.measure_idx` (`measure_unit_id`),
  KEY `property_source.measure_idx` (`property_source_id`),
  CONSTRAINT `device_type_id.measure` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`id`),
  CONSTRAINT `measure_unit_id.measure` FOREIGN KEY (`measure_unit_id`) REFERENCES `measure_unit` (`id`),
  CONSTRAINT `property_source_id.measure` FOREIGN KEY (`property_source_id`) REFERENCES `property_source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure`
--

LOCK TABLES `measure` WRITE;
/*!40000 ALTER TABLE `measure` DISABLE KEYS */;
/*!40000 ALTER TABLE `measure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure_unit`
--

DROP TABLE IF EXISTS `measure_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `measure_unit` (
  `id` binary(16) NOT NULL,
  `measure_unit` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure_unit`
--

LOCK TABLES `measure_unit` WRITE;
/*!40000 ALTER TABLE `measure_unit` DISABLE KEYS */;
/*!40000 ALTER TABLE `measure_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_source`
--

DROP TABLE IF EXISTS `property_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `property_source` (
  `id` binary(16) NOT NULL,
  `property_source` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_source`
--

LOCK TABLES `property_source` WRITE;
/*!40000 ALTER TABLE `property_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topology`
--

DROP TABLE IF EXISTS `topology`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `topology` (
  `id` binary(16) NOT NULL,
  `label` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topology`
--

LOCK TABLES `topology` WRITE;
/*!40000 ALTER TABLE `topology` DISABLE KEYS */;
/*!40000 ALTER TABLE `topology` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-16 14:52:50
