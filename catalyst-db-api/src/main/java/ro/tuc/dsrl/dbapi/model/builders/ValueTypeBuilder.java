package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.ValueTypeDTO;
import ro.tuc.dsrl.dbapi.model.ValueType;

public class ValueTypeBuilder {

    private ValueTypeBuilder() {

    }

    public static ValueTypeDTO generateDTOFromEntity(ValueType valueType) {
        return new ValueTypeDTO(
                valueType.getId(),
                valueType.getValueType()
        );
    }

    public static ValueType generateEntityFromDTO(ValueTypeDTO valueTypeDTO) {
        return new ValueType(
                valueTypeDTO.getId(),
                valueTypeDTO.getValueType()
        );
    }
}
