package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.OptimizationActionsValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.OptimizationPageDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.OptimizationRefrenceValuesDTO;

public class OptimizationPageBuilder {
    private OptimizationPageBuilder(){}

    public static OptimizationPageDTO toDTO(OptimizationActionsValuesDTO optimizationActionsValues, OptimizationRefrenceValuesDTO optimizationRefrenceValues, PredictionGranularity granularity, int hour){
      OptimizationPageDTO optimizationPage = new OptimizationPageDTO(granularity.getNoOutputs());

        optimizationPage.setRelocateValues(optimizationActionsValues.getRelocateValues());
        optimizationPage.setHostValues(optimizationActionsValues.getHostValues());
        optimizationPage.setChargeBatteryValues(optimizationActionsValues.getChargeBatteryValues());
        optimizationPage.setDischargeBatteryValues(optimizationActionsValues.getDischargeBatteryValues());
        optimizationPage.setChargeTesValues(optimizationActionsValues.getChargeTesValues());
        optimizationPage.setDischargeTesValues(optimizationActionsValues.getDischargeTesValues());
        optimizationPage.setCoolingValues(optimizationActionsValues.getCoolingValues());
        optimizationPage.setConsumptionRTValues(optimizationActionsValues.getConsumptionRTValues());
        optimizationPage.setConsumptionDTValues(optimizationActionsValues.getConsumptionDTValues());

        optimizationPage.setProvidedEnergyValues(optimizationRefrenceValues.getProvidedEnergyValues());
        optimizationPage.setOptimizedProfileValues(optimizationRefrenceValues.getOptimizedProfileValues());
        optimizationPage.setPredictedDemandValues(optimizationRefrenceValues.getPredictedDemandValues());
        optimizationPage.setRequestedProfileValues(optimizationRefrenceValues.getRequestedProfileValues());
        optimizationPage.setRefrencePrices(optimizationRefrenceValues.getRefrencePrices());
        optimizationPage.setStartHour(hour);

        return  optimizationPage;
    }
}
