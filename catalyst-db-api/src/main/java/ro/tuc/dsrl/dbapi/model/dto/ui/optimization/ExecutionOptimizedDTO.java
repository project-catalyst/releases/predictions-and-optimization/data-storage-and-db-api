package ro.tuc.dsrl.dbapi.model.dto.ui.optimization;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ExecutionOptimizedDTO {

    // Consumption
    private List<Double> consumptionDTValues;
    private List<Double> consumptionRTValues;
    private List<Double> coolingValues;
    private List<Double> upsChargeValues;
    private List<Double> tesChargeValues;
    private List<Double> soldValues;
    private List<Double> hostValues;
    private List<Double> relocateValues;
    // Production
    private List<Double> providedEnergyValues;
    private List<Double> renewableEnergyValues;
    private List<Double> upsDischargeValues;
    private List<Double> tesDischargeValues;

    //Flexibility Order
    private List<Double> flexibilityOrder;
    private List<Double> deltaDiff;
    private List<Double> refrencePrices;

    private int length = 0;

    public ExecutionOptimizedDTO(int length) {
        this.length = length;
    }

    public List<Double> getConsumptionDTValues() {
        return consumptionDTValues;
    }

    public void setConsumptionDTValues(List<Double> consumptionDTValues) {
        assertTrue("consumptionDTValues " + consumptionDTValues.size(), consumptionDTValues.size() == length);
        this.consumptionDTValues = consumptionDTValues;
    }

    public List<Double> getConsumptionRTValues() {
        return consumptionRTValues;
    }

    public void setConsumptionRTValues(List<Double> consumptionRTValues) {
        assertTrue("consumptionRTValues " + consumptionRTValues.size(), consumptionRTValues.size() == length);
        this.consumptionRTValues = consumptionRTValues;
    }

    public List<Double> getCoolingValues() {
        return coolingValues;
    }

    public void setCoolingValues(List<Double> coolingValues) {
        assertTrue("coolingValuesValues " + coolingValues.size(), coolingValues.size() == length);
        this.coolingValues = coolingValues;
    }

    public List<Double> getUpsChargeValues() {
        return upsChargeValues;
    }

    public void setUpsChargeValues(List<Double> upsChargeValues) {
        assertTrue("upsChargeValues " + upsChargeValues.size(), upsChargeValues.size() == length);
        this.upsChargeValues = upsChargeValues;
    }

    public List<Double> getTesChargeValues() {
        return tesChargeValues;
    }

    public void setTesChargeValues(List<Double> tesChargeValues) {
        assertTrue("tesChargeValues " + tesChargeValues.size(), tesChargeValues.size() == length);
        this.tesChargeValues = tesChargeValues;
    }

    public List<Double> getSoldValues() {
        return soldValues;
    }

    public void setSoldValues(List<Double> soldValues) {
        assertTrue("soldValues " + soldValues.size(), soldValues.size() == length);
        this.soldValues = soldValues;
    }

    public List<Double> getProvidedEnergyValues() {
        return providedEnergyValues;
    }

    public void setProvidedEnergyValues(List<Double> providedEnergyValues) {
        assertTrue("greenEnergyValues " + providedEnergyValues.size(), providedEnergyValues.size() == length);
        this.providedEnergyValues = providedEnergyValues;
    }

    public List<Double> getRelocateValues() {
        return relocateValues;
    }

    public void setRelocateValues(List<Double> relocateValues) {
        assertTrue("relocateValues " + relocateValues.size(), relocateValues.size() == length);
        this.relocateValues = relocateValues;
    }

    public List<Double> getUpsDischargeValues() {
        return upsDischargeValues;
    }

    public void setUpsDischargeValues(List<Double> upsDischargeValues) {
        assertTrue("upsDischargeValues " + upsDischargeValues.size(), upsDischargeValues.size() == length);
        this.upsDischargeValues = upsDischargeValues;
    }

    public List<Double> getTesDischargeValues() {
        return tesDischargeValues;
    }

    public void setTesDischargeValues(List<Double> tesDischargeValues) {
        assertTrue("tesDischargeValues " + tesDischargeValues.size(), tesDischargeValues.size() == length);
        this.tesDischargeValues = tesDischargeValues;
    }


    public List<Double> getFlexibilityOrder() {
        return flexibilityOrder;
    }

    public void setFlexibilityOrder(List<Double> flexibilityOrder) {
        assertTrue("flexibilityOrder " + flexibilityOrder.size(), flexibilityOrder.size() == length);
        this.flexibilityOrder = flexibilityOrder;
    }


    public List<Double> getDeltaDiff() {
        return deltaDiff;
    }

    public void setDeltaDiff(List<Double> deltaDiff) {
        assertTrue("deltaDiff " + deltaDiff.size(), deltaDiff.size() == length);

        this.deltaDiff = deltaDiff;
    }

    public List<Double> getHostValues() {
        return hostValues;
    }

    public void setHostValues(List<Double> hostValues) {
        assertTrue("hostValues " + hostValues.size(), hostValues.size() == length);

        this.hostValues = hostValues;
    }

    public List<Double> getRefrencePrices() {
        return refrencePrices;
    }

    public void setRefrencePrices(List<Double> refrencePrices) {
        assertTrue("refrence Prices " + refrencePrices.size(), refrencePrices.size() == length);

        this.refrencePrices = refrencePrices;
    }

    public List<Double> getRenewableEnergyValues() {
        return renewableEnergyValues;
    }

    public void setRenewableEnergyValues(List<Double> renewableEnergyValues) {
        assertTrue("renewableEnergyValues " + renewableEnergyValues.size(), renewableEnergyValues.size() == length);

        this.renewableEnergyValues = renewableEnergyValues;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
