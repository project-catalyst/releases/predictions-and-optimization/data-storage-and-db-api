//package ro.tuc.dsrl.dbapi.service.validators;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import ro.tuc.dsrl.catalyst.model.dto.ActionPropertyDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ActionPropertyValidator {
//
//    private static final Log LOGGER = LogFactory.getLog(ActionPropertyValidator.class);
//
//    private ActionPropertyValidator() {
//    }
//
//    public static void validateInsert(ActionPropertyDTO actionPropertyDTO) {
//
//        List<String> errors = new ArrayList<>();
//        if (actionPropertyDTO == null) {
//            errors.add("Action Property DTO is null");
//            throw new IncorrectParameterException(MeasureUnitValidator.class.getSimpleName(), errors);
//        }
//
//        if (actionPropertyDTO.getProperty() == null || actionPropertyDTO.getProperty().equals("")) {
//            errors.add("Action Property property is null");
//        }
//
//        if (actionPropertyDTO.getObservation() == null || actionPropertyDTO.getObservation().equals("")) {
//            errors.add("Action Property observation is null");
//        }
//
//        if (actionPropertyDTO.getMeasureUnitId() == null) {
//            errors.add("Action Property measure unit is null");
//        }
//
//        if (actionPropertyDTO.getValueTypeId() == null) {
//            errors.add("Action property value type is null");
//        }
//
//        if (actionPropertyDTO.getActionTypeId() == null) {
//            errors.add("Action property action type is null");
//        }
//
//        if (!errors.isEmpty()) {
//            LOGGER.error(errors);
//            throw new IncorrectParameterException(ActionPropertyValidator.class.getSimpleName(), errors);
//        }
//    }
//}
