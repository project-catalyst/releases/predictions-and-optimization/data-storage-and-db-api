package ro.tuc.dsrl.dbapi.service.basic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Metrics;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MetricsDTO;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;
import ro.tuc.dsrl.dbapi.model.Metric;
import ro.tuc.dsrl.dbapi.model.OptimizationPlan;
import ro.tuc.dsrl.dbapi.model.builders.MetricsBuilder;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.MeasurementRepository;
import ro.tuc.dsrl.dbapi.repository.MetricRepository;
import ro.tuc.dsrl.dbapi.repository.OptimizationPlanRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MetricService {

    private static final Log LOGGER = LogFactory.getLog(MetricService.class);
    private final MetricRepository metricRepository;
    private final MeasurementRepository measurementRepository;
    private final DeviceRepository deviceRepository;
    private final OptimizationPlanRepository optimizationPlanRepository;

    @Autowired
    public MetricService(MetricRepository metricRepository, MeasurementRepository measurementRepository, DeviceRepository deviceRepository,OptimizationPlanRepository optimizationPlanRepository ) {
        this.metricRepository = metricRepository;
        this.measurementRepository = measurementRepository;
        this.deviceRepository = deviceRepository;
        this.optimizationPlanRepository = optimizationPlanRepository;
    }

    public List<List<MetricsDTO>> getAllMetricsByTime(String dc) {
        List<List<MetricsDTO>> metrics = new ArrayList<>();

        List<Metric> rawMetrics = this.metricRepository.findByDC(dc);
        List<MetricsDTO> rawMetricsDTO = rawMetrics.stream()
                .map(e -> MetricsBuilder.generateDTOFromEntity(e, e.getMeasurement(), e.getDevice(), e.getPlan()))
                .collect(Collectors.toList());

        rawMetricsDTO.sort(new Comparator<MetricsDTO>() {
            @Override
            public int compare(MetricsDTO m0, MetricsDTO m1) {
                return (int) (m0.getTime() - m1.getTime());
            }
        });

        long time = -1;
        for(MetricsDTO metricsDTO : rawMetricsDTO)
        {
            if(metricsDTO.getTime() != time)
            {
                time = metricsDTO.getTime();
                metrics.add(new ArrayList<>());
            }

            metrics.get(metrics.size() - 1).add(metricsDTO);
        }

        return metrics;
    }

    public List<MetricsDTO> getMetricsByPlan(LocalDateTime time, String dcName, Timeframe timeframe){
        List<Metric> metricsList;
        if(Timeframe.NONE.equals(timeframe)){
            metricsList = metricRepository.findByDCandPlanTiemframe(time,dcName);
        }else {
            metricsList = metricRepository.findByDCandPlanTiemframe(time, dcName, timeframe.getName());
        }
        return metricsList.stream()
                .map(e ->  MetricsBuilder.generateDTOFromEntity( e, e.getMeasurement(), e.getDevice(), e.getPlan()))
                .collect(Collectors.toList());
    }

    public List<MetricsDTO> getMetrics(LocalDateTime time, String dcName){
        List<Metric> metricsList = metricRepository.findByDeviceAfterDate(time,dcName);
        return metricsList.stream()
                .map(e ->  MetricsBuilder.generateDTOFromEntity( e, e.getMeasurement(), e.getDevice(), e.getPlan()))
                .collect(Collectors.toList());
    }

    public MetricsDTO insertMetric(MetricsDTO dto){
        Measurement m = measurementRepository.findMeasurementByProperty(dto.getMetric());
        if (m == null) {
            throw new ResourceNotFoundException(Measurement.class.getSimpleName());
        }
        Device dc = deviceRepository.findDeviceByLabel(dto.getDatacenterName());
        if (dc == null) {
            throw new ResourceNotFoundException(Device.class.getSimpleName());
        }
        LocalDateTime time = DateUtils.millisToLocalDateTime(dto.getTime());
        List<OptimizationPlan> plans = optimizationPlanRepository.findByStartDateAndTimeframe(time, dto.getTimeframe(), dc.getId());

        OptimizationPlan plan = null;
        if(plans!= null && plans.size() >1){
            LOGGER.error("There are multiple plans saved for: " + dto.getTimeframe()+" "  + dto.getTime() +" "+ dto.getDatacenterName());
            LOGGER.info("Only first plan is considered. ");
            plan = plans.get(0);
        }else{
            if(plans!= null && plans.size() == 1){
                plan = plans.get(0);
            }
        }
        Metric metricEntity = new Metric(time, dto.getValue(), m, dc, plan);
        metricEntity =   metricRepository.save(metricEntity);
        return MetricsBuilder.generateDTOFromEntity( metricEntity, m, dc, plan);
    }
}
