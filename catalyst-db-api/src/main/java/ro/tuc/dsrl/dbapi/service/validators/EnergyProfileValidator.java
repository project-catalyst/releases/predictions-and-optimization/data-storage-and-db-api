package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.error_handler.ErrorMessageConstants;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EnergyProfileValidator {

    private static final Log LOGGER = LogFactory.getLog(EnergyProfileValidator.class);

    private EnergyProfileValidator() {

    }

    public static List<String> validateParams(UUID componentID,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity) {

        List<String> errors = new ArrayList<>();

        if (componentID == null) {
            errors.add(ErrorMessageConstants.COMPONENT_ID_NOT_NULL);
        }

        if (startTime == null) {
            errors.add(ErrorMessageConstants.START_TIME_NOT_NULL);
        }

        if (predictionGranularity == null) {
            errors.add("Granularity is null.");
        }

        return errors;
    }

    public static List<String> validateParams(String componentName,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity,
                                              String energyType) {

        List<String> errors = new ArrayList<>();

        if (componentName == null) {
            errors.add(ErrorMessageConstants.COMPONENT_ID_NOT_NULL);
        }

        if (startTime == null) {
            errors.add(ErrorMessageConstants.START_TIME_NOT_NULL);
        }

        if (predictionGranularity == null) {
            errors.add("Granularity is null.");
        }

        if (energyType == null) {
            errors.add(ErrorMessageConstants.ENERGY_TYPE_NOT_NULL);
        }

        return errors;
    }

    public static List<String> validateParams(UUID componentID,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity,
                                              String energyType,
                                              int granularity) {

        List<String> errors;

        errors = EnergyProfileValidator.validateParams(componentID, startTime, predictionGranularity);

        if (energyType == null) {
            errors.add(ErrorMessageConstants.ENERGY_TYPE_NOT_NULL);
        }

        if (granularity < 0) {
            errors.add("Granularity is smaller than 0");
        }

        return errors;
    }

    public static List<String> validateParams(UUID componentID,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity,
                                              UUID serverID,
                                              String energyType,
                                              int granularity) {

        List<String> errors;

        errors = EnergyProfileValidator.validateParams(componentID, startTime, predictionGranularity, energyType, granularity);

        if (serverID == null) {
            errors.add("Server ID is null");
        }

        return errors;
    }

    public static List<String> validateParams(UUID componentID,
                                              LocalDateTime startTime,
                                              LocalDateTime endTime,
                                              PredictionGranularity predictionGranularity,
                                              String energyType,
                                              int granularity) {

        List<String> errors;

        errors = EnergyProfileValidator.validateParams(componentID, startTime, predictionGranularity, energyType, granularity);

        if (endTime == null) {
            errors.add("End Time is null");
        }

        return errors;
    }

    public static List<String> validateParams(UUID componentID,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity,
                                              UUID serverID,
                                              String energyType) {

        List<String> errors;

        errors = EnergyProfileValidator.validateParams(componentID, startTime, predictionGranularity, energyType);

        if (serverID == null) {
            errors.add("Server ID is null");
        }

        return errors;
    }

    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(EnergyProfileValidator.class.getSimpleName(), errors);
        }

    }

    public static List<String> validateParams(UUID componentID,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity,
                                              String energyType) {

        List<String> errors = new ArrayList<>();

        if (componentID == null) {
            errors.add(ErrorMessageConstants.COMPONENT_ID_NOT_NULL);
            throw new IncorrectParameterException(DeviceValidator.class.getSimpleName(), errors);
        }

        if (startTime == null) {
            errors.add(ErrorMessageConstants.START_TIME_NOT_NULL);
        }

        if (predictionGranularity == null) {
            errors.add("Granularity is null");
        }

        if (energyType == null) {
            errors.add(ErrorMessageConstants.ENERGY_TYPE_NOT_NULL);
        }

        return errors;
    }

    public static List<String> validateParams(UUID componentID,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity,
                                              String energyType,
                                              UUID measurementID) {
        List<String> errors = validateParams(componentID, startTime, predictionGranularity, energyType);

        if(measurementID == null) {
            errors.add(ErrorMessageConstants.COMPONENT_ID_NOT_NULL);
        }

        return errors;

    }

    public static List<String> validateCurveParams(EnergyProfileDTO energyProfileDTO) {

        List<String> errors = new ArrayList<>();

        if (energyProfileDTO.getCurve() == null) {
            errors.add("Energy Curve must not be null");
        }

        if (energyProfileDTO.getDeviceId() == null) {
            errors.add("DeviceId must not be null");
        }

        if (energyProfileDTO.getMeasurementId() == null) {
            errors.add("Measurement Id must not be null.");
        }

        if (energyProfileDTO.getAggregationGranularity() == null) {
            errors.add("Aggregation Granularity must not be null");
        }

        if (energyProfileDTO.getPredictionGranularity() == null) {
            errors.add("Prediction Granularity must not be null");
        }

        if (energyProfileDTO.getPredictionType() == null) {
            errors.add("Prediction Type must not be null.");
        }

        return errors;
    }
}
