package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.enums.ActionTypeName;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.OptimizationPreconditions;
import ro.tuc.dsrl.dbapi.model.dto.ItLoadBalancerActions;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.service.basic.OptimizationActionService;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/action")
public class OptimizationActionController {

    @Value("${datacenter.name}")
    private String dcName;

    private final OptimizationActionService optimizationActionService;

    public OptimizationActionController(OptimizationActionService optimizationActionService) {
        this.optimizationActionService = optimizationActionService;
    }


    @GetMapping(value = "/active/{currentDate}/{confidenceLevel}")
    public List<OptimizationActionDTO> getAllCurrentActionsToBeExecuted(@PathVariable("currentDate") Long currentDateMillis,
                                                    @PathVariable("confidenceLevel") Double confidenceLevel) {

        OptimizationPreconditions.validateCurrentDateAndConfidenceLevelThrowErrors(currentDateMillis, confidenceLevel);
        LocalDateTime currentDateTime = DateUtils.millisToUTCLocalDateTime(currentDateMillis);
        return optimizationActionService.getCurrentActionsToBeExecuted(currentDateTime, confidenceLevel, dcName);

    }

    @GetMapping(value = "/active/split-sdtw/{currentDate}")
    public ItLoadBalancerActions getAllCurrentActionsToBeExecutedSplitLoad(@PathVariable("currentDate") Long currentDateMillis) {

        OptimizationPreconditions.validateCurrentDateAndConfidenceLevelThrowErrors(currentDateMillis, 0.0);
        LocalDateTime currentDateTime = DateUtils.millisToUTCLocalDateTime(currentDateMillis);
        return optimizationActionService.getCurrentSplitSDTWActions(currentDateTime, 0.0, dcName);
    }

    @GetMapping(value = "/active/it-load/{currentDate}")
    public List<OptimizationActionDTO>  getAllCurrentActionsToBeExecutedITLoad(@PathVariable("currentDate") Long currentDateMillis) {

        OptimizationPreconditions.validateCurrentDateAndConfidenceLevelThrowErrors(currentDateMillis, 0.0);
        LocalDateTime currentDateTime = DateUtils.millisToUTCLocalDateTime(currentDateMillis);
        return optimizationActionService.getCurrentItLoadBalancerActions(currentDateTime, 0.0, dcName);
    }

    @GetMapping(value = "/day-ahead/{actionType}/{startTime}/{endTime}/{confidenceLevel}")
    public List<OptimizationActionDTO> getSpecificOptimizationActionDA(@PathVariable("actionType") String actionType,
                                                             @PathVariable("startTime") Long startTime,
                                                             @PathVariable("endTime") Long endTime,
                                                             @PathVariable("confidenceLevel") Double confidenceLevel) {
        OptimizationPreconditions.validateActivityDataCenterIdAndIntervalThrowErrors(actionType, startTime, endTime);
        LocalDateTime timeStart = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime timeEnd = DateUtils.millisToLocalDateTime(endTime);
        ActionTypeName action = ActionTypeName.getByShortName(actionType);
        return this.optimizationActionService.getActionsInIntervalByType(dcName, timeStart, timeEnd, action, confidenceLevel, Timeframe.DAY_AHEAD);
    }

    @GetMapping(value = "/intraday/{actionType}/{startTime}/{endTime}/{confidenceLevel}")
    public List<OptimizationActionDTO> getSpecificOptimizationActionID(@PathVariable("actionType") String actionType,
                                                                     @PathVariable("startTime") Long startTime,
                                                                     @PathVariable("endTime") Long endTime,
                                                                     @PathVariable("confidenceLevel") Double confidenceLevel) {
        OptimizationPreconditions.validateActivityDataCenterIdAndIntervalThrowErrors(actionType, startTime, endTime);
        LocalDateTime timeStart = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime timeEnd = DateUtils.millisToLocalDateTime(endTime);
        ActionTypeName action = ActionTypeName.getByShortName(actionType);
        return this.optimizationActionService.getActionsInIntervalByType(dcName, timeStart, timeEnd, action, confidenceLevel, Timeframe.INTRA_DAY);
    }






//    @PostMapping(value = "/actions/new-optimization-plan")
//    public List<BaseActionDTO> postOptimizationPlanWithActions(@RequestBody OptimizationPlanWithActionsDTO optPlan) {
//
//        OptimizationPreconditions.validateNewOptimizationPlanWithActionsThrowErrors(optPlan);
//
//        return this.optimizationActionService.insertOptimizationPlanWithActions(optPlan);
//
//    }
//        @GetMapping(value = "/all-actions/{currentDate}/{confidenceLevel}/{dataCenterName}")
//    public List<OptimizationActionDTO> getAllCurrentActions(@PathVariable("currentDate") Long currentDateMillis,
//                                                            @PathVariable("confidenceLevel") Double confidenceLevel,
//                                                            @PathVariable("dataCenterName") String dataCenterName) {
//
//
//
//        OptimizationPreconditions.validateCurrentDateAndConfidenceLevelThrowErrors(currentDateMillis, confidenceLevel);
//
//        LocalDateTime currentDateTime = DateUtils.millisToUTCLocalDateTime(currentDateMillis);
//
//        UUID dataCenterId = this.deviceRepository.findByLabel(dataCenterName);
//
//        if(dataCenterId == null){
//           throw new ResourceNotFoundException("Datacenter with name: " + dataCenterName + " NOT FOUND!");
//        }
//
//        return optimizationActionService.getAllCurrentActions(currentDateTime, confidenceLevel, dataCenterName);
//
//    }

//    @Deprecated
//    @PostMapping(value = "/redundant-actions/{date}/{confidenceLevel}/{dataCenterName}")
//    public List<BaseActionDTO> getRedundantDayAheadActions(@PathVariable("date") Long dateMillis,
//                                                           @PathVariable("confidenceLevel") Double confidenceLevel,
//                                                           @PathVariable("dataCenterName") String dataCenterName,
//                                                           @RequestBody String[] types) {
//
//
//
//        OptimizationPreconditions.validateCurrentDateAndConfidenceLevelThrowErrors(dateMillis, confidenceLevel);
//
//        LocalDateTime dateTime = DateUtils.millisToUTCLocalDateTime(dateMillis);
//
//        UUID dataCenterId = this.deviceRepository.findByLabel(dataCenterName);
//
//        if(dataCenterId == null){
//            throw new ResourceNotFoundException("Datacenter with name: " + dataCenterName + " NOT FOUND!");
//        }
//
//        return optimizationActionService.getRedundantDayAheadActions(dateTime, confidenceLevel, types, dataCenterName);
//
//    }


}
