//package ro.tuc.dsrl.dbapi.model.builders;
//
//import ro.tuc.dsrl.catalyst.model.dto.ActionTypeDTO;
//import ro.tuc.dsrl.dbapi.model.ActionType;
//
//public class ActionTypeBuilder {
//
//    private ActionTypeBuilder() {
//
//    }
//
//    public static ActionTypeDTO generateDTOFromEntity(ActionType actionType) {
//        return new ActionTypeDTO(
//                actionType.getId(),
//                actionType.getType()
//        );
//    }
//
//    public static ActionType generateEntityFromDTO(ActionTypeDTO actionTypeDTO) {
//        return new ActionType(
//                actionTypeDTO.getId(),
//                actionTypeDTO.getTypeOfActivity()
//        );
//    }
//}
