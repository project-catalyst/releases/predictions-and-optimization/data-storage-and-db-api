package ro.tuc.dsrl.dbapi.service.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.GraphDataDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.LabeledValues;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class FlexibilityGraphDataService {

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;

    @Autowired
    public FlexibilityGraphDataService(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService) {

        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
    }

    public GraphDataDTO getPredictedFlexibilityForDC(UUID dataCenterID,
                                                     LocalDateTime startTime,
                                                     PredictionGranularity predictionGranularity,
                                                     String energyType) {
        // validate inputs
        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        // get predicted lower bounds from db
        List<EnergyProfileDTO> lowerBoundsDTO = energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(
                dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.LOWER.getType());

        EnergyProfileDTO dtLowerBounds = lowerBoundsDTO.get(0);
        EnergyProfileDTO rtLowerBounds = lowerBoundsDTO.get(1);


        List<Double> lowerBoundsDataDT = padWithZerosIfNeeded(dtLowerBounds, predictionGranularity.getNoOutputs());
        List<Double> lowerBoundsDataRT = padWithZerosIfNeeded(rtLowerBounds, predictionGranularity.getNoOutputs());
        List<Double> summedLowerBounds = new ArrayList<>();
        for(int i=0; i< lowerBoundsDataDT.size(); i++)
        {
            summedLowerBounds.add(Double.valueOf(lowerBoundsDataDT.get(i)) + Double.valueOf(lowerBoundsDataRT.get(i)));
        }

        // get predicted upper bounds from db
        List<EnergyProfileDTO> upperBoundsDTO = energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(
                dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.UPPER.getType());

        EnergyProfileDTO dtUpperBounds = upperBoundsDTO.get(0);
        EnergyProfileDTO rtUpperBounds = upperBoundsDTO.get(1);


        List<Double> upperBoundsDataDT = padWithZerosIfNeeded(dtUpperBounds, predictionGranularity.getNoOutputs());
        List<Double> upperBoundsDataRT = padWithZerosIfNeeded(rtUpperBounds, predictionGranularity.getNoOutputs());

        // sum rt and dt and return only two curves
        List<Double> summedUpperBounds = new ArrayList<>();
        for(int i=0; i< upperBoundsDataDT.size(); i++)
        {
            summedUpperBounds.add(Double.valueOf(upperBoundsDataDT.get(i)) + Double.valueOf(upperBoundsDataRT.get(i)));
        }

        LabeledValues predictedLabeledValuesLowerBounds = new LabeledValues(
                FlexibilityType.LOWER.getType().toLowerCase(), summedLowerBounds);
        LabeledValues predictedLabeledValuesUpperBounds = new LabeledValues(
                FlexibilityType.UPPER.getType().toLowerCase(), summedUpperBounds);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(predictedLabeledValuesLowerBounds);
        graphDataDTO.addLabeledValues(predictedLabeledValuesUpperBounds);

        return graphDataDTO;
    }

    private static List<Double> padWithZerosIfNeeded(EnergyProfileDTO energyProfileDTO, int size) {

        if (energyProfileDTO == null) {
            return new ArrayList<>(Collections.nCopies(size, 0.0));
        }

        List<Double> energyValues = energyProfileDTO.getEnergyValues();
        if (energyValues.size() == size) {
            return energyValues;
        }

        energyValues.addAll(Collections.nCopies(size - energyValues.size(), 0.0));
        return energyValues;
    }
}
