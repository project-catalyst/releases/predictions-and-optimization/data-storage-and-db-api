package ro.tuc.dsrl.dbapi.service.basic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationPlanDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.utils.Constants;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.OptimizationAction;
import ro.tuc.dsrl.dbapi.model.OptimizationPlan;
import ro.tuc.dsrl.dbapi.model.builders.OptimizationPlanBuilder;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.OptimizationActionRepository;
import ro.tuc.dsrl.dbapi.repository.OptimizationPlanRepository;
import ro.tuc.dsrl.dbapi.service.validators.OptimizationPlanValidator;
import ro.tuc.dsrl.dbapi.util.MathUtils;
import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;
import ro.tuc.dsrl.geyser.datamodel.actions.ShiftDelayTolerantWorkload;
import ro.tuc.dsrl.geyser.datamodel.actions.WorkloadHosting;
import ro.tuc.dsrl.geyser.datamodel.actions.WorkloadRelocation;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OptimizationPlanService {
    private static final Log LOGGER = LogFactory.getLog(OptimizationPlanService.class);

    private final OptimizationPlanRepository optimizationPlanRepository;
    private final OptimizationActionRepository optimizationActionRepository;

    private final DeviceRepository deviceRepository;

    @Autowired
    public OptimizationPlanService(OptimizationPlanRepository optimizationPlanRepository,
                                   OptimizationActionRepository optimizationActionRepository,
                                   DeviceRepository deviceRepository) {
        this.optimizationPlanRepository = optimizationPlanRepository;
        this.deviceRepository = deviceRepository;
        this.optimizationActionRepository = optimizationActionRepository;
    }

    public List<OptimizationPlanDTO> findAll() {
        List<OptimizationPlan> optimizationPlans = optimizationPlanRepository.findAll();

        return optimizationPlans.stream()
                .map(OptimizationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public void insert(OptimizationPlansContainer plans) {
        Device dataCenter = this.deviceRepository.findDeviceByLabel(plans.getDataCenterName());

        if (dataCenter == null) {
            LOGGER.error("Data center with name: " + plans.getDataCenterName() + " not found");
        }

        for (OptimizerPlan plan : plans.getOptimizations()) {
            LocalDateTime start = DateUtils.millisToUTCLocalDateTime(plans.getStartDate().getTime());
            LocalDateTime end = DateUtils.millisToUTCLocalDateTime(plans.getEndDate().getTime());

            OptimizationPlan planRest = new OptimizationPlan(plans.getTimeframe(), start, end, false, plan.getConfidenceLevel(), plan.getWE(), plan.getWL(), plan.getWF(), plan.getWT(), plan.getWren(), plan.isReallocActive(), dataCenter);
            planRest = this.optimizationPlanRepository.save(planRest);


            List<OptimizationAction> actions = new ArrayList<>();

            for (EnergyEfficiencyOptimizationAction action : plan.getActions()) {

                String name = action.getDescription();

                start = DateUtils.millisToUTCLocalDateTime(action.getStartTime().getTime());
                end = DateUtils.millisToUTCLocalDateTime(action.getEndTime().getTime());

                if (action.getAmountOfEnergy() >= Constants.MEASURE_UNIT_CONVERSION) {
                    OptimizationAction optAction = new OptimizationAction(name, start, end, MathUtils.round(action
                            .getAmountOfEnergy(), 2), MathUtils.round(action
                            .getAmountThermal(), 2), planRest);

                    if (action instanceof ShiftDelayTolerantWorkload) {
                        ShiftDelayTolerantWorkload sdtw = (ShiftDelayTolerantWorkload) action;
                        LocalDateTime sdtwTime = DateUtils.millisToUTCLocalDateTime(sdtw.getFromTime().getTime());
                        optAction.setMoveFromDate(sdtwTime);
                        optAction.setMovePercentage(sdtw.getPercentOfWorkloadDelayed());
                    }

                    if (action instanceof WorkloadRelocation) {
                        WorkloadRelocation reallocw = (WorkloadRelocation) action;
                        optAction.setMovePercentage(reallocw.getPercentOfWorkloadReallocated());
                    }

                    if (action instanceof WorkloadHosting) {
                        WorkloadHosting reallocw = (WorkloadHosting) action;
                        optAction.setMovePercentage(reallocw.getpercentOfWorkloadHosting());
                    }
                    actions.add(optAction);
                }
            }
            actions = optimizationActionRepository.saveAll(actions);
            LOGGER.info("Saved actions list of size :  " + actions.size());
        }
    }

}
