//package ro.tuc.dsrl.dbapi.controller.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ro.tuc.dsrl.catalyst.model.dto.ActionInstanceDTO;
//import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
//import ro.tuc.dsrl.dbapi.service.basic.ActionInstanceService;
//
//import java.util.List;
//import java.util.UUID;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/action-instance")
//public class ActionInstanceController {
//
//    private final ActionInstanceService actionInstanceService;
//
//    @Autowired
//    public ActionInstanceController(ActionInstanceService actionInstanceService) {
//        this.actionInstanceService = actionInstanceService;
//    }
//
//    @GetMapping(value = "/byId/{actionInstanceId}")
//    public ActionInstanceDTO findById(@PathVariable("actionInstanceId") String actionInstanceId) {
//
//        UUIDPreconditions.validate(actionInstanceId);
//
//        UUID actionInstanceUUID = UUID.fromString(actionInstanceId);
//
//        return actionInstanceService.findById(actionInstanceUUID);
//    }
//
//    @GetMapping(value = "/all")
//    public List<ActionInstanceDTO> findAll() {
//
//        return actionInstanceService.findAll();
//    }
//
//    @PostMapping(value = "/insert")
//    public UUID insertActionInstanceDTO(@RequestBody ActionInstanceDTO actionInstanceDTO) {
//
//        return actionInstanceService.insert(actionInstanceDTO);
//    }
//
//}
