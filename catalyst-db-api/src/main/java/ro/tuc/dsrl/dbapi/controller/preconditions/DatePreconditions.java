package ro.tuc.dsrl.dbapi.controller.preconditions;

import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class DatePreconditions {

    private DatePreconditions() {

    }

    public static void validate(Long date) {

        List<String> errors = new ArrayList<>();
        if (date == null) {
            errors.add("Date must not be null");
        }
        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect Parameters", errors);
        }
    }
}
