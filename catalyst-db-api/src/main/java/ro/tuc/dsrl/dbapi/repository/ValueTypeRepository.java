package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.ValueType;

import java.util.UUID;

@Repository
public interface ValueTypeRepository extends JpaRepository<ValueType, UUID> {

    @Query(value = "SELECT e " +
            "FROM ValueType e " +
            "WHERE e.id = :uuid")
    ValueType findByUUId(@Param("uuid") UUID uuid);
}
