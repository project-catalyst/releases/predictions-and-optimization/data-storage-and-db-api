package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.PropertySourceDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.PropertySourceService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/property-source")
public class PropertySourceController {

    private final PropertySourceService propertySourceService;

    @Autowired
    public PropertySourceController(PropertySourceService propertySourceService) {
        this.propertySourceService = propertySourceService;
    }

    @GetMapping(value = "/byId/{propertySourceId}")
    public PropertySourceDTO findById(@PathVariable("propertySourceId") String propertySourceId) {

        UUIDPreconditions.validate(propertySourceId);

        UUID propertySourceUUID = UUID.fromString(propertySourceId);

        return propertySourceService.findById(propertySourceUUID);
    }

    @GetMapping(value = "/all")
    public List<PropertySourceDTO> findAll() {

        return propertySourceService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertPropertySourceDTO(@RequestBody PropertySourceDTO propertySourceDTO) {

        return propertySourceService.insert(propertySourceDTO);
    }
}
