//package ro.tuc.dsrl.dbapi.service.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import ro.tuc.dsrl.catalyst.model.dto.ActionInstanceDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
//import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
//import ro.tuc.dsrl.dbapi.model.ActionInstance;
//import ro.tuc.dsrl.dbapi.model.ActionType;
//import ro.tuc.dsrl.dbapi.model.OptimizationPlan;
//import ro.tuc.dsrl.dbapi.model.builders.ActionInstanceBuilder;
//import ro.tuc.dsrl.dbapi.repository.ActionInstanceRepository;
//import ro.tuc.dsrl.dbapi.repository.ActionTypeRepository;
//import ro.tuc.dsrl.dbapi.repository.OptimizationPlanRepository;
//import ro.tuc.dsrl.dbapi.service.validators.ActionInstanceValidator;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//import java.util.stream.Collectors;
//
//@Service
//public class ActionInstanceService {
//
//    private final ActionInstanceRepository actionInstanceRepository;
//    private final ActionTypeRepository actionTypeRepository;
//    private final OptimizationPlanRepository optimizationPlanRepository;
//
//    @Autowired
//    public ActionInstanceService(ActionInstanceRepository actionInstanceRepository, ActionTypeRepository actionTypeRepository, OptimizationPlanRepository optimizationPlanRepository) {
//        this.actionInstanceRepository = actionInstanceRepository;
//        this.actionTypeRepository = actionTypeRepository;
//        this.optimizationPlanRepository = optimizationPlanRepository;
//    }
//
//
//    public List<ActionInstanceDTO> findAll() {
//
//        List<ActionInstance> actionInstances = actionInstanceRepository.findAll();
//
//        return actionInstances.stream()
//                .map(ai -> ActionInstanceBuilder.generateDTOFromEntity(ai, ai.getActionType(), ai.getOptimizationPlan()))
//                .collect(Collectors.toList());
//    }
//
//    public UUID insert(ActionInstanceDTO actionInstanceDTO) {
//
//        ActionInstanceValidator.validateInsert(actionInstanceDTO);
//
//        List<String> errors = new ArrayList<>();
//        ActionType actionType = actionTypeRepository.findByUUID(actionInstanceDTO.getActionTypeId());
//        OptimizationPlan optimizationPlan = optimizationPlanRepository.findByUUID(actionInstanceDTO.getOptimizationPlanId());
//        if (actionType == null) {
//            errors.add("Action type was not found in database.");
//        }
//
//        if (optimizationPlan == null) {
//            errors.add("Optimization plan was not found in database.");
//        }
//
//        if (!errors.isEmpty()) {
//            throw new EntityValidationException(ActionInstance.class.getSimpleName(), errors);
//        }
//
//        return actionInstanceRepository.save(ActionInstanceBuilder.generateEntityFromDTO(actionInstanceDTO, actionType, optimizationPlan)).getId();
//    }
//
//    public ActionInstanceDTO findById(UUID actionInstanceUUID) {
//
//        ActionInstance actionInstance = actionInstanceRepository.findByUUID(actionInstanceUUID);
//        if (actionInstance == null) {
//            throw new ResourceNotFoundException("ActionInstance with ID " + actionInstanceUUID);
//        }
//
//        return ActionInstanceBuilder.generateDTOFromEntity(actionInstance, actionInstance.getActionType(), actionInstance.getOptimizationPlan());
//
//    }
//}
