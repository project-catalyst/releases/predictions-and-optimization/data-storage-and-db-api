package ro.tuc.dsrl.dbapi.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/measure")
public class DCComponentInformationController {

    //region GET
    @GetMapping(value = "/{dataCenterID}/entireDC")
    public void findEntireDCInformation(@PathVariable("dataCenterID") String dataCenterID) {

        // TODO
    }

    @GetMapping(value = "/{dataCenterID}/itComponent")
    public void findITComponentInformation(@PathVariable("dataCenterID") String dataCenterID) {

        // TODO
    }

    @GetMapping(value = "/{dataCenterID}/rack/{rackID}")
    public void findRackInformation(@PathVariable("dataCenterID") String dataCenterID,
                                    @PathVariable("rackID") String rackID) {

        // TODO
    }

    @GetMapping(value = "/{dataCenterID}/server/{serverID}")
    public void findServerInformation(@PathVariable("dataCenterID") String dataCenterID,
                                      @PathVariable("serverID") String serverID) {

        // TODO
    }

    @GetMapping(value = "/{dataCenterID}/vm/{vmID}")
    public void findVMInformation(@PathVariable("dataCenterID") String dataCenterID,
                                  @PathVariable("vmID") String vmID) {

        // TODO
    }

    @GetMapping(value = "/{dataCenterID}/cooling-system/{coolingSystemID}")
    public void findCoolingSystemInformation(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("coolingSystemID") String coolingSystemID) {

        // TODO
    }

    @GetMapping(value = "/{dataCenterID}/battery/{batteryID}")
    public void findBatteryInformation(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("batteryID") String vmID) {

        // TODO
    }

    @GetMapping(value = "/{dataCenterID}/heat-pump/{heatPumpID}")
    public void findHeatPumpInformation(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("heatPumpID") String heatPumpID) {

        // TODO
    }
    //endregion


    //region Insertions
    @PostMapping(value = "/{dataCenterID}/entireDC")
    public void insertEntireDCInformation(@PathVariable("dataCenterID") String dataCenterID) {

        // TODO
    }

    @PostMapping(value = "/{dataCenterID}/itComponent")
    public void insertITComponentInformation(@PathVariable("dataCenterID") String dataCenterID) {

        // TODO
    }

    @PostMapping(value = "/{dataCenterID}/rack/{rackID}")
    public void insertRackInformation(@PathVariable("dataCenterID") String dataCenterID,
                                      @PathVariable("rackID") String rackID) {

        // TODO
    }

    @PostMapping(value = "/{dataCenterID}/server/{serverID}")
    public void insertServerInformation(@PathVariable("dataCenterID") String dataCenterID,
                                        @PathVariable("serverID") String serverID) {

        // TODO
    }

    @PostMapping(value = "/{dataCenterID}/vm/{vmID}")
    public void insertVMInformation(@PathVariable("dataCenterID") String dataCenterID,
                                    @PathVariable("vmID") String vmID) {

        // TODO
    }

    @PostMapping(value = "/{dataCenterID}/cooling-system/{coolingSystemID}")
    public void insertCoolingSystemInformation(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("coolingSystemID") String coolingSystemID) {

        // TODO
    }

    @PostMapping(value = "/{dataCenterID}/battery/{batteryID}")
    public void insertBatteryInformation(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("batteryID") String vmID) {

        // TODO
    }

    @PostMapping(value = "/{dataCenterID}/heat-pump/{heatPumpID}")
    public void insertHeatPumpInformation(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("heatPumpID") String heatPumpID) {

        // TODO
    }
    //endregion

}
