package ro.tuc.dsrl.dbapi.exception_handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.error_handler.ExceptionHandlerResponseDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    protected ResponseEntity<Object> handleResourceNotFound(RuntimeException ex,
                                                            WebRequest request) {
        if (!(ex instanceof ResourceNotFoundException)) {
            return handleExceptionInternal(
                    ex, null,
                    new HttpHeaders(),
                    HttpStatus.CONFLICT,
                    request
            );
        }

        ResourceNotFoundException customEx = (ResourceNotFoundException) ex;
        List<String> details = new ArrayList<>();
        details.add("The requested " + customEx.getResource() + " was not found!");
        HttpStatus status = HttpStatus.NOT_FOUND;
        ExceptionHandlerResponseDTO errorInformation = new ExceptionHandlerResponseDTO(customEx.getResource(),
                status.getReasonPhrase(),
                status.value(),
                ex.getMessage(),
                details,
                request.getDescription(false));
        return handleExceptionInternal(
                ex,
                errorInformation,
                new HttpHeaders(),
                status,
                request
        );
    }

    @ExceptionHandler(value = {EntityValidationException.class})
    protected ResponseEntity<Object> handleEntityValidationExceptionConflict(RuntimeException ex, WebRequest request) {
        if (!(ex instanceof EntityValidationException)) {
            return handleExceptionInternal(
                    ex, null,
                    new HttpHeaders(),
                    HttpStatus.CONFLICT,
                    request
            );
        }
        HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;
        EntityValidationException customEx = (EntityValidationException) ex;
        List<String> details = ((EntityValidationException) ex).getValidationErrors();
        ExceptionHandlerResponseDTO error = new ExceptionHandlerResponseDTO(customEx.getResource(), status.getReasonPhrase(), status.value(), ex.getMessage(), details, request.getDescription(false));
        return handleExceptionInternal(
                ex,
                error,
                new HttpHeaders(),
                status,
                request
        );
    }

    @ExceptionHandler(value = {IncorrectParameterException.class})
    protected ResponseEntity<Object> handleIncorrectParametersExceptionConflict(RuntimeException ex, WebRequest request) {
        if (!(ex instanceof IncorrectParameterException)) {
            return handleExceptionInternal(
                    ex, null,
                    new HttpHeaders(),
                    HttpStatus.CONFLICT,
                    request
            );
        }
        IncorrectParameterException customEx = (IncorrectParameterException) ex;
        List<String> details = customEx.getInvalidParams();
        HttpStatus status = HttpStatus.PRECONDITION_FAILED;
        ExceptionHandlerResponseDTO error = new ExceptionHandlerResponseDTO(customEx.getResource(), status.getReasonPhrase(),
                status.value(), ex.getMessage(), details, request.getDescription(false));
        return handleExceptionInternal(
                ex,
                error,
                new HttpHeaders(),
                status,
                request
        );
    }


}
