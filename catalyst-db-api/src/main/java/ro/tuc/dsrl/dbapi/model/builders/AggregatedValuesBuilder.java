package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.dbapi.model.dto.AggregatedActionValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;

import java.util.ArrayList;
import java.util.List;

public class AggregatedValuesBuilder {

    private AggregatedValuesBuilder() {
    }

    public static List<AggregatedValuesDTO> generateMonitoredValuesDTOs(Object[][] tableRowValues) {
        List<AggregatedValuesDTO> values = new ArrayList<>();
        for (Object[] row : tableRowValues) {
            AggregatedValuesDTO valueDTO = new AggregatedValuesDTO();
            valueDTO.setValue(((Double) row[0]));
            valueDTO.setYear(((Number) row[1]).intValue());
            valueDTO.setMonth(((Number) row[2]).intValue());
            valueDTO.setDay(((Number) row[3]).intValue());
            valueDTO.setHour(((Number) row[4]).intValue());
            valueDTO.setMinuteSlots(((Number) row[5]).intValue());
            values.add(valueDTO);
        }
        return values;
    }


    public static List<AggregatedValuesDTO> generateMonitoredValuesAggregatedByYearDTOs(Object[][] tableRowValues) {
        List<AggregatedValuesDTO> values = new ArrayList<>();
        for (Object[] row : tableRowValues) {
            AggregatedValuesDTO valueDTO = new AggregatedValuesDTO();
            valueDTO.setValue(((Double) row[0]));
            valueDTO.setYear(((Number) row[1]).intValue());
            valueDTO.setHour(((Number) row[2]).intValue());
            valueDTO.setMinuteSlots(((Number) row[3]).intValue());
            values.add(valueDTO);
        }
        return values;
    }


    public static List<AggregatedActionValuesDTO> generateActionValuesDTOs(Object[][] tableRowValues) {
        List<AggregatedActionValuesDTO> values = new ArrayList<>();
        for (Object[] row : tableRowValues) {
            AggregatedActionValuesDTO valueDTO = new AggregatedActionValuesDTO();
            valueDTO.setValue(((Double) row[0]));
            valueDTO.setThermalValue(((Double) row[1]));
            valueDTO.setYear(((Number) row[2]).intValue());
            valueDTO.setMonth(((Number) row[3]).intValue());
            valueDTO.setDay(((Number) row[4]).intValue());
            valueDTO.setHour(((Number) row[5]).intValue());
            valueDTO.setMinuteSlots(((Number) row[6]).intValue());
            values.add(valueDTO);
        }
        return values;
    }
}
