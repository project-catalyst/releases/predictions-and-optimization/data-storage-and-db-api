package ro.tuc.dsrl.dbapi.model.dto;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;

import java.util.List;

public class ItLoadBalancerActions {

    List<OptimizationActionDTO> sdtwFromCurrentHour;

    List<OptimizationActionDTO> activeActions;

    public ItLoadBalancerActions() {
    }

    public ItLoadBalancerActions(List<OptimizationActionDTO> moveFromCurrentHour, List<OptimizationActionDTO> activeActions) {
        this.sdtwFromCurrentHour = moveFromCurrentHour;
        this.activeActions = activeActions;
    }

    public List<OptimizationActionDTO> getSdtwFromCurrentHour() {
        return sdtwFromCurrentHour;
    }

    public void setSdtwFromCurrentHour(List<OptimizationActionDTO> sdtwFromCurrentHour) {
        this.sdtwFromCurrentHour = sdtwFromCurrentHour;
    }

    public List<OptimizationActionDTO> getActiveActions() {
        return activeActions;
    }

    public void setActiveActions(List<OptimizationActionDTO> activeActions) {
        this.activeActions = activeActions;
    }
}
