package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationPlanDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class OptimizationPlanValidator {

    private static final Log LOGGER = LogFactory.getLog(OptimizationPlanValidator.class);

    private OptimizationPlanValidator() {
    }

    public static void validateInsert(OptimizationPlanDTO optimizationPlanDTO) {

        List<String> errors = new ArrayList<>();
        if (optimizationPlanDTO == null) {
            errors.add("Optimization Plan DTO is null");
            throw new IncorrectParameterException(OptimizationPlanValidator.class.getSimpleName(), errors);
        }

        if (optimizationPlanDTO.getId() == null) {
            errors.add("Optimization Plan ID is null");
        }

        if (optimizationPlanDTO.getTimeframe() == null) {
            errors.add("Optimization Plan Timeframe is null or empty");
        }

        if (optimizationPlanDTO.getStart() == null) {
            errors.add("Optimization Plan Start Time is null");
        }

        if (optimizationPlanDTO.getEnd() == null) {
            errors.add("Optimization Plan End Time is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(OptimizationPlanValidator.class.getSimpleName(), errors);
        }
    }

}
