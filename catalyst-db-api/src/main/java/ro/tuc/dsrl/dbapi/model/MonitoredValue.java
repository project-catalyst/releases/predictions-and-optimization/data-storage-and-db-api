package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "monitored_value")
public class MonitoredValue {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "timestamp")
    @NotNull
    private LocalDateTime timestamp;

    @Column(name = "value")
    @NotNull
    private double value;

    @ManyToOne
    @JoinColumn(name = "device_id")
    @NotNull
    private Device deviceId;

    @ManyToOne
    @JoinColumn(name = "measurement_id")
    @NotNull
    private Measurement measurementId;

    public MonitoredValue() {

    }

    public MonitoredValue(@NotNull LocalDateTime timestamp,
                          @NotNull double value,
                          @NotNull Device deviceId,
                          @NotNull Measurement measurementId) {
        this.timestamp = timestamp;
        this.value = value;
        this.deviceId = deviceId;
        this.measurementId = measurementId;
    }

    public MonitoredValue(@NotNull UUID id,
                          @NotNull LocalDateTime timestamp,
                          @NotNull double value,
                          @NotNull Device deviceId,
                          @NotNull Measurement measurementId) {

        this(timestamp, value, deviceId, measurementId);
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public double getValue() {
        return value;
    }

    public Device getDeviceId() {
        return deviceId;
    }

    public Measurement getMeasurementId() {
        return measurementId;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setDeviceId(Device deviceId) {
        this.deviceId = deviceId;
    }

    public void setMeasurementId(Measurement measurementId) {
        this.measurementId = measurementId;
    }
}
