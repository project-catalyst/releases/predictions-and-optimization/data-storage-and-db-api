package ro.tuc.dsrl.dbapi.controller.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.OptimizationPageDTO;
import ro.tuc.dsrl.dbapi.service.uisupport.OptimizationPageService;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.ProfileFormatter;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
public class OptimizationPageController {
    @Value("${datacenter.name}")
    private String dcName;

    private final OptimizationPageService optPageService;

    @Autowired
    public OptimizationPageController(OptimizationPageService actionValueService) {
        this.optPageService = actionValueService;
    }
    @GetMapping(value = "/optimization/actions/intraday/{hour}/{time}")
    public List<OptimizationActionDTO> getOptimizationActionsID(@PathVariable("time") long time,
                                                                @PathVariable("hour") Integer hour)
    {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time).withHour(hour);
        return optPageService.getActions(currentTime, 0.0, dcName, PredictionGranularity.INTRADAY);

    }


    @GetMapping(value = "/optimization/actions/dayahead/{hour}/{time}")
    public List<OptimizationActionDTO> getOptimizationActionsDA(@PathVariable("time") long time,
                                                                @PathVariable("hour") Integer hour)
    {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time).withHour(hour);
        return optPageService.getActions(currentTime, 0.0, dcName, PredictionGranularity.DAYAHEAD);
    }

    @GetMapping(value = "/optimization/strategies/dayahead/{time}/{confidenceLevel}")
    public FlexibilityStrategyDTO getOptimizationStrategies(@PathVariable("time") long time,
                                                            @PathVariable("confidenceLevel") double conf)
    {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        return optPageService.getPlanStrategies(currentTime, 0.0, dcName, PredictionGranularity.DAYAHEAD);
    }


    @GetMapping(value = "/plan/energy/intraday/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planID(@PathVariable("time") long time,
                                          @PathVariable("confidenceLevel") double conf,
                                          @PathVariable("hour") int hour,
                                          @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getEnergyPlanID(dcName, currentTime,  conf);
    }


    @GetMapping(value = "/plan/energy/dayahead/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planDA(@PathVariable("time") long time,
                                      @PathVariable("confidenceLevel") double conf,
                                      @PathVariable("hour") int hour,
                                      @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getEnergyPlanDA(dcName, currentTime, conf);
    }

    @GetMapping(value = "/plan/renewable/intraday/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planRenID(@PathVariable("time") long time,
                                      @PathVariable("confidenceLevel") double conf,
                                      @PathVariable("hour") int hour,
                                      @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getRenPlanID(dcName, currentTime,  conf);
    }


    @GetMapping(value = "/plan/renewable/dayahead/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planRenDA(@PathVariable("time") long time,
                                      @PathVariable("confidenceLevel") double conf,
                                      @PathVariable("hour") int hour,
                                      @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getRenPlanDA(dcName, currentTime, conf);
    }


    @GetMapping(value = "/plan/thermal/intraday/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planThermalID(@PathVariable("time") long time,
                                      @PathVariable("confidenceLevel") double conf,
                                      @PathVariable("hour") int hour,
                                      @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getThermalPlanID(dcName, currentTime,  conf);
    }


    @GetMapping(value = "/plan/thermal/dayahead/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planThermalDA(@PathVariable("time") long time,
                                      @PathVariable("confidenceLevel") double conf,
                                      @PathVariable("hour") int hour,
                                      @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getThermalPlanDA(dcName, currentTime, conf);
    }

    @GetMapping(value = "/plan/price-driven/intraday/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planPriceDrivenID(@PathVariable("time") long time,
                                      @PathVariable("confidenceLevel") double conf,
                                      @PathVariable("hour") int hour,
                                      @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getPriceDrivenPlanID(dcName, currentTime,  conf);
    }


    @GetMapping(value = "/plan/price-driven/dayahead/{time}/{confidenceLevel}/{hour},{min}")
    public OptimizationPageDTO planPriceDrivenDA(@PathVariable("time") long time,
                                      @PathVariable("confidenceLevel") double conf,
                                      @PathVariable("hour") int hour,
                                      @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return optPageService.getPriceDrivenPlanDA(dcName, currentTime, conf);
    }
}
