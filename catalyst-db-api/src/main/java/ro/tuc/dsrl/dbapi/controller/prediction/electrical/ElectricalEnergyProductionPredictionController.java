package ro.tuc.dsrl.dbapi.controller.prediction.electrical;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/electrical/prediction/production")
public class ElectricalEnergyProductionPredictionController {

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    private final PredictedValueService predictedValueService;
    @Value( "${datacenter.name}" )
    private String datacenterName;

    @Autowired
    public ElectricalEnergyProductionPredictionController(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService,
                                                          PredictedValueService predictedValueService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
        this.predictedValueService = predictedValueService;
    }

    //region Get
    @GetMapping(value = "/intraday/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfIntraDayProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType(),
                FlexibilityType.NONE.getType());

    }

    @GetMapping(value = "/dayahead/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfDayAheadProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType(),
                FlexibilityType.NONE.getType());

    }

//    @GetMapping(value = "energy-production-curve/forecasted/day/{startdate}")
//    public EnergyProfileDTO findElectricalPredictionOfDayAheadProductionForEntireDCInInterval(
//            @PathVariable("startdate") Long startTime) {
//
//        List<String> errors = EnergyProfilePreconditions.validate(Datacenter.DATACENTER_ID.getDatacenterName(), startTime);
//        EnergyProfilePreconditions.throwIncorrectParameterException(errors);
//
//        UUID dataCenterUUID = UUID.fromString(Datacenter.DATACENTER_ID.getDatacenterName());
//
//        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
//
//        return energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
//                dataCenterUUID,
//                startLocalDateTime,
//                PredictionGranularity.DAYAHEAD,
//                EnergyType.ELECTRICAL.name(),
//                PredictionType.ENERGY_PRODUCTION.getType(),
//                FlexibilityType.NONE.getType());
//
//    }

    @GetMapping(value = "/nearRealTime/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfNearRealtimeProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType(),
                FlexibilityType.NONE.getType());
    }
    //endregion

    @PostMapping(value = "/{dataCenterID}/entireDC")
    public void insertElectricalProductionPredictionForEntireDC(@PathVariable("dataCenterID") String dataCenterID,
                                                                @RequestBody PredictedValueDTO predictedValueDTO) {

        UUIDPreconditions.validate(dataCenterID);

        predictedValueService.insert(predictedValueDTO);
    }
}
