package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.UserDTO;
import ro.tuc.dsrl.dbapi.model.User;

public class UserBuilder {

    private static final String DEFAULT_ROLE = "ADMIN";

    private UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User user) {
        return new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                DEFAULT_ROLE
        );
    }
}
