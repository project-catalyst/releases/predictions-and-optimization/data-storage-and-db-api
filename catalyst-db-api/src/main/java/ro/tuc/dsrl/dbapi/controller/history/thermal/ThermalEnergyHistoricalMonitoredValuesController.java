package ro.tuc.dsrl.dbapi.controller.history.thermal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.service.history.EnergyHistoricalMonitoredValuesService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/thermal/history/monitored-values/production")
public class ThermalEnergyHistoricalMonitoredValuesController {

    /**
     * Thermal Energy will treat only production monitored values
     */

    private final EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService;

    @Autowired
    public ThermalEnergyHistoricalMonitoredValuesController(EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService) {
        this.energyHistoricalMonitoredValuesService = energyHistoricalMonitoredValuesService;
    }

    //region Production
    //region IntraDay Get Methods
    @GetMapping(value = "/4hoursbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfIntraDayProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/4hoursbefore/{dataCenterID}/itComponent/{startTime}")
    public List<EnergyProfileDTO> findThermalHistoricalMonitoredValuesOfIntraDayProductionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForITComponent(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/4hoursbefore/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfIntraDayProductionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForCoolingSystem(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/4hoursbefore/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfIntraDayProductionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForServer(
                UUID.fromString(dataCenterID),
                UUID.fromString(serverID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }
    //endregion


    //region DayAhead Get Methods
    @GetMapping(value = "/24hoursbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfDayAheadProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/itComponent/{startTime}")
    public List<EnergyProfileDTO> findThermalHistoricalMonitoredValuesOfDayAheadProductionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForITComponent(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfDayAheadProductionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForCoolingSystem(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfDayAheadProductionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForServer(
                UUID.fromString(dataCenterID),
                UUID.fromString(serverID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }
    //endregion


    //region NearRealTime Get Methods
    @GetMapping(value = "/1hourbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfNearRealtimeProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.THERMAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/1hourbefore/{dataCenterID}/itComponent/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfNearRealtimeProductionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForITComponent(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.THERMAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/1hourbefore/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfNearRealtimeProductionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForCoolingSystem(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/1hourbefore/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findThermalHistoricalMonitoredValuesOfNearRealtimeProductionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForServer(
                UUID.fromString(dataCenterID),
                UUID.fromString(serverID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.THERMAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());

    }
    //endregion
    //endregion
}
