package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.PropertySource;

import java.util.UUID;

@Repository
public interface PropertySourceRepository extends JpaRepository<PropertySource, UUID> {
    @Query(value = "SELECT e " +
            "FROM PropertySource e " +
            "WHERE e.id = :uuid")
    PropertySource findByUUId(@Param("uuid") UUID uuid);
}
