package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.TotalExecutionResultsPreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.MeasurementService;
import ro.tuc.dsrl.dbapi.service.basic.MonitoredValueService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/monitored-value")
public class MonitoredValueController {

    private final MonitoredValueService monitoredValueService;
    private final MeasurementService measurementService;
    private final DeviceService deviceService;


    @Autowired
    public MonitoredValueController(MonitoredValueService monitoredValuesService, MeasurementService measurementService, DeviceService deviceService) {
        this.monitoredValueService = monitoredValuesService;
        this.measurementService = measurementService;
        this.deviceService = deviceService;
    }

    @GetMapping(value = "/byId/{monitoredValueId}")
    public MonitoredValueDTO findById(@PathVariable("monitoredValueId") String monitoredValueId) {

        UUIDPreconditions.validate(monitoredValueId);

        UUID monitoredValueUUID = UUID.fromString(monitoredValueId);

        return monitoredValueService.findById(monitoredValueUUID);
    }

    @GetMapping(value = "/all")
    public List<MonitoredValueDTO> findAll() {

        return monitoredValueService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertMonitoredValueDTO(@RequestBody MonitoredValueDTO monitoredValueDTO) {
        return monitoredValueService.insert(monitoredValueDTO);
    }

    @GetMapping(value = "/property/{dc-name}/{startTime}/{endTime}")
    public List<Double> getSecificProperty(@PathVariable("dc-name") String datacenterName,
                                           @PathVariable("startTime") Long startTime,
                                           @PathVariable("endTime") Long endTime){
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return monitoredValueService.getPropertyCurve(
                datacenterName,
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.REALTIME_INTRADAY_FRAME,
                MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION);
    }

    /**
     * MOVED from TotalExecutionController
     */
    @GetMapping(value =  "/current/{date}/{dc-name}")
    public DatacenterStateDTO getLatestMonitoring(@PathVariable("date") Long currentDateMillis,
                                                  @PathVariable("dc-name") String datacenterName){

        TotalExecutionResultsPreconditions.validateCurrentDateAndDCNameThrowErrors(currentDateMillis, datacenterName);

        LocalDateTime current = DateUtils.millisToUTCLocalDateTime(currentDateMillis);

        return this.monitoredValueService.getLatestMonitoring(datacenterName, current);

    }


    @GetMapping(value =  "/pre-opt/current/{date}/{dc-name}")
    public DatacenterStateDTO getLatestMonitoringPreOpt(@PathVariable("date") Long currentDateMillis,
                                                        @PathVariable("dc-name") String datacenterName){

        TotalExecutionResultsPreconditions.validateCurrentDateAndDCNameThrowErrors(currentDateMillis, datacenterName);

        LocalDateTime current = DateUtils.millisToUTCLocalDateTime(currentDateMillis);

        return this.monitoredValueService.getLatestMonitoringPreOpt(datacenterName, current);

    }

    /**
     * MOVED from TotalExecutionController
     */
    @PostMapping(value =  "/insert-snapshot/{dc-name}")
    public void insertTotalExecutionResults(@RequestBody DatacenterStateDTO newResults,
                                            @PathVariable("dc-name") String datacenterName){

        TotalExecutionResultsPreconditions.validateTotalExecutionResultsThrowErrors(newResults);

       this.monitoredValueService.insertDatacenterStat(newResults, datacenterName);

    }

}
