package ro.tuc.dsrl.dbapi.model.dto.resources;

public class LossFactors {
    private double chargeLossFactor;
    private double dischargeLossFactor;
    private double loadState;

    public LossFactors(double chargeLossFactor, double dischargeLossFactor, double loadState) {
        this.chargeLossFactor = chargeLossFactor;
        this.dischargeLossFactor = dischargeLossFactor;
        this.loadState = loadState;
    }

    public double getChargeLossFactor() {
        return chargeLossFactor;
    }

    public void setChargeLossFactor(double chargeLossFactor) {
        this.chargeLossFactor = chargeLossFactor;
    }

    public double getDischargeLossFactor() {
        return dischargeLossFactor;
    }

    public void setDischargeLossFactor(double dischargeLossFactor) {
        this.dischargeLossFactor = dischargeLossFactor;
    }

    public double getLoadState() {
        return loadState;
    }

    public void setLoadState(double loadState) {
        this.loadState = loadState;
    }
}
