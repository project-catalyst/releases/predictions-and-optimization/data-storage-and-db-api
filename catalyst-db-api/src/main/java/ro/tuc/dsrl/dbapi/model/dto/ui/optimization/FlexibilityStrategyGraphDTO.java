package ro.tuc.dsrl.dbapi.model.dto.ui.optimization;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class FlexibilityStrategyGraphDTO {

    private List<Double> drSig;
    private List<Double> heat;
    private List<Double> ePrice;
    private List<Double> lPrice;
    private List<Double> renewableEnergy;

    private int length = 0;

    public FlexibilityStrategyGraphDTO(int length) {
        this.length = length;
    }

    public List<Double> getDrSig() {
        return drSig;
    }

    public void setDrSig(List<Double> drSig) {
        assertTrue("drSig " + drSig.size(), drSig.size() == length);
        this.drSig = drSig;
    }

    public List<Double> getHeat() {
        return heat;
    }

    public void setHeat(List<Double> heat) {
        assertTrue("heat " + heat.size(), heat.size() == length);
        this.heat = heat;
    }

    public List<Double> getePrice() {
        return ePrice;
    }

    public void setePrice(List<Double> ePrice) {
        assertTrue("ePrice " + ePrice.size(), ePrice.size() == length);
        this.ePrice = ePrice;
    }

    public List<Double> getlPrice() {
        return lPrice;
    }

    public void setlPrice(List<Double> lPrice) {
        assertTrue("lPrice " + lPrice.size(), lPrice.size() == length);
        this.lPrice = lPrice;
    }

    public List<Double> getRenewableEnergy() {
        return renewableEnergy;
    }

    public void setRenewableEnergy(List<Double> renewableEnergy) {
        assertTrue("renewableEnergy " + renewableEnergy.size(), renewableEnergy.size() == length);
        this.renewableEnergy = renewableEnergy;
    }
}
