//package ro.tuc.dsrl.dbapi.controller.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ro.tuc.dsrl.catalyst.model.dto.ActionValueDTO;
//import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
//import ro.tuc.dsrl.dbapi.service.basic.ActionValueService;
//
//import java.util.List;
//import java.util.UUID;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/action-value")
//public class ActionValueController {
//
//    private final ActionValueService actionValueService;
//
//    @Autowired
//    public ActionValueController(ActionValueService actionValueService) {
//        this.actionValueService = actionValueService;
//    }
//
//    @GetMapping(value = "/byId/{actionValueId}")
//    public ActionValueDTO findById(@PathVariable("actionValueId") String actionValueId) {
//
//        UUIDPreconditions.validate(actionValueId);
//
//        UUID actionValueUUID = UUID.fromString(actionValueId);
//
//        return actionValueService.findById(actionValueUUID);
//    }
//
//    @GetMapping(value = "/all")
//    public List<ActionValueDTO> findAll() {
//
//        return actionValueService.findAll();
//    }
//
//    @PostMapping(value = "/insert")
//    public UUID insertActionValueDTO(@RequestBody ActionValueDTO actionValueDTO) {
//
//        return actionValueService.insert(actionValueDTO);
//    }
//}
