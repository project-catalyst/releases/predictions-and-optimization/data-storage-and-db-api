package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.Measurement;

import java.util.UUID;

@Repository
public interface MeasurementRepository extends JpaRepository<Measurement, UUID> {

    @Query(value = "SELECT m.id FROM Measurement m WHERE m.property = :property")
    UUID findByProperty(@Param("property") String property);

    @Query(value = "SELECT m FROM Measurement m WHERE m.property = :property")
    Measurement findMeasurementByProperty(@Param("property") String property);

    @Query(value = "SELECT e " +
            "FROM Measurement e " +
            "WHERE e.id = :measurementId")
    Measurement findByUUID(@Param("measurementId") UUID measurementId);

    @Query(value = "SELECT m " +
            "FROM Measurement m " +
            "WHERE m.deviceTypeId.id = :device_type_id AND " +
            "m.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "m.property LIKE UPPER(CONCAT('%', :predictionType, '%'))")
    Measurement findMeasurementByDeviceId(@Param("device_type_id") UUID device_type_id,
                                          @Param("energyType") String energyType,
                                          @Param("predictionType") String predictionType);
}
