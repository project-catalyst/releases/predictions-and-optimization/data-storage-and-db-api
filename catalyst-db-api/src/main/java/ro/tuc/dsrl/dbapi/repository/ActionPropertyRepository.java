//package ro.tuc.dsrl.dbapi.repository;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//import ro.tuc.dsrl.dbapi.model.ActionProperty;
//
//import java.util.UUID;
//
//@Repository
//public interface ActionPropertyRepository extends JpaRepository<ActionProperty, UUID> {
//
//    @Query(value = "SELECT a FROM ActionProperty a where a.id = :id")
//    ActionProperty findByUUID(@Param("id") UUID actionPropertyUUID);
//
//    @Query(value = "SELECT a FROM ActionProperty a where a.property = :actionPropertyName")
//    ActionProperty findByName(@Param("actionPropertyName") String actionPropertyName);
//}
