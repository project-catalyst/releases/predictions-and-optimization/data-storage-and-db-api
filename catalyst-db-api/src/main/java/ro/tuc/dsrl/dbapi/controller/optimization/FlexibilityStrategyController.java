package ro.tuc.dsrl.dbapi.controller.optimization;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.service.basic.FlexibilityStrategyService;

@RestController
@CrossOrigin
@RequestMapping(value = "/strategies")
public class FlexibilityStrategyController {
    @Value("${datacenter.name}")
    private String dcName;

    private final FlexibilityStrategyService flexibilityStrategyService;

    public FlexibilityStrategyController(FlexibilityStrategyService flexibilityStrategyService) {
        this.flexibilityStrategyService = flexibilityStrategyService;
    }

    @PostMapping("/insert")
    public FlexibilityStrategyDTO insertStrategy(@RequestBody FlexibilityStrategyDTO flexibilityStrategyDTO) {
        flexibilityStrategyDTO.setDatacenter(dcName);
        return flexibilityStrategyService.save(flexibilityStrategyDTO);
    }

    @GetMapping("/{time}")
    public FlexibilityStrategyDTO getStrategy( @PathVariable long time) {
        return flexibilityStrategyService.get(dcName, DateUtils.millisToLocalDateTime(time));
    }
}
