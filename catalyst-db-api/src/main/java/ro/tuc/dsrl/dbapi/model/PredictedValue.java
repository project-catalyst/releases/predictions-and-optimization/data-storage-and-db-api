package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "predicted_value")
public class PredictedValue {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "timestamp")
    @NotNull
    private LocalDateTime timestamp;

    @Column(name = "value")
    @NotNull
    private double value;

    @ManyToOne
    @JoinColumn(name = "measurement_id")
    @NotNull
    private Measurement measurementId;

    @ManyToOne
    @JoinColumn(name = "device_id")
    @NotNull
    private Device deviceId;

    @ManyToOne
    @JoinColumn(name = "prediction_job_id")
    @NotNull
    private PredictionJob predictionJobId;

    public PredictedValue() {

    }

    public PredictedValue(UUID id, @NotNull LocalDateTime timestamp,
                          @NotNull double value,
                          @NotNull Measurement measurementId,
                          @NotNull Device deviceId,
                          @NotNull PredictionJob predictionJobId) {
        this.id = id;
        this.timestamp = timestamp;
        this.value = value;
        this.measurementId = measurementId;
        this.deviceId = deviceId;
        this.predictionJobId = predictionJobId;
    }

    public PredictedValue(@NotNull LocalDateTime timestamp,
                          @NotNull double value,
                          @NotNull Measurement measurementId,
                          @NotNull Device deviceId,
                          @NotNull PredictionJob predictionJobId) {
        this.timestamp = timestamp;
        this.value = value;
        this.measurementId = measurementId;
        this.deviceId = deviceId;
        this.predictionJobId = predictionJobId;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public double getValue() {
        return value;
    }

    public Measurement getMeasurementId() {
        return measurementId;
    }

    public Device getDeviceId() {
        return deviceId;
    }

    public PredictionJob getPredictionJobId() {
        return predictionJobId;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setMeasurementId(Measurement measurementId) {
        this.measurementId = measurementId;
    }

    public void setDeviceId(Device deviceId) {
        this.deviceId = deviceId;
    }

    public void setPredictionJobId(PredictionJob predictionJobId) {
        this.predictionJobId = predictionJobId;
    }
}
