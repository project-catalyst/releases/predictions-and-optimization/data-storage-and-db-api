package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.HeatRecoveryInfrastructure;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.util.ArrayList;
import java.util.List;

public class DeviceValidator {

    private static final Log LOGGER = LogFactory.getLog(DeviceValidator.class);

    private DeviceValidator() {

    }

    public static void validateInsert(DeviceDTO deviceDTO) {

        List<String> errors = new ArrayList<>();
        if (deviceDTO == null) {
            errors.add("Device DTO is null");
            throw new IncorrectParameterException(DeviceValidator.class.getSimpleName(), errors);
        }

        if (deviceDTO.getId() == null) {
            errors.add("Device ID is null");
        }

        if (deviceDTO.getLabel() == null || deviceDTO.getLabel().isEmpty()) {
            errors.add("Device label is null or empty");
        }

        if (deviceDTO.getDeviceTypeId() == null) {
            errors.add("Device device type is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DeviceValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(EnergyStorageComponent energyStorageComponent) {
        List<String> errors = new ArrayList<>();
        if (energyStorageComponent == null) {
            errors.add("Energy Storage must not be null");
            throw new IncorrectParameterException(EnergyStorageComponent.class.getSimpleName(), errors);
        }

        if (energyStorageComponent.getActualLoadedCapacity() < 0 || energyStorageComponent.getChargeLossRate() < 0  ||
                energyStorageComponent.getDischargeLossRate() < 0 || energyStorageComponent.getEnergyLossRate() < 0 ||
                energyStorageComponent.getMaxChargeRate() < 0 || energyStorageComponent.getMaxDischargeRate() < 0 ||
                energyStorageComponent.getMaximumCapacity() < 0) {
            errors.add("Energy Storage measurements can not contains negative values");
        }
        if(energyStorageComponent.getChargeLossRate() < 0  || energyStorageComponent.getChargeLossRate() >1 ||
                energyStorageComponent.getDischargeLossRate() < 0 || energyStorageComponent.getDischargeLossRate() >1 ){
            errors.add("Energy Storage loss-factor fields should have values between: 0.0 and 1.0 ");
        }

        if (energyStorageComponent.getDeviceLabel() == null || energyStorageComponent.getDeviceLabel().equals("")) {
            errors.add("Device label is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
             throw new IncorrectParameterException(DeviceValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(Battery deviceDTO) {

        List<String> errors = new ArrayList<>();
        if (deviceDTO == null) {
            errors.add("Battery must not be null");
            throw new IncorrectParameterException(Battery.class.getSimpleName(), errors);
        }

        validateInsert((EnergyStorageComponent) deviceDTO);

        if (deviceDTO.getDod() < 0 || deviceDTO.getEsdFactor() < 0) {
            errors.add("Battery measurements can not contain negative values");
        }
        if(deviceDTO.getEsdFactor()< 0  || deviceDTO.getEsdFactor() >1 ){
            errors.add("Battery loss-factor fields should have values between: 0.0 and 1.0 ");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(Battery.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(ThermalEnergyStorage thermalEnergyStorage) {
        List<String> errors = new ArrayList<>();
        if (thermalEnergyStorage == null) {
            errors.add("Thermal Energy Storage must not be null");
            throw new IncorrectParameterException(ThermalEnergyStorage.class.getSimpleName(), errors);
        }

        validateInsert((EnergyStorageComponent) thermalEnergyStorage);

        if (thermalEnergyStorage.getSpecificHeat() < 0 || thermalEnergyStorage.getTesFactor() < 0) {
            errors.add("Thermal energy storage measurements can not be negative values");
        }

        if(thermalEnergyStorage.getTesFactor()< 0  ||thermalEnergyStorage.getTesFactor() >1 ){
            errors.add("Thermal energy storage loss-factor fields should have values between: 0.0 and 1.0 ");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(ThermalEnergyStorage.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(ServerRoom serverRoom) {
        List<String> errors = new ArrayList<>();
        if (serverRoom == null) {
            errors.add("Server room must not be null");
            throw new IncorrectParameterException(ServerRoom.class.getSimpleName(), errors);
        }

        if (serverRoom.getEdPercentage() < 0 || serverRoom.getMaxEnergyConsumption() < 0 ||
                serverRoom.getMaxHostLoad() < 0 || serverRoom.getMaxReallocLoad() < 0) {
            errors.add("Server room parameters can not be negative");
        }

        if (serverRoom.getDeviceLabel() == null || serverRoom.getDeviceLabel().isEmpty()) {
            errors.add("Server room device label is null or empty");
        }
        if(serverRoom.getEdPercentage()< 0  || serverRoom.getEdPercentage() >1 ){
            errors.add("ServerRoom delay percentage fields should have values between: 0.0 and 1.0 ");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(ServerRoom.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(CoolingSystem coolingSystem) {
        List<String> errors = new ArrayList<>();
        if (coolingSystem == null) {
            errors.add("Cooling system must not be null");
            throw new IncorrectParameterException(CoolingSystem.class.getSimpleName(), errors);
        }

        if (coolingSystem.getCoolingCapacityKWh() < 0 || coolingSystem.getCopC() < 0 || coolingSystem.getCopH() < 0 ||
                 coolingSystem.getMaxCoolingLoadKWh() < 0  ) {
            errors.add("Cooling system parameters can not be negative");
        }

        if (coolingSystem.getDeviceLabel() == null || coolingSystem.getDeviceLabel().isEmpty()) {
            errors.add("Cooling system device label is null or empty");
        }


        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(CoolingSystem.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(HeatRecoveryInfrastructure hri) {
        List<String> errors = new ArrayList<>();
        if (hri == null) {
            errors.add("Heat Recovery instance must not be null");
            throw new IncorrectParameterException(CoolingSystem.class.getSimpleName(), errors);
        }

        if (hri.getCopH() < 0 || hri.getHeatCapacityKWh() < 0 || hri.getMaxHeatLoadKWh() < 0 ){
            errors.add("Cooling system parameters can not be negative");
        }

        if (hri.getDeviceLabel() == null || hri.getDeviceLabel().isEmpty()) {
            errors.add("Heat Recovery device label is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(HeatRecoveryInfrastructure.class.getSimpleName(), errors);
        }
    }

    public static void validateInsert(DataCentre dataCentre) {
        List<String> errors = new ArrayList<>();
        if (dataCentre == null) {
            errors.add("Data centre must not be null");
            throw new IncorrectParameterException(DataCentre.class.getSimpleName(), errors);
        }

        if (dataCentre.getLabel() == null || dataCentre.getLabel().isEmpty()) {
            errors.add("Data centre device label is null or empty");
        }


        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DataCentre.class.getSimpleName(), errors);
        }
    }
}
