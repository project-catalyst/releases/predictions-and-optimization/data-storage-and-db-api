package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.dsrl.dbapi.model.OptimizationAction;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface OptimizationActionRepository  extends JpaRepository<OptimizationAction, UUID> {


    @Query(value = " SELECT oa " +
            " FROM OptimizationAction oa " +
            " JOIN oa.optimizationPlan op " +
            " JOIN op.dataCenter dc " +
            " WHERE op.timeframe like :timeframe" +
            " AND dc.label like :dcName "+
            " AND oa.type in (:types)" +
            " AND oa.startTime <= :currentTime " +
            " AND oa.endTime> :currentTime " +
            " AND op.confidenceLevel = :confidenceLevel " )
    List<OptimizationAction> findCurrentRedundantActions(@Param("currentTime") LocalDateTime currentTime,
                                                  @Param("confidenceLevel") double confidenceLevel,
                                                  @Param("types") String[] types,
                                                  @Param("timeframe") String timeframe,
                                                  @Param("dcName") String dcName);

    @Query(value = " SELECT oa " +
            " FROM OptimizationAction oa " +
            " JOIN oa.optimizationPlan op " +
            " JOIN op.dataCenter dc " +
            " WHERE dc.label like :dcName "+
            " AND oa.startTime <= :currentTime " +
            " AND oa.endTime> :currentTime " +
            " AND op.confidenceLevel = :confidenceLevel " )
    List<OptimizationAction> findCurrentActions(@Param("currentTime") LocalDateTime currentTime,
                                                  @Param("confidenceLevel") double confidenceLevel,
                                                  @Param("dcName") String dcName);

    @Query(value = " SELECT oa " +
            " FROM OptimizationAction oa " +
            " JOIN oa.optimizationPlan op " +
            " JOIN op.dataCenter dc " +
            " WHERE dc.label like :dcName "+
            " AND oa.moveFromDate < :endTime " +
            " AND oa.moveFromDate>= :currentTime " +
            " AND oa.type like :oaType" +
            " AND op.confidenceLevel = :confidenceLevel " )
    List<OptimizationAction> findCurrentSDTWActionsMoveFromDate(@Param("currentTime") LocalDateTime currentTime,
                                                                @Param("endTime") LocalDateTime endTime,
                                                @Param("confidenceLevel") double confidenceLevel,
                                                @Param("dcName") String dcName,
                                                @Param("oaType") String type);

    @Query(value = " SELECT oa " +
            " FROM OptimizationAction oa " +
            " JOIN oa.optimizationPlan op " +
            " JOIN op.dataCenter dc " +
            " WHERE dc.label like :dcName "+
            " AND oa.startTime <= :currentTime " +
            " AND oa.endTime> :currentTime " +
            " AND oa.type like :oaType" +
            " AND op.confidenceLevel = :confidenceLevel " )
    List<OptimizationAction> findCurrentActionsByType(@Param("currentTime") LocalDateTime currentTime,
                                                @Param("confidenceLevel") double confidenceLevel,
                                                @Param("dcName") String dcName,
                                                @Param("oaType") String type);


    @Query(value = " SELECT oa " +
            " FROM OptimizationAction oa " +
            " JOIN oa.optimizationPlan op " +
            " JOIN op.dataCenter dc " +
            " WHERE dc.label like :dcName " +
            " AND op.timeframe like :timeframe "+
            " AND oa.startTime >= :startTime " +
            " AND oa.startTime< :endTime " +
            " AND op.confidenceLevel = :confidenceLevel " )
    List<OptimizationAction> findActionsInIntervalAndTimeframe(@Param("startTime") LocalDateTime atartTime,
                                                   @Param("endTime") LocalDateTime endTime,
                                                   @Param("confidenceLevel") double confidenceLevel,
                                                   @Param("dcName") String dcName,
                                                   @Param("timeframe") String timeframe);


    @Query(value = " SELECT oa " +
            " FROM OptimizationAction oa " +
            " JOIN oa.optimizationPlan op " +
            " JOIN op.dataCenter dc " +
            " WHERE dc.label like :dcName " +
            " AND op.timeframe like :timeframe "+
            " AND oa.startTime >= :startTime " +
            " AND oa.startTime< :endTime " +
            " AND oa.type like :oaType" +
            " AND op.confidenceLevel = :confidenceLevel " )
    List<OptimizationAction> findActionByTypeInIntervalAndTimeframe(@Param("startTime") LocalDateTime atartTime,
                                                               @Param("endTime") LocalDateTime endTime,
                                                               @Param("confidenceLevel") double confidenceLevel,
                                                               @Param("dcName") String dcName,
                                                               @Param("timeframe") String timeframe,
                                                               @Param("oaType") String type);



    @Query(value = "SELECT (SUM(ai.amount) * 60/:minutesPerSample),(SUM(ai.thermal_amount) * 60/:minutesPerSample), YEAR(ai.start_date), MONTH(ai.start_date), DAY(ai.start_date), HOUR(ai.start_date),  FLOOR(MINUTE(ai.start_date) /:minutesPerSample )  " +
            "FROM optimization_action ai " +
            "JOIN optimization_plans op ON op.id = ai.optimization_plan_id " +
            "WHERE op.timeframe like :timeframe " +
            "AND ai.type like  :type " +
            "AND ai.start_date >= :start_date " +
            "AND ai.start_date < :end_date " +
            "AND op.confidence_level = :confidenceLevel " +
            "GROUP BY YEAR(ai.start_date),MONTH(ai.start_date), DAY(ai.start_date), HOUR(ai.start_date),  FLOOR(MINUTE(ai.start_date)/:minutesPerSample  ) " +
            "ORDER BY  YEAR(ai.start_date),MONTH(ai.start_date), DAY(ai.start_date), HOUR(ai.start_date),  FLOOR(MINUTE(ai.start_date)/:minutesPerSample )",  nativeQuery = true)
    Object[][] getOptimizationActionValueHourRate(@Param("start_date") LocalDateTime startDate,
                                                  @Param("end_date") LocalDateTime endDate,
                                                  @Param("minutesPerSample") Integer sample,
                                                  @Param("timeframe") String timeframe,
                                                  @Param("type") String type,
                                                  @Param("confidenceLevel") Double confidenceLevel);


//    /**
//     * BE careful! Query specific to SDTW actions
//     */
//
//    @Query(value = "SELECT (AVG(av.value) * 60/:minutesPerSample), YEAR(ai.move_to_date), MONTH(ai.move_to_date), DAY(ai.move_to_date), HOUR(ai.move_to_date),  FLOOR(MINUTE(ai.move_to_date) /:minutesPerSample )  " +
//            "FROM optimization_action ai " +
//            "JOIN optimization_plans op ON op.id = ai.optimization_plan_id " +
//            "WHERE op.timeframe like :timeframe " +
//            "AND ai.type like  :type " +
//            "AND ai.start_time >= :start_date " +
//            "AND ai.start_time < :end_date " +
//            "AND op.confidence_level = :confidenceLevel " +
//            "GROUP BY YEAR(ai.start_time),MONTH(ai.move_to_date), DAY(ai.move_to_date), HOUR(ai.move_to_date),  FLOOR(MINUTE(ai.move_to_date)/:minutesPerSample  ) " +
//            "ORDER BY  YEAR(ai.start_time),MONTH(ai.move_to_date), DAY(ai.move_to_date), HOUR(ai.move_to_date),  FLOOR(MINUTE(ai.move_to_date)/:minutesPerSample )",  nativeQuery = true)
//    Object[][] getSDTWHourRate(@Param("start_date") LocalDateTime startDate,
//                                                  @Param("end_date") LocalDateTime endDate,
//                                                  @Param("minutesPerSample") Integer sample,
//                                                  @Param("timeframe") String timeframe,
//                                                  @Param("type") String type,
//                                                  @Param("confidenceLevel") Double confidenceLevel);


}
