package ro.tuc.dsrl.dbapi.controller.prediction.market;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
import ro.tuc.dsrl.geyser.datamodel.other.MarketPrices;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/market/reference-prices")
public class RefrencePriceController {
    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;

    @Autowired
    public RefrencePriceController(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
    }

    @GetMapping(value = "/dr-incentives/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePricesDR(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {
        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByType(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_DR_PRICE);
    }

    @GetMapping(value = "/dr-incentives/latest/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePricesLatestDR(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {
        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByTypeWithLatestFallback(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_DR_PRICE);
    }

    @GetMapping(value = "/energy/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePricesEnergy(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByType(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_E_PRICE);
    }

    @GetMapping(value = "/energy/latest/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePricesEnergyOrLatest(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByTypeWithLatestFallback(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_E_PRICE);
    }

    @GetMapping(value = "/energy/latest/intraday/{datacenter-name}/{startdate}")
    public List<Double> findIntradayRefrencePricesEnergyOrLatest(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByTypeWithLatestFallback(dcName, startLocalDateTime, PredictionGranularity.INTRADAY, MeasurementType.DC_E_PRICE);
    }

    @GetMapping(value = "/thermal/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePricesThermal(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByType(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_T_PRICE);
    }

    @GetMapping(value = "/thermal/latest/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePricesThermalOrLatest(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByTypeWithLatestFallback(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_T_PRICE);
    }

    @GetMapping(value = "/thermal/latest/intraday/{datacenter-name}/{startdate}")
    public List<Double> findIntraDayRefrencePricesThermalOrLatest(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByTypeWithLatestFallback(dcName, startLocalDateTime, PredictionGranularity.INTRADAY, MeasurementType.DC_T_PRICE);
    }



    @GetMapping(value = "/workload/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePrices(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByType(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_L_PRICE);
    }

    @GetMapping(value = "/workload/latest/dayahead/{datacenter-name}/{startdate}")
    public List<Double> findDayAheadRefrencePricesOrLatest(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByTypeWithLatestFallback(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD, MeasurementType.DC_L_PRICE);
    }

    @GetMapping(value = "/workload/latest/intraday/{datacenter-name}/{startdate}")
    public List<Double> findIntradayRefrencePricesOrLatest(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPricesByTypeWithLatestFallback(dcName, startLocalDateTime, PredictionGranularity.INTRADAY, MeasurementType.DC_L_PRICE);
    }



    @GetMapping(value = "/dayahead/{datacenter-name}/{startdate}")
    public MarketPrices findIntradayRefrencePricesID(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {
        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPrices(dcName, startLocalDateTime, PredictionGranularity.DAYAHEAD);
    }

    @GetMapping(value = "/intraday/{datacenter-name}/{startdate}")
    public MarketPrices findIntradayRefrencePricesDA(
            @PathVariable("datacenter-name") String dcName,
            @PathVariable("startdate") Long startTime) {
        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        return energyHistoricalPredictedValuesService.getMarketPrices(dcName, startLocalDateTime, PredictionGranularity.INTRADAY);
    }

}

