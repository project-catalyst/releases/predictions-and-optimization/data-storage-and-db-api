package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.FlexibilityStrategy;
import ro.tuc.dsrl.dbapi.model.builders.DeviceBuilder;
import ro.tuc.dsrl.dbapi.model.builders.FlexibilityStrategyBuilder;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.FlexibilityStrategyRepository;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;

import java.time.LocalDateTime;

@Service
public class FlexibilityStrategyService {

    private final FlexibilityStrategyRepository flexibilityStrategyRepository;
    private final DeviceRepository deviceRepository;

    public FlexibilityStrategyService(FlexibilityStrategyRepository flexibilityStrategyRepository, DeviceRepository deviceRepository) {
        this.flexibilityStrategyRepository = flexibilityStrategyRepository;
        this.deviceRepository = deviceRepository;
    }

    public FlexibilityStrategyDTO save(FlexibilityStrategyDTO flexibilityStrategies) {
        double sum = flexibilityStrategies.getWe() + flexibilityStrategies.getWf() + flexibilityStrategies.getWt() + flexibilityStrategies.getREN();

        double we = sum > 0.0 ? flexibilityStrategies.getWe() / sum : 0.25;
        double wf = sum > 0.0 ? flexibilityStrategies.getWf() / sum : 0.25;
        double wt = sum > 0.0 ? flexibilityStrategies.getWt() / sum : 0.25;
        double wr = sum > 0.0 ? flexibilityStrategies.getREN() / sum : 0.25;

        flexibilityStrategies.setWe(we);
        //flexibilityStrategies.setWl(wl);
        flexibilityStrategies.setWf(wf);
        flexibilityStrategies.setWt(wt);
        flexibilityStrategies.setREN(wr);

        Device d =  deviceRepository.findDeviceByLabel(flexibilityStrategies.getDatacenter());
        if (d == null) {
            throw new ResourceNotFoundException("DataCenter with name: " + flexibilityStrategies.getDatacenter() + " NOT FOUND!");
        }
        FlexibilityStrategy fsEntity = FlexibilityStrategyBuilder.generateEntityFromDTO(flexibilityStrategies, d);
        fsEntity = flexibilityStrategyRepository.save(fsEntity);

        return FlexibilityStrategyBuilder.generateDTOFromEntity(fsEntity);

    }

    public FlexibilityStrategyDTO get(String datacenter, LocalDateTime time) {
        Device d =  deviceRepository.findDeviceByLabel(datacenter);
        if (d == null) {
            throw new ResourceNotFoundException("DataCenter with name: " + datacenter + " NOT FOUND!");
        }
        FlexibilityStrategy fs =  flexibilityStrategyRepository.findByDatacenterAndTime(d, time);
        if(fs==null){
            return FlexibilityStrategyBuilder.getDefaultStrategies(datacenter);
        }
        return FlexibilityStrategyBuilder.generateDTOFromEntity(fs);
    }
}
