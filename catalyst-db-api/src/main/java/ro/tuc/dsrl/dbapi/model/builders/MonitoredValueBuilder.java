package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;
import ro.tuc.dsrl.dbapi.model.MonitoredValue;

public class MonitoredValueBuilder {

    private MonitoredValueBuilder() {

    }

    public static MonitoredValueDTO generateDTOFromEntity(MonitoredValue monitoredValue,
                                                          Measurement measurement,
                                                          Device device) {
        return new MonitoredValueDTO(
                monitoredValue.getId(),
                monitoredValue.getTimestamp(),
                monitoredValue.getValue(),
                device.getId(),
                measurement.getId()
        );
    }

    public static MonitoredValue generateEntityFromDTO(MonitoredValueDTO monitoredValueDTO,
                                                       Measurement measurement,
                                                       Device device) {
        return new MonitoredValue(
                monitoredValueDTO.getId(),
                monitoredValueDTO.getTimestamp(),
                monitoredValueDTO.getValue(),
                device,
                measurement
        );
    }
}
