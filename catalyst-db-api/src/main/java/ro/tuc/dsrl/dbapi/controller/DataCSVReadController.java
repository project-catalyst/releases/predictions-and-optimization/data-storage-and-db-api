package ro.tuc.dsrl.dbapi.controller;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.MarketDataDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.dto.resources.CopFactors;
import ro.tuc.dsrl.dbapi.service.DataCSVReadService;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.MeasurementService;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.dbapi.service.basic.PredictionJobService;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/data-init")
public class DataCSVReadController {
    private static final Log LOGGER = LogFactory.getLog(DataCSVReadController.class);
    @Value("${datacenter.name}")
    private String datacenterName;
    private static final String BASE_PATH = "data/scenario1";

    private final DataCSVReadService dataCSVReadService;
    private final DeviceService deviceService;
    private final PredictedValueService predictedValueService;
    private final PredictionJobService predictionJobService;
    private final MeasurementService measurementService;

    public DataCSVReadController(DataCSVReadService dataCSVReadService,
                                 DeviceService deviceService, PredictedValueService predictedValueService,
                                 PredictionJobService predictionJobService, MeasurementService measurementService) {
        this.dataCSVReadService = dataCSVReadService;
        this.deviceService = deviceService;
        this.predictedValueService = predictedValueService;
        this.predictionJobService = predictionJobService;
        this.measurementService = measurementService;
    }

    @PostMapping(value = "/da/profiles/{measurement}/{deviceType}/{startdate}")
    public boolean initEstimatedProfiles(
            @PathVariable("startdate") Long startTime, @PathVariable("measurement") String measurement,
            @PathVariable("deviceType") String deviceType,@RequestBody List<Double> estimatedProfile) {

        LocalDateTime dateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime startDate = DateUtils.computeCurrentDayStart(dateTime);

        AggregationGranularity aggregationGranularity = AggregationGranularity.HOUR;
        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), startDate,
                PredictionGranularity.DAYAHEAD.getGranularity(),
                AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
        UUID pjId = predictionJobService.insert(pjDTO);

        UUID measurementUUID = measurementService.getMeasurementByProperty(measurement);

        List<DeviceDTO> devices = deviceService.findByParentAndType(datacenterName, DeviceTypeEnum.setType(deviceType));

        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(AggregationGranularity.HOUR);
        eEnergyPriceProfile.setPredictionGranularity(PredictionGranularity.DAYAHEAD);
        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_CONSUMPTION);
        eEnergyPriceProfile.setMeasurementId(measurementUUID);
        eEnergyPriceProfile.setDeviceId(devices.get(0).getId());

        List<EnergySampleDTO> ePredicted = new ArrayList<>();

        LocalDateTime ldt = startDate;
        for(Double dValue : estimatedProfile ){
            EnergySampleDTO e = new EnergySampleDTO(dValue, ldt);
            ePredicted.add(e);
            ldt = ldt.plus(aggregationGranularity.getGranularity(), ChronoUnit.MINUTES);
        }

        eEnergyPriceProfile.setCurve(ePredicted);

        return predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);
    }


    @GetMapping(value = "/set-total-power/{dc-name}/{time}/{scenarioID}")
    public String setTPS(@PathVariable("dc-name") String dcName, @PathVariable("time") long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/TotalPower.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populatePreOptimizationProfiles(PATH, dcName, dt);
        return "done";
    }

    @GetMapping(value = "/set-prices-da/{dc-name}/{time}/{scenarioID}")
    public String setPricesDA(@PathVariable("dc-name") String dcName, @PathVariable("time") long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/PricesDA.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populatePriceData(PATH, dcName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        return "done";
    }


    @GetMapping(value = "/set-prices-id/{dc-name}/{time}/{scenarioID}")
    public String setPricesID(@PathVariable("dc-name") String dcName, @PathVariable("time") long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/PricesID.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populatePriceData(PATH, dcName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        return "done";
    }

    @GetMapping(value = "/dr-signal-da/{dc-name}/{time}/{scenarioID}")
    public String daDrSignal(@PathVariable("dc-name") String dcName, @PathVariable("time") Long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/dr_signalDA.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populateDRSIGNAL(PATH, dcName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        return "done";
    }

    @GetMapping(value = "/dr-signal-id/{dc-name}/{time}/{scenarioID}")
    public String idDrSignal(@PathVariable("dc-name") String dcName, @PathVariable("time") Long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/dr_signalID.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populateDRSIGNAL(PATH, dcName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        return "done";
    }

    @GetMapping(value = "/set-predicted-da/{dc-name}/{time}/{scenarioID}")
    public String setPredictedDA(@PathVariable("dc-name") String dcName, @PathVariable("time") long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/EnergyPredictionRT-DA.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populatePredictedData(PATH, dcName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);


        final String PATH2 = BASE_PATH + scenarioID + "/EnergyPredictionDT-DA.xls";
        dataCSVReadService.populatePredictedData(PATH2, dcName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);

        return "done";
    }

    @GetMapping(value = "/set-predicted-id/{dc-name}/{time}/{scenarioID}")
    public String setPredictedID(@PathVariable("dc-name") String dcName, @PathVariable("time") long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/EnergyPredictionRT-ID.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populatePredictedData(PATH, dcName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);

        final String PATH2 = BASE_PATH + scenarioID + "/EnergyPredictionDT-ID.xls";
        dataCSVReadService.populatePredictedData(PATH2, dcName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
        return "done";
    }

    @GetMapping(value = "/set-baseline-da/{dc-name}/{time}/{scenarioID}")
    public String setBaselineDA(@PathVariable("dc-name") String dcName, @PathVariable("time") long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/EnergyBaselineDA.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populateBaselineData(PATH, dcName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        return "done";
    }

    @GetMapping(value = "/set-baseline-id/{dc-name}/{time}/{scenarioID}")
    public String setBaselineID(@PathVariable("dc-name") String dcName, @PathVariable("time") long time, @PathVariable("scenarioID") String scenarioID) {
        final String PATH = BASE_PATH + scenarioID + "/EnergyBaselineID.xls";
        LocalDateTime dt = DateUtils.millisToLocalDateTime(time);
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        dataCSVReadService.populateBaselineData(PATH, dcName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        return "done";
    }

    @GetMapping(value = "/populate-all-input-files/{scenarioID}")
    public Map<String, Boolean> populateAll(@PathVariable("scenarioID") int scenarioID) {
        Scenario[] scenarios = Scenario.values();

        if (scenarios.length < scenarioID + 1) {
            throw new ResourceNotFoundException("Scenario");
        }
        Scenario scenario = scenarios[scenarioID];

        final String drSignalDA = scenario.getFileInputData() + "/dr_signalDA.xls";
        final String drSignalID = scenario.getFileInputData() + "/dr_signalID.xls";
        final String setTotalPower = scenario.getFileInputData() + "/PreOptWorkload.xls";
        final String setPricesDA = scenario.getFileInputData() + "/PricesDA.xls";
        final String setPricesID = scenario.getFileInputData() + "/PricesID.xls";

        final String setPredictedRT_DA = scenario.getFileInputData() + "/EnergyPredictionRT-DA.xls";
        final String setPredictedRT_ID = scenario.getFileInputData() + "/EnergyPredictionRT-ID.xls";
        final String setPredictedDT_DA = scenario.getFileInputData() + "/EnergyPredictionDT-DA.xls";
        final String setPredictedDT_ID = scenario.getFileInputData() + "/EnergyPredictionDT-ID.xls";

        final String baselineDA = scenario.getFileInputData() + "/EnergyBaselineDA.xls";
        final String baselineID = scenario.getFileInputData() + "/EnergyBaselineID.xls";

        final String co2baselineDA = scenario.getFileInputData() + "/CO2BaselineDA.xls";
        final String co2baselineID = scenario.getFileInputData() + "/CO2BaselineID.xls";

        final String heatbaselineDA = scenario.getFileInputData() + "/HeatBaselineDA.xls";
        final String heatbaselineID = scenario.getFileInputData() + "/HeatBaselineID.xls";

        LocalDateTime dt = scenario.getTime();
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        LOGGER.info("POPULATE DR SIGNAL INTRADAY");
        boolean drID = dataCSVReadService.populateDRSIGNAL(drSignalID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE DR SIGNAL DAYAHEAD");
        boolean drDA = dataCSVReadService.populateDRSIGNAL(drSignalDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE TOTAL POWER");
        boolean tp = dataCSVReadService.populatePreOptimizationProfiles(setTotalPower, datacenterName, dt);
        LOGGER.info("POPULATE PRICE INTRADAY");
        boolean pricesID = dataCSVReadService.populatePriceData(setPricesID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE PRICE DAYAHEAD");
        boolean pricesDA = dataCSVReadService.populatePriceData(setPricesDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE RT PREDICTED INTRADAY");
        boolean predIDrt = dataCSVReadService.populatePredictedData(setPredictedRT_ID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        LOGGER.info("POPULATE RT PREDICTED DAYAHEAD");
        boolean predDArt = dataCSVReadService.populatePredictedData(setPredictedRT_DA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);

        LOGGER.info("POPULATE DT PREDICTED INTRADAY");
        boolean predIDdt = dataCSVReadService.populatePredictedData(setPredictedDT_ID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
        LOGGER.info("POPULATE DT PREDICTED DAYAHEAD");
        boolean predDAdt = dataCSVReadService.populatePredictedData(setPredictedDT_DA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);


        LOGGER.info("POPULATE BASELINE INTRADAY");
        boolean baseeID = dataCSVReadService.populateBaselineData(baselineID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE BASELINE DAYAHEAD");
        boolean baseDA = dataCSVReadService.populateBaselineData(baselineDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);


        HashMap<String, Boolean> map = new HashMap<>();
        map.put("DR-signal DayAhead", drDA);
        map.put("DR-signal Intraday", drID);

        map.put("TotalPower", tp);
        map.put("Prices Intraday", pricesID);
        map.put("Prices DayAhead", pricesDA);

        map.put("Predicted Intraday rt", predIDrt);
        map.put("Predicted DayAhead rt", predDArt);

       map.put("Predicted Intraday dt", predIDdt);
       map.put("Predicted DayAhead dt", predDAdt);

        map.put("Baseline Intraday", baseeID);
       map.put("Baseline DayAhead", baseDA);

        return map;
    }

    @GetMapping(value = "/populate-all-input-files-ren/{scenarioID}")
    public Map<String, Boolean> populateAllwithREN(@PathVariable("scenarioID") int scenarioID) {
        Scenario[] scenarios = Scenario.values();

        if (scenarios.length < scenarioID + 1) {
            throw new ResourceNotFoundException("Scenario");
        }
        Scenario scenario = scenarios[scenarioID];

        final String drSignalDA = scenario.getFileInputData() + "/dr_signalDA.xls";
        final String drSignalID = scenario.getFileInputData() + "/dr_signalID.xls";
        final String setTotalPower = scenario.getFileInputData() + "/PreOptWorkload.xls";
        final String setPricesDA = scenario.getFileInputData() + "/PricesDA.xls";
        final String setPricesID = scenario.getFileInputData() + "/PricesID.xls";
        final String setProduction = scenario.getFileInputData() + "/production-ren.xls";

        final String setPredictedRT_DA = scenario.getFileInputData() + "/EnergyPredictionRT-DA.xls";
        final String setPredictedRT_ID = scenario.getFileInputData() + "/EnergyPredictionRT-ID.xls";
        final String setPredictedDT_DA = scenario.getFileInputData() + "/EnergyPredictionDT-DA.xls";
        final String setPredictedDT_ID = scenario.getFileInputData() + "/EnergyPredictionDT-ID.xls";

        final String baselineDA = scenario.getFileInputData() + "/EnergyBaselineDA.xls";
        final String baselineID = scenario.getFileInputData() + "/EnergyBaselineID.xls";

        final String co2baselineDA = scenario.getFileInputData() + "/CO2BaselineDA.xls";
        final String co2baselineID = scenario.getFileInputData() + "/CO2BaselineID.xls";

        final String heatbaselineDA = scenario.getFileInputData() + "/HeatBaselineDA.xls";
        final String heatbaselineID = scenario.getFileInputData() + "/HeatBaselineID.xls";

        LocalDateTime dt = scenario.getTime();
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        LOGGER.info("POPULATE DR SIGNAL INTRADAY");
        boolean drID = dataCSVReadService.populateDRSIGNAL(drSignalID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE DR SIGNAL DAYAHEAD");
        boolean drDA = dataCSVReadService.populateDRSIGNAL(drSignalDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE TOTAL POWER");
        boolean tp = dataCSVReadService.populatePreOptimizationProfiles(setTotalPower, datacenterName, dt);
        LOGGER.info("POPULATE PRICE INTRADAY");
        boolean pricesID = dataCSVReadService.populatePriceData(setPricesID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE PRICE DAYAHEAD");
        boolean pricesDA = dataCSVReadService.populatePriceData(setPricesDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE RT PREDICTED INTRADAY");
        boolean predIDrt = dataCSVReadService.populatePredictedData(setPredictedRT_ID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        LOGGER.info("POPULATE RT PREDICTED DAYAHEAD");
        boolean predDArt = dataCSVReadService.populatePredictedData(setPredictedRT_DA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);

        LOGGER.info("POPULATE DT PREDICTED INTRADAY");
        boolean predIDdt = dataCSVReadService.populatePredictedData(setPredictedDT_ID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
        LOGGER.info("POPULATE DT PREDICTED DAYAHEAD");
        boolean predDAdt = dataCSVReadService.populatePredictedData(setPredictedDT_DA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);


        LOGGER.info("POPULATE BASELINE INTRADAY");
        boolean baseeID = dataCSVReadService.populateBaselineData(baselineID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE BASELINE DAYAHEAD");
        boolean baseDA = dataCSVReadService.populateBaselineData(baselineDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);

        boolean ren = dataCSVReadService.populatePredictedRenewableData(setProduction, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR, MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE);
        LOGGER.info("POPULATE PRODUCTION  day:" + dt + " " + ren);


        HashMap<String, Boolean> map = new HashMap<>();
        map.put("DR-signal DayAhead", drDA);
        map.put("DR-signal Intraday", drID);

        map.put("TotalPower", tp);
        map.put("Prices Intraday", pricesID);
        map.put("Prices DayAhead", pricesDA);

        map.put("Predicted Intraday rt", predIDrt);
        map.put("Predicted DayAhead rt", predDArt);

        map.put("Predicted Intraday dt", predIDdt);
        map.put("Predicted DayAhead dt", predDAdt);

        map.put("Baseline Intraday", baseeID);
        map.put("Baseline DayAhead", baseDA);

        map.put("Production ", ren);

        return map;
    }

    @GetMapping(value = "/populate-marketplace-values/{scenarioID}")
    public Map<String, Boolean> populateMarketplaceValues(@PathVariable("scenarioID") int scenarioID) {
        Scenario[] scenarios = Scenario.values();

        if (scenarios.length < scenarioID + 1) {
            throw new ResourceNotFoundException("Scenario");
        }
        Scenario scenario = scenarios[scenarioID];

        final String drSignalDA = scenario.getFileInputData() + "/dr_signalDA.xls";
        final String drSignalID = scenario.getFileInputData() + "/dr_signalID.xls";

        final String setPricesDA = scenario.getFileInputData() + "/PricesDA.xls";
        final String setPricesID = scenario.getFileInputData() + "/PricesID.xls";



        LocalDateTime dt = scenario.getTime();
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        LOGGER.info("POPULATE DR SIGNAL INTRADAY");
        boolean drID = dataCSVReadService.populateDRSIGNAL(drSignalID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE DR SIGNAL DAYAHEAD");
        boolean drDA = dataCSVReadService.populateDRSIGNAL(drSignalDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);

        LOGGER.info("POPULATE PRICE INTRADAY");
        boolean pricesID = dataCSVReadService.populatePriceData(setPricesID, datacenterName, dt, PredictionGranularity.INTRADAY, AggregationGranularity.MINUTES_30);
        LOGGER.info("POPULATE PRICE DAYAHEAD");
        boolean pricesDA = dataCSVReadService.populatePriceData(setPricesDA, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);

        HashMap<String, Boolean> map = new HashMap<>();
       map.put("DR-signal DayAhead", drDA);
       map.put("DR-signal Intraday", drID);

       map.put("Prices Intraday", pricesID);
       map.put("Prices DayAhead", pricesDA);

        return map;
    }

    @PostMapping("/save-marketplace-values/")
    public Map<String, Boolean> populateMarketplaceValues(@RequestBody MarketDataDTO marketDataDTO) {
        LocalDateTime dt = marketDataDTO.getRecordTime();
        dt = dt.withNano(0).withSecond(0).withMinute(0).withHour(0);
        LOGGER.info("POPULATE DR SIGNAL DAYAHEAD");
        boolean drDA = dataCSVReadService.populateDRSIGNALFromArray(marketDataDTO.getDrSignal(), datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);
        LOGGER.info("POPULATE PRICE DAYAHEAD");
       // boolean pricesDA = dataCSVReadService.populatePriceDataFromArray(marketDataDTO, datacenterName, dt, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);

        HashMap<String, Boolean> map = new HashMap<>();
        map.put("DR-signal DayAhead", drDA);

     //   map.put("Prices DayAhead", pricesDA);

        return map;
    }

    @GetMapping(value = "/init/estimated-profiles/{startdate}")
    public HashMap initEstimatedProfiles(
            @PathVariable("startdate") Long startTime) {
        Scenario[] scenarios = Scenario.values();
        Scenario scenario = scenarios[9];

        final String drSignalDA = scenario.getFileInputData() + "/dr_signalDA.xls";
        final String setPricesDA = scenario.getFileInputData() + "/PricesDA.xls";

        LocalDateTime dateTime = DateUtils.millisToLocalDateTime(startTime);
        dateTime = dateTime.withNano(0).withSecond(0).withMinute(0).withHour(0);
        LOGGER.info("POPULATE DR SIGNAL DAYAHEAD");
        boolean drDA = dataCSVReadService.populateDRSIGNAL(drSignalDA, datacenterName, dateTime, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);

        LOGGER.info("POPULATE PRICE DAYAHEAD");
        boolean pricesDA = dataCSVReadService.populatePriceData(setPricesDA, datacenterName, dateTime, PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR);


        double max = getServerRoomMaxCapacity(dateTime);
        double edPercentage = deviceService.getDTPercentage(datacenterName, dateTime);
        CopFactors copFactors = deviceService.getCopFactors(datacenterName, dateTime);

        double coolRefrenceValue = max /copFactors.getCopC();
        double rtRefrenceValue = max * (1 - edPercentage);
        double dtRefrenceValue = max * edPercentage;

        List<EnergySampleDTO> eDT = new ArrayList<>();
        List<EnergySampleDTO> eRT = new ArrayList<>();
        List<EnergySampleDTO> eCOOL = new ArrayList<>();
        List<EnergySampleDTO> eBaseline = new ArrayList<>();

        for (int i = 0; i < PredictionGranularity.DAYAHEAD.getNoOutputs(); i++) {
            EnergySampleDTO rt = new EnergySampleDTO(rtRefrenceValue, dateTime.withHour(i));
            EnergySampleDTO dt = new EnergySampleDTO(dtRefrenceValue, dateTime.withHour(i));
            EnergySampleDTO cool = new EnergySampleDTO(coolRefrenceValue, dateTime.withHour(i));
            EnergySampleDTO baseline = new EnergySampleDTO(rtRefrenceValue + dtRefrenceValue + coolRefrenceValue, dateTime.withHour(i));

            eDT.add(dt);
            eRT.add(rt);
            eBaseline.add(baseline);
            eCOOL.add(cool);
        }
        LOGGER.info("POPULATE RT DAYAHEAD");
        boolean rtDA = predictedValueService.setupEstimatedProfiles(datacenterName, dateTime, DeviceTypeEnum.SERVER_ROOM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION, PredictionType.ENERGY_CONSUMPTION, eRT);

        LOGGER.info("POPULATE DT DAYAHEAD");
        boolean dtDA = predictedValueService.setupEstimatedProfiles(datacenterName, dateTime, DeviceTypeEnum.SERVER_ROOM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION, PredictionType.ENERGY_CONSUMPTION, eDT);
        LOGGER.info("POPULATE COOL DAYAHEAD");
        boolean coolingDA = predictedValueService.setupEstimatedProfiles(datacenterName, dateTime, DeviceTypeEnum.COOLING_SYSTEM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION, PredictionType.ENERGY_CONSUMPTION, eCOOL);
        LOGGER.info("POPULATE Baseline DAYAHEAD");
        boolean baselineDA = predictedValueService.setupEstimatedProfiles(datacenterName, dateTime, DeviceTypeEnum.SERVER_ROOM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.DATA_CENTER_BASELINE, PredictionType.ENERGY_BASELINE, eBaseline);

        HashMap<String, Boolean> map = new HashMap<>();
        map.put("DR-signal DayAhead", drDA);
        map.put("Prices DayAhead", pricesDA);
        map.put("RT DayAhead", rtDA);
        map.put("DT DayAhead", dtDA);
        map.put("Cool DayAhead", coolingDA);
        map.put("Baseline DayAhead", baselineDA);
        return map;
    }

    @GetMapping(value = "/init/production-profiles/{startdate}/{days}")
    public boolean initProuctiondProfiles(
            @PathVariable("startdate") Long startTime, @PathVariable("days") int days) {
        Scenario[] scenarios = Scenario.values();
        Scenario scenario = scenarios[2];

        final String setProduction = scenario.getFileInputData() + "/production-ren.xls";
        LocalDateTime dateTime = DateUtils.millisToLocalDateTime(startTime);
        dateTime = dateTime.withNano(0).withSecond(0).withMinute(0).withHour(0);

        for (int i = 0; i < days; i++) {
            boolean tp = dataCSVReadService.populateProductionProfiles(setProduction, datacenterName, dateTime.plusDays(i));
            LOGGER.info("POPULATE PRODUCTION  day:" + dateTime.plusDays(i) + " " + tp);
        }
        return true;
    }


    @GetMapping(value = "/init/preopt-profiles/{startdate}/{days}")
    public boolean initEstimatedProfiles(
            @PathVariable("startdate") Long startTime, @PathVariable("days") int days) {
        Scenario[] scenarios = Scenario.values();
        Scenario scenario = scenarios[9];

        final String setTotalPower = scenario.getFileInputData() + "/PreOptWorkload.xls";
        LocalDateTime dateTime = DateUtils.millisToLocalDateTime(startTime);
        dateTime = dateTime.withNano(0).withSecond(0).withMinute(0).withHour(0);

        for (int i = 0; i < days; i++) {
            boolean tp = dataCSVReadService.populatePreOptimizationProfiles(setTotalPower, datacenterName, dateTime.plusDays(i));
            LOGGER.info("POPULATE PRE OPT  day:" + dateTime.plusDays(i) + " " + tp);
        }
        return true;
    }

    private double getServerRoomMaxCapacity(LocalDateTime startLocalDateTime) {
        List<ServerRoom> serverRoomStates = deviceService.getServerRoom(datacenterName, startLocalDateTime);

        double max = 0;
        for (ServerRoom serverRoom : serverRoomStates) {
            max += serverRoom.getMaxEnergyConsumption();
        }
        return max;
    }


}
