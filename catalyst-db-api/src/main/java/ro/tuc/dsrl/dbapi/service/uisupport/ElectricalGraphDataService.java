package ro.tuc.dsrl.dbapi.service.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.GraphDataDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.LabeledValues;
import ro.tuc.dsrl.dbapi.service.basic.MonitoredValueService;
import ro.tuc.dsrl.dbapi.service.history.EnergyHistoricalMonitoredValuesService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.EnergyProfileFormatter;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.ProfileFormatter;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class ElectricalGraphDataService {

    private final EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService;
    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    private final MonitoredValueService monitoredValueService;
    private final boolean executionOn;
    private final boolean serverroomSplit;

    @Autowired
    public ElectricalGraphDataService(EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService,
                                      EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService,
                                      MonitoredValueService monitoredValueService,
                                      @Value("${execution.on}") boolean executionOn,
                                      @Value("${serverroom.split}") boolean serverroomSplit) {

        this.energyHistoricalMonitoredValuesService = energyHistoricalMonitoredValuesService;
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
        this.monitoredValueService = monitoredValueService;
        this.serverroomSplit = serverroomSplit;
        this.executionOn = executionOn;
    }



    public GraphDataDTO getMonitoredConsumptionProductionForDC(UUID dataCenterID,
                                                               LocalDateTime startTime,
                                                               PredictionGranularity predictionGranularity,
                                                               String energyType) {

        // validate inputs
        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        // get energy values from db
        EnergyProfileDTO consumptionDTO = energyHistoricalMonitoredValuesService
                .getConsumptionForEntireDC(dataCenterID,
                        startTime,
                        predictionGranularity,
                        energyType,
                        predictionGranularity.getSampleFrequencyMin(),
                        PredictionType.ENERGY_CONSUMPTION.getType());

        List<Double> historicalConsumptionData = padWithZerosIfNeeded(consumptionDTO, predictionGranularity.getNoOutputs());

        EnergyProfileDTO productionDTO = energyHistoricalMonitoredValuesService
                .getProductionForEntireDC(dataCenterID,
                        startTime,
                        predictionGranularity,
                        energyType,
                        predictionGranularity.getSampleFrequencyMin(),
                        PredictionType.ENERGY_PRODUCTION.getType());

        List<Double> historicalProductionData = padWithZerosIfNeeded(productionDTO, predictionGranularity.getNoOutputs());

        // compute output
        LabeledValues consumptionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_CONSUMPTION.getType().toLowerCase(), historicalConsumptionData);
        LabeledValues productionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_PRODUCTION.getType().toLowerCase(), historicalProductionData);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(consumptionLabeledValues);
        graphDataDTO.addLabeledValues(productionLabeledValues);

        return graphDataDTO;
    }

    public GraphDataDTO getMonitoredAggregatedProductionConsumptionForDC(String dcName,
                                                                  LocalDateTime startTime,
                                                                  PredictionGranularity predictionGranularity){
        LocalDateTime startDate;
        LocalDateTime endDate;
        if (PredictionGranularity.INTRADAY.equals(predictionGranularity)) {
            startDate = DateUtils.computeCurrentIntraDayStart(startTime);
            endDate = DateUtils.computeCurrentIntraDayEnd(startTime);
        } else {
            startDate = DateUtils.computeCurrentDayStart(startTime);
            endDate = DateUtils.computeCurrentDayEnd(startTime);
        }

        List<AggregatedValuesDTO> itRealTimeAveragedResult;
        List<AggregatedValuesDTO> itDelayTolerantAveragedResult = new ArrayList<>();
        List<AggregatedValuesDTO> coolingAveragedResults;
        List<AggregatedValuesDTO> renAveragedResults;

        List<Double> historicalConsumptionData = new ArrayList<>();


        if (executionOn) {
            if (serverroomSplit) {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION);
                itDelayTolerantAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION);
            } else {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_CONSUMPTION);
            }
            coolingAveragedResults = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_COOLING_SYSTEM_CURRENT_CONSUMPTION);
        } else {
            if (serverroomSplit) {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
                itDelayTolerantAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
            } else {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION);
            }
            coolingAveragedResults = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION);
        }
        renAveragedResults = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE);;

        EnergyProfileFormatter formatter = (EnergyProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);

        List<Double> itRealTimeArray = formatter.populateArray(itRealTimeAveragedResult, startDate.getHour(), predictionGranularity);
        List<Double> itDelayTolerantArray = formatter.populateArray(itDelayTolerantAveragedResult, startDate.getHour(), predictionGranularity);
        List<Double> coolingArray = formatter.populateArray(coolingAveragedResults, startDate.getHour(), predictionGranularity);
        List<Double> historicalProductionData = formatter.populateArray(renAveragedResults, startDate.getHour(), predictionGranularity);

        int sizeArray = Math.min(itRealTimeArray.size(), Math.min(itDelayTolerantArray.size(), coolingArray.size()));
        for(int i = 0; i < sizeArray; i++){
            double value = itRealTimeArray.get(i) + itDelayTolerantArray.get(i) + coolingArray.get(i);
            historicalConsumptionData.add(value);
        }
        // compute output
        LabeledValues consumptionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_CONSUMPTION.getType().toLowerCase(), historicalConsumptionData);
        LabeledValues productionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_PRODUCTION.getType().toLowerCase(), historicalProductionData);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(consumptionLabeledValues);
        graphDataDTO.addLabeledValues(productionLabeledValues);

        return graphDataDTO;

    }

    public GraphDataDTO getMonitoredDisaggregatedConsumptionForDC(String dcName,
                                                                  LocalDateTime startTime,
                                                                  PredictionGranularity predictionGranularity) {


        LocalDateTime startDate;
        LocalDateTime endDate;
        if (PredictionGranularity.INTRADAY.equals(predictionGranularity)) {
            startDate = DateUtils.computeCurrentIntraDayStart(startTime);
            endDate = DateUtils.computeCurrentIntraDayEnd(startTime);
        } else {
            startDate = DateUtils.computeCurrentDayStart(startTime);
            endDate = DateUtils.computeCurrentDayEnd(startTime);
        }

        List<AggregatedValuesDTO> itRealTimeAveragedResult;
        List<AggregatedValuesDTO> itDelayTolerantAveragedResult = new ArrayList<>();
        List<AggregatedValuesDTO> coolingAveragedResults;
        if (executionOn) {
            if (serverroomSplit) {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION);
                itDelayTolerantAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION);
            } else {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_CONSUMPTION);
            }
            coolingAveragedResults = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.PRE_OPT_COOLING_SYSTEM_CURRENT_CONSUMPTION);
        } else {
            if (serverroomSplit) {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
                itDelayTolerantAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
            } else {
                itRealTimeAveragedResult = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION);
            }
            coolingAveragedResults = monitoredValueService.getAveragedMonitoredValues(dcName, startDate, endDate, predictionGranularity, MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION);
        }
        EnergyProfileFormatter formatter = (EnergyProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);

        List<Double> itRealTimeArray = formatter.populateArray(itRealTimeAveragedResult, startDate.getHour(), predictionGranularity);
        List<Double> itDelayTolerantArray = formatter.populateArray(itDelayTolerantAveragedResult, startDate.getHour(), predictionGranularity);
        List<Double> coolingArray = formatter.populateArray(coolingAveragedResults, startDate.getHour(), predictionGranularity);


        // compute output
        LabeledValues itDTCompConsumptionLabeledValues = new LabeledValues(
                EnergyProfileLabel.IT_COMPONENT_DT.getLabel().toLowerCase(), itDelayTolerantArray);
        LabeledValues itRTCompConsumptionLabeledValues = new LabeledValues(
                EnergyProfileLabel.IT_COMPONENT_RT.getLabel().toLowerCase(), itRealTimeArray);
        LabeledValues coolingSystemConsumptionLabeledValues = new LabeledValues(
                EnergyProfileLabel.COOLING_SYSTEM.getLabel().toLowerCase(), coolingArray);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(itDTCompConsumptionLabeledValues);
        graphDataDTO.addLabeledValues(itRTCompConsumptionLabeledValues);
        graphDataDTO.addLabeledValues(coolingSystemConsumptionLabeledValues);

        return graphDataDTO;
    }

//    public GraphDataDTO getMonitoredDisaggregatedConsumptionForDC(UUID dataCenterID,
//                                                                  LocalDateTime startTime,
//                                                                  PredictionGranularity predictionGranularity,
//                                                                  String energyType) {
//
//        // validate inputs
//        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
//        EnergyProfileValidator.throwIncorrectParameterException(errors);
//
//        // get energy values from db
//        List<EnergyProfileDTO> itCompDTO = energyHistoricalMonitoredValuesService.getConsumptionForITComponent(
//                dataCenterID,
//                startTime,
//                predictionGranularity,
//                energyType,
//                predictionGranularity.getSampleFrequencyMin(),
//                PredictionType.ENERGY_CONSUMPTION.getType());
//
//        List<Double> itCompConsumptionDataDT = padWithZerosIfNeeded(itCompDTO.get(0), predictionGranularity.getNoOutputs());
//
//        List<Double> itCompConsumptionDataRT = padWithZerosIfNeeded(itCompDTO.get(1), predictionGranularity.getNoOutputs());
//
//        EnergyProfileDTO coolingSystemDTO = energyHistoricalMonitoredValuesService.getConsumptionForCoolingSystem(
//                dataCenterID,
//                startTime,
//                predictionGranularity,
//                energyType,
//                predictionGranularity.getSampleFrequencyMin(),
//                PredictionType.ENERGY_CONSUMPTION.getType());
//
//        List<Double> coolingSystemConsumptionData = padWithZerosIfNeeded(coolingSystemDTO, predictionGranularity.getNoOutputs());
//
//        // compute output
//        LabeledValues itDTCompConsumptionLabeledValues = new LabeledValues(
//                EnergyProfileLabel.IT_COMPONENT_DT.getLabel().toLowerCase(), itCompConsumptionDataDT);
//        LabeledValues itRTCompConsumptionLabeledValues = new LabeledValues(
//                EnergyProfileLabel.IT_COMPONENT_RT.getLabel().toLowerCase(), itCompConsumptionDataRT);
//        LabeledValues coolingSystemConsumptionLabeledValues = new LabeledValues(
//                EnergyProfileLabel.COOLING_SYSTEM.getLabel().toLowerCase(), coolingSystemConsumptionData);
//
//        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
//        graphDataDTO.addLabeledValues(itDTCompConsumptionLabeledValues);
//        graphDataDTO.addLabeledValues(itRTCompConsumptionLabeledValues);
//        graphDataDTO.addLabeledValues(coolingSystemConsumptionLabeledValues);
//
//        return graphDataDTO;
//    }

    public GraphDataDTO getPredictedConsumptionProductionForDC(UUID dataCenterID,
                                                               LocalDateTime startTime,
                                                               PredictionGranularity predictionGranularity,
                                                               String energyType) {

        // validate inputs
        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        // get energy values from db
        EnergyProfileDTO consumptionDTO = energyHistoricalPredictedValuesService.getPredictedConsumptionForEntireDC(
                dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());

        List<Double> predictedConsumptionData = padWithZerosIfNeeded(consumptionDTO, predictionGranularity.getNoOutputs());

        EnergyProfileDTO productionDTO = energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
                dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                PredictionType.ENERGY_PRODUCTION.getType(),
                FlexibilityType.NONE.getType());

        List<Double> predictedProductionData = padWithZerosIfNeeded(productionDTO, predictionGranularity.getNoOutputs());

        // compute output
        LabeledValues consumptionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_CONSUMPTION.getType().toLowerCase(), predictedConsumptionData);
        LabeledValues productionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_PRODUCTION.getType().toLowerCase(), predictedProductionData);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(consumptionLabeledValues);
        graphDataDTO.addLabeledValues(productionLabeledValues);

        return graphDataDTO;
    }

    public GraphDataDTO getPredictedDisaggregatedConsumptionForDC(UUID dataCenterID,
                                                                  LocalDateTime startTime,
                                                                  PredictionGranularity predictionGranularity,
                                                                  String energyType) {

        // validate inputs
        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        // get energy values from db
        List<EnergyProfileDTO> itCompDTO = energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(
                dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());

        List<Double> itCompConsumptionDataDT = padWithZerosIfNeeded(itCompDTO.get(0), predictionGranularity.getNoOutputs());

        List<Double> itCompConsumptionDataRT = padWithZerosIfNeeded(itCompDTO.get(1), predictionGranularity.getNoOutputs());

        EnergyProfileDTO coolingSystemDTO = energyHistoricalPredictedValuesService.getPredictedConsumptionForCoolingSystem(
                dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());

        List<Double> coolingSystemConsumptionData = padWithZerosIfNeeded(coolingSystemDTO, predictionGranularity.getNoOutputs());

        // compute output
        LabeledValues itCompConsumptionLabeledValuesDT = new LabeledValues(
                EnergyProfileLabel.IT_COMPONENT_DT.getLabel().toLowerCase(), itCompConsumptionDataDT);
        LabeledValues itCompConsumptionLabeledValuesRT = new LabeledValues(
                EnergyProfileLabel.IT_COMPONENT_RT.getLabel().toLowerCase(), itCompConsumptionDataRT);
        LabeledValues coolingSystemConsumptionLabeledValues = new LabeledValues(
                EnergyProfileLabel.COOLING_SYSTEM.getLabel().toLowerCase(), coolingSystemConsumptionData);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(itCompConsumptionLabeledValuesDT);
        graphDataDTO.addLabeledValues(itCompConsumptionLabeledValuesRT);
        graphDataDTO.addLabeledValues(coolingSystemConsumptionLabeledValues);

        return graphDataDTO;
    }

    private static List<Double> padWithZerosIfNeeded(EnergyProfileDTO energyProfileDTO, int size) {

        if (energyProfileDTO == null) {
            return new ArrayList<>(Collections.nCopies(size, 0.0));
        }

        List<Double> energyValues = energyProfileDTO.getEnergyValues();
        if (energyValues.size() == size) {
            return energyValues;
        }

        energyValues.addAll(Collections.nCopies(size - energyValues.size(), 0.0));
        return energyValues;
    }
}
