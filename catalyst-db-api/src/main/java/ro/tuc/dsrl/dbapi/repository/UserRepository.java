package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.User;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    @Query(value = "SELECT u from User u WHERE u.username = :username and u.password = :password")
    User findUser(@Param("username") String username, @Param("password") String password);

    @Query(value = "SELECT u from User u WHERE u.id = :id")
    User findByUserId(@Param("id") UUID id);

    @Query(value = "SELECT d from UserToDC u_to_dc "+
            "JOIN u_to_dc.device d JOIN u_to_dc.user u " +
            "INNER JOIN d.deviceTypeId dt " +
            "WHERE dt.type like :deviceType " +
            "AND d.deleted = false " +
            "AND d.parent is NULL "+
            "AND u.username = :username ")
    List<Device> findDevices(@Param("username") String username, @Param("deviceType") String deviceType);
}
