//package ro.tuc.dsrl.dbapi.model.builders;
//
//import ro.tuc.dsrl.catalyst.model.dto.ActionInstanceDTO;
//import ro.tuc.dsrl.dbapi.model.ActionInstance;
//import ro.tuc.dsrl.dbapi.model.ActionType;
//import ro.tuc.dsrl.dbapi.model.OptimizationPlan;
//
//public class ActionInstanceBuilder {
//
//    private ActionInstanceBuilder() {
//
//    }
//
//    public static ActionInstanceDTO generateDTOFromEntity(ActionInstance actionInstance,
//                                                          ActionType actionType, OptimizationPlan optimizationPlan) {
//        return new ActionInstanceDTO(
//                actionInstance.getId(),
//                actionInstance.getLabel(),
//                actionInstance.getStartTime(),
//                actionInstance.getEndTime(),
//                actionType.getId(),
//                optimizationPlan.getId()
//        );
//    }
//
//    public static ActionInstance generateEntityFromDTO(ActionInstanceDTO actionInstanceDTO,
//                                                       ActionType actionType, OptimizationPlan optimizationPlan) {
//        return new ActionInstance(
//                actionInstanceDTO.getId(),
//                actionInstanceDTO.getLabel(),
//                actionInstanceDTO.getStartTime(),
//                actionInstanceDTO.getEndTime(),
//                actionType,
//                optimizationPlan
//        );
//    }
//
//
//}
