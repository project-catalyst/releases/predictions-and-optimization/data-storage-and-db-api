package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.Device;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface DeviceRepository extends JpaRepository<Device, UUID> {

    @Query(value = "SELECT d FROM Device d where d.deleted = false ")
    List<Device> findAllAvailable();

    @Query(value = "SELECT d.id " +
                    "FROM Device d " +
                    "WHERE d.label like :label " +
                    "AND d.deleted = false ")
    UUID findByLabel(@Param("label") String label);

    @Query(value = "SELECT d " +
                     "FROM Device d " +
                     "JOIN FETCH d.deviceTypeId " +
                     "WHERE d.label like :label " +
                     "AND d.deleted = false ")
    Device findDeviceByLabel(@Param("label") String label);

    @Query(value = "SELECT d.id FROM Device d WHERE d.label LIKE UPPER(CONCAT('%',:label,'%'))  and d.deleted = false ")
    UUID findByLabelComponent(@Param("label") String label);

    @Query(value = "SELECT d FROM Device d where d.id = :id and d.deleted = false ")
    Device findByUUID(@Param("id") UUID deviceUUID);

    @Query(value = " SELECT inn.idDevice, inn.label,  inn.prop, mv.timestamp, mv.value " +
                   " FROM monitored_value mv "+
                   " INNER JOIN (SELECT m.id id, d.id idDevice, d.label label, m.property prop, MAX(mv2.timestamp) times" +
                        "  FROM monitored_value mv2 " +
                        "  JOIN device d on d.id = mv2.device_id " +
                        "  LEFT JOIN device p on p.id = d.parent " +
                        "  JOIN device_type dt on dt.id = d.device_type_id " +
                        "  JOIN measurement m on m.id = mv2.measurement_id " +
                        "   WHERE dt.type like :deviceType " +
                        "   AND  p.label like :parentLabel " +
                        "   AND mv2.timestamp <= :time " +
                        "   AND d.deleted = false " +
                        "  GROUP BY m.id, d.id, d.label, m.property ) " +
            " inn on (inn.id = mv.measurement_id and mv.timestamp = inn.times) "  , nativeQuery = true)
   Object[][] findDeviceStates2(@Param("deviceType") String deviceType, @Param("parentLabel") String parentLabel, @Param("time") LocalDateTime time);


    @Query(value = "SELECT d " +
            "FROM Device d " +
            "INNER JOIN d.deviceTypeId dt " +
            "WHERE dt.type like :deviceType " +
            "AND d.deleted = false " +
            "AND d.parent is NULL ")
    List<Device> findParentDevices(@Param("deviceType") String deviceType);



    @Query(value = "SELECT d " +
            "FROM Device d " +
            "INNER JOIN d.deviceTypeId dt " +
            "LEFT JOIN d.parent p " +
            "WHERE dt.type like :deviceType " +
            "AND d.deleted = false " +
            "AND ( d.label like :label " +
            "OR p.label like :label ) ")
    List<Device> findByDeviceTypeAndParent(@Param("deviceType") String deviceType, @Param("label") String label);

    @Query(value = "SELECT d.id " +
            "FROM Device d " +
            "WHERE d.parent.id = :dataCenterId " +
            "AND d.label LIKE UPPER(CONCAT('%',:componentType,'%')) ")
    UUID findIdForComponentByDatacCenterId(@Param("dataCenterId") UUID dataCenterID,
                                                  @Param("componentType") String componentType);

    @Query(value = "SELECT d " +
            "FROM Device d " +
            "INNER JOIN d.deviceTypeId dt " +
            "WHERE dt.type LIKE UPPER(CONCAT('%',:type,'%')) ")
    Device findDeviceByDeviceType(@Param("type") String type);
}
