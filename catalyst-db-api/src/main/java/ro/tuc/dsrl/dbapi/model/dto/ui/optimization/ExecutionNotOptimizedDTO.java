package ro.tuc.dsrl.dbapi.model.dto.ui.optimization;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ExecutionNotOptimizedDTO {

    // Consumption
    private List<Double> consumptionDTValues;
    private List<Double> consumptionRTValues;
    private List<Double> coolingValues;
    private List<Double> incomingReallocValues;

    // Production
    private List<Double> greenEnergyValues;
    private List<Double> boughtValues;
    private List<Double> lostValues;
    private List<Double> deltaDiff;

    private int length = 0;

    public ExecutionNotOptimizedDTO(int length) {
        this.length = length;
    }

    public List<Double> getConsumptionDTValues() {
        return consumptionDTValues;
    }

    public void setConsumptionDTValues(List<Double> consumptionDTValues) {
        assertTrue("consumptionDTValues " + consumptionDTValues.size(), consumptionDTValues.size() == length);
        this.consumptionDTValues = consumptionDTValues;
    }

    public List<Double> getConsumptionRTValues() {
        return consumptionRTValues;
    }

    public void setConsumptionRTValues(List<Double> consumptionRTValues) {
        assertTrue("consumptionRTValues " + consumptionRTValues.size(), consumptionRTValues.size() == length);
        this.consumptionRTValues = consumptionRTValues;
    }

    public List<Double> getCoolingValues() {
        return coolingValues;
    }

    public void setCoolingValues(List<Double> coolingValues) {
        assertTrue("coolingValuesValues " + coolingValues.size(), coolingValues.size() == length);
        this.coolingValues = coolingValues;
    }

    public List<Double> getGreenEnergyValues() {
        return greenEnergyValues;
    }

    public void setGreenEnergyValues(List<Double> greenEnergyValues) {
        assertTrue("greenEnergyValues " + greenEnergyValues.size(), greenEnergyValues.size() == length);
        this.greenEnergyValues = greenEnergyValues;
    }

    public List<Double> getBoughtValues() {
        return boughtValues;
    }

    public void setBoughtValues(List<Double> boughtValues) {
        assertTrue("boughtValues " + boughtValues.size(), boughtValues.size() == length);
        this.boughtValues = boughtValues;
    }

    public List<Double> getLostValues() {
        return lostValues;
    }

    public void setLostValues(List<Double> lostValues) {
        assertTrue("lostValues " + lostValues.size(), lostValues.size() == length);
        this.lostValues = lostValues;
    }

    public List<Double> getIncomingReallocValues() {
        return incomingReallocValues;
    }

    public void setIncomingReallocValues(List<Double> incomingReallocValues) {
        assertTrue("incomingReallocValues " + incomingReallocValues.size(), incomingReallocValues.size() == length);
        this.incomingReallocValues = incomingReallocValues;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
