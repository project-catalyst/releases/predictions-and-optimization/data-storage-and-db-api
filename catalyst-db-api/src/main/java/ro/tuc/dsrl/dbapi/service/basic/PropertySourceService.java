package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.PropertySourceDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.PropertySource;
import ro.tuc.dsrl.dbapi.model.builders.PropertySourceBuilder;
import ro.tuc.dsrl.dbapi.repository.PropertySourceRepository;
import ro.tuc.dsrl.dbapi.service.validators.PropertySourceValidator;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PropertySourceService {
    private final PropertySourceRepository propertySourceRepository;

    @Autowired
    public PropertySourceService(PropertySourceRepository propertySourceRepository) {
        this.propertySourceRepository = propertySourceRepository;
    }

    public PropertySourceDTO findById(UUID propertySourceUUID) {
        PropertySource propertySource = propertySourceRepository.findByUUId(propertySourceUUID);
        if (propertySource == null) {
            throw new ResourceNotFoundException("PropertySource with ID " + propertySourceUUID);
        }

        return PropertySourceBuilder.generateDTOFromEntity(propertySource);
    }

    public List<PropertySourceDTO> findAll() {
        List<PropertySource> propertySources = propertySourceRepository.findAll();

        return propertySources.stream()
                .map(PropertySourceBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public UUID insert(PropertySourceDTO propertySourceDTO) {
        PropertySourceValidator.validateInsert(propertySourceDTO);

        PropertySource propertySource = PropertySourceBuilder.generateEntityFromDTO(propertySourceDTO);
        return propertySourceRepository.save(propertySource).getId();
    }
}
