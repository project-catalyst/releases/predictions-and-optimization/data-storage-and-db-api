package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "optimization_action")
public class OptimizationAction {


    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "type")
    @NotNull
    private String type;

    @Column(name = "start_date")
    @NotNull
    private LocalDateTime startTime;

    @Column(name = "end_date")
    @NotNull
    private LocalDateTime endTime;

    @Column(name = "amount")
    @NotNull
    private Double amount;

    @Column(name = "thermal_amount")
    @NotNull
    private Double thermalAmount;

    @ManyToOne
    @JoinColumn(name = "optimization_plan_id")
    @NotNull
    private OptimizationPlan optimizationPlan;

    @Column(name = "move_from_date")
    private LocalDateTime moveFromDate;

    @Column(name = "move_percentage")
    private Double movePercentage;

    public OptimizationAction() {
    }

    public OptimizationAction(String type, LocalDateTime startTime, LocalDateTime endTime, Double amount, Double thermalAmount,OptimizationPlan plan) {
        this.type = type;
        this.startTime = startTime;
        this.endTime = endTime;
        this.amount = amount;
        this.thermalAmount = thermalAmount;
        this.optimizationPlan = plan;
        this.movePercentage = 0.0;
    }

    public OptimizationAction(String type, LocalDateTime startTime, LocalDateTime endTime, Double amount,Double thermalAmount, OptimizationPlan plan, LocalDateTime moveToDate, Double movePercentage) {
        this(type, startTime, endTime, amount,thermalAmount, plan);
        this.movePercentage = movePercentage;
        this.moveFromDate = moveToDate;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public OptimizationPlan getOptimizationPlan() {
        return optimizationPlan;
    }

    public void setOptimizationPlan(OptimizationPlan optimizationPlan) {
        this.optimizationPlan = optimizationPlan;
    }

    public LocalDateTime getMoveFromDate() {
        return moveFromDate;
    }

    public void setMoveFromDate(LocalDateTime moveToDate) {
        this.moveFromDate = moveToDate;
    }

    public Double getMovePercentage() {
        return movePercentage;
    }

    public void setMovePercentage(Double movePercentage) {
        this.movePercentage = movePercentage;
    }

    public Double getThermalAmount() {
        return thermalAmount;
    }

    public void setThermalAmount(Double thermalAmount) {
        this.thermalAmount = thermalAmount;
    }
}
