package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.OptimizationAction;
import ro.tuc.dsrl.dbapi.model.OptimizationPlan;

import java.time.LocalDateTime;

public class OptimizationActionBuilder {

    private OptimizationActionBuilder(){
    }

    public static OptimizationActionDTO generateDTOFromEntity(OptimizationAction optAction) {
        long moveMillis = (optAction.getMoveFromDate()!=null)? DateUtils.dateTimeToMillis(optAction.getMoveFromDate()) : 0;
        return new OptimizationActionDTO(
                optAction.getId(),
                optAction.getType(),
                DateUtils.dateTimeToMillis(optAction.getStartTime()),
                DateUtils.dateTimeToMillis(optAction.getEndTime()),
                optAction.getAmount(),
                optAction.getThermalAmount(),
                moveMillis,
                optAction.getMovePercentage());
    }

    public static OptimizationAction generateEntityFromDTO(OptimizationActionDTO optActionDTO,OptimizationPlan plan) {
        LocalDateTime moveFrom =  (optActionDTO.getMoveFromDate()!=0)? DateUtils.millisToUTCLocalDateTime(optActionDTO.getMoveFromDate()): null;
        return new OptimizationAction(
                optActionDTO.getType(),
                DateUtils.millisToUTCLocalDateTime(optActionDTO.getStartTime()),
                DateUtils.millisToUTCLocalDateTime(optActionDTO.getEndTime()),
                optActionDTO.getAmount(),
                optActionDTO.getAmount(),
                plan,
                moveFrom,
                optActionDTO.getMovePercentage());

    }

}
