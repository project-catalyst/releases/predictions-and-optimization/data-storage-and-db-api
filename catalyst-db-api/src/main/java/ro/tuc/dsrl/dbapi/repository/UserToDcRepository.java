package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.dsrl.dbapi.model.User;
import ro.tuc.dsrl.dbapi.model.UserToDC;

import java.util.UUID;

public interface UserToDcRepository extends JpaRepository<UserToDC, UUID> {
}
