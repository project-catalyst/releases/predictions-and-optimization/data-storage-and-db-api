package ro.tuc.dsrl.dbapi.controller.preconditions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.enums.ActionTypeName;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OptimizationPreconditions {

    private static final Log LOGGER = LogFactory.getLog(OptimizationPreconditions.class);

    private OptimizationPreconditions() {
    }

    public static List<String> validateScenarioId(Integer scenarioId) {

        List<String> errors = new ArrayList<>();

        if (scenarioId == null) {
            errors.add("Scenario id must not be null");
        }

        return errors;
    }

    public static List<String> validateScenarioId(Integer scenarioId, String granularity) {

        List<String> errors = validateScenarioId(scenarioId);

        if (!granularity.equals(PredictionGranularity.INTRADAY.getGranularity())
                && !granularity.equals(PredictionGranularity.REALTIME_INTRADAY_FRAME.getGranularity())
                && !granularity.equals(PredictionGranularity.DAYAHEAD.getGranularity())
                && !granularity.equals(PredictionGranularity.NEAR_REAL_TIME.getGranularity())
                && !granularity.equals(PredictionGranularity.REALTIME_DAYAHEAD_FRAME.getGranularity())) {
            errors.add("Incorrect granularity type " + granularity + " " + PredictionGranularity.REALTIME_DAYAHEAD_FRAME.getGranularity() + ". It must be INTRADAY, DAYAHEAD or REAL_TIME!");
        }

        return errors;
    }

    public static void validateScenarioIdThrowErrors(Integer scenarioId) {

        List<String> errors = OptimizationPreconditions.validateScenarioId(scenarioId);
        OptimizationPreconditions.throwIncorrectParameterException(errors);
    }

    public static List<String> validateActivityDataCenterIdAndInterval(String actionType,
                                                                       Long startTime, Long endTime) {

        List<String> errors = new ArrayList<>();

        if (actionType == null) {
            errors.add("Action type must not be null");
        } else {
            if (ActionTypeName.getByShortName(actionType) == null) {
                errors.add("Invalid action type: " + actionType);
            }
        }

        if (startTime == null) {
            errors.add("startTime must not be null");
        }

        if (endTime == null) {
            errors.add("endTime must not be null");
        }

        if (startTime != null && endTime != null && startTime > endTime) {
            errors.add(" startTime must be lower than endTime ");
        }

        return errors;

    }

    public static void validateActivityDataCenterIdAndIntervalThrowErrors(String actionType,
                                                                          Long startTime, Long endTime) {
        List<String> errors = OptimizationPreconditions.validateActivityDataCenterIdAndInterval(actionType, startTime, endTime);

        OptimizationPreconditions.throwIncorrectParameterException(errors);
    }
//
//    public static List<String> validateNewOptimizationPlanWithActions(OptimizationPlanWithActionsDTO optimizationPlan) {
//
//        List<String> errors = new ArrayList<>();
//
//        if (optimizationPlan == null) {
//            errors.add("Optimization plan DTO must not be null");
//            return errors;
//        }
//
//        if (optimizationPlan.getStartTimePlan() == null) {
//            errors.add("startTimePlan field must not be null");
//        }
//
//        if (optimizationPlan.getEndTimePlan() == null) {
//            errors.add("endTimePlan field must not be null");
//        }
//
//        if (optimizationPlan.getStartTimePlan() != null && optimizationPlan.getEndTimePlan() != null
//                && optimizationPlan.getStartTimePlan() > optimizationPlan.getEndTimePlan()) {
//            errors.add("startTimePlan must be lower then endTimePlan");
//        }
//
//        if (optimizationPlan.getTimeframe() == null || optimizationPlan.getTimeframe().isEmpty()) {
//            errors.add("timeframe field must not be null or an empty string");
//        }
//
//        if (optimizationPlan.getCarbonSavings() == null) {
//            errors.add("carbonSavings field must not be null");
//        }
//
//        if (optimizationPlan.getConfidenceLevel() == null) {
//            errors.add("confidenceLevel field must not be null");
//        }
//
//        if (optimizationPlan.getCostSavings() == null) {
//            errors.add("costSavings field must not be null");
//        }
//
//        if (optimizationPlan.getEnergySavings() == null) {
//            errors.add("energySavings field must not be null");
//        }
//
//        if (optimizationPlan.getSelected() == null) {
//            errors.add("selected field must not be null");
//        }
//
//
//        if (optimizationPlan.getActions() == null || optimizationPlan.getActions().isEmpty()) {
//            errors.add("actions list must not be null or empty");
//        }
//
//        return errors;
//    }
//
//    public static void validateNewOptimizationPlanWithActionsThrowErrors(OptimizationPlanWithActionsDTO optimizationPlan) {
//
//        List<String> errors = validateNewOptimizationPlanWithActions(optimizationPlan);
//        throwIncorrectParameterException(errors);
//    }

    public static List<String> validateOptimizationPlansContainer(OptimizationPlansContainer plans) {

        List<String> errors = new ArrayList<>();

        if (plans == null) {
            errors.add("OptimizationPlansContainer must not be null");
            return errors;
        }
        // check timeframe
        if (plans.getTimeframe() == null || plans.getTimeframe().isEmpty()) {
            errors.add("Timeframe must not br null or empty");
        } else if (Timeframe.getFromName(plans.getTimeframe()) == null) {
            errors.add("Timeframe: " + plans.getTimeframe() + " is an invalid timeframe name");
        }

        Date startDate = plans.getStartDate();
        Date endDate = plans.getEndDate();

        // check dates
        if (startDate == null) {
            errors.add("Start date must not be null");
        }

        if (endDate == null) {
            errors.add("End date must not be null");
        }

        if (startDate != null && endDate != null && endDate.before(startDate)) {
            errors.add("End date must be after start date");
        }


        // check plan list
        List<OptimizerPlan> optimizations = plans.getOptimizations();
        if (optimizations == null || optimizations.isEmpty()) {
            errors.add("List of optimizations must not be null or empty");
        }


        return errors;
    }


    public static List<String> validateCurrentDateAndConfidenceLevel(Long currentDateMillis,
                                                                     Double confidenceLevel) {
        List<String> errors = new ArrayList<>();

        if (currentDateMillis == null) {
            errors.add("Current date must not be null");
        } else if (currentDateMillis == 0) {
            errors.add("Current date must not be 0");
        }

        if (confidenceLevel == null) {
            errors.add("confidence level must not be null");
        }

        return errors;

    }

    public static void validateCurrentDateAndConfidenceLevelThrowErrors(Long currentDateMillis,
                                                                        Double confidenceLevel) {

        List<String> errors = validateCurrentDateAndConfidenceLevel(currentDateMillis, confidenceLevel);

        throwIncorrectParameterException(errors);
    }

    public static void validateOptimizationPlansContainerThrowErrors(OptimizationPlansContainer plans) {
        List<String> errors = validateOptimizationPlansContainer(plans);
        throwIncorrectParameterException(errors);

    }


    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(OptimizationPreconditions.class.getSimpleName(), errors);
        }
    }
}
