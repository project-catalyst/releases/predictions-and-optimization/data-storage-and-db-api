package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "device")
public class Device {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "label", unique = true)
    @NotNull
    private String label;

    @ManyToOne
    @JoinColumn(name = "device_type_id")
    @NotNull
    private DeviceType deviceTypeId;

    @OneToOne
    @JoinColumn(name = "parent")
    private Device parent;

    @Column(name = "deleted")
    private boolean deleted;

    public Device() {

    }

    public Device(@NotNull String label,
                  @NotNull DeviceType deviceTypeId,
                  Device parent) {
        this.label = label;
        this.deviceTypeId = deviceTypeId;
        this.parent = parent;
    }

    public Device(@NotNull UUID id,
                  @NotNull String label,
                  @NotNull DeviceType deviceTypeId,
                  Device parent) {
        this(label, deviceTypeId, parent);
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public DeviceType getDeviceTypeId() {
        return deviceTypeId;
    }

    public Device getParent() {
        return parent;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDeviceTypeId(DeviceType deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public void setParent(Device parent) {
        this.parent = parent;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
