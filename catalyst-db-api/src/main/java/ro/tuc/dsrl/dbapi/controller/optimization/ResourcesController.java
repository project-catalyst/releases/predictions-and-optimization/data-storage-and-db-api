package ro.tuc.dsrl.dbapi.controller.optimization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.UserDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.User;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.UserService;
import ro.tuc.dsrl.geyser.datamodel.components.Component;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.HeatRecoveryInfrastructure;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/resources")
public class ResourcesController {

    private final DeviceService deviceService;
    private final UserService userService;


    @Autowired
    public ResourcesController(DeviceService deviceService, UserService userService) {
        this.deviceService = deviceService;
        this.userService = userService;
    }


    @PostMapping(value = "/battery/{dc-name}")
    public List<Battery> insertBatteryDTO(@PathVariable("dc-name") String dcName, @RequestBody Battery battery) {
        return deviceService.insertBattery(battery, dcName);
    }

    @PutMapping(value = "/battery/{dc-name}")
    public List<Battery> updateBatteryDTO(@PathVariable("dc-name") String dcName, @RequestBody Battery battery) {
        return deviceService.updateBattery(battery, dcName);
    }

    @GetMapping(value = "/battery/{dc-name}/{time}")
    public List<Battery> findAllBatteries(@PathVariable("dc-name") String dcName, @PathVariable("time") long time) {
        return deviceService.getBatteries(dcName, DateUtils.millisToLocalDateTime(time));
    }

    @PostMapping(value = "/tes/{dc-name}")
    public List<ThermalEnergyStorage> insertTesDTO(@PathVariable("dc-name") String dcName, @RequestBody ThermalEnergyStorage tes) {
        return deviceService.insertThermalEnergyStorage(tes, dcName);
    }

    @PutMapping(value = "/tes/{dc-name}")
    public List<ThermalEnergyStorage> updateTesDTO(@PathVariable("dc-name") String dcName, @RequestBody ThermalEnergyStorage tes) {
        return deviceService.updateThermalEnergyStorage(tes, dcName);
    }

    @GetMapping(value = "/tes/{dc-name}/{time}")
    public List<ThermalEnergyStorage> findAllTES(@PathVariable("dc-name") String dcName, @PathVariable("time") long time) {
        return deviceService.getThermalEnergyStorages(dcName, DateUtils.millisToLocalDateTime(time));
    }

    @PostMapping(value = "/server-room/{dc-name}")
    public List<ServerRoom> insertServerRoomDTO(@PathVariable("dc-name") String dcName, @RequestBody ServerRoom serverRoom) {
        return deviceService.insertServerRoom(serverRoom, dcName);
    }

    @PutMapping(value = "/server-room/{dc-name}")
    public List<ServerRoom> updateServerRoomDTO(@PathVariable("dc-name") String dcName, @RequestBody ServerRoom serverRoom) {
        return deviceService.updateServerRoom(serverRoom, dcName);
    }

    @GetMapping(value = "/server-room/{dc-name}/{time}")
    public List<ServerRoom> findAllServerRoom(@PathVariable("dc-name") String dcName, @PathVariable("time") long time) {
        return deviceService.getServerRoom(dcName, DateUtils.millisToLocalDateTime(time));
    }

    @PostMapping(value = "/cooling-system/{dc-name}")
    public List<CoolingSystem> insertCoolingSystemDTO(@PathVariable("dc-name") String dcName, @RequestBody CoolingSystem coolingSystem) {
        return deviceService.insertCoolingSystem(coolingSystem, dcName);
    }

    @PutMapping(value = "/cooling-system/{dc-name}")
    public List<CoolingSystem> updateCoolingSystemDTO(@PathVariable("dc-name") String dcName, @RequestBody CoolingSystem coolingSystem) {
        return deviceService.updateCoolingSystem(coolingSystem, dcName);
    }

    @GetMapping(value = "/cooling-system/{dc-name}/{time}")
    public List<CoolingSystem> findAllCoolingSystem(@PathVariable("dc-name") String dcName, @PathVariable("time") long time) {
        return deviceService.getCoolingSystem(dcName, DateUtils.millisToLocalDateTime(time));
    }

    @PostMapping(value = "/heat-recovery-system/{dc-name}")
    public List<HeatRecoveryInfrastructure> insertHRI(@PathVariable("dc-name") String dcName, @RequestBody HeatRecoveryInfrastructure hri) {
        return deviceService.insertHeatRecoverySystems(hri, dcName);
    }

    @PutMapping(value = "/heat-recovery-system/{dc-name}")
    public List<HeatRecoveryInfrastructure> updateHRI(@PathVariable("dc-name") String dcName, @RequestBody HeatRecoveryInfrastructure hri) {
        return deviceService.updateHeatRecoverySystems(hri, dcName);
    }

    @GetMapping(value = "/heat-recovery-system/{dc-name}/{time}")
    public List<HeatRecoveryInfrastructure> finHRIS(@PathVariable("dc-name") String dcName, @PathVariable("time") long time) {
        return deviceService.getHeatRecoveryInfrastructure(dcName, DateUtils.millisToLocalDateTime(time));
    }

    @PostMapping(value = "/datacenter")
    public List<DataCentre> insertDatacentreDTO(@RequestBody DataCentre dataCentre) {
        return deviceService.insertDatacentre(dataCentre);
    }

    @PostMapping(value = "/user_datacenter/{user_id}")
    public List<DataCentre> insertDatacentreDTO(@PathVariable UUID user_id, @RequestBody DataCentre dataCentre) {
        return userService.insertDatacenter(dataCentre, user_id);
    }

    @PutMapping(value = "/datacenter")
    public List<DataCentre> updateDatacentreDTO( @RequestBody DataCentre dataCentre) {
        return deviceService.updateDatacentre(dataCentre);
    }

    @GetMapping(value = "/datacenter/{dc-name}/{time}")
    public DataCentre findDatacenterProps(@PathVariable("dc-name") String dcName, @PathVariable("time") long time) {
        return deviceService.getDatacenterProperties(dcName, DateUtils.millisToLocalDateTime(time));
    }

    @GetMapping(value = "/datacenters/{time}")
    public List<DataCentre> findDatacenterProps(@PathVariable("time") long time) {
        return deviceService.getDatacenters(DateUtils.millisToLocalDateTime(time));
    }

    @GetMapping(value = "/user_datacenters/{time}/{username}")
    public List<DataCentre> findDatacenterProps2(@PathVariable("time") long time, @PathVariable("username") String username) {
        return userService.getUserDC(username, DateUtils.millisToLocalDateTime(time));
    }

    @DeleteMapping(value = "/{device-label}")
    public boolean findDatacenterProps(@PathVariable("device-label") String deviceLabel) {
        return deviceService.deleteDevice( deviceLabel);
    }
    @GetMapping(value = "/empty")
    public List<Component>  getEmptyObjects() {
        List<Component> list = new ArrayList<>();
        Battery battery = new Battery();
        HeatRecoveryInfrastructure heat = new HeatRecoveryInfrastructure();
        ThermalEnergyStorage thermal = new ThermalEnergyStorage();
        CoolingSystem cooling = new CoolingSystem();
        ServerRoom server = new ServerRoom();
        list.add(server);
        list.add(cooling);
        list.add(thermal);
        list.add(heat);
        list.add(battery);
        return list;
    }


}