package ro.tuc.dsrl.dbapi.controller.prediction.electrical;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/electrical/prediction/consumption")
public class ElectricalEnergyConsumptionPredictionController {

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    private final PredictedValueService predictedValueService;
    private final DeviceService deviceService;

    @Value("${datacenter.name}")
    private String dataCenterName;

    @Autowired
    public ElectricalEnergyConsumptionPredictionController(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService,
                                                           PredictedValueService predictedValueService,
                                                           DeviceService deviceService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
        this.predictedValueService = predictedValueService;
        this.deviceService = deviceService;
    }

    //region IntraDay Get Methods
    @GetMapping(value = "/intraday/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfIntraDayConsumptionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());

    }

    @GetMapping(value = "/intraday/{dataCenterID}/itComponent/{startTime}")
    public List<EnergyProfileDTO> findElectricalPredictionOfIntraDayConsumptionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/intraday/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfIntraDayConsumptionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForCoolingSystem(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/intraday/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfIntraDayConsumptionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);
        UUID serverUUID = UUID.fromString(serverID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForServer(
                dataCenterUUID,
                serverUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name());
    }

    //endregion


    //region DayAhead Get Methods
    @GetMapping(value = "/dayahead/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfDayAheadConsumptionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }


    @GetMapping(value = "/dayahead/{dataCenterID}/itComponent/{startTime}")
    public List<EnergyProfileDTO> findElectricalPredictionOfDayAheadConsumptionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/dayahead/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfDayAheadConsumptionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForCoolingSystem(dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/dayahead/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfDayAheadConsumptionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("serverID") String serverID){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);
        UUID serverUUID = UUID.fromString(serverID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForServer(
                dataCenterUUID,
                serverUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name());
    }
    //endregion


    //region NearRealTime Get Methods
    @GetMapping(value = "/nearRealTime/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfNearRealtimeConsumptionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime){

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);


        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForEntireDC(dataCenterID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/nearRealTime/itComponent/{startTime}")
    public List<EnergyProfileDTO> findElectricalPredictionOfNearRealtimeConsumptionForITComponentInInterval(
            @PathVariable("startTime") Long startTime){

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForITComponent(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/nearRealTime/coolingSystem/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfNearRealtimeConsumptionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime){

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForCoolingSystem(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                PredictionType.ENERGY_CONSUMPTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/nearRealTime/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfNearRealtimeConsumptionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);
        UUID serverUUID = UUID.fromString(serverID);

        return energyHistoricalPredictedValuesService.getPredictedConsumptionForServer(
                dataCenterUUID,
                serverUUID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name());
    }
    //endregion


    //region Insertions
    @PostMapping(value = {"/{dataCenterID}/entireDC",
            "/{dataCenterID}/itComponent",
            "/{dataCenterID}/CoolingSystem"})
    public void insertEnergyConsumptionPredictionForEntireDC(@PathVariable("dataCenterID") String dataCenterID,
                                                             @RequestBody PredictedValueDTO predictedValueDTO){

        UUIDPreconditions.validate(dataCenterID);

        predictedValueService.insert(predictedValueDTO);
    }

    @PostMapping(value = "/{dataCenterID}/server/{serverID}")
    public void insertEnergyConsumptionPredictionForServer(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @RequestBody PredictedValueDTO predictedValueDTO){

        UUIDPreconditions.validate(dataCenterID);
        UUIDPreconditions.validate(serverID);

        predictedValueService.insert(predictedValueDTO);

    }
    //endregion
}
