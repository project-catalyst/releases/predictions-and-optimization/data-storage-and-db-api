package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.DeviceType;

import java.util.UUID;

@Repository
public interface DeviceTypeRepository extends JpaRepository<DeviceType, UUID> {

    @Query(value = "SELECT d " +
            "FROM DeviceType d " +
            "WHERE d.id = :id")
    DeviceType findByUUId(@Param("id") UUID deviceTypeUUID);

    @Query(value = "SELECT d " +
            "FROM DeviceType d " +
            "WHERE d.type = :type")
    DeviceType findByType(@Param("type") String deviceType);


}
