package ro.tuc.dsrl.dbapi.controller.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * @author Vlad Lazar
 */
@RestController
@RequestMapping(value = "/test-marketplace-connector")
public class TestMarketPlaceConnectorComponent {

    @RequestMapping(value = "/activeSessions/{marketActorName}/{marketplaceForm}/{timeframe}/", method = RequestMethod.GET)
    public String getActiveSessions(@PathVariable("marketActorName") String marketActorName,
                                    @PathVariable("marketplaceForm") String marketplaceForm,
                                    @PathVariable("timeframe") String timeframe) {


        return "[\n" +
                "    {\n" +
                "        \"deliveryEndTime\": \"2020-05-29T19:00:00Z\",\n" +
                "        \"sessionEndTime\": \"2020-05-29T16:34:00Z\",\n" +
                "        \"sessionStartTime\": \"2020-05-29T16:00:00Z\",\n" +
                "        \"deliveryStartTime\": \"2020-05-29T18:00:00Z\"\n" +
                "    }\n" +
                "]";

    }

    @RequestMapping(value = "/marketActions/{marketActorName}/{marketplaceForm}/{timeframe}/", method = RequestMethod.GET)
    public String getMarketActions(@PathVariable("marketActorName") String marketActorName,
                                   @PathVariable("marketplaceForm") String marketplaceForm,
                                   @PathVariable("timeframe") String timeframe) {


        return "[\n" +
                "{\n" +
                "\"id\": 1,\n" +
                "\"date\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionStartTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionEndTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"value\": 10.000,\n" +
                "\"uom\": \"Kwh\",\n" +
                "\"price\": 10.000,\n" +
                "\"actionType\": \"bid\",\n" +
                "\"status\": \"valid\"\n" +
                "},\n" +
                "{\n" +
                "\"id\": 2,\n" +
                "\"date\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionStartTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionEndTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"value\": 10.000,\n" +
                "\"uom\": \"Kwh\",\n" +
                "\"price\": 10.000,\n" +
                "\"actionType\": \"offer\",\n" +
                "\"status\": \"invalid\"\n" +
                "}\n" +
                "]";

    }

    @RequestMapping(value = "/marketActions/{marketActorName}/{marketplaceForm}/{timeframe}/", method = RequestMethod.POST)
    public String registerActions(@PathVariable("marketActorName") String marketActorName,
                                  @PathVariable("marketplaceForm") String marketplaceForm,
                                  @PathVariable("timeframe") String timeframe,
                                  @RequestBody List<Object> actions) throws IOException {


        ObjectMapper objMapper = new ObjectMapper();

        String actionsBody = objMapper.writeValueAsString(actions);
        String actionExpected = "[\n" +
                "{\n" +
                "\"date\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionStartTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionEndTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"value\": 10.000,\n" +
                "\"uom\": \"Kwh\",\n" +
                "\"price\": 10.000,\n" +
                "\"actionType\": \"bid\",\n" +
                "},\n" +
                "{\n" +
                "\"date\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionStartTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionEndTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"value\": 10.000,\n" +
                "\"uom\": \"Kwh\",\n" +
                "\"price\": 10.000,\n" +
                "\"actionType\": \"offer\",\n" +
                "}\n" +
                "]";

        JsonNode actionBodyNode = objMapper.readTree(actionsBody);
        JsonNode actionsExpectedNode = objMapper.readTree(actionExpected);


        if(!actionBodyNode.equals(actionsExpectedNode)){
            return "";
        }

        return "[\n" +
                "{\n" +
                "\"id\": 1,\n" +
                "\"date\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionStartTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionEndTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"value\": \"10.000\",\n" +
                "\"uom\": \"Kwh\",\n" +
                "\"price\": \"10.000\",\n" +
                "\"actionType\": \"bid\",\n" +
                "\"status\": \"valid\"\n" +
                "},\n" +
                "{\n" +
                "\"id\": 2,\n" +
                "\"date\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionStartTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"actionEndTime\": \"2020-01-20T16:00:00Z\",\n" +
                "\"value\": \"10.000\",\n" +
                "\"uom\": \"Kwh\",\n" +
                "\"price\": \"10.000\",\n" +
                "\"actionType\": \"offer\",\n" +
                "\"status\": \"invalid\"\n" +
                "}\n" +
                "]";

    }

    @RequestMapping(value = "/correlatedMarketActions/{marketActorName}/{timeframe}/", method = RequestMethod.POST)
    public String registerMarketCorrelations(@PathVariable("marketActorName") String marketActorName,
                                             @PathVariable("timeframe") String timeframe,
                                             @RequestBody Object marketCorrelation) throws IOException {


        ObjectMapper objMapper = new ObjectMapper();

        String correlationBody = objMapper.writeValueAsString(marketCorrelation);
        String correlationExpected = "{\n" +
                "        \"id\": 0,\n" +
                "        \"constraintType\": \"ALL OR NOTHING\",\n" +
                "        \"marketActions\": \n" +
                "        [\t{\n" +
                "            \t\"marketplaceForm\": \"electric_energy\",\n" +
                "            \t\"actionId\": 10\n" +
                "        \t}, \n" +
                "        \t{\n" +
                "            \t\"marketplaceForm\": \"thermal\",\n" +
                "            \t\"actionId\": 20\n" +
                "        \t}\n" +
                "        ]\n" +
                "}";

        JsonNode correlationBodyNode = objMapper.readTree(correlationBody);
        JsonNode correlationExpectedNode = objMapper.readTree(correlationExpected);


        if(!correlationBodyNode.equals(correlationExpectedNode)){
            return "";
        }

        return "{\n" +
                "\"id\": 1,\n" +
                "        \"constraintType\": \"ALL OR NOTHING\",\n" +
                "        \"marketActions\": \n" +
                "        [\t{\n" +
                "            \t\"marketplaceForm\": \"electric_energy\",\n" +
                "            \t\"actionId\": 10\n" +
                "        \t}, \n" +
                "        \t{\n" +
                "            \t\"marketplaceForm\": \"thermal\",\n" +
                "            \t\"actionId\": 20\n" +
                "        \t}\n" +
                "        ]" +
                "}\n";


    }


    @RequestMapping(value = "/referencePricesPreviousDay/{marketActorName}/{marketplaceForm}/{timeframe}/", method = RequestMethod.GET)
    public String getReferencePricePreviousDay(@PathVariable("marketActorName") String marketActorName,
                                               @PathVariable("marketplaceForm") String marketplaceForm,
                                               @PathVariable("timeframe") String timeframe) {


        return "\n" +
                "[{\n" +
                "        \"validityStartTime\": \"2020-06-07T00:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T00:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T01:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T01:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T02:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T02:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T03:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T03:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T04:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T04:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T05:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T05:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T06:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T06:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T07:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T07:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T08:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T08:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T09:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T09:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T10:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T10:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T11:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T11:59:59Z\",\n" +
                "        \"referencePrice\": 222.333\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T12:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T12:59:59Z\",\n" +
                "        \"referencePrice\": 333.444\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T13:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T13:59:59Z\",\n" +
                "        \"referencePrice\": 333.444\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T14:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T14:59:59Z\",\n" +
                "        \"referencePrice\": 333.444\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T15:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T15:59:59Z\",\n" +
                "        \"referencePrice\": 333.444\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T16:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T16:59:59Z\",\n" +
                "        \"referencePrice\": 333.444\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T17:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T17:59:59Z\",\n" +
                "        \"referencePrice\": 333.444\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T18:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T18:59:59Z\",\n" +
                "        \"referencePrice\": 444.555\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T21:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T21:59:59Z\",\n" +
                "        \"referencePrice\": 444.555\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T22:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T22:59:59Z\",\n" +
                "        \"referencePrice\": 444.555\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-06-07T23:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-06-07T23:59:59Z\",\n" +
                "        \"referencePrice\": 444.555\n" +
                "}]\n";

    }

    @RequestMapping(value = "/clearingPricesPreviousDay/{marketActorName}/{marketplaceForm}/{timeframe}/", method = RequestMethod.GET)
    public String getClearingPricePreviousDay(@PathVariable("marketActorName") String marketActorName,
                                              @PathVariable("marketplaceForm") String marketplaceForm,
                                              @PathVariable("timeframe") String timeframe) {


        return "\n" +
                "[{\n" +
                "        \"validityStartTime\": \"2020-05-27T00:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T00:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T01:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T01:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T02:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T02:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T03:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T03:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T04:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T04:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T05:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T05:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T06:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T06:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T07:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T07:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T08:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T08:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T09:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T09:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T10:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T10:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T11:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T11:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T12:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T12:59:59Z\",\n" +
                "        \"clearingPrice\": 110\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T13:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T13:59:59Z\",\n" +
                "        \"clearingPrice\": 120\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T14:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T14:59:59Z\",\n" +
                "        \"clearingPrice\": 120\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T15:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T15:59:59Z\",\n" +
                "        \"clearingPrice\": 120\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T18:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T18:59:59Z\",\n" +
                "        \"clearingPrice\": 145\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T19:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T19:59:59Z\",\n" +
                "        \"clearingPrice\": 145\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T20:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T20:59:59Z\",\n" +
                "        \"clearingPrice\": 145\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T21:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T21:59:59Z\",\n" +
                "        \"clearingPrice\": 145\n" +
                "}, {\n" +
                "        \"validityStartTime\": \"2020-05-27T23:00:00Z\",\n" +
                "        \"validityEndTime\": \"2020-05-27T23:59:59Z\",\n" +
                "        \"clearingPrice\": 115.667\n" +
                "}]";

    }


}
