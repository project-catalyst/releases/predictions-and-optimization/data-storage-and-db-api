package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.MonitoredValue;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface MonitoredValueRepository extends JpaRepository<MonitoredValue, UUID> {

    @Query(value = "SELECT m " +
            "FROM MonitoredValue m " +
            "WHERE m.id = :monitoredValueUUID")
    MonitoredValue findByUUID(@Param("monitoredValueUUID") UUID monitoredValueUUID);

    @Query(value = "SELECT new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "where " +
            "dev.parent.id = :datacenterId AND " +
            "mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) AND " +
            "mv.timestamp >= :startTime AND " +
            "mv.timestamp < :endTime " +
            "GROUP BY dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample " +
            "ORDER BY dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample")
    List<AggregatedValuesDTO> findDataInIntervalForEntireDC(@Param("datacenterId") UUID datacenterId,
                                                            @Param("startTime") LocalDateTime startTime,
                                                            @Param("endTime") LocalDateTime endTime,
                                                            @Param("energyType") String energyType,
                                                            @Param("predictionType") String predictionType,
                                                            @Param("minutesPerSample") int minutesPerSample);

    @Query(value = "SELECT new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "where " +
            "(dev.parent.id = :datacenterId OR dev.id = :datacenterId) AND " +
            "mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) AND " +
            "mv.timestamp >= :startTime AND " +
            "mv.timestamp < :endTime " +
            "GROUP BY YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp),  " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample ,dev, mes " +
            "ORDER BY  YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev, mes")
    List<AggregatedValuesDTO> findDataInIntervalForEntireDCFixed(@Param("datacenterId") UUID datacenterId,
                                                                 @Param("startTime") LocalDateTime startTime,
                                                                 @Param("endTime") LocalDateTime endTime,
                                                                 @Param("energyType") String energyType,
                                                                 @Param("predictionType") String predictionType,
                                                                 @Param("minutesPerSample") int minutesPerSample);


    @Query(value = "SELECT new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "where " +
            "dev.id = :datacenterId AND " +
            "mes.property LIKE :drSignal  AND " +
            "mv.timestamp >= :startTime AND " +
            "mv.timestamp < :endTime " +
            "GROUP BY dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample " +
            "ORDER BY dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample")
    List<AggregatedValuesDTO> findPropertyInInterval(@Param("datacenterId") UUID datacenterId,
                                                     @Param("startTime") LocalDateTime startTime,
                                                     @Param("endTime") LocalDateTime endTime,
                                                     @Param("drSignal") String drSignal,
                                                     @Param("minutesPerSample") int minutesPerSample);


    @Query(value = "SELECT AVG(mv.value), YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) " +
            "FROM monitored_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE ( dev.label like :label  OR p.label like :label) " +
            "AND mes.property LIKE  :propertyType " +
            "AND mv.timestamp >= :startTime " +
            "AND  mv.timestamp < :endTime " +
            "GROUP BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample)  " +
            "ORDER BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample)  ",  nativeQuery = true)
    Object[][] getAveragedMonitoredValues(@Param("label") String label,
                                            @Param("startTime") LocalDateTime startTime,
                                            @Param("endTime") LocalDateTime endTime,
                                            @Param("minutesPerSample") int minutesPerSample,
                                            @Param("propertyType") String propertyType );

    @Query(value = "SELECT AVG(mv.value), YEAR(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) " +
            "FROM monitored_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE ( dev.label like :label  OR p.label like :label) " +
            "AND mes.property LIKE  :propertyType " +
            "AND mv.timestamp >= :startTime " +
            "AND  mv.timestamp < :endTime " +
            "GROUP BY YEAR(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample)  " +
            "ORDER BY YEAR(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample)  ",  nativeQuery = true)
    Object[][] getAveragedMonitoredValuesGroupedByYear(@Param("label") String label,
                                          @Param("startTime") LocalDateTime startTime,
                                          @Param("endTime") LocalDateTime endTime,
                                          @Param("minutesPerSample") int minutesPerSample,
                                          @Param("propertyType") String propertyType );



    @Query(value = "SELECT " +
            "new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "WHERE " +
            "dev.parent.id = :datacenterId AND " +
            "dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) AND " +
            "mv.timestamp >= :startTime AND " +
            "mv.timestamp < :endTime " +
            "GROUP BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample " +
            "ORDER BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample")
    List<AggregatedValuesDTO> findDataInIntervalForComponent(@Param("datacenterId") UUID datacenterId,
                                                             @Param("startTime") LocalDateTime startTime,
                                                             @Param("endTime") LocalDateTime endTime,
                                                             @Param("energyType") String energyType,
                                                             @Param("componentType") String componentType,
                                                             @Param("predictionType") String predictionType,
                                                             @Param("minutesPerSample") int minutesPerSample);

    // TODO: METHOD NOT WORKING! ONLY COPY-PASTED SO IT WOULDN'T THROW AN ERROR AT COMPILE TIME
    @Query(value = "SELECT m " +
            "FROM MonitoredValue m " +
            "INNER JOIN FETCH m.measurementId " +
            "INNER JOIN FETCH m.deviceId " +
            "WHERE m.deviceId.parent.id = :datacenterId " +
            "AND m.deviceId.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND m.measurementId.property LIKE UPPER(CONCAT('%',:energyType,'%')) " +
            "AND m.measurementId.property LIKE UPPER(CONCAT('%', :predictionType, '%')) " +
            "AND (m.timestamp >= :startTime AND m.timestamp < :endTime) " +
            "AND :serverID = :serverID AND :componentName = :componentName " +
            "ORDER BY m.timestamp")
    List<MonitoredValue> findDataInIntervalForComponentServer(@Param("datacenterId") UUID dataCenterID,
                                                              @Param("serverID") UUID serverID,
                                                              @Param("startTime") LocalDateTime startTime,
                                                              @Param("endTime") LocalDateTime endTime,
                                                              @Param("energyType") String energyType,
                                                              @Param("componentName") String componentName, // this is not mandatory
                                                              @Param("predictionType") String predictionType);

    @Query(value = "SELECT new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "where " +
            "dev.parent.id = :datacenterId AND " +
            "mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) AND " +
            "mv.timestamp < :endTime " +
            "GROUP BY dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample " +
            "ORDER BY dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample")
    List<AggregatedValuesDTO> findDataBeforeDateForEntireDC(@Param("datacenterId") UUID datacenterId,
                                                            @Param("endTime") LocalDateTime endTime,
                                                            @Param("energyType") String energyType,
                                                            @Param("predictionType") String predictionType,
                                                            @Param("minutesPerSample") int minutesPerSample);

    @Query(value = "SELECT " +
            "new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "WHERE " +
            "dev.parent.id = :datacenterId AND " +
            "dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) AND " +
            "mv.timestamp < :endTime " +
            "GROUP BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample " +
            "ORDER BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample")
    List<AggregatedValuesDTO> findDataBeforeDateForComponent(@Param("datacenterId") UUID datacenterId,
                                                             @Param("endTime") LocalDateTime endTime,
                                                             @Param("energyType") String energyType,
                                                             @Param("componentType") String componentType,
                                                             @Param("predictionType") String predictionType,
                                                             @Param("minutesPerSample") int minutesPerSample);

    
    @Query(value = "SELECT AVG(mv.value), YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id as devId , mes.id as mesId " +
            "FROM monitored_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE p.id = :datacenterId " +
            "AND dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) " +
            "AND mv.timestamp >= :startTime " +
            "AND  mv.timestamp < :endTime " +
            "GROUP BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id " +
            "ORDER BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id ",  nativeQuery = true)
    Object[][] findAveragedSignalsInIntervalForComponent(@Param("datacenterId") UUID datacenterId,
                                             @Param("startTime") LocalDateTime startTime,
                                             @Param("endTime") LocalDateTime endTime,
                                             @Param("energyType") String energyType,
                                             @Param("componentType") String componentType,
                                             @Param("predictionType") String predictionType,
                                             @Param("minutesPerSample") int minutesPerSample);

    @Query(value = "SELECT AVG(mv.value), YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id as devId , mes.id as mesId " +
            "FROM monitored_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE p.id = :datacenterId " +
            "AND dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :measurementType, '%')) " +
            "AND mv.timestamp >= :startTime " +
            "AND  mv.timestamp < :endTime " +
            "GROUP BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id " +
            "ORDER BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id ",  nativeQuery = true)
    Object[][] findEstimatedProfileValues(@Param("datacenterId") UUID datacenterId,
                                                     @Param("startTime") LocalDateTime startTime,
                                                     @Param("endTime") LocalDateTime endTime,
                                                     @Param("energyType") String energyType,
                                                     @Param("componentType") String componentType,
                                                     @Param("measurementType") String measurementType,
                                                     @Param("predictionType") String predictionType,
                                                     @Param("minutesPerSample") int minutesPerSample);


    @Query(value = "SELECT AVG(mv.value), YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id as devId , mes.id as mesId " +
            "FROM monitored_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE p.id = :datacenterId " +
            "AND dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :measurementType, '%')) " +
            "AND  mv.timestamp < :endTime " +
            "GROUP BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id " +
            "ORDER BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id ",  nativeQuery = true)
    Object[][] findEstimatedProfileValuesBeforeDate(@Param("datacenterId") UUID datacenterId,
                                          @Param("endTime") LocalDateTime endTime,
                                          @Param("energyType") String energyType,
                                          @Param("componentType") String componentType,
                                          @Param("measurementType") String measurementType,
                                          @Param("predictionType") String predictionType,
                                          @Param("minutesPerSample") int minutesPerSample);

    @Query(value = "SELECT " +
            "new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "WHERE " +
            "dev.parent.id = :datacenterId AND " +
            "dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) AND " +
            "mv.timestamp >= :startTime AND mv.timestamp <= :endTime " +
            "GROUP BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample " +
            "ORDER BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample")
    List<AggregatedValuesDTO> findDataBetweenDatesForEntireDC(@Param("datacenterId") UUID datacenterId,
                                                              @Param("startTime") LocalDateTime startTime,
                                                              @Param("endTime") LocalDateTime endTime,
                                                              @Param("energyType") String energyType,
                                                              @Param("predictionType") String predictionType,
                                                              @Param("minutesPerSample") int minutesPerSample);

    @Query(value = "SELECT " +
            "new ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO(" +
            "AVG(mv.value), dev, mes, YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample) " +
            "FROM MonitoredValue mv " +
            "INNER JOIN mv.deviceId dev " +
            "INNER JOIN mv.measurementId mes " +
            "WHERE " +
            "dev.parent.id = :datacenterId AND " +
            "dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) AND " +
            "mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) AND " +
            "mv.timestamp >= :startTime AND mv.timestamp <= :endTime " +
            "GROUP BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample " +
            "ORDER BY " +
            "dev, mes, YEAR(mv.timestamp), Month(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), " +
            "floor(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample")
    List<AggregatedValuesDTO> findDataBetweenDatesForComponent(@Param("datacenterId") UUID datacenterId,
                                                               @Param("startTime") LocalDateTime startTime,
                                                               @Param("endTime") LocalDateTime endTime,
                                                               @Param("energyType") String energyType,
                                                               @Param("componentType") String componentType,
                                                               @Param("predictionType") String predictionType,
                                                               @Param("minutesPerSample") int minutesPerSample);

    @Query(value = "SELECT AVG(mv.value), YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id as devId , mes.id as mesId " +
            "FROM monitored_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE p.id = :datacenterId " +
            "AND dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :measurementType, '%')) " +
            "AND mv.timestamp >= :startTime AND mv.timestamp <= :endTime " +
            "GROUP BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id " +
            "ORDER BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp), FLOOR(MINUTE(mv.timestamp)/:minutesPerSample) * :minutesPerSample, dev.id, mes.id ",  nativeQuery = true)
    Object[][] findEstimatedProfileValuesBetweenDates(@Param("datacenterId") UUID datacenterId,
                                                      @Param("startTime") LocalDateTime startTime,
                                                      @Param("endTime") LocalDateTime endTime,
                                                      @Param("energyType") String energyType,
                                                      @Param("componentType") String componentType,
                                                      @Param("measurementType") String measurementType,
                                                      @Param("predictionType") String predictionType,
                                                      @Param("minutesPerSample") int minutesPerSample);

}
