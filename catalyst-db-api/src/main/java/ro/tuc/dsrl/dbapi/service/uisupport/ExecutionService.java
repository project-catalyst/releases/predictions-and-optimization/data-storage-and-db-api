package ro.tuc.dsrl.dbapi.service.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.resources.CopFactors;
import ro.tuc.dsrl.dbapi.model.dto.resources.LossFactors;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.ExecutionNotOptimizedDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.ExecutionOptimizedDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.catalyst.model.utils.EnergyArraysUtility;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.MonitoredValueService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.EnergyProfileFormatter;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.HeatProfileFormatter;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.ProfileFormatter;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

@Service
public class ExecutionService {
    @Value("${is.pilot}")
    private boolean isPilot;

    public static final long RT_HOUR_SLOTS = ChronoUnit.HOURS.getDuration().toMillis() / (ChronoUnit.MINUTES.getDuration().toMillis() * OptimizerConfig.MONITORING_RATE_IN_MINUTES);

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    private final DeviceService deviceService;
    private final MonitoredValueService monitoredValueService;

    @Autowired
    public ExecutionService(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService,
                            DeviceService deviceService, MonitoredValueService monitoredValueService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
        this.deviceService = deviceService;
        this.monitoredValueService = monitoredValueService;
    }

    public ExecutionOptimizedDTO getOptimizedEnergyValues(LocalDateTime untilDate, String datacenter, PredictionGranularity predictionGranularity) {

        TimePeriod period = getRTPeriod(untilDate, predictionGranularity);
        LocalDateTime startDate = period.getStartTime();
        LocalDateTime endDate = period.getEndTime();

        int startHour = startDate.getHour();

        EnergyProfileFormatter formatter = (EnergyProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);

        // ========= UPS ==========
        List<AggregatedValuesDTO> values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.BATTERY_CURRENT);

        List<Double> upsState = formatter.populateArray(values, startHour, predictionGranularity);
        LossFactors factorsUPS = deviceService.getBatteryLossFactors(datacenter, startDate.minusMinutes(5));
        List<Double> upsChargeValues = new ArrayList<>();
        List<Double> upsDischargeValues = new ArrayList<>();
        computeChargeDischargeUPS(factorsUPS,upsState, upsChargeValues, upsDischargeValues);

        // ========= TES ==========
        values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.TES_CURRENT);

        List<Double> tesState = formatter.populateArray(values, startHour, predictionGranularity);
        LossFactors factorsTES = deviceService.getTESLossFactors(datacenter, startDate.minusMinutes(5));
        List<Double> tesChargeValues = new ArrayList<>();
        List<Double> tesDischargeValues = new ArrayList<>();
        computeChargeDischargeTES(factorsTES, tesState, tesChargeValues, tesDischargeValues);

        // ========= DT Workload ==========
        values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
        List<Double> consumptionDTValues = formatter.populateArray(values, startHour, predictionGranularity);

        // ========= RT Workload ==========
        values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);

        List<Double> consumptionRTValues = formatter.populateArray(values, startHour,predictionGranularity);

        // ========= Cooling Values ==========
        values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION);

        List<Double> coolingValuesMonitored = formatter.populateArray(values, startHour, predictionGranularity);
        List<Double> coolingValues = EnergyArraysUtility.revertCoolingValuesFromActions(coolingValuesMonitored, tesChargeValues, tesDischargeValues);

        // ========= DC Baseline  ==========
        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DATA_CENTER_BASELINE);
        List<Double> baseline = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(), startHour);

        // =========  DR Signal  ==========
        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DR_SIGNAL);
        List<Double> drSignal = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(),startHour);

        // =========  Optimized DC  ==========
        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.OPTIMIZED_DC);
        List<Double> optimizedDC = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(), startHour);

        List<Double> delta = EnergyArraysUtility.computeDeliveredFlexibility(baseline, optimizedDC);


        ExecutionOptimizedDTO executionOptimizedDTO = new ExecutionOptimizedDTO(predictionGranularity.getNoOutputs());
        // Consumption
        executionOptimizedDTO.setConsumptionDTValues(consumptionDTValues);
        executionOptimizedDTO.setConsumptionRTValues(consumptionRTValues);
        executionOptimizedDTO.setCoolingValues(coolingValues);
        executionOptimizedDTO.setUpsChargeValues(upsChargeValues);
        executionOptimizedDTO.setTesChargeValues(tesChargeValues);

        // Production
        executionOptimizedDTO.setUpsDischargeValues(upsDischargeValues);
        executionOptimizedDTO.setTesDischargeValues(tesDischargeValues);
        executionOptimizedDTO.setProvidedEnergyValues(optimizedDC);
        executionOptimizedDTO.setFlexibilityOrder(drSignal);
        executionOptimizedDTO.setDeltaDiff(delta);
        return executionOptimizedDTO;

    }

    public ExecutionOptimizedDTO getOptimizedPriceDrivenValues(LocalDateTime untilDate, String datacenter, PredictionGranularity predictionGranularity) {
        TimePeriod period = getRTPeriod(untilDate, predictionGranularity);
        LocalDateTime startDate = period.getStartTime();
        LocalDateTime endDate = period.getEndTime();
        int startHour = startDate.getHour();

        ExecutionOptimizedDTO results = getOptimizedEnergyValues(untilDate, datacenter, predictionGranularity);
        // =========  Thermal Price  ==========
        EnergyProfileFormatter formatter = (EnergyProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DC_T_PRICE);
        List<Double> refrencePrices = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(),startHour);

        results.setRefrencePrices(refrencePrices);
        return  results;
    }


    public ExecutionOptimizedDTO getOptimizedRenewableValues(LocalDateTime untilDate, String datacenter, PredictionGranularity predictionGranularity) {
        TimePeriod period = getRTPeriod(untilDate, predictionGranularity);
        LocalDateTime startDate = period.getStartTime();
        LocalDateTime endDate = period.getEndTime();
        int startHour = startDate.getHour();

        ExecutionOptimizedDTO results = getOptimizedEnergyValues(untilDate, datacenter, predictionGranularity);
        // =========  Renewable  ==========
        EnergyProfileFormatter formatter = (EnergyProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE);
        List<Double> renewable = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(),startHour);

        results.setRenewableEnergyValues(renewable);
        return  results;
    }


    public ExecutionOptimizedDTO getOptimizedThermalValues(LocalDateTime untilDate, String datacenter, PredictionGranularity predictionGranularity) {

        TimePeriod period = getRTPeriod(untilDate, predictionGranularity);
        LocalDateTime startDate = period.getStartTime();
        LocalDateTime endDate = period.getEndTime();
        int startHour = startDate.getHour();

        HeatProfileFormatter formatter = (HeatProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.HEAT_PROFILE);
        CopFactors copFactors = deviceService.getCopFactors(datacenter, startDate);
        formatter.setCopFactors(copFactors);

        // ========= UPS ==========
        List<Double> upsChargeValues = new ArrayList<Double>(Collections.nCopies(predictionGranularity.getNoOutputs(), 0.0));
        List<Double> upsDischargeValues = new ArrayList<Double>(Collections.nCopies(predictionGranularity.getNoOutputs(), 0.0));

        // ========= TES ==========
        List<AggregatedValuesDTO> values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.TES_CURRENT);

        List<Double> tesState = formatter.populateArray(values, startHour, predictionGranularity);
        LossFactors factorsTES = deviceService.getTESLossFactors(datacenter, startDate.minusMinutes(5));
        double tesThermalInit = copFactors.getThermalEquivalentForElectricalEnergy(factorsTES.getLoadState());
        factorsTES.setLoadState(tesThermalInit);
        List<Double> tesChargeValues = new ArrayList<>();
        List<Double> tesDischargeValues = new ArrayList<>();
        computeChargeDischargeTES(factorsTES, tesState, tesChargeValues, tesDischargeValues);


        // ========= DT Workload ==========
         values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
        List<Double> consumptionDTValues = formatter.populateArray(values, startHour, predictionGranularity);

        // ========= RT Workload ==========
        values = monitoredValueService.getAveragedMonitoredValues(datacenter,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);

        List<Double> consumptionRTValues = formatter.populateArray(values, startHour, predictionGranularity);

        // ========= Cooling Values ==========
        List<Double> coolingValues = new ArrayList<Double>(Collections.nCopies(predictionGranularity.getNoOutputs(), 0.0));

        // ========= DC Thermal Baseline  ==========
        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DATA_CENTER_HEAT_BASELINE);
        List<Double> baseline = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(),startHour);

        // =========  Thermal Price  ==========
        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DC_T_PRICE);
        List<Double> refrencePrices = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(),startHour);

        // =========  Optimized Thermal  ==========
        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenter,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.OPTIMIZED_THERMAL);
        List<Double> optimizedThermalDC = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(),startHour);

        List<Double> delta = EnergyArraysUtility.computeDeliveredFlexibility(baseline, optimizedThermalDC);

        ExecutionOptimizedDTO executionOptimizedDTO = new ExecutionOptimizedDTO(predictionGranularity.getNoOutputs());
        // Consumption
        executionOptimizedDTO.setConsumptionDTValues(consumptionDTValues);
        executionOptimizedDTO.setConsumptionRTValues(consumptionRTValues);
        executionOptimizedDTO.setCoolingValues(coolingValues);
        executionOptimizedDTO.setUpsChargeValues(upsChargeValues);
        executionOptimizedDTO.setTesChargeValues(tesChargeValues);
        // Production
        executionOptimizedDTO.setUpsDischargeValues(upsDischargeValues);
        executionOptimizedDTO.setTesDischargeValues(tesDischargeValues);
        executionOptimizedDTO.setProvidedEnergyValues(optimizedThermalDC);
        executionOptimizedDTO.setRefrencePrices(refrencePrices);
        executionOptimizedDTO.setDeltaDiff(delta);

        return executionOptimizedDTO;

    }


    /**
     * TODO: For Pilot, consider Forecasting for Non-Optimized Charts
     * - getForecastedNonOptimizedValues
     * <p>
     * TODO: For Scenarios (& pilots without execution), consider pre-optimized monitored values for Non-Optimized Charts
     * -getMonitoredPreOptimizedValues
     */
    public ExecutionNotOptimizedDTO getNonOptimizedEnergyValues(LocalDateTime untilDate, String datacenterName, PredictionGranularity granularity) {
        TimePeriod period = getRTPeriod(untilDate, granularity);
        LocalDateTime startDate = period.getStartTime();
        LocalDateTime endDate = period.getEndTime();

        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);

        ExecutionNotOptimizedDTO pageExec;

        if (isPilot) {
             pageExec =  getForecastedNonOptimizedValues(datacenterName, startDate,endDate, granularity,formatter);
        } else {
            pageExec = getMonitoredPreOptimizedValues(datacenterName, startDate, endDate, granularity, formatter);
        }

        List<AggregatedValuesDTO> values = monitoredValueService.getAveragedMonitoredValues(datacenterName,
                startDate,
                endDate,
                granularity,
                MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE);
        List<Double> renewable = formatter.populateArray(values, startDate.getHour(), granularity);
        pageExec.setGreenEnergyValues(renewable);
        return pageExec;
    }

    public ExecutionNotOptimizedDTO getNonOptimizedThermalValues(LocalDateTime untilDate, String datacenterName, PredictionGranularity granularity) {
        TimePeriod period = getRTPeriod(untilDate, granularity);
        LocalDateTime startDate = period.getStartTime();
        LocalDateTime endDate = period.getEndTime();

        HeatProfileFormatter formatter = (HeatProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.HEAT_PROFILE);
        CopFactors copFactors = deviceService.getCopFactors(datacenterName, startDate);
        formatter.setCopFactors(copFactors);
        ExecutionNotOptimizedDTO toReturn;
        if (isPilot) {
            toReturn = getForecastedNonOptimizedValues(datacenterName, startDate,endDate, granularity, formatter);
        } else {
            toReturn = getMonitoredPreOptimizedValues(datacenterName, startDate, endDate, granularity, formatter);
        }
        toReturn.setCoolingValues(new ArrayList<Double>(Collections.nCopies(granularity.getNoOutputs(), 0.0)));
        return toReturn;
    }

    private ExecutionNotOptimizedDTO getMonitoredPreOptimizedValues(String datacenterName, LocalDateTime startDate, LocalDateTime endDate, PredictionGranularity granularity, ProfileFormatter formatter) {
        int startHour = startDate.getHour();
        // ========= DT Workload ==========
        List<AggregatedValuesDTO> values = monitoredValueService.getAveragedMonitoredValues(datacenterName,
                startDate,
                endDate,
                granularity,
                MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION);
        List<Double> consumptionDTValues = formatter.populateArray(values, startHour, granularity);

        // ========= RT Workload ==========
        values = monitoredValueService.getAveragedMonitoredValues(datacenterName,
                startDate,
                endDate,
                granularity,
                MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        List<Double> consumptionRTValues = formatter.populateArray(values, startHour, granularity);

        List<Double> coolingValues =  computeCooling(datacenterName, startDate, consumptionDTValues, consumptionRTValues);

        ExecutionNotOptimizedDTO nonOptValues = new ExecutionNotOptimizedDTO(granularity.getNoOutputs());
        nonOptValues.setConsumptionDTValues(consumptionDTValues);
        nonOptValues.setConsumptionRTValues(consumptionRTValues);
        nonOptValues.setCoolingValues(coolingValues);
        return nonOptValues;
    }

    private List<Double> computeCooling(String datacenterName, LocalDateTime startDate, List<Double> consumptionDTValues, List<Double> consumptionRTValues) {
        List<Double> coolingValues = new ArrayList<>();
        CopFactors copFactors = deviceService.getCopFactors(datacenterName, startDate);
        double cop = copFactors.getCopC();

        assertTrue("size inconsistency " + consumptionDTValues.size() + " vs " + consumptionRTValues.size(),
                consumptionDTValues.size() == consumptionRTValues.size());
        for (int i = 0; i < consumptionDTValues.size(); i++) {
            double rt = consumptionRTValues.get(i);
            double dt = consumptionDTValues.get(i);
            double cool = (rt + dt) / cop;
            coolingValues.add(cool);
        }
        return coolingValues;
    }

    /**
     * TOOD: forecasted is per Intraday => 48 values
     * Configure for 288 as well
     */
    private ExecutionNotOptimizedDTO getForecastedNonOptimizedValues(String datacenterName, LocalDateTime startDate,  LocalDateTime endDate, PredictionGranularity predictionGranularity, ProfileFormatter formatter) {
        int startHour = startDate.getHour();
       List<AggregatedValuesDTO>  values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenterName,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
        List<Double> valuesDT = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(),startHour);


        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(datacenterName,
                startDate,
                endDate,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        List<Double> valuesRT = formatter.populateRealTimeArrayFromDayAyead(values, predictionGranularity.getNoOutputs(), startHour);


        List<Double> coolingValues =  computeCooling(datacenterName, startDate, valuesDT, valuesRT);
        ExecutionNotOptimizedDTO nonOptValues = new ExecutionNotOptimizedDTO(predictionGranularity.getNoOutputs());
        nonOptValues.setConsumptionDTValues(valuesDT);
        nonOptValues.setConsumptionRTValues(valuesRT);
        nonOptValues.setCoolingValues(coolingValues);
        return nonOptValues;
    }

    private void computeChargeDischargeUPS(LossFactors factors, List<Double> esdState, List<Double> upsChargeValues, List<Double> upsDischargeValues) {
        double esdChargeLossRate = factors.getChargeLossFactor();
        double esdDischargeLossRate = factors.getDischargeLossFactor();
        double initialState = factors.getLoadState();

        for (int i = 0; i < esdState.size(); i++) {
            double previousState = (i == 0) ? initialState : esdState.get(i - 1);
            if (previousState >= 0 && esdState.get(i) >= 0) {
                double diff = esdState.get(i) - previousState;
                diff = (OptimizerConfig.ID_ACTIVE) ? diff * RT_HOUR_SLOTS / 2 : diff * RT_HOUR_SLOTS;
                if (diff >= 0) {
                    upsChargeValues.add(diff * esdChargeLossRate);
                    upsDischargeValues.add(0.0);
                } else {
                    upsChargeValues.add(0.0);
                    upsDischargeValues.add(diff * esdDischargeLossRate);
                }
            } else {
                upsChargeValues.add(esdState.get(i));
                upsDischargeValues.add(esdState.get(i));
            }
        }
        System.out.println();
    }

    private void computeChargeDischargeTES( LossFactors factors , List<Double> tesState, List<Double> tesChargeValues, List<Double> tesDischargeValues) {
        double tesChargeLossRate = factors.getChargeLossFactor();
        double tesDischargeLossRate = factors.getDischargeLossFactor();
        double initialState = factors.getLoadState();

        for (int i = 0; i < tesState.size(); i++) {
            double previousState = (i == 0) ? initialState : tesState.get(i - 1);
            if (previousState >= 0 && tesState.get(i) >= 0) {
                double diff = tesState.get(i) - previousState;
                diff = (OptimizerConfig.ID_ACTIVE) ? diff * RT_HOUR_SLOTS / 2 : diff * RT_HOUR_SLOTS;
                if (diff >= 0) {
                    tesChargeValues.add(diff * tesChargeLossRate);
                    tesDischargeValues.add(0.0);
                } else {
                    tesChargeValues.add(0.0);
                    tesDischargeValues.add(diff * tesDischargeLossRate);
                }
            } else {
                tesChargeValues.add(tesState.get(i));
                tesDischargeValues.add(tesState.get(i));
            }
        }
    }


    private TimePeriod getRTPeriod(LocalDateTime untilDate, PredictionGranularity predictionGranularity) {
        LocalDateTime startDate;
        LocalDateTime endDate;

        if (PredictionGranularity.REALTIME_INTRADAY_FRAME.equals(predictionGranularity)) {
            startDate = DateUtils.computeCurrentIntraDayStart(untilDate);
            endDate = DateUtils.computeCurrentIntraDayEnd(untilDate);
        } else {
            if (PredictionGranularity.REALTIME_DAYAHEAD_FRAME.equals(predictionGranularity)) {
                startDate = DateUtils.computeCurrentDayStart(untilDate);
                endDate = DateUtils.computeCurrentDayEnd(untilDate);
            } else {
                throw new ResourceNotFoundException(predictionGranularity.name());
            }
        }
        return new TimePeriod(startDate, endDate);
    }

    private static class TimePeriod {
        private LocalDateTime startTime;
        private LocalDateTime endTime;

        public TimePeriod(LocalDateTime startTime, LocalDateTime endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
        }

        public LocalDateTime getStartTime() {
            return startTime;
        }

        public LocalDateTime getEndTime() {
            return endTime;
        }
    }
}
