package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "trained_model")
public class TrainedModel {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    @NotNull
    private UUID id;

    @Column(name = "scenario")
    @NotNull
    private String scenario;

    @Column(name = "prediction_type")
    @NotNull
    private String predictionType;

    @Column(name = "algo_type")
    @NotNull
    private String algorithmType;

    @Column(name = "component_type")
    @NotNull
    private String componentType;

    @Column(name = "path")
    @NotNull
    private String pathOnDisk;

    @Column(name = "status")
    @NotNull
    private String status;

    @Column(name = "start_time")
    @NotNull
    private LocalDateTime startTime;

    public TrainedModel(){

    }


    public TrainedModel(@NotNull UUID id,
                        @NotNull String scenario,
                        @NotNull String predictionType,
                        @NotNull String algorithmType,
                        @NotNull String componentType,
                        @NotNull String pathOnDisk,
                        @NotNull String status,
                        @NotNull LocalDateTime startTime) {
        this.id = id;
        this.scenario = scenario;
        this.predictionType = predictionType;
        this.algorithmType = algorithmType;
        this.componentType = componentType;
        this.pathOnDisk = pathOnDisk;
        this.status = status;
        this.startTime = startTime;
    }

    public UUID getId() {
        return id;
    }

    public String getScenario() {
        return scenario;
    }

    public String getPredictionType() {
        return predictionType;
    }

    public String getAlgorithmType() {
        return algorithmType;
    }

    public String getComponentType() {
        return componentType;
    }

    public String getPathOnDisk() {
        return pathOnDisk;
    }

    public String getStatus() {
        return status;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public void setPredictionType(String predictionType) {
        this.predictionType = predictionType;
    }

    public void setAlgorithmType(String algorithmType) {
        this.algorithmType = algorithmType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public void setPathOnDisk(String pathOnDisk) {
        this.pathOnDisk = pathOnDisk;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }
}
