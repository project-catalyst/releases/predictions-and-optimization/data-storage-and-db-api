package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class PredictionJobValidator {

    private static final Log LOGGER = LogFactory.getLog(PredictionJobValidator.class);

    private PredictionJobValidator() {

    }

    public static void validateInsert(PredictionJobDTO predictionJobDTO) {

        List<String> errors = new ArrayList<>();

        if (predictionJobDTO == null) {
            errors.add("Prediction Job DTO is null");
            throw new IncorrectParameterException(PredictionJobValidator.class.getSimpleName(), errors);
        }

        if (predictionJobDTO.getStartTime() == null) {
            errors.add("Prediction Job Start Time is null");
        }

        if (predictionJobDTO.getGranularity() == null || predictionJobDTO.getGranularity().equals("")) {
            errors.add("Prediction Job Granularity is null or empty");
        }else  if(!predictionJobDTO.getGranularity().equals("DAYAHEAD") &&
                !predictionJobDTO.getGranularity().equals("INTRADAY") &&
                !predictionJobDTO.getGranularity().equals("NEAR_REAL_TIME")){
            errors.add("Prediction Job Granularity is different from DAYAHEAD, INTRADAY or NEAR_REAL_TIME");
        }

        if (predictionJobDTO.getAlgoType() == null || predictionJobDTO.getAlgoType().equals("")) {
            errors.add("Prediction Job Algo Type is null or empty");
        }else if(!predictionJobDTO.getAlgoType().equals("ENSEMBLE") &&
                !predictionJobDTO.getAlgoType().equals("MLP") &&
                !predictionJobDTO.getAlgoType().equals("LSTM")&&
                !predictionJobDTO.getAlgoType().equals("MARKET")&&
                !predictionJobDTO.getAlgoType().equals("OPTIMIZATION")){
            errors.add("Prediction Job Algo Type is different from ENSEMBLE, MLP or LSTM");
        }

        if(predictionJobDTO.getFlexibilityType() == null || predictionJobDTO.getFlexibilityType().equals("")){
            errors.add("Prediction Job Flexibility Type is null or empty");
        }else if(!predictionJobDTO.getFlexibilityType().equals("NONE") &&
                !predictionJobDTO.getFlexibilityType().equals("LOWER") &&
                !predictionJobDTO.getFlexibilityType().equals("UPPER")){
            errors.add("Prediction Job Flexibility is different from NONE, LOWER or UPPER");
        }


        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PredictionJobValidator.class.getSimpleName(), errors);
        }
    }
}
