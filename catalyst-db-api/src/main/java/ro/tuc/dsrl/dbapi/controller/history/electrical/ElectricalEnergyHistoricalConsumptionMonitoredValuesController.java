package ro.tuc.dsrl.dbapi.controller.history.electrical;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.service.history.EnergyHistoricalMonitoredValuesService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/electrical/history/monitored-values/consumption")
public class ElectricalEnergyHistoricalConsumptionMonitoredValuesController {

    /**
     * Electrical Energy will treat consumption of
     * monitored values for dayahead, intraday and nearRealTime timing intervals.
     */


    private final EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService;

    @Autowired
    public ElectricalEnergyHistoricalConsumptionMonitoredValuesController(EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService) {
        this.energyHistoricalMonitoredValuesService = energyHistoricalMonitoredValuesService;
    }


    //region Consumption
    //region IntraDay (4 hours) Get Methods
    @GetMapping(value = "/4hoursbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfIntraDayConsumptionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/4hoursbefore/{dataCenterID}/itComponent/{startTime}")
    public List<EnergyProfileDTO> findElectricalHistoricalMonitoredValuesOfIntraDayConsumptionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForITComponent(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/4hoursbefore/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfIntraDayConsumptionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForCoolingSystem(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/4hoursbefore/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfIntraDayConsumptionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("serverID") String serverID,
            @PathVariable("dataCenterID") String dataCenterID) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForServer(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                UUID.fromString(serverID),
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    //online training
    @GetMapping(value = "/historyBetweenDatesEvery4Hours/{dataCenterID}/entireDC/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfIntraDayConsumptionForEntireDCBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLcalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForEntireDCBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLcalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/historyBetweenDatesEvery4Hours/{dataCenterID}/itComponent/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfIntraDayConsumptionForITComponentBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getRTConsumptionForITComponentBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/historyBetweenDatesEvery4Hours/{dataCenterID}/coolingSystem/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfIntraDayConsumptionForCoolingSystemBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForCoolingSystemBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }
    //endregion


    //region DayAhead (24 hours) Get Methods
    @GetMapping(value = "/24hoursbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfDayAheadConsumptionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/entireDCFixed/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfDayAheadConsumptionForEntireDCInIntervalFixed(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return  energyHistoricalMonitoredValuesService.getConsumptionForEntireDCFixed(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/4hoursbefore/{dataCenterID}/entireDCFixed/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfIntradayConsumptionForEntireDCInIntervalFixed(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return  energyHistoricalMonitoredValuesService.getConsumptionForEntireDCFixed(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/itComponent/{startTime}")
    public List<EnergyProfileDTO> findElectricalHistoricalMonitoredValuesOfDayAheadConsumptionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForITComponent(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfDayAheadConsumptionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForCoolingSystem(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfDayAheadConsumptionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForServer(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                UUID.fromString(serverID),
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }


    //online training
    @GetMapping(value = "/historyBetweenDatesEvery24Hours/{dataCenterID}/entireDC/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfDayaheadConsumptionForEntireDCBetweenDates(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForEntireDCBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/historyBetweenDatesEvery24Hours/{dataCenterID}/itComponent/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfDayaheadConsumptionForITComponentBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getRTConsumptionForITComponentBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());

    }

    @GetMapping(value = "/historyBetweenDatesEvery24Hours/{dataCenterID}/coolingSystem/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfDayaheadConsumptionForCoolingSystemBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForCoolingSystemBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }
    //endregion


    //region NearRealTime (1 hour) Get Methods
    @GetMapping(value = "/1hourbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfNearRealtimeConsumptionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/1hourbefore/{dataCenterID}/itComponent/{startTime}")
    public List<EnergyProfileDTO> findElectricalHistoricalMonitoredValuesOfNearRealtimeConsumptionForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForITComponent(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/1hourbefore/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfNearRealtimeConsumptionForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForCoolingSystem(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/1hourbefore/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalMonitoredValuesOfNearRealtimeConsumptionForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForServer(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                UUID.fromString(serverID),
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/historyBetweenDatesEvery1Hour/{dataCenterID}/entireDC/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfNearRealTimeConsumptionForEntireDCBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForEntireDCBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/historyBetweenDatesEvery1Hour/{dataCenterID}/itComponent/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfNearRealTimeConsumptionForITComponentBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getRTConsumptionForITComponentBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }

    @GetMapping(value = "/historyBetweenDatesEvery1Hour/{dataCenterID}/coolingSystem/{startTime}/{endTime}")
    public EnergyProfileDTO findElectricalHistoricalConsumptionMonitoredValuesOfNearRealTimeConsumptionForCoolingSystemBetweenDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("endTime") Long endTime){

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, endTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endLocalDateTime = DateUtils.millisToLocalDateTime(endTime);

        return energyHistoricalMonitoredValuesService.getConsumptionForCoolingSystemBetweenDates(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                endLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.HOUR.getGranularity(),
                PredictionType.ENERGY_CONSUMPTION.getType());
    }
    //endregion
    //endregion
}
