package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceTypeDTO;
import ro.tuc.dsrl.dbapi.model.DeviceType;

public class DeviceTypeBuilder {

    private DeviceTypeBuilder() {

    }

    public static DeviceTypeDTO generateDTOFromEntity(DeviceType deviceType) {
        return new DeviceTypeDTO(
                deviceType.getId(),
                deviceType.getType()
        );
    }

    public static DeviceType generateEntityFromDTO(DeviceTypeDTO deviceTypeDTO) {
        return new DeviceType(
                deviceTypeDTO.getId(),
                deviceTypeDTO.getType()
        );
    }
}
