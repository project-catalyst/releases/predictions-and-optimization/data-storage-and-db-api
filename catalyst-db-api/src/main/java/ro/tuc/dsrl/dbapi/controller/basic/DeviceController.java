package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.ServerRoomDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.TotalExecutionResultsPreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.model.builders.ServerRoomBuilder;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/device")
public class DeviceController {

    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping(value = "/byId/{deviceId}")
    public DeviceDTO findById(@PathVariable("deviceId") String deviceId) {

        UUIDPreconditions.validate(deviceId);

        UUID deviceUUID = UUID.fromString(deviceId);

        return deviceService.findById(deviceUUID);
    }

    @GetMapping(value = "/label/{device-name}")
    public String findByLabel(@PathVariable("device-name") String deviceName) {
        return deviceService.getDeviceIdFromLabel(deviceName).toString();
    }

    @GetMapping(value = "/labelComponent/{device-name}")
    public String findByLabelComponent(@PathVariable("device-name") String deviceName) {
        return deviceService.getDeviceIdByLabelComponent(deviceName).toString();
    }

    @GetMapping(value = "/all")
    public List<DeviceDTO> findAll() {

        return deviceService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertDeviceDTO(@RequestBody DeviceDTO deviceDTO) {

        return deviceService.insert(deviceDTO);
    }

    @GetMapping(value = "/findServerRoomsForDC/{dc-name}/{startTime}")
    public List<ServerRoomDTO> findServerRoomsForDC(@PathVariable("dc-name") String dcName, @PathVariable("startTime") Long startTime){

        TotalExecutionResultsPreconditions.validateCurrentDateAndDCNameThrowErrors(startTime, dcName);
        LocalDateTime startTimeLDT = DateUtils.millisToUTCLocalDateTime(startTime);
        return ServerRoomBuilder.toDTOList(deviceService.getServerRoom(dcName, startTimeLDT));
    }

}
