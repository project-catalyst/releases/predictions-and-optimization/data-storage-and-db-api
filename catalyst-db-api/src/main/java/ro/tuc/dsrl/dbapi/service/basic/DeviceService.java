package ro.tuc.dsrl.dbapi.service.basic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.Pair;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.DeviceType;
import ro.tuc.dsrl.dbapi.model.Measurement;
import ro.tuc.dsrl.dbapi.model.MonitoredValue;
import ro.tuc.dsrl.dbapi.model.builders.DeviceBuilder;
import ro.tuc.dsrl.dbapi.model.dto.resources.CopFactors;
import ro.tuc.dsrl.dbapi.model.dto.resources.DeviceStateDTO;
import ro.tuc.dsrl.dbapi.model.dto.resources.LossFactors;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.DeviceTypeRepository;
import ro.tuc.dsrl.dbapi.repository.MeasurementRepository;
import ro.tuc.dsrl.dbapi.repository.MonitoredValueRepository;
import ro.tuc.dsrl.dbapi.service.validators.DeviceValidator;
import ro.tuc.dsrl.dbapi.util.RepoConversionUtility;
import ro.tuc.dsrl.geyser.datamodel.components.Component;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.HeatRecoveryInfrastructure;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DeviceService {
    private static final Log LOGGER = LogFactory.getLog(DeviceService.class);
    private final DeviceRepository deviceRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final MeasurementRepository measurementRepository;
    private final MonitoredValueRepository monitoredValueRepository;

    private static final String NO_PARENT_DC = "Parent DC does not exist";

    public DeviceService(DeviceRepository deviceRepository, DeviceTypeRepository deviceTypeRepository, MeasurementRepository measurementRepository, MonitoredValueRepository monitoredValueRepository) {
        this.deviceRepository = deviceRepository;
        this.deviceTypeRepository = deviceTypeRepository;
        this.measurementRepository = measurementRepository;
        this.monitoredValueRepository = monitoredValueRepository;
    }

    public UUID getDeviceIdFromLabel(String label) {

        UUID uuid = deviceRepository.findByLabel(label);
        if (uuid == null) {
            throw new ResourceNotFoundException("DataCenter with name: " + label + " NOT FOUND!");
        }
        return uuid;
    }

    public DeviceDTO getDeviceByLabel(String label) {

        Device d = deviceRepository.findDeviceByLabel(label);
        if (d == null) {
            throw new ResourceNotFoundException("DataCenter with name: " + label + " NOT FOUND!");
        }
        return DeviceBuilder.generateDTOFromEntity(d, d.getDeviceTypeId());
    }

    public UUID getDeviceIdByLabelComponent(@Param("label") String label){

        UUID uuid = deviceRepository.findByLabelComponent(label);
        if(uuid == null){
            throw new ResourceNotFoundException("Component with name: " + label + " NOT FOUND!");
        }

        return uuid;
    }

    public List<DeviceDTO> findAll() {

        List<Device> devices = deviceRepository.findAllAvailable();

        return devices.stream()
                .map(d -> DeviceBuilder.generateDTOFromEntity(d, d.getDeviceTypeId()))
                .collect(Collectors.toList());
    }

    public UUID insert(DeviceDTO deviceDTO) {

        DeviceValidator.validateInsert(deviceDTO);

        List<String> errors = new ArrayList<>();
        DeviceType deviceType = deviceTypeRepository.findByUUId(deviceDTO.getDeviceTypeId());
        if (deviceType == null) {
            errors.add("Device type was not found in database.");
        }

        Device parent = null;
        if (deviceDTO.getParent() != null) {
            parent = deviceRepository.findByUUID(deviceDTO.getParent());
            if (parent == null) {
                errors.add("Parent device was not found in database.");
            }

            if (!errors.isEmpty()) {
                throw new EntityValidationException(Device.class.getSimpleName(), errors);
            }
        }

        return deviceRepository
                .save(DeviceBuilder.generateEntityFromDTO(deviceDTO, deviceType, parent))
                .getId();
    }

    public DeviceDTO findById(UUID deviceUUID) {

        Device device = deviceRepository.findByUUID(deviceUUID);
        if (device == null) {
            throw new ResourceNotFoundException("Device with ID " + deviceUUID);
        }

        return DeviceBuilder.generateDTOFromEntity(device, device.getDeviceTypeId());

    }

    public List<DeviceDTO> findByParentAndType(String dcName, DeviceTypeEnum deviceType) {

        List<Device> devices = deviceRepository.findByDeviceTypeAndParent(deviceType.type(), dcName);
        if (devices == null || devices.isEmpty()) {
            throw new ResourceNotFoundException("Device type " + deviceType);
        }

        return devices.stream()
                .map(d -> DeviceBuilder.generateDTOFromEntity(d, d.getDeviceTypeId()))
                .collect(Collectors.toList());
    }

    public List<DeviceStateDTO> getDevicesState(String dcName, DeviceTypeEnum deviceType, LocalDateTime time) {
        Object[][] results = deviceRepository.findDeviceStates2(deviceType.type(), dcName, time);
        return RepoConversionUtility.getDevicesState(results);
    }


    @Transactional
    public List<ThermalEnergyStorage> insertThermalEnergyStorage(ThermalEnergyStorage tes, String dcDevice) {
        DeviceValidator.validateInsert(tes);
        Device d = deviceRepository.findDeviceByLabel(tes.getDeviceLabel());
        if (d != null) {
            List<String> errors = new ArrayList<>();
            errors.add("Thermal energy storage name is duplicated");
            throw new IncorrectParameterException(ThermalEnergyStorage.class.getSimpleName(), errors);
        }


        DeviceType type = deviceTypeRepository.findByType(DeviceTypeEnum.TES.type());
        Device parent = deviceRepository.findDeviceByLabel(dcDevice);
        if (parent == null) {
            List<String> errors = new ArrayList<>();
            errors.add(NO_PARENT_DC);
            throw new IncorrectParameterException(ThermalEnergyStorage.class.getSimpleName(), errors);
        }
        Device device = new Device();

        device.setDeviceTypeId(type);
        device.setParent(parent);

        return saveTes(tes, device, dcDevice);
    }

    @Transactional
    public List<Battery> insertBattery(Battery battery, String dcDevice) {
        DeviceValidator.validateInsert(battery);
        Device d = deviceRepository.findDeviceByLabel(battery.getDeviceLabel());
        if (d != null) {
            List<String> errors = new ArrayList<>();
            errors.add("Battery name is duplicated");
            throw new IncorrectParameterException(Battery.class.getSimpleName(), errors);
        }


        DeviceType type = deviceTypeRepository.findByType(DeviceTypeEnum.BATTERY.type());
        Device parent = deviceRepository.findDeviceByLabel(dcDevice);
        if (parent == null) {
            List<String> errors = new ArrayList<>();
            errors.add(NO_PARENT_DC);
            throw new IncorrectParameterException(Battery.class.getSimpleName(), errors);
        }

        Device device = new Device();

        device.setDeviceTypeId(type);
        device.setParent(parent);
        device.setLabel(battery.getDeviceLabel());

        return saveBattery(battery, device, dcDevice);
    }

    @Transactional
    public List<ServerRoom> insertServerRoom(ServerRoom serverRoom, String dcDevice) {
        DeviceValidator.validateInsert(serverRoom);
        Device d = deviceRepository.findDeviceByLabel(serverRoom.getDeviceLabel());
        if (d != null) {
            List<String> errors = new ArrayList<>();
            errors.add("Server room name is duplicated");
            throw new IncorrectParameterException(ServerRoom.class.getSimpleName(), errors);
        }

        DeviceType type = deviceTypeRepository.findByType(DeviceTypeEnum.SERVER_ROOM.type());
        Device parent = deviceRepository.findDeviceByLabel(dcDevice);
        if (parent == null) {
            List<String> errors = new ArrayList<>();
            errors.add(NO_PARENT_DC);
            throw new IncorrectParameterException(ServerRoom.class.getSimpleName(), errors);
        }

        Device device = new Device();
        device.setDeviceTypeId(type);
        device.setParent(parent);

        return saveServerRoom(serverRoom, device, dcDevice);
    }

    @Transactional
    public List<CoolingSystem> insertCoolingSystem(CoolingSystem coolingSystem, String dcDevice) {
        DeviceValidator.validateInsert(coolingSystem);
        Device d = deviceRepository.findDeviceByLabel(coolingSystem.getDeviceLabel());
        if (d != null) {
            List<String> errors = new ArrayList<>();
            errors.add("Cooling system name is duplicated");
            throw new IncorrectParameterException(CoolingSystem.class.getSimpleName(), errors);
        }


        DeviceType type = deviceTypeRepository.findByType(DeviceTypeEnum.COOLING_SYSTEM.type());
        Device parent = deviceRepository.findDeviceByLabel(dcDevice);
        if (parent == null) {
            List<String> errors = new ArrayList<>();
            errors.add(NO_PARENT_DC);
            throw new IncorrectParameterException(CoolingSystem.class.getSimpleName(), errors);
        }

        Device device = new Device();
        device.setDeviceTypeId(type);
        device.setParent(parent);


        return saveCoolingSystem(coolingSystem, device, dcDevice);
    }

    @Transactional
    public List<DataCentre> insertDatacentre(DataCentre dataCentre) {
        DeviceValidator.validateInsert(dataCentre);
        Device d = deviceRepository.findDeviceByLabel(dataCentre.getLabel());
        if (d != null) {
            List<String> errors = new ArrayList<>();
            errors.add("Data centre name is duplicated");
            throw new IncorrectParameterException(DataCentre.class.getSimpleName(), errors);
        }
        DeviceType type = deviceTypeRepository.findByType(DeviceTypeEnum.DATACENTER.type());
        Device device = new Device();
        device.setDeviceTypeId(type);

        return saveDataCentre(dataCentre, device);
    }


    @Transactional
    public List<HeatRecoveryInfrastructure> insertHeatRecoverySystems(HeatRecoveryInfrastructure hri, String dcDevice) {
        DeviceValidator.validateInsert(hri);
        Device d = deviceRepository.findDeviceByLabel(hri.getDeviceLabel());
        if (d != null) {
            List<String> errors = new ArrayList<>();
            errors.add("Heat Recovery Infrastructure name is duplicated");
            throw new IncorrectParameterException(HeatRecoveryInfrastructure.class.getSimpleName(), errors);
        }

        DeviceType type = deviceTypeRepository.findByType(DeviceTypeEnum.HEAT_RECOVERY_SYSTEM.type());
        Device parent = deviceRepository.findDeviceByLabel(dcDevice);
        if (parent == null) {
            List<String> errors = new ArrayList<>();
            errors.add(NO_PARENT_DC);
            throw new IncorrectParameterException(HeatRecoveryInfrastructure.class.getSimpleName(), errors);
        }

        Device device = new Device();

        device.setDeviceTypeId(type);
        device.setParent(parent);

        return saveHeatRecoveryInfrastructures(hri, device, dcDevice);
    }

    @Transactional
    public List<ThermalEnergyStorage> updateThermalEnergyStorage(ThermalEnergyStorage tes, String dcDevice) {
        DeviceValidator.validateInsert(tes);

        Device d = deviceRepository.findByUUID(tes.getDeviceId());
        if (d == null) {
            throw new ResourceNotFoundException(ThermalEnergyStorage.class.getSimpleName());
        }

        Device d2 = deviceRepository.findDeviceByLabel(tes.getDeviceLabel());
        if (d2 != null && !d2.getId().equals(d.getId())) {
            List<String> errors = new ArrayList<>();
            errors.add("TES name is duplicated");
            throw new IncorrectParameterException(ThermalEnergyStorage.class.getSimpleName(), errors);
        }

        return saveTes(tes, d, dcDevice);
    }

    @Transactional
    public List<Battery> updateBattery(Battery battery, String dcDevice) {
        DeviceValidator.validateInsert(battery);

        Device d = deviceRepository.findByUUID(battery.getDeviceId());
        if (d == null) {
            throw new ResourceNotFoundException(Battery.class.getSimpleName());
        }

        Device d2 = deviceRepository.findDeviceByLabel(battery.getDeviceLabel());
        if (d2 != null && !d2.getId().equals(d.getId())) {
            List<String> errors = new ArrayList<>();
            errors.add("Battery name is duplicated");
            throw new IncorrectParameterException(Battery.class.getSimpleName(), errors);
        }
        return saveBattery(battery, d, dcDevice);
    }

    @Transactional
    public List<ServerRoom> updateServerRoom(ServerRoom serverRoom, String dcDevice) {
        DeviceValidator.validateInsert(serverRoom);

        Device d = deviceRepository.findByUUID(serverRoom.getDeviceId());
        if (d == null) {
            throw new ResourceNotFoundException(ServerRoom.class.getSimpleName());
        }
        Device d2 = deviceRepository.findDeviceByLabel(serverRoom.getDeviceLabel());
        if (d2 != null && !d2.getId().equals(d.getId())) {
            List<String> errors = new ArrayList<>();
            errors.add("Server Room name is duplicated");
            throw new IncorrectParameterException(ServerRoom.class.getSimpleName(), errors);
        }


        return saveServerRoom(serverRoom, d, dcDevice);
    }

    @Transactional
    public List<CoolingSystem> updateCoolingSystem(CoolingSystem coolingSystem, String dcDevice) {
        DeviceValidator.validateInsert(coolingSystem);

        Device d = deviceRepository.findByUUID(coolingSystem.getDeviceId());
        if (d == null) {
            throw new ResourceNotFoundException(CoolingSystem.class.getSimpleName());
        }
        Device d2 = deviceRepository.findDeviceByLabel(coolingSystem.getDeviceLabel());
        if (d2 != null && !d2.getId().equals(d.getId())) {
            List<String> errors = new ArrayList<>();
            errors.add("Cooling System name is duplicated");
            throw new IncorrectParameterException(CoolingSystem.class.getSimpleName(), errors);
        }
        return saveCoolingSystem(coolingSystem, d, dcDevice);
    }


    @Transactional
    public List<DataCentre> updateDatacentre(DataCentre dataCentre) {
        DeviceValidator.validateInsert(dataCentre);

        Device d = deviceRepository.findByUUID(dataCentre.getIdDataCentre());
        if (d == null) {
            throw new ResourceNotFoundException(DataCentre.class.getSimpleName());
        }
        Device d2 = deviceRepository.findDeviceByLabel(dataCentre.getLabel());
        if (d2 != null && !d2.getId().equals(d.getId())) {
            List<String> errors = new ArrayList<>();
            errors.add("Data Center name is duplicated");
            throw new IncorrectParameterException(DataCentre.class.getSimpleName(), errors);
        }
        return saveDataCentre(dataCentre, d);
    }

    @Transactional
    public List<HeatRecoveryInfrastructure> updateHeatRecoverySystems(HeatRecoveryInfrastructure hri, String dcDevice) {
        DeviceValidator.validateInsert(hri);
        Device d = deviceRepository.findByUUID(hri.getDeviceId());
        if (d == null) {
            throw new ResourceNotFoundException(HeatRecoveryInfrastructure.class.getSimpleName());
        }
        Device d2 = deviceRepository.findDeviceByLabel(hri.getDeviceLabel());
        if (d2 != null && !d2.getId().equals(d.getId())) {
            List<String> errors = new ArrayList<>();
            errors.add("Cooling System name is duplicated");
            throw new IncorrectParameterException(HeatRecoveryInfrastructure.class.getSimpleName(), errors);
        }
        return saveHeatRecoveryInfrastructures(hri, d, dcDevice);
    }

    private List<MonitoredValue> insertMeasurements(Device deviceId, Map<String, Double> properties, LocalDateTime time) {

        List<MonitoredValue> monitoredValues = new ArrayList<>();
        for (Map.Entry<String, Double> prop : properties.entrySet()) {
            Measurement m = measurementRepository.findMeasurementByProperty(prop.getKey());
            if (m == null) {
                LOGGER.error("NO MEASUREMENT INSTANCE FOUND FOR " + prop.getKey());
            } else {
                if ((deviceId != null && m != null) && (m.getDeviceTypeId() == null || deviceId.getDeviceTypeId() == null ||
                        !m.getDeviceTypeId().getId().equals(deviceId.getDeviceTypeId().getId()))) {
                    List<String> errors = new ArrayList<>();
                    errors.add("Measurement and Device types are inconsistent.");
                    throw new EntityValidationException(MonitoredValue.class.getSimpleName(), errors);
                }

                MonitoredValue mv = new MonitoredValue();
                mv.setDeviceId(deviceId);
                mv.setMeasurementId(m);
                mv.setTimestamp(time);
                mv.setValue(prop.getValue());
                monitoredValues.add(mv);
            }
        }
        return monitoredValueRepository.saveAll(monitoredValues);
    }

    public static Map<Pair<String, UUID>, Map<String, Double>> mapDeviceStateKeyValuePairs(List<DeviceStateDTO> deviceStates) {
        Map<Pair<String, UUID>, Map<String, Double>> map = new HashMap<>();
        for (DeviceStateDTO state : deviceStates) {
            String deviceLabel = state.getDeviceLabel();
            UUID deviceId = state.getDeviceId();
            Pair<String, UUID> key = new Pair<>(deviceLabel, deviceId);
            if (!map.containsKey(key)) {
                map.put(key, new HashMap<>());
            }
            Map<String, Double> deviceProperties = map.get(key);
            deviceProperties.put(state.getProperty(), state.getValue());
        }
        return map;
    }

    public List<ThermalEnergyStorage> getThermalEnergyStorages(String dcName, LocalDateTime time) {
        List<DeviceStateDTO> tesStates = getDevicesState(dcName, DeviceTypeEnum.TES, time);
        Map<Pair<String, UUID>, Map<String, Double>> tesedProperties = mapDeviceStateKeyValuePairs(tesStates);

        List<ThermalEnergyStorage> tesComps = new ArrayList<>();
        for (Map.Entry<Pair<String, UUID>, Map<String, Double>> tesProperties : tesedProperties.entrySet()) {

            Map<String, Double> currentTES = tesProperties.getValue();

            Double actualLoadedCapacity = currentTES.getOrDefault(MeasurementType.TES_CURRENT.type(),  currentTES.getOrDefault(MeasurementType.TES_MAX.type(), 100.0));
            Double factor = currentTES.getOrDefault(MeasurementType.TES_FACTOR.type(), 0.0);
            Double maxDischargeRate = currentTES.getOrDefault(MeasurementType.TES_MAX_DIS_TES.type(), 0.0);
            Double maxChargeRate = currentTES.getOrDefault(MeasurementType.TES_MAX_CHR_TES.type(), 0.0);
            Double chargeLossRate = currentTES.getOrDefault(MeasurementType.TES_R.type(), 0.0);
            Double dischargeLossRate = currentTES.getOrDefault(MeasurementType.TES_D.type(), 0.0);
            Double maximumCapacity = currentTES.getOrDefault(MeasurementType.TES_MAX.type(), 0.0);
            ThermalEnergyStorage tes = new ThermalEnergyStorage(tesProperties.getKey().getSecond(), tesProperties.getKey().getFirst(), actualLoadedCapacity, factor, maxDischargeRate, maxChargeRate, chargeLossRate, dischargeLossRate, maximumCapacity);
            tesComps.add(tes);
        }
        return tesComps;
    }

    public List<Battery> getBatteries(String dcName, LocalDateTime time) {
        List<DeviceStateDTO> batteryStates = getDevicesState(dcName, DeviceTypeEnum.BATTERY, time);
        Map<Pair<String, UUID>, Map<String, Double>> batteriesProperties = mapDeviceStateKeyValuePairs(batteryStates);

        List<Battery> batteries = new ArrayList<>();
        for (Map.Entry<Pair<String, UUID>, Map<String, Double>> batteryProperties : batteriesProperties.entrySet()) {

            Map<String, Double> currentBattery = batteryProperties.getValue();

            Double actualLoadedCapacity = currentBattery.getOrDefault(MeasurementType.BATTERY_CURRENT.type(), currentBattery.getOrDefault(MeasurementType.BATTERY_ESD_MAX.type(), 300.0));
            Double factor = currentBattery.getOrDefault(MeasurementType.BATTERY_FACTOR.type(), 0.0);
            Double maxDischargeRate = currentBattery.getOrDefault(MeasurementType.BATTERY_MAX_DIS_ESD.type(), 0.0);
            Double maxChargeRate = currentBattery.getOrDefault(MeasurementType.BATTERY_MAX_CHR_ESD.type(), 0.0);
            Double chargeLossRate = currentBattery.getOrDefault(MeasurementType.BATTERY_ESD_R.type(), 0.0);
            Double dischargeLossRate = currentBattery.getOrDefault(MeasurementType.BATTERY_ESD_D.type(), 0.0);
            Double dod = currentBattery.getOrDefault(MeasurementType.BATTERY_DOD.type(), 0.0);
            Double maximumCapacity = currentBattery.getOrDefault(MeasurementType.BATTERY_ESD_MAX.type(), 0.0);
            Battery battery = new Battery(batteryProperties.getKey().getSecond(), batteryProperties.getKey().getFirst(), actualLoadedCapacity, factor, maxDischargeRate, maxChargeRate, chargeLossRate, dischargeLossRate, dod, maximumCapacity);
            batteries.add(battery);
        }
        return batteries;
    }

    public List<ServerRoom> getServerRoom(String dcName, LocalDateTime time) {
        List<DeviceStateDTO> serverRoomStates = getDevicesState(dcName, DeviceTypeEnum.SERVER_ROOM, time);
        Map<Pair<String, UUID>, Map<String, Double>> serverRoomsProperties = mapDeviceStateKeyValuePairs(serverRoomStates);

        List<ServerRoom> serverRooms = new ArrayList<>();
        for (Map.Entry<Pair<String, UUID>, Map<String, Double>> serverRoomProps : serverRoomsProperties.entrySet()) {

            Map<String, Double> currentServerRoom = serverRoomProps.getValue();

            Double actualLoadedCapacity = currentServerRoom.getOrDefault(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type(), 0.0);
            Double maxConsumption = currentServerRoom.getOrDefault(MeasurementType.SERVER_ROOM_MAX_CONSUMPTION.type(), 0.0);
            Double edPercentage = currentServerRoom.getOrDefault(MeasurementType.ED_PERCENTAGE.type(), 0.0);
            Double maxHost = currentServerRoom.getOrDefault(MeasurementType.DC_MAX_HOST.type(), 0.0);
            Double maxRealloc = currentServerRoom.getOrDefault(MeasurementType.DC_MAX_REALLOC.type(), 0.0);
            Double rt = currentServerRoom.getOrDefault(MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type(), -1.0);
            Double dt = currentServerRoom.getOrDefault(MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION.type(), -1.0);


            ServerRoom sr = new ServerRoom(serverRoomProps.getKey().getSecond(), serverRoomProps.getKey().getFirst(), actualLoadedCapacity, maxConsumption, edPercentage, maxHost, maxRealloc, rt, dt);
            serverRooms.add(sr);
        }
        return serverRooms;
    }

    public List<CoolingSystem> getCoolingSystem(String dcName, LocalDateTime time) {
        List<DeviceStateDTO> coolingSystemsStates = getDevicesState(dcName, DeviceTypeEnum.COOLING_SYSTEM, time);
        Map<Pair<String, UUID>, Map<String, Double>> coolingSystemsProperties = mapDeviceStateKeyValuePairs(coolingSystemsStates);

        List<CoolingSystem> coolingSystems = new ArrayList<>();
        for (Map.Entry<Pair<String, UUID>, Map<String, Double>> coolingSystemProps : coolingSystemsProperties.entrySet()) {

            Map<String, Double> currentCoolingSystem = coolingSystemProps.getValue();

            Double actualLoadedCapacity = currentCoolingSystem.getOrDefault(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type(), 0.0);
            Double maxConsumption = currentCoolingSystem.getOrDefault(MeasurementType.COOLING_SYSTEM_MAX_CONSUMPTION.type(), 0.0);
            Double maxCapacity = currentCoolingSystem.getOrDefault(MeasurementType.COOLING_SYSTEM_CAPACITY.type(), 0.0);
            Double copC = currentCoolingSystem.getOrDefault(MeasurementType.COOLING_SYSTEM_COP_C.type(), 0.0);
            Double copH = currentCoolingSystem.getOrDefault(MeasurementType.COOLING_SYSTEM_COP_H.type(), 0.0);


            CoolingSystem sr = new CoolingSystem(coolingSystemProps.getKey().getSecond(), coolingSystemProps.getKey().getFirst(), actualLoadedCapacity, maxConsumption, copC, copH, maxCapacity);
            coolingSystems.add(sr);
        }
        return coolingSystems;
    }

    public List<HeatRecoveryInfrastructure> getHeatRecoveryInfrastructure(String dcName, LocalDateTime time) {
        List<DeviceStateDTO> hriStates = getDevicesState(dcName, DeviceTypeEnum.HEAT_RECOVERY_SYSTEM, time);
        Map<Pair<String, UUID>, Map<String, Double>> hriProperties = mapDeviceStateKeyValuePairs(hriStates);

        List<HeatRecoveryInfrastructure> hris = new ArrayList<>();
        for (Map.Entry<Pair<String, UUID>, Map<String, Double>> hriProps : hriProperties.entrySet()) {

            Map<String, Double> currentHRI = hriProps.getValue();

            Double actualLoadedCapacity = currentHRI.getOrDefault(MeasurementType.HEAT_RECOVER_SYSTEM_CURRENT_CONSUMPTION.type(), 0.0);
            Double maxConsumption = currentHRI.getOrDefault(MeasurementType.HEAT_RECOVER_SYSTEM_MAX_CONSUMPTION.type(), 0.0);

            Double maxCapacity = currentHRI.getOrDefault(MeasurementType.HEAT_RECOVER_SYSTEM_CAPACITY.type(), 0.0);
            Double copH = currentHRI.getOrDefault(MeasurementType.HEAT_RECOVER_SYSTEM_COP_H.type(), 0.0);

            HeatRecoveryInfrastructure sr = new HeatRecoveryInfrastructure(hriProps.getKey().getSecond(), hriProps.getKey().getFirst(), actualLoadedCapacity, copH, maxConsumption, maxCapacity);
            hris.add(sr);
        }
        return hris;
    }

    public DataCentre getDatacenterProperties(String dcName, LocalDateTime time) {

        double totalConsumption = 0;

        List<CoolingSystem> coolingSystems = getCoolingSystem(dcName, time);
        for (CoolingSystem cs : coolingSystems) {
            totalConsumption += cs.getEnergyConsumption();
        }
        List<ServerRoom> serverRooms = getServerRoom(dcName, time);
        for (ServerRoom sr : serverRooms) {
            totalConsumption += sr.getEnergyConsumption();
        }
        List<Battery> batteries = getBatteries(dcName, time);
        List<ThermalEnergyStorage> tes = getThermalEnergyStorages(dcName, time);
        List<HeatRecoveryInfrastructure> hris = getHeatRecoveryInfrastructure(dcName, time);
        List<Component> components = new ArrayList<>();
        components.addAll(coolingSystems);
        components.addAll(serverRooms);
        components.addAll(batteries);
        components.addAll(tes);
        components.addAll(hris);
        DataCentre sr = new DataCentre(dcName, totalConsumption);
        sr.setComponents(components);

        return sr;
    }

    public List<DataCentre> getDatacenters(LocalDateTime time) {
        List<Device> parents = deviceRepository.findParentDevices(DeviceTypeEnum.DATACENTER.type());

        List<DataCentre> dcOutput = new ArrayList<>();
        for (Device d : parents) {
            DataCentre dc = getDatacenterProperties(d.getLabel(), time);
            dc.setIdDataCentre(d.getId());
            dcOutput.add(dc);
        }
        return dcOutput;
    }

    private List<Battery> saveBattery(Battery battery, Device device, String dcDevice) {
        Map<String, Double> properties = new HashMap<>();
        properties.put(MeasurementType.BATTERY_CURRENT.type(), battery.getActualLoadedCapacity());
        properties.put(MeasurementType.BATTERY_FACTOR.type(), battery.getEsdFactor());
        properties.put(MeasurementType.BATTERY_MAX_DIS_ESD.type(), battery.getMaxDischargeRate());
        properties.put(MeasurementType.BATTERY_MAX_CHR_ESD.type(), battery.getMaxChargeRate());
        properties.put(MeasurementType.BATTERY_ESD_R.type(), battery.getChargeLossRate());
        properties.put(MeasurementType.BATTERY_ESD_D.type(), battery.getDischargeLossRate());
        properties.put(MeasurementType.BATTERY_DOD.type(), battery.getDod());
        properties.put(MeasurementType.BATTERY_ESD_MAX.type(), battery.getMaximumCapacity());

        device.setLabel(battery.getDeviceLabel());
        device = deviceRepository.save(device);
        battery.setDeviceId(device.getId());
        LocalDateTime now = LocalDateTime.now();
        insertMeasurements(device, properties, now);
        LocalDateTime afterinsert = LocalDateTime.now().plusMinutes(2);
        return getBatteries(dcDevice, afterinsert);
    }

    private List<ThermalEnergyStorage> saveTes(ThermalEnergyStorage tes, Device device, String dcDevice) {
        Map<String, Double> properties = new HashMap<>();
        properties.put(MeasurementType.TES_CURRENT.type(), tes.getActualLoadedCapacity());
        properties.put(MeasurementType.TES_FACTOR.type(), tes.getTesFactor());
        properties.put(MeasurementType.TES_MAX_DIS_TES.type(), tes.getMaxDischargeRate());
        properties.put(MeasurementType.TES_MAX_CHR_TES.type(), tes.getMaxChargeRate());
        properties.put(MeasurementType.TES_R.type(), tes.getChargeLossRate());
        properties.put(MeasurementType.TES_D.type(), tes.getDischargeLossRate());
        properties.put(MeasurementType.TES_MAX.type(), tes.getMaximumCapacity());

        device.setLabel(tes.getDeviceLabel());
        device = deviceRepository.save(device);
        tes.setDeviceId(device.getId());
        LocalDateTime now = LocalDateTime.now();
        insertMeasurements(device, properties, now);
        LocalDateTime afterinsert = LocalDateTime.now().plusMinutes(2);
        return getThermalEnergyStorages(dcDevice, afterinsert);
    }

    private List<ServerRoom> saveServerRoom(ServerRoom serverRoom, Device device, String dcDevice) {
        Map<String, Double> properties = new HashMap<>();
        properties.put(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type(), serverRoom.getEnergyConsumption());
        properties.put(MeasurementType.SERVER_ROOM_MAX_CONSUMPTION.type(), serverRoom.getMaxEnergyConsumption());
        properties.put(MeasurementType.ED_PERCENTAGE.type(), serverRoom.getEdPercentage());
        properties.put(MeasurementType.DC_MAX_HOST.type(), serverRoom.getMaxHostLoad());
        properties.put(MeasurementType.DC_MAX_REALLOC.type(), serverRoom.getMaxReallocLoad());

        device.setLabel(serverRoom.getDeviceLabel());
        device = deviceRepository.save(device);
        serverRoom.setDeviceId(device.getId());
        LocalDateTime now = LocalDateTime.now();
        insertMeasurements(device, properties, now);
        LocalDateTime afterinsert = LocalDateTime.now().plusMinutes(2);
        return getServerRoom(dcDevice, afterinsert);
    }

    private List<CoolingSystem> saveCoolingSystem(CoolingSystem coolingSystem, Device device, String dcDevice) {
        Map<String, Double> properties = new HashMap<>();
        properties.put(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type(), coolingSystem.getEnergyConsumption());
        properties.put(MeasurementType.COOLING_SYSTEM_CAPACITY.type(), coolingSystem.getCoolingCapacityKWh());
        properties.put(MeasurementType.COOLING_SYSTEM_MAX_CONSUMPTION.type(), coolingSystem.getMaxCoolingLoadKWh());
        properties.put(MeasurementType.COOLING_SYSTEM_COP_C.type(), coolingSystem.getCopC());
        properties.put(MeasurementType.COOLING_SYSTEM_COP_H.type(), coolingSystem.getCopH());

        device.setLabel(coolingSystem.getDeviceLabel());
        device = deviceRepository.save(device);
        coolingSystem.setDeviceId(device.getId());
        LocalDateTime now = LocalDateTime.now();
        insertMeasurements(device, properties, now);
        LocalDateTime afterinsert = LocalDateTime.now().plusMinutes(2);
        return getCoolingSystem(dcDevice, afterinsert);
    }

    private List<DataCentre> saveDataCentre(DataCentre dataCentre, Device device) {
        //Map<String, Double> properties = new HashMap<>();
        //properties.put(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type(), dataCentre.getEnergyConsumption());

        device.setLabel(dataCentre.getLabel());
        device = deviceRepository.save(device);
        dataCentre.setIdDataCentre(device.getId());
        LocalDateTime now = LocalDateTime.now();
        //insertMeasurements(device, properties, now);
        LocalDateTime afterinsert = LocalDateTime.now().plusMinutes(2);
        return getDatacenters(afterinsert);
    }


    private List<HeatRecoveryInfrastructure> saveHeatRecoveryInfrastructures(HeatRecoveryInfrastructure hri, Device device, String dcDevice) {
        Map<String, Double> properties = new HashMap<>();
        properties.put(MeasurementType.HEAT_RECOVER_SYSTEM_CURRENT_CONSUMPTION.type(), hri.getEnergyConsumption());
        properties.put(MeasurementType.HEAT_RECOVER_SYSTEM_MAX_CONSUMPTION.type(), hri.getMaxHeatLoadKWh());
        properties.put(MeasurementType.HEAT_RECOVER_SYSTEM_CAPACITY.type(), hri.getHeatCapacityKWh());
        properties.put(MeasurementType.HEAT_RECOVER_SYSTEM_COP_H.type(), hri.getCopH());


        device.setLabel(hri.getDeviceLabel());
        device = deviceRepository.save(device);
        hri.setDeviceId(device.getId());
        LocalDateTime now = LocalDateTime.now();
        insertMeasurements(device, properties, now);
        LocalDateTime afterinsert = LocalDateTime.now().plusMinutes(2);
        return getHeatRecoveryInfrastructure(dcDevice, afterinsert);
    }


    public LossFactors getTESLossFactors(String dcName, LocalDateTime startDate) {
        List<ThermalEnergyStorage> teses = getThermalEnergyStorages(dcName, startDate);
        double tesChargeLossRate = 1;
        double tesDischargeLossRate = 1;
        double initialState = 0;
        if (!teses.isEmpty()) {
            double dischargeLossRate = 0.0;
            double chargeLossRate = 0.0;
            for (ThermalEnergyStorage tes : teses) {
                dischargeLossRate += tes.getDischargeLossRate();
                chargeLossRate += tes.getChargeLossRate();
                initialState += tes.getActualLoadedCapacity();
            }
            tesDischargeLossRate = dischargeLossRate / (double) teses.size();
            tesChargeLossRate = chargeLossRate / (double) teses.size();
        }
        return new LossFactors(tesChargeLossRate, tesDischargeLossRate, initialState);
    }

    public LossFactors getBatteryLossFactors(String dcName, LocalDateTime startDate) {
        List<Battery> batts = getBatteries(dcName, startDate);
        double esdChargeLossRate = 1;
        double esdDischargeLossRate = 1;
        double initialState = 0;

        if (!batts.isEmpty()) {
            double dischargeLossRate = 0.0;
            double chargeLossRate = 0.0;
            for (Battery battery : batts) {
                dischargeLossRate += battery.getDischargeLossRate();
                chargeLossRate += battery.getChargeLossRate();
                initialState += battery.getActualLoadedCapacity();
            }
            esdDischargeLossRate = dischargeLossRate / (double) batts.size();
            esdChargeLossRate = chargeLossRate / (double) batts.size();
        }
        return new LossFactors(esdChargeLossRate, esdDischargeLossRate, initialState);
    }

    public CopFactors getCopFactors(String dcName, LocalDateTime currentTime) {
        List<CoolingSystem> css = getCoolingSystem(dcName, currentTime);

        if (css == null || css.isEmpty()) {
            return new CopFactors(1, 1);
        }
        double copC = 0;
        double copH = 0;
        for (CoolingSystem cs : css) {
            copC += cs.getCopC();
            copH += cs.getCopH();
        }
        return new CopFactors(copC / css.size(), copH / css.size());

    }

    public double getDTPercentage(String datacenterName, LocalDateTime startLocalDateTime) {
        List<ServerRoom> serverRoomStates = getServerRoom(datacenterName, startLocalDateTime);

        double percentage = 0;
        for (ServerRoom serverRoom : serverRoomStates) {
            percentage += serverRoom.getEdPercentage();
        }
        if (serverRoomStates.isEmpty()) {
            return 0;
        } else {
            return percentage / serverRoomStates.size();
        }
    }

    public boolean deleteDevice(String deviceName) {
        Device d = deviceRepository.findDeviceByLabel(deviceName);

        if (d == null) {
            throw new ResourceNotFoundException(deviceName);
        }
        d.setDeleted(true);
        d.setLabel(d.getLabel() + "*" + LocalDateTime.now().toString());
        deviceRepository.save(d);
        return true;
    }

}
