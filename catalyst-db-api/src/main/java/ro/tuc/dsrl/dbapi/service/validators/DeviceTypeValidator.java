package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceTypeDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class DeviceTypeValidator {

    private static final Log LOGGER = LogFactory.getLog(DeviceTypeValidator.class);

    private DeviceTypeValidator() {

    }

    public static void validateInsert(DeviceTypeDTO deviceTypeDTO) {

        List<String> errors = new ArrayList<>();
        if (deviceTypeDTO == null) {
            errors.add("Device Type DTO is null");
            throw new IncorrectParameterException(DeviceTypeValidator.class.getSimpleName(), errors);
        }

        if (deviceTypeDTO.getId() == null) {
            errors.add("Device type ID is null");
        }

        if (deviceTypeDTO.getType() == null || deviceTypeDTO.getType().equals("")) {
            errors.add("Device type type is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DeviceTypeValidator.class.getSimpleName(), errors);
        }
    }
}
