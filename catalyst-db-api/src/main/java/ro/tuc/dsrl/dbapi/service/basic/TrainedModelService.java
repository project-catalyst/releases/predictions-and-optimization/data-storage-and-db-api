package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.TrainedModelDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.TrainedModel;
import ro.tuc.dsrl.dbapi.model.builders.TrainedModelBuilder;
import ro.tuc.dsrl.dbapi.repository.TrainedModelRepository;
import ro.tuc.dsrl.dbapi.service.validators.TrainedModelValidator;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TrainedModelService {

    private final TrainedModelRepository trainedModelRepository;

    @Autowired
    public TrainedModelService(TrainedModelRepository trainedModelRepository) {
        this.trainedModelRepository = trainedModelRepository;
    }

    public UUID insert(TrainedModelDTO trainedModelDTO) {

        TrainedModelValidator.validateInsert(trainedModelDTO);

        return trainedModelRepository.save(TrainedModelBuilder.generateEntityFromDTO(trainedModelDTO)).getId();
    }


    public UUID updateStatus(TrainedModelDTO trainedModelDTO) {

        TrainedModelValidator.validateInsert(trainedModelDTO);

        TrainedModel trainedModel = trainedModelRepository.findByUUId(trainedModelDTO.getId());
        if(trainedModel != null) {
            trainedModel.setStatus("COMPLETED");
            trainedModel.setPathOnDisk(trainedModelDTO.getPathOnDisk());
            TrainedModelValidator.validateInsert(TrainedModelBuilder.generateDTOFromEntity(trainedModel));

            return trainedModelRepository.save(trainedModel).getId();
        }

        return null;
    }

    public TrainedModelDTO getStatusOnLatestTrainedModel(String dataScenario,
                            String predictionGranularity,
                            String algorithmType,
                            String topologyComponentType) {

        List<TrainedModel> trainedModels = trainedModelRepository.getStatusOfLatestTrainedModelForComponent(dataScenario,
                predictionGranularity,
                algorithmType,
                topologyComponentType);

        if(trainedModels == null || trainedModels.isEmpty()){
            throw new ResourceNotFoundException("Trained Models do not exist for the requested params");
        }else {
            return TrainedModelBuilder.generateDTOFromEntity(trainedModels.get(0));
        }
    }

    public List<TrainedModelDTO> findAll() {

        List<TrainedModel> trainedModelList = trainedModelRepository.findAll();

        return trainedModelList.stream()
                .map(TrainedModelBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
}
