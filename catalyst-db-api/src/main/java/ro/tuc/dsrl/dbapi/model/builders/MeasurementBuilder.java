package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasurementDTO;
import ro.tuc.dsrl.dbapi.model.*;

public class MeasurementBuilder {

    private MeasurementBuilder() {

    }

    public static MeasurementDTO generateDTOFromEntity(Measurement measurement,
                                                       DeviceType deviceType,
                                                       MeasureUnit measureUnit,
                                                       PropertySource propertySource,
                                                       ValueType valueType) {
        return new MeasurementDTO(
                measurement.getId(),
                measurement.getProperty(),
                measurement.getObservation(),
                deviceType.getId(),
                measureUnit.getId(),
                propertySource.getId(),
                valueType.getId()
        );
    }

    public static Measurement generateEntityFromDTO(MeasurementDTO measurementDTO,
                                                    DeviceType deviceType,
                                                    MeasureUnit measureUnit,
                                                    PropertySource propertySource,
                                                    ValueType valueType) {
        return new Measurement(
                measurementDTO.getId(),
                measurementDTO.getProperty(),
                measurementDTO.getObservation(),
                deviceType,
                measureUnit,
                propertySource,
                valueType
        );
    }
}