package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "property_source")
public class PropertySource {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "property_source")
    @NotNull
    private String sourceForProperty;

    public PropertySource() {

    }

    public PropertySource(@NotNull String propertySource) {
        this.sourceForProperty = propertySource;
    }

    public PropertySource(@NotNull UUID id, @NotNull String propertySource) {
        this.id = id;
        this.sourceForProperty = propertySource;
    }

    public UUID getId() {
        return id;
    }

    public String getPropertySource() {
        return sourceForProperty;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setPropertySource(String propertySource) {
        this.sourceForProperty = propertySource;
    }
}
