//package ro.tuc.dsrl.dbapi.repository;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//import ro.tuc.dsrl.catalyst.model.dto.optimization.ActionWithValuePerPropertyDTO;
//import ro.tuc.dsrl.catalyst.model.dto.optimization.FlexibilityOrderAndDCResponseDTO;
//import ro.tuc.dsrl.dbapi.model.ActionValue;
//
//import java.time.LocalDateTime;
//import java.util.List;
//import java.util.UUID;
//
//@Repository
//public interface ActionValueRepository extends JpaRepository<ActionValue, UUID> {
//
//    @Query(value = "SELECT a FROM ActionValue a where a.id = :id")
//    ActionValue findByUUID(@Param("id") UUID actionValueUUID);
//
//    @Query(value = "SELECT new ro.tuc.dsrl.catalyst.model.dto.optimization.ActionWithValuePerPropertyDTO(actionInstance.id, actionInstance.startTime, actionInstance.endTime, actionType.type, actionProperty.property, a.value) " +
//            "FROM ActionValue  a " +
//            "INNER JOIN  a.actionInstanceId actionInstance " +
//            "INNER JOIN  a.actionPropertyId actionProperty " +
//            "INNER JOIN  actionInstance.actionType actionType " +
//            "INNER JOIN  actionInstance.optimizationPlan optimizationPlan " +
//            "INNER JOIN  optimizationPlan.dataCenter dc " +
//            "WHERE actionInstance.startTime >= :startTime AND actionInstance.startTime < :endTime AND actionType.type = :actionType " +
//            "AND optimizationPlan.timeframe = :timeframe AND optimizationPlan.confidenceLevel = :confidenceLevel " +
//            "AND dc.label = :dataCenterName " +
//            "ORDER BY actionInstance.startTime")
//    List<ActionWithValuePerPropertyDTO> getActionsOnIntervalByTimeByTimeframe(@Param("startTime") LocalDateTime startTime,
//                                                                              @Param("endTime") LocalDateTime endTime,
//                                                                              @Param("actionType") String actionType,
//                                                                              @Param("confidenceLevel") Double confidenceLevel,
//                                                                              @Param("timeframe") String timeframe,
//                                                                              @Param("dataCenterName") String dataCenterName);
//
//    @Query(value = "SELECT new ro.tuc.dsrl.catalyst.model.dto.optimization.ActionWithValuePerPropertyDTO(actionInstance.id, actionInstance.startTime, actionInstance.endTime, actionType.type, actionProperty.property, a.value) " +
//            "FROM ActionValue  a " +
//            "INNER JOIN  a.actionInstanceId actionInstance " +
//            "INNER JOIN  a.actionPropertyId actionProperty " +
//            "INNER JOIN  actionInstance.actionType actionType " +
//            "INNER JOIN  actionInstance.optimizationPlan optimizationPlan " +
//            "INNER JOIN  optimizationPlan.dataCenter dc " +
//            "WHERE actionInstance.startTime <= :currentTime AND actionInstance.endTime > :currentTime AND actionType.type = :actionType " +
//            "AND optimizationPlan.timeframe = :timeframe AND optimizationPlan.confidenceLevel = :confidenceLevel " +
//            "AND dc.label = :dataCenterName " +
//            "ORDER BY actionInstance.startTime")
//    List<ActionWithValuePerPropertyDTO> getCurrentActionsByTypeByTimeframe(@Param("currentTime") LocalDateTime currentTime,
//                                                                           @Param("actionType") String actionType,
//                                                                           @Param("confidenceLevel") Double confidenceLevel,
//                                                                           @Param("timeframe") String timeframe,
//                                                                           @Param("dataCenterName") String dataCenterName);
//
//    @Query(value = "SELECT (AVG(av.value) * 60/:minutesPerSample), YEAR(ai.start_time), MONTH(ai.start_time), DAY(ai.start_time), HOUR(ai.start_time),  FLOOR(MINUTE(ai.start_time) /:minutesPerSample )  " +
//            "FROM action_value av " +
//            "JOIN action_instance ai  ON  ai.id = av.action_instance_id " +
//            "JOIN action_property ap ON ap.id = av.action_property_id " +
//            "JOIN action_type at0 ON at0.id = ai.action_type_id " +
//            "JOIN optimization_plans op ON op.id = ai.optimization_plan_id " +
//            "WHERE op.timeframe like :timeframe " +
//            "AND at0.type like  :type " +
//            "AND ai.start_time >= :start_date " +
//            "AND ai.start_time < :end_date " +
//            "AND op.confidence_level = :confidenceLevel " +
//            "GROUP BY YEAR(ai.start_time),MONTH(ai.start_time), DAY(ai.start_time), HOUR(ai.start_time),  FLOOR(MINUTE(ai.start_time)/:minutesPerSample  ) " +
//            "ORDER BY  YEAR(ai.start_time),MONTH(ai.start_time), DAY(ai.start_time), HOUR(ai.start_time),  FLOOR(MINUTE(ai.start_time)/:minutesPerSample )",  nativeQuery = true)
//    Object[][] getOptimizationActionValueHourRate(@Param("start_date") LocalDateTime startDate,
//                                                                              @Param("end_date") LocalDateTime endDate,
//                                                                              @Param("minutesPerSample") Integer sample,
//                                                                              @Param("timeframe") String timeframe,
//                                                                              @Param("type") String type,
//                                                                              @Param("confidenceLevel") Double confidenceLevel);
//
//
//    //TODO: query specific only to SDTW, reconsider setting parementers in query???
//    @Query(value = " SELECT   sum(av1.value), YEAR(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                        "MONTH(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                        "DAY(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                        "HOUR(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')) " +
//                    "FROM action_value av1 " +
//                    "JOIN action_instance ai1 ON  ai1.id = av1.action_instance_id " +
//                    "JOIN action_property ap1 ON ap1.id = av1.action_property_id " +
//                    "JOIN action_type at1 ON at1.id = ai1.action_type_id " +
//                    "INNER JOIN (Select  ai.id id, ai.start_time, ai.end_time, at0.type, ap.property, av.value  as hourAV " +
//                                "FROM action_value av " +
//                                "JOIN action_instance ai ON  ai.id = av.action_instance_id " +
//                                "JOIN action_property ap ON ap.id = av.action_property_id " +
//                                "JOIN action_type at0 ON at0.id = ai.action_type_id " +
//                                "JOIN optimization_plans op ON op.id = ai.optimization_plan_id "  +
//                                "WHERE  op.timeframe like \"day_ahead\" "+
//                                "and ai.start_time>= :start_date " +
//                                "and ai.end_time<= :end_date " +
//                                "and at0.type like \"Shift delay-tolerant workload (KWh)\" " +
//                                "and ap.property like \"sdtw_end_time\" ) inn " +
//                    "ON (inn.id = ai1.id ) " +
//                    "WHERE ap1.property like \"sdtw_amount\" "  +
//                    "GROUP BY YEAR(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                            "MONTH(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                            "DAY(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                            "HOUR(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')) " +
//                    "ORDER BY YEAR(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                            " MONTH(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                            " DAY(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i')), " +
//                            " HOUR(DATE_FORMAT(FROM_UNIXTIME( inn.hourAV/1000), '%Y-%m-%d %H:%i'))"  , nativeQuery = true)
//   Object[][] getSDTWHourRate(@Param("start_date") LocalDateTime startDate,
//                                      @Param("end_date") LocalDateTime endDate);
//}
