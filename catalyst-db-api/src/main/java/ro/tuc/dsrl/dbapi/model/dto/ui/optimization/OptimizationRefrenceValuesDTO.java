package ro.tuc.dsrl.dbapi.model.dto.ui.optimization;

import java.util.List;

public class OptimizationRefrenceValuesDTO {

    private List<Double> providedEnergyValues;

    private List<Double> requestedProfileValues;
    private List<Double> refrencePrices;
    private List<Double> predictedDemandValues;
    private List<Double> optimizedProfileValues;

    public OptimizationRefrenceValuesDTO(){}
    public OptimizationRefrenceValuesDTO(List<Double> providedEnergyValues, List<Double> requestedProfileValues, List<Double> refrencePrices, List<Double> predictedDemandValues, List<Double> optimizedProfileValues) {
        this.providedEnergyValues = providedEnergyValues;
        this.requestedProfileValues = requestedProfileValues;
        this.refrencePrices = refrencePrices;
        this.predictedDemandValues = predictedDemandValues;
        this.optimizedProfileValues = optimizedProfileValues;
    }

    public List<Double> getProvidedEnergyValues() {
        return providedEnergyValues;
    }

    public void setProvidedEnergyValues(List<Double> providedEnergyValues) {
        this.providedEnergyValues = providedEnergyValues;
    }

    public List<Double> getRequestedProfileValues() {
        return requestedProfileValues;
    }

    public void setRequestedProfileValues(List<Double> requestedProfileValues) {
        this.requestedProfileValues = requestedProfileValues;
    }

    public List<Double> getRefrencePrices() {
        return refrencePrices;
    }

    public void setRefrencePrices(List<Double> refrencePrices) {
        this.refrencePrices = refrencePrices;
    }

    public List<Double> getPredictedDemandValues() {
        return predictedDemandValues;
    }

    public void setPredictedDemandValues(List<Double> predictedDemandValues) {
        this.predictedDemandValues = predictedDemandValues;
    }

    public List<Double> getOptimizedProfileValues() {
        return optimizedProfileValues;
    }

    public void setOptimizedProfileValues(List<Double> optimizedProfileValues) {
        this.optimizedProfileValues = optimizedProfileValues;
    }
}
