package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "value_type")
public class ValueType {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "value_type")
    @NotNull
    private String typeOfValue;

    public ValueType() {

    }

    public ValueType(@NotNull String valueType) {
        this.typeOfValue = valueType;
    }

    public ValueType(@NotNull UUID id, @NotNull String valueType) {
        this.id = id;
        this.typeOfValue = valueType;
    }

    public UUID getId() {
        return id;
    }

    public String getValueType() {
        return typeOfValue;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setValueType(String valueType) {
        this.typeOfValue = valueType;
    }
}
