package ro.tuc.dsrl.dbapi.service.basic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.*;
import ro.tuc.dsrl.dbapi.model.builders.OptimizationActionBuilder;
import ro.tuc.dsrl.dbapi.model.builders.PredictedValueBuilder;
import ro.tuc.dsrl.dbapi.model.builders.PredictionJobBuilder;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.repository.*;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;
import ro.tuc.dsrl.dbapi.service.validators.OptimizationActionServiceValidator;
import ro.tuc.dsrl.dbapi.service.validators.PredictedValueValidator;
import ro.tuc.dsrl.geyser.datamodel.actions.*;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PredictedValueService {

    private final PredictedValueRepository predictedValueRepository;
    private final MeasurementRepository measurementRepository;
    private final DeviceRepository deviceRepository;
    private final PredictionJobRepository predictionJobRepository;
    private final PredictionJobService predictionJobService;

    private static final Log LOGGER = LogFactory.getLog(PredictedValueService.class);

    @Autowired
    public PredictedValueService(PredictedValueRepository predictedValueRepository, MeasurementRepository measurementRepository, DeviceRepository deviceRepository, PredictionJobRepository predictionJobRepository, PredictionJobService predictionJobService) {
        this.predictedValueRepository = predictedValueRepository;
        this.measurementRepository = measurementRepository;
        this.deviceRepository = deviceRepository;
        this.predictionJobRepository = predictionJobRepository;
        this.predictionJobService = predictionJobService;

    }

    public List<PredictedValueDTO> findAll() {

        List<PredictedValue> predictedValues = predictedValueRepository.findAll();

        return predictedValues.stream()
                .map(pv -> PredictedValueBuilder.generateDTOFromEntity(pv,
                        pv.getMeasurementId(),
                        pv.getDeviceId(),
                        pv.getPredictionJobId()))
                .collect(Collectors.toList());
    }

    public PredictedValueDTO findById(UUID predictedValueUUID) {
        PredictedValue predictedValue = predictedValueRepository.findByUUID(predictedValueUUID);
        if (predictedValue == null) {
            throw new ResourceNotFoundException("Predicted value with ID " + predictedValueUUID);
        }

        return PredictedValueBuilder.generateDTOFromEntity(
                predictedValue,
                predictedValue.getMeasurementId(),
                predictedValue.getDeviceId(),
                predictedValue.getPredictionJobId());
    }

    public UUID insert(PredictedValueDTO predictedValueDTO) {

        PredictedValueValidator.validateInsert(predictedValueDTO);

        List<String> errors = new ArrayList<>();
        Device device = deviceRepository.findByUUID(predictedValueDTO.getDeviceId());
        if (device == null) {
            errors.add("Device was not found in database.");
        }

        Measurement measurement = measurementRepository.findByUUID(predictedValueDTO.getMeasurementId());
        if (measurement == null) {
            errors.add("Measurement was not found in database.");
        }

        PredictionJob predictionJob = predictionJobRepository.findByUUID(predictedValueDTO.getPredictionJobId());
        if (predictionJob == null) {
            errors.add("Prediction Job was not found in database.");
        }

        if (!errors.isEmpty()) {
            throw new EntityValidationException(PredictedValue.class.getSimpleName(), errors);
        }

        return predictedValueRepository.save(PredictedValueBuilder.generateEntityFromDTO(predictedValueDTO, measurement, device, predictionJob)).getId();
    }

    public boolean setupEstimatedProfiles(String datacenterName, LocalDateTime startTime, DeviceTypeEnum deviceType,
                                          PredictionGranularity predictionGranularity, AggregationGranularity aggregationGranularity,
                                          MeasurementType measurementType, PredictionType predictionType, List<EnergySampleDTO> ePredicted) {
        List<Device> devices = deviceRepository.findByDeviceTypeAndParent(deviceType.type(), datacenterName);
        Device device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            throw new ResourceNotFoundException("Device type " + datacenterName);
        }

        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), startTime, predictionGranularity.getGranularity(), AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
        PredictionJob predictionJob = PredictionJobBuilder.generateEntityFromDTO(pjDTO);
        predictionJob = predictionJobRepository.save(predictionJob);

        Measurement measurement = measurementRepository.findMeasurementByProperty(measurementType.type());
        if (measurement == null) {
            throw new ResourceNotFoundException("Measurement type " + measurementType.type());
        }


        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(aggregationGranularity);
        eEnergyPriceProfile.setPredictionGranularity(predictionGranularity);
        eEnergyPriceProfile.setPredictionType(predictionType);
        eEnergyPriceProfile.setMeasurementId(measurement.getId());
        eEnergyPriceProfile.setDeviceId(device.getId());
        eEnergyPriceProfile.setCurve(ePredicted);

        boolean inserted = insertPredictedEnergyCurve(eEnergyPriceProfile, predictionJob.getId());
        if (!inserted) {
            LOGGER.error(measurementType.type() + " WAS NOT INSERTED " + predictionGranularity.getGranularity());
        }
        return inserted;
    }

    public boolean insertPredictedEnergyCurve(EnergyProfileDTO energyProfileDTO, UUID predictionJobUUID) {

        List<String> validationErrors = EnergyProfileValidator.validateCurveParams(energyProfileDTO);

        if (!validationErrors.isEmpty()) {
            return false;
        }
        for (EnergySampleDTO e : energyProfileDTO.getCurve()) {

            PredictedValueDTO predictedValueDTO = new PredictedValueDTO(e.getTimestamp(),
                    e.getEnergyValue(),
                    energyProfileDTO.getMeasurementId(),
                    energyProfileDTO.getDeviceId(),
                    predictionJobUUID );

            List<String> errors = new ArrayList<>();

            Measurement measurement = measurementRepository.findByUUID(predictedValueDTO.getMeasurementId());
            if (measurement == null) {
                errors.add("Measurement was not found in database." + predictedValueDTO.getMeasurementId());
            }
            Device device = deviceRepository.findByUUID(predictedValueDTO.getDeviceId());
            if (device == null) {
                errors.add("Device was not found in database." + predictedValueDTO.getDeviceId());
            }

            PredictionJob predictionJob = predictionJobRepository.findByUUID(predictedValueDTO.getPredictionJobId());
            if (predictionJob == null) {
                errors.add("Prediction Job was not found in database." + predictedValueDTO.getPredictionJobId());
            }

            if (!errors.isEmpty()) {
                throw new EntityValidationException(PredictedValueDTO.class.getSimpleName(), errors);
            }

            predictedValueRepository.save(PredictedValueBuilder.generateEntityFromDTO(predictedValueDTO,
                    measurement,
                    device,
                    predictionJob));
        }

        return true;

    }

    public void insertPredictedValuesMultipleEnergyTypes(EnergyProfileMultipleEnergyTypesDTO energyProfileDTO) {
        PredictedValueValidator.validateInsertPredictedValuesForMultipleEnergyTypes(energyProfileDTO);

        LocalDateTime predictionStartTime = DateUtils.millisToUTCLocalDateTime(energyProfileDTO.getStartTime());
        Device device = this.deviceRepository.findByUUID(this.deviceRepository.findByLabel(energyProfileDTO.getDeviceName()));

        if (device == null) {
            LOGGER.error("Device with name: " + energyProfileDTO.getDeviceName() + " NOT FOUND!");
            return;
        }

        AggregationGranularity aggGranurality = energyProfileDTO.getAggregationGranularity();
        PredictionGranularity predGranurality = energyProfileDTO.getPredictionGranularity();
        PredictionType predictionType = energyProfileDTO.getPredictionType();
        AlgorithmType algoType = energyProfileDTO.getAlgorithmType();

        PredictionJobDTO pjDTO = new PredictionJobDTO(predictionStartTime,
                predGranurality.getGranularity(),
                algoType.name(),
                FlexibilityType.NONE.getType());

        UUID pjId = predictionJobService.insert(pjDTO);

        for (EnergySamplesForMeasurementDTO esfMeasurementDTO : energyProfileDTO.getSamples()
        ) {

            Measurement measurement = this.measurementRepository.findByUUID(this.measurementRepository.findByProperty(esfMeasurementDTO.getMeasurementType().type()));
            if (measurement == null) {
                LOGGER.error("Measurement with type: " + esfMeasurementDTO.getMeasurementType().type() + " not found!!");
                continue;
            }

            EnergyProfileDTO eEnergyProfile = new EnergyProfileDTO();
            eEnergyProfile.setAggregationGranularity(aggGranurality);
            eEnergyProfile.setPredictionGranularity(predGranurality);
            eEnergyProfile.setPredictionType(predictionType);
            eEnergyProfile.setMeasurementId(measurement.getId());
            eEnergyProfile.setDeviceId(device.getId());

            List<EnergySampleDTO> ePredicted = new ArrayList<>();

            LocalDateTime time = predictionStartTime;

            for (Double value : esfMeasurementDTO.getValues()
            ) {
                EnergySampleDTO e = new EnergySampleDTO(value, time);
                ePredicted.add(e);
                time = time.plus(aggGranurality.getGranularity(), ChronoUnit.MINUTES);
            }

            eEnergyProfile.setCurve(ePredicted);

            this.insertPredictedEnergyCurve(eEnergyProfile, pjId);

        }

    }

}