package ro.tuc.dsrl.dbapi.service.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.GraphDataDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.LabeledValues;
import ro.tuc.dsrl.dbapi.service.history.EnergyHistoricalMonitoredValuesService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class ThermalGraphDataService {

    private final EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService;
    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;

    @Autowired
    public ThermalGraphDataService(EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService,
                                   EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService) {

        this.energyHistoricalMonitoredValuesService = energyHistoricalMonitoredValuesService;
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
    }

    /* monitored thermal production for a DC (taken only for the server component!!)*/
    public GraphDataDTO getMonitoredProductionForDC(UUID dataCenterID,
                                                    LocalDateTime startTime,
                                                    PredictionGranularity predictionGranularity,
                                                    String energyType) {

        // validate inputs
        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        List<EnergyProfileDTO> productionDTO = energyHistoricalMonitoredValuesService
                .getConsumptionForITComponent(dataCenterID,
                        startTime,
                        predictionGranularity,
                        energyType,
                        predictionGranularity.getSampleFrequencyMin(),
                        PredictionType.ENERGY_CONSUMPTION.getType());

        // sum the two profiles into one
        List<EnergySampleDTO> summedProduction = new ArrayList<>();
        for(int i=0; i< productionDTO.get(0).getCurve().size(); i++){
            EnergySampleDTO eDT =  productionDTO.get(0).getCurve().get(i);
            EnergySampleDTO eRT = productionDTO.get(1).getCurve().get(i);

            summedProduction.add(new EnergySampleDTO(eRT.getEnergyValue()+ eDT.getEnergyValue(),
                    eRT.getTimestamp()));
        }

        EnergyProfileDTO summedProductionProfile = new EnergyProfileDTO(summedProduction,
                productionDTO.get(0).getDeviceId(),
                productionDTO.get(0).getMeasurementId(),
                productionDTO.get(0).getAggregationGranularity(),
                productionDTO.get(0).getPredictionGranularity(),
                productionDTO.get(0).getPredictionType(),
                productionDTO.get(0).getEnergyType()
                );

        List<Double> historicalProductionData = padWithZerosIfNeeded(summedProductionProfile, predictionGranularity.getNoOutputs());

        // compute output
        LabeledValues productionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_PRODUCTION.getType().toLowerCase(), historicalProductionData);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(productionLabeledValues);

        return graphDataDTO;
    }

    /* monitored thermal production for a DC (taken only for the server component!!)*/
    public GraphDataDTO getPredictedProductionForDC(UUID dataCenterID,
                                                    LocalDateTime startTime,
                                                    PredictionGranularity predictionGranularity,
                                                    String energyType) {

        // validate inputs
        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        List<EnergyProfileDTO> energyProfiles = energyHistoricalMonitoredValuesService.getConsumptionForITComponent(
                dataCenterID,
                startTime,
                predictionGranularity,
                EnergyType.ELECTRICAL.name(),
                predictionGranularity.getSampleFrequencyMin(),
                PredictionType.ENERGY_CONSUMPTION.getType());

        List<Double> aggregatedValues = new ArrayList<>(Collections.nCopies(predictionGranularity.getNoOutputs(), 0.0));

        if(energyProfiles != null && !energyProfiles.isEmpty()) {

            for (EnergyProfileDTO profile : energyProfiles) {

                for (int j = 0; j < profile.getCurve().size(); j++) {

                    aggregatedValues.set(j, aggregatedValues.get(j) + profile.getCurve().get(j).getEnergyValue());
                }
            }
        }

        // compute output
        LabeledValues productionLabeledValues = new LabeledValues(
                PredictionType.ENERGY_PRODUCTION.getType().toLowerCase(), aggregatedValues);

        GraphDataDTO graphDataDTO = new GraphDataDTO(predictionGranularity.getNoOutputs());
        graphDataDTO.addLabeledValues(productionLabeledValues);

        return graphDataDTO;
    }

    private static List<Double> padWithZerosIfNeeded(EnergyProfileDTO energyProfileDTO, int size) {

        if (energyProfileDTO == null) {
            return new ArrayList<>(Collections.nCopies(size, 0.0));
        }

        List<Double> energyValues = energyProfileDTO.getEnergyValues();
        if (energyValues.size() == size) {
            return energyValues;
        }

        energyValues.addAll(Collections.nCopies(size - energyValues.size(), 0.0));
        return energyValues;
    }
}
