package ro.tuc.dsrl.dbapi.model.dto;

import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;

public class AggregatedActionValuesDTO {
    private double value;
    private double thermalValue;
    private Device deviceID;
    private Measurement measurementID;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int minuteSlots;


    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getThermalValue() {
        return thermalValue;
    }

    public void setThermalValue(double thermalValue) {
        this.thermalValue = thermalValue;
    }

    public Device getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(Device deviceID) {
        this.deviceID = deviceID;
    }

    public Measurement getMeasurementID() {
        return measurementID;
    }

    public void setMeasurementID(Measurement measurementID) {
        this.measurementID = measurementID;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getMinuteSlots() {
        return minuteSlots;
    }

    public void setMinuteSlots(int minuteSlots) {
        this.minuteSlots = minuteSlots;
    }
}
