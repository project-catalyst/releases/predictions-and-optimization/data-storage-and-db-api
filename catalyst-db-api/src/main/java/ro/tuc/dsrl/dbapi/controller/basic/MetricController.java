package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MetricsDTO;
import ro.tuc.dsrl.catalyst.model.enums.Scenario;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.service.basic.MetricService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/metric")
public class MetricController {
    private final MetricService metricService;

    @Value("${datacenter.name}")
    private String DC;

    private static final String SCENARIO_NOT_FOUND = "Scenario not found with scenario id: ";

    @Autowired
    public MetricController(MetricService metricService) {
        this.metricService = metricService;
    }

    @GetMapping(value = "/{dc-name}/{time}")
    public List<MetricsDTO> findAll(@PathVariable("dc-name") String dcName, @PathVariable("time") long time) {
        return metricService.getMetrics(DateUtils.millisToLocalDateTime(time), dcName);
    }

    @GetMapping(value = "/dayahead/{scenarioId}/{confidenceLevel}/{hour},{min}")
    public List<MetricsDTO> findAllPerDA(@PathVariable("scenarioId") int scenarioId,
                                         @PathVariable("confidenceLevel") double conf,
                                         @PathVariable("hour") int hour,
                                         @PathVariable("min") int min) {

        Scenario scenario = Scenario.getByIndex(scenarioId);

        if (scenario == null) {
            throw new ResourceNotFoundException(SCENARIO_NOT_FOUND + scenarioId);
        }


        LocalDateTime currentTime = scenario.getTime();
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);

        return metricService.getMetricsByPlan(currentTime, DC, Timeframe.DAY_AHEAD);
    }

    @GetMapping(value = "/intraday/{scenarioId}/{confidenceLevel}/{hour},{min}")
    public List<MetricsDTO> findAllPerID(@PathVariable("scenarioId") int scenarioId,
                                         @PathVariable("confidenceLevel") double conf,
                                         @PathVariable("hour") int hour,
                                         @PathVariable("min") int min) {

        Scenario scenario = Scenario.getByIndex(scenarioId);

        if (scenario == null) {
            throw new ResourceNotFoundException(SCENARIO_NOT_FOUND + scenarioId);
        }

        LocalDateTime currentTime = scenario.getTime();
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);

        return metricService.getMetricsByPlan(currentTime, DC, Timeframe.INTRA_DAY);
    }


    @GetMapping(value = "/realtime/{scenarioId}/{confidenceLevel}/{hour},{min}")
    public List<MetricsDTO> findAllPerRT(@PathVariable("scenarioId") int scenarioId,
                                         @PathVariable("confidenceLevel") double conf,
                                         @PathVariable("hour") int hour,
                                         @PathVariable("min") int min) {

        Scenario scenario = Scenario.getByIndex(scenarioId);

        if (scenario == null) {
            throw new ResourceNotFoundException(SCENARIO_NOT_FOUND + scenarioId);
        }


        LocalDateTime currentTime = scenario.getTime();
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);

        return metricService.getMetricsByPlan(currentTime, DC, Timeframe.NONE);
    }

    @PostMapping
    public MetricsDTO insertMetricDTO(@RequestBody MetricsDTO metricsDTO) {
        return metricService.insertMetric(metricsDTO);
    }

    @GetMapping(value = "/all")
    public List<List<MetricsDTO>> getAllMetrics()
    {
        return metricService.getAllMetricsByTime(DC);
    }
}
