package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.DeviceType;

public class DeviceBuilder {

    private DeviceBuilder() {

    }

    public static DeviceDTO generateDTOFromEntity(Device device,
                                                  DeviceType deviceType) {
        return new DeviceDTO(
                device.getId(),
                device.getLabel(),
                deviceType.getId(),
                (device.getParent()!=null) ? device.getParent().getId() : null );
    }

    public static Device generateEntityFromDTO(DeviceDTO deviceDTO,
                                               DeviceType deviceType, Device parent) {
        return new Device(
                deviceDTO.getId(),
                deviceDTO.getLabel(),
                deviceType,
                parent);
    }
}
