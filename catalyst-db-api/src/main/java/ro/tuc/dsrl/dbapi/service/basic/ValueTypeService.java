package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.ValueTypeDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.ValueType;
import ro.tuc.dsrl.dbapi.model.builders.ValueTypeBuilder;
import ro.tuc.dsrl.dbapi.repository.ValueTypeRepository;
import ro.tuc.dsrl.dbapi.service.validators.ValueTypeValidator;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ValueTypeService {
    private final ValueTypeRepository valueTypeRepository;

    @Autowired
    public ValueTypeService(ValueTypeRepository valueTypeRepository) {
        this.valueTypeRepository = valueTypeRepository;
    }

    public ValueTypeDTO findById(UUID valueTypeId) {
        ValueType valueType = valueTypeRepository.findByUUId(valueTypeId);
        if (valueType == null) {
            throw new ResourceNotFoundException("ValueType with ID " + valueTypeId);
        }

        return ValueTypeBuilder.generateDTOFromEntity(valueType);
    }

    public List<ValueTypeDTO> findAll() {
        List<ValueType> valueTypes = valueTypeRepository.findAll();

        return valueTypes.stream()
                .map(ValueTypeBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public UUID insert(ValueTypeDTO valueTypeDTO) {
        ValueTypeValidator.validateInsert(valueTypeDTO);

        ValueType valueType = ValueTypeBuilder.generateEntityFromDTO(valueTypeDTO);
        return valueTypeRepository.save(valueType).getId();
    }
}
