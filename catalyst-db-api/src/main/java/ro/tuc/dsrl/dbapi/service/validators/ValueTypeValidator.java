package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.ValueTypeDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class ValueTypeValidator {

    private ValueTypeValidator() {

    }

    private static final Log LOGGER = LogFactory.getLog(ValueTypeValidator.class);

    public static void validateInsert(ValueTypeDTO valueTypeDTO) {

        List<String> errors = new ArrayList<>();
        if (valueTypeDTO == null) {
            errors.add("Value Type DTO is null");
            throw new IncorrectParameterException(ValueTypeDTO.class.getSimpleName(), errors);
        }

        if (valueTypeDTO.getId() == null) {
            errors.add("Value type ID is null");
        }

        if (valueTypeDTO.getValueType() == null || valueTypeDTO.getValueType().equals("")) {
            errors.add("Value Type type is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(ValueTypeDTO.class.getSimpleName(), errors);
        }
    }
}
