package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.dbapi.model.PredictionJob;

public class PredictionJobBuilder {

    private PredictionJobBuilder() {

    }

    public static PredictionJobDTO generateDTOFromEntity(PredictionJob predictionJob) {
        return new PredictionJobDTO(
                predictionJob.getId(),
                predictionJob.getStartTime(),
                predictionJob.getGranularity(),
                predictionJob.getAlgorithmType(),
                predictionJob.getFlexibilityType()
        );
    }

    public static PredictionJob generateEntityFromDTO(PredictionJobDTO predictionJobDTO) {
        return new PredictionJob(
                predictionJobDTO.getStartTime(),
                predictionJobDTO.getGranularity(),
                predictionJobDTO.getAlgoType(),
                predictionJobDTO.getFlexibilityType()
        );
    }
}
