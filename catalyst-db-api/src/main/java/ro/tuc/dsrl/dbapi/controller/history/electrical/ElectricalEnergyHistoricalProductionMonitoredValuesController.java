package ro.tuc.dsrl.dbapi.controller.history.electrical;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.service.history.EnergyHistoricalMonitoredValuesService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/electrical/history/monitored-values/production")
public class ElectricalEnergyHistoricalProductionMonitoredValuesController {

    private final EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService;

    @Autowired
    public ElectricalEnergyHistoricalProductionMonitoredValuesController(EnergyHistoricalMonitoredValuesService energyHistoricalMonitoredValuesService) {
        this.energyHistoricalMonitoredValuesService = energyHistoricalMonitoredValuesService;
    }

    //region Production
    @GetMapping(value = "/4hoursbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalProductionMonitoredValuesOfIntraDayProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/24hoursbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalProductionMonitoredValuesOfDayAheadProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/1hourbefore/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalProductionMonitoredValuesOfNearRealtimeProductionForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalMonitoredValuesService.getProductionForEntireDC(
                UUID.fromString(dataCenterID),
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name(),
                AggregationGranularity.MINUTES_30.getGranularity(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }
    //endregion

    //region Production for Online Training
    @GetMapping(value = "/historyBeforeDateEvery4Hours/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalProductionMonitoredValuesOfIntraDayProductionForEntireDCBeforeDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        //TODO
        return null;
    }

    @GetMapping(value = "/historyBeforeDateEvery24Hours/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalProductionMonitoredValuesOfDayaheadProductionForEntireDCBeforeDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        //TODO
        return null;
    }

    @GetMapping(value = "/historyBeforeDateEvery1Hour/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalHistoricalProductionMonitoredValuesOfNearRealtimeProductionForEntireDCBeforeDate(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime){

        //TODO
        return null;
    }
    //endregion
}
