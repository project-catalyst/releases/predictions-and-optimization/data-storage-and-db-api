package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileMultipleEnergyTypesDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class PredictedValueValidator {

    private static final Log LOGGER = LogFactory.getLog(PredictedValueValidator.class);

    private PredictedValueValidator() {

    }

    public static void validateInsert(PredictedValueDTO predictedValueDTO) {

        List<String> errors = new ArrayList<>();
        if (predictedValueDTO == null) {
            errors.add("Predicted value DTO is null");
            throw new IncorrectParameterException(PredictedValueValidator.class.getSimpleName(), errors);
        }

        if (predictedValueDTO.getTimestamp() == null) {
            errors.add("Predicted value timestamp is null");
        }

        if (predictedValueDTO.getMeasurementId() == null) {
            errors.add("Predicted value measurement is null");
        }

        if (predictedValueDTO.getDeviceId() == null) {
            errors.add("Predicted value device is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PredictedValueValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsertPredictedValuesForMultipleEnergyTypes(EnergyProfileMultipleEnergyTypesDTO energyProfileMultipleEnergyTypesDTO) {

        List<String> errors = new ArrayList<>();

        if (energyProfileMultipleEnergyTypesDTO == null) {
            errors.add("EnergyProfileMultipleEnergyTypesDTO is null");
            throw new IncorrectParameterException(PredictedValueValidator.class.getSimpleName(), errors);
        }
        if (energyProfileMultipleEnergyTypesDTO.getStartTime() == null || energyProfileMultipleEnergyTypesDTO.getStartTime() == 0) {
            errors.add("Start time must not be null or empty");
        }

        if (energyProfileMultipleEnergyTypesDTO.getDeviceName() == null || energyProfileMultipleEnergyTypesDTO.getDeviceName().isEmpty()) {
            errors.add("Device name must not be null or empty");
        }

        if (energyProfileMultipleEnergyTypesDTO.getAggregationGranularity() == null) {
            errors.add("AggregationGranularity must not be null");
        }
        if (energyProfileMultipleEnergyTypesDTO.getPredictionGranularity() == null) {
            errors.add("PredictionGranularity must not be null");
        }
        if (energyProfileMultipleEnergyTypesDTO.getPredictionType() == null) {
            errors.add("PredictionType must not be null");
        }
        if (energyProfileMultipleEnergyTypesDTO.getAlgorithmType() == null) {
            errors.add("AlgorithmType must not be null");
        }
        if (energyProfileMultipleEnergyTypesDTO.getSamples() == null || energyProfileMultipleEnergyTypesDTO.getSamples().isEmpty()) {
            errors.add("Samples mut not be null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PredictedValueValidator.class.getSimpleName(), errors);
        }
    }
}
