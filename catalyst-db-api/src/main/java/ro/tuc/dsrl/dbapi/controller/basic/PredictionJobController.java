package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.PredictionJobService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/prediction-job")
public class PredictionJobController {

    private final PredictionJobService predictionJobService;

    @Autowired
    public PredictionJobController(PredictionJobService predictionJobService) {
        this.predictionJobService = predictionJobService;
    }

    @GetMapping(value = "/byId/{predictionJobId}")
    public PredictionJobDTO findById(@PathVariable("predictionJobId") String predictionJobId) {

        UUIDPreconditions.validate(predictionJobId);

        UUID predictionJobUUID = UUID.fromString(predictionJobId);

        return predictionJobService.findById(predictionJobUUID);
    }

    @GetMapping(value = "/all")
    public List<PredictionJobDTO> findAll() {

        return predictionJobService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertPredictionJobDTO(@RequestBody PredictionJobDTO predictionJobDTO) {

        return predictionJobService.insert(predictionJobDTO);
    }
}
