package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.FlexibilityStrategy;
import ro.tuc.dsrl.dbapi.model.OptimizationPlan;

import java.time.LocalDateTime;

public class FlexibilityStrategyBuilder {

    public FlexibilityStrategyBuilder() {
    }

    public static FlexibilityStrategyDTO getDefaultStrategies(String dcName) {
        return new FlexibilityStrategyDTO(0,0, 0,1, false, dcName  );
    }

    public static FlexibilityStrategyDTO generateDTOFromPlan(OptimizationPlan plan, String dcName) {
        return new FlexibilityStrategyDTO(
                plan.getWE(),
                plan.getWF(),
                plan.getWT(),
                plan.getWren(),
                plan.getReallocActive(),
                dcName
        );
    }

    public static FlexibilityStrategyDTO generateDTOFromEntity(FlexibilityStrategy flexibilityStrategy) {
        return new FlexibilityStrategyDTO(
                flexibilityStrategy.getWe(),
                flexibilityStrategy.getWf(),
                flexibilityStrategy.getWt(),
                flexibilityStrategy.getWren(),
                flexibilityStrategy.isReallocActive(),
                flexibilityStrategy.getDatacenter().getLabel()
        );
    }

    public static FlexibilityStrategy generateEntityFromDTO(FlexibilityStrategyDTO flexibilityStrategyDTO, Device device) {
        return new FlexibilityStrategy(
                flexibilityStrategyDTO.getWe(),
                0,
                flexibilityStrategyDTO.getWf(),
                flexibilityStrategyDTO.getWt(),
                flexibilityStrategyDTO.getREN(),
                flexibilityStrategyDTO.isReallocActive(),
                LocalDateTime.now(),
                device
        );
    }
}
