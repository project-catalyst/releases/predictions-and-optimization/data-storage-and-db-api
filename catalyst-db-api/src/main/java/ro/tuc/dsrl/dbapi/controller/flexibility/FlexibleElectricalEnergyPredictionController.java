package ro.tuc.dsrl.dbapi.controller.flexibility;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/electrical/prediction")
public class FlexibleElectricalEnergyPredictionController {


    //region Day ahead
    //region EntireDC
    @GetMapping(value = "/flexibility-above/dayahead/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfDayAheadProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfDayAheadProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region ITComponent
    @GetMapping(value = "/flexibility-above/dayahead/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfDayAheadProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfDayAheadProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region CoolingSystem
    @GetMapping(value = "flexibility-above/dayahead/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfDayAheadProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfDayAheadProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region Server
    @GetMapping(value = "/flexibility-above/dayahead/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfDayAheadProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfDayAheadProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }
    //endregion
    //endregion


    //region Intra day
    //region EntireDC
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfIntraDayProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfIntraDayProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region ITComponent
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfIntraDayProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfIntraDayProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region CoolingSystem
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfIntraDayProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfIntraDayProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region Server
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleAboveElectricalPredictionOfIntraDayProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleBelowElectricalPredictionOfIntraDayProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }
    //endregion
    //endregion
}
