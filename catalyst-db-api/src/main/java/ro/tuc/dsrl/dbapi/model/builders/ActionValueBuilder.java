//package ro.tuc.dsrl.dbapi.model.builders;
//
//import ro.tuc.dsrl.catalyst.model.dto.ActionValueDTO;
//import ro.tuc.dsrl.dbapi.model.ActionInstance;
//import ro.tuc.dsrl.dbapi.model.ActionProperty;
//import ro.tuc.dsrl.dbapi.model.ActionValue;
//
//public class ActionValueBuilder {
//
//    private ActionValueBuilder() {
//
//    }
//
//    public static ActionValueDTO generateDTOFromEntity(ActionValue actionValue,
//                                                       ActionInstance actionInstance,
//                                                       ActionProperty actionProperty) {
//        return new ActionValueDTO(
//                actionValue.getId(),
//                actionValue.getValue(),
//                actionInstance.getId(),
//                actionProperty.getId()
//        );
//    }
//
//    public static ActionValue generateEntityFromDTO(ActionValueDTO actionValueDTO,
//                                                    ActionInstance actionInstance,
//                                                    ActionProperty actionProperty) {
//        return new ActionValue(
//                actionValueDTO.getId(),
//                actionValueDTO.getValue(),
//                actionInstance,
//                actionProperty
//        );
//    }
//}
