package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.PropertySourceDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class PropertySourceValidator {

    private static final Log LOGGER = LogFactory.getLog(PropertySourceValidator.class);

    private PropertySourceValidator() {

    }

    public static void validateInsert(PropertySourceDTO propertySourceDTO) {

        List<String> errors = new ArrayList<>();
        if (propertySourceDTO == null) {
            errors.add("Property Source DTO is null");
            throw new IncorrectParameterException(PropertySourceDTO.class.getSimpleName(), errors);
        }

        if (propertySourceDTO.getId() == null) {
            errors.add("Property source ID is null");
        }

        if (propertySourceDTO.getPropertySource() == null || propertySourceDTO.getPropertySource().equals("")) {
            errors.add("Property Source is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PropertySourceDTO.class.getSimpleName(), errors);
        }
    }
}
