package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationPlanDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.OptimizationPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.OptimizationPlanService;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/optimization-plan")
public class OptimizationPlanController {

    private final OptimizationPlanService optimizationPlanService;

    @Autowired
    public OptimizationPlanController(OptimizationPlanService optimizationPlanService) {
        this.optimizationPlanService = optimizationPlanService;
    }

    @GetMapping(value = "/all")
    public List<OptimizationPlanDTO> findAll() {
        return optimizationPlanService.findAll();
    }

    @PostMapping(value = "/opt-container")
    public void insertOptimizationPlanContainer(@RequestBody OptimizationPlansContainer plans) {

        OptimizationPreconditions.validateOptimizationPlansContainerThrowErrors(plans);
        this.optimizationPlanService.insert(plans);

    }

}
