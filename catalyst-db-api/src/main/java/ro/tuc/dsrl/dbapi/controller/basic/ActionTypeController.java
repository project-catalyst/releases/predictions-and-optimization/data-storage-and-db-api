//package ro.tuc.dsrl.dbapi.controller.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ro.tuc.dsrl.catalyst.model.dto.ActionTypeDTO;
//import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
//import ro.tuc.dsrl.dbapi.service.basic.ActionTypeService;
//
//import java.util.List;
//import java.util.UUID;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/action-type")
//public class ActionTypeController {
//
//    private final ActionTypeService actionTypeService;
//
//    @Autowired
//    public ActionTypeController(ActionTypeService actionTypeService) {
//        this.actionTypeService = actionTypeService;
//    }
//
//    @GetMapping(value = "/byId/{actionTypeId}")
//    public ActionTypeDTO findById(@PathVariable("actionTypeId") String actionTypeId) {
//
//        UUIDPreconditions.validate(actionTypeId);
//
//        UUID actionTypeUUID = UUID.fromString(actionTypeId);
//
//        return actionTypeService.findById(actionTypeUUID);
//    }
//
//    @GetMapping(value = "/all")
//    public List<ActionTypeDTO> findAll() {
//
//        return actionTypeService.findAll();
//    }
//
//    @PostMapping(value = "/insert")
//    public UUID insertActionTypeDTO(@RequestBody ActionTypeDTO actionTypeDTO) {
//
//        return actionTypeService.insert(actionTypeDTO);
//    }
//}
