package ro.tuc.dsrl.dbapi.controller.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.ExecutionNotOptimizedDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.ExecutionOptimizedDTO;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.uisupport.ExecutionService;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
@RequestMapping(value = "/execution")
public class OptimizedExecutionController {
    private final ExecutionService executionService;
    private final DeviceService deviceService;
    @Value("${datacenter.name}")
    private String dcName;

    @Autowired
    public OptimizedExecutionController(ExecutionService executionService, DeviceService deviceService) {
        this.executionService = executionService;
        this.deviceService = deviceService;

    }

    @GetMapping(value = "/optimized/energy/{granularity}/{time}/{hour},{min}")
    public ExecutionOptimizedDTO getPredictedFlexibilityOfConsumptionForDC(@PathVariable("granularity") String granularity,
                                                                           @PathVariable("time") long time,
                                                                           @PathVariable("hour") int hour,
                                                                           @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return executionService.getOptimizedEnergyValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }

    @GetMapping(value = "/non-optimized/energy/{granularity}/{time}/{hour},{min}")
    public ExecutionNotOptimizedDTO getUnoptimizedPredictedFlexibilityOfConsumptionForDC(@PathVariable("granularity") String granularity,
                                                                                         @PathVariable("time") long time,
                                                                                         @PathVariable("hour") int hour,
                                                                                         @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return executionService.getNonOptimizedEnergyValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }

    @GetMapping(value = "/optimized/thermal/{granularity}/{time}/{hour},{min}")
    public ExecutionOptimizedDTO getOptimizedThermalForDC(@PathVariable("granularity") String granularity,
                                                                           @PathVariable("time") long time,
                                                                           @PathVariable("hour") int hour,
                                                                           @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);

        return executionService.getOptimizedThermalValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }

    @GetMapping(value = "/non-optimized/thermal/{granularity}/{time}/{hour},{min}")
    public ExecutionNotOptimizedDTO getUnoptimizedThermalForDC(@PathVariable("granularity") String granularity,
                                                                                         @PathVariable("time") long time,
                                                                                         @PathVariable("hour") int hour,
                                                                                         @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return executionService.getNonOptimizedThermalValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }

    @GetMapping(value = "/optimized/price-driven/{granularity}/{time}/{hour},{min}")
    public ExecutionOptimizedDTO getOptimizedPriceDrivenForDC(@PathVariable("granularity") String granularity,
                                                          @PathVariable("time") long time,
                                                          @PathVariable("hour") int hour,
                                                          @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);

        return executionService.getOptimizedPriceDrivenValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }

    @GetMapping(value = "/non-optimized/price-driven/{granularity}/{time}/{hour},{min}")
    public ExecutionNotOptimizedDTO getUnoptimizedPriceDrivenForDC(@PathVariable("granularity") String granularity,
                                                               @PathVariable("time") long time,
                                                               @PathVariable("hour") int hour,
                                                               @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return executionService.getNonOptimizedEnergyValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }


    @GetMapping(value = "/optimized/renewable/{granularity}/{time}/{hour},{min}")
    public ExecutionOptimizedDTO getOptimizedRenewableForDC(@PathVariable("granularity") String granularity,
                                                              @PathVariable("time") long time,
                                                              @PathVariable("hour") int hour,
                                                              @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);

        return executionService.getOptimizedRenewableValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }

    @GetMapping(value = "/non-optimized/renewable/{granularity}/{time}/{hour},{min}")
    public ExecutionNotOptimizedDTO getUnoptimizedRenewableForDC(@PathVariable("granularity") String granularity,
                                                                   @PathVariable("time") long time,
                                                                   @PathVariable("hour") int hour,
                                                                   @PathVariable("min") int min) {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);
        return executionService.getNonOptimizedEnergyValues(currentTime, dcName, PredictionGranularity.setGranularity(granularity));
    }

}