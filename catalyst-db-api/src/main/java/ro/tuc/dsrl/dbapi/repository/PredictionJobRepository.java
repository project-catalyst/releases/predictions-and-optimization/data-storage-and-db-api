package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.PredictionJob;

import java.time.LocalDateTime;
import java.util.UUID;

@Repository
public interface PredictionJobRepository extends JpaRepository<PredictionJob, UUID> {

    @Query(value = "SELECT e " +
            "FROM PredictionJob e " +
            "WHERE e.id = :predictionJobUUID")
    PredictionJob findByUUID(@Param("predictionJobUUID") UUID predictionJobUUID);

    @Query(value = "SELECT MAX(pv.predictionJobId.startTime) " +
            "FROM PredictedValue pv " +
            "JOIN pv.deviceId dev " +
            "JOIN pv.predictionJobId " +
            "LEFT JOIN dev.parent p " +
            "WHERE (dev.label LIKE UPPER(CONCAT('%', :deviceLabel,'%')) or p.label LIKE UPPER(CONCAT('%', :deviceLabel,'%'))) " +
            "AND pv.predictionJobId.granularity = :granularity " +
            "AND pv.predictionJobId.algorithmType = :algorithmType " +
            "AND pv.predictionJobId.flexibilityType = :flexibilityType " +
            "AND pv.timestamp = :startTime")
    LocalDateTime findJobDateTime(@Param("granularity") String granularity,
                                  @Param("algorithmType") String algorithmType,
                                  @Param("deviceLabel") String deviceLabel,
                                  @Param("flexibilityType") String flexibilityType,
                                  @Param("startTime") LocalDateTime startTime);

    @Query(value = "SELECT MAX(pv.predictionJobId.startTime) " +
            "FROM PredictedValue pv " +
            "JOIN pv.deviceId " +
            "JOIN pv.predictionJobId " +
            "JOIN pv.measurementId " +
            "WHERE pv.deviceId.label LIKE UPPER(CONCAT('%', :deviceLabel,'%')) " +
            "AND pv.measurementId.property LIKE UPPER(CONCAT('%', :measurementType,'%'))" +
            "AND pv.predictionJobId.granularity = :granularity " +
            "AND pv.predictionJobId.algorithmType = :algorithmType " +
            "AND pv.predictionJobId.flexibilityType = :flexibilityType " +
            "AND pv.timestamp = :startTime")
    LocalDateTime findJobDateTimeForMeasurement(@Param("granularity") String granularity,
                                  @Param("algorithmType") String algorithmType,
                                  @Param("deviceLabel") String deviceLabel,
                                  @Param("flexibilityType") String flexibilityType,
                                  @Param("startTime") LocalDateTime startTime,
                                  @Param("measurementType") String measurementType);

}