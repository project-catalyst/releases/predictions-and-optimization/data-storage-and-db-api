package ro.tuc.dsrl.dbapi.model.dto.resources;

public class CopFactors {
    private double copC;
    private  double copH;

    public CopFactors(double copC, double copH) {
        this.copC = copC;
        this.copH = copH;
    }
    public double getThermalEquivalentForElectricalEnergy(double electricalEnergy){
        return (copH-1) / copC * electricalEnergy;
    }

    public double getCopC() {
        return copC;
    }

    public void setCopC(double copC) {
        this.copC = copC;
    }

    public double getCopH() {
        return copH;
    }

    public void setCopH(double copH) {
        this.copH = copH;
    }

}
