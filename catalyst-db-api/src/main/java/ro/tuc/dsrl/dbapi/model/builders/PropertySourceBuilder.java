package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.PropertySourceDTO;
import ro.tuc.dsrl.dbapi.model.PropertySource;

public class PropertySourceBuilder {

    private PropertySourceBuilder() {

    }

    public static PropertySourceDTO generateDTOFromEntity(PropertySource propertySource) {
        return new PropertySourceDTO(
                propertySource.getId(),
                propertySource.getPropertySource()
        );
    }

    public static PropertySource generateEntityFromDTO(PropertySourceDTO propertySourceDTO) {
        return new PropertySource(
                propertySourceDTO.getId(),
                propertySourceDTO.getPropertySource()
        );
    }
}
