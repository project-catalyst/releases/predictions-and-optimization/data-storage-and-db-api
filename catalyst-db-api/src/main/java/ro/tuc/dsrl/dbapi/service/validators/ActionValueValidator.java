//package ro.tuc.dsrl.dbapi.service.validators;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import ro.tuc.dsrl.catalyst.model.dto.ActionValueDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ActionValueValidator {
//
//    private static final Log LOGGER = LogFactory.getLog(ActionValueValidator.class);
//
//    private ActionValueValidator() {
//
//    }
//
//    public static void validateInsert(ActionValueDTO actionValueDTO) {
//
//        List<String> errors = new ArrayList<>();
//        if (actionValueDTO == null) {
//            errors.add("Action Value DTO is null");
//            throw new IncorrectParameterException(ActionValueValidator.class.getSimpleName(), errors);
//        }
//
//        if (actionValueDTO.getId() == null) {
//            errors.add("Action value ID is null");
//        }
//
//        if (actionValueDTO.getValue() == null || actionValueDTO.getValue() == 0) {
//            errors.add("Action value value is null or empty");
//        }
//
//        if (actionValueDTO.getActionInstanceId() == null) {
//            errors.add("Action value action instance is null");
//        }
//
//        if (actionValueDTO.getActionPropertyId() == null) {
//            errors.add("Action value action property is null");
//        }
//
//        if (!errors.isEmpty()) {
//            LOGGER.error(errors);
//            throw new IncorrectParameterException(ActionValueValidator.class.getSimpleName(), errors);
//        }
//    }
//}
