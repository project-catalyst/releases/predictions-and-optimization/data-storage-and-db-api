package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.enums.ActionTypeName;
import ro.tuc.dsrl.catalyst.model.enums.Timeframe;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.dbapi.controller.preconditions.OptimizationPreconditions;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class OptimizationActionServiceValidator {

    private static final Log LOGGER = LogFactory.getLog(OptimizationActionServiceValidator.class);

    private static final String CONFIDENCE_NOT_NULL = "confidenceLevel time must not be null";

    private OptimizationActionServiceValidator() {
    }

    public static List<String> validateActivityDataCenterIdAndIntervalAndTimeframeAndConfidenceLevel(
                                                                                                     LocalDateTime startTime,
                                                                                                     LocalDateTime endTime,
                                                                                                     ActionTypeName actionTypeName,
                                                                                                     Double confidenceLevel,
                                                                                                     Timeframe timeframe) {

        List<String> errors = new ArrayList<>();


        if (startTime == null) {
            errors.add("startTime must not be null");
        }

        if (endTime == null) {
            errors.add("endTime must not be null");
        }

        if (startTime != null && endTime != null && startTime.isAfter(endTime)) {
            errors.add("StartTime must be after EndTime");
        }

        if (actionTypeName == null) {
            errors.add("actionTypeName must not be null");
        }

        if (confidenceLevel == null) {
            errors.add("confidenceLevel must not be null");
        }

        if (timeframe == null) {
            errors.add("timeframe must not be null");
        }

        return errors;

    }

    public static void validateActivityDataCenterIdAndIntervalAndTimeframeAndConfidenceLevelThrowErrors(
                                                                                                        LocalDateTime startTime,
                                                                                                        LocalDateTime endTime,
                                                                                                        ActionTypeName actionTypeName,
                                                                                                        Double confidenceLevel,
                                                                                                        Timeframe timeframe) {

        List<String> errors = OptimizationActionServiceValidator.validateActivityDataCenterIdAndIntervalAndTimeframeAndConfidenceLevel(startTime, endTime, actionTypeName, confidenceLevel, timeframe);
        OptimizationActionServiceValidator.throwIncorrectParameterException(errors);

    }

    public static List<String> validateCurrentTimeAndConfidenceLevel(LocalDateTime current, Double confidenceLevel) {

        List<String> errors = new ArrayList<>();

        if (current == null) {
            errors.add("current time must not be null");
        }

        if (confidenceLevel == null) {
            errors.add(CONFIDENCE_NOT_NULL);
        }

        return errors;

    }

    public static void validateCurrentTimeAndConfidenceLevelThrowErrors(LocalDateTime current, Double confidenceLevel) {
        List<String> errors = validateCurrentTimeAndConfidenceLevel(current, confidenceLevel);
        OptimizationActionServiceValidator.throwIncorrectParameterException(errors);
    }

    public static List<String> validateTimeIntervalConfidenceLevelAndTimeframe(LocalDateTime startTime,
                                                                               LocalDateTime endTime,
                                                                               Double confidenceLevel,
                                                                               Timeframe timeframe) {

        List<String> errors = new ArrayList<>();

        if (startTime == null) {
            errors.add("startTime time must not be null");
        }

        if (endTime == null) {
            errors.add("endTime time must not be null");
        }

        if (startTime != null && endTime != null && startTime.isAfter(endTime)) {
            errors.add("startTime must be before endTime");
        }

        if (confidenceLevel == null) {
            errors.add(CONFIDENCE_NOT_NULL);
        }

        if (timeframe == null) {
            errors.add("timeframe time must not be null");
        }


        return errors;

    }

    public static void validateTimeIntervalConfidenceLevelAndTimeframeThrowErrors(LocalDateTime startTime,
                                                                                  LocalDateTime endTime,
                                                                                  Double confidenceLevel,
                                                                                  Timeframe timeframe) {
        List<String> errors = OptimizationActionServiceValidator.validateTimeIntervalConfidenceLevelAndTimeframe(startTime, endTime, confidenceLevel, timeframe);
        OptimizationActionServiceValidator.throwIncorrectParameterException(errors);
    }

    public static List<String> validateCurrentTimeConfidenceLevelAndTimeframe(LocalDateTime currentTime,
                                                                              Double confidenceLevel,
                                                                              Timeframe timeframe) {

        List<String> errors = new ArrayList<>();

        if (currentTime == null) {
            errors.add("currentTime time must not be null");
        }

        if (confidenceLevel == null) {
            errors.add(CONFIDENCE_NOT_NULL);
        }

        if (timeframe == null) {
            errors.add("timeframe time must not be null");
        }


        return errors;

    }

    public static void validateCurrentTimeConfidenceLevelAndTimeframeThrowErrors(LocalDateTime currentTime,
                                                                                 Double confidenceLevel,
                                                                                 Timeframe timeframe) {
        List<String> errors = OptimizationActionServiceValidator.validateCurrentTimeConfidenceLevelAndTimeframe(currentTime, confidenceLevel, timeframe);
        OptimizationActionServiceValidator.throwIncorrectParameterException(errors);
    }
//
//    public static List<String> validateNewOptimizationPlanWithActions(OptimizationPlanWithActionsDTO optimizationPlan) {
//
//        List<String> errors = new ArrayList<>();
//
//        if (optimizationPlan == null) {
//            errors.add("Optimization plan DTO must not be null");
//            return errors;
//        }
//
//
//        if (optimizationPlan.getStartTimePlan() == null) {
//            errors.add("startTimePlan field must not be null");
//        }
//
//        if (optimizationPlan.getEndTimePlan() == null) {
//            errors.add("endTimePlan field must not be null");
//        }
//
//        if (optimizationPlan.getStartTimePlan() != null && optimizationPlan.getEndTimePlan() != null
//                && optimizationPlan.getStartTimePlan() > optimizationPlan.getEndTimePlan()) {
//            errors.add("startTimePlan must be lower then endTimePlan");
//        }
//
//        if (optimizationPlan.getTimeframe() == null || optimizationPlan.getTimeframe().isEmpty()) {
//            errors.add("timeframe field must not be null or an empty string");
//        } else if (Timeframe.getFromName(optimizationPlan.getTimeframe()) == null) {
//                errors.add(optimizationPlan.getTimeframe() + " is not a valid timeframe value");
//        }
//
//        if (optimizationPlan.getCarbonSavings() == null) {
//            errors.add("carbonSavings field must not be null");
//        }
//
//        if (optimizationPlan.getConfidenceLevel() == null) {
//            errors.add("confidenceLevel field must not be null");
//        }
//
//        if (optimizationPlan.getCostSavings() == null) {
//            errors.add("costSavings field must not be null");
//        }
//
//        if (optimizationPlan.getEnergySavings() == null) {
//            errors.add("energySavings field must not be null");
//        }
//
//        if (optimizationPlan.getSelected() == null) {
//            errors.add("selected field must not be null");
//        }
//
//
//        if (optimizationPlan.getActions() == null || optimizationPlan.getActions().isEmpty()) {
//            errors.add("actions list must not be null or empty");
//        }
//
//
//        return errors;
//    }
//
//    public static void validateNewOptimizationPlanWithActionsThrowErrors(OptimizationPlanWithActionsDTO optimizationPlan) {
//
//        List<String> errors = validateNewOptimizationPlanWithActions(optimizationPlan);
//        throwIncorrectParameterException(errors);
//    }

    public static void validateOptimizationPlanContainer(OptimizationPlansContainer plans) {
        List<String> errors = OptimizationPreconditions.validateOptimizationPlansContainer(plans);
        throwIncorrectParameterException(errors);
    }

    public static void validateCurrentDateAndConfidenceLevelThrowErrors(Long currentDateMillis,
                                                                        Double confidenceLevel) {

        OptimizationPreconditions.validateCurrentDateAndConfidenceLevelThrowErrors(currentDateMillis, confidenceLevel);
    }


    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(OptimizationPreconditions.class.getSimpleName(), errors);
        }
    }
}
