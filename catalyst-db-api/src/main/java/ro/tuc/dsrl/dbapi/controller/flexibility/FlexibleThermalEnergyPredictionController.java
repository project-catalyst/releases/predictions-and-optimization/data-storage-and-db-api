package ro.tuc.dsrl.dbapi.controller.flexibility;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/thermal/prediction")
public class FlexibleThermalEnergyPredictionController {


    //region Day-ahead
    //region EntireDC
    @GetMapping(value = "/flexibility-above/dayahead/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleAboveThermalPredictionOfDayAheadProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleBelowThermalPredictionOfDayAheadProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region ITComponent
    @GetMapping(value = "/flexibility-above/dayahead/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleAboveThermalPredictionOfDayAheadProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleBelowThermalPredictionOfDayAheadProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region CoolingSystem
    @GetMapping(value = "/flexibility-above/dayahead/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleAboveThermalPredictionOfDayAheadProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleBelowThermalPredictionOfDayAheadProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region Server
    @GetMapping(value = "/flexibility-above/dayahead/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleAboveThermalPredictionOfDayAheadProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/dayahead/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleBelowThermalPredictionOfDayAheadProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }
    //endregion
    //endregion


    //region Intra-day
    //region EntireDC
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleAboveThermalPredictionOfIntraDayProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/entireDC/{startTime}")
    public void findFlexibleBelowThermalPredictionOfIntraDayProductionForEntireDCInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region ITComponent
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleAboveThermalPredictionOfIntraDayProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/itComponent/{startTime}")
    public void findFlexibleBelowThermalPredictionOfIntraDayProductionForITComponentInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region CoolingSystem
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleAboveThermalPredictionOfIntraDayProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/coolingSystem/{startTime}")
    public void findFlexibleBelowThermalPredictionOfIntraDayProductionForCoolingSystemInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID) {

        //TODO
    }
    //endregion


    //region Server
    @GetMapping(value = "/flexibility-above/intraday/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleAboveThermalPredictionOfIntraDayProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }

    @GetMapping(value = "/flexibility-below/intraday/{dataCenterID}/server/{serverID}/{startTime}")
    public void findFlexibleBelowThermalPredictionOfIntraDayProductionForServerInInterval(
            @PathVariable("startTime") Long startTime,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable String serverID) {

        //TODO
    }
    //endregion
    //endregion
}
