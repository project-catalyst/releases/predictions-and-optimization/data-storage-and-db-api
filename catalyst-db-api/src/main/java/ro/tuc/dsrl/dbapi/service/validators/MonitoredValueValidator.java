package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class MonitoredValueValidator {

    private static final Log LOGGER = LogFactory.getLog(MonitoredValueValidator.class);

    private MonitoredValueValidator() {

    }

    public static void validateInsert(MonitoredValueDTO monitoredValueDTO) {

        List<String> errors = new ArrayList<>();
        if (monitoredValueDTO == null) {
            errors.add("Monitored value DTO is null");
            throw new IncorrectParameterException(MonitoredValueValidator.class.getSimpleName(), errors);
        }

        if (monitoredValueDTO.getTimestamp() == null) {
            errors.add("Monitored value timestamp is null");
        }

        if (monitoredValueDTO.getDeviceId() == null) {
            errors.add("Monitored value device is null");
        }

        if (monitoredValueDTO.getMeasurementId() == null) {
            errors.add("Monitored value measurement is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MeasurementValidator.class.getSimpleName(), errors);
        }
    }
}
