//package ro.tuc.dsrl.dbapi.service.validators;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import ro.tuc.dsrl.catalyst.model.dto.ActionInstanceDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ActionInstanceValidator {
//
//    private static final Log LOGGER = LogFactory.getLog(ActionInstanceValidator.class);
//
//    private ActionInstanceValidator() {
//    }
//
//    public static void validateInsert(ActionInstanceDTO actionInstanceDTO) {
//
//        List<String> errors = new ArrayList<>();
//        if (actionInstanceDTO == null) {
//            errors.add("Action Instance DTO is null");
//            throw new IncorrectParameterException(ActionInstanceValidator.class.getSimpleName(), errors);
//        }
//
//        if (actionInstanceDTO.getId() == null) {
//            errors.add("Action instance ID is null");
//        }
//
//        if (actionInstanceDTO.getLabel() == null || actionInstanceDTO.getLabel().equals("")) {
//            errors.add("Action Instance Label is null or empty");
//        }
//
//        if (actionInstanceDTO.getStartTime() == null) {
//            errors.add("Action Instance Start Time is null");
//        }
//
//        if (actionInstanceDTO.getEndTime() == null) {
//            errors.add("Action Instance End Time is null");
//        }
//
//        if (actionInstanceDTO.getEndTime().isBefore(actionInstanceDTO.getStartTime())) {
//            errors.add("Action Instance End Time is before Start Time");
//        }
//
//        if (actionInstanceDTO.getActionTypeId() == null) {
//            errors.add("Action instance action type is null");
//        }
//
//        if (!errors.isEmpty()) {
//            LOGGER.error(errors);
//            throw new IncorrectParameterException(ActionInstanceValidator.class.getSimpleName(), errors);
//        }
//    }
//}
