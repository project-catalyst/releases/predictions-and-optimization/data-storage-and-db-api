package ro.tuc.dsrl.dbapi.controller.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.GraphDataDTO;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.uisupport.ThermalGraphDataService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/graph-data/thermal")
public class ThermalGraphDataController {

    @Value("${datacenter.name}")
    private String dataCenterName;

    private final ThermalGraphDataService thermalGraphDataService;
    private final DeviceService deviceService;

    @Autowired
    public ThermalGraphDataController(ThermalGraphDataService thermalGraphDataService, DeviceService deviceService) {
        this.thermalGraphDataService = thermalGraphDataService;
        this.deviceService = deviceService;
    }

    //region Thermal History Production for DC is the history consumption for server
    @GetMapping(value = "/history/production/dayahead/{startTime}")
    public GraphDataDTO getMonitoredDayaheadProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return thermalGraphDataService.getMonitoredProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name());
    }

    @GetMapping(value = "/history/production/intraday/{startTime}")
    public GraphDataDTO getMonitoredIntradayProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return thermalGraphDataService.getMonitoredProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name());
    }

    @GetMapping(value = "/history/production/nearRealTime/{startTime}")
    public GraphDataDTO getMonitoredNearRealTimeProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return thermalGraphDataService.getMonitoredProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name());
    }
    //endregion

    //region Thermal Prediction Production for DC
    @GetMapping(value = "/prediction/production/dayahead/{startTime}")
    public GraphDataDTO getPredictedDayaheadProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return thermalGraphDataService.getPredictedProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name());
    }

    @GetMapping(value = "/prediction/production/intraday/{startTime}")
    public GraphDataDTO getPredictedIntradayProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return thermalGraphDataService.getPredictedProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name());
    }

    @GetMapping(value = "/prediction/production/nearRealTime/{startTime}")
    public GraphDataDTO getPredictedNearRealTimeProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return thermalGraphDataService.getPredictedProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name());
    }
    //endregion
}
