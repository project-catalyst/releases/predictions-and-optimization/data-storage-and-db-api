//package ro.tuc.dsrl.dbapi.model.builders;
//
//import ro.tuc.dsrl.catalyst.model.dto.ActionPropertyDTO;
//import ro.tuc.dsrl.dbapi.model.ActionProperty;
//import ro.tuc.dsrl.dbapi.model.ActionType;
//import ro.tuc.dsrl.dbapi.model.MeasureUnit;
//import ro.tuc.dsrl.dbapi.model.ValueType;
//
//public class ActionPropertyBuilder {
//
//    private ActionPropertyBuilder() {
//
//    }
//
//    public static ActionPropertyDTO generateDTOFromEntity(ActionProperty actionProperty,
//                                                          MeasureUnit measureUnit,
//                                                          ValueType valueType,
//                                                          ActionType actionType) {
//        return new ActionPropertyDTO(
//                actionProperty.getId(),
//                actionProperty.getProperty(),
//                actionProperty.getObservation(),
//                measureUnit.getId(),
//                valueType.getId(),
//                actionType.getId()
//        );
//    }
//
//    public static ActionProperty generateEntityFromDTO(ActionPropertyDTO actionPropertyDTO,
//                                                       MeasureUnit measureUnit,
//                                                       ValueType valueType,
//                                                       ActionType actionType) {
//        return new ActionProperty(
//                actionPropertyDTO.getId(),
//                actionPropertyDTO.getProperty(),
//                actionPropertyDTO.getObservation(),
//                measureUnit,
//                valueType,
//                actionType
//        );
//    }
//}
//
