package ro.tuc.dsrl.dbapi.controller.preconditions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class PropertyPreconditions {

    private static final Log LOGGER = LogFactory.getLog(PropertyPreconditions.class);

    private PropertyPreconditions() {

    }

    public static List<String> validate(String property) {

        List<String> errors = new ArrayList<>();
        if (property == null) {
            errors.add("Property must not be null");
        }
        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect Parameters", errors);
        }

        return errors;
    }

    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(TotalPowerPreconditions.class.getSimpleName(), errors);
        }
    }
}
