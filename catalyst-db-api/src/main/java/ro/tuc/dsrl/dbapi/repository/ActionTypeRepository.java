//package ro.tuc.dsrl.dbapi.repository;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//import ro.tuc.dsrl.dbapi.model.ActionType;
//
//import java.util.UUID;
//
//@Repository
//public interface ActionTypeRepository extends JpaRepository<ActionType, UUID> {
//
//    @Query(value = "SELECT e " +
//            "FROM ActionType e " +
//            "WHERE e.id = :actionTypeUUID")
//    ActionType findByUUID(@Param("actionTypeUUID") UUID actionTypeUUID);
//
//    @Query(value = "SELECT e " +
//            "FROM ActionType e " +
//            "WHERE e.type = :actionTypeName")
//    ActionType findByName(@Param("actionTypeName") String actionTypeName);
//}
