package ro.tuc.dsrl.dbapi.controller.prediction.thermal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/thermal/prediction/production")
public class ThermalEnergyProductionPredictionController {

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    private final PredictedValueService predictedValueService;

    @Autowired
    public ThermalEnergyProductionPredictionController(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService,
                                                       PredictedValueService predictedValueService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
        this.predictedValueService = predictedValueService;
    }

    //region Get
    //region IntraDay Get Methods
    @GetMapping(value = "/intraday/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfIntraDayForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType(),
                FlexibilityType.NONE.getType());
    }

    @GetMapping(value = "/intraday/{dataCenterID}/itComponent/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfIntraDayForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForITComponent(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/intraday/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfIntraDayForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForCoolingSystem(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/intraday/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfIntraDayForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime, serverID);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);
        UUID serverUUID = UUID.fromString(serverID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForServer(
                dataCenterUUID,
                startLocalDateTime,
                serverUUID,
                PredictionGranularity.INTRADAY,
                EnergyType.THERMAL.name());
    }
    //endregion


    //region DayAhead Get Methods
    @GetMapping(value = "/dayahead/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfDayAheadForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType(),
                FlexibilityType.NONE.getType());

    }

    @GetMapping(value = "/dayahead/{dataCenterID}/itComponent/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfDayAheadForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForITComponent(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/dayahead/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfDayAheadForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForCoolingSystem(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/dayahead/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfDayAheadForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("serverID") String serverID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);
        UUID serverUUID = UUID.fromString(serverID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForServer(
                dataCenterUUID,
                startLocalDateTime,
                serverUUID,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name());
    }
    //endregion


    //region NearRealTime Get Methods
    @GetMapping(value = "/nearRealTime/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO findElectricalPredictionOfNearRealtimeForEntireDCInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        EnergyProfilePreconditions.validate(dataCenterID, startTime);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForEntireDC(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType(),
                FlexibilityType.NONE.getType());

    }

    @GetMapping(value = "/nearRealTime/{dataCenterID}/itComponent/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfNearRealtimeForITComponentInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForITComponent(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/nearRealTime/{dataCenterID}/coolingSystem/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfNearRealtimeForCoolingSystemInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForCoolingSystem(
                dataCenterUUID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.THERMAL.name(),
                PredictionType.ENERGY_PRODUCTION.getType());
    }

    @GetMapping(value = "/nearRealTime/{dataCenterID}/server/{serverID}/{startTime}")
    public EnergyProfileDTO findThermalPredictionOfNearRealtimeForServerInInterval(
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime,
            @PathVariable("serverID") String serverID) {

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        UUID dataCenterUUID = UUID.fromString(dataCenterID);
        UUID serverUUID = UUID.fromString(serverID);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return energyHistoricalPredictedValuesService.getPredictedProductionForServer(
                dataCenterUUID,
                startLocalDateTime,
                serverUUID,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.THERMAL.name());
    }
    //endregion
    //endregion


    //region Insertions
    @PostMapping(value = {"/{dataCenterID}/entireDC",
            "/{dataCenterID}/itComponent",
            "/{dataCenterID}/CoolingSystem"})
    public void insertThermalPredictionForEntireDC(@PathVariable("dataCenterID") String dataCenterID,
                                                   @RequestBody PredictedValueDTO predictedValueDTO) {

        UUIDPreconditions.validate(dataCenterID);

        predictedValueService.insert(predictedValueDTO);
    }

    @PostMapping(value = "/{dataCenterID}/server/{serverID}")
    public void insertThermalPredictionForServer(@PathVariable("dataCenterID") String dataCenterID,
                                                 @PathVariable("serverID") String serverID, @RequestBody PredictedValueDTO predictedValueDTO) {

        UUIDPreconditions.validate(dataCenterID);

        predictedValueService.insert(predictedValueDTO);
    }
    //endregion
}
