package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileMultipleEnergyTypesDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/predicted-value")
public class PredictedValueController {

    private final PredictedValueService predictedValueService;

    @Autowired
    public PredictedValueController(PredictedValueService predictedValueService) {
        this.predictedValueService = predictedValueService;
    }

    @GetMapping(value = "/byId/{predictedValueId}")
    public PredictedValueDTO findById(@PathVariable("predictedValueId") String predictedValueId) {

        UUIDPreconditions.validate(predictedValueId);

        UUID predictedValueUUID = UUID.fromString(predictedValueId);

        return predictedValueService.findById(predictedValueUUID);
    }

    @GetMapping(value = "/all")
    public List<PredictedValueDTO> findAll() {

        return predictedValueService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertPredictedValueDTO(@RequestBody PredictedValueDTO predictedValueDTO) {

        return predictedValueService.insert(predictedValueDTO);
    }

    @PostMapping(value = "/insert_predicted_energy_curve/{predictionJobID}")
    public boolean insertPredictedValueDTO(
            @RequestBody EnergyProfileDTO energyProfileDTO,
            @PathVariable("predictionJobID") String predictionJobID) {

        UUIDPreconditions.validate(predictionJobID);
        UUID predictionJobUUID = UUID.fromString(predictionJobID);

        return predictedValueService.insertPredictedEnergyCurve(energyProfileDTO, predictionJobUUID);
    }

    @PostMapping(value = "/insert_predicted_values_multiple_energy_types/")
    public void insertPredictedValuesForMultipleEnergyTypes(
            @RequestBody EnergyProfileMultipleEnergyTypesDTO energyProfileDTO) {

        predictedValueService.insertPredictedValuesMultipleEnergyTypes(energyProfileDTO);
    }
}
