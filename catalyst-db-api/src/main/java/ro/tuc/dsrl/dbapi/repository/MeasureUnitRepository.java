package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.MeasureUnit;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MeasureUnitRepository extends JpaRepository<MeasureUnit, UUID> {

    Optional<MeasureUnit> findById(UUID uuid);

    @Query(value = "SELECT e " +
            "FROM MeasureUnit e " +
            "WHERE e.id = :measureUnitUUID")
    MeasureUnit findByUUID(@Param("measureUnitUUID") UUID measureUnitUUID);
}
