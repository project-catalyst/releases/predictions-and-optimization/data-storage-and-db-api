package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationPlanDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.OptimizationPlan;

public class OptimizationPlanBuilder {

    private OptimizationPlanBuilder(){
    }

    public static OptimizationPlanDTO generateDTOFromEntity(OptimizationPlan optPlan) {
        return new OptimizationPlanDTO(
                optPlan.getId(),
                optPlan.getTimeframe(),
                optPlan.getStart(),
                optPlan.getEnd(),
                optPlan.getSelected(),
                optPlan.getConfidenceLevel(),
                optPlan.getCostSavings(),
                optPlan.getCarbonSavings(),
                optPlan.getEnergySavings(),
                optPlan.getWE(),
                optPlan.getWL(),
                optPlan.getWF(),
                optPlan.getWT(),
                optPlan.getWren(),
                optPlan.getReallocActive(),
                optPlan.getDataCenter() != null ? optPlan.getDataCenter().getId() : null);
    }

    public static OptimizationPlan generateEntityFromDTO(OptimizationPlanDTO optimizationPlanDTO, Device dataCenter) {
        return new OptimizationPlan(optimizationPlanDTO.getTimeframe(),
                optimizationPlanDTO.getStart(),
                optimizationPlanDTO.getEnd(),
                optimizationPlanDTO.getSelected(),
                optimizationPlanDTO.getConfidenceLevel(),
                optimizationPlanDTO.getCostSavings(),
                optimizationPlanDTO.getCarbonSavings(),
                optimizationPlanDTO.getEnergySavings(),
                optimizationPlanDTO.getWE(),
                optimizationPlanDTO.getWL(),
                optimizationPlanDTO.getWF(),
                optimizationPlanDTO.getWT(),
                optimizationPlanDTO.getWren(),
                optimizationPlanDTO.getReallocActive(),
                dataCenter
        );
    }

}
