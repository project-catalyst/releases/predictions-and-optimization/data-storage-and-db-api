//package ro.tuc.dsrl.dbapi.service.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import ro.tuc.dsrl.catalyst.model.dto.ActionTypeDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
//import ro.tuc.dsrl.dbapi.model.ActionType;
//import ro.tuc.dsrl.dbapi.model.builders.ActionTypeBuilder;
//import ro.tuc.dsrl.dbapi.repository.ActionTypeRepository;
//import ro.tuc.dsrl.dbapi.service.validators.ActionTypeValidator;
//
//import java.util.List;
//import java.util.UUID;
//import java.util.stream.Collectors;
//
//@Service
//public class ActionTypeService {
//    private final ActionTypeRepository actionTypeRepository;
//
//    @Autowired
//    public ActionTypeService(ActionTypeRepository actionTypeRepository) {
//        this.actionTypeRepository = actionTypeRepository;
//    }
//
//    public ActionTypeDTO findById(UUID actionTypeUUID) {
//
//        ActionType actionType = actionTypeRepository.findByUUID(actionTypeUUID);
//        if (actionType == null) {
//            throw new ResourceNotFoundException("ActionType with ID " + actionTypeUUID);
//        }
//
//        return ActionTypeBuilder.generateDTOFromEntity(actionType);
//    }
//
//    public List<ActionTypeDTO> findAll() {
//
//        List<ActionType> actionTypes = actionTypeRepository.findAll();
//
//        return actionTypes.stream()
//                .map(ActionTypeBuilder::generateDTOFromEntity)
//                .collect(Collectors.toList());
//    }
//
//    public UUID insert(ActionTypeDTO actionTypeDTO) {
//
//        ActionTypeValidator.validateInsert(actionTypeDTO);
//
//        ActionType actionType = ActionTypeBuilder.generateEntityFromDTO(actionTypeDTO);
//        return actionTypeRepository.save(actionType).getId();
//    }
//}
