package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MetricsDTO;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.*;

public class MetricsBuilder {

    private MetricsBuilder() {
    }

    public static MetricsDTO generateDTOFromEntity(Metric metric, Measurement m, Device d , OptimizationPlan p) {
        String timeframe = p!=null? p.getTimeframe() : "realtime";

        return new MetricsDTO(metric.getId().toString(), DateUtils.localDateTimeToLongInMillis(metric.getTimestamp()), d.getLabel(),m.getProperty(), metric.getValue(), timeframe );
    }
}
