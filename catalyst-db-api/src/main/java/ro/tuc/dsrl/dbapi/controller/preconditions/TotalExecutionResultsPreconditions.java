package ro.tuc.dsrl.dbapi.controller.preconditions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class TotalExecutionResultsPreconditions {

    private static final Log LOGGER = LogFactory.getLog(TotalExecutionResultsPreconditions.class);

    private TotalExecutionResultsPreconditions(){

    }

    public static void validateCurrentDateAndDCNameThrowErrors(Long currentDateMillis,
                                                                        String dcName) {
        List<String> errors = new ArrayList<>();

        if (currentDateMillis == null) {
            errors.add("Current date must not be null");
        } else if (currentDateMillis == 0) {
            errors.add("Current date must not be 0");
        }

        if (dcName == null  || dcName.isEmpty()) {
            errors.add("DC Name must not be empty. ");
        }
        throwIncorrectParameterException(errors);
    }

    public static List<String> validateTotalExecutionResults(DatacenterStateDTO totalExecutionResultsDTO){

        List<String> errors = new ArrayList<>();

        if(totalExecutionResultsDTO == null){
            errors.add("TotalExecutionResultsDTO must not be null");
        }

        return errors;
    }


    public static void validateTotalExecutionResultsThrowErrors(DatacenterStateDTO totalExecutionResultsDTO){

        List<String> errors = validateTotalExecutionResults(totalExecutionResultsDTO);
        throwIncorrectParameterException(errors);

    }

    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(TotalPowerPreconditions.class.getSimpleName(), errors);
        }
    }
}
