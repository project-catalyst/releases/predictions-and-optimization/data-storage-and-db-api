package ro.tuc.dsrl.dbapi.repository;

import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.dbapi.model.PredictedValue;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PredictedValueRepository extends JpaRepository<PredictedValue, UUID> {

    Optional<PredictedValue> findById(UUID uuid);

    @Query(value = "SELECT e " +
            "FROM PredictedValue e " +
            "WHERE e.id = :predictedValueUUID")
    PredictedValue findByUUID(@Param("predictedValueUUID") UUID predictedValueUUID);

    @Query(value = "SELECT pv " +
            "FROM PredictedValue pv " +
            "JOIN FETCH pv.predictionJobId  pj " +
            "JOIN FETCH pv.measurementId  m " +
            "JOIN FETCH pv.deviceId d " +
            "INNER JOIN d.parent p " +
            "JOIN FETCH d.deviceTypeId dt " +
            "WHERE pj.granularity = :granularity " +
            "AND pj.algorithmType = :algorithmType " +
            "AND pj.flexibilityType = :flexibilityType " +
            "AND pj.startTime = :predictionJobDateTime " +
            "AND p.id = :datacenterID " +
            "AND dt.type LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND m.property LIKE UPPER(CONCAT('%', :energyType,'%')) " +
            "AND m.property LIKE UPPER(CONCAT('%', :predictionType, '%')) " +
            "ORDER BY pv.timestamp")
    List<PredictedValue> findDataInIntervalForComponent(@Param("datacenterID") UUID datacenterID,
                                                        @Param("energyType") String energyType,
                                                        @Param("componentType") String componentType,
                                                        @Param("predictionType") String predictionType,
                                                        @Param("algorithmType") String algorithmType,
                                                        @Param("granularity") String granularity,
                                                        @Param("predictionJobDateTime") LocalDateTime predictionJobDateTime,
                                                        @Param("flexibilityType") String flexibilityType);

    @Query(value = "SELECT pv " +
            "FROM PredictedValue pv " +
            "JOIN FETCH pv.predictionJobId  pj " +
            "JOIN FETCH pv.measurementId  m " +
            "JOIN FETCH pv.deviceId d " +
            "JOIN FETCH d.deviceTypeId dt " +
            "WHERE pj.granularity = :granularity " +
            "AND pj.algorithmType = :algorithmType " +
            "AND pj.flexibilityType = :flexibilityType " +
            "AND pj.startTime = :predictionJobDateTime " +
            "AND dt.type LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND m.property LIKE UPPER(CONCAT('%', :energyType,'%')) " +
            "ORDER BY pv.timestamp")
    List<PredictedValue> findDataInIntervalForComponentRenewable(@Param("energyType") String energyType,
                                                        @Param("componentType") String componentType,
                                                        @Param("algorithmType") String algorithmType,
                                                        @Param("granularity") String granularity,
                                                        @Param("predictionJobDateTime") LocalDateTime predictionJobDateTime,
                                                        @Param("flexibilityType") String flexibilityType);

    // TODO: METHOD NOT WORKING! ONLY COPY-PASTED SO IT WOULDN'T THROW AN ERROR AT COMPILE TIME
    @Query(value = "SELECT m " +
            "FROM PredictedValue m " +
            "INNER JOIN FETCH m.measurementId " +
            "INNER JOIN FETCH m.deviceId " +
            "WHERE m.deviceId.parent = :datacenterId " +
            "AND m.deviceId.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND m.measurementId.property LIKE UPPER(CONCAT('%',:energyType,'%')) " +
            "AND (m.timestamp >= :startTime AND m.timestamp < :endTime) " +
            "AND :serverID = :serverID AND :componentName = :componentName " +
            "ORDER BY m.timestamp")
    List<PredictedValue> findDataInIntervalForComponentServer(@Param("datacenterId") UUID dataCenterID,
                                                              @Param("serverID") UUID serverID,
                                                              @Param("startTime") LocalDateTime startTime,
                                                              @Param("endTime") LocalDateTime endTime,
                                                              @Param("energyType") String energyType,
                                                              @Param("componentName") String componentName);

    @Query(value = "SELECT m " +
            "FROM PredictedValue m " +
            "INNER JOIN FETCH m.predictionJobId " +
            "INNER JOIN FETCH m.measurementId " +
            "INNER JOIN FETCH m.deviceId " +
            "WHERE m.predictionJobId.granularity = :granularity " +
            "AND m.timestamp >= :afterDate " +
            "AND m.timestamp < :endDate " +
            "AND m.predictionJobId.algorithmType = :algorithmType " +
            "AND m.predictionJobId.flexibilityType = :flexibilityType " +
            "AND m.predictionJobId.startTime = :predictionJobDateTime " +
            "AND m.deviceId.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND m.measurementId.property LIKE UPPER(CONCAT('%', :energyType,'%')) " +
            "ORDER BY m.timestamp")
    List<PredictedValue> getSpecificValues(@Param("afterDate") LocalDateTime afterDate,
                                           @Param("endDate") LocalDateTime endDate,
                                           @Param("energyType") String energyType,
                                           @Param("componentType") String componentType,
                                           @Param("algorithmType") String algorithmType,
                                           @Param("granularity") String granularity,
                                           @Param("predictionJobDateTime") LocalDateTime predictionJobDateTime,
                                           @Param("flexibilityType") String flexibilityType);



    @Query(value = "SELECT AVG(mv.value), YEAR(mv.timestamp), MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp),  FLOOR(MINUTE(mv.timestamp) /:minutesPerSample )  ,  dev.id as devId , mes.id as mesId  " +
            "FROM predicted_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "INNER JOIN prediction_job pj on pj.id = mv.prediction_job_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE ( dev.label like :label  OR p.label like :label) " +
            "AND mes.property LIKE :property " +
            "AND pj.granularity = :granularity " +
            "AND pj.start_time = :predictionJobDateTime " +
            "AND mv.timestamp >=  :startTime " +
            "AND mv.timestamp < :endTime " +
            "GROUP BY YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp),  FLOOR(MINUTE(mv.timestamp)/:minutesPerSample  ), dev.id, mes.id " +
            "ORDER BY  YEAR(mv.timestamp),MONTH(mv.timestamp), DAY(mv.timestamp), HOUR(mv.timestamp),  FLOOR(MINUTE(mv.timestamp)/:minutesPerSample ), dev.id, mes.id ",  nativeQuery = true)
    Object[][] findAveragedSignalsInInterval(@Param("label") String label,
                                                              @Param("granularity") String granularity,
                                                              @Param("startTime") LocalDateTime startTime,
                                                              @Param("endTime") LocalDateTime endTime,
                                                              @Param("property") String property,
                                                              @Param("predictionJobDateTime") LocalDateTime predictionJobDateTime,
                                                              @Param("minutesPerSample") double minutesPerSample);


    @Query(value = "SELECT MAX(mv.timestamp) " +
            "FROM predicted_value mv " +
            "INNER JOIN device dev on dev.id = mv.device_id " +
            "INNER JOIN measurement mes on mes.id = mv.measurement_id " +
            "INNER JOIN prediction_job pj on pj.id = mv.prediction_job_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE ( dev.label like :label  OR p.label like :label) " +
            "AND mes.property LIKE :property " +
            "AND pj.granularity = :granularity " +
            "AND mv.timestamp < :startTime ", nativeQuery = true)
    Object[][] findLatestMeasurement(@Param("label") String label,
                                         @Param("startTime") LocalDateTime startTime,
                                         @Param("granularity") String granularity,
                                         @Param("property") String property);

    @Query(value = "SELECT new ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO(pv.value, pv.timestamp) " +
            "FROM PredictedValue pv " +
            "INNER JOIN pv.deviceId dev " +
            "INNER JOIN pv.measurementId mes " +
            "INNER JOIN pv.predictionJobId pred " +
            "LEFT JOIN dev.parent p " +
            "WHERE " +
            "pred.granularity = :granularity  AND " +
            "( dev.label like :label OR p.label like :label ) AND "+
            "mes.property LIKE :measurementName AND " +
            "pv.timestamp >= :startTime AND " +
            "pv.timestamp < :endTime " +
            "ORDER BY pv.timestamp")
    List<EnergySampleDTO> findEstimatedProfileValues(@Param("label") String label,
                                                     @Param("granularity") String granularity,
                                                     @Param("measurementName") String measurementName,
                                                     @Param("startTime") LocalDateTime startTime,
                                                     @Param("endTime") LocalDateTime endTime);

    @Query(value = "SELECT AVG(pv.value), YEAR(pv.timestamp), MONTH(pv.timestamp), DAY(pv.timestamp), HOUR(pv.timestamp), MINUTE(pv.timestamp), dev.id as devId , mes.id as mesId " +
            "FROM predicted_value pv " +
            "INNER JOIN device dev on dev.id = pv.device_id " +
            "INNER JOIN measurement mes on mes.id = pv.measurement_id " +
            "INNER JOIN prediction_job pj on pj.id = pv.prediction_job_id " +
            "LEFT JOIN device p on p.id = dev.parent " +
            "WHERE p.id = :datacenterId " +
            "AND dev.label LIKE UPPER(CONCAT('%',:componentType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%',:energyType,'%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :predictionType, '%')) " +
            "AND mes.property LIKE UPPER(CONCAT('%', :measurementType, '%')) " +
            "AND pj.granularity = :predictionGranularity " +
            "AND pj.start_time = :predictionJobDateTime " +
            "AND pv.timestamp >= :startTime " +
            "AND  pv.timestamp < :endTime " +
            "GROUP BY YEAR(pv.timestamp),MONTH(pv.timestamp), DAY(pv.timestamp), HOUR(pv.timestamp), MINUTE(pv.timestamp), dev.id, mes.id " +
            "ORDER BY YEAR(pv.timestamp),MONTH(pv.timestamp), DAY(pv.timestamp), HOUR(pv.timestamp), MINUTE(pv.timestamp), dev.id, mes.id ",  nativeQuery = true)
    Object[][] findEstimatedProfileValuesForComponent(@Param("datacenterId") UUID datacenterId,
                                          @Param("startTime") LocalDateTime startTime,
                                          @Param("endTime") LocalDateTime endTime,
                                          @Param("energyType") String energyType,
                                          @Param("predictionGranularity") String predictionGranularity,
                                          @Param("componentType") String componentType,
                                          @Param("measurementType") String measurementType,
                                          @Param("predictionType") String predictionType,
                                          @Param("predictionJobDateTime") LocalDateTime predictionJobDateTime);

}
