package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.UserDTO;
import ro.tuc.dsrl.dbapi.model.User;
import ro.tuc.dsrl.dbapi.service.basic.UserService;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;


    @Autowired
    public UserController( UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/login")
    public UserDTO findUser(@RequestBody User user){
       return userService.findUser(user);
    }

    @PostMapping(value = "/add_user")
    public User addUser(@RequestBody User user){
        return userService.saveUser(user);
    }


}
