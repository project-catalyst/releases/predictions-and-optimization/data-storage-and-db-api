package ro.tuc.dsrl.dbapi.service.uisupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.FlexibilityStrategyDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.catalyst.model.utils.EnergyArraysUtility;
import ro.tuc.dsrl.dbapi.model.*;
import ro.tuc.dsrl.dbapi.model.builders.FlexibilityStrategyBuilder;
import ro.tuc.dsrl.dbapi.model.builders.OptimizationPageBuilder;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedActionValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.resources.CopFactors;
import ro.tuc.dsrl.dbapi.model.dto.resources.LossFactors;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.OptimizationActionsValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.OptimizationPageDTO;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.OptimizationRefrenceValuesDTO;
import ro.tuc.dsrl.dbapi.repository.*;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.OptimizationActionService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.HeatProfileFormatter;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.ProfileFormatter;
import ro.tuc.dsrl.dbapi.service.validators.OptimizationActionServiceValidator;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class OptimizationPageService {

    private static final Log LOGGER = LogFactory.getLog(OptimizationPageService.class);

    private final PredictedValueRepository predictedValueRepository;
    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    private final DeviceService deviceService;
    private final PredictionJobRepository predictionJobRepository;
    private final OptimizationActionService optimizationActionService;
    private final OptimizationPlanRepository optimizationPlanRepository;

    @Autowired
    public OptimizationPageService(PredictedValueRepository predictedValueRepository,
                                   EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService,
                                   DeviceService deviceService, PredictionJobRepository predictionJobRepository,
                                   OptimizationActionService optimizationActionService,
                                   OptimizationPlanRepository optimizationPlanRepository) {
        this.predictedValueRepository = predictedValueRepository;
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
        this.deviceService = deviceService;
        this.predictionJobRepository = predictionJobRepository;
        this.optimizationActionService = optimizationActionService;
        this.optimizationPlanRepository = optimizationPlanRepository;
    }


    public OptimizationPageDTO getEnergyPlanDA(String dcName, LocalDateTime untilDate, double confidence) {
        PredictionGranularity predictionGranularity = PredictionGranularity.DAYAHEAD;
        LocalDateTime startDate = DateUtils.computeCurrentDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentDayEnd(untilDate);
        int hour = startDate.getHour();

        ProfileFormatter optActionsFormatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.OPTIMIZED_ENERGY_ACTIONS);
        OptimizationActionsValuesDTO optActionsEnergyValues = getOptimizationActions(dcName, confidence, optActionsFormatter, predictionGranularity, startDate, endDate);

        //============== DELAY TOLERABLE  DA ====================
        List<AggregatedActionValuesDTO> valuesActionsDTDA = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.SHIFT_DELAY_TOLERANT_WORKLOAD, confidence);
        List<Double> consumptionForecastDT = optActionsFormatter.populateArray(valuesActionsDTDA, hour, predictionGranularity);
        optActionsEnergyValues.setConsumptionDTValues(consumptionForecastDT);

        //============== REALTIME ENERGY  DA ====================
        ProfileFormatter actualProfileFormatter =  ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        List<Double> consumptionForecastRT = actualProfileFormatter.populateArray(values, hour, predictionGranularity);
        optActionsEnergyValues.setConsumptionRTValues(consumptionForecastRT);

        //============== COOLING  DA ====================
        valuesActionsDTDA = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.DYNAMIC_ADJUSTMENT_OF_COOLING_INTENSITY, confidence);
        List<Double> daci = optActionsFormatter.populateArray(valuesActionsDTDA, hour, predictionGranularity);
        optActionsEnergyValues.setCoolingValues(EnergyArraysUtility.revertCoolingValuesFromActions(daci,
                optActionsEnergyValues.getChargeTesValues(),
                optActionsEnergyValues.getDischargeTesValues()));


        OptimizationRefrenceValuesDTO refrenceValuesDTO = getEnergyOptimizationRefrenceValues(dcName, confidence, predictionGranularity, startDate, endDate);

        return OptimizationPageBuilder.toDTO(optActionsEnergyValues, refrenceValuesDTO, predictionGranularity, hour);
    }

    public OptimizationPageDTO getRenPlanDA(String dcName, LocalDateTime untilDate, double confidence) {
        PredictionGranularity predictionGranularity = PredictionGranularity.DAYAHEAD;
        LocalDateTime startDate = DateUtils.computeCurrentDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentDayEnd(untilDate);

        OptimizationPageDTO optPageDTO = getEnergyPlanDA(dcName, untilDate, confidence);
        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE);
        List<Double> renewable = formatter.populateArray(values, startDate.getHour(), predictionGranularity);

        optPageDTO.setRenewableEnergyValues(renewable);
        return optPageDTO;
    }


    public OptimizationPageDTO getThermalPlanDA(String dcName, LocalDateTime untilDate, double confidence) {
        PredictionGranularity predictionGranularity = PredictionGranularity.DAYAHEAD;
        LocalDateTime startDate = DateUtils.computeCurrentDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentDayEnd(untilDate);
        int hour = startDate.getHour();


        ProfileFormatter optActionsHeatFormatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.OPTIMIZED_HEAT_ACTIONS);
        OptimizationActionsValuesDTO optActionsThermalValues = getOptimizationActions(dcName, confidence, optActionsHeatFormatter, predictionGranularity, startDate, endDate);

        //============== DELAY TOLERABLE DA ===================
        List<AggregatedActionValuesDTO>    heatActionValues = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.SHIFT_DELAY_TOLERANT_WORKLOAD, confidence);
        List<Double> consumptionForecastDT = optActionsHeatFormatter.populateArray(heatActionValues, hour, predictionGranularity);
        optActionsThermalValues.setConsumptionDTValues(consumptionForecastDT);

        //============== COOLING DA Thermal ====================
        heatActionValues = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.DYNAMIC_ADJUSTMENT_OF_COOLING_INTENSITY, confidence);
        List<Double> daci = optActionsHeatFormatter.populateArray(heatActionValues, hour, predictionGranularity);
        optActionsThermalValues.setCoolingValues(daci);

        //============== REAL TIME THERMAL ===================
        CopFactors copFactors = deviceService.getCopFactors(dcName, startDate);
        HeatProfileFormatter heatProfileFormatter = (HeatProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.HEAT_PROFILE);
        heatProfileFormatter.setCopFactors(copFactors);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        List<Double> consumptionForecastRT = heatProfileFormatter.populateArray(values, hour, predictionGranularity);
        optActionsThermalValues.setConsumptionRTValues(consumptionForecastRT);

        OptimizationRefrenceValuesDTO refrenceValuesDTO = getThermalOptimizationRefrenceValues(dcName, confidence, predictionGranularity, startDate, endDate);


        return OptimizationPageBuilder.toDTO(optActionsThermalValues, refrenceValuesDTO, predictionGranularity, hour);
    }

    public OptimizationPageDTO getPriceDrivenPlanDA(String dcName, LocalDateTime untilDate, double confidence) {
        PredictionGranularity predictionGranularity = PredictionGranularity.DAYAHEAD;
        LocalDateTime startDate = DateUtils.computeCurrentDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentDayEnd(untilDate);

        OptimizationPageDTO optPageDTO = getEnergyPlanDA(dcName, untilDate, confidence);
        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.DC_E_PRICE);
        List<Double> refrencePrices = formatter.populateArray(values, startDate.getHour(), predictionGranularity);

        optPageDTO.setRefrencePrices(refrencePrices);
        return optPageDTO;
    }

    public OptimizationPageDTO getEnergyPlanID(String dcName, LocalDateTime untilDate, double confidence) {

        PredictionGranularity predictionGranularity = PredictionGranularity.INTRADAY;
        LocalDateTime startDate = DateUtils.computeCurrentIntraDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentIntraDayEnd(untilDate);
        int hour = startDate.getHour();

        ProfileFormatter optEnergyActionsFormatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.OPTIMIZED_ENERGY_ACTIONS);
        OptimizationActionsValuesDTO optActionsEnergyValues  = getOptimizationActions(dcName, confidence, optEnergyActionsFormatter, predictionGranularity, startDate, endDate);

        //============== DELAY TOLERABLE ====================
        List<AggregatedActionValuesDTO> valuesIDEnergyActions = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.SHIFT_DELAY_TOLERANT_WORKLOAD, confidence);
        List<Double> consumptionForecastDT = optEnergyActionsFormatter.populateIntraDayArrayFromDayAyead(valuesIDEnergyActions);
        optActionsEnergyValues.setConsumptionDTValues(consumptionForecastDT);

        //============== REALTIME ENERGY ID ====================
        ProfileFormatter actualProfileFormatter =  ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        List<Double> consumptionForecastRT = actualProfileFormatter.populateArray(values, hour, predictionGranularity);
        optActionsEnergyValues.setConsumptionRTValues(consumptionForecastRT);

        //============== COOLING  ID ====================
        valuesIDEnergyActions = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.DYNAMIC_ADJUSTMENT_OF_COOLING_INTENSITY, confidence);
        List<Double> daci = optEnergyActionsFormatter.populateArray(valuesIDEnergyActions, hour, predictionGranularity);
        optActionsEnergyValues.setCoolingValues(EnergyArraysUtility.revertCoolingValuesFromActions(daci,
                optActionsEnergyValues.getChargeTesValues(),
                optActionsEnergyValues.getDischargeTesValues()));

        OptimizationRefrenceValuesDTO refrenceValuesDTO = getEnergyOptimizationRefrenceValues(dcName, confidence, predictionGranularity, startDate, endDate);

        return OptimizationPageBuilder.toDTO(optActionsEnergyValues, refrenceValuesDTO, predictionGranularity, hour);
    }


    public OptimizationPageDTO getThermalPlanID(String dcName, LocalDateTime untilDate, double confidence) {

        PredictionGranularity predictionGranularity = PredictionGranularity.INTRADAY;
        LocalDateTime startDate = DateUtils.computeCurrentIntraDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentIntraDayEnd(untilDate);
        int hour = startDate.getHour();

        ProfileFormatter optActionsHeatFormatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.OPTIMIZED_HEAT_ACTIONS);
        OptimizationActionsValuesDTO optActionsThermalValues = getOptimizationActions(dcName, confidence, optActionsHeatFormatter, predictionGranularity, startDate, endDate);

        //============== DELAY TOLERABLE ====================
        List<AggregatedActionValuesDTO> actionValues = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.SHIFT_DELAY_TOLERANT_WORKLOAD, confidence);
        List<Double> consumptionForecastDT = optActionsHeatFormatter.populateIntraDayArrayFromDayAyead(actionValues);
        optActionsThermalValues.setConsumptionDTValues(consumptionForecastDT);

        //============== COOLING DA Thermal ====================
        actionValues = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.DYNAMIC_ADJUSTMENT_OF_COOLING_INTENSITY, confidence);
        List<Double> daci = optActionsHeatFormatter.populateArray(actionValues, hour, predictionGranularity);
        optActionsThermalValues.setCoolingValues(daci);

        //============== REAL TIME THERMAL ===================
        CopFactors copFactors = deviceService.getCopFactors(dcName, startDate);
        HeatProfileFormatter heatProfileFormatter = (HeatProfileFormatter) ProfileFormatter.getInstance(ProfileFormatter.ProfileType.HEAT_PROFILE);
        heatProfileFormatter.setCopFactors(copFactors);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
        List<Double> consumptionForecastRT = heatProfileFormatter.populateArray(values, hour, predictionGranularity);
        optActionsThermalValues.setConsumptionRTValues(consumptionForecastRT);

        OptimizationRefrenceValuesDTO refrenceValuesDTO = getThermalOptimizationRefrenceValues(dcName, confidence, predictionGranularity, startDate, endDate);


        return OptimizationPageBuilder.toDTO(optActionsThermalValues, refrenceValuesDTO, predictionGranularity, hour);
    }

    public OptimizationPageDTO getPriceDrivenPlanID(String dcName, LocalDateTime untilDate, double confidence) {
        PredictionGranularity predictionGranularity = PredictionGranularity.INTRADAY;
        LocalDateTime startDate = DateUtils.computeCurrentIntraDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentIntraDayEnd(untilDate);

        OptimizationPageDTO optPageDTO = getEnergyPlanID(dcName, untilDate, confidence);

        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.DC_E_PRICE);
        List<Double> refrencePrices = formatter.populateArray(values,startDate.getHour(), predictionGranularity);

        optPageDTO.setRefrencePrices(refrencePrices);
        return optPageDTO;
    }



    public OptimizationPageDTO getRenPlanID(String dcName, LocalDateTime untilDate, double confidence) {
        PredictionGranularity predictionGranularity = PredictionGranularity.INTRADAY;
        LocalDateTime startDate = DateUtils.computeCurrentIntraDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentIntraDayEnd(untilDate);

        OptimizationPageDTO optPageDTO = getEnergyPlanID(dcName, untilDate, confidence);

        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE);
        List<Double> renewable = formatter.populateArray(values,startDate.getHour(), predictionGranularity);

        optPageDTO.setRenewableEnergyValues(renewable);
        return optPageDTO;
    }



    public FlexibilityStrategyDTO getPlanStrategies(LocalDateTime current, Double confidenceLevel, String dataCenterName, PredictionGranularity granularity) {
        LocalDateTime startTime = DateUtils.computeCurrentDayStart(current);
        DeviceDTO deviceDTO = deviceService.getDeviceByLabel(dataCenterName);
        List<OptimizationPlan> plans = optimizationPlanRepository.findByStartDateAndTimeframe(startTime, granularity.getTimeframeLabel(), deviceDTO.getId());
        OptimizationPlan plan = null;
        if (plans != null && plans.size() > 1) {
            LOGGER.error("There are multiple plans saved for: " + granularity.getTimeframeLabel() + " " + startTime + " " + dataCenterName);
            LOGGER.info("Only first plan is considered. ");
            plan = plans.get(0);
        } else {
            if (plans != null && plans.size() == 1) {
                plan = plans.get(0);
            } else {
                return FlexibilityStrategyBuilder.getDefaultStrategies(dataCenterName);
            }
        }
        return FlexibilityStrategyBuilder.generateDTOFromPlan(plan, dataCenterName);
    }

    public List<OptimizationActionDTO> getActions(LocalDateTime current, Double confidenceLevel, String dataCenterName, PredictionGranularity granularity) {

        OptimizationActionServiceValidator.validateCurrentTimeAndConfidenceLevelThrowErrors(current, confidenceLevel);
        LocalDateTime startDate = current;
        LocalDateTime endDate = current;
        if (granularity.equals(PredictionGranularity.DAYAHEAD)) {
            startDate = DateUtils.computeCurrentDayStart(current);
            endDate = DateUtils.computeCurrentDayEnd(current);
        }
        if (granularity.equals(PredictionGranularity.INTRADAY)) {
            startDate = DateUtils.computeCurrentIntraDayStart(current);
            endDate = DateUtils.computeCurrentIntraDayEnd(current);
        }
        List<OptimizationActionDTO> actionsInInterval = optimizationActionService.getAllActionsInInterval(startDate, endDate, confidenceLevel, granularity.getTimeframe(), dataCenterName);

        return this.markCurrentActions(current, actionsInInterval);
    }

    private OptimizationRefrenceValuesDTO getThermalOptimizationRefrenceValues(String dcName, double confidence, PredictionGranularity predictionGranularity, LocalDateTime startDate, LocalDateTime endDate) {
        int hour = startDate.getHour();
        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.DATA_CENTER_HEAT_BASELINE);
        List<Double> predictedDemand = formatter.populateArray(values, hour, predictionGranularity);

        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.DC_T_PRICE);
        List<Double> refrencePrices = formatter.populateArray(values, hour, predictionGranularity);

        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.OPTIMIZED_THERMAL);
        List<Double> optProfile = formatter.populateArray(values, hour, predictionGranularity);


        OptimizationRefrenceValuesDTO optimizationRefrenceValues = new OptimizationRefrenceValuesDTO();

        optimizationRefrenceValues.setProvidedEnergyValues(optProfile);
        optimizationRefrenceValues.setOptimizedProfileValues(optProfile);
        optimizationRefrenceValues.setPredictedDemandValues(predictedDemand);
        optimizationRefrenceValues.setRefrencePrices(refrencePrices);
        optimizationRefrenceValues.setRequestedProfileValues(new ArrayList<Double>(Collections.nCopies(predictionGranularity.getNoOutputs(), 0.0)));
        return optimizationRefrenceValues;
    }

    private OptimizationRefrenceValuesDTO getEnergyOptimizationRefrenceValues(String dcName, double confidence, PredictionGranularity predictionGranularity, LocalDateTime startDate, LocalDateTime endDate) {

        int hour = startDate.getHour();
        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.DATA_CENTER_BASELINE);
        List<Double> predictedDemand = formatter.populateArray(values, hour, predictionGranularity);

        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.DR_SIGNAL);
        List<Double> requestedProfile = formatter.populateArray(values, hour, predictionGranularity);

        values = energyHistoricalPredictedValuesService.getEstimatedValuesProfile(dcName,
                startDate,
                endDate,
                predictionGranularity,
                MeasurementType.OPTIMIZED_DC);
        List<Double> optProfile = formatter.populateArray(values, hour, predictionGranularity);


        OptimizationRefrenceValuesDTO optimizationRefrenceValues = new OptimizationRefrenceValuesDTO();

        optimizationRefrenceValues.setProvidedEnergyValues(optProfile);
        optimizationRefrenceValues.setOptimizedProfileValues(optProfile);
        optimizationRefrenceValues.setPredictedDemandValues(predictedDemand);
        optimizationRefrenceValues.setRequestedProfileValues(requestedProfile);
        optimizationRefrenceValues.setRefrencePrices(new ArrayList<Double>(Collections.nCopies(predictionGranularity.getNoOutputs(), 0.0)));
        return optimizationRefrenceValues;
    }

    private OptimizationActionsValuesDTO getOptimizationActions(String dcName, double confidence, ProfileFormatter<List<AggregatedActionValuesDTO>> formatter, PredictionGranularity predictionGranularity, LocalDateTime startDate, LocalDateTime endDate) {
        int hour = startDate.getHour();
        //============== BATTERIES ====================
        LossFactors batteryLF = deviceService.getBatteryLossFactors(dcName, startDate);
        List<AggregatedActionValuesDTO> values = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.CHARGE_BATTERY, confidence);
        List<Double> chargeBattery = formatter.populateArray(values, hour, predictionGranularity);
        chargeBattery = EnergyArraysUtility.computeLostFactorBatteryCharge(chargeBattery, batteryLF.getChargeLossFactor());

        values = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.DISCHARGE_BATTERY, confidence);
        List<Double> dischargeBattery = formatter.populateArray(values, hour, predictionGranularity);
        dischargeBattery = EnergyArraysUtility.computeLostFactorBatteryDischarge(dischargeBattery, batteryLF.getDischargeLossFactor());

        //============== TES ====================
        LossFactors tesLF = deviceService.getTESLossFactors(dcName, startDate);
        values = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.CHARGE_TES, confidence);
        List<Double> chargeTES = formatter.populateArray(values, hour, predictionGranularity);
        chargeTES = EnergyArraysUtility.computeLostFactorTesCharge(chargeTES, tesLF.getChargeLossFactor());

        values = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.DISCHARGE_TES, confidence);
        List<Double> dischargeTES = formatter.populateArray(values, hour, predictionGranularity);
        dischargeTES = EnergyArraysUtility.computeLostFactorTesDischarge(dischargeTES, tesLF.getDischargeLossFactor());

        //============== RELLOCATE HOST ====================
        values = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.HOST_WORKLOAD, confidence);
        List<Double> hostRealoc = formatter.populateArray(values, hour, predictionGranularity);

        //============== RELLOCATE LOAD====================
        values = optimizationActionService.getAveragedActionValues(startDate, endDate,
                predictionGranularity, ActionTypeName.RELOCATE_WORKLOAD, confidence);
        List<Double> relocate = formatter.populateArray(values, hour, predictionGranularity);

        OptimizationActionsValuesDTO actions = new OptimizationActionsValuesDTO();
        actions.setRelocateValues(relocate);
        actions.setHostValues(hostRealoc);
        actions.setChargeBatteryValues(chargeBattery);
        actions.setDischargeBatteryValues(dischargeBattery);
        actions.setChargeTesValues(chargeTES);
        actions.setDischargeTesValues(dischargeTES);
        return actions;
    }


    public List<OptimizationActionDTO> markCurrentActions(LocalDateTime current, List<OptimizationActionDTO> intervalActions) {
        for (OptimizationActionDTO dto : intervalActions) {
            LocalDateTime start = DateUtils.millisToLocalDateTime(dto.getStartTime());
            if (start.getHour() == current.getHour()) {
                dto.setActive(true);
            }
        }

        intervalActions.sort((h1, h2) -> (int) (h1.getStartTime() - h2.getStartTime()));
        return intervalActions;
    }


}
