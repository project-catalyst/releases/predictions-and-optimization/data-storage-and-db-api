//package ro.tuc.dsrl.dbapi.service.validators;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import ro.tuc.dsrl.catalyst.model.dto.ActionTypeDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ActionTypeValidator {
//
//    private static final Log LOGGER = LogFactory.getLog(ActionTypeValidator.class);
//
//    private ActionTypeValidator() {
//
//    }
//
//    public static void validateInsert(ActionTypeDTO actionTypeDTO) {
//
//        List<String> errors = new ArrayList<>();
//        if (actionTypeDTO == null) {
//            errors.add("Action Type DTO is null");
//            throw new IncorrectParameterException(ActionTypeValidator.class.getSimpleName(), errors);
//        }
//
//        if (actionTypeDTO.getTypeOfActivity() == null || actionTypeDTO.getTypeOfActivity().equals("")) {
//            errors.add("Action Type type is null or empty");
//        }
//
//        if (!errors.isEmpty()) {
//            LOGGER.error(errors);
//            throw new IncorrectParameterException(ActionTypeValidator.class.getSimpleName(), errors);
//        }
//    }
//}
