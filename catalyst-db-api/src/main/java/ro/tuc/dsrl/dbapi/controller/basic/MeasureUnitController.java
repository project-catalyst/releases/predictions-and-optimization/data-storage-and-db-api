package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasureUnitDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.MeasureUnitService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/measure-unit")
public class MeasureUnitController {

    private final MeasureUnitService measureUnitService;

    @Autowired
    public MeasureUnitController(MeasureUnitService measureUnitService) {
        this.measureUnitService = measureUnitService;
    }

    @GetMapping(value = "/byId/{measureUnitId}")
    public MeasureUnitDTO findById(@PathVariable("measureUnitId") String measureUnitId) {

        UUIDPreconditions.validate(measureUnitId);

        UUID measureUnitUUID = UUID.fromString(measureUnitId);
        return measureUnitService.findById(measureUnitUUID);
    }

    @GetMapping(value = "/all")
    public List<MeasureUnitDTO> findAll() {

        return measureUnitService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertMeasureUnitDTO(@RequestBody MeasureUnitDTO measureUnitDTO) {

        return measureUnitService.insert(measureUnitDTO);
    }
}
