package ro.tuc.dsrl.dbapi.model.dto.resources;

import java.time.LocalDateTime;
import java.util.UUID;

public class DeviceStateDTO {

    private UUID deviceId;
    private String deviceLabel;
    private String property;
    private Double value;
    private LocalDateTime date;

    public DeviceStateDTO() {
    }

    public DeviceStateDTO(String deviceID, String property,  LocalDateTime date,Double value) {
        this.deviceLabel = deviceID;
        this.property = property;
        this.value = value;
        this.date = date;
    }

    public DeviceStateDTO(UUID deviceId, String deviceLabel, String property,  LocalDateTime date,Double value) {
        this.deviceId = deviceId;
        this.deviceLabel = deviceLabel;
        this.property = property;
        this.value = value;
        this.date = date;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceLabel() {
        return deviceLabel;
    }

    public void setDeviceLabel(String deviceLabel) {
        this.deviceLabel = deviceLabel;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
