package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasurementDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.*;
import ro.tuc.dsrl.dbapi.model.builders.MeasurementBuilder;
import ro.tuc.dsrl.dbapi.repository.*;
import ro.tuc.dsrl.dbapi.service.validators.MeasurementValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MeasurementService {

    private final MeasurementRepository measurementRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final MeasureUnitRepository measureUnitRepository;
    private final PropertySourceRepository propertySourceRepository;
    private final ValueTypeRepository valueTypeRepository;
    private final DeviceRepository deviceRepository;

    public MeasurementService(MeasurementRepository measurementRepository, DeviceTypeRepository deviceTypeRepository,
                              MeasureUnitRepository measureUnitRepository, PropertySourceRepository propertySourceRepository,
                              ValueTypeRepository valueTypeRepository, DeviceRepository deviceRepository) {
        this.measurementRepository = measurementRepository;
        this.deviceTypeRepository = deviceTypeRepository;
        this.measureUnitRepository = measureUnitRepository;
        this.propertySourceRepository = propertySourceRepository;
        this.valueTypeRepository = valueTypeRepository;
        this.deviceRepository = deviceRepository;
    }

    public UUID getMeasurementByProperty(String property) {
        UUID measurementId = measurementRepository.findByProperty(property);
        if (measurementId == null) {
            throw new ResourceNotFoundException("Measurement type " + property);
        }
        return measurementId;

    }

    public List<MeasurementDTO> findAll() {

        List<Measurement> measurements = measurementRepository.findAll();

        return measurements.stream()
                .map(m -> MeasurementBuilder.generateDTOFromEntity(
                        m,
                        m.getDeviceTypeId(),
                        m.getMeasureUnitId(),
                        m.getPropertySourceId(),
                        m.getValueTypeId()))
                .collect(Collectors.toList());
    }

    public UUID insert(MeasurementDTO measurementDTO) {
        MeasurementValidator.validateInsert(measurementDTO);

        List<String> errors = new ArrayList<>();
        DeviceType deviceType = deviceTypeRepository.findByUUId(measurementDTO.getDeviceTypeId());
        if (deviceType == null) {
            errors.add("Device type was not found in database.");
        }

        MeasureUnit measureUnit = measureUnitRepository.findByUUID(measurementDTO.getMeasureUnitId());
        if (measureUnit == null) {
            errors.add("Measure unit was not found in database.");
        }

        PropertySource propertySource = propertySourceRepository.findByUUId(measurementDTO.getPropertySourceId());
        if (propertySource == null) {
            errors.add("Property source was not found in database.");
        }

        ValueType valueType = valueTypeRepository.findByUUId(measurementDTO.getValueTypeId());
        if (valueType == null) {
            errors.add("Value type was not found in database.");
        }

        if (!errors.isEmpty()) {
            throw new EntityValidationException(Measurement.class.getSimpleName(), errors);
        }

        return measurementRepository
                .save(MeasurementBuilder.generateEntityFromDTO(
                        measurementDTO,
                        deviceType,
                        measureUnit,
                        propertySource,
                        valueType))
                .getId();
    }

    public MeasurementDTO findById(UUID measurementUUID) {

        Measurement measurement = measurementRepository.findByUUID(measurementUUID);
        if (measurement == null) {
            throw new ResourceNotFoundException("Measurement with ID " + measurementUUID);
        }

        return MeasurementBuilder.generateDTOFromEntity(
                measurement,
                measurement.getDeviceTypeId(),
                measurement.getMeasureUnitId(),
                measurement.getPropertySourceId(),
                measurement.getValueTypeId());
    }
}
