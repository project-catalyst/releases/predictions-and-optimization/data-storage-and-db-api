//package ro.tuc.dsrl.dbapi.repository;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//import ro.tuc.dsrl.dbapi.model.ActionInstance;
//
//import java.util.UUID;
//
//@Repository
//public interface ActionInstanceRepository extends JpaRepository<ActionInstance, UUID> {
//
//    @Query(value = "SELECT a FROM ActionInstance a where a.id = :id")
//    ActionInstance findByUUID(@Param("id") UUID id);
//}
