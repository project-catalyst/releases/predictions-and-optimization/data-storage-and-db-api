//package ro.tuc.dsrl.dbapi.controller.optimization;
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ro.tuc.dsrl.catalyst.model.dto.ui.optimization.ExecutionNotOptimizedDTO;
//import ro.tuc.dsrl.catalyst.model.dto.ui.optimization.ExecutionOptimizedDTO;
//import ro.tuc.dsrl.catalyst.model.enums.Datacenter;
//import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
//import ro.tuc.dsrl.catalyst.model.enums.Scenario;
//import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
//import ro.tuc.dsrl.dbapi.controller.preconditions.OptimizationPreconditions;
//import ro.tuc.dsrl.dbapi.service.basic.TotalExecutionResultsService;
//import ro.tuc.dsrl.dbapi.service.basic.TotalPowerService;
//
//import java.time.LocalDateTime;
//import java.util.List;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/as")
//public class ExecutionController {
//
//    private final TotalExecutionResultsService totalExecutionResultsService;
//
//    private final TotalPowerService totalPowerService;
//
//    @Autowired
//    public ExecutionController(TotalExecutionResultsService totalExecutionResultsService, TotalPowerService totalPowerService) {
//        this.totalExecutionResultsService = totalExecutionResultsService;
//        this.totalPowerService = totalPowerService;
//    }
//
//    @GetMapping(value = "/optimized/{granularity}/{scenarioId}/{hour},{min}")
//    public ExecutionOptimizedDTO getOptimizedEnergyValues(@PathVariable("granularity") String granularity,
//                                                          @PathVariable("scenarioId") int scenarioId,
//                                                          @PathVariable("hour") int hour,
//                                                          @PathVariable("min") int min) {
//
//        List<String> errors = OptimizationPreconditions.validateScenarioId(scenarioId, granularity);
//        OptimizationPreconditions.throwIncorrectParameterException(errors);
//
//        Scenario currentScenario = Scenario.getByIndex(scenarioId);
//
//        if (currentScenario == null) {
//            throw new ResourceNotFoundException("Scenario not found with scenarion id: " + scenarioId);
//        }
//
//        LocalDateTime currentTime = currentScenario.getTime();
//        currentTime = currentTime.withHour(hour);
//        currentTime = currentTime.withMinute(min);
//
//        return totalExecutionResultsService.getOptimizedValues(currentTime, Datacenter.DATACENTER_NAME, PredictionGranularity.setGranularity(granularity));
//    }
//
//
//    @GetMapping(value = "/optimized/{scenarioId}/{hour},{min}")
//    public ExecutionOptimizedDTO getOptimizedEnergyValues(@PathVariable("scenarioId") int scenarioId,
//                                                          @PathVariable("hour") int hour,
//                                                          @PathVariable("min") int min) {
//
//        List<String> errors = OptimizationPreconditions.validateScenarioId(scenarioId);
//        OptimizationPreconditions.throwIncorrectParameterException(errors);
//
//        Scenario currentScenario = Scenario.getByIndex(scenarioId);
//
//        if (currentScenario == null) {
//            throw new ResourceNotFoundException("Scenario not found with scenarion id: " + scenarioId);
//        }
//
//        LocalDateTime currentTime = currentScenario.getTime();
//        currentTime = currentTime.withHour(hour);
//        currentTime = currentTime.withMinute(min);
//
//        return totalExecutionResultsService.getOptimizedValuesOld(currentTime, Datacenter.DATACENTER_NAME, PredictionGranularity.REAL_TIME);
//    }
//
//
//    @GetMapping(value = "/not-optimized/{scenarioId}/{hour},{min}")
//    public ExecutionNotOptimizedDTO getNotOptimizedEnergyValues(@PathVariable("scenarioId") int scenarioId,
//                                                                @PathVariable("hour") int hour, @PathVariable("min") int min) {
//
//        List<String> errors = OptimizationPreconditions.validateScenarioId(scenarioId);
//        OptimizationPreconditions.throwIncorrectParameterException(errors);
//
//        Scenario currentScenario = Scenario.getByIndex(scenarioId);
//
//        if (currentScenario == null) {
//            throw new ResourceNotFoundException("Scenario not found with scenarion id: " + scenarioId);
//        }
//
//        LocalDateTime currentTime = currentScenario.getTime();
//        currentTime = currentTime.withHour(hour);
//        currentTime = currentTime.withMinute(min);
//
//        return totalPowerService.getNonOptimizedValues(currentTime);
//    }
//}
