package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;
import ro.tuc.dsrl.dbapi.model.PredictedValue;
import ro.tuc.dsrl.dbapi.model.PredictionJob;

public class PredictedValueBuilder {

    private PredictedValueBuilder() {

    }

    public static PredictedValueDTO generateDTOFromEntity(PredictedValue predictedValue,
                                                          Measurement measurement,
                                                          Device device,
                                                          PredictionJob predictionJob) {
        return new PredictedValueDTO(
                predictedValue.getId(),
                predictedValue.getTimestamp(),
                predictedValue.getValue(),
                measurement.getId(),
                device.getId(),
                predictionJob.getId()
        );
    }

    public static PredictedValue generateEntityFromDTO(PredictedValueDTO predictedValueDTO,
                                                       Measurement measurement,
                                                       Device device,
                                                       PredictionJob predictionJob) {
        return new PredictedValue(
                predictedValueDTO.getId(),
                predictedValueDTO.getTimestamp(),
                predictedValueDTO.getValue(),
                measurement,
                device,
                predictionJob
        );
    }
}
