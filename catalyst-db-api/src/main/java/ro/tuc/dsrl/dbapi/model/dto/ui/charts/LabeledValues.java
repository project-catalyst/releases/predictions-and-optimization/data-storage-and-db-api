package ro.tuc.dsrl.dbapi.model.dto.ui.charts;

import java.io.Serializable;
import java.util.List;

public class LabeledValues implements Serializable {

    private String label;
    private List<Double> values;

    public LabeledValues() {
    }

    public LabeledValues(String label, List<Double> values) {
        this.label = label;
        this.values = values;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }

}
