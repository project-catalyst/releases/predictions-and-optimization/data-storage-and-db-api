//package ro.tuc.dsrl.dbapi.model;
//
//import org.hibernate.annotations.GenericGenerator;
//
//import javax.persistence.*;
//import javax.validation.constraints.NotNull;
//import java.util.UUID;
//
//@Entity
//@Table(name = "action_type")
//public class ActionType {
//
//    @Id
//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
//    @Column(name = "id", columnDefinition = "BINARY(16)")
//    private UUID id;
//
//    @Column(name = "type")
//    @NotNull
//    private String type;
//
//    public ActionType() {
//
//    }
//
//    public ActionType(@NotNull UUID id,
//                      @NotNull String type) {
//        this.type = type;
//        this.id = id;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//}
