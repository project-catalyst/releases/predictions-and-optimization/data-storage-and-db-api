//package ro.tuc.dsrl.dbapi.model;
//
//import org.hibernate.annotations.GenericGenerator;
//
//import javax.persistence.*;
//import javax.validation.constraints.NotNull;
//import java.time.LocalDateTime;
//import java.util.HashSet;
//import java.util.Set;
//import java.util.UUID;
//
//@Entity
//@Table(name = "action_instance")
//public class ActionInstance {
//
//    @Id
//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
//    @Column(name = "id", columnDefinition = "BINARY(16)")
//    private UUID id;
//
//    @Column(name = "label")
//    @NotNull
//    private String label;
//
//    @Column(name = "start_time")
//    @NotNull
//    private LocalDateTime startTime;
//
//    @Column(name = "end_time")
//    @NotNull
//    private LocalDateTime endTime;
//
//    @ManyToOne
//    @JoinColumn(name = "action_type_id")
//    @NotNull
//    private ActionType actionType;
//
//    @ManyToOne
//    @JoinColumn(name = "optimization_plan_id")
//    @NotNull
//    private OptimizationPlan optimizationPlan;
//
//    @OneToMany(cascade = CascadeType.ALL,
//            fetch = FetchType.LAZY,
//            mappedBy = "actionInstanceId")
//    private Set<ActionValue> actionValues = new HashSet<>();
//
//    public ActionInstance() {
//
//    }
//
//    public ActionInstance(@NotNull String label,
//                          @NotNull LocalDateTime startTime,
//                          @NotNull LocalDateTime endTime,
//                          @NotNull ActionType actionType,
//                          @NotNull OptimizationPlan optimizationPlan) {
//        this.label = label;
//        this.startTime = startTime;
//        this.endTime = endTime;
//        this.actionType = actionType;
//        this.optimizationPlan = optimizationPlan;
//    }
//
//    public ActionInstance(@NotNull UUID id,
//                          @NotNull String label,
//                          @NotNull LocalDateTime startTime,
//                          @NotNull LocalDateTime endTime,
//                          @NotNull ActionType actionType,
//                          @NotNull OptimizationPlan optimizationPlan) {
//        this.id = id;
//        this.label = label;
//        this.startTime = startTime;
//        this.endTime = endTime;
//        this.actionType = actionType;
//        this.optimizationPlan = optimizationPlan;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public String getLabel() {
//        return label;
//    }
//
//    public LocalDateTime getStartTime() {
//        return startTime;
//    }
//
//    public LocalDateTime getEndTime() {
//        return endTime;
//    }
//
//    public ActionType getActionType() {
//        return actionType;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public void setLabel(String label) {
//        this.label = label;
//    }
//
//    public void setStartTime(LocalDateTime startTime) {
//        this.startTime = startTime;
//    }
//
//    public void setEndTime(LocalDateTime endTime) {
//        this.endTime = endTime;
//    }
//
//    public void setActionType(ActionType actionType) {
//        this.actionType = actionType;
//    }
//
//    public OptimizationPlan getOptimizationPlan() {
//        return optimizationPlan;
//    }
//
//    public void setOptimizationPlan(OptimizationPlan optimizationPlan) {
//        this.optimizationPlan = optimizationPlan;
//    }
//
//    public Set<ActionValue> getActionValues() {
//        return actionValues;
//    }
//
//    public void setActionValues(Set<ActionValue> actionValues) {
//        this.actionValues = actionValues;
//    }
//
//    public void addActionValues(ActionValue actionValue) {
//        this.actionValues.add(actionValue);
//    }
//}
