package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "measure_unit")
public class MeasureUnit {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "unit")
    @NotNull
    private String unit;

    public MeasureUnit() {

    }

    public MeasureUnit(@NotNull String unit) {
        this.unit = unit;
    }

    public MeasureUnit(@NotNull UUID id, @NotNull String unit) {
        this.id = id;
        this.unit = unit;
    }

    public UUID getId() {
        return id;
    }

    public String getUnit() {
        return unit;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
