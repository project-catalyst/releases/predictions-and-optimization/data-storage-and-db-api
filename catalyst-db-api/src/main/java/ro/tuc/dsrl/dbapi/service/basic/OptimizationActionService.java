package ro.tuc.dsrl.dbapi.service.basic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.OptimizationActionDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.OptimizationAction;
import ro.tuc.dsrl.dbapi.model.OptimizationPlan;
import ro.tuc.dsrl.dbapi.model.builders.AggregatedValuesBuilder;
import ro.tuc.dsrl.dbapi.model.builders.OptimizationActionBuilder;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedActionValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.ItLoadBalancerActions;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.OptimizationActionRepository;
import ro.tuc.dsrl.dbapi.repository.OptimizationPlanRepository;
import ro.tuc.dsrl.dbapi.service.validators.OptimizationActionServiceValidator;
import ro.tuc.dsrl.geyser.datamodel.actions.*;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizationPlansContainer;
import ro.tuc.dsrl.geyser.datamodel.other.OptimizerPlan;

import javax.swing.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OptimizationActionService {
    private static final Log LOGGER = LogFactory.getLog(OptimizationActionService.class);

    private final OptimizationPlanRepository optimizationPlanRepository;
    private final OptimizationActionRepository optimizationActionRepository;
    private final DeviceRepository deviceRepository;


    public OptimizationActionService(OptimizationPlanRepository optimizationPlanRepository,
                                     OptimizationActionRepository optimizationActionRepository,
                                     DeviceRepository deviceRepository) {
        this.optimizationPlanRepository = optimizationPlanRepository;
        this.optimizationActionRepository = optimizationActionRepository;
        this.deviceRepository = deviceRepository;
    }


    public List<OptimizationActionDTO> getCurrentActionsToBeExecuted(LocalDateTime currentDateTime,
                                                                     Double confidenceLevel,
                                                                     String dataCenterName) {
        //OptimizationActionServiceValidator.validateCurrentTimeAndConfidenceLevel(currentDateTime, confidenceLevel);

        List<OptimizationAction> currentActions = optimizationActionRepository.findCurrentActions(currentDateTime, confidenceLevel, dataCenterName);
        if (OptimizerConfig.ID_ACTIVE) {
            List<OptimizationAction> redundantActions = getCurrentRedundantActions(currentDateTime, confidenceLevel, dataCenterName);
            currentActions.removeAll(redundantActions);
        }
        return currentActions.stream()
                .map(OptimizationActionBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public ItLoadBalancerActions getCurrentSplitSDTWActions(LocalDateTime currentDateTime,
                                                                 Double confidenceLevel,
                                                                 String dataCenterName) {
        List<OptimizationAction> currentActions = optimizationActionRepository.findCurrentActions(currentDateTime, confidenceLevel, dataCenterName);

        currentDateTime = currentDateTime.withMinute(0).withSecond(0).withNano(0);
        LocalDateTime endDate = currentDateTime.plusHours(1);
        List<OptimizationAction> moveFromCurrentActions = optimizationActionRepository.findCurrentSDTWActionsMoveFromDate(currentDateTime, endDate, confidenceLevel, dataCenterName, ActionTypeName.SHIFT_DELAY_TOLERANT_WORKLOAD.getName());

        List<OptimizationActionDTO> moveFromCurrentActionsDTO = moveFromCurrentActions.stream()
                .map(OptimizationActionBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        List<OptimizationActionDTO> currentActionsDTO = currentActions.stream()
                .map(OptimizationActionBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        return new ItLoadBalancerActions(moveFromCurrentActionsDTO, currentActionsDTO);
    }

    public List<OptimizationActionDTO> getCurrentItLoadBalancerActions(LocalDateTime currentDateTime,
                                                                 Double confidenceLevel,
                                                                 String dataCenterName) {
        List<OptimizationAction> currentHostActions = optimizationActionRepository.findCurrentActionsByType(currentDateTime, confidenceLevel, dataCenterName, ActionTypeName.HOST_WORKLOAD.getName());
        List<OptimizationAction> currentRelocateActions = optimizationActionRepository.findCurrentActionsByType(currentDateTime, confidenceLevel, dataCenterName, ActionTypeName.RELOCATE_WORKLOAD.getName());

        currentDateTime = currentDateTime.withMinute(0).withSecond(0).withNano(0);
        LocalDateTime endDate = currentDateTime.plusHours(1);
        List<OptimizationAction> moveFromCurrentActions = optimizationActionRepository.findCurrentSDTWActionsMoveFromDate(currentDateTime, endDate, confidenceLevel, dataCenterName, ActionTypeName.SHIFT_DELAY_TOLERANT_WORKLOAD.getName());

        List<OptimizationAction> currentWorkloadActions = new ArrayList<>();
        currentWorkloadActions.addAll(moveFromCurrentActions);
        currentWorkloadActions.addAll(currentHostActions);
        currentWorkloadActions.addAll(currentRelocateActions);

        return currentWorkloadActions.stream()
                .map(OptimizationActionBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

    }

    public List<OptimizationActionDTO> getAllActionsInInterval(LocalDateTime startTime,
                                                               LocalDateTime endTime,
                                                               Double confidenceLevel,
                                                               Timeframe timeframe,
                                                               String dataCenterName) {
        OptimizationActionServiceValidator.validateTimeIntervalConfidenceLevelAndTimeframeThrowErrors(startTime, endTime, confidenceLevel, timeframe);

        List<OptimizationAction> allActions = optimizationActionRepository.findActionsInIntervalAndTimeframe(startTime, endTime, confidenceLevel, dataCenterName, timeframe.getName());
        return allActions.stream()
                .map(OptimizationActionBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public List<OptimizationActionDTO> getActionsInIntervalByType(String dataCenterName,
                                                                  LocalDateTime startTime,
                                                                  LocalDateTime endTime,
                                                                  ActionTypeName actionTypeName,
                                                                  Double confidenceLevel,
                                                                  Timeframe timeframe) {

        List<OptimizationAction> allActions = optimizationActionRepository.findActionByTypeInIntervalAndTimeframe(startTime, endTime, confidenceLevel, dataCenterName, timeframe.getName(), actionTypeName.getName());
        return allActions.stream()
                .map(OptimizationActionBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    private List<OptimizationAction> getCurrentRedundantActions(LocalDateTime dateTime,
                                                                Double confidenceLevel,
                                                                String dataCenterName) {

        OptimizationActionServiceValidator.validateCurrentTimeAndConfidenceLevel(dateTime, confidenceLevel);
        String[] overriddenTypes = {ChargeBattery.ACTION_TYPE,
                DischargeBattery.ACTION_TYPE,
                ChargeTes.ACTION_TYPE,
                DischargeTes.ACTION_TYPE,
                DynamicAdjustmentOfCoolingIntensity.ACTION_TYPE};
        return optimizationActionRepository.findCurrentRedundantActions(dateTime, confidenceLevel, overriddenTypes, Timeframe.DAY_AHEAD.getName(), dataCenterName);
    }


    public List<AggregatedActionValuesDTO> getAveragedActionValues(LocalDateTime startTime, LocalDateTime endTime, PredictionGranularity granularity, ActionTypeName type, double confidenceLevel) {
        Object[][] values = optimizationActionRepository.getOptimizationActionValueHourRate(startTime, endTime, granularity.getSampleFrequencyMin(), granularity.getTimeframeLabel(), type.getName(), confidenceLevel);
        return AggregatedValuesBuilder.generateActionValuesDTOs(values);
    }

}
