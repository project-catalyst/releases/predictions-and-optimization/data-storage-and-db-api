package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasureUnitDTO;
import ro.tuc.dsrl.dbapi.model.MeasureUnit;

public class MeasureUnitBuilder {

    private MeasureUnitBuilder() {

    }

    public static MeasureUnitDTO generateDTOFromEntity(MeasureUnit measureUnit) {
        return new MeasureUnitDTO(
                measureUnit.getId(),
                measureUnit.getUnit()
        );
    }

    public static MeasureUnit generateEntityFromDTO(MeasureUnitDTO measureUnitDTO) {
        return new MeasureUnit(
                measureUnitDTO.getId(),
                measureUnitDTO.getUnit()
        );
    }
}
