package ro.tuc.dsrl.dbapi.service.uisupport.formatter;

import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.service.uisupport.ExecutionService;
import ro.tuc.dsrl.dbapi.util.MathUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EnergyProfileFormatter extends ProfileFormatter<List<AggregatedValuesDTO>> {

    @Override
    public List<Double> populateArray(List<AggregatedValuesDTO> tableRowValues, int startHour, PredictionGranularity granularity) {
        List<Double> valuesToReturn = new ArrayList<Double>(Collections.nCopies(granularity.getNoOutputs(), DEFAULT_UNMONIIORED_VALUE));
        int periodicityFactor = PredictionGranularity.DAYAHEAD.getSampleFrequencyMin()/granularity.getSampleFrequencyMin();
        for (AggregatedValuesDTO row : tableRowValues) {
            int index = periodicityFactor * (row.getHour() - startHour) + row.getMinuteSlots();
            valuesToReturn.set(index, MathUtils.round(row.getValue(), 2));
        }
        return valuesToReturn;
    }

    @Override
    public List<Double> populateIntraDayArrayFromDayAyead(List<AggregatedValuesDTO> tableRowValues) {
        List<Double> valuesToReturn = new ArrayList<Double>(Collections.nCopies(PredictionGranularity.INTRADAY.getNoOutputs(), DEFAULT_UNMONIIORED_VALUE));
        for (AggregatedValuesDTO row : tableRowValues) {
            int index = (row.getHour()) % PredictionGranularity.INTRADAY.getNoHours();
            valuesToReturn.set(2 * index, row.getValue());
            valuesToReturn.set(1 + 2 * index, MathUtils.round(row.getValue(), 2));
        }
        return valuesToReturn;
    }
    @Override
    public List<Double> populateRealTimeArrayFromDayAyead(List<AggregatedValuesDTO> tableRowValues, int noOutputs, int startHour) {
        List<Double> valuesToReturn = new ArrayList<Double>(Collections.nCopies(noOutputs, DEFAULT_UNMONIIORED_VALUE));
        int hourSlots = (int) ExecutionService.RT_HOUR_SLOTS;
        for (AggregatedValuesDTO row : tableRowValues) {
            int index = (row.getHour() - startHour);
            for (int i = 0; i < hourSlots ; i++) {
                valuesToReturn.set(i + hourSlots*index, row.getValue());
            }
        }
        return valuesToReturn;
    }
}
