package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.dsrl.dbapi.model.OptimizationPlan;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface OptimizationPlanRepository extends JpaRepository<OptimizationPlan, UUID> {

    @Query(value = "Select op " +
            "FROM OptimizationPlan op " +
            "WHERE op.id = :optimizationPlanUUID")
    OptimizationPlan findByUUID(@Param("optimizationPlanUUID") UUID optimizationPlanUUID);


    @Query(value = "Select op " +
            "FROM OptimizationPlan op " +
            "WHERE op.start = :startdate " +
            "AND op.timeframe like :timeframe "+
            "AND op.dataCenter.id like :dcId")
    List<OptimizationPlan> findByStartDateAndTimeframe(@Param("startdate")LocalDateTime time, @Param("timeframe") String timeframe, @Param("dcId") UUID dc);
}
