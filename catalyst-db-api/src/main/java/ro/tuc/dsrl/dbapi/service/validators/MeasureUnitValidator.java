package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasureUnitDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class MeasureUnitValidator {

    private static final Log LOGGER = LogFactory.getLog(MeasureUnitValidator.class);

    private MeasureUnitValidator() {
    }

    public static void validateInsert(MeasureUnitDTO measureUnitDTO) {

        List<String> errors = new ArrayList<>();
        if (measureUnitDTO == null) {
            errors.add("Measurement Unit DTO is null");
            throw new IncorrectParameterException(MeasureUnitValidator.class.getSimpleName(), errors);
        }

        if (measureUnitDTO.getId() == null) {
            errors.add("Measure unit ID is null");
        }

        if (measureUnitDTO.getUnit() == null || measureUnitDTO.getUnit().equals("")) {
            errors.add("Measurement Unit is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MeasureUnitValidator.class.getSimpleName(), errors);
        }
    }

}
