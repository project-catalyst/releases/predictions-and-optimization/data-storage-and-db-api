package ro.tuc.dsrl.dbapi.controller.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.GraphDataDTO;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.uisupport.ElectricalGraphDataService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/graph-data/electrical")
public class ElectricalGraphDataController {

    @Value("${datacenter.name}")
    private String dataCenterName;

    private final ElectricalGraphDataService electricalGraphDataService;
    private final DeviceService deviceService;

    @Autowired
    public ElectricalGraphDataController(ElectricalGraphDataService electricalGraphDataService,
                                         DeviceService deviceService) {
        this.electricalGraphDataService = electricalGraphDataService;
        this.deviceService = deviceService;
    }

    //region Electrical History Production-Consumption for DC
    @GetMapping(value = "/history/production-consumption/dayahead/{startTime}")
    public GraphDataDTO getMonitoredDayaheadConsumptionProductionDataForDC(
            @PathVariable("startTime") Long startTime) {



        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getMonitoredAggregatedProductionConsumptionForDC(
                dataCenterName,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD);
    }

    @GetMapping(value = "/history/production-consumption/intraday/{startTime}")
    public GraphDataDTO getMonitoredIntradayConsumptionProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getMonitoredAggregatedProductionConsumptionForDC(
                dataCenterName,
                startLocalDateTime,
                PredictionGranularity.INTRADAY);
    }

    @GetMapping(value = "/history/production-consumption/nearRealTime/{startTime}")
    public GraphDataDTO getMonitoredNearRealTimeConsumptionProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getMonitoredAggregatedProductionConsumptionForDC(
                dataCenterName,
                startLocalDateTime,
                PredictionGranularity.REALTIME_DAYAHEAD_FRAME);
    }
    //endregion


    //region Electrical History Disaggregated Data for DC
    @GetMapping(value = "/history/disaggregated/dayahead/{startTime}")
    public GraphDataDTO getMonitoredDayaheadDissagregatedConsumptionDataForDC(
            @PathVariable("startTime") Long startTime) {


        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getMonitoredDisaggregatedConsumptionForDC(
                dataCenterName,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD);
    }

    @GetMapping(value = "/history/disaggregated/intraday/{startTime}")
    public GraphDataDTO getMonitoredIntradayDissagregatedConsumptionDataForDC(
            @PathVariable("startTime") Long startTime) {



        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getMonitoredDisaggregatedConsumptionForDC(
                dataCenterName,
                startLocalDateTime,
                PredictionGranularity.INTRADAY);
    }

    @GetMapping(value = "/history/disaggregated/nearRealTime/{startTime}")
    public GraphDataDTO getMonitoredNearRealTimeDissagregatedConsumptionDataForDC(
            @PathVariable("startTime") Long startTime) {


        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getMonitoredDisaggregatedConsumptionForDC(
                dataCenterName,
                startLocalDateTime,
                PredictionGranularity.REALTIME_DAYAHEAD_FRAME);
    }
    //endregion


    //region Electrical Prediction Production-Consumption for DC
    @GetMapping(value = "/prediction/production-consumption/dayahead/{startTime}")
    public GraphDataDTO getPredictedDayaheadConsumptionProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getPredictedConsumptionProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name());
    }

    @GetMapping(value = "/prediction/production-consumption/intraday/{startTime}")
    public GraphDataDTO getPredictedIntradayConsumptionProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        GraphDataDTO graphDataDTO = electricalGraphDataService.getPredictedConsumptionProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name());
        return graphDataDTO;
    }

    @GetMapping(value = "/prediction/production-consumption/nearRealTime/{startTime}")
    public GraphDataDTO getPredictedNearRealTimeConsumptionProductionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getPredictedConsumptionProductionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name());
    }
    //endregion


    //region Electrical Predicted Disaggregated Data for DC
    @GetMapping(value = "/prediction/disaggregated/dayahead/{startTime}")
    public GraphDataDTO getPredictedDayaheadDissagregatedConsumptionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getPredictedDisaggregatedConsumptionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.name());
    }

    @GetMapping(value = "/prediction/disaggregated/intraday/{startTime}")
    public GraphDataDTO getPredictedIntradayDissagregatedConsumptionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getPredictedDisaggregatedConsumptionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                EnergyType.ELECTRICAL.name());
    }

    @GetMapping(value = "/prediction/disaggregated/nearRealTime/{startTime}")
    public GraphDataDTO getPredictedNearRealTimeDissagregatedConsumptionDataForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return electricalGraphDataService.getPredictedDisaggregatedConsumptionForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.NEAR_REAL_TIME,
                EnergyType.ELECTRICAL.name());
    }
    //endregion

}
