//package ro.tuc.dsrl.dbapi.service.validators;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import ro.tuc.dsrl.catalyst.model.dto.AncillaryServiceResultsDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class AncillaryServiceResultsValidator {
//
//    private static final Log LOGGER = LogFactory.getLog(AncillaryServiceResultsValidator.class);
//
//    private AncillaryServiceResultsValidator()
//    {
//    }
//
//    public static void validateInsert(AncillaryServiceResultsDTO ancillaryServiceResultsDTO) {
//
//        List<String> errors = new ArrayList<>();
//        if (ancillaryServiceResultsDTO == null) {
//            errors.add("Ancillary Service Results DTO is null");
//            throw new IncorrectParameterException(ActionValueValidator.class.getSimpleName(), errors);
//        }
//
//        if (ancillaryServiceResultsDTO.getId() == null) {
//            errors.add("Ancillary Service Results ID is null");
//        }
//
//        if (ancillaryServiceResultsDTO.getAdaptedProfile() == null) {
//            errors.add("Ancillary Service Results adaptedProfile is null");
//        }
//
//        if (ancillaryServiceResultsDTO.getRecordTime() == null) {
//            errors.add("Ancillary Service Results recordTime is null");
//        }
//
//        if (!errors.isEmpty()) {
//            LOGGER.error(errors);
//            throw new IncorrectParameterException(ActionValueValidator.class.getSimpleName(), errors);
//        }
//    }
//}
