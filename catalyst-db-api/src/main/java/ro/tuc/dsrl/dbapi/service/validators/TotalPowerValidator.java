//package ro.tuc.dsrl.dbapi.service.validators;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import ro.tuc.dsrl.catalyst.model.dto.TotalPowerDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
//import ro.tuc.dsrl.dbapi.model.TotalPower;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//public class TotalPowerValidator {
//
//    private static final Log LOGGER = LogFactory.getLog(TotalPowerValidator.class);
//
//    private TotalPowerValidator() {
//    }
//
//    public static void validateInsert(TotalPowerDTO totalPowerDTO) {
//        List<String> errors = new ArrayList<>();
//        if (totalPowerDTO == null) {
//            errors.add("Total Power DTO is null");
//            throw new IncorrectParameterException(ActionValueValidator.class.getSimpleName(), errors);
//        }
//
//        if (totalPowerDTO.getId() == null) {
//            errors.add("Total Power ID is null");
//        }
//
//        if (totalPowerDTO.getRecordTime() ==0) {
//            errors.add("Total Power recordTime is null or empty");
//        }
//
//        if (!errors.isEmpty()) {
//            LOGGER.error(errors);
//            throw new IncorrectParameterException(TotalPowerValidator.class.getSimpleName(), errors);
//        }
//    }
//
//    public static List<String> validateCurrentTime(LocalDateTime current){
//        List<String> errors = new ArrayList<>();
//
//        if(current == null){
//            errors.add("Current time must not be null");
//        }
//
//        return errors;
//
//    }
//
//    public static void validateCurrentTimeThrowErrors(LocalDateTime current) {
//        List<String> errors = validateCurrentTime(current);
//        throwIncorrectParameterException(errors);
//    }
//
//
//    public static void throwIncorrectParameterException(List<String> errors) {
//
//        if (!errors.isEmpty()) {
//            LOGGER.error(errors);
//            throw new IncorrectParameterException(TotalPower.class.getSimpleName(), errors);
//        }
//    }
//}
