package ro.tuc.dsrl.dbapi.util;

import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;
import ro.tuc.dsrl.dbapi.model.dto.resources.DeviceStateDTO;

import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

public class RepoConversionUtility {

    private RepoConversionUtility() {
    }

    public static List<DeviceStateDTO> getDevicesState(Object[][] results) {

        List<DeviceStateDTO> states = new ArrayList<>();
        for (Object[] row : results) {

            String label = (String) row[1];
            String property = (String) row[2];

            Double d = (Double) row[4];
            UUID id = getUUIDFromBytes((byte[]) row[0]);
            Timestamp date = (Timestamp) row[3];
            DeviceStateDTO state = new DeviceStateDTO(id, label, property, date.toLocalDateTime(), d);
            states.add(state);
        }
        return states;
    }

    public static List<Double> getSDTWValues(Object[][] sdtwResults) {

        List<Double> states = new ArrayList<>();
        for (Object[] row : sdtwResults) {

            Double d = (Double) row[2];
            states.add(d);
        }
        return states;
    }

    public static List<AggregatedValuesDTO> getAveragedValues(Object[][] results, Device device, Measurement measurement) {

        List<AggregatedValuesDTO> states = new ArrayList<>();
        for (Object[] row : results) {

            Double value = (Double) row[0];
            Integer year = (Integer) row[1];
            Integer month = (Integer) row[2];
            Integer day = (Integer) row[3];
            Integer hour = (Integer) row[4];
            Number minutes = (Number) row[5];
            AggregatedValuesDTO averagedValue = new AggregatedValuesDTO(value, device, measurement, year, month, day, hour, minutes.intValue());
            states.add(averagedValue);
        }
        return states;
    }


    public static List<EnergySampleDTO> convertAggregatedValuesToEnergySamples(List<AggregatedValuesDTO> values, LocalDateTime startTime, PredictionGranularity granularity) {
        List<EnergySampleDTO> eEstimated = new ArrayList<>();
        for(int i = 0; i < granularity.getNoOutputs(); i ++ ){
            EnergySampleDTO sample = new EnergySampleDTO(0.0, startTime.withHour(i));
            eEstimated.add(sample);
        }

        //TODO: fix ID
        for (AggregatedValuesDTO aggValue : values) {
            EnergySampleDTO sample = new EnergySampleDTO(aggValue.getValue(), LocalDateTime.of(aggValue.getYear(), startTime.getMonth(), startTime.getDayOfMonth(), aggValue.getHour(), aggValue.getMinute()));
            eEstimated.set(aggValue.getHour(), sample);
        }
        return eEstimated;
    }


    public static UUID getUUIDFromBytes(byte[] bytes) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        Long high = byteBuffer.getLong();
        Long low = byteBuffer.getLong();

        return new UUID(high, low);
    }
}
