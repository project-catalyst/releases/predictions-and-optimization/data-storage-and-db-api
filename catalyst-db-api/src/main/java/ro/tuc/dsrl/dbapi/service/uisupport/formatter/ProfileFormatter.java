package ro.tuc.dsrl.dbapi.service.uisupport.formatter;


import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

import java.util.List;

public abstract class ProfileFormatter<T> {
    public static final double DEFAULT_UNMONIIORED_VALUE = -0.00001;

    public static ProfileFormatter getInstance(ProfileType type) {

        if (ProfileType.OPTIMIZED_HEAT_ACTIONS.equals(type)) {
            return new OptimizedHeatProfileFormatter();
        }
        if (ProfileType.ACTUAL_PROFILES.equals(type)) {
            return new EnergyProfileFormatter();
        }

        if (ProfileType.HEAT_PROFILE.equals(type)) {
            return new HeatProfileFormatter();
        }
        return new OptimizedEnergyProfileFormatter();
    }

    public abstract List<Double> populateArray(T tableRowValues, int startHour, PredictionGranularity granularity);

    public abstract List<Double> populateIntraDayArrayFromDayAyead(T tableRowValues);

    public abstract List<Double> populateRealTimeArrayFromDayAyead(T tableRowValues, int noOutputs, int startHour);

    public static enum ProfileType {
        OPTIMIZED_ENERGY_ACTIONS, OPTIMIZED_HEAT_ACTIONS, ACTUAL_PROFILES, HEAT_PROFILE
    }
}
