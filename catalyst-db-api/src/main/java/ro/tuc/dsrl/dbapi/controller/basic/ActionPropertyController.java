//package ro.tuc.dsrl.dbapi.controller.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ro.tuc.dsrl.catalyst.model.dto.ActionPropertyDTO;
//import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
//import ro.tuc.dsrl.dbapi.service.basic.ActionPropertyService;
//
//import java.util.List;
//import java.util.UUID;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/action-property")
//public class ActionPropertyController {
//
//    private final ActionPropertyService actionPropertyService;
//
//    @Autowired
//    public ActionPropertyController(ActionPropertyService actionInstanceService) {
//        this.actionPropertyService = actionInstanceService;
//    }
//
//    @GetMapping(value = "/byId/{actionPropertyId}")
//    public ActionPropertyDTO findById(@PathVariable("actionPropertyId") String actionPropertyId) {
//
//        UUIDPreconditions.validate(actionPropertyId);
//
//        UUID actionPropertyUUID = UUID.fromString(actionPropertyId);
//
//        return actionPropertyService.findById(actionPropertyUUID);
//    }
//
//    @GetMapping(value = "/all")
//    public List<ActionPropertyDTO> findAll() {
//
//        return actionPropertyService.findAll();
//    }
//
//    @PostMapping(value = "/insert")
//    public UUID insertActionPropertyDTO(@RequestBody ActionPropertyDTO actionPropertyDTO) {
//
//        return actionPropertyService.insert(actionPropertyDTO);
//    }
//}
