package ro.tuc.dsrl.dbapi.service.uisupport;

import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.FlexibilityStrategyGraphDTO;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class OptimizationSetupService {

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;

    public OptimizationSetupService(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
    }

    public FlexibilityStrategyGraphDTO getGraph(String dcName, LocalDateTime untilDate)
    {
        FlexibilityStrategyGraphDTO flexibilityStrategyGraphDTO = new FlexibilityStrategyGraphDTO(PredictionGranularity.DAYAHEAD.getNoOutputs());

        LocalDateTime startDate = DateUtils.computeCurrentDayStart(untilDate);
        LocalDateTime endDate = DateUtils.computeCurrentDayEnd(untilDate);

        List<Double> drSig = convertValuesGranularity(dcName,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DR_SIGNAL,
                startDate,
                endDate);

        List<Double> heat = convertValuesGranularity(dcName, PredictionGranularity.DAYAHEAD, MeasurementType.DATA_CENTER_HEAT_BASELINE, startDate, endDate);
        List<Double> ePrice = convertValuesGranularity(dcName, PredictionGranularity.DAYAHEAD, MeasurementType.DC_E_PRICE, startDate, endDate);
        List<Double> lPrice = convertValuesGranularity(dcName, PredictionGranularity.DAYAHEAD, MeasurementType.DC_L_PRICE, startDate, endDate);

        //TO BE UPDATED
        List<Double> renewableEnergy = convertValuesGranularity(dcName,PredictionGranularity.DAYAHEAD, MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE, startDate, endDate);
        /*
        = Arrays.asList(200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0,
                200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0);
        */
        flexibilityStrategyGraphDTO.setDrSig(drSig);
        flexibilityStrategyGraphDTO.setHeat(heat);
        flexibilityStrategyGraphDTO.setePrice(ePrice);
        flexibilityStrategyGraphDTO.setlPrice(lPrice);
        flexibilityStrategyGraphDTO.setRenewableEnergy(renewableEnergy);

        return flexibilityStrategyGraphDTO;
    }

    public List<Double> convertValuesGranularity(String datacenter, PredictionGranularity predictionGranularity, MeasurementType measurementType, LocalDateTime startTime, LocalDateTime endTime) {
        ForecastDayAheadDTO value = energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(datacenter,
                startTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                measurementType);
        List<Double> doubleValues = convertListToDouble(value.getEnergyPointValueDTOS());

        List<Double> newList = new ArrayList<>();

        if (!predictionGranularity.equals(PredictionGranularity.DAYAHEAD)) {
            for (int x = 0; x < 4; x++) {
                if (predictionGranularity.equals(PredictionGranularity.INTRADAY)) {
                    newList.add(doubleValues.get(x));
                    newList.add(doubleValues.get(x));
                } else {
                    int i = 12;
                    while (i > 0) {
                        newList.add(doubleValues.get(x));
                        i--;
                    }
                }
            }
        } else {
            newList = doubleValues;
        }

        return newList;
    }

    public List<Double> convertListToDouble(List<EnergyPointValueDTO> energyPointValueDTOS) {
        List<Double> energy = new ArrayList<>();
        for (EnergyPointValueDTO energyPointValueDTO : energyPointValueDTOS) {
            energy.add(energyPointValueDTO.getEnergyValue());
        }
        return energy;
    }
}
