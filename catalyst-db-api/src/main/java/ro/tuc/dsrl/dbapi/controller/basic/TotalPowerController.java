//package ro.tuc.dsrl.dbapi.controller.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ro.tuc.dsrl.catalyst.model.dto.TotalPowerDTO;
//import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
//import ro.tuc.dsrl.dbapi.controller.preconditions.TotalPowerPreconditions;
//import ro.tuc.dsrl.dbapi.service.basic.TotalPowerService;
//
//import java.time.LocalDateTime;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/total-power")
//public class TotalPowerController {
//
//    private final TotalPowerService totalPowerService;
//
//    @Autowired
//    public TotalPowerController(TotalPowerService totalPowerService){
//        this.totalPowerService = totalPowerService;
//    }
//
//    @GetMapping("/current/{date}")
//    public TotalPowerDTO getCurrentTotalPower(@PathVariable("date") Long currentTimeMillis){
//
//        TotalPowerPreconditions.validateCurrentTimeMillisThrowErrors(currentTimeMillis);
//
//        LocalDateTime currentDateTime = DateUtils.millisToUTCLocalDateTime(currentTimeMillis);
//
//        return this.totalPowerService.getCurrent(currentDateTime);
//    }
//}
