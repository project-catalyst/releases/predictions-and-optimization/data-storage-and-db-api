package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.dbmodel.TrainedModelDTO;
import ro.tuc.dsrl.dbapi.model.TrainedModel;

public class TrainedModelBuilder {

    private TrainedModelBuilder() {

    }

    public static TrainedModelDTO generateDTOFromEntity(TrainedModel trainedModel) {
        return new TrainedModelDTO(
                trainedModel.getId(),
                trainedModel.getScenario(),
                trainedModel.getPredictionType(),
                trainedModel.getAlgorithmType(),
                trainedModel.getComponentType(),
                trainedModel.getPathOnDisk(),
                trainedModel.getStatus(),
                trainedModel.getStartTime()

        );
    }

    public static TrainedModel generateEntityFromDTO(TrainedModelDTO trainedModelDTO) {
        return new TrainedModel(
                trainedModelDTO.getId(),
                trainedModelDTO.getScenario(),
                trainedModelDTO.getPredictionType(),
                trainedModelDTO.getAlgorithmType(),
                trainedModelDTO.getComponentType(),
                trainedModelDTO.getPathOnDisk(),
                trainedModelDTO.getStatus(),
                trainedModelDTO.getStartTime()
        );
    }
}
