//package ro.tuc.dsrl.dbapi.service.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import ro.tuc.dsrl.catalyst.model.dto.ActionValueDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
//import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
//import ro.tuc.dsrl.dbapi.model.ActionInstance;
//import ro.tuc.dsrl.dbapi.model.ActionProperty;
//import ro.tuc.dsrl.dbapi.model.ActionValue;
//import ro.tuc.dsrl.dbapi.model.builders.ActionValueBuilder;
//import ro.tuc.dsrl.dbapi.repository.*;
//import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
//import ro.tuc.dsrl.dbapi.service.validators.ActionValueValidator;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//import java.util.stream.Collectors;
//
//@Service
//public class ActionValueService {
//
//    private final ActionValueRepository actionValueRepository;
//    private final ActionInstanceRepository actionInstanceRepository;
//    private final ActionPropertyRepository actionPropertyRepository;
//
//
//    @Autowired
//    public ActionValueService(ActionValueRepository actionValueRepository, ActionInstanceRepository actionInstanceRepository, ActionPropertyRepository actionPropertyRepository) {
//        this.actionValueRepository = actionValueRepository;
//        this.actionInstanceRepository = actionInstanceRepository;
//        this.actionPropertyRepository = actionPropertyRepository;
//
//    }
//
//    public List<ActionValueDTO> findAll() {
//
//        List<ActionValue> actionValues = actionValueRepository.findAll();
//
//        return actionValues.stream()
//                .map(av -> ActionValueBuilder.generateDTOFromEntity(av, av.getActionInstanceId(), av.getActionPropertyId()))
//                .collect(Collectors.toList());
//    }
//
//    public UUID insert(ActionValueDTO actionValueDTO) {
//        ActionValueValidator.validateInsert(actionValueDTO);
//
//        List<String> errors = new ArrayList<>();
//        ActionInstance actionInstance = actionInstanceRepository.findByUUID(actionValueDTO.getActionInstanceId());
//        if (actionInstance == null)
//            errors.add("Action instance was not found in database.");
//
//        ActionProperty actionProperty = actionPropertyRepository.findByUUID(actionValueDTO.getActionPropertyId());
//        if (actionProperty == null)
//            errors.add("Action property was not found in database.");
//
//        if (!errors.isEmpty()) {
//            throw new EntityValidationException(ActionValue.class.getSimpleName(), errors);
//        }
//
//        return actionValueRepository
//                .save(ActionValueBuilder.generateEntityFromDTO(actionValueDTO, actionInstance, actionProperty))
//                .getId();
//    }
//
//    public ActionValueDTO findById(UUID actionValueUUID) {
//
//        ActionValue actionValue = actionValueRepository.findByUUID(actionValueUUID);
//        if (actionValue == null) {
//            throw new ResourceNotFoundException("ActionValue with ID " + actionValueUUID);
//        }
//
//        return ActionValueBuilder.generateDTOFromEntity(
//                actionValue,
//                actionValue.getActionInstanceId(),
//                actionValue.getActionPropertyId());
//    }
//}
