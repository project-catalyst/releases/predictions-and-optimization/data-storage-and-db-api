package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasureUnitDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.MeasureUnit;
import ro.tuc.dsrl.dbapi.model.builders.MeasureUnitBuilder;
import ro.tuc.dsrl.dbapi.repository.MeasureUnitRepository;
import ro.tuc.dsrl.dbapi.service.validators.MeasureUnitValidator;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MeasureUnitService {

    private final MeasureUnitRepository measureUnitRepository;

    @Autowired
    public MeasureUnitService(MeasureUnitRepository measureUnitRepository) {
        this.measureUnitRepository = measureUnitRepository;
    }

    public MeasureUnitDTO findById(UUID measureUnitUUID) {
        MeasureUnit measureUnit = measureUnitRepository.findByUUID(measureUnitUUID);
        if (measureUnit == null) {
            throw new ResourceNotFoundException("MeasureUnit with ID " + measureUnitUUID);
        }

        return MeasureUnitBuilder.generateDTOFromEntity(measureUnit);
    }

    public List<MeasureUnitDTO> findAll() {
        List<MeasureUnit> measureUnitList = measureUnitRepository.findAll();

        return measureUnitList.stream()
                .map(MeasureUnitBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public UUID insert(MeasureUnitDTO measureUnitDTO) {
        MeasureUnitValidator.validateInsert(measureUnitDTO);

        MeasureUnit measureUnit = MeasureUnitBuilder.generateEntityFromDTO(measureUnitDTO);
        return measureUnitRepository.save(measureUnit).getId();
    }
}
