package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.FlexibilityStrategy;

import java.time.LocalDateTime;
import java.util.UUID;

@Repository
public interface FlexibilityStrategyRepository extends JpaRepository<FlexibilityStrategy, UUID> {

    @Query(value =  "SELECT fs " +
                    "FROM FlexibilityStrategy fs " +
                    "WHERE fs.time = (SELECT MAX(time) " +
                                     "FROM FlexibilityStrategy " +
                                     "WHERE datacenter = :datacenter AND time < :time)" +
                    "GROUP BY fs.id")
    FlexibilityStrategy findByDatacenterAndTime(@Param("datacenter") Device datacenter, @Param("time") LocalDateTime time);
}
