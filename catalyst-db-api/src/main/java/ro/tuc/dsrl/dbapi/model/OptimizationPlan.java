package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "optimization_plans")
public class OptimizationPlan {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "timeframe")
    private String timeframe;

    @Column(name = "start")
    private LocalDateTime start;

    @Column(name = "end")
    private LocalDateTime end;

    @Column(name = "selected")
    private Boolean selected;

    @Column(name = "confidence_level")
    private Double confidenceLevel;

    @Column(name = "cost_savings")
    private Double costSavings;

    @Column(name = "carbon_savings")
    private Double carbonSavings;

    @Column(name = "energy_savings")
    private Double energySavings;

    @Column(name = "we")
    private Double wE;

    @Column(name = "wl")
    private Double wL;

    @Column(name = "wf")
    private Double wF;

    @Column(name = "wt")
    private Double wT;

    @Column(name = "wren")
    private Double wren;

    @Column(name = "realloc_active")
    private Boolean reallocActive;

    @ManyToOne
    @JoinColumn(name = "datacenter")
    private Device dataCenter;

    public OptimizationPlan() {
    }
    public OptimizationPlan(String timeframe, LocalDateTime start, LocalDateTime end, Boolean selected, Double confidenceLevel, Double wE, Double wL, Double wF, Double wT, Double wren, Boolean reallocActive, Device dataCenter) {
        this.timeframe = timeframe;
        this.start = start;
        this.end = end;
        this.selected = selected;
        this.confidenceLevel = confidenceLevel;
        this.wE = wE;
        this.wL = wL;
        this.wF = wF;
        this.wT = wT;
        this.wren = wren;
        this.reallocActive = reallocActive;
        this.dataCenter = dataCenter;
    }

    public OptimizationPlan(String timeframe, LocalDateTime start, LocalDateTime end, Boolean selected, Double confidenceLevel, Double costSavings, Double carbonSavings, Double energySavings, Double wE, Double wL, Double wF, Double wT, Double wren,Boolean reallocActive, Device dataCenter) {
        this.timeframe = timeframe;
        this.start = start;
        this.end = end;
        this.selected = selected;
        this.confidenceLevel = confidenceLevel;
        this.costSavings = costSavings;
        this.carbonSavings = carbonSavings;
        this.energySavings = energySavings;
        this.wE = wE;
        this.wL = wL;
        this.wF = wF;
        this.wT = wT;
        this.wren = wren;
        this.reallocActive = reallocActive;
        this.dataCenter = dataCenter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OptimizationPlan that = (OptimizationPlan) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Double getConfidenceLevel() {
        return confidenceLevel;
    }

    public void setConfidenceLevel(Double confidenceLevel) {
        this.confidenceLevel = confidenceLevel;
    }

    public Double getCostSavings() {
        return costSavings;
    }

    public void setCostSavings(Double costSavings) {
        this.costSavings = costSavings;
    }

    public Double getCarbonSavings() {
        return carbonSavings;
    }

    public void setCarbonSavings(Double carbonSavings) {
        this.carbonSavings = carbonSavings;
    }

    public Double getEnergySavings() {
        return energySavings;
    }

    public void setEnergySavings(Double energySavings) {
        this.energySavings = energySavings;
    }

    public Double getWE() {
        return wE;
    }

    public void setWE(Double wE) {
        this.wE = wE;
    }

    public Double getWL() {
        return wL;
    }

    public void setWL(Double wL) {
        this.wL = wL;
    }

    public Double getWF() {
        return wF;
    }

    public void setWF(Double wF) {
        this.wF = wF;
    }

    public Double getWT() {
        return wT;
    }

    public void setWT(Double wT) {
        this.wT = wT;
    }

    public Double getWren() {
        return wren;
    }

    public void setWren(Double wren) {
        this.wren = wren;
    }

    public Boolean getReallocActive() {
        return reallocActive;
    }

    public void setReallocActive(Boolean reallocActive) {
        this.reallocActive = reallocActive;
    }

    public Device getDataCenter() {
        return dataCenter;
    }

    public void setDataCenter(Device dataCenter) {
        this.dataCenter = dataCenter;
    }
}
