package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.PredictionJob;
import ro.tuc.dsrl.dbapi.model.builders.PredictionJobBuilder;
import ro.tuc.dsrl.dbapi.repository.PredictionJobRepository;
import ro.tuc.dsrl.dbapi.service.validators.PredictionJobValidator;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PredictionJobService {
    private final PredictionJobRepository predictionJobRepository;

    @Autowired
    public PredictionJobService(PredictionJobRepository predictionJobRepository) {
        this.predictionJobRepository = predictionJobRepository;
    }

    public PredictionJobDTO findById(UUID predictionJobUUID) {
        PredictionJob predictionJob = predictionJobRepository.findByUUID(predictionJobUUID);
        if (predictionJob == null) {
            throw new ResourceNotFoundException("PredictionJob with ID " + predictionJobUUID);
        }

        return PredictionJobBuilder.generateDTOFromEntity(predictionJob);
    }

    public List<PredictionJobDTO> findAll() {
        List<PredictionJob> predictionJobs = predictionJobRepository.findAll();

        return predictionJobs.stream()
                .map(PredictionJobBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public UUID insert(PredictionJobDTO predictionJobDTO) {

        PredictionJobValidator.validateInsert(predictionJobDTO);

        PredictionJob predictionJob = PredictionJobBuilder.generateEntityFromDTO(predictionJobDTO);
        return predictionJobRepository.save(predictionJob).getId();
    }
}
