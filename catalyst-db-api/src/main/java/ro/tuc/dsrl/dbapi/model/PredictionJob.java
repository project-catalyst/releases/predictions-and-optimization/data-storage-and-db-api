package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "prediction_job")
public class PredictionJob {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "start_time")
    @NotNull
    private LocalDateTime startTime;

    @Column(name = "granularity")
    @NotNull
    private String granularity;

    @Column(name = "algo_type")
    @NotNull
    private String algorithmType;

    @Column(name = "flexibility_type")
    @NotNull
    private String flexibilityType;

    public PredictionJob() {

    }

    public PredictionJob(@NotNull LocalDateTime startTime, @NotNull String granularity, @NotNull String algorithmType, @NotNull String flexibilityType) {
        this.startTime = startTime;
        this.granularity = granularity;
        this.algorithmType = algorithmType;
        this.flexibilityType = flexibilityType;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public String getGranularity() {
        return granularity;
    }

    public String getAlgorithmType() {
        return algorithmType;
    }

    public String getFlexibilityType() {
        return flexibilityType;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void setGranularity(String granularity) {
        this.granularity = granularity;
    }

    public void setAlgorithmType(String algorithmType) {
        this.algorithmType = algorithmType;
    }

    public void setFlexibilityType(String flexibilityType) {
        this.flexibilityType = flexibilityType;
    }
}
