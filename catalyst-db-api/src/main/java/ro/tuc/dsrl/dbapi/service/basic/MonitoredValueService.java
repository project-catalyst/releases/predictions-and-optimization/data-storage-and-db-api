package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.catalyst.model.utils.Pair;
import ro.tuc.dsrl.dbapi.model.builders.AggregatedValuesBuilder;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;
import ro.tuc.dsrl.dbapi.model.MonitoredValue;
import ro.tuc.dsrl.dbapi.model.builders.MonitoredValueBuilder;
import ro.tuc.dsrl.dbapi.model.dto.resources.DeviceStateDTO;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.MeasurementRepository;
import ro.tuc.dsrl.dbapi.repository.MonitoredValueRepository;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;
import ro.tuc.dsrl.dbapi.service.validators.MonitoredValueValidator;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.HeatRecoveryInfrastructure;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MonitoredValueService {

    private final MonitoredValueRepository monitoredValueRepository;
    private final DeviceRepository deviceRepository;
    private final MeasurementRepository measurementRepository;
    private final DeviceService deviceService;

    @Autowired
    public MonitoredValueService(MonitoredValueRepository monitoredValueRepository, DeviceRepository deviceRepository, MeasurementRepository measurementRepository, DeviceService deviceService) {
        this.monitoredValueRepository = monitoredValueRepository;
        this.deviceRepository = deviceRepository;
        this.measurementRepository = measurementRepository;
        this.deviceService = deviceService;
    }

    public UUID insert(MonitoredValueDTO monitoredValueDTO) {

        MonitoredValueValidator.validateInsert(monitoredValueDTO);

        List<String> errors = new ArrayList<>();
        Device device = deviceRepository.findByUUID(monitoredValueDTO.getDeviceId());
        if (device == null) {
            errors.add("Device was not found in database.");
        }

        Measurement measurement = measurementRepository.findByUUID(monitoredValueDTO.getMeasurementId());
        if (measurement == null) {
            errors.add("Measurement was not found in database.");
        }

        if( (device!=null && measurement!=null ) && (measurement.getDeviceTypeId()== null || device.getDeviceTypeId() == null  ||
               ! measurement.getDeviceTypeId().getId().equals(device.getDeviceTypeId().getId())) ){
            errors.add("Measurement and Device types are inconsistent.");
        }

        if (!errors.isEmpty()) {
            throw new EntityValidationException(MonitoredValue.class.getSimpleName(), errors);
        }

        return monitoredValueRepository.save(MonitoredValueBuilder.generateEntityFromDTO(monitoredValueDTO, measurement, device)).getId();
    }

    public void insertDatacenterStat(DatacenterStateDTO newResults, String datacenterName) {
        LocalDateTime time = DateUtils.millisToUTCLocalDateTime(newResults.getRecordTime());
        Double batteryCurrent = newResults.getEsdCurrent();
        Double tesCurrent = newResults.getTesCurrent();

        Double srDT = newResults.getItPowerConsumptionDt() + newResults.getHostWorkload() - newResults.getRealocEnergy();
        Double srRT = newResults.getItPowerConsumptionRt();
        Double serverRoom = srDT + srRT;
        Double coolingValues = newResults.getFacilityPowerConsumption();
        Double selfThermalHeat = newResults.getSelfThermalHeat();

        extractAndInsertMonitoredValueInstance(datacenterName, time, batteryCurrent, MeasurementType.BATTERY_CURRENT, DeviceTypeEnum.BATTERY);
        extractAndInsertMonitoredValueInstance(datacenterName, time, tesCurrent, MeasurementType.TES_CURRENT, DeviceTypeEnum.TES);
        extractAndInsertMonitoredValueInstance(datacenterName, time, serverRoom, MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION, DeviceTypeEnum.SERVER_ROOM);
        extractAndInsertMonitoredValueInstance(datacenterName, time, srDT, MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION, DeviceTypeEnum.SERVER_ROOM);
        extractAndInsertMonitoredValueInstance(datacenterName, time, srRT, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION, DeviceTypeEnum.SERVER_ROOM);
        extractAndInsertMonitoredValueInstance(datacenterName, time, coolingValues, MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION, DeviceTypeEnum.COOLING_SYSTEM);
        extractAndInsertMonitoredValueInstance(datacenterName, time, selfThermalHeat, MeasurementType.SELF_THERMAL_HEAT, DeviceTypeEnum.SERVER_ROOM);
    }

    /**
     * Corner case: What happens when the DC has multiple devices of one type? How the simulated monitoring should be handled?
     */
    private void extractAndInsertMonitoredValueInstance(String datacenterName, LocalDateTime time, Double value, MeasurementType measurementType, DeviceTypeEnum deviceType) {
        UUID measurementId = measurementRepository.findByProperty(measurementType.type());
        List<DeviceDTO> devices = deviceService.findByParentAndType(datacenterName, deviceType);
        if (!devices.isEmpty()) {
            insert(new MonitoredValueDTO(time, value, devices.get(0).getId(), measurementId));
        }
    }

    public void insertMore(List<MonitoredValueDTO> monitoredValueDTOs) {

        List<MonitoredValue> mv = new ArrayList<>();
        for (MonitoredValueDTO dto : monitoredValueDTOs) {
            MonitoredValueValidator.validateInsert(dto);

            List<String> errors = new ArrayList<>();
            Device device = deviceRepository.findByUUID(dto.getDeviceId());
            if (device == null) {
                errors.add("Device was not found in database.");
            }
            Measurement measurement = measurementRepository.findByUUID(dto.getMeasurementId());
            if (measurement == null) {
                errors.add("Measurement was not found in database.");
            }
            if (!errors.isEmpty()) {
                throw new EntityValidationException(MonitoredValue.class.getSimpleName(), errors);
            }
            mv.add(MonitoredValueBuilder.generateEntityFromDTO(dto, measurement, device));
        }

        monitoredValueRepository.saveAll(mv);
    }

    public List<MonitoredValueDTO> findAll() {

        List<MonitoredValue> monitoredValues = monitoredValueRepository.findAll();

        return monitoredValues.stream()
                .map(mv -> MonitoredValueBuilder.generateDTOFromEntity(mv, mv.getMeasurementId(), mv.getDeviceId()))
                .collect(Collectors.toList());
    }

    public MonitoredValueDTO findById(UUID monitoredValueUUID) {

        MonitoredValue monitoredValue = monitoredValueRepository.findByUUID(monitoredValueUUID);
        if (monitoredValue == null) {
            throw new ResourceNotFoundException("Monitored value with ID " + monitoredValueUUID);
        }

        return MonitoredValueBuilder.generateDTOFromEntity(
                monitoredValue,
                monitoredValue.getMeasurementId(),
                monitoredValue.getDeviceId());
    }


    public List<AggregatedValuesDTO> getAveragedMonitoredValues(String dcName, LocalDateTime startTime, LocalDateTime endTime, PredictionGranularity granularity, MeasurementType type){
            Object[][] values = monitoredValueRepository.getAveragedMonitoredValues(dcName, startTime, endTime, granularity.getSampleFrequencyMin(), type.type());
            return AggregatedValuesBuilder.generateMonitoredValuesDTOs(values);
    }


    public List<AggregatedValuesDTO> getBaselineMonitoredValuesGroupedByYear(String dcName, LocalDateTime startTime, LocalDateTime endTime, PredictionGranularity granularity, MeasurementType type){
        Object[][] values = monitoredValueRepository.getAveragedMonitoredValuesGroupedByYear(dcName, startTime, endTime, granularity.getSampleFrequencyMin(), type.type());
        return AggregatedValuesBuilder.generateMonitoredValuesAggregatedByYearDTOs(values);
    }

    public List<Double> getPropertyCurve(String dataCenterName,
                                         LocalDateTime startTime,
                                         LocalDateTime endTime,
                                         PredictionGranularity predictionGranularity,
                                         MeasurementType measurementType) {
        List<String> errors = EnergyProfileValidator.validateParams(dataCenterName, startTime, predictionGranularity, MeasurementType.DR_SIGNAL.type());
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        UUID dataCenterID = deviceRepository.findByLabel(dataCenterName);

        // find dr_signal in interval
        List<AggregatedValuesDTO> historicalDisaggregatedConsumptionData = monitoredValueRepository.findPropertyInInterval(
                dataCenterID,
                startTime,
                endTime,
                measurementType.type(),
                predictionGranularity.getSampleFrequencyMin());


        List<Double> energyPointValueDTOS = new ArrayList<>();
        for (AggregatedValuesDTO energySampleDTO : historicalDisaggregatedConsumptionData) {
            energyPointValueDTOS.add(energySampleDTO.getValue());
        }

        while (energyPointValueDTOS.size() < predictionGranularity.getNoOutputs()) {
            energyPointValueDTOS.add(0.0);
        }
        return energyPointValueDTOS;
    }

    public DatacenterStateDTO getLatestMonitoringPreOpt(String dcName, LocalDateTime time) {
        List<DeviceStateDTO> serverRoomStates = deviceService.getDevicesState(dcName, DeviceTypeEnum.SERVER_ROOM, time);
        Map<Pair<String, UUID>, Map<String, Double>> serverRoomsProperties = DeviceService.mapDeviceStateKeyValuePairs(serverRoomStates);

        List<ServerRoom> serverRooms = new ArrayList<>();

        double totalConsumptionDT = 0;
        double totalConsumptionRT = 0;

        for (Map.Entry<Pair<String, UUID>, Map<String, Double>> serverRoomProps : serverRoomsProperties.entrySet()) {

            Map<String, Double> currentServerRoom = serverRoomProps.getValue();

            Double edPercentage = currentServerRoom.getOrDefault(MeasurementType.ED_PERCENTAGE.type(), 0.0);
            Double preOptTotal = currentServerRoom.getOrDefault(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_CONSUMPTION.type(), 0.0);
            Double preOptRT = currentServerRoom.getOrDefault(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type(), preOptTotal * (1 - edPercentage));
            Double preOptDT = currentServerRoom.getOrDefault(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION.type(), preOptTotal * edPercentage);

            totalConsumptionDT += preOptDT;
            totalConsumptionRT += preOptRT;
        }
        DatacenterStateDTO exec = new DatacenterStateDTO();
        exec.setItPowerConsumptionRt(totalConsumptionRT);
        exec.setItPowerConsumptionDt(totalConsumptionDT);
        return exec;

    }

    public DatacenterStateDTO getLatestMonitoring(String dcName, LocalDateTime time) {

        double totalConsumptionCooling = 0;
        List<CoolingSystem> coolingSystems = deviceService.getCoolingSystem(dcName, time);
        for (CoolingSystem cs : coolingSystems) {
            totalConsumptionCooling += cs.getEnergyConsumption();
        }

        double totalConsumptionDT = 0;
        double totalConsumptionRT = 0;
        List<ServerRoom> serverRooms = deviceService.getServerRoom(dcName, time);
        for (ServerRoom sr : serverRooms) {
            double rt = (sr.getItRT() >= 0) ? sr.getItRT() : (1 - sr.getEdPercentage()) * sr.getEnergyConsumption();
            double dt = (sr.getItDT() >= 0) ? sr.getItDT() : sr.getEdPercentage() * sr.getEnergyConsumption();

            totalConsumptionDT += dt;
            totalConsumptionRT += rt;
        }

        double currentStateBatteries = 0;
        List<Battery> batteries = deviceService.getBatteries(dcName, time);
        for (Battery battery : batteries) {
            currentStateBatteries += battery.getActualLoadedCapacity();

        }
        double currentStateTes = 0;
        List<ThermalEnergyStorage> tes = deviceService.getThermalEnergyStorages(dcName, time);
        for (ThermalEnergyStorage t : tes) {
            currentStateTes += t.getActualLoadedCapacity();
        }

        double totalConsumptionHRI = 0;
        List<HeatRecoveryInfrastructure> hris = deviceService.getHeatRecoveryInfrastructure(dcName, time);
        for (HeatRecoveryInfrastructure h : hris) {
            totalConsumptionHRI += h.getEnergyConsumption();
        }

        DatacenterStateDTO exec = new DatacenterStateDTO();
        exec.setEsdCurrent(currentStateBatteries);
        exec.setFacilityPowerConsumption(totalConsumptionCooling);
        exec.setTesCurrent(currentStateTes);
        exec.setItPowerConsumptionRt(totalConsumptionRT);
        exec.setItPowerConsumptionDt(totalConsumptionDT);
        return exec;
    }


}
