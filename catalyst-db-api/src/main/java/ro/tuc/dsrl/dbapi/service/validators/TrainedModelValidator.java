package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.TrainedModelDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.ValueTypeDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class TrainedModelValidator {

    private TrainedModelValidator() {

    }

    private static final Log LOGGER = LogFactory.getLog(TrainedModelValidator.class);

    public static void validateInsert(TrainedModelDTO trainedModelDTO) {

        List<String> errors = new ArrayList<>();
        if (trainedModelDTO == null) {
            errors.add("Trained Model DTO is null");
            throw new IncorrectParameterException(TrainedModelDTO.class.getSimpleName(), errors);
        }

        if (trainedModelDTO.getAlgorithmType() == null || trainedModelDTO.getAlgorithmType().equals("")) {
            errors.add("Trained model algorithm type is null");
        }

        if (trainedModelDTO.getComponentType() == null || trainedModelDTO.getComponentType().equals("")) {
            errors.add("Trained model component type is null or empty");
        }

        if (trainedModelDTO.getPathOnDisk() == null || trainedModelDTO.getPathOnDisk().equals("")) {
            errors.add("Trained model path is null or empty");
        }

        if (trainedModelDTO.getPredictionType() == null || trainedModelDTO.getPredictionType().equals("")) {
            errors.add("Trained model prediction type is null or empty");
        }

        if (trainedModelDTO.getScenario() == null || trainedModelDTO.getScenario().equals("")) {
            errors.add("Trained model scenario is null or empty");
        }

        if (trainedModelDTO.getStartTime() == null) {
            errors.add("Trained model start time is null");
        }

        if (trainedModelDTO.getStatus() == null || trainedModelDTO.getStatus().equals("")) {
            errors.add("Trained model status is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(ValueTypeDTO.class.getSimpleName(), errors);
        }
    }
}
