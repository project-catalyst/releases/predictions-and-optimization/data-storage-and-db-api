package ro.tuc.dsrl.dbapi.model.dto.ui.optimization;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class OptimizationPageDTO {

    private int startHour;
    private double cost;
    private List<Double> chargeBatteryValues;
    private List<Double> dischargeBatteryValues;
    private List<Double> chargeTesValues;
    private List<Double> dischargeTesValues;
    private List<Double> coolingValues;
    private List<Double> relocateValues;
    private List<Double> hostValues;

    private List<Double> consumptionDTValues;
    private List<Double> consumptionRTValues;
    private List<Double> providedEnergyValues;
    private List<Double> renewableEnergyValues;

    private List<Double> requestedProfileValues;
    private List<Double> refrencePrices;
    private List<Double> predictedDemandValues;
    private List<Double> optimizedProfileValues;
    private int length;

    public OptimizationPageDTO(int sizeID) {
        this.length = sizeID;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public List<Double> getChargeBatteryValues() {

        return chargeBatteryValues;
    }

    public List<Double> getRelocateValues() {
        return relocateValues;
    }

    public void setRelocateValues(List<Double> relocateValues) {
        assertTrue("relocateValues " + relocateValues.size(), relocateValues.size() == length);
        this.relocateValues = relocateValues;
    }

    public void setChargeBatteryValues(List<Double> chargeBatteryValues) {
        assertTrue("chargeBatteryValues " + chargeBatteryValues.size(), chargeBatteryValues.size() == length);
        this.chargeBatteryValues = chargeBatteryValues;
    }

    public List<Double> getDischargeBatteryValues() {
        return dischargeBatteryValues;
    }

    public void setDischargeBatteryValues(List<Double> dischargeBatteryValues) {
        assertTrue("dischargeBatteryValues " + dischargeBatteryValues.size(),
                dischargeBatteryValues.size() == length);
        this.dischargeBatteryValues = dischargeBatteryValues;
    }

    public List<Double> getChargeTesValues() {
        return chargeTesValues;
    }

    public void setChargeTesValues(List<Double> chargeTesValues) {
        assertTrue("chargeTesValues " + chargeTesValues.size(), chargeTesValues.size() == length);
        this.chargeTesValues = chargeTesValues;
    }

    public List<Double> getDischargeTesValues() {
        return dischargeTesValues;
    }

    public List<Double> getConsumptionDTValues() {
        return consumptionDTValues;
    }

    public void setConsumptionDTValues(List<Double> consumptionDTValues) {
        assertTrue("consumptionDTValues " + consumptionDTValues.size(), consumptionDTValues.size() == length);
        this.consumptionDTValues = consumptionDTValues;
    }

    public List<Double> getConsumptionRTValues() {
        return consumptionRTValues;
    }

    public void setConsumptionRTValues(List<Double> consumptionRTValues) {
        assertTrue("consumptionRTValues " + consumptionRTValues.size(), consumptionRTValues.size() == length);
        this.consumptionRTValues = consumptionRTValues;
    }

    public List<Double> getProvidedEnergyValues() {
        return providedEnergyValues;
    }

    public void setProvidedEnergyValues(List<Double> providedEnergyValues) {
        assertTrue("productionValues " + providedEnergyValues.size(), providedEnergyValues.size() == length);
        this.providedEnergyValues = providedEnergyValues;
    }

    public List<Double> getRequestedProfileValues() {
        return requestedProfileValues;
    }

    public void setRequestedProfileValues(List<Double> requestedProfileValues) {
        assertTrue("requestedProfileValues " + requestedProfileValues.size(),
                requestedProfileValues.size() == length);
        this.requestedProfileValues = requestedProfileValues;
    }

    public List<Double> getPredictedDemandValues() {
        return predictedDemandValues;
    }

    public void setPredictedDemandValues(List<Double> predictedDemandValues) {
        assertTrue("predictedDemandValues " + predictedDemandValues.size(), predictedDemandValues.size() == length);
        this.predictedDemandValues = predictedDemandValues;
    }

    public List<Double> getOptimizedProfileValues() {
        return optimizedProfileValues;
    }

    public void setOptimizedProfileValues(List<Double> optimizedProfileValues) {
        assertTrue("optimizedProfileValues " + optimizedProfileValues.size(),
                optimizedProfileValues.size() == length);
        this.optimizedProfileValues = optimizedProfileValues;
    }

    public void setDischargeTesValues(List<Double> dischargeTesValues) {
        assertTrue("dischargeTesValues " + dischargeTesValues.size(), dischargeTesValues.size() == length);
        this.dischargeTesValues = dischargeTesValues;
    }

    public List<Double> getCoolingValues() {
        return coolingValues;
    }

    public void setCoolingValues(List<Double> coolingValues) {
        assertTrue("coolingValues " + coolingValues.size(), coolingValues.size() == length);
        this.coolingValues = coolingValues;
    }

    public List<Double> getHostValues() {
        return hostValues;
    }

    public void setHostValues(List<Double> hostValues) {
        assertTrue("hostLoadValues " + hostValues.size(), hostValues.size() == length);
        this.hostValues = hostValues;
    }

    public List<Double> getRefrencePrices() {
        return refrencePrices;
    }

    public void setRefrencePrices(List<Double> refrencePrices) {
        assertTrue("refrencePrices " + refrencePrices.size(), refrencePrices.size() == length);
        this.refrencePrices = refrencePrices;

    }

    public List<Double> getRenewableEnergyValues() {
        return renewableEnergyValues;
    }

    public void setRenewableEnergyValues(List<Double> renewableEnergyValues) {
        assertTrue("renewableValues " + renewableEnergyValues.size(), renewableEnergyValues.size() == length);
        this.renewableEnergyValues = renewableEnergyValues;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}