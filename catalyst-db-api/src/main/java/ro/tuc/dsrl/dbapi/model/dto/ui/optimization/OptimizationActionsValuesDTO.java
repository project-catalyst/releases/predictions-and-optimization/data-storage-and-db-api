package ro.tuc.dsrl.dbapi.model.dto.ui.optimization;

import java.util.List;

public class OptimizationActionsValuesDTO {

    private List<Double> chargeBatteryValues;
    private List<Double> dischargeBatteryValues;
    private List<Double> chargeTesValues;
    private List<Double> dischargeTesValues;
    private List<Double> coolingValues;
    private List<Double> relocateValues;
    private List<Double> hostValues;

    private List<Double> consumptionDTValues;
    private List<Double> consumptionRTValues;

    public OptimizationActionsValuesDTO(){}
    public OptimizationActionsValuesDTO(List<Double> chargeBatteryValues, List<Double> dischargeBatteryValues, List<Double> chargeTesValues, List<Double> dischargeTesValues, List<Double> coolingValues, List<Double> relocateValues, List<Double> hostValues, List<Double> consumptionDTValues, List<Double> consumptionRTValues) {
        this.chargeBatteryValues = chargeBatteryValues;
        this.dischargeBatteryValues = dischargeBatteryValues;
        this.chargeTesValues = chargeTesValues;
        this.dischargeTesValues = dischargeTesValues;
        this.coolingValues = coolingValues;
        this.relocateValues = relocateValues;
        this.hostValues = hostValues;
        this.consumptionDTValues = consumptionDTValues;
        this.consumptionRTValues = consumptionRTValues;
    }

    public List<Double> getChargeBatteryValues() {
        return chargeBatteryValues;
    }

    public void setChargeBatteryValues(List<Double> chargeBatteryValues) {
        this.chargeBatteryValues = chargeBatteryValues;
    }

    public List<Double> getDischargeBatteryValues() {
        return dischargeBatteryValues;
    }

    public void setDischargeBatteryValues(List<Double> dischargeBatteryValues) {
        this.dischargeBatteryValues = dischargeBatteryValues;
    }

    public List<Double> getChargeTesValues() {
        return chargeTesValues;
    }

    public void setChargeTesValues(List<Double> chargeTesValues) {
        this.chargeTesValues = chargeTesValues;
    }

    public List<Double> getDischargeTesValues() {
        return dischargeTesValues;
    }

    public void setDischargeTesValues(List<Double> dischargeTesValues) {
        this.dischargeTesValues = dischargeTesValues;
    }

    public List<Double> getCoolingValues() {
        return coolingValues;
    }

    public void setCoolingValues(List<Double> coolingValues) {
        this.coolingValues = coolingValues;
    }

    public List<Double> getRelocateValues() {
        return relocateValues;
    }

    public void setRelocateValues(List<Double> relocateValues) {
        this.relocateValues = relocateValues;
    }

    public List<Double> getHostValues() {
        return hostValues;
    }

    public void setHostValues(List<Double> hostValues) {
        this.hostValues = hostValues;
    }

    public List<Double> getConsumptionDTValues() {
        return consumptionDTValues;
    }

    public void setConsumptionDTValues(List<Double> consumptionDTValues) {
        this.consumptionDTValues = consumptionDTValues;
    }

    public List<Double> getConsumptionRTValues() {
        return consumptionRTValues;
    }

    public void setConsumptionRTValues(List<Double> consumptionRTValues) {
        this.consumptionRTValues = consumptionRTValues;
    }
}
