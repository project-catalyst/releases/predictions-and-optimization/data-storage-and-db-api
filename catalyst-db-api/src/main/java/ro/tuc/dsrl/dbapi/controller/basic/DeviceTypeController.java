package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceTypeDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.DeviceTypeService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/device-type")
public class DeviceTypeController {
    private final DeviceTypeService deviceTypeService;

    @Autowired
    public DeviceTypeController(DeviceTypeService deviceTypeService) {
        this.deviceTypeService = deviceTypeService;
    }

    @GetMapping(value = "/byId/{deviceTypeId}")
    public DeviceTypeDTO findById(@PathVariable("deviceTypeId") String deviceTypeId) {

        UUIDPreconditions.validate(deviceTypeId);

        UUID deviceTypeUUID = UUID.fromString(deviceTypeId);
        return deviceTypeService.findById(deviceTypeUUID);
    }

    @GetMapping(value = "/all")
    public List<DeviceTypeDTO> findAll() {
        return deviceTypeService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertDeviceTypeDTO(@RequestBody DeviceTypeDTO deviceTypeDTO) {
        return deviceTypeService.insert(deviceTypeDTO);
    }
}
