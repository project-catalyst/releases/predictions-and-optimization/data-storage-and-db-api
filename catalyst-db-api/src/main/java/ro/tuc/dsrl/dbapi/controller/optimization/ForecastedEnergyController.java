package ro.tuc.dsrl.dbapi.controller.optimization;

import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/energy-consumption")
public class ForecastedEnergyController {


    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;

    public ForecastedEnergyController(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
    }

    @GetMapping(value= "/dayahead/dt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregeddayAheadDT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
    }

    @GetMapping(value= "/dayahead/rt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregeddayAheadRT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
    }


    @GetMapping(value= "/intraday/rt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregedIntradayRT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
    }


    @GetMapping(value= "/intraday/dt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregedIntradayDT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
    }




}
