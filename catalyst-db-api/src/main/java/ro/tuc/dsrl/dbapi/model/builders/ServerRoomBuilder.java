package ro.tuc.dsrl.dbapi.model.builders;

import ro.tuc.dsrl.catalyst.model.dto.ServerRoomDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.DeviceType;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;

import java.util.ArrayList;
import java.util.List;

public class ServerRoomBuilder {

    public static List<ServerRoomDTO> toDTOList(List<ServerRoom> serverRoomsList) {

        List<ServerRoomDTO> serverRoomDTOList = new ArrayList<>();
        for(ServerRoom s: serverRoomsList){
            serverRoomDTOList.add(new ServerRoomDTO(s.getMaxEnergyConsumption(),
                    s.getEdPercentage(),
                    s.getMaxHostLoad(),
                    s.getMaxReallocLoad(),
                    s.getItDT(),
                    s.getItRT()));
        }

        return serverRoomDTOList;
    }
}
