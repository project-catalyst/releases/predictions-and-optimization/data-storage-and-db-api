package ro.tuc.dsrl.dbapi.controller.uisupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.model.dto.ui.charts.GraphDataDTO;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.uisupport.FlexibilityGraphDataService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/graph-data/flexibility")
public class FlexibilityGraphDataController {

    @Value("${datacenter.name}")
    private String dataCenterName;

    private final FlexibilityGraphDataService flexibilityGraphDataService;
    private final DeviceService deviceService;

    @Autowired
    public FlexibilityGraphDataController(FlexibilityGraphDataService flexibilityGraphDataService,
                                          DeviceService deviceService) {
        this.flexibilityGraphDataService = flexibilityGraphDataService;
        this.deviceService = deviceService;
    }

    @GetMapping(value = "/dayahead/{startTime}")
    public GraphDataDTO getPredictedFlexibilityOfConsumptionForDC(
            @PathVariable("startTime") Long startTime) {

        UUID dataCenterID = deviceService.getDeviceIdFromLabel(dataCenterName);

        List<String> errors = EnergyProfilePreconditions.validate(dataCenterID.toString(), startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);

        return flexibilityGraphDataService.getPredictedFlexibilityForDC(
                dataCenterID,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                EnergyType.ELECTRICAL.getType());
    }

}
