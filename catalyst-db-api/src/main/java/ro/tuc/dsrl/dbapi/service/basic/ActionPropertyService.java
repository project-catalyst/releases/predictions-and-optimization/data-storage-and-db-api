//package ro.tuc.dsrl.dbapi.service.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import ro.tuc.dsrl.catalyst.model.dto.ActionPropertyDTO;
//import ro.tuc.dsrl.catalyst.model.error_handler.EntityValidationException;
//import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
//import ro.tuc.dsrl.dbapi.model.*;
//import ro.tuc.dsrl.dbapi.model.builders.ActionPropertyBuilder;
//import ro.tuc.dsrl.dbapi.repository.ActionPropertyRepository;
//import ro.tuc.dsrl.dbapi.repository.ActionTypeRepository;
//import ro.tuc.dsrl.dbapi.repository.MeasureUnitRepository;
//import ro.tuc.dsrl.dbapi.repository.ValueTypeRepository;
//import ro.tuc.dsrl.dbapi.service.validators.ActionPropertyValidator;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//import java.util.stream.Collectors;
//
//@Service
//public class ActionPropertyService {
//
//    private final ActionPropertyRepository actionPropertyRepository;
//    private final MeasureUnitRepository measureUnitRepository;
//    private final ValueTypeRepository valueTypeRepository;
//    private final ActionTypeRepository actionTypeRepository;
//
//    @Autowired
//    public ActionPropertyService(ActionPropertyRepository actionPropertyRepository, MeasureUnitRepository measureUnitRepository, ValueTypeRepository valueTypeRepository, ActionTypeRepository actionTypeRepository) {
//        this.actionPropertyRepository = actionPropertyRepository;
//        this.measureUnitRepository = measureUnitRepository;
//        this.valueTypeRepository = valueTypeRepository;
//        this.actionTypeRepository = actionTypeRepository;
//    }
//
//    public List<ActionPropertyDTO> findAll() {
//
//        List<ActionProperty> actionProperties = actionPropertyRepository.findAll();
//
//        return actionProperties.stream()
//                .map(ap -> ActionPropertyBuilder.generateDTOFromEntity(
//                        ap,
//                        ap.getMeasureUnitId(),
//                        ap.getValueTypeId(),
//                        ap.getActionTypeId()
//                ))
//                .collect(Collectors.toList());
//    }
//
//    public UUID insert(ActionPropertyDTO actionPropertyDTO) {
//
//        ActionPropertyValidator.validateInsert(actionPropertyDTO);
//
//        List<String> errors = new ArrayList<>();
//        MeasureUnit measureUnit = measureUnitRepository.findByUUID(actionPropertyDTO.getMeasureUnitId());
//        if (measureUnit == null) {
//            errors.add("Measure unit was not found in database.");
//        }
//
//        ValueType valueType = valueTypeRepository.findByUUId(actionPropertyDTO.getValueTypeId());
//        if (valueType == null) {
//            errors.add("Value Type was not found in database.");
//        }
//
//        ActionType actionType = actionTypeRepository.findByUUID(actionPropertyDTO.getActionTypeId());
//        if (actionType == null) {
//            errors.add("Action Type was not found in database.");
//        }
//
//        if (!errors.isEmpty()) {
//            throw new EntityValidationException(ActionInstance.class.getSimpleName(), errors);
//        }
//
//        return actionPropertyRepository
//                .save(ActionPropertyBuilder.generateEntityFromDTO(actionPropertyDTO, measureUnit, valueType, actionType))
//                .getId();
//    }
//
//    public ActionPropertyDTO findById(UUID actionPropertyUUID) {
//
//        ActionProperty actionProperty = actionPropertyRepository.findByUUID(actionPropertyUUID);
//        if (actionProperty == null)
//            throw new ResourceNotFoundException("ActionProperty with ID " + actionPropertyUUID);
//
//        return ActionPropertyBuilder.generateDTOFromEntity(
//                actionProperty,
//                actionProperty.getMeasureUnitId(),
//                actionProperty.getValueTypeId(),
//                actionProperty.getActionTypeId());
//
//    }
//}
