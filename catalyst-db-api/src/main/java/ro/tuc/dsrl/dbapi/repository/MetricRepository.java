package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.Metric;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface MetricRepository extends JpaRepository<Metric, UUID> {

    @Query(value = "SELECT m " +
            "FROM Metric m " +
            "JOIN FETCH m.device " +
            "JOIN FETCH m.measurement " +
            "LEFT JOIN FETCH m.plan " +
            "WHERE m.timestamp >= :startdate " +
            "AND m.device.label like :device ")
    List<Metric> findByDeviceAfterDate(@Param("startdate") LocalDateTime startDate, @Param("device") String device);

    @Query(value = "SELECT m " +
            "FROM Metric m " +
            "JOIN FETCH m.device d " +
            "JOIN FETCH m.measurement mes " +
            "LEFT JOIN FETCH m.plan p " +
            "WHERE p.start <= :startdate and p.end >= :startdate " +
            "AND m.device.label like :dc "+
            "AND p.timeframe like CONCAT('%',:timeframe,'%') ")
    List<Metric> findByDCandPlanTiemframe(@Param("startdate") LocalDateTime startDate, @Param("dc") String dc , @Param("timeframe") String timeframe);


    @Query(value = "SELECT m " +
            "FROM Metric m " +
            "JOIN FETCH m.device d " +
            "JOIN FETCH m.measurement mes " +
            "LEFT JOIN FETCH m.plan p " +
            "WHERE p.start <= :startdate and p.end >= :startdate " +
            "AND m.device.label like :dc "+
            "AND p.timeframe is NULL ")
    List<Metric> findByDCandPlanTiemframe(@Param("startdate") LocalDateTime startDate, @Param("dc") String dc);

    @Query(value = "SELECT m " +
            "FROM Metric m " +
            "JOIN FETCH m.device d " +
            "JOIN FETCH m.measurement mes " +
            "LEFT JOIN FETCH m.plan p " +
            "WHERE m.device.label like :dc")
    List<Metric> findByDC(@Param("dc") String dc);
}
