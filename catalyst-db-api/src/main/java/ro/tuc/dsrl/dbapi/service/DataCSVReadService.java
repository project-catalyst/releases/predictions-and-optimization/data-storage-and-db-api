package ro.tuc.dsrl.dbapi.service;

import com.opencsv.CSVReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictedValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.dbapi.service.basic.*;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.CoolingSystem;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class DataCSVReadService {

    private static final Log LOGGER = LogFactory.getLog(DataCSVReadService.class);

    private final DeviceService deviceService;
    private final MeasurementService measurementService;
    private final MonitoredValueService monitoredValueService;

   // private final ActionPropertyService actionPropertyService;
    private final PredictedValueService predictedValueService;
    private final PredictionJobService predictionJobService;

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String FILE_READ_ERROR = "Error reading files";

    @Autowired
    public DataCSVReadService(DeviceService deviceService, MeasurementService measurementService, MonitoredValueService monitoredValueService,
                              PredictedValueService predictedValueService,
                              PredictionJobService predictionJobService) {
        this.deviceService = deviceService;
        this.measurementService = measurementService;
        this.monitoredValueService = monitoredValueService;
        this.predictedValueService = predictedValueService;
        this.predictionJobService = predictionJobService;
    }
//
//    public void readMonitoredValuesFromQarnot() {
//
//        Path folderPath = Paths.get("D://ResearchProjects//Catalyst//CODE//catalyst-db-api//src//main//resources//qarnot//");
//
//        // retrieve a list of the files in the folder
//        List<String> fileNames = new ArrayList<>();
//        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
//            for (Path path : directoryStream) {
//                fileNames.add(path.toString());
//            }
//        } catch (IOException ex) {
//            LOGGER.error(FILE_READ_ERROR);
//        }
//
//        for (String fileEntry : fileNames) {
//
//            try (CSVReader reader = new CSVReader(new FileReader(fileEntry))) {
//                LOGGER.info(fileEntry);
//                String[] line;
//                String[] headerLine = reader.readNext();
//                LOGGER.info(headerLine[0] + " " + headerLine[1] + " " + headerLine[2] + " " + headerLine[3]);
//                while ((line = reader.readNext()) != null) {
//                    LOGGER.info("qguid = " + line[0] +
//                            " , time = " + line[1] +
//                            " , additional_heater_on = " + line[2] +
//                            " , ambient_temperature = " + line[3] +
//                            " , ambient_temperature_offset = " + line[4] +
//                            " , children_nodes = " + line[5] +
//                            " , heatsink_temperature = " + line[6] +
//                            " , power_consumption_aux = " + line[7] +
//                            " , power_consumption_aux_watt_hour = " + line[8] +
//                            " , power_consumption_main = " + line[9] +
//                            " , power_consumption_main_watt_hour = " + line[10] +
//                            " , power_instant_aux = " + line[11] +
//                            " , power_instant_main = " + line[12] +
//                            " , target_temperature = " + line[13] +
//                            "]");
//
//                    // Insert Monitored Value in Mysql
//                    UUID deviceId = deviceService.getDeviceIdFromLabel("Server_Qarnot");
//                    UUID measurementId = measurementService.getMeasurementByProperty("SERVER_ELECTRICAL_POWER");
//                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//                    MonitoredValueDTO monitoredValue = new MonitoredValueDTO(UUID.randomUUID(),
//                            LocalDateTime.parse(line[1], formatter),
//                            Double.parseDouble(line[10]),
//                            deviceId,
//                            measurementId);
//                    monitoredValueService.insert(monitoredValue);
//
//                }
//            } catch (IOException e) {
//                LOGGER.error(e);
//            }
//        }
//    }
//
//
//    public List<MonitoredValueDTO> readMonitoredValuesFromPaper() throws IOException {
//
//        List<MonitoredValueDTO> monitoredValueDTOList = new ArrayList<>();
//
//        Path folderPath = Paths.get("D://Work//all_together");
//
//        LocalDateTime timestamp = LocalDateTime.parse("2019-04-01 00:00:00",
//                DateTimeFormatter.ofPattern(DATE_FORMAT));
//
//        // retrieve a list of the files in the folder
//        List<String> fileNames = new ArrayList<>();
//        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
//            for (Path path : directoryStream) {
//                fileNames.add(path.toString());
//            }
//        } catch (IOException ex) {
//            LOGGER.error(FILE_READ_ERROR);
//        }
//
//        LOGGER.info(fileNames);
//
//        for (String fileEntry : fileNames) {
//
//            try (CSVReader reader = new CSVReader(new FileReader(fileEntry))) {
//
//                LOGGER.info(fileEntry);
//                String[] line;
//                String[] headerLine = reader.readNext();
//                LOGGER.info(headerLine[0] + " " + headerLine[1] + " " + headerLine[2]);
//                LOGGER.info(fileEntry);
//                while ((line = reader.readNext()) != null) {
//
//                    timestamp = timestamp.plusMinutes(10);
//
//                    // Insert Monitored Value in Mysql
//                    UUID deviceIdServer = deviceService.getDeviceIdFromLabel("SERVER_ROOM_PAPER");
//                    UUID measurementIdServer = measurementService.getMeasurementByProperty("ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM");
//                    UUID deviceIdCooling = deviceService.getDeviceIdFromLabel("COOLING_SYSTEM_PAPER");
//                    UUID measurementIdCooling = measurementService.getMeasurementByProperty("ELECTRICAL_POWER_CONSUMPTION_COOLING_SYSTEM");
//                    MonitoredValueDTO monitoredValueServer = new MonitoredValueDTO(UUID.randomUUID(),
//                            timestamp,
//                            Double.parseDouble(line[1]),
//                            deviceIdServer,
//                            measurementIdServer);
//                    MonitoredValueDTO monitoredValueCooling = new MonitoredValueDTO(UUID.randomUUID(),
//                            timestamp,
//                            Double.parseDouble(line[2]),
//                            deviceIdCooling,
//                            measurementIdCooling);
//
//                    monitoredValueService.insert(monitoredValueServer);
//                    monitoredValueService.insert(monitoredValueCooling);
//
//                }
//            }
//        }
//
//        return monitoredValueDTOList;
//    }
//
//    public LocalDateTime convert(String str) {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
//        return LocalDateTime.parse(str, formatter);
//    }
//
//
//    public void readActionProperty() {
//
//        Path folderPath = Paths.get("/home/vlad/Documents/CatalystCSV/ActionProperty");
//
//        List<String> fileNames = new ArrayList<>();
//
//        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
//            for (Path path : directoryStream) {
//                fileNames.add(path.toString());
//            }
//        } catch (IOException ex) {
//            LOGGER.error(FILE_READ_ERROR);
//        }
//
//        for (String fileEntry : fileNames) {
//
//            try (CSVReader reader = new CSVReader(new FileReader(fileEntry))) {
//                LOGGER.info(fileEntry);
//                String[] line;
//                String[] headerLine = reader.readNext();
//                LOGGER.info(headerLine[0] + " " + headerLine[1] + " " + headerLine[2] + " " + headerLine[3] + " " + headerLine[4] + " " + headerLine[5]
//                        + " " + headerLine[6] + " " + headerLine[7] + " " + headerLine[8]);
//                while ((line = reader.readNext()) != null) {
//                    LOGGER.info("id = " + line[0] +
//                            " , property = " + line[1] +
//                            " , observation = " + line[2] +
//                            " , measure_unit = " + line[3] +
//                            " , measure_unit_id = " + line[4] +
//                            " , value_type = " + line[5] +
//                            " , value_type_id = " + line[6] +
//                            " , action_type = " + line[7] +
//                            " , action_type_id = " + line[8] +
//                            "]");
//
////                    // Insert Action Property in Mysql
//                    ActionPropertyDTO actionPropertyDTO = new ActionPropertyDTO(
//                            UUID.randomUUID(),
//                            line[1] != null && !line[1].equals("") ? line[1] : "EMPTY",
//                            line[2] != null && !line[2].equals("") ? line[2] : "EMPTY",
//                            UUID.fromString(line[4]),
//                            UUID.fromString(line[6]),
//                            UUID.fromString(line[8]));
//
//                    this.actionPropertyService.insert(actionPropertyDTO);
//                }
//            } catch (IOException e) {
//                LOGGER.error(e);
//            }
//        }
//
//
//    }
//
//    public List<MonitoredValueDTO> populateDbMonitoredValuesPoznan() throws IOException {
//
//        List<MonitoredValueDTO> monitoredValueDTOList = new ArrayList<>();
//
//        Path folderPath = Paths.get("D://Work//Poznan//poznan-data//");
//
//        LocalDateTime timestamp = LocalDateTime.parse("2018-09-01 00:00:00",
//                DateTimeFormatter.ofPattern(DATE_FORMAT));
//
//        // retrieve a list of the files in the folder
//        List<String> fileNames = new ArrayList<>();
//        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
//            for (Path path : directoryStream) {
//                fileNames.add(path.toString());
//            }
//        } catch (IOException ex) {
//            LOGGER.error(FILE_READ_ERROR);
//        }
//
//        LOGGER.info(fileNames);
//
//        for (String fileEntry : fileNames) {
//
//            try (CSVReader reader = new CSVReader(new FileReader(fileEntry))) {
//
//                LOGGER.info(fileEntry);
//                String[] line;
//                String[] headerLine = reader.readNext();
//                LOGGER.info(headerLine[0] + " " + headerLine[1] + " " + headerLine[2]);
//                LOGGER.info(fileEntry);
//                while ((line = reader.readNext()) != null) {
//                    // Insert Monitored Value in Mysql
//                    UUID deviceIdServer = deviceService.getDeviceIdFromLabel("SERVER_ROOM_POZNAN");
//                    UUID measurementIdServer = measurementService.getMeasurementByProperty("ELECTRICAL_POWER_CONSUMPTION_SERVER_ROOM");
//                    UUID deviceIdCooling = deviceService.getDeviceIdFromLabel("COOLING_SYSTEM_POZNAN");
//                    UUID measurementIdCooling = measurementService.getMeasurementByProperty("ELECTRICAL_POWER_CONSUMPTION_COOLING_SYSTEM");
//                    MonitoredValueDTO monitoredValueServer = new MonitoredValueDTO(UUID.randomUUID(),
//                            timestamp,
//                            Double.parseDouble(line[1]),
//                            deviceIdServer,
//                            measurementIdServer);
//                    MonitoredValueDTO monitoredValueCooling = new MonitoredValueDTO(UUID.randomUUID(),
//                            timestamp,
//                            Double.parseDouble(line[2]),
//                            deviceIdCooling,
//                            measurementIdCooling);
//
//                    monitoredValueService.insert(monitoredValueServer);
//                    monitoredValueService.insert(monitoredValueCooling);
//
//                    timestamp = timestamp.plusMinutes(30);
//                }
//            }
//        }
//
//        return monitoredValueDTOList;
//    }
//
//    public void readHistoryAndPredicted() {
//
//        Path folderPath = Paths.get("/home/daniel/Desktop/Catalyst-demo");
//
//        List<String> fileNames = new ArrayList<>();
//
//        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
//            for (Path path : directoryStream) {
//                fileNames.add(path.toString());
//            }
//        } catch (IOException ex) {
//            LOGGER.error(FILE_READ_ERROR);
//        }
//
//        for (String fileEntry : fileNames) {
//
//            try (CSVReader reader = new CSVReader(new FileReader(fileEntry))) {
//                LOGGER.info(fileEntry);
//                String[] line;
//                String[] headerLine = reader.readNext();
//                LOGGER.info(headerLine[0] + " " + headerLine[1] + " " + headerLine[2] + " " + headerLine[3] + " " + headerLine[4] + " " + headerLine[5]
//                        + " " + headerLine[6] + " " + headerLine[7] + " " + headerLine[8] + " " + headerLine[9] + " " + headerLine[10]);
//                while ((line = reader.readNext()) != null) {
//                    LOGGER.info("time = " + line[0] +
//                            " , historicalTotalDC = " + line[1] +
//                            " , historicalIT = " + line[2] +
//                            " , historicalRT = " + line[3] +
//                            " , historicalDT = " + line[4] +
//                            " , histricalCooling = " + line[5] +
//                            " , predictedTotalDC = " + line[6] +
//                            " , predictedIT = " + line[7] +
//                            " , predictedRT = " + line[8] +
//                            " , predictedDT = " + line[9] +
//                            " , predictedCooling = " + line[10] +
//                            "]");
//
//                    //insert historical data
//                    UUID electicalPowerConsumptionServerRoom = UUID.fromString("D2973A70-4FEF-4317-8389-9BAFD295DF16");
//                    UUID electricalPowerConsumptionCoolingSystem = UUID.fromString("6CF288C8-BA50-4B1D-9430-C83E13AF512D");
//                    UUID serverRoom = UUID.fromString("1B1DF5DD-6E50-48D2-A549-7AF0672A5A85");
//                    UUID coolingSystem = UUID.fromString("538BCA40-02E0-4530-A057-A03F707011C8");
//
//                    List<UUID> monitoredValuesUUID = new ArrayList<>();
//
//                    line[2] = line[2].replace(",", ".");
//                    line[3] = line[3].replace(",", ".");
//                    line[4] = line[4].replace(",", ".");
//                    line[5] = line[5].replace(",", ".");
//                    line[6] = line[6].replace(",", ".");
//                    line[7] = line[7].replace(",", ".");
//                    line[8] = line[8].replace(",", ".");
//                    line[9] = line[9].replace(",", ".");
//                    line[10] = line[10].replace(",", ".");
//
//                    MonitoredValueDTO monitoredValueDTO = new MonitoredValueDTO(
//                            UUID.randomUUID(),
//                            this.convert(line[0]),
//                            Double.parseDouble(line[2]),
//                            serverRoom,
//                            electicalPowerConsumptionServerRoom);
//
//                    monitoredValuesUUID.add(this.monitoredValueService.insert(monitoredValueDTO));
//
//                    monitoredValueDTO = new MonitoredValueDTO(
//                            UUID.randomUUID(),
//                            this.convert(line[0]),
//                            Double.parseDouble(line[5]),
//                            coolingSystem,
//                            electricalPowerConsumptionCoolingSystem);
//                    monitoredValuesUUID.add(this.monitoredValueService.insert(monitoredValueDTO));
//
//
//                    monitoredValueDTO = new MonitoredValueDTO(
//                            UUID.randomUUID(),
//                            this.convert(line[0]).plusMinutes(30),
//                            Double.parseDouble(line[2]),
//                            serverRoom,
//                            electicalPowerConsumptionServerRoom);
//
//                    monitoredValuesUUID.add(this.monitoredValueService.insert(monitoredValueDTO));
//
//                    monitoredValueDTO = new MonitoredValueDTO(
//                            UUID.randomUUID(),
//                            this.convert(line[0]).plusMinutes(30),
//                            Double.parseDouble(line[5]),
//                            coolingSystem,
//                            electricalPowerConsumptionCoolingSystem);
//
//                    monitoredValuesUUID.add(this.monitoredValueService.insert(monitoredValueDTO));
//
//                    //insert DAYAHEAD predictedValues
//
//                    UUID intradayID = UUID.fromString("8a59ce93-2f70-44b7-9509-5d3b7282d7c5");
//                    UUID dayaheadID = UUID.fromString("f8a61eb3-8753-4570-99e8-4a01d7a66790");
//
//                    UUID coolingSustemDeviceID = UUID.fromString("538BCA40-02E0-4530-A057-A03F707011C8");
//                    UUID serverRoomDeviceID = UUID.fromString("1B1DF5DD-6E50-48D2-A549-7AF0672A5A85");
//
//                    PredictedValueDTO predictedValueDTO = new PredictedValueDTO(
//                            UUID.randomUUID(),
//                            this.convert(line[0]),
//                            Double.parseDouble(line[7]),
//                            electicalPowerConsumptionServerRoom,
//                            serverRoomDeviceID,
//                            dayaheadID);
//                    this.predictedValueService.insert(predictedValueDTO);
//
//                    predictedValueDTO = new PredictedValueDTO(
//                            UUID.randomUUID(),
//                            this.convert(line[0]),
//                            Double.parseDouble(line[10]),
//                            electricalPowerConsumptionCoolingSystem,
//                            coolingSustemDeviceID,
//                            dayaheadID);
//                    this.predictedValueService.insert(predictedValueDTO);
//
//                    //insert intraDay predicted values
//                    if (this.convert(line[0]).getHour() <= 4)
//
//                        predictedValueDTO = new PredictedValueDTO(
//                                UUID.randomUUID(),
//                                this.convert(line[0]),
//                                Double.parseDouble(line[7]),
//                                electicalPowerConsumptionServerRoom,
//                                serverRoomDeviceID,
//                                intradayID);
//
//                    this.predictedValueService.insert(predictedValueDTO);
//
//                    predictedValueDTO = new PredictedValueDTO(
//                            UUID.randomUUID(),
//                            this.convert(line[0]),
//                            Double.parseDouble(line[10]),
//                            electricalPowerConsumptionCoolingSystem,
//                            coolingSustemDeviceID,
//                            intradayID);
//
//                    this.predictedValueService.insert(predictedValueDTO);
//
//                    if (this.convert(line[0]).getHour() < 4) {
//                        predictedValueDTO = new PredictedValueDTO(
//                                UUID.randomUUID(),
//                                this.convert(line[0]).plusMinutes(30),
//                                Double.parseDouble(line[7]),
//                                electicalPowerConsumptionServerRoom,
//                                serverRoomDeviceID,
//                                intradayID);
//
//
//                        this.predictedValueService.insert(predictedValueDTO);
//
//                        predictedValueDTO = new PredictedValueDTO(
//                                UUID.randomUUID(),
//                                this.convert(line[0]).plusMinutes(30),
//                                Double.parseDouble(line[10]),
//                                electricalPowerConsumptionCoolingSystem,
//                                coolingSustemDeviceID,
//                                intradayID);
//
//                        this.predictedValueService.insert(predictedValueDTO);
//                    }
//
//                }
//            } catch (IOException e) {
//                LOGGER.error(e);
//            }
//        }
//    }
//

    private InputStream loadFile(String path) {
        try {
            return new ClassPathResource(path).getInputStream();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    public boolean populatePriceData(String path1, String dcName, LocalDateTime time, PredictionGranularity predGranurality, AggregationGranularity aggGranurality) {

        InputStream input = loadFile(path1);
        List<DeviceDTO> devices = deviceService.findByParentAndType(dcName, DeviceTypeEnum.MARKETPLACE);
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), time, predGranurality.getGranularity(), AlgorithmType.MARKET.name(), FlexibilityType.NONE.getType());
        UUID pjId = predictionJobService.insert(pjDTO);

        UUID ePriceMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.DC_E_PRICE.type());
        UUID lPriceMeasurementUID = measurementService.getMeasurementByProperty(MeasurementType.DC_L_PRICE.type());
        UUID tPriceMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.DC_T_PRICE.type());
        UUID dPriceMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.DC_DR_PRICE.type());

        List<EnergySampleDTO> ePrices = new ArrayList<>();
        List<EnergySampleDTO> lPrices = new ArrayList<>();
        List<EnergySampleDTO> tPrices = new ArrayList<>();
        List<EnergySampleDTO> drPrices = new ArrayList<>();

        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(aggGranurality);
        eEnergyPriceProfile.setPredictionGranularity(predGranurality);
        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_PRICE);
        eEnergyPriceProfile.setMeasurementId(ePriceMeasurementUUID);
        eEnergyPriceProfile.setDeviceId(device.getId());


        EnergyProfileDTO tEnergyPriceProfile = new EnergyProfileDTO();
        tEnergyPriceProfile.setAggregationGranularity(aggGranurality);
        tEnergyPriceProfile.setPredictionGranularity(predGranurality);
        tEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_PRICE);
        tEnergyPriceProfile.setMeasurementId(tPriceMeasurementUUID);
        tEnergyPriceProfile.setDeviceId(device.getId());


        EnergyProfileDTO lEnergyPriceProfile = new EnergyProfileDTO();
        lEnergyPriceProfile.setAggregationGranularity(aggGranurality);
        lEnergyPriceProfile.setPredictionGranularity(predGranurality);
        lEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_PRICE);
        lEnergyPriceProfile.setMeasurementId(lPriceMeasurementUID);
        lEnergyPriceProfile.setDeviceId(device.getId());


        EnergyProfileDTO dEnergyPriceProfile = new EnergyProfileDTO();
        dEnergyPriceProfile.setAggregationGranularity(aggGranurality);
        dEnergyPriceProfile.setPredictionGranularity(predGranurality);
        dEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_PRICE);
        dEnergyPriceProfile.setMeasurementId(dPriceMeasurementUUID);
        dEnergyPriceProfile.setDeviceId(device.getId());


        try {
            HSSFWorkbook workbook = new HSSFWorkbook(input);

            HSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                double ePriceValue = nextCell(cellIterator);
                double lPriceValue = nextCell(cellIterator);
                double tPriceValue = nextCell(cellIterator);
                double drPriceValue = nextCell(cellIterator);
                if (ePriceValue != -1 && lPriceValue != -1 && tPriceValue != -1 && drPriceValue != 1) {

                    EnergySampleDTO ePrice = new EnergySampleDTO(ePriceValue, time);
                    EnergySampleDTO lPrice = new EnergySampleDTO(lPriceValue, time);
                    EnergySampleDTO tPrice = new EnergySampleDTO(tPriceValue, time);
                    EnergySampleDTO drPrice = new EnergySampleDTO(drPriceValue, time);

                    ePrices.add(ePrice);
                    lPrices.add(lPrice);
                    tPrices.add(tPrice);
                    drPrices.add(drPrice);
                    time = time.plus(aggGranurality.getGranularity(), ChronoUnit.MINUTES);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }


        eEnergyPriceProfile.setCurve(ePrices);
        lEnergyPriceProfile.setCurve(lPrices);
        tEnergyPriceProfile.setCurve(tPrices);
        dEnergyPriceProfile.setCurve(drPrices);

        boolean inserted1 = predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);
        boolean inserted2 = predictedValueService.insertPredictedEnergyCurve(lEnergyPriceProfile, pjId);
        boolean inserted3 = predictedValueService.insertPredictedEnergyCurve(dEnergyPriceProfile, pjId);
        boolean inserted4 = predictedValueService.insertPredictedEnergyCurve(tEnergyPriceProfile, pjId);

        if (!(inserted1 && inserted2 && inserted3 && inserted4)) {
            LOGGER.error("BASELINE WAS NOT INSERTED " + predGranurality.getGranularity());
        }
        return inserted1 && inserted2 && inserted3 && inserted4;

    }

    public boolean populateProductionProfiles(String path1, String dcName, LocalDateTime time) {

        InputStream input = loadFile(path1);
        List<DeviceDTO> devices = deviceService.findByParentAndType(dcName, DeviceTypeEnum.DATACENTER);
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        UUID prodMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE.type());

        List<MonitoredValueDTO> prodDTOs = new ArrayList<>();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(input);

            HSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                LOGGER.info(nextCell(cellIterator));
                double eR = nextCell(cellIterator);

                if (eR != -1) {
                    MonitoredValueDTO mvRT = new MonitoredValueDTO(time, eR, device.getId(), prodMeasurementUUID);
                    prodDTOs.add(mvRT);
                    time = time.plus(5, ChronoUnit.MINUTES);

                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        monitoredValueService.insertMore(prodDTOs);

        return true;
    }


    public boolean populatePreOptimizationProfiles(String path1, String dcName, LocalDateTime time) {

        InputStream input = loadFile(path1);
        List<CoolingSystem> cs = deviceService.getCoolingSystem(dcName, time);
        double cop = (!cs.isEmpty()) ? cs.get(0).getCopC() : 3.0;
        List<DeviceDTO> devices = deviceService.findByParentAndType(dcName, DeviceTypeEnum.SERVER_ROOM);
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        UUID dtMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION.type());
        UUID rtMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());

        List<MonitoredValueDTO> rtDTOs = new ArrayList<>();
        List<MonitoredValueDTO> dtDTOs = new ArrayList<>();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(input);

            HSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                LOGGER.info(nextCell(cellIterator));
                LOGGER.info(nextCell(cellIterator));
                double eR = nextCell(cellIterator);
                LOGGER.info(nextCell(cellIterator));
                double eD = nextCell(cellIterator);

                if (eR != -1 && eD != -1) {
                    double cool = (eR + eD) / cop;

                    MonitoredValueDTO mvRT = new MonitoredValueDTO(time, eR, device.getId(), rtMeasurementUUID);
                    MonitoredValueDTO mvDT = new MonitoredValueDTO(time, eD, device.getId(), dtMeasurementUUID);
                    rtDTOs.add(mvRT);
                    dtDTOs.add(mvDT);
                    time = time.plus(5, ChronoUnit.MINUTES);

                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        monitoredValueService.insertMore(rtDTOs);
        monitoredValueService.insertMore(dtDTOs);

        return true;
    }

    public boolean populatePredictedData(String path1, String dcName, LocalDateTime time, PredictionGranularity predGranurality, AggregationGranularity aggGranurality, MeasurementType mType) {

        InputStream input = loadFile(path1);
        List<DeviceDTO> devices = deviceService.findByParentAndType(dcName, DeviceTypeEnum.SERVER_ROOM);
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), time, predGranurality.getGranularity(), AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
        UUID pjId = predictionJobService.insert(pjDTO);

        UUID ePriceMeasurementUUID = measurementService.getMeasurementByProperty(mType.type());

        List<EnergySampleDTO> ePredicted = new ArrayList<>();

        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(aggGranurality);
        eEnergyPriceProfile.setPredictionGranularity(predGranurality);
        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_CONSUMPTION);
        eEnergyPriceProfile.setMeasurementId(ePriceMeasurementUUID);
        eEnergyPriceProfile.setDeviceId(device.getId());


        try {
            HSSFWorkbook workbook = new HSSFWorkbook(input);

            HSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                double eValue = nextCell(cellIterator);
                if (eValue != -1) {

                    EnergySampleDTO e = new EnergySampleDTO(eValue, time);

                    ePredicted.add(e);
                    time = time.plus(aggGranurality.getGranularity(), ChronoUnit.MINUTES);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }


        eEnergyPriceProfile.setCurve(ePredicted);

        boolean inserted = predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);

        if (!inserted) {
            LOGGER.error("PREDICTIONS WERE NOT INSERTED " + predGranurality.getGranularity());
        }
        return inserted;

    }

    public boolean populatePredictedRenewableData(String path1, String dcName, LocalDateTime time, PredictionGranularity predGranurality, AggregationGranularity aggGranurality, MeasurementType mType) {

        InputStream input = loadFile(path1);
        List<DeviceDTO> devices = deviceService.findByParentAndType(dcName, DeviceTypeEnum.DATACENTER);
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), time, predGranurality.getGranularity(), AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
        UUID pjId = predictionJobService.insert(pjDTO);

        UUID ePriceMeasurementUUID = measurementService.getMeasurementByProperty(mType.type());

        List<EnergySampleDTO> ePredicted = new ArrayList<>();

        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(aggGranurality);
        eEnergyPriceProfile.setPredictionGranularity(predGranurality);
        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_PRODUCTION);
        eEnergyPriceProfile.setMeasurementId(ePriceMeasurementUUID);
        eEnergyPriceProfile.setDeviceId(device.getId());


        try {
            HSSFWorkbook workbook = new HSSFWorkbook(input);

            HSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                double eValue = nextCell(cellIterator);
                if (eValue != -1) {

                    EnergySampleDTO e = new EnergySampleDTO(eValue, time);

                    ePredicted.add(e);
                    time = time.plus(aggGranurality.getGranularity(), ChronoUnit.MINUTES);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }


        eEnergyPriceProfile.setCurve(ePredicted);

        boolean inserted = predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);

        if (!inserted) {
            LOGGER.error("RENEWABLE PREDICTIONS WERE NOT INSERTED " + predGranurality.getGranularity());
        }
        return inserted;

    }

    public boolean populateDRSIGNAL(String path, String deviceName, LocalDateTime time, PredictionGranularity predictionGranularity, AggregationGranularity aggregationGranularity) {

        InputStream input = loadFile(path);
        List<DeviceDTO> devices = deviceService.findByParentAndType(deviceName, DeviceTypeEnum.DATACENTER); //TODO check device type
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), time, predictionGranularity.getGranularity(), AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
        UUID pjId = predictionJobService.insert(pjDTO);

        UUID ePriceMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.DR_SIGNAL.type());

        List<EnergySampleDTO> ePredicted = new ArrayList<>();

        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(aggregationGranularity);
        eEnergyPriceProfile.setPredictionGranularity(predictionGranularity);
        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_BASELINE);
        eEnergyPriceProfile.setMeasurementId(ePriceMeasurementUUID);
        eEnergyPriceProfile.setDeviceId(device.getId());


        try {
            HSSFWorkbook workbook = new HSSFWorkbook(input);

            HSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                double eValue = nextCell(cellIterator);
                if (eValue != -1) {

                    EnergySampleDTO e = new EnergySampleDTO(eValue, time);

                    ePredicted.add(e);
                    time = time.plus(aggregationGranularity.getGranularity(), ChronoUnit.MINUTES);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }


        eEnergyPriceProfile.setCurve(ePredicted);

        boolean inserted = predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);

        if (!inserted) {
            LOGGER.error("DR SIGNAL WAS NOT INSERTED " + predictionGranularity.getGranularity());
        }
        return inserted;
    }

    public boolean populateDRSIGNALFromArray(List<Double> drSignal, String deviceName, LocalDateTime time, PredictionGranularity predictionGranularity, AggregationGranularity aggregationGranularity) {


        List<DeviceDTO> devices = deviceService.findByParentAndType(deviceName, DeviceTypeEnum.DATACENTER); //TODO check device type
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), time, predictionGranularity.getGranularity(), AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
        UUID pjId = predictionJobService.insert(pjDTO);

        UUID ePriceMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.DR_SIGNAL.type());

        List<EnergySampleDTO> ePredicted = new ArrayList<>();

        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(aggregationGranularity);
        eEnergyPriceProfile.setPredictionGranularity(predictionGranularity);
        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_BASELINE);
        eEnergyPriceProfile.setMeasurementId(ePriceMeasurementUUID);
        eEnergyPriceProfile.setDeviceId(device.getId());

        for(Double eValue: drSignal){
            if (eValue != -1) {

                EnergySampleDTO e = new EnergySampleDTO(eValue, time);

                ePredicted.add(e);
                time = time.plus(aggregationGranularity.getGranularity(), ChronoUnit.MINUTES);
            }
        }
        eEnergyPriceProfile.setCurve(ePredicted);

        boolean inserted = predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);

        if (!inserted) {
            LOGGER.error("DR SIGNAL WAS NOT INSERTED " + predictionGranularity.getGranularity());
        }
        return inserted;
    }

    public boolean populateBaselineData(String path1, String dcName, LocalDateTime time, PredictionGranularity predGranurality, AggregationGranularity aggGranurality) {

        InputStream input = loadFile(path1);
        List<DeviceDTO> devices = deviceService.findByParentAndType(dcName, DeviceTypeEnum.DATACENTER); //TODO check device type
        DeviceDTO device;
        if (!devices.isEmpty()) {
            device = devices.get(0);
        } else {
            return false;
        }

        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), time, predGranurality.getGranularity(), AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
        UUID pjId = predictionJobService.insert(pjDTO);

        UUID ePriceMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.DATA_CENTER_BASELINE.type());

        List<EnergySampleDTO> ePredicted = new ArrayList<>();

        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
        eEnergyPriceProfile.setAggregationGranularity(aggGranurality);
        eEnergyPriceProfile.setPredictionGranularity(predGranurality);
        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_BASELINE);
        eEnergyPriceProfile.setMeasurementId(ePriceMeasurementUUID);
        eEnergyPriceProfile.setDeviceId(device.getId());


        try {
            HSSFWorkbook workbook = new HSSFWorkbook(input);

            HSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                double eValue = nextCell(cellIterator);
                if (eValue != -1) {

                    EnergySampleDTO e = new EnergySampleDTO(eValue, time);

                    ePredicted.add(e);
                    time = time.plus(aggGranurality.getGranularity(), ChronoUnit.MINUTES);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }


        eEnergyPriceProfile.setCurve(ePredicted);

        boolean inserted = predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);
        if (!inserted) {
            LOGGER.error("BASELINE WAS NOT INSERTED " + predGranurality.getGranularity());
        }
        return inserted;
    }

//    public void readReadASResults() {
//
//        Path folderPath = Paths.get("/home/daniel/Desktop/Catalyst-demo/Oldd/");
//
//        String deviceName = "DATACENTER_POZNAN";
//        LocalDateTime time = LocalDateTime.of(2019, 6, 29, 0, 0, 0);
//        PredictionGranularity predictionGranularity = PredictionGranularity.INTRADAY;
//        AggregationGranularity aggregationGranularity = AggregationGranularity.MINUTES_30;
//
//        List<DeviceDTO> devices = deviceService.findByParentAndType(deviceName, DeviceTypeEnum.DATACENTER); //TODO check device type
//        DeviceDTO device;
//        if (!devices.isEmpty()) {
//            device = devices.get(0);
//        } else {
//            return;
//        }
//
//        PredictionJobDTO pjDTO = new PredictionJobDTO(UUID.randomUUID(), time, predictionGranularity.getGranularity(), AlgorithmType.ENSEMBLE.name(), FlexibilityType.NONE.getType());
//        UUID pjId = predictionJobService.insert(pjDTO);
//
//        UUID ePriceMeasurementUUID = measurementService.getMeasurementByProperty(MeasurementType.OPTIMIZED_SERVER.type());
//
//        List<EnergySampleDTO> ePredicted = new ArrayList<>();
//
//        EnergyProfileDTO eEnergyPriceProfile = new EnergyProfileDTO();
//        eEnergyPriceProfile.setAggregationGranularity(aggregationGranularity);
//        eEnergyPriceProfile.setPredictionGranularity(predictionGranularity);
//        eEnergyPriceProfile.setPredictionType(PredictionType.ENERGY_BASELINE);
//        eEnergyPriceProfile.setMeasurementId(ePriceMeasurementUUID);
//        eEnergyPriceProfile.setDeviceId(device.getId());
//
//        List<String> fileNames = new ArrayList<>();
//
//        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
//            for (Path path : directoryStream) {
//                fileNames.add(path.toString());
//            }
//        } catch (IOException ex) {
//            LOGGER.error(FILE_READ_ERROR);
//        }
//
//        for (String fileEntry : fileNames) {
//
//            try (CSVReader reader = new CSVReader(new FileReader(fileEntry))) {
//                LOGGER.info(fileEntry);
//                String[] line;
//                reader.readNext();
//                while ((line = reader.readNext()) != null) {
//                    LOGGER.info("time = " + line[0] +
//                            " , dr signal = " + line[1]);
//
//                    ePredicted.add(new EnergySampleDTO(Double.parseDouble(line[1]), this.convert(line[0])));
//                }
//                eEnergyPriceProfile.setCurve(ePredicted);
//
//                predictedValueService.insertPredictedEnergyCurve(eEnergyPriceProfile, pjId);
//            } catch (IOException e) {
//                LOGGER.error(e);
//            }
//        }
//    }
//
//
//    public void readTER() {
//
//        String deviceName = "DATACENTER_POZNAN";
//
//        List<DeviceDTO> devices = deviceService.findByParentAndType(deviceName, DeviceTypeEnum.DATACENTER); //TODO check device type
//        DeviceDTO device;
//        if (!devices.isEmpty()) {
//            device = devices.get(0);
//        } else {
//            return;
//        }
//
//        Path folderPath = Paths.get("/home/daniel/Desktop/Catalyst-demo/Oldd/");
//
//        // retrieve a list of the files in the folder
//        List<String> fileNames = new ArrayList<>();
//        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
//            for (Path path : directoryStream) {
//                fileNames.add(path.toString());
//            }
//        } catch (IOException ex) {
//            LOGGER.error(FILE_READ_ERROR);
//        }
//
//        for (String fileEntry : fileNames) {
//
//            try (CSVReader reader = new CSVReader(new FileReader(fileEntry))) {
//                LOGGER.info(fileEntry);
//                String[] line;
//                String[] headerLine = reader.readNext();
//                LOGGER.info(headerLine[0] + " " + headerLine[1] + " " + headerLine[2] + " " + headerLine[3]);
//                while ((line = reader.readNext()) != null) {
//                    LOGGER.info("[esd_current = " + line[0] +
//                            " , facility_power_consumption = " + line[1] +
//                            " , it_power_consumption_dt = " + line[2] +
//                            " , it_power_consumption_rt = " + line[3] +
//                            " , record_time = " + line[4] +
//                            " , tes_current = " + line[5] +
//                            " , serverRoom = " + line[6] +
//                            " , realoc_energy = " + line[7] +
//                            "]");
//
//                    LocalDateTime time = this.convert(line[4]);
//                    Double batteryCurrent = Double.parseDouble(line[0]);
//                    Double tesCurrent = Double.parseDouble(line[5]);
//                    Double serverRoom = Double.parseDouble(line[6]);
//                    Double coolingValues = Double.parseDouble(line[1]);
//                    Double relocateValues = Double.parseDouble(line[7]);
//
//
//                    UUID batteryId = measurementService.getMeasurementByProperty(MeasurementType.BATTERY_CURRENT.type());
//                    monitoredValueService.insert(new MonitoredValueDTO(time, batteryCurrent, device.getId(), batteryId));
//
//                    UUID tesId = measurementService.getMeasurementByProperty(MeasurementType.TES_CURRENT.type());
//                    monitoredValueService.insert(new MonitoredValueDTO(time, tesCurrent, device.getId(), tesId));
//
//                    UUID serverRoomId = measurementService.getMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type());
//                    monitoredValueService.insert(new MonitoredValueDTO(time, serverRoom, device.getId(), serverRoomId));
//
//                    UUID coolingSystemId = measurementService.getMeasurementByProperty(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
//                    monitoredValueService.insert(new MonitoredValueDTO(time, coolingValues, device.getId(), coolingSystemId));
//
//                    UUID rellocateId = measurementService.getMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION_HOST_RELOCATE.type());
//                    monitoredValueService.insert(new MonitoredValueDTO(time, relocateValues, device.getId(), rellocateId));
//
//                    // Insert Monitored Value in Mysql
//
//                }
//            } catch (IOException e) {
//                LOGGER.error(e);
//            }
//        }
//    }

    private static double nextCell(Iterator<Cell> cellIterator) {
        if (cellIterator.hasNext()) {
            Cell cell1 = cellIterator.next();
            if (cell1.getCellType().equals(CellType.NUMERIC)) {
                return cell1.getNumericCellValue();
            } else {
                LOGGER.error(" UNABLE TO READ FIELD");
                return -1;
            }
        }
        return -1;
    }

}


