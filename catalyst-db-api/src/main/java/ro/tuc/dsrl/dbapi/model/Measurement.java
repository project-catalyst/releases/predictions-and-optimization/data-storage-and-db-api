package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "measurement")
public class Measurement {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "property", unique = true)
    @NotNull
    private String property;

    @Column(name = "observation")
    @NotNull
    private String observation;

    @ManyToOne
    @JoinColumn(name = "device_type_id")
    @NotNull
    private DeviceType deviceTypeId;

    @ManyToOne
    @JoinColumn(name = "measure_unit_id")
    @NotNull
    private MeasureUnit measureUnitId;

    @ManyToOne
    @JoinColumn(name = "property_source_id")
    @NotNull
    private PropertySource propertySourceId;

    @ManyToOne
    @JoinColumn(name = "value_type_id")
    @NotNull
    private ValueType valueTypeId;

    public Measurement() {

    }

    public Measurement(@NotNull String property,
                       @NotNull String observation,
                       @NotNull DeviceType deviceTypeId,
                       @NotNull MeasureUnit measureUnitId,
                       @NotNull PropertySource propertySourceId,
                       @NotNull ValueType valueTypeId) {
        this.property = property;
        this.observation = observation;
        this.deviceTypeId = deviceTypeId;
        this.measureUnitId = measureUnitId;
        this.propertySourceId = propertySourceId;
        this.valueTypeId = valueTypeId;
    }

    public Measurement(@NotNull UUID id,
                       @NotNull String property,
                       @NotNull String observation,
                       @NotNull DeviceType deviceTypeId,
                       @NotNull MeasureUnit measureUnitId,
                       @NotNull PropertySource propertySourceId,
                       @NotNull ValueType valueTypeId) {

        this(property, observation, deviceTypeId, measureUnitId, propertySourceId, valueTypeId);

        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getProperty() {
        return property;
    }

    public String getObservation() {
        return observation;
    }

    public DeviceType getDeviceTypeId() {
        return deviceTypeId;
    }

    public MeasureUnit getMeasureUnitId() {
        return measureUnitId;
    }

    public PropertySource getPropertySourceId() {
        return propertySourceId;
    }

    public ValueType getValueTypeId() {
        return valueTypeId;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public void setDeviceTypeId(DeviceType deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public void setMeasureUnitId(MeasureUnit measureUnitId) {
        this.measureUnitId = measureUnitId;
    }

    public void setPropertySourceId(PropertySource propertySourceId) {
        this.propertySourceId = propertySourceId;
    }

    public void setValueTypeId(ValueType valueTypeId) {
        this.valueTypeId = valueTypeId;
    }
}
