package ro.tuc.dsrl.dbapi.controller.optimization;

import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/monitored-measurement")
public class MonitoredMeasurementController {

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;

    public MonitoredMeasurementController(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
    }

    @GetMapping(value= "/baseline/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findBaselineDayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DATA_CENTER_BASELINE);
    }



    @GetMapping(value= "/baseline/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findBaselineIntraDayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_BASELINE);
    }

    @GetMapping(value= "/baseline/co2/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findBaselineCo2DayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DATA_CENTER_CO2_BASELINE);
    }


    @GetMapping(value= "/baseline/co2/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findBaselineCO2IntraDayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_CO2_BASELINE);
    }


    @GetMapping(value= "/baseline/thermal/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findBaselineThermalDayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DATA_CENTER_HEAT_BASELINE);
    }


    @GetMapping(value= "/baseline/thermal/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findBaselineThermalIntraDayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.DATA_CENTER_HEAT_BASELINE);
    }



    @GetMapping(value= "/dr-signal/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findDrSignalIntraDayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.DR_SIGNAL);
    }


    @GetMapping(value= "/dr-signal/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findDrSignalDayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.DR_SIGNAL);
    }
    @GetMapping(value= "/optimized/dc/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedDCDayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.OPTIMIZED_DC);
    }

    @GetMapping(value= "/optimized/dc/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedDCIntradayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.OPTIMIZED_DC);
    }

    @GetMapping(value= "/optimized/server/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedEnergyDayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.OPTIMIZED_SERVER);
    }

    @GetMapping(value= "/optimized/server/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedEnergyIntradayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.OPTIMIZED_SERVER);
    }

    @GetMapping(value= "/optimized/thermal/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedThermalDayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.OPTIMIZED_THERMAL);
    }

    @GetMapping(value= "/optimized/thermal/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedThermalIntradayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.OPTIMIZED_THERMAL);
    }

    @GetMapping(value= "/optimized/cooling/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedCoolingDayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.OPTIMIZED_COOLING);
    }

    @GetMapping(value= "/optimized/cooling/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedCoolingIntradayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.OPTIMIZED_COOLING);
    }

    @GetMapping(value= "/optimized/co2/dayahead/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedCO2DayAheadForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.DAYAHEAD.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.OPTIMIZED_CO2);
    }

    @GetMapping(value= "/optimized/co2/intraday/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO findAdaptedCo2IntradayForItComponent(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime endTime = startLocalDateTime.plusHours(PredictionGranularity.INTRADAY.getNoHours());

        return energyHistoricalPredictedValuesService.getEstimatedProfileValuesInInterval(
                datacenterName,
                startLocalDateTime,
                endTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.OPTIMIZED_CO2);
    }
}
