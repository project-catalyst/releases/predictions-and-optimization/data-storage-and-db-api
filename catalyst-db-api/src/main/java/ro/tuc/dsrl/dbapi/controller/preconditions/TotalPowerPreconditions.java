package ro.tuc.dsrl.dbapi.controller.preconditions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class TotalPowerPreconditions {

    private static final Log LOGGER = LogFactory.getLog(TotalPowerPreconditions.class);

    private TotalPowerPreconditions() {
    }

    public static List<String> validateCurrentTimeMillis(Long currentTimeMillis){
        List<String> errors = new ArrayList<>();

        if(currentTimeMillis == null){
            errors.add("Current time millis must not be null");
        }

        return errors;
    }

    public static void validateCurrentTimeMillisThrowErrors(Long currentTimeMillis) {
        List<String> errors = validateCurrentTimeMillis(currentTimeMillis);
        throwIncorrectParameterException(errors);
    }

    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(TotalPowerPreconditions.class.getSimpleName(), errors);
        }
    }
}
