//package ro.tuc.dsrl.dbapi.model;
//
//import org.hibernate.annotations.GenericGenerator;
//
//import javax.persistence.*;
//import javax.validation.constraints.NotNull;
//import java.util.UUID;
//
//@Entity
//@Table(name = "action_value")
//public class ActionValue {
//
//    @Id
//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
//    @Column(name = "id", columnDefinition = "BINARY(16)")
//    private UUID id;
//
//    @Column(name = "value")
//    @NotNull
//    private Double value;
//
//    @ManyToOne
//    @JoinColumn(name = "action_instance_id")
//    @NotNull
//    private ActionInstance actionInstanceId;
//
//    @ManyToOne
//    @JoinColumn(name = "action_property_id")
//    @NotNull
//    private ActionProperty actionPropertyId;
//
//    public ActionValue() {
//
//    }
//
//    public ActionValue(@NotNull Double value,
//                       @NotNull ActionInstance actionInstanceId,
//                       @NotNull ActionProperty actionPropertyId) {
//        this.value = value;
//        this.actionInstanceId = actionInstanceId;
//        this.actionPropertyId = actionPropertyId;
//    }
//
//    public ActionValue(@NotNull UUID id,
//                       @NotNull Double value,
//                       @NotNull ActionInstance actionInstanceId,
//                       @NotNull ActionProperty actionPropertyId) {
//        this.id = id;
//        this.value = value;
//        this.actionInstanceId = actionInstanceId;
//        this.actionPropertyId = actionPropertyId;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public Double getValue() {
//        return value;
//    }
//
//    public ActionInstance getActionInstanceId() {
//        return actionInstanceId;
//    }
//
//    public ActionProperty getActionPropertyId() {
//        return actionPropertyId;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public void setValue(Double value) {
//        this.value = value;
//    }
//
//    public void setActionInstanceId(ActionInstance actionInstanceId) {
//        this.actionInstanceId = actionInstanceId;
//    }
//
//    public void setActionPropertyId(ActionProperty actionPropertyId) {
//        this.actionPropertyId = actionPropertyId;
//    }
//}
