package ro.tuc.dsrl.dbapi.model.dto;

import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;

import java.time.LocalDateTime;

public class AggregatedValuesDTO {
    private double value;
    private Device deviceID;
    private Measurement measurementID;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int minuteSlots;

    public AggregatedValuesDTO() {
    }

    public AggregatedValuesDTO(double value){
        this.value = value;
    }

    public AggregatedValuesDTO(double value, int year, int month, int day, int hour, Double minute){
        this.value = value;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute.intValue();
    }

    public AggregatedValuesDTO(double value, Device deviceID, Measurement measurementID, int year, int month, int day,
                               int hour, Double minute) {
        this.value = value;
        this.deviceID = deviceID;
        this.measurementID = measurementID;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute.intValue();
    }

    public AggregatedValuesDTO(double value, Device deviceID, Measurement measurementID, int year, int month, int day,
                               int hour, int minute) {
        this.value = value;
        this.deviceID = deviceID;
        this.measurementID = measurementID;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Device getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(Device deviceID) {
        this.deviceID = deviceID;
    }

    public Measurement getMeasurementID() {
        return measurementID;
    }

    public void setMeasurementID(Measurement measurementID) {
        this.measurementID = measurementID;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getMinuteSlots() {
        return minuteSlots;
    }

    public void setMinuteSlots(int minuteSlots) {
        this.minuteSlots = minuteSlots;
    }

    public LocalDateTime getDateTime(){
        LocalDateTime dateTime = LocalDateTime.of(year, month, day, hour, minute);
        return dateTime;
    }
}