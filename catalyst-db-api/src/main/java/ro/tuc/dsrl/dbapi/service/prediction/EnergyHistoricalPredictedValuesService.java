package ro.tuc.dsrl.dbapi.service.prediction;

import org.hibernate.cfg.NotYetImplementedException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.*;
import ro.tuc.dsrl.dbapi.model.builders.PredictionJobBuilder;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.repository.*;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.uisupport.formatter.ProfileFormatter;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;
import ro.tuc.dsrl.dbapi.util.RepoConversionUtility;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;
import ro.tuc.dsrl.geyser.datamodel.other.MarketPrices;

import java.io.InputStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.apache.commons.lang3.math.NumberUtils.max;

@Service
public class EnergyHistoricalPredictedValuesService {
    private final PredictedValueRepository predictedValueRepository;
    private final DeviceRepository deviceRepository;
    private final MeasurementRepository measurementRepository;
    private final PredictionJobRepository predictionJobRepository;
    private final DeviceService deviceService;
    private final String datacenterName;

    @Autowired
    public EnergyHistoricalPredictedValuesService(PredictedValueRepository predictedValueRepository,
                                                  DeviceRepository deviceRepository,
                                                  MeasurementRepository measurementRepository,
                                                  PredictionJobRepository predictionJobRepository,
                                                  DeviceService deviceService,
                                                  @Value("${datacenter.name}") String datacenterName) {

        this.predictedValueRepository = predictedValueRepository;
        this.deviceRepository = deviceRepository;
        this.measurementRepository = measurementRepository;
        this.predictionJobRepository = predictionJobRepository;
        this.deviceService = deviceService;
        this.datacenterName = datacenterName;
    }


    public List<AggregatedValuesDTO> getEstimatedValuesProfile(String dcName,
                                                               LocalDateTime startTime,
                                                               LocalDateTime endTime,
                                                               PredictionGranularity predictionGranularity,
                                                               MeasurementType measurementType) {
        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime lastPredictionJobDateTime = predictionJobRepository.findJobDateTime(
                predictionGranularity.getGranularity(),
                AlgorithmType.ENSEMBLE.name(),
                dcName,
                FlexibilityType.NONE.getType(),
                startTime);
        Device device = deviceRepository.findDeviceByLabel(dcName);
        if (device == null) {
            throw new ResourceNotFoundException("Device type " + dcName);
        }

        Measurement measurement = measurementRepository.findMeasurementByProperty(measurementType.type());
        if (measurement == null) {
            throw new ResourceNotFoundException("Measurement type " + measurementType.type());
        }

        Object[][] results = predictedValueRepository.findAveragedSignalsInInterval(
                dcName,
                predictionGranularity.getGranularity(),
                startTime,
                endTime,
                measurementType.type(),
                lastPredictionJobDateTime,
                predictionGranularity.getSampleFrequencyMin());
        return RepoConversionUtility.getAveragedValues(results, device, measurement);

    }


    //region Consumption

    public List<EnergyProfileDTO> getPredictedConsumptionForITComponent(UUID dataCenterID,
                                                                        LocalDateTime startTime,
                                                                        PredictionGranularity predictionGranularity,
                                                                        String energyType,
                                                                        String predictionType,
                                                                        String flexibilityType) {

        List<EnergyProfileDTO> predictedDTAndRTServerSignals = new ArrayList<>();

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        List<EnergyProfileDTO> energyPredictedProfiles = new ArrayList<>();

        String algorithmType = AlgorithmType.ENSEMBLE.getShortName();

        // find last prediction job date time for DT
        LocalDateTime lastPredictionJobDateTimeDT = predictionJobRepository.findJobDateTimeForMeasurement(
                predictionGranularity.getGranularity(),
                algorithmType,
                DeviceTypeEnum.SERVER_ROOM.type(),
                flexibilityType,
                startTime,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());

        // find last prediction job date time for RT
        LocalDateTime lastPredictionJobDateTimeRT = predictionJobRepository.findJobDateTimeForMeasurement(
                predictionGranularity.getGranularity(),
                algorithmType,
                DeviceTypeEnum.SERVER_ROOM.type(),
                flexibilityType,
                startTime,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());

        if (lastPredictionJobDateTimeDT == null && lastPredictionJobDateTimeRT == null) {

            // empty list win DT and RT empty energy profile DTOs
            energyPredictedProfiles.add(EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity));
            energyPredictedProfiles.add(EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity));
            return energyPredictedProfiles;
        } else {

            // search for DT and RT energy predicted profiles
            List<PredictedValue> sampledValuesServerDT = new ArrayList<>();
            List<PredictedValue> sampledValuesServerRT = new ArrayList<>();

            // find DT and RT averaged signals (we suppose that we have them inserted like that
            sampledValuesServerDT = this.findServerDTConsumptionAveragedSignal(dataCenterID,
                    startTime,
                    predictionGranularity,
                    energyType,
                    predictionGranularity.getSampleFrequencyMin(),
                    predictionType,
                    lastPredictionJobDateTimeDT);

            sampledValuesServerRT = this.findServerRTConsumptionAveragedSignal(dataCenterID,
                    startTime,
                    predictionGranularity,
                    energyType,
                    predictionGranularity.getSampleFrequencyMin(),
                    predictionType,
                    lastPredictionJobDateTimeRT);

            // return both curves in the list
            if (!sampledValuesServerDT.isEmpty()) {

                EnergyProfileDTO dtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesServerDT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity);
                predictedDTAndRTServerSignals.add(dtEnergyProfileDTO);
            } else {
                EnergyProfileDTO dtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
                predictedDTAndRTServerSignals.add(dtEnergyProfileDTO);
            }

            if (!sampledValuesServerRT.isEmpty()) {

                EnergyProfileDTO rtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesServerRT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity);
                predictedDTAndRTServerSignals.add(rtEnergyProfileDTO);
            } else {
                EnergyProfileDTO rtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
                predictedDTAndRTServerSignals.add(rtEnergyProfileDTO);
            }

        }

        return predictedDTAndRTServerSignals;

    }


    public ForecastDayAheadDTO forecastedWorkloadValues(String dataCenterName,
                                                        LocalDateTime startTime,
                                                        PredictionGranularity predictionGranularity,
                                                        MeasurementType type) {
        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());
        ForecastDayAheadDTO forecastDayAheadDTO = this.getEstimatedProfileValuesInInterval(dataCenterName, startTime, endTime, predictionGranularity, type);
        if (forecastDayAheadDTO == null || forecastDayAheadDTO.getEnergyPointValueDTOS().isEmpty()) {
            return ForecastDayAheadDTO.getDefaultInstance(startTime, predictionGranularity);
        }

        return averageValues(forecastDayAheadDTO);
    }

    public ForecastDayAheadDTO averageValues(ForecastDayAheadDTO values) {
        ForecastDayAheadDTO average = new ForecastDayAheadDTO();
        List<EnergyPointValueDTO> averageValues = new ArrayList<>();

        int j = -1;
        long timestamp = -1;
        int k = 1;
        for (EnergyPointValueDTO value : values.getEnergyPointValueDTOS()) {
            if (value.getTimeStamp() != timestamp) {
                if (k > 1) {
                    averageValues.set(j, new EnergyPointValueDTO(averageValues.get(j).getEnergyValue() / k, averageValues.get(j).getTimeStamp()));
                }
                timestamp = value.getTimeStamp();
                ++j;
                k = 1;
                averageValues.add(j, value);
            } else {
                averageValues.set(j, new EnergyPointValueDTO(averageValues.get(j).getEnergyValue() + value.getEnergyValue(), value.getTimeStamp()));
                ++k;
            }
        }

        average.setEnergyPointValueDTOS(averageValues);

        return average;
    }


    /**
     * The percentage based forecast needs to be replaced with actually DT vs RT forecast
     */
    @Deprecated
    public ForecastDayAheadDTO forecastedSplitPercentageDayAheadValues(String dataCenterName,
                                                                       LocalDateTime startTime,
                                                                       PredictionGranularity predictionGranularity,
                                                                       Integer type) {

        List<ServerRoom> serverRooms = deviceService.getServerRoom(dataCenterName, startTime);

        double edPercentage = 0;

        for (ServerRoom serverRoom : serverRooms) {
            edPercentage += serverRoom.getEdPercentage();
        }

        edPercentage = edPercentage / serverRooms.size();

        if (type == 1) { //rt
            edPercentage = 1 - edPercentage;
        }

        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());
        ForecastDayAheadDTO forecastDayAheadDTO = this.getEstimatedProfileValuesInInterval(dataCenterName, startTime, endTime, predictionGranularity, MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION);
        if (forecastDayAheadDTO != null && !forecastDayAheadDTO.getEnergyPointValueDTOS().isEmpty()) {
            List<EnergyPointValueDTO> energyInputDTOS = forecastDayAheadDTO.getEnergyPointValueDTOS();
            List<EnergyPointValueDTO> energyOutputDTOS = new ArrayList<>();
            for (EnergyPointValueDTO energySampleDTO : energyInputDTOS) {
                energyOutputDTOS.add(new EnergyPointValueDTO(energySampleDTO.getEnergyValue() * edPercentage, energySampleDTO.getTimeStamp()));
            }
            forecastDayAheadDTO.setEnergyPointValueDTOS(energyOutputDTOS);
        } else {
            return ForecastDayAheadDTO.getDefaultInstance(startTime, predictionGranularity);
        }
        return forecastDayAheadDTO;
    }


    public ForecastDayAheadDTO getEstimatedProfileValuesInInterval(String datacenterName,
                                                                   LocalDateTime startLocalDateTime,
                                                                   LocalDateTime endLocalDateTime,
                                                                   PredictionGranularity predictionGranularity,
                                                                   MeasurementType measurementType) {

        List<EnergySampleDTO> energySampleDTOList = predictedValueRepository.findEstimatedProfileValues(
                datacenterName,
                predictionGranularity.getGranularity(),
                measurementType.type(),
                startLocalDateTime,
                endLocalDateTime);

        if (!energySampleDTOList.isEmpty()) {
            List<EnergyPointValueDTO> energyPointValueDTOS = new ArrayList<>();
            for (EnergySampleDTO energySampleDTO : energySampleDTOList) {
                energyPointValueDTOS.add(new EnergyPointValueDTO(energySampleDTO.getEnergyValue(), DateUtils.localDateTimeToLongInMillis(energySampleDTO.getTimestamp())));
            }

            ForecastDayAheadDTO forecastDayAheadDTO = new ForecastDayAheadDTO();
            forecastDayAheadDTO.setEnergyPointValueDTOS(energyPointValueDTOS);
            return averageValues(forecastDayAheadDTO);
        }

        return ForecastDayAheadDTO.getDefaultInstance(startLocalDateTime, predictionGranularity);
    }


    public EnergyProfileDTO getPredictedConsumptionForCoolingSystem(UUID dataCenterID,
                                                                    LocalDateTime startTime,
                                                                    PredictionGranularity predictionGranularity,
                                                                    String energyType,
                                                                    String predictionType,
                                                                    String flexibilityType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

    /* TODO: Change the way we pick a certain prediction job.
            Currently, if multiple prediction jobs are ran for the same algorithm, energy type, granularity, topology component,
            consumption/production scenario we are picking the last prediction job (last timestamp).
            AlgorithmType is always hardcoded to ENSEMBLE.
        */

        LocalDateTime lastPredictionJobDateTime = predictionJobRepository.findJobDateTime(
                predictionGranularity.getGranularity(),
                AlgorithmType.ENSEMBLE.getShortName(),
                DeviceTypeEnum.COOLING_SYSTEM.type(),
                flexibilityType,
                startTime);

        if (lastPredictionJobDateTime == null) {
            return EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
        }

        List<PredictedValue> predictedConsumptionData = predictedValueRepository.findDataInIntervalForComponent(
                dataCenterID,
                energyType,
                DeviceTypeEnum.COOLING_SYSTEM.type(),
                predictionType,
                AlgorithmType.ENSEMBLE.getShortName(),
                predictionGranularity.getGranularity(),
                lastPredictionJobDateTime,
                flexibilityType);

        if (!predictedConsumptionData.isEmpty()) {
            return this.getEnergyProfileBasedOnData(predictedConsumptionData,
                    PredictionType.ENERGY_CONSUMPTION.name(),
                    energyType,
                    predictionGranularity);
        }

        return EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
    }

    public EnergyProfileDTO getPredictedConsumptionForEntireDC(UUID dataCenterID,
                                                               LocalDateTime startTime,
                                                               PredictionGranularity predictionGranularity,
                                                               String energyType,
                                                               String predictionType,
                                                               String flexibilityType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        // get predicted consumption for SERVER
        List<EnergyProfileDTO> energyPredictedConsumptionForServer = this.getPredictedConsumptionForITComponent(dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                predictionType,
                flexibilityType);

        // get predicted consumption for COOLING
        EnergyProfileDTO energyPredictedConsumptionForCooling = this.getPredictedConsumptionForCoolingSystem(dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                predictionType,
                flexibilityType);

        EnergyProfileDTO energyProfileConsumptionForDC = new EnergyProfileDTO();
        for (int i = 0; i < energyPredictedConsumptionForServer.get(0).getCurve().size(); i++) {

            double energyValueDT = energyPredictedConsumptionForServer.get(0).getCurve().get(i).getEnergyValue();
            double energyValueRT = energyPredictedConsumptionForServer.get(1).getCurve().get(i).getEnergyValue();
            double energyValueCooling = energyPredictedConsumptionForCooling.getCurve().get(i).getEnergyValue();
            energyPredictedConsumptionForServer.get(0).getCurve().get(i).setEnergyValue(
                    energyValueDT + energyValueRT + energyValueCooling);
        }

        energyProfileConsumptionForDC = energyPredictedConsumptionForServer.get(0);

        return energyProfileConsumptionForDC;
    }


    public EnergyProfileDTO getPredictedConsumptionForServer(UUID dataCenterID,
                                                             UUID serverID,
                                                             LocalDateTime startTime,
                                                             PredictionGranularity predictionGranularity,
                                                             String energyType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, serverID, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        if (true) throw new NotYetImplementedException(MonitoredValueRepository.class.toString());
        List<PredictedValue> historicalConsumptionDataForDC = predictedValueRepository.findDataInIntervalForComponentServer(
                dataCenterID,
                serverID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.SERVER.type());

        if (!historicalConsumptionDataForDC.isEmpty()) {
            return this.getEnergyProfileBasedOnData(historicalConsumptionDataForDC,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity);
        }

        return null;
    }
    //endregion


    //region Production
    public EnergyProfileDTO getPredictedProductionForEntireDC(UUID dataCenterID,
                                                              LocalDateTime startTime,
                                                              PredictionGranularity predictionGranularity,
                                                              String energyType,
                                                              String predictionType,
                                                              String flexibilityType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        // get predicted production for SERVER
        EnergyProfileDTO itProduction = this.getPredictedProductionForITComponent(dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                predictionType);

        // get predicted production for COOLING
        EnergyProfileDTO coolingProduction = this.getPredictedProductionForCoolingSystem(dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                predictionType);

        EnergyProfileDTO datacenterProduction = this.getPredictedProductionForDatacenter(dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                predictionType);

        List<EnergySampleDTO> summedProduction;
        if (itProduction == null && coolingProduction == null && datacenterProduction == null) {
            return null;
        } else if (itProduction == null && datacenterProduction == null) {
            return coolingProduction;
        } else if (coolingProduction == null && datacenterProduction == null) {
            return itProduction;
        } else if (coolingProduction == null && itProduction == null) {
            return datacenterProduction;
        } else {
            int size = max(
                    itProduction != null ? itProduction.getCurve().size() : 0,
                    coolingProduction != null ? coolingProduction.getCurve().size() : 0,
                    datacenterProduction != null ? datacenterProduction.getCurve().size() : 0
                    );
            summedProduction = Arrays.asList(new EnergySampleDTO[size]);
            for (int i = 0; i < size; i++)
            {
                LocalDateTime timestamp = startTime.plusHours(i);
                summedProduction.set(i, new EnergySampleDTO(
                        (itProduction != null && itProduction.getCurve().size() > i ? itProduction.getCurve().get(i).getEnergyValue() : 0) +
                                (coolingProduction != null && coolingProduction.getCurve().size() > i ? coolingProduction.getCurve().get(i).getEnergyValue() : 0) +
                                (datacenterProduction != null && datacenterProduction.getCurve().size() > i ? datacenterProduction.getCurve().get(i).getEnergyValue() : 0),
                        timestamp));
            }

                return new EnergyProfileDTO(summedProduction,
                        dataCenterID,
                        itProduction.getMeasurementId(),
                        itProduction.getAggregationGranularity(),
                        itProduction.getPredictionGranularity(),
                        itProduction.getPredictionType(),
                        itProduction.getEnergyType()
                );
            }
    }

    public EnergyProfileDTO getPredictedProductionForITComponent(UUID dataCenterID,
                                                                 LocalDateTime startTime,
                                                                 PredictionGranularity predictionGranularity,
                                                                 String energyType,
                                                                 String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime lastPredictionJobDateTime = predictionJobRepository.findJobDateTime(
                predictionGranularity.getGranularity(),
                AlgorithmType.ENSEMBLE.getShortName(),
                DeviceTypeEnum.SERVER_ROOM.type(),
                FlexibilityType.NONE.getType(),
                startTime);

        if (lastPredictionJobDateTime == null) return null;

        List<PredictedValue> predictedConsumptionData = predictedValueRepository.findDataInIntervalForComponent(
                dataCenterID,
                energyType,
                DeviceTypeEnum.SERVER_ROOM.type(),
                predictionType,
                AlgorithmType.ENSEMBLE.getShortName(),
                predictionGranularity.getGranularity(),
                lastPredictionJobDateTime,
                FlexibilityType.NONE.getType());

        if (!predictedConsumptionData.isEmpty()) {
            return this.getEnergyProfileBasedOnData(predictedConsumptionData,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity);
        }

        return null;
    }

    public EnergyProfileDTO getPredictedProductionForCoolingSystem(UUID dataCenterID,
                                                                   LocalDateTime startTime,
                                                                   PredictionGranularity predictionGranularity,
                                                                   String energyType,
                                                                   String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime lastPredictionJobDateTime = predictionJobRepository.findJobDateTime(
                predictionGranularity.getGranularity(),
                AlgorithmType.ENSEMBLE.getShortName(),
                DeviceTypeEnum.COOLING_SYSTEM.type(),
                FlexibilityType.NONE.getType(),
                startTime);

        if (lastPredictionJobDateTime == null) return null;

        List<PredictedValue> predictedConsumptionData = predictedValueRepository.findDataInIntervalForComponent(
                dataCenterID,
                energyType,
                DeviceTypeEnum.COOLING_SYSTEM.type(),
                predictionType,
                AlgorithmType.ENSEMBLE.getShortName(),
                predictionGranularity.getGranularity(),
                lastPredictionJobDateTime,
                FlexibilityType.NONE.getType());

        if (!predictedConsumptionData.isEmpty()) {
            return this.getEnergyProfileBasedOnData(predictedConsumptionData,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity);
        }

        return null;
    }

    public EnergyProfileDTO getPredictedProductionForServer(UUID dataCenterID,
                                                            LocalDateTime startTime,
                                                            UUID serverID,
                                                            PredictionGranularity predictionGranularity,
                                                            String energyType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, serverID, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        if (true) throw new NotYetImplementedException(MonitoredValueRepository.class.toString());
        List<PredictedValue> historicalProductionDataForDC = predictedValueRepository.findDataInIntervalForComponentServer(
                dataCenterID,
                serverID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.SERVER.type());

        if (!historicalProductionDataForDC.isEmpty()) {
            return this.getEnergyProfileBasedOnData(historicalProductionDataForDC,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity);
        }

        return null;
    }

    public EnergyProfileDTO getPredictedProductionForDatacenter(UUID dataCenterID,
                                                                LocalDateTime startTime,
                                                                PredictionGranularity predictionGranularity,
                                                                String energyType,
                                                                String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime lastPredictionJobDateTime = predictionJobRepository.findJobDateTime(
                predictionGranularity.getGranularity(),
                AlgorithmType.ENSEMBLE.getShortName(),
                DeviceTypeEnum.DATACENTER.type(),
                FlexibilityType.NONE.getType(),
                startTime);

        if (lastPredictionJobDateTime == null) return null;

        List<PredictedValue> predictedConsumptionData = predictedValueRepository.findDataInIntervalForComponentRenewable(
                energyType,
                DeviceTypeEnum.DATACENTER.type(),
                AlgorithmType.ENSEMBLE.getShortName(),
                predictionGranularity.getGranularity(),
                lastPredictionJobDateTime,
                FlexibilityType.NONE.getType());

        if (!predictedConsumptionData.isEmpty()) {
            return this.getEnergyProfileBasedOnData(predictedConsumptionData,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity);
        }

        return null;
    }

    //endregion

    private EnergyProfileDTO getEnergyProfileBasedOnData(List<PredictedValue> data,
                                                         String predictionType,
                                                         String energyType,
                                                         PredictionGranularity predictionGranularity) {

        // find device used for measuring the data
        Device device = deviceRepository.findByUUID(data.get(0).getDeviceId().getId());

        // find measurement for the data
        Measurement measurement = measurementRepository.findByUUID(data.get(0).getMeasurementId().getId());

        List<EnergySampleDTO> energySampleDTOList = new ArrayList<>();
        for (PredictedValue p : data) {
            energySampleDTOList.add(new EnergySampleDTO(p.getValue(), p.getTimestamp()));
        }

        EnergyProfileDTO energyProfileDTO;

        switch (predictionGranularity) {

            case NEAR_REAL_TIME:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOList,
                        device.getId(),
                        measurement.getId(),
                        AggregationGranularity.HOUR,
                        PredictionGranularity.NEAR_REAL_TIME,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            case INTRADAY:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOList,
                        device.getId(),
                        measurement.getId(),
                        AggregationGranularity.MINUTES_30,
                        PredictionGranularity.INTRADAY,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            case DAYAHEAD:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOList,
                        device.getId(),
                        measurement.getId(),
                        AggregationGranularity.HOUR,
                        PredictionGranularity.DAYAHEAD,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            default:
                energyProfileDTO = new EnergyProfileDTO();
                break;
        }

        return energyProfileDTO;
    }


    public MarketPrices getMarketPrices(String dcName,
                                        LocalDateTime startTime,
                                        PredictionGranularity predictionGranularity) {
        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        int hour = startTime.getHour();
        ForecastDayAheadDTO energyPrices;
        ForecastDayAheadDTO workloadPrices;
        ForecastDayAheadDTO thermalPrices;
        ForecastDayAheadDTO incentivePrices;

        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);

        List<AggregatedValuesDTO> values;

        values = getEstimatedValuesProfile(dcName, startTime, endTime, predictionGranularity, MeasurementType.DC_E_PRICE);
        formatter.populateArray(values, hour, predictionGranularity);
        energyPrices = getFromAgregated(values);

        values = getEstimatedValuesProfile(dcName, startTime, endTime, predictionGranularity, MeasurementType.DC_L_PRICE);
        formatter.populateArray(values, hour, predictionGranularity);
        workloadPrices = getFromAgregated(values);

        values = getEstimatedValuesProfile(dcName, startTime, endTime, predictionGranularity, MeasurementType.DC_T_PRICE);
        formatter.populateArray(values, hour, predictionGranularity);
        thermalPrices = getFromAgregated(values);

        values = getEstimatedValuesProfile(dcName, startTime, endTime, predictionGranularity, MeasurementType.DC_DR_PRICE);
        formatter.populateArray(values, hour, predictionGranularity);
        incentivePrices = getFromAgregated(values);


        return new MarketPrices(energyPrices, workloadPrices, thermalPrices, incentivePrices);

    }


    public List<Double> getMarketPricesByType(String dcName,
                                              LocalDateTime startTime,
                                              PredictionGranularity predictionGranularity,
                                              MeasurementType measurementType) {
        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = getEstimatedValuesProfile(dcName, startTime, endTime, predictionGranularity, measurementType);
        return formatter.populateArray(values, startTime.getHour(), predictionGranularity);
    }

    public List<Double> getMarketPricesByTypeWithLatestFallback(String dcName,
                                                                LocalDateTime startTime,
                                                                PredictionGranularity predictionGranularity,
                                                                MeasurementType measurementType) {
        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        ProfileFormatter formatter = ProfileFormatter.getInstance(ProfileFormatter.ProfileType.ACTUAL_PROFILES);
        List<AggregatedValuesDTO> values = getEstimatedValuesProfile(dcName, startTime, endTime, predictionGranularity, measurementType);

        if (values.size() == 0) {
            Object[][] latestMeasurement = predictedValueRepository.findLatestMeasurement(
                    dcName,
                    startTime,
                    predictionGranularity.getGranularity(),
                    measurementType.type());
            if (latestMeasurement[0] == null)
                return new ArrayList<Double>(Collections.nCopies(predictionGranularity.getNoOutputs(), 0.0));

            LocalDateTime foundValueTime = ((Timestamp) latestMeasurement[0][0]).toLocalDateTime();
            startTime = foundValueTime.withHour(foundValueTime.getHour() / predictionGranularity.getNoHours() * predictionGranularity.getNoHours());
            endTime = startTime.plusHours(predictionGranularity.getNoHours());
            values = getEstimatedValuesProfile(dcName, startTime, endTime, predictionGranularity, measurementType);
        }
        return formatter.populateArray(values, startTime.getHour(), predictionGranularity);
    }

    public List<PredictedValue> findServerDTConsumptionAveragedSignal(UUID dataCenterID,
                                                                      LocalDateTime startTime,
                                                                      PredictionGranularity predictionGranularity,
                                                                      String energyType,
                                                                      Integer granularity,
                                                                      String predictionType,
                                                                      LocalDateTime predictionJobDateTime) {

        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());
        Object[][] dtServerEnergyProfileDTO = predictedValueRepository.findEstimatedProfileValuesForComponent(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                predictionGranularity.getGranularity(),
                DeviceTypeEnum.SERVER_ROOM.type(),
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION.type(),
                predictionType,
                predictionJobDateTime);

        Device deviceServerDT = deviceRepository.findDeviceByLabel(deviceRepository.findByUUID(dataCenterID).getLabel());
        if (deviceServerDT == null) {
            throw new ResourceNotFoundException("Device type " + deviceServerDT.toString());
        }

        Measurement measurementServerDT = measurementRepository.findMeasurementByDeviceId(deviceServerDT.getDeviceTypeId().getId(),
                energyType, predictionType);
        if (measurementServerDT == null) {
            throw new ResourceNotFoundException("Measurement type " + measurementServerDT.toString());
        }

        List<PredictedValue> sampledValuesServerDT = this.getEnergySampledForComponent(dtServerEnergyProfileDTO,
                deviceServerDT,
                measurementServerDT,
                startTime,
                predictionGranularity);


        return sampledValuesServerDT;
    }

    public List<PredictedValue> findServerRTConsumptionAveragedSignal(UUID dataCenterID,
                                                                      LocalDateTime startTime,
                                                                      PredictionGranularity predictionGranularity,
                                                                      String energyType,
                                                                      Integer granularity,
                                                                      String predictionType,
                                                                      LocalDateTime predictionJobDateTime) {
        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());
        Object[][] rtServerEnergyProfileDTO = predictedValueRepository.findEstimatedProfileValuesForComponent(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                predictionGranularity.getGranularity(),
                DeviceTypeEnum.SERVER_ROOM.type(),
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type(),
                predictionType,
                predictionJobDateTime);

        Device deviceServerRT = deviceRepository.findDeviceByLabel(deviceRepository.findByUUID(dataCenterID).getLabel());
        if (deviceServerRT == null) {
            throw new ResourceNotFoundException("Device type " + deviceServerRT.toString());
        }

        Measurement measurementServerRT = measurementRepository.findMeasurementByDeviceId(deviceServerRT.getDeviceTypeId().getId(),
                energyType, predictionType);
        if (measurementServerRT == null) {
            throw new ResourceNotFoundException("Measurement type " + measurementServerRT.toString());
        }

        List<PredictedValue> sampledValuesServerDT = this.getEnergySampledForComponent(rtServerEnergyProfileDTO,
                deviceServerRT,
                measurementServerRT,
                startTime,
                predictionGranularity);


        return sampledValuesServerDT;

    }

    private List<PredictedValue> getEnergySampledForComponent(Object[][] results,
                                                              Device device,
                                                              Measurement measurement,
                                                              LocalDateTime startTime,
                                                              PredictionGranularity predictionGranularity) {

        List<PredictedValue> sampledValues = new ArrayList<>();
        List<AggregatedValuesDTO> predictedNewlyConsumptionData = RepoConversionUtility.getAveragedValues(results, device, measurement);

        if (!predictedNewlyConsumptionData.isEmpty()) {
            List<EnergyPointValueDTO> energyPointValueDTOS = new ArrayList<>();

            LocalDateTime time = startTime;
            for (int i = 0; i < predictionGranularity.getNoInputs(); i++) {
                energyPointValueDTOS.add(new EnergyPointValueDTO(0.0, DateUtils.localDateTimeToLongInMillis(time)));
                time = time.plusMinutes(predictionGranularity.getSampleFrequencyMin());
            }

            if (predictionGranularity.getGranularity().equals("DAYAHEAD")) {
                for (AggregatedValuesDTO mav : predictedNewlyConsumptionData) {
                    int index = (mav.getHour() - startTime.getHour()) * (predictionGranularity.getSampleFrequencyMin() / predictionGranularity.getSampleFrequencyMin()) + mav.getMinute();
                    energyPointValueDTOS.get(index).setEnergyValue((mav.getValue()));
                }
            } else if (predictionGranularity.getGranularity().equals("INTRADAY")) {
                for (AggregatedValuesDTO mav : predictedNewlyConsumptionData) {
                    int index = 2 * (mav.getHour() - startTime.getHour()) + mav.getMinute() / predictionGranularity.getSampleFrequencyMin();
                    energyPointValueDTOS.get(index).setEnergyValue((mav.getValue()));
                }
            }

            // transform list of energy point values into list of predicted values
            for (int i = 0; i < energyPointValueDTOS.size(); i++) {
                sampledValues.add(new PredictedValue(Instant.ofEpochMilli(energyPointValueDTOS.get(i).getTimeStamp()).atZone(ZoneOffset.UTC).toLocalDateTime(),
                        energyPointValueDTOS.get(i).getEnergyValue(),
                        measurement,
                        device,
                        new PredictionJob()));
            }
        }

        return sampledValues;
    }

    private ForecastDayAheadDTO getFromAgregated(List<AggregatedValuesDTO> aggregatedValuesDTOS) {
        ForecastDayAheadDTO forecastDayAheadDTO = new ForecastDayAheadDTO();

        for (AggregatedValuesDTO aggregatedValuesDTO : aggregatedValuesDTOS) {

            LocalDateTime dateTime = aggregatedValuesDTO.getDateTime();
            long time = DateUtils.dateTimeToMillis(dateTime);

            forecastDayAheadDTO.getEnergyPointValueDTOS().add(new EnergyPointValueDTO(aggregatedValuesDTO.getValue(), time));
        }

        return forecastDayAheadDTO;
    }

//    public ForecastDayAheadDTO getAveragedValues(String dcName,
//                                                 LocalDateTime startTime,
//                                                 PredictionGranularity predictionGranularity,
//                                                 MeasurementType measurementType,
//                                                 String algorithmType,
//                                                 String flexibilityType) {
//
//        startTime = startTime.withNano(0).withSecond(0);
//        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());
//        LocalDateTime lastPredictionJobDateTime = predictionJobRepository.findJobDateTime(
//                predictionGranularity.getGranularity(),
//                algorithmType,
//                dcName,
//                flexibilityType,
//                startTime);
//        Device device = deviceRepository.findDeviceByLabel(dcName);
//        if (device == null) {
//            throw new ResourceNotFoundException("Device type " + dcName);
//        }
//
//        Measurement measurement = measurementRepository.findMeasurementByProperty(measurementType.type());
//        if (measurement == null) {
//            throw new ResourceNotFoundException("Measurement type " + measurementType.type());
//        }
//
//
//        Object[][] results = predictedValueRepository.findAveragedSignalsInInterval(
//                dcName,
//                predictionGranularity.getGranularity(),
//                startTime,
//                endTime,
//                measurementType.type(),
//                lastPredictionJobDateTime,
//                predictionGranularity.getSampleFrequencyMin());
//        List<AggregatedValuesDTO> historicalDisaggregatedConsumptionData = RepoConversionUtility.getAveragedValues(results, device, measurement);
//
//        List<EnergyPointValueDTO> energyPointValueDTOS = new ArrayList<>();
//
//        LocalDateTime time = startTime;
//        for (int i = 0; i < predictionGranularity.getNoInputs(); i++) {
//            energyPointValueDTOS.add(new EnergyPointValueDTO(0.0, DateUtils.localDateTimeToLongInMillis(time)));
//            time = time.plusMinutes(predictionGranularity.getSampleFrequencyMin());
//        }
//
//        for (AggregatedValuesDTO pav : historicalDisaggregatedConsumptionData) {
//            int index = (pav.getHour() - startTime.getHour())*(PredictionGranularity.DAYAHEAD.getSampleFrequencyMin() / predictionGranularity.getSampleFrequencyMin()) + pav.getMinute();
//            energyPointValueDTOS.get(index).setEnergyValue( (pav.getValue()));
//        }
//
//        ForecastDayAheadDTO forecastDayAheadDTO = new ForecastDayAheadDTO();
//        forecastDayAheadDTO.setEnergyPointValueDTOS(energyPointValueDTOS);
//
//        return forecastDayAheadDTO;
//    }


}
