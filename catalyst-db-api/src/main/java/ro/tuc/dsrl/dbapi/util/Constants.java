package ro.tuc.dsrl.dbapi.util;

public class Constants {

    private Constants(){

    }

    public static final int MINUTES_30 = 30;
    public static final int HOUR = 60;

    public static final int MONITORED_VALUES_GRANULARITY_MINUTES = 10;
    public static final int NUMBER_OF_DC_COMPONENTS = 2;

}
