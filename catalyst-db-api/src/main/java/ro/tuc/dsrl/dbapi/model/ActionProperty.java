//package ro.tuc.dsrl.dbapi.model;
//
//import org.hibernate.annotations.GenericGenerator;
//
//import javax.persistence.*;
//import javax.validation.constraints.NotNull;
//import java.util.UUID;
//
//@Entity
//@Table(name = "action_property")
//public class ActionProperty {
//
//    @Id
//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
//    @Column(name = "id", columnDefinition = "BINARY(16)")
//    private UUID id;
//
//    @Column(name = "property")
//    @NotNull
//    private String property;
//
//    @Column(name = "observation")
//    @NotNull
//    private String observation;
//
//    @OneToOne
//    @JoinColumn(name = "measure_unit_id")
//    @NotNull
//    private MeasureUnit measureUnitId;
//
//    @OneToOne
//    @JoinColumn(name = "value_type_id")
//    @NotNull
//    private ValueType valueTypeId;
//
//    @OneToOne
//    @JoinColumn(name = "action_type_id")
//    @NotNull
//    private ActionType actionTypeId;
//
//    public ActionProperty() {
//
//    }
//
//    public ActionProperty(@NotNull String property,
//                          @NotNull String observation,
//                          @NotNull MeasureUnit measureUnitId,
//                          @NotNull ValueType valueTypeId,
//                          @NotNull ActionType actionTypeId) {
//        this.property = property;
//        this.observation = observation;
//        this.measureUnitId = measureUnitId;
//        this.valueTypeId = valueTypeId;
//        this.actionTypeId = actionTypeId;
//    }
//
//    public ActionProperty(@NotNull UUID id,
//                          @NotNull String property,
//                          @NotNull String observation,
//                          @NotNull MeasureUnit measureUnitId,
//                          @NotNull ValueType valueTypeId,
//                          @NotNull ActionType actionTypeId) {
//
//        this(property, observation, measureUnitId, valueTypeId, actionTypeId);
//        this.id = id;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public String getProperty() {
//        return property;
//    }
//
//    public String getObservation() {
//        return observation;
//    }
//
//    public MeasureUnit getMeasureUnitId() {
//        return measureUnitId;
//    }
//
//    public ValueType getValueTypeId() {
//        return valueTypeId;
//    }
//
//    public ActionType getActionTypeId() {
//        return actionTypeId;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public void setProperty(String property) {
//        this.property = property;
//    }
//
//    public void setObservation(String observation) {
//        this.observation = observation;
//    }
//
//    public void setMeasureUnitId(MeasureUnit measureUnitId) {
//        this.measureUnitId = measureUnitId;
//    }
//
//    public void setValueTypeId(ValueType valueTypeId) {
//        this.valueTypeId = valueTypeId;
//    }
//
//    public void setActionTypeId(ActionType actionTypeId) {
//        this.actionTypeId = actionTypeId;
//    }
//}
