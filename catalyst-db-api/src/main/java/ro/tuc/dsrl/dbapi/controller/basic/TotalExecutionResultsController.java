//package ro.tuc.dsrl.dbapi.controller.basic;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MonitoredValueDTO;
//import ro.tuc.dsrl.catalyst.model.dto.TotalExecutionResultsDTO;
//import ro.tuc.dsrl.catalyst.model.enums.Datacenter;
//import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;
//import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
//import ro.tuc.dsrl.dbapi.controller.preconditions.TotalExecutionResultsPreconditions;
//import ro.tuc.dsrl.dbapi.service.basic.*;
//
//import java.time.LocalDateTime;
//import java.util.UUID;
//
//@RestController
//@CrossOrigin
//@RequestMapping(value = "/total-execution-results")
//public class TotalExecutionResultsController {
//
//    private final TotalExecutionResultsService totalExecutionResultsService;
//    private final MonitoredValueService monitoredValueService;
//    private final MeasurementService measurementService;
//    private final DeviceService deviceService;
//
//    @Autowired
//    public TotalExecutionResultsController(TotalExecutionResultsService totalExecutionResultsService,
//                                           MonitoredValueService monitoredValueService,
//                                           MeasurementService measurementService,
//                                           DeviceService deviceService){
//        this.totalExecutionResultsService = totalExecutionResultsService;
//        this.monitoredValueService = monitoredValueService;
//        this.measurementService = measurementService;
//        this.deviceService = deviceService;
//    }
//
//    @GetMapping(value =  "/current/{date}/{confidence}")
//    public TotalExecutionResultsDTO getLatestMonitoring(@PathVariable("date") Long currentDateMillis,
//                                                        @PathVariable("confidence") Double confidenceLevel){
//
//        TotalExecutionResultsPreconditions.validateCurrentDateAndConfidenceLevelThrowErrors(currentDateMillis, confidenceLevel);
//
//        LocalDateTime current = DateUtils.millisToUTCLocalDateTime(currentDateMillis);
//
//        return this.totalExecutionResultsService.getLatestMonitoring(current, confidenceLevel);
//
//    }
//
//    @PostMapping(value =  "/")
//    public void insertTotalExecutionResults(@RequestBody TotalExecutionResultsDTO newResults){
//
//        TotalExecutionResultsPreconditions.validateTotalExecutionResultsThrowErrors(newResults);
//
//        UUID datacenterID = deviceService.getDeviceIdFromLabel(Datacenter.DATACENTER_NAME.getDatacenterName());
//
//        LocalDateTime time = DateUtils.millisToLocalDateTime(newResults.getRecordTime());
//        Double batteryCurrent = newResults.getEsdCurrent();
//        Double tesCurrent = newResults.getTesCurrent();
//        Double serverRoom = newResults.getItPowerConsumptionDt() + newResults.getItPowerConsumptionRt();
//        Double coolingValues = newResults.getFacilityPowerConsumption();
//        Double relocateValues = newResults.getRealocEnergy();
//        Double selfThermalHeat  = newResults.getSelfThermalHeat();
//
//        UUID batteryId = measurementService.getMeasurementByProperty(MeasurementType.BATTERY_CURRENT.type());
//        monitoredValueService.insert(new MonitoredValueDTO(time, batteryCurrent, datacenterID, batteryId));
//
//        UUID tesId = measurementService.getMeasurementByProperty(MeasurementType.TES_CURRENT.type());
//        monitoredValueService.insert(new MonitoredValueDTO(time, tesCurrent, datacenterID, tesId));
//
//        UUID serverRoomId = measurementService.getMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type());
//        monitoredValueService.insert(new MonitoredValueDTO(time, serverRoom, datacenterID, serverRoomId));
//
//        UUID coolingSystemId = measurementService.getMeasurementByProperty(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
//        monitoredValueService.insert(new MonitoredValueDTO(time, coolingValues, datacenterID, coolingSystemId));
//
//        UUID rellocateId = measurementService.getMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION_HOST_RELOCATE.type());
//        monitoredValueService.insert(new MonitoredValueDTO(time, relocateValues, datacenterID, rellocateId));
//
//        UUID selfThermalHeatId = measurementService.getMeasurementByProperty(MeasurementType.SELF_THERMAL_HEAT.type());
//        monitoredValueService.insert(new MonitoredValueDTO(time, selfThermalHeat, datacenterID, selfThermalHeatId));
//
//        this.totalExecutionResultsService.insert(newResults);
//
//    }
//            }