package ro.tuc.dsrl.dbapi.controller.uisupport;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ro.tuc.dsrl.catalyst.model.enums.Scenario;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.OptimizationPreconditions;
import ro.tuc.dsrl.dbapi.model.dto.ui.optimization.FlexibilityStrategyGraphDTO;
import ro.tuc.dsrl.dbapi.service.uisupport.OptimizationSetupService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
public class OptimizationSetupController {
    @Value("${datacenter.name}")
    private String dcName;


    private final OptimizationSetupService optPlanUIService;

    public OptimizationSetupController(OptimizationSetupService optPlanUIService) {
        this.optPlanUIService = optPlanUIService;
    }

    @GetMapping(value = "/flexibility-strategy/{time}/{hour},{min}")
    public FlexibilityStrategyGraphDTO graphFS(@PathVariable("time") long time,
                                               @PathVariable("hour") int hour,
                                               @PathVariable("min") int min)
    {
        LocalDateTime currentTime = DateUtils.millisToLocalDateTime(time);
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(min);

        return optPlanUIService.getGraph(dcName, currentTime);
    }
}
