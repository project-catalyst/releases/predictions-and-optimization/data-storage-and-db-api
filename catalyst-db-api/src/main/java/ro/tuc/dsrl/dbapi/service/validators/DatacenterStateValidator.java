package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.optimization.DatacenterStateDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DatacenterStateValidator {

    private static final Log LOGGER = LogFactory.getLog(DatacenterStateValidator.class);

    private DatacenterStateValidator() {
    }

    public static void validateInsert(DatacenterStateDTO totalExecutionResultsDTO) {
        List<String> errors = new ArrayList<>();
        if (totalExecutionResultsDTO == null) {
            errors.add("Total Execution Results DTO is null");
            throw new IncorrectParameterException(DatacenterStateValidator.class.getSimpleName(), errors);
        }


        if (totalExecutionResultsDTO.getRecordTime() == 0) {
            errors.add("Total Execution Results recordTime is null or empty");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DatacenterStateValidator.class.getSimpleName(), errors);
        }
    }


    public static List<String> validateCurrentTimeAndConfidenceLevel(LocalDateTime current,
                                                                     Double confidenceLevel){

        List<String> errors = new ArrayList<>();

        if(current == null){
            errors.add("Current date must not be null");
        }

        if(confidenceLevel == null){
            errors.add("Confidence level must not be null");
        }

        return errors;

    }

    public static void validateCurrentTimeAndConfidenceLevelThrowErrors(LocalDateTime current,
                                                             Double confidenceLevel) {

        List<String> errors = validateCurrentTimeAndConfidenceLevel(current, confidenceLevel);
        throwIncorrectParameterException(errors);
    }

    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DatacenterStateDTO.class.getSimpleName(), errors);
        }
    }

}
