package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.ValueTypeDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.ValueTypeService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/value-type")
public class ValueTypeController {

    private final ValueTypeService valueTypeService;

    @Autowired
    public ValueTypeController(ValueTypeService valueTypeService) {
        this.valueTypeService = valueTypeService;
    }

    @GetMapping(value = "/byId/{valueTypeId}")
    public ValueTypeDTO findById(@PathVariable("valueTypeId") String valueTypeId) {

        UUIDPreconditions.validate(valueTypeId);

        UUID valueTypeUUID = UUID.fromString(valueTypeId);

        return valueTypeService.findById(valueTypeUUID);
    }

    @GetMapping(value = "/all")
    public List<ValueTypeDTO> findAll() {

        return valueTypeService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertValueTypeDTO(@RequestBody ValueTypeDTO valueTypeDTO) {

        return valueTypeService.insert(valueTypeDTO);
    }
}
