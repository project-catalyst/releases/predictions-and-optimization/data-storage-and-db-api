package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "flexibility_strategy")
public class FlexibilityStrategy {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "We")
    @NotNull
    private double We;

    @Column(name = "Wl")
    @NotNull
    private double Wl;

    @Column(name = "Wf")
    @NotNull
    private double Wf;

    @Column(name = "Wt")
    @NotNull
    private double Wt;

    @Column(name = "Wren")
    @NotNull
    private double Wren;

    @Column(name = "reallocActive")
    @NotNull
    private boolean reallocActive;

    @Column(name = "time")
    @NotNull
    private LocalDateTime time;

    @ManyToOne
    @JoinColumn(name = "datacenter")
    @NotNull
    private Device datacenter;

    public FlexibilityStrategy() { }

    public FlexibilityStrategy(@NotNull double we, @NotNull double wl, @NotNull double wf, @NotNull double wt, @NotNull double wren, @NotNull boolean reallocActive, @NotNull LocalDateTime time, @NotNull Device datacenter) {
        We = we;
        Wl = wl;
        Wf = wf;
        Wt = wt;
        Wren = wren;
        this.reallocActive = reallocActive;
        this.time = time;
        this.datacenter = datacenter;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public double getWe() {
        return We;
    }

    public void setWe(double we) {
        We = we;
    }

    public double getWl() {
        return Wl;
    }

    public void setWl(double wl) {
        Wl = wl;
    }

    public double getWf() {
        return Wf;
    }

    public void setWf(double wf) {
        Wf = wf;
    }

    public double getWt() {
        return Wt;
    }

    public void setWt(double wt) {
        Wt = wt;
    }

    public double getWren() {
        return Wren;
    }

    public void setWren(double wren) {
        Wren = wren;
    }

    public boolean isReallocActive() {
        return reallocActive;
    }

    public void setReallocActive(boolean reallocActive) {
        this.reallocActive = reallocActive;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Device getDatacenter() {
        return datacenter;
    }

    public void setDatacenter(Device datacenter) {
        this.datacenter = datacenter;
    }
}
