package ro.tuc.dsrl.dbapi.service.history;

import org.hibernate.cfg.NotYetImplementedException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.EnergyPointValueDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.Measurement;
import ro.tuc.dsrl.dbapi.model.MonitoredValue;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.MeasurementRepository;
import ro.tuc.dsrl.dbapi.repository.MonitoredValueRepository;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.MonitoredValueService;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;
import ro.tuc.dsrl.dbapi.util.Constants;
import ro.tuc.dsrl.dbapi.util.RepoConversionUtility;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static ro.tuc.dsrl.dbapi.util.Constants.MONITORED_VALUES_GRANULARITY_MINUTES;



@Service
public class EnergyHistoricalMonitoredValuesService {

    private final MonitoredValueRepository monitoredValueRepository;
    private final MeasurementRepository measurementRepository;
    private final DeviceRepository deviceRepository;
    private final DeviceService deviceService;
    private final MonitoredValueService monitoredValueService;
    private final String dcName;
    private boolean executionOn;
    private boolean serverroomSplit;

    @Autowired
    public EnergyHistoricalMonitoredValuesService(MonitoredValueRepository monitoredValueRepository,
                                                  MeasurementRepository measurementRepository,
                                                  DeviceRepository deviceRepository,
                                                  DeviceService deviceService,
                                                  MonitoredValueService monitoredValueService,
                                                  @Value("${datacenter.name}") String dcName,
                                                  @Value("${execution.on}") boolean executionOn,
                                                  @Value("${serverroom.split}") boolean serverroomSplit) {
        this.monitoredValueRepository = monitoredValueRepository;
        this.measurementRepository = measurementRepository;
        this.deviceRepository = deviceRepository;
        this.deviceService = deviceService;
        this.monitoredValueService = monitoredValueService;
        this.dcName = dcName;
        this.executionOn = executionOn;
        this.serverroomSplit = serverroomSplit;
    }

    //region Consumption
    public EnergyProfileDTO getConsumptionForEntireDC(UUID dataCenterID,
                                                      LocalDateTime startTime,
                                                      PredictionGranularity predictionGranularity,
                                                      String energyType,
                                                      Integer granularity,
                                                      String predictionType) {

        List<EnergyProfileDTO> energyProfilesForDC = new ArrayList<>();

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        List<EnergyProfileDTO> energyConsumptionForServer = this.getConsumptionForITComponent(dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                granularity,
                predictionType);

        EnergyProfileDTO energyConsumptionForCooling = this.getConsumptionForCoolingSystem(dataCenterID,
                startTime,
                predictionGranularity,
                energyType,
                granularity,
                predictionType);

        EnergyProfileDTO energyProfileConsumptionForDC = new EnergyProfileDTO();
        for(int i=0; i< energyConsumptionForServer.get(0).getCurve().size(); i++) {

            double energyValueDT = energyConsumptionForServer.get(0).getCurve().get(i).getEnergyValue();
            double energyValueRT = energyConsumptionForServer.get(1).getCurve().get(i).getEnergyValue();
            double energyValueCooling = energyConsumptionForCooling.getCurve().get(i).getEnergyValue();
            energyConsumptionForServer.get(0).getCurve().get(i).setEnergyValue(
                    energyValueDT + energyValueRT + energyValueCooling);
        }
        energyProfileConsumptionForDC=energyConsumptionForServer.get(0);

        return energyProfileConsumptionForDC;

    }

    private List<MonitoredValue> getEnergySampledForComponent(Object[][] results,
                                                              Device device,
                                                              Measurement measurement,
                                                              LocalDateTime startTime,
                                                              PredictionGranularity predictionGranularity){

        List<MonitoredValue> sampledValues = new ArrayList<>();
        List<AggregatedValuesDTO> historicalNewlyConsumptionData = RepoConversionUtility.getAveragedValues(results, device, measurement);

        if (!historicalNewlyConsumptionData.isEmpty()) {
            List<EnergyPointValueDTO> energyPointValueDTOS = new ArrayList<>();

            LocalDateTime time = startTime;
            for (int i = 0; i < predictionGranularity.getNoInputs(); i++) {
                energyPointValueDTOS.add(new EnergyPointValueDTO(0.0, DateUtils.localDateTimeToLongInMillis(time)));
                time = time.plusMinutes(predictionGranularity.getSampleFrequencyMin());
            }

            if(predictionGranularity.getGranularity().equals("DAYAHEAD")){
                for (AggregatedValuesDTO mav : historicalNewlyConsumptionData) {
                    int index = (mav.getHour() - startTime.getHour()) * (predictionGranularity.getSampleFrequencyMin() / predictionGranularity.getSampleFrequencyMin()) + mav.getMinute();
                    energyPointValueDTOS.get(index).setEnergyValue((mav.getValue()));
                }
            }
            else if(predictionGranularity.getGranularity().equals("INTRADAY")){
                for (AggregatedValuesDTO mav : historicalNewlyConsumptionData) {
                    int index = 2*(mav.getHour() - startTime.getHour()) + mav.getMinute()/predictionGranularity.getSampleFrequencyMin();
                    energyPointValueDTOS.get(index).setEnergyValue((mav.getValue()));
                }
            }

            // transform list of energy point values into list of monitored values
            for (int i = 0; i < energyPointValueDTOS.size(); i++) {
                sampledValues.add(new MonitoredValue(Instant.ofEpochMilli(energyPointValueDTOS.get(i).getTimeStamp()).atZone(ZoneOffset.UTC).toLocalDateTime(),
                        energyPointValueDTOS.get(i).getEnergyValue(),
                        device,
                        measurement));
            }
        }

        return sampledValues;
    }

    public EnergyProfileDTO getConsumptionForEntireDCFixed(UUID dataCenterID,
                                                           LocalDateTime startTime,
                                                           PredictionGranularity predictionGranularity,
                                                           String energyType,
                                                           Integer granularity,
                                                           String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        List<AggregatedValuesDTO> historicalConsumptionData = monitoredValueRepository.findDataInIntervalForEntireDCFixed(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                predictionType,
                predictionGranularity.getSampleFrequencyMin());

        List<MonitoredValue> energyDataValues = new ArrayList<>();
        int dbDataIndex = 0;
        int dbDataLimit = historicalConsumptionData.size();
        for (LocalDateTime time = startTime; time.isBefore(endTime) ; time = time.plusMinutes(predictionGranularity.getSampleFrequencyMin())) {
            AggregatedValuesDTO newElement  =new AggregatedValuesDTO(0.0, time.getYear(), time.getMonthValue(), time.getDayOfMonth(), time.getHour(), (double)time.getMinute());
            while(dbDataIndex < dbDataLimit && historicalConsumptionData.get(dbDataIndex).getDateTime().equals(newElement.getDateTime())) {
                newElement.setValue(newElement.getValue() + historicalConsumptionData.get(dbDataIndex).getValue());
                dbDataIndex++;
            }
            energyDataValues.add(new MonitoredValue(newElement.getDateTime(),newElement.getValue(), null, newElement.getMeasurementID()));
        }

        if (!energyDataValues.isEmpty()) {
            return this.getEnergyProfileBasedOnData(energyDataValues,
                    PredictionType.ENERGY_CONSUMPTION.name(),
                    energyType,
                    predictionGranularity,
                    true,
                    dataCenterID);
        }


        return EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);

    }


    public List<EnergyProfileDTO> getConsumptionForITComponent(UUID dataCenterID,
                                                         LocalDateTime startTime,
                                                         PredictionGranularity predictionGranularity,
                                                         String energyType,
                                                         Integer granularity,
                                                         String predictionType) {

        List<EnergyProfileDTO> dtAndRTServerSignals = new ArrayList<>();

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());
        List<MonitoredValue> sampledValuesServerDT = new ArrayList<>();
        List<MonitoredValue> sampledValuesServerRT = new ArrayList<>();

        // find DT and RT averaged signals
        sampledValuesServerDT = this.findServerDTConsumptionAaveragedSignal(dataCenterID, startTime, predictionGranularity,
                energyType, granularity, predictionType);

        sampledValuesServerRT = this.findServerRTConsumptionAveragedSignal(dataCenterID, startTime, predictionGranularity,
                energyType, granularity, predictionType);

        // find server room measurements
        Measurement measurementServer;
        Measurement measurementServerDT;
        Measurement measurementServerRT;

        if(executionOn) {
            energyType = "PRE_OPT_" + energyType;
            measurementServer = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_CONSUMPTION.type());
            if (measurementServer == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_CONSUMPTION.type());
            }
            measurementServerDT = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION.type());
            if (measurementServerDT == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION.type());
            }
            measurementServerRT = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            if (measurementServerRT == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            }
        }else{
            measurementServer = measurementRepository.findMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type());
            if (measurementServer == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type());
            }

            measurementServerDT = measurementRepository.findMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION.type());
            if (measurementServerDT == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION.type());
            }
            measurementServerRT = measurementRepository.findMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            if (measurementServerRT == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            }
        }

        // find server room device
        Device deviceServer = deviceRepository.findDeviceByDeviceType(DeviceTypeEnum.SERVER_ROOM.type());
        if (deviceServer == null) {
            throw new ResourceNotFoundException("Device not found for device type " + DeviceTypeEnum.SERVER_ROOM.type());
        }

        if(!serverroomSplit){

            // compute the DT and RT curves as percentages
            //search then for the overall server consumption
            predictionType = predictionType + "_SERVER_ROOM";
            Object[][] resultsServer = monitoredValueRepository.findAveragedSignalsInIntervalForComponent(dataCenterID,
                startTime, endTime, energyType, DeviceTypeEnum.SERVER_ROOM.type(), predictionType, predictionGranularity.getSampleFrequencyMin());

            List<MonitoredValue> sampledValuesServer = this.getEnergySampledForComponent(resultsServer, deviceServer,
                measurementServer, startTime, predictionGranularity);

            // then split values into two different curves for RT and DT
            List<MonitoredValue> sampledValuesRT = new ArrayList<>();
            List<MonitoredValue> sampledValuesDT = new ArrayList<>();
            double dt_ratio = 0.0;
            double rt_ratio = 0.0;
            List<ServerRoom> serverRooms = deviceService.getServerRoom(dcName, startTime);
            for (ServerRoom serverRoom : serverRooms){
                dt_ratio = serverRoom.getEdPercentage();
                rt_ratio = 1.0 - dt_ratio;
            }
            for(MonitoredValue m: sampledValuesServer){
                sampledValuesRT.add(new MonitoredValue(m.getTimestamp(), rt_ratio * m.getValue(), m.getDeviceId(), m.getMeasurementId()));
                sampledValuesDT.add(new MonitoredValue(m.getTimestamp(), dt_ratio * m.getValue(), m.getDeviceId(), m.getMeasurementId()));
            }

            if (!sampledValuesDT.isEmpty()) {

                EnergyProfileDTO dtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesDT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity,
                        false,
                        null);
                dtEnergyProfileDTO.setDeviceId(deviceServer.getId());
                dtEnergyProfileDTO.setMeasurementId(measurementServerDT.getId());
                dtAndRTServerSignals.add(dtEnergyProfileDTO);
            }else
            {
                EnergyProfileDTO dtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstanceExpanded(startTime, predictionGranularity,
                        deviceServer.getId(), measurementServerDT.getId(), energyType, granularity);
                dtAndRTServerSignals.add(dtEnergyProfileDTO);
            }

            if (!sampledValuesRT.isEmpty()) {

                EnergyProfileDTO rtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesRT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity,
                        false,
                        null);
                rtEnergyProfileDTO.setDeviceId(deviceServer.getId());
                rtEnergyProfileDTO.setMeasurementId(measurementServerRT.getId());
                dtAndRTServerSignals.add(rtEnergyProfileDTO);

            }else
            {
                EnergyProfileDTO rtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstanceExpanded(startTime, predictionGranularity,
                        deviceServer.getId(), measurementServerRT.getId(), energyType, granularity);
                dtAndRTServerSignals.add(rtEnergyProfileDTO);
            }


        }else{

            // both servert RT and DT monitorings exist
            // return both curves in the list
            if (!sampledValuesServerDT.isEmpty()) {

                EnergyProfileDTO dtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesServerDT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity,
                        false,
                        null);
                dtEnergyProfileDTO.setDeviceId(deviceServer.getId());
                dtEnergyProfileDTO.setMeasurementId(measurementServerDT.getId());
                dtAndRTServerSignals.add(dtEnergyProfileDTO);

            }else {

                EnergyProfileDTO dtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstanceExpanded(startTime, predictionGranularity,
                        deviceServer.getId(), measurementServerDT.getId(), energyType, granularity);
                dtAndRTServerSignals.add(dtEnergyProfileDTO);
            }

            if (!sampledValuesServerRT.isEmpty()) {

                EnergyProfileDTO rtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesServerRT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity,
                        false,
                        null);
                rtEnergyProfileDTO.setDeviceId(deviceServer.getId());
                rtEnergyProfileDTO.setMeasurementId(measurementServerRT.getId());
                dtAndRTServerSignals.add(rtEnergyProfileDTO);
            }else
            {
                EnergyProfileDTO rtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstanceExpanded(startTime, predictionGranularity,
                        deviceServer.getId(), measurementServerRT.getId(), energyType, granularity);
                dtAndRTServerSignals.add(rtEnergyProfileDTO);
            }
        }

     return dtAndRTServerSignals;

    }

    public EnergyProfileDTO getRTConsumptionForITComponentBetweenDates(UUID dataCenterID,
                                                               LocalDateTime startTime,
                                                               LocalDateTime endTime,
                                                               PredictionGranularity predictionGranularity,
                                                               String energyType,
                                                               Integer granularity,
                                                               String predictionType) {

        EnergyProfileDTO rtEnergyProfileDTO = new EnergyProfileDTO();

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        List<MonitoredValue> sampledValuesServerRT = new ArrayList<>();

        // find DT and RT averaged signals
        sampledValuesServerRT = this.findServerRTConsumptionAveragedSignalBetweenDates(dataCenterID, startTime, endTime,
                predictionGranularity, energyType, granularity, predictionType);

        // find server room measurements
        Measurement measurementServer;
        if(executionOn) {
            measurementServer = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_CONSUMPTION.type());
            if (measurementServer == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_CONSUMPTION.type());
            }
        }else{
            measurementServer = measurementRepository.findMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type());
            if (measurementServer == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION.type());
            }
        }

        Measurement measurementServerRT;
        if(executionOn){
             measurementServerRT = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            if (measurementServerRT == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            }
        }else {
            measurementServerRT = measurementRepository.findMeasurementByProperty(MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            if (measurementServerRT == null) {
                throw new ResourceNotFoundException("Measurement not found measurement type" + MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type());
            }
        }

        // find server room device
        Device deviceServer = deviceRepository.findDeviceByDeviceType(DeviceTypeEnum.SERVER_ROOM.type());
        if (deviceServer == null) {
            throw new ResourceNotFoundException("Device not found for device type " + DeviceTypeEnum.SERVER_ROOM.type());
        }

        if(sampledValuesServerRT == null || sampledValuesServerRT.isEmpty()){

            // compute the RT curves as percentages
            //search then for the overall server consumption
            Object[][] resultsServer = monitoredValueRepository.findAveragedSignalsInIntervalForComponent(dataCenterID,
                    startTime, endTime, energyType, DeviceTypeEnum.SERVER_ROOM.type(), predictionType, predictionGranularity.getSampleFrequencyMin());

            List<MonitoredValue> sampledValuesServer = this.getEnergySampledForComponentBetweenDates(resultsServer, deviceServer,
                    measurementServer, startTime, endTime, predictionGranularity);

            // then split values into two different curves for RT and DT
            List<MonitoredValue> sampledValuesRT = new ArrayList<>();
            List<MonitoredValue> sampledValuesDT = new ArrayList<>();
            double dt_ratio = 0.0;
            double rt_ratio = 0.0;
            List<ServerRoom> serverRooms = deviceService.getServerRoom(dcName, startTime);
            for (ServerRoom serverRoom : serverRooms){
                dt_ratio = serverRoom.getEdPercentage();
                rt_ratio = 1.0 - dt_ratio;
            }
            for(MonitoredValue m: sampledValuesServer){
                sampledValuesRT.add(new MonitoredValue(m.getTimestamp(), rt_ratio * m.getValue(), m.getDeviceId(), m.getMeasurementId()));
                sampledValuesDT.add(new MonitoredValue(m.getTimestamp(), dt_ratio * m.getValue(), m.getDeviceId(), m.getMeasurementId()));
            }

            if (!sampledValuesRT.isEmpty()) {

                rtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesRT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity,
                        false,
                        null);
                rtEnergyProfileDTO.setDeviceId(deviceServer.getId());
                rtEnergyProfileDTO.setMeasurementId(measurementServerRT.getId());

            }else
            {
                rtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstanceExpanded(startTime, predictionGranularity,
                        deviceServer.getId(), measurementServerRT.getId(), energyType, granularity);
            }


        }else{

            // server RT monitorings exist
            // return curve in the list
            if (!sampledValuesServerRT.isEmpty()) {

                rtEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesServerRT,
                        PredictionType.ENERGY_CONSUMPTION.name(),
                        energyType,
                        predictionGranularity,
                        false,
                        null);
                rtEnergyProfileDTO.setDeviceId(deviceServer.getId());
                rtEnergyProfileDTO.setMeasurementId(measurementServerRT.getId());
            }else
            {
                rtEnergyProfileDTO = EnergyProfileDTO.getDefaultInstanceExpanded(startTime, predictionGranularity,
                        deviceServer.getId(), measurementServerRT.getId(), energyType, granularity);
            }
        }

        return rtEnergyProfileDTO;

    }

    public EnergyProfileDTO getConsumptionForCoolingSystem(UUID dataCenterID,
                                                           LocalDateTime startTime,
                                                           PredictionGranularity predictionGranularity,
                                                           String energyType,
                                                           Integer granularity,
                                                           String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

//        Object[][] resultsCooling = monitoredValueRepository.findAveragedSignalsInIntervalForComponent(
//                dataCenterID,
//                startTime,
//                endTime,
//                energyType,
//                DeviceTypeEnum.COOLING_SYSTEM.type(),
//                predictionType,
//                predictionGranularity.getSampleFrequencyMin());
//
//        //Device deviceCooling = deviceRepository.findDeviceByLabel(deviceRepository.findByUUID(dataCenterID).getLabel());
        Device deviceCooling = deviceRepository.findDeviceByDeviceType(DeviceTypeEnum.COOLING_SYSTEM.type());
        if (deviceCooling == null) {
            throw new ResourceNotFoundException("Device not found for device type " + DeviceTypeEnum.COOLING_SYSTEM.type());
        }

        Measurement measurementCooling;
        MeasurementType measurementCoolingType;
        if(executionOn) {
            measurementCoolingType = MeasurementType.PRE_OPT_COOLING_SYSTEM_CURRENT_CONSUMPTION;
            measurementCooling = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
            if (measurementCooling == null) {
                throw new ResourceNotFoundException("Measurement not found for device " + deviceCooling.getId());
            }
        }else{
            measurementCoolingType = MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION;
            measurementCooling = measurementRepository.findMeasurementByProperty(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
            if (measurementCooling == null) {
                throw new ResourceNotFoundException("Measurement not found for device " + deviceCooling.getId());
            }
        }

        List<AggregatedValuesDTO> resultsCooling = monitoredValueService.getAveragedMonitoredValues(dcName, startTime, endTime, predictionGranularity, measurementCoolingType);



        if (!resultsCooling.isEmpty()) {
            EnergyProfileDTO energyProfileDTO = this.getEnergyProfileBasedOnAggregatedData(resultsCooling,
                                                                                PredictionType.ENERGY_CONSUMPTION.name(),
                                                                                energyType,
                                                                                predictionGranularity,
                                                                                deviceCooling.getId(),
                                                                                measurementCooling.getId());
            energyProfileDTO.setDeviceId(deviceCooling.getId());
            energyProfileDTO.setMeasurementId(measurementCooling.getId());
            energyProfileDTO.setEnergyType(EnergyType.setType(energyType));
            energyProfileDTO.setPredictionGranularity(predictionGranularity);
            energyProfileDTO.setAggregationGranularity(AggregationGranularity.setGranularity(granularity));
            return energyProfileDTO;
        }
        else

            return EnergyProfileDTO.getDefaultInstanceExpanded(startTime,
                    predictionGranularity,
                    deviceCooling.getId(),
                    measurementCooling.getId(),
                    energyType,
                    granularity);
    }

    private EnergyProfileDTO getEnergyProfileBasedOnAggregatedData(List<AggregatedValuesDTO> data,
                                                                   String predictionType,
                                                                   String energyType,
                                                                   PredictionGranularity predictionGranularity,
                                                                   UUID deviceId,
                                                                   UUID measurementId) {



        List<EnergySampleDTO> energySampleDTOs = new ArrayList<>();
        for (AggregatedValuesDTO m : data) {
            LocalDateTime localDateTime =  LocalDateTime.of(m.getYear(),m.getMonth(), m.getDay(),m.getHour(),m.getMinute(),0  );
            energySampleDTOs.add(new EnergySampleDTO(m.getValue(), localDateTime));
        }

        EnergyProfileDTO energyProfileDTO;

        switch (predictionGranularity) {

            case NEAR_REAL_TIME:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOs,
                        deviceId,
                        measurementId,
                        AggregationGranularity.HOUR,
                        PredictionGranularity.NEAR_REAL_TIME,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            case INTRADAY:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOs,
                        deviceId,
                        measurementId,
                        AggregationGranularity.MINUTES_30,
                        PredictionGranularity.INTRADAY,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            case DAYAHEAD:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOs,
                        deviceId,
                        measurementId,
                        AggregationGranularity.HOUR,
                        PredictionGranularity.DAYAHEAD,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            default:
                energyProfileDTO = new EnergyProfileDTO();
                break;
        }

        return energyProfileDTO;
    }


    public EnergyProfileDTO getConsumptionForCoolingSystemOLD(UUID dataCenterID,
                                                           LocalDateTime startTime,
                                                           PredictionGranularity predictionGranularity,
                                                           String energyType,
                                                           Integer granularity,
                                                           String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        Object[][] resultsCooling = monitoredValueRepository.findAveragedSignalsInIntervalForComponent(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.COOLING_SYSTEM.type(),
                predictionType,
                predictionGranularity.getSampleFrequencyMin());

        //Device deviceCooling = deviceRepository.findDeviceByLabel(deviceRepository.findByUUID(dataCenterID).getLabel());
        Device deviceCooling = deviceRepository.findDeviceByDeviceType(DeviceTypeEnum.COOLING_SYSTEM.type());
        if (deviceCooling == null) {
            throw new ResourceNotFoundException("Device not found for device type " + DeviceTypeEnum.COOLING_SYSTEM.type());
        }

        Measurement measurementCooling;
        if(executionOn) {
            measurementCooling = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
            if (measurementCooling == null) {
                throw new ResourceNotFoundException("Measurement not found for device " + deviceCooling.getId());
            }
        }else{
            measurementCooling = measurementRepository.findMeasurementByProperty(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
            if (measurementCooling == null) {
                throw new ResourceNotFoundException("Measurement not found for device " + deviceCooling.getId());
            }
        }

        List<MonitoredValue> sampledValuesCooling = this.getEnergySampledForComponent(resultsCooling,
                deviceCooling,
                measurementCooling,
                startTime,
                predictionGranularity);

        if (!sampledValuesCooling.isEmpty()) {
            EnergyProfileDTO energyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesCooling,
                    PredictionType.ENERGY_CONSUMPTION.name(),
                    energyType,
                    predictionGranularity,
                    false,
                    null);
            energyProfileDTO.setDeviceId(deviceCooling.getId());
            energyProfileDTO.setMeasurementId(measurementCooling.getId());
            energyProfileDTO.setEnergyType(EnergyType.setType(energyType));
            energyProfileDTO.setPredictionGranularity(predictionGranularity);
            energyProfileDTO.setAggregationGranularity(AggregationGranularity.setGranularity(granularity));
            return energyProfileDTO;
        }
        else

            return EnergyProfileDTO.getDefaultInstanceExpanded(startTime,
                    predictionGranularity,
                    deviceCooling.getId(),
                    measurementCooling.getId(),
                    energyType,
                    granularity);
    }

    public EnergyProfileDTO getConsumptionForServer(UUID dataCenterID,
                                                    LocalDateTime startTime,
                                                    UUID serverID,
                                                    PredictionGranularity predictionGranularity,
                                                    String energyType,
                                                    Integer granularity,
                                                    String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, serverID, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        if (true) throw new NotYetImplementedException(MonitoredValueRepository.class.toString());
        List<MonitoredValue> historicalConsumptionData = monitoredValueRepository.findDataInIntervalForComponentServer(
                dataCenterID,
                serverID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.SERVER.type(),
                predictionType);

        List<MonitoredValue> sampledValues = sampleDbData(historicalConsumptionData, granularity);

        if (!sampledValues.isEmpty()) {
            return this.getEnergyProfileBasedOnData(sampledValues,
                    PredictionType.ENERGY_CONSUMPTION.name(),
                    energyType,
                    predictionGranularity,
                    false,
                    null);
        }

        return null;
    }
    //endregion


    //region Production
    public EnergyProfileDTO getProductionForEntireDC(UUID dataCenterID,
                                                     LocalDateTime startTime,
                                                     PredictionGranularity predictionGranularity,
                                                     String energyType,
                                                     Integer granularity,
                                                     String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        List<AggregatedValuesDTO> historicalConsumptionData = monitoredValueRepository.findDataInIntervalForEntireDC(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                predictionType,
                predictionGranularity.getSampleFrequencyMin());

        List<MonitoredValue> sampledValues = sampleDbData(historicalConsumptionData);

        if (!sampledValues.isEmpty()) {
            return this.getEnergyProfileBasedOnData(sampledValues,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity,
                    true,
                    dataCenterID);
        }

        return EnergyProfileDTO.getDefaultInstance(startTime,predictionGranularity);
    }

    public EnergyProfileDTO getProductionForITComponent(UUID dataCenterID,
                                                        LocalDateTime startTime,
                                                        PredictionGranularity predictionGranularity,
                                                        String energyType,
                                                        int granularity,
                                                        String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        List<AggregatedValuesDTO> historicalConsumptionData = monitoredValueRepository.findDataInIntervalForComponent(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.SERVER_ROOM.type(),
                predictionType,
                predictionGranularity.getSampleFrequencyMin());

        List<MonitoredValue> sampledValues = dcAverageValuesToMonitoredValues(historicalConsumptionData);

        if (!sampledValues.isEmpty()) {
            return this.getEnergyProfileBasedOnData(sampledValues,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity,
                    false,
                    null);
        }

        return EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
    }

    public EnergyProfileDTO getProductionForCoolingSystem(UUID dataCenterID,
                                                          LocalDateTime startTime,
                                                          PredictionGranularity predictionGranularity,
                                                          String energyType,
                                                          String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        List<AggregatedValuesDTO> historicalConsumptionData = monitoredValueRepository.findDataInIntervalForComponent(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.COOLING_SYSTEM.type(),
                predictionType,
                predictionGranularity.getSampleFrequencyMin());

        List<MonitoredValue> sampledValues = dcAverageValuesToMonitoredValues(historicalConsumptionData);

        if (!sampledValues.isEmpty()) {
            return this.getEnergyProfileBasedOnData(sampledValues,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity,
                    false,
                    null);
        }

        return EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
    }

    public EnergyProfileDTO getProductionForServer(UUID dataCenterID,
                                                   UUID serverID,
                                                   LocalDateTime startTime,
                                                   PredictionGranularity predictionGranularity,
                                                   String energyType,
                                                   Integer granularity,
                                                   String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, predictionGranularity, serverID, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        // find energy data in interval
        if (true) throw new NotYetImplementedException(MonitoredValueRepository.class.toString());
        List<MonitoredValue> historicalConsumptionData = monitoredValueRepository.findDataInIntervalForComponentServer(
                dataCenterID,
                serverID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.SERVER.type(),
                predictionType);

        List<MonitoredValue> sampledValues = sampleDbData(historicalConsumptionData, granularity);

        if (!sampledValues.isEmpty()) {
            return this.getEnergyProfileBasedOnData(sampledValues,
                    PredictionType.ENERGY_PRODUCTION.name(),
                    energyType,
                    predictionGranularity,
                    false,
                    null);
        }

        return EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
    }
    //endregion


    private EnergyProfileDTO getEnergyProfileBasedOnData(List<MonitoredValue> data,
                                                         String predictionType,
                                                         String energyType,
                                                         PredictionGranularity predictionGranularity,
                                                         boolean isDataCenter,
                                                         UUID dataCenterId) {

        // find device used for measuring the data
        UUID deviceId = isDataCenter
                ? dataCenterId
                : data.get(0).getDeviceId().getId();

        // find measurement for the data
        UUID measurementId = isDataCenter
                ? measurementRepository.findByProperty("ELECTRICAL_POWER_CONSUMPTION")
                : data.get(0).getMeasurementId().getId();

        List<EnergySampleDTO> energySampleDTOs = new ArrayList<>();
        for (MonitoredValue m : data) {
            energySampleDTOs.add(new EnergySampleDTO(m.getValue(), m.getTimestamp()));
        }

        EnergyProfileDTO energyProfileDTO;

        switch (predictionGranularity) {

            case NEAR_REAL_TIME:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOs,
                        deviceId,
                        measurementId,
                        AggregationGranularity.HOUR,
                        PredictionGranularity.NEAR_REAL_TIME,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            case INTRADAY:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOs,
                        deviceId,
                        measurementId,
                        AggregationGranularity.MINUTES_30,
                        PredictionGranularity.INTRADAY,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            case DAYAHEAD:
                energyProfileDTO = new EnergyProfileDTO(energySampleDTOs,
                        deviceId,
                        measurementId,
                        AggregationGranularity.HOUR,
                        PredictionGranularity.DAYAHEAD,
                        PredictionType.setType(predictionType),
                        EnergyType.setType(energyType));
                break;

            default:
                energyProfileDTO = new EnergyProfileDTO();
                break;
        }

        return energyProfileDTO;
    }

    private static List<MonitoredValue> sampleDbData(List<MonitoredValue> monitoredValues, Integer granularity) {

        List<MonitoredValue> sampledValues = new ArrayList<>();
        int step = granularity / MONITORED_VALUES_GRANULARITY_MINUTES;
        float totalValue = 0;

        for (int i = 0; i < monitoredValues.size(); i++) {
            totalValue += monitoredValues.get(i).getValue();

            if ((i + 1) % step == 0) {
                sampledValues.add(new MonitoredValue(monitoredValues.get(i).getTimestamp(),
                        totalValue / step,
                        monitoredValues.get(i).getDeviceId(),
                        monitoredValues.get(i).getMeasurementId()));

                totalValue = 0;
            }
        }

        return sampledValues;
    }

    /**
     * @param monitoredValues - list of aggregated values for SERVER_ROOM and COOLING_SYSTEM ordered by
     *                        Device, Measurement, Year, Month, Day, Hour, Minute
     * @return list of summed corresponding values, representing the DATA_CENTER (SERVER_ROOM + COOLING_SYSTEM)
     */
    private static List<MonitoredValue> sampleDbData(List<AggregatedValuesDTO> monitoredValues) {

        int numberOfSamplesPerDevice = monitoredValues.size() / Constants.NUMBER_OF_DC_COMPONENTS;

        List<MonitoredValue> dcMonitoredValues = new ArrayList<>();
        for (int i = 0; i < numberOfSamplesPerDevice; i++) {

            AggregatedValuesDTO serverValue = monitoredValues.get(i);

            MonitoredValue mv = new MonitoredValue(
                    LocalDateTime.of(serverValue.getYear(), serverValue.getMonth(), serverValue.getDay(),
                            serverValue.getHour(), serverValue.getMinute()),
                    serverValue.getValue() + monitoredValues.get(i + numberOfSamplesPerDevice).getValue(),
                    serverValue.getDeviceID(),
                    serverValue.getMeasurementID()
            );

            dcMonitoredValues.add(mv);
        }

        return dcMonitoredValues;
    }

    private static List<MonitoredValue> dcAverageValuesToMonitoredValues(List<AggregatedValuesDTO> averageValues) {
        return averageValues.stream()
                .map(EnergyHistoricalMonitoredValuesService::averageValueToMonitoredValue)
                .collect(Collectors.toList());
    }

    private static MonitoredValue averageValueToMonitoredValue(AggregatedValuesDTO averageValue) {
        return new MonitoredValue(
                LocalDateTime.of(averageValue.getYear(), averageValue.getMonth(), averageValue.getDay(),
                        averageValue.getHour(), averageValue.getMinute()),
                averageValue.getValue(),
                averageValue.getDeviceID(),
                averageValue.getMeasurementID()
        );
    }

    public EnergyProfileDTO getConsumptionForEntireDCBetweenDates(UUID dataCenterID,
                                                                LocalDateTime startTime,
                                                                  LocalDateTime endTime,
                                                                PredictionGranularity predictionGranularity,
                                                                String energyType,
                                                                Integer granularity,
                                                                String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, endTime, predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        //training data represents all monitored data between introduced dates

        // find energy data in interval
        List<AggregatedValuesDTO> historicalDisaggregatedConsumptionData = monitoredValueRepository.findDataBetweenDatesForEntireDC(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                predictionType,
                predictionGranularity.getSampleFrequencyMin());

        List<MonitoredValue> sampledValues = sampleDbData(historicalDisaggregatedConsumptionData);

        if (!sampledValues.isEmpty()) {
            return this.getEnergyProfileBasedOnData(sampledValues,
                    PredictionType.ENERGY_CONSUMPTION.name(),
                    energyType,
                    predictionGranularity,
                    true,
                    dataCenterID);
        }


        return null;

    }

    public EnergyProfileDTO getConsumptionForCoolingSystemBetweenDates(UUID dataCenterID,
                                                                     LocalDateTime startTime,
                                                                       LocalDateTime endTime,
                                                                     PredictionGranularity predictionGranularity,
                                                                     String energyType,
                                                                     int granularity,
                                                                     String predictionType) {

        List<String> errors = EnergyProfileValidator.validateParams(dataCenterID, startTime, endTime,predictionGranularity, energyType, granularity);
        EnergyProfileValidator.throwIncorrectParameterException(errors);

        Object[][] resultsCooling = monitoredValueRepository.findAveragedSignalsInIntervalForComponent(
                dataCenterID,
                startTime,
                endTime,
                energyType,
                DeviceTypeEnum.COOLING_SYSTEM.type(),
                predictionType,
                predictionGranularity.getSampleFrequencyMin());

        Device deviceCooling = deviceRepository.findDeviceByDeviceType(DeviceTypeEnum.COOLING_SYSTEM.type());
        if (deviceCooling == null) {
            throw new ResourceNotFoundException("Device not found for device type " + DeviceTypeEnum.COOLING_SYSTEM.type());
        }

        Measurement measurementCooling;
        if(executionOn) {
            measurementCooling = measurementRepository.findMeasurementByProperty(MeasurementType.PRE_OPT_COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
            if (measurementCooling == null) {
                throw new ResourceNotFoundException("Measurement not found for device " + deviceCooling.getId());
            }
        }else{
            measurementCooling = measurementRepository.findMeasurementByProperty(MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION.type());
            if (measurementCooling == null) {
                throw new ResourceNotFoundException("Measurement not found for device " + deviceCooling.getId());
            }
        }

        List<MonitoredValue> sampledValuesCooling = new ArrayList<>();
        for(LocalDateTime l=startTime; l.isBefore(endTime); l=l.plusMinutes(predictionGranularity.getNoOutputs() * predictionGranularity.getSampleFrequencyMin())){

            //compute ther energy sampledd values for each sub-curve
            List<MonitoredValue> monitoredValuesPerSubCurve = this.getEnergySampledForComponent(resultsCooling,
                    deviceCooling,
                    measurementCooling,
                    l,
                    predictionGranularity);

            for(int i=0; i<monitoredValuesPerSubCurve.size(); i++){
                sampledValuesCooling.add(monitoredValuesPerSubCurve.get(i));
            }
        }

        EnergyProfileDTO coolingEnergyProfileDTO;
        if (!sampledValuesCooling.isEmpty()) {

            coolingEnergyProfileDTO = this.getEnergyProfileBasedOnData(sampledValuesCooling,
                    PredictionType.ENERGY_CONSUMPTION.name(),
                    energyType,
                    predictionGranularity,
                    false,
                    null);
            coolingEnergyProfileDTO.setDeviceId(deviceCooling.getId());
            coolingEnergyProfileDTO.setMeasurementId(measurementCooling.getId());

        }else {

            coolingEnergyProfileDTO = EnergyProfileDTO.getDefaultInstanceExpanded(startTime, predictionGranularity,
                    deviceCooling.getId(), measurementCooling.getId(), energyType, granularity);
        }

        return coolingEnergyProfileDTO;
    }

    public List<MonitoredValue> findServerDTConsumptionAaveragedSignal(UUID dataCenterID,
                                                                   LocalDateTime startTime,
                                                                   PredictionGranularity predictionGranularity,
                                                                   String energyType,
                                                                   Integer granularity,
                                                                   String predictionType){

        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());

        Object[][] dtServerEnergyProfileDTO;
        if(executionOn) {
            dtServerEnergyProfileDTO = monitoredValueRepository.findEstimatedProfileValues(
                    dataCenterID,
                    startTime,
                    endTime,
                    energyType,
                    DeviceTypeEnum.SERVER_ROOM.type(),
                    MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_DT_CONSUMPTION.type(),
                    predictionType,
                    granularity);
        }
        else{
            dtServerEnergyProfileDTO = monitoredValueRepository.findEstimatedProfileValues(
                    dataCenterID,
                    startTime,
                    endTime,
                    energyType,
                    DeviceTypeEnum.SERVER_ROOM.type(),
                    MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION.type(),
                    predictionType,
                    granularity);
        }

        Device deviceServerDT = deviceRepository.findDeviceByLabel(deviceRepository.findByUUID(dataCenterID).getLabel());
        if (deviceServerDT == null) {
            throw new ResourceNotFoundException("Device type " + deviceServerDT.toString());
        }

        Measurement measurementServerDT = measurementRepository.findMeasurementByDeviceId(deviceServerDT.getDeviceTypeId().getId(),
                energyType, predictionType);
        if (measurementServerDT == null) {
            throw new ResourceNotFoundException("Measurement type " + measurementServerDT.toString());
        }

        List<MonitoredValue> sampledValuesServerDT = this.getEnergySampledForComponent(dtServerEnergyProfileDTO,
                deviceServerDT,
                measurementServerDT,
                startTime,
                predictionGranularity);


        return sampledValuesServerDT;
    }

    public List<MonitoredValue> findServerRTConsumptionAveragedSignal(UUID dataCenterID,
                                                                  LocalDateTime startTime,
                                                                  PredictionGranularity predictionGranularity,
                                                                  String energyType,
                                                                  Integer granularity,
                                                                  String predictionType){
        startTime = startTime.withNano(0).withSecond(0);
        LocalDateTime endTime = startTime.plusHours(predictionGranularity.getNoHours());
        Object[][] rtServerEnergyProfileDTO;
        if(executionOn) {
            rtServerEnergyProfileDTO = monitoredValueRepository.findEstimatedProfileValues(
                    dataCenterID,
                    startTime,
                    endTime,
                    energyType,
                    DeviceTypeEnum.SERVER_ROOM.type(),
                    MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type(),
                    predictionType,
                    granularity);
        }else{
            rtServerEnergyProfileDTO = monitoredValueRepository.findEstimatedProfileValues(
                    dataCenterID,
                    startTime,
                    endTime,
                    energyType,
                    DeviceTypeEnum.SERVER_ROOM.type(),
                    MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type(),
                    predictionType,
                    granularity);
        }

        Device deviceServerRT = deviceRepository.findDeviceByLabel(deviceRepository.findByUUID(dataCenterID).getLabel());
        if (deviceServerRT == null) {
            throw new ResourceNotFoundException("Device type " + deviceServerRT.toString());
        }

        Measurement measurementServerRT = measurementRepository.findMeasurementByDeviceId(deviceServerRT.getDeviceTypeId().getId(),
                energyType, predictionType);
        if (measurementServerRT == null) {
            throw new ResourceNotFoundException("Measurement type " + measurementServerRT.toString());
        }

        List<MonitoredValue> sampledValuesServerRT = this.getEnergySampledForComponent(rtServerEnergyProfileDTO,
                deviceServerRT,
                measurementServerRT,
                startTime,
                predictionGranularity);


        return sampledValuesServerRT;

    }

    public List<MonitoredValue> findServerRTConsumptionAveragedSignalBetweenDates(UUID dataCenterID,
                                                                                  LocalDateTime startTime,
                                                                                  LocalDateTime endTime,
                                                                                  PredictionGranularity predictionGranularity,
                                                                                  String energyType,
                                                                                  Integer granularity,
                                                                                  String predictionType){

        Object[][] rtServerEnergyProfileDTO;
        if(executionOn) {
            rtServerEnergyProfileDTO = monitoredValueRepository.findEstimatedProfileValuesBetweenDates(
                    dataCenterID,
                    startTime,
                    endTime,
                    energyType,
                    DeviceTypeEnum.SERVER_ROOM.type(),
                    MeasurementType.PRE_OPT_SERVER_ROOM_CURRENT_RT_CONSUMPTION.type(),
                    predictionType,
                    granularity);
        }else{
            rtServerEnergyProfileDTO = monitoredValueRepository.findEstimatedProfileValuesBetweenDates(
                    dataCenterID,
                    startTime,
                    endTime,
                    energyType,
                    DeviceTypeEnum.SERVER_ROOM.type(),
                    MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION.type(),
                    predictionType,
                    granularity);
        }

        Device deviceServerRT = deviceRepository.findDeviceByLabel(deviceRepository.findByUUID(dataCenterID).getLabel());
        if (deviceServerRT == null) {
            throw new ResourceNotFoundException("Device type " + deviceServerRT.toString());
        }

        Measurement measurementServerRT = measurementRepository.findMeasurementByDeviceId(deviceServerRT.getDeviceTypeId().getId(),
                energyType, predictionType);
        if (measurementServerRT == null) {
            throw new ResourceNotFoundException("Measurement type " + measurementServerRT.toString());
        }

        List<MonitoredValue> sampledValuesServerRT = new ArrayList<>();
        for(LocalDateTime l=startTime; l.isBefore(endTime); l=l.plusMinutes(predictionGranularity.getNoOutputs() * predictionGranularity.getSampleFrequencyMin())){

            //compute ther energy sampledd values for each sub-curve
            List<MonitoredValue> monitoredValuesPerSubCurve = this.getEnergySampledForComponent(rtServerEnergyProfileDTO,
                deviceServerRT,
                measurementServerRT,
                l,
                predictionGranularity);

            for(int i=0; i<monitoredValuesPerSubCurve.size(); i++){
                sampledValuesServerRT.add(monitoredValuesPerSubCurve.get(i));
            }
        }

        return sampledValuesServerRT;

    }

    private List<MonitoredValue> getEnergySampledForComponentBetweenDates(Object[][] results,
                                                                        Device device,
                                                                        Measurement measurement,
                                                                          LocalDateTime startTime,
                                                                        LocalDateTime endTime,
                                                                        PredictionGranularity predictionGranularity) {

        List<MonitoredValue> sampledValues = new ArrayList<>();
        List<AggregatedValuesDTO> historicalNewlyConsumptionData = RepoConversionUtility.getAveragedValues(results, device, measurement);

        LocalDateTime startTimeLDT = startTime.withHour(0).withMinute(0).withSecond(0);
        LocalDateTime endTimeLDT = endTime.withHour(0).withMinute(0).withSecond(0);

        List<AggregatedValuesDTO> formattedHistoricalNewlyConsumptionData = new ArrayList<>();

        //format historical data
        for(LocalDateTime l=startTime; l.isBefore(endTime); l=l.plusMinutes(predictionGranularity.getSampleFrequencyMin())){
            formattedHistoricalNewlyConsumptionData.add(new AggregatedValuesDTO(0.0, device, measurement,
                    l.getYear(), l.getMonth().getValue(), l.getDayOfMonth(), l.getHour(), l.getMinute()));
        }
        for (int i=0; i<formattedHistoricalNewlyConsumptionData.size(); i++){
            for(AggregatedValuesDTO a: historicalNewlyConsumptionData){
                if(a.getDateTime().isEqual(formattedHistoricalNewlyConsumptionData.get(i).getDateTime())){
                    formattedHistoricalNewlyConsumptionData.get(i).setValue(a.getValue());
                }
            }
        }

        if (!formattedHistoricalNewlyConsumptionData.isEmpty()) {
            List<EnergyPointValueDTO> energyPointValueDTOS = new ArrayList<>();

            LocalDateTime time = startTimeLDT;

            for (int i = 0; i < formattedHistoricalNewlyConsumptionData.size(); i++) {
                double mav = formattedHistoricalNewlyConsumptionData.get(i).getValue();
                energyPointValueDTOS.add(new EnergyPointValueDTO(mav, DateUtils.localDateTimeToLongInMillis(time)));
                time = time.plusMinutes(predictionGranularity.getSampleFrequencyMin());
            }

//            if(predictionGranularity.getGranularity().equals("DAYAHEAD")){
//                for (AggregatedValuesDTO mav : formattedHistoricalNewlyConsumptionData) {
//                    int index = (mav.getHour() - startTime.getHour()) * (predictionGranularity.getSampleFrequencyMin() / predictionGranularity.getSampleFrequencyMin()) + mav.getMinute();
//                    energyPointValueDTOS.get(index).setEnergyValue((mav.getValue()));
//                }
//            }
//            else if(predictionGranularity.getGranularity().equals("INTRADAY")){
//                for (AggregatedValuesDTO mav : formattedHistoricalNewlyConsumptionData) {
//                    int index = 2*(mav.getHour() - startTime.getHour()) + mav.getMinute()/predictionGranularity.getSampleFrequencyMin();
//                    energyPointValueDTOS.get(index).setEnergyValue((mav.getValue()));
//                }
//            }

            // transform list of energy point values into list of monitored values
            for (int i = 0; i < energyPointValueDTOS.size(); i++) {
                sampledValues.add(new MonitoredValue(Instant.ofEpochMilli(energyPointValueDTOS.get(i).getTimeStamp()).atZone(ZoneOffset.UTC).toLocalDateTime(),
                        energyPointValueDTOS.get(i).getEnergyValue(),
                        device,
                        measurement));
            }
        }

        return sampledValues;
    }
}
