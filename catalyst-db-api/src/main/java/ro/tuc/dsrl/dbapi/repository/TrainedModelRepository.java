package ro.tuc.dsrl.dbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.dbapi.model.TrainedModel;

import java.util.List;
import java.util.UUID;

@Repository
public interface TrainedModelRepository extends JpaRepository<TrainedModel, UUID> {

    @Query(value = "SELECT t " +
            "FROM TrainedModel t " +
            "WHERE t.id = :uuid")
    TrainedModel findByUUId(@Param("uuid") UUID uuid);

    @Query(value = "SELECT t " +
            "FROM TrainedModel t " +
            "WHERE t.scenario = :dataScenario " +
            "AND t.predictionType = :predictionGranularity  " +
            "AND t.algorithmType LIKE :algorithmType " +
            "AND t.componentType = :topologyComponentType " +
            "ORDER BY t.startTime DESC")
    List<TrainedModel> getStatusOfLatestTrainedModelForComponent(@Param("dataScenario") String dataScenario,
                                                                 @Param("predictionGranularity") String predictionGranularity,
                                                                 @Param ("algorithmType") String algorithmType,
                                                                 @Param("topologyComponentType") String topologyComponentType);
}
