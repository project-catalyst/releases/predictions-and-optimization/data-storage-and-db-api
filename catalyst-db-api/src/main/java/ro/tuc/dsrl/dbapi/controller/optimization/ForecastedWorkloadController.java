package ro.tuc.dsrl.dbapi.controller.optimization;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.model.dto.AggregatedValuesDTO;
import ro.tuc.dsrl.dbapi.model.dto.resources.CopFactors;
import ro.tuc.dsrl.dbapi.service.basic.DeviceService;
import ro.tuc.dsrl.dbapi.service.basic.MonitoredValueService;
import ro.tuc.dsrl.dbapi.service.basic.PredictedValueService;
import ro.tuc.dsrl.dbapi.service.prediction.EnergyHistoricalPredictedValuesService;
import ro.tuc.dsrl.dbapi.util.RepoConversionUtility;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/energy-consumption")
public class ForecastedWorkloadController {


    private static final Log LOGGER = LogFactory.getLog(ForecastedWorkloadController.class);

    private final EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService;
    private final PredictedValueService predictedValueService;
    private final DeviceService deviceService;
    private final MonitoredValueService monitoredValueService;

    @Value("${datacenter.name}")
    private String datacenterName;

    @Value("${execution.on}")
    private boolean executionOn;

    @Value("${serverroom.split}")
    private boolean serverroomSplit;

    public ForecastedWorkloadController(EnergyHistoricalPredictedValuesService energyHistoricalPredictedValuesService,
                                        PredictedValueService predictedValueService, DeviceService deviceService,
                                        MonitoredValueService monitoredValueService) {
        this.energyHistoricalPredictedValuesService = energyHistoricalPredictedValuesService;
        this.predictedValueService = predictedValueService;
        this.deviceService = deviceService;
        this.monitoredValueService = monitoredValueService;
    }

    @GetMapping(value = "/dayahead/dt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregeddayAheadDT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
    }

    @GetMapping(value = "/dayahead/rt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregeddayAheadRT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.DAYAHEAD,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
    }


    @GetMapping(value = "/intraday/rt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregedIntradayRT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
    }


    @GetMapping(value = "/intraday/dt/{datacenter-name}/{startdate}")
    public ForecastDayAheadDTO getAvaregedIntradayDT(
            @PathVariable("datacenter-name") String datacenterName,
            @PathVariable("startdate") Long startTime) {
        List<String> errors = EnergyProfilePreconditions.validate(startTime);
        EnergyProfilePreconditions.throwIncorrectParameterException(errors);

        LocalDateTime startLocalDateTime = DateUtils.millisToLocalDateTime(startTime);


        return energyHistoricalPredictedValuesService.forecastedWorkloadValues(datacenterName,
                startLocalDateTime,
                PredictionGranularity.INTRADAY,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION);
    }



    @GetMapping(value = "/setup/estimated-profiles/{startdate}")
    public HashMap setupEstimatedProfiles(
            @PathVariable("startdate") Long startTime) {

        LocalDateTime currentDateTime = DateUtils.millisToLocalDateTime(startTime);
        LocalDateTime historicalEndTime = currentDateTime;
        LocalDateTime historicalStartTime = currentDateTime.minusDays(5);

        double edPercentage = deviceService.getDTPercentage(datacenterName, historicalEndTime);

        List<EnergySampleDTO> eAveragedRT = new ArrayList<>();
        List<EnergySampleDTO> eAveragedDT = new ArrayList<>();

        if (serverroomSplit) {
            List<AggregatedValuesDTO> valuesRT = monitoredValueService.getBaselineMonitoredValuesGroupedByYear(datacenterName, historicalStartTime, historicalEndTime,
                    PredictionGranularity.DAYAHEAD, MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION);
            eAveragedRT = RepoConversionUtility.convertAggregatedValuesToEnergySamples(valuesRT, currentDateTime, PredictionGranularity.DAYAHEAD);

            for (EnergySampleDTO rtValue : eAveragedRT) {
                double dtEnergyValue = (rtValue.getEnergyValue() * edPercentage) / (1 - edPercentage);
                EnergySampleDTO dtValue = new EnergySampleDTO(dtEnergyValue, rtValue.getTimestamp());
                eAveragedDT.add(dtValue);
            }
        } else {
            List<AggregatedValuesDTO> valuesServerRoom = monitoredValueService.getBaselineMonitoredValuesGroupedByYear(datacenterName, historicalStartTime, historicalEndTime,
                    PredictionGranularity.DAYAHEAD, MeasurementType.SERVER_ROOM_CURRENT_CONSUMPTION);
            List<EnergySampleDTO> eAveragedServerRoom = RepoConversionUtility.convertAggregatedValuesToEnergySamples(valuesServerRoom, currentDateTime, PredictionGranularity.DAYAHEAD);

            for (EnergySampleDTO serverRoomValue : eAveragedServerRoom) {
                EnergySampleDTO dtValue = new EnergySampleDTO(serverRoomValue.getEnergyValue() * edPercentage, serverRoomValue.getTimestamp());
                EnergySampleDTO rtValue = new EnergySampleDTO(serverRoomValue.getEnergyValue() * (1 - edPercentage), serverRoomValue.getTimestamp());

                eAveragedDT.add(dtValue);
                eAveragedRT.add(rtValue);
            }
        }

        LOGGER.info("POPULATE RT");
        boolean rtDA = predictedValueService.setupEstimatedProfiles(datacenterName, currentDateTime, DeviceTypeEnum.SERVER_ROOM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.SERVER_ROOM_CURRENT_RT_CONSUMPTION, PredictionType.ENERGY_CONSUMPTION, eAveragedRT);

        LOGGER.info("POPULATE DT");
        boolean dtDA = predictedValueService.setupEstimatedProfiles(datacenterName, currentDateTime, DeviceTypeEnum.SERVER_ROOM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION, PredictionType.ENERGY_CONSUMPTION, eAveragedDT);

        LOGGER.info("POPULATE Cooling");
//        List<AggregatedValuesDTO> valuesCooling = monitoredValueService.getBaselineMonitoredValuesGroupedByYear(datacenterName, historicalStartTime, historicalEndTime,
//                PredictionGranularity.DAYAHEAD, MeasurementType.PRE_OPT_COOLING_SYSTEM_CURRENT_CONSUMPTION);
//        List<EnergySampleDTO> eAveragedCooling = RepoConversionUtility.convertAggregatedValuesToEnergySamples(valuesCooling, currentDateTime, PredictionGranularity.DAYAHEAD);
        List<EnergySampleDTO> eAveragedCooling = computeCool(eAveragedRT,eAveragedDT, currentDateTime);
        boolean coolingDA = predictedValueService.setupEstimatedProfiles(datacenterName, currentDateTime, DeviceTypeEnum.COOLING_SYSTEM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.COOLING_SYSTEM_CURRENT_CONSUMPTION, PredictionType.ENERGY_CONSUMPTION, eAveragedCooling);


        LOGGER.info("POPULATE baseline");
        List<EnergySampleDTO> baseline = addLists(eAveragedRT, eAveragedDT, eAveragedCooling);
        boolean baselineDA = predictedValueService.setupEstimatedProfiles(datacenterName, currentDateTime, DeviceTypeEnum.COOLING_SYSTEM,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.DATA_CENTER_BASELINE, PredictionType.ENERGY_BASELINE, baseline);

        LOGGER.info("POPULATE Renewable");
        List<AggregatedValuesDTO> valuesRenewable = monitoredValueService.getBaselineMonitoredValuesGroupedByYear(datacenterName, historicalStartTime, historicalEndTime,
                PredictionGranularity.DAYAHEAD, MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE);
        List<EnergySampleDTO> eRenewable = RepoConversionUtility.convertAggregatedValuesToEnergySamples(valuesRenewable, currentDateTime, PredictionGranularity.DAYAHEAD);
        boolean renDA = predictedValueService.setupEstimatedProfiles(datacenterName, currentDateTime, DeviceTypeEnum.DATACENTER,
                PredictionGranularity.DAYAHEAD, AggregationGranularity.HOUR,
                MeasurementType.ELECTRICAL_POWER_PRODUCTION_RENEWABLE, PredictionType.ENERGY_PRODUCTION, eRenewable);

        HashMap<String, Boolean> map = new HashMap<>();
        map.put("Baseline DayAhead", baselineDA);
        map.put("Predicted DayAhead rt", rtDA);
        map.put("Predicted DayAhead dt", dtDA);
        map.put("Predicted DayAhead Cooling", coolingDA);
        map.put("Predicted DayAhead Renewable", renDA);
        return map;
    }

    private List<EnergySampleDTO> computeCool(List<EnergySampleDTO> rt, List<EnergySampleDTO> dt, LocalDateTime currentDateTime){

        CopFactors copFactors = deviceService.getCopFactors(datacenterName, currentDateTime);

        List<EnergySampleDTO> energySampleDTOS = new ArrayList<>();
        for (int i = 0; i < rt.size(); i++) {
            if (rt.get(i).getTimestamp().getHour() == dt.get(i).getTimestamp().getHour() ) {
                double value = (rt.get(i).getEnergyValue() + dt.get(i).getEnergyValue() ) / copFactors.getCopC();
                EnergySampleDTO coolValue = new EnergySampleDTO(value, rt.get(i).getTimestamp());
                energySampleDTOS.add(coolValue);
            } else {
                LOGGER.info("The Sample values are not for the same hour");
            }
        }
        return energySampleDTOS;
    }
    private List<EnergySampleDTO> addLists(List<EnergySampleDTO> rt, List<EnergySampleDTO> dt, List<EnergySampleDTO> cool) {
        List<EnergySampleDTO> energySampleDTOS = new ArrayList<>();
        for (int i = 0; i < rt.size(); i++) {
            if (rt.get(i).getTimestamp().getHour() == dt.get(i).getTimestamp().getHour() &&
                    cool.get(i).getTimestamp().getHour() == dt.get(i).getTimestamp().getHour()) {
                double value = rt.get(i).getEnergyValue() + dt.get(i).getEnergyValue() + cool.get(i).getEnergyValue();
                EnergySampleDTO summedValue = new EnergySampleDTO(value, rt.get(i).getTimestamp());
                energySampleDTOS.add(summedValue);
            } else {
                LOGGER.info("The Sample values are not for the same hour");
            }
        }
        return energySampleDTOS;
    }


}
