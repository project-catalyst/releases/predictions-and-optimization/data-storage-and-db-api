package ro.tuc.dsrl.dbapi.controller.preconditions;

import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class UUIDPreconditions {

    private static final Pattern VALID_UUID_REGEX =
            Pattern.compile("\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b",
                    Pattern.CASE_INSENSITIVE);

    private UUIDPreconditions() {

    }

    public static void validate(String uuid) {

        List<String> errors = validateUUID(uuid);

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect Parameters", errors);
        }
    }

    public static List<String> validateUUID(String uuid) {
        List<String> errors = new ArrayList<>();
        if (uuid == null) {
            errors.add("ID must not be null");
        } else {
            if (!VALID_UUID_REGEX.matcher(uuid).find()) {
                errors.add("ID does not have the correct format");
            }
        }
        return errors;
    }
}
