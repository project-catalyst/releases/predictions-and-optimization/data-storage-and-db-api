package ro.tuc.dsrl.dbapi.controller.preconditions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.dbapi.service.validators.EnergyProfileValidator;

import java.util.ArrayList;
import java.util.List;

public class EnergyProfilePreconditions {

    private static final Log LOGGER = LogFactory.getLog(EnergyProfilePreconditions.class);

    private EnergyProfilePreconditions() {
    }

    public static List<String> validate(String dataCenterID, Long startTime) {

        List<String> errors = new ArrayList<>();

        UUIDPreconditions.validate(dataCenterID);

        if (startTime == null) {
            errors.add("Start time is null");
        }

        return errors;
    }

    public static List<String> validate(Long startTime) {

        List<String> errors = new ArrayList<>();

        if (startTime == null) {
            errors.add("Start time is null");
        }

        return errors;
    }

    public static List<String> validate(String dataCenterID, Long startTime, String serverID) {

        List<String> errors;

        errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);

        if (serverID == null || serverID.equals("")) {
            errors.add("Server ID is null or empty");
        }

        return errors;
    }

    public static List<String> validate(String dataCenterID, Long startTime, Long endTime) {

        List<String> errors;

        errors = EnergyProfilePreconditions.validate(dataCenterID, startTime);

        if (endTime == null) {
            errors.add("End time is null");
        }

        return errors;
    }

    public static void throwIncorrectParameterException(List<String> errors) {

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(EnergyProfileValidator.class.getSimpleName(), errors);
        }
    }

}
