package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasurementDTO;
import ro.tuc.dsrl.dbapi.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.PropertyPreconditions;
import ro.tuc.dsrl.dbapi.controller.preconditions.UUIDPreconditions;
import ro.tuc.dsrl.dbapi.service.basic.MeasurementService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/measurement")
public class MeasurementController {

    private final MeasurementService measurementService;

    @Autowired
    public MeasurementController(MeasurementService measurementService) {
        this.measurementService = measurementService;
    }

    @GetMapping(value = "/byId/{measurementId}")
    public MeasurementDTO findById(@PathVariable("measurementId") String measurementId) {

        UUIDPreconditions.validate(measurementId);

        UUID measurementUUID = UUID.fromString(measurementId);

        return measurementService.findById(measurementUUID);
    }

    @GetMapping(value = "/all")
    public List<MeasurementDTO> findAll() {

        return measurementService.findAll();
    }

    @PostMapping(value = "/insert")
    public UUID insertMeasurementDTO(@RequestBody MeasurementDTO measurementDTO) {

        return measurementService.insert(measurementDTO);
    }

    @GetMapping(value = "/byProperty/{property}")
    public UUID getMeasurementIDByProperty(@PathVariable("property") String property){

        List<String> errors = PropertyPreconditions.validate(property);
        PropertyPreconditions.throwIncorrectParameterException(errors);

        return measurementService.getMeasurementByProperty(property);
    }
}
