package ro.tuc.dsrl.dbapi.service.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.DeviceTypeDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.DeviceType;
import ro.tuc.dsrl.dbapi.model.builders.DeviceTypeBuilder;
import ro.tuc.dsrl.dbapi.repository.DeviceTypeRepository;
import ro.tuc.dsrl.dbapi.service.validators.DeviceTypeValidator;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DeviceTypeService {
    private final DeviceTypeRepository deviceTypeRepository;

    @Autowired
    public DeviceTypeService(DeviceTypeRepository deviceTypeRepository) {
        this.deviceTypeRepository = deviceTypeRepository;
    }

    public DeviceTypeDTO findById(UUID deviceTypeUUID) {

        DeviceType deviceType = deviceTypeRepository.findByUUId(deviceTypeUUID);
        if (deviceType == null) {
            throw new ResourceNotFoundException("DeviceTypeEnum with ID " + deviceTypeUUID);
        }

        return DeviceTypeBuilder.generateDTOFromEntity(deviceType);
    }

    public DeviceTypeDTO findByType(String deviceType) {

        DeviceType device = deviceTypeRepository.findByType(deviceType);
        if (device == null) {
            throw new ResourceNotFoundException("DeviceTypeEnum with ID " + deviceType);
        }

        return DeviceTypeBuilder.generateDTOFromEntity(device);
    }

    public List<DeviceTypeDTO> findAll() {
        List<DeviceType> deviceTypes = deviceTypeRepository.findAll();

        return deviceTypes.stream()
                .map(DeviceTypeBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }



    public UUID insert(DeviceTypeDTO deviceTypeDTO) {
        DeviceTypeValidator.validateInsert(deviceTypeDTO);

        DeviceType deviceType = DeviceTypeBuilder.generateEntityFromDTO(deviceTypeDTO);
        return deviceTypeRepository.save(deviceType).getId();
    }
}
