package ro.tuc.dsrl.dbapi.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.TrainedModelDTO;
import ro.tuc.dsrl.dbapi.service.basic.TrainedModelService;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/trained_model")
public class TrainedModelController {

    private final TrainedModelService trainedModelService;

    @Autowired
    public TrainedModelController(TrainedModelService trainedModelService) {
        this.trainedModelService = trainedModelService;
    }

    @PostMapping(value = "/insert")
    public UUID insertTrainedModelDTO(@RequestBody TrainedModelDTO trainedModelDTO) {

        return trainedModelService.insert(trainedModelDTO);
    }

    @PostMapping(value = "/update_status")
    public UUID updateTrainedModelDTO(@RequestBody TrainedModelDTO trainedModelDTO) {

        return trainedModelService.updateStatus(trainedModelDTO);
    }

    @GetMapping(value = "/status/{dataScenario}/{predictionGranularity}/{algorithmType}/{topologyComponentType}")
    public TrainedModelDTO getStatus(@PathVariable("dataScenario") String dataScenario,
                            @PathVariable("predictionGranularity") String predictionGranularity,
                            @PathVariable("algorithmType") String algorithmType,
                            @PathVariable("topologyComponentType") String topologyComponentType){

        return trainedModelService.getStatusOnLatestTrainedModel(dataScenario, predictionGranularity, algorithmType, topologyComponentType);
    }
}
