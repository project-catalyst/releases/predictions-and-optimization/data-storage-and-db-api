package ro.tuc.dsrl.dbapi.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "metric")
public class Metric {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "timestamp")
    @NotNull
    private LocalDateTime timestamp;

    @Column(name = "value")
    @NotNull
    private double value;

    @ManyToOne
    @JoinColumn(name = "measurement_id")
    @NotNull
    private Measurement measurement;

    @ManyToOne
    @JoinColumn(name = "device_id")
    @NotNull
    private Device device;

    @ManyToOne
    @JoinColumn(name = "plan_id")
    private OptimizationPlan plan;

    public Metric(){}

    public Metric(@NotNull LocalDateTime timestamp, @NotNull double value, @NotNull Measurement measurement, @NotNull Device device, OptimizationPlan plan) {
        this.timestamp = timestamp;
        this.value = value;
        this.measurement = measurement;
        this.device = device;
        this.plan = plan;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public OptimizationPlan getPlan() {
        return plan;
    }

    public void setPlan(OptimizationPlan plan) {
        this.plan = plan;
    }
}
