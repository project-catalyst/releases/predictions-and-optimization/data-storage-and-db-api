package ro.tuc.dsrl.dbapi.service.basic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.UserDTO;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;
import ro.tuc.dsrl.dbapi.model.Device;
import ro.tuc.dsrl.dbapi.model.User;
import ro.tuc.dsrl.dbapi.model.UserToDC;
import ro.tuc.dsrl.dbapi.model.builders.UserBuilder;
import ro.tuc.dsrl.dbapi.repository.DeviceRepository;
import ro.tuc.dsrl.dbapi.repository.UserRepository;
import ro.tuc.dsrl.dbapi.repository.UserToDcRepository;
import ro.tuc.dsrl.geyser.datamodel.components.DataCentre;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {
    private static final Log LOGGER = LogFactory.getLog(UserService.class);
    private final UserRepository userRepository;
    private final DeviceService deviceService;
    private final DeviceRepository deviceRepository;
    private final UserToDcRepository userToDcRepository;

    public UserService(UserRepository userRepository, DeviceService deviceService, DeviceRepository deviceRepository, UserToDcRepository userToDcRepository) {
        this.userToDcRepository = userToDcRepository;
        this.userRepository = userRepository;
        this.deviceService = deviceService;
        this.deviceRepository = deviceRepository;
    }

    public UserDTO findUser(User user){
        User found_user = userRepository.findUser(user.getUsername(), user.getPassword());
        if(found_user == null)
            throw new ResourceNotFoundException("User not found");

        return UserBuilder.generateDTOFromEntity(found_user);
    }

    public User saveUser(User user){
        return userRepository.save(user);
    }

    public List<DataCentre> getUserDC(String username, LocalDateTime time){
        List<Device> list = userRepository.findDevices(username, DeviceTypeEnum.DATACENTER.type());
        List<DataCentre> dcOutput = new ArrayList<>();
        for (Device d : list) {
            DataCentre dc = deviceService.getDatacenterProperties(d.getLabel(), time);
            dc.setIdDataCentre(d.getId());
            dcOutput.add(dc);
        }
        return dcOutput;
    }

    public List<DataCentre> insertDatacenter(DataCentre dataCentre, UUID user_id){
        if (user_id == null){
            List<String> errors = new ArrayList<>();
            errors.add("No user logged in");
            throw new IncorrectParameterException(User.class.getSimpleName(), errors);
        }

        Device dc = deviceRepository.findDeviceByLabel(dataCentre.getLabel());
        if (dc != null) {
            List<String> errors = new ArrayList<>();
            errors.add("Data centre name is duplicated");
            throw new IncorrectParameterException(DataCentre.class.getSimpleName(), errors);
        }

        List<DataCentre> allDc = deviceService.insertDatacentre(dataCentre);
        dc = deviceRepository.findDeviceByLabel(dataCentre.getLabel());
        User user = userRepository.findByUserId(user_id);

        UserToDC user_to_dc = new UserToDC();
        user_to_dc.setDevice(dc);
        user_to_dc.setUser(user);
        userToDcRepository.save(user_to_dc);
        return allDc;
    }
}
