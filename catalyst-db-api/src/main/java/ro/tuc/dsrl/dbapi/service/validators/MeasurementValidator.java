package ro.tuc.dsrl.dbapi.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.MeasurementDTO;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class MeasurementValidator {

    private static final Log LOGGER = LogFactory.getLog(MeasurementValidator.class);

    private MeasurementValidator() {

    }

    public static void validateInsert(MeasurementDTO measurementDTO) {

        List<String> errors = new ArrayList<>();
        if (measurementDTO == null) {
            errors.add("Measurement DTO is null");
            throw new IncorrectParameterException(MeasurementValidator.class.getSimpleName(), errors);
        }

        if (measurementDTO.getProperty() == null || measurementDTO.getProperty().isEmpty()) {
            errors.add("Measurement property is null or empty");
        }

        if (measurementDTO.getObservation() == null || measurementDTO.getObservation().isEmpty()) {
            errors.add("Measurement observation is null or empty");
        }

        if (measurementDTO.getDeviceTypeId() == null) {
            errors.add("Measurement device type is null");
        }

        if (measurementDTO.getMeasureUnitId() == null) {
            errors.add("Measurement measure unit is null");
        }

        if (measurementDTO.getPropertySourceId() == null) {
            errors.add("Measurement property source is null");
        }

        if (measurementDTO.getValueTypeId() == null) {
            errors.add("Measurement value type is null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MeasurementValidator.class.getSimpleName(), errors);
        }
    }
}
