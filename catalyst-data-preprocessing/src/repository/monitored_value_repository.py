from src.extensions import mysql
import logging
import uuid

logging.basicConfig(level=logging.INFO)

def insert_preprocessed_data(monitored_values_list):
    try:
        connection = mysql.connect()
        cursor = connection.cursor()
        sql = "INSERT INTO monitored_value (id, timestamp, value, device_id, measurement_id) VALUES (_binary %s, %s, %s, _binary %s, _binary %s)"
        for elem in monitored_values_list:
            tuple = (elem.id, elem.timestamp.to_pydatetime(), float(elem.value), uuid.UUID(elem.deviceId).bytes, uuid.UUID(elem.deviceMeasurementId).bytes)
            cursor.execute(sql, tuple)
            logging.info("Record inserted successfully into monitoredValue table")
        connection.commit()
        cursor.close()
        connection.close()
    except Exception as e:
        logging.info(e)

def insert_single_preprocessed_data(elem):
    try:
        connection = mysql.connect()
        cursor = connection.cursor()
        sql = "INSERT INTO monitored_value (id, timestamp, value, device_id, measurement_id) VALUES (_binary %s, %s, %s, _binary %s, _binary %s)"
        tuple = (elem.id, elem.timestamp.to_pydatetime(), float(elem.value), uuid.UUID(elem.deviceId).bytes, uuid.UUID(elem.deviceMeasurementId).bytes)
        cursor.execute(sql, tuple)
        logging.info("Record inserted successfully into monitoredValue table")
        connection.commit()
        cursor.close()
        connection.close()
    except Exception as e:
        logging.info(e)