import logging
import os, sys
sys.path.insert(0, os.path.abspath(".."))
from src.controllers.data_preprocessing import data_preprocessing
from src.extensions import mysql
from flask import Flask
import sys
sys.path.insert(0, '/catalyst-data-preprocessing/src') # location of src

app = Flask(__name__)

# app.config['MYSQL_DATABASE_USER'] = 'catalyst-user'
# app.config['MYSQL_DATABASE_PASSWORD'] = 'ILoveRandomPasswords#'
# app.config['MYSQL_DATABASE_DB'] = 'catalyst-d43'
# app.config['MYSQL_DATABASE_HOST'] = '192.168.1.119'
# app.config['MYSQL_DATABASE_PORT'] = 3307

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'abra2415cadabra'
app.config['MYSQL_DATABASE_DB'] = 'catalyst-d43'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_PORT'] = 3306

mysql.init_app(app)
logging.basicConfig(level=logging.INFO)

# test route
@app.route('/')
def hello_world():
   return 'Hello'

# register controllers
app.register_blueprint(data_preprocessing, url_prefix='/preprocessing')

if __name__ == '__main__':

    logging.info('Starting the Data Preprocessing application...')

    app.run(debug=False, host="localhost", port=5001)
