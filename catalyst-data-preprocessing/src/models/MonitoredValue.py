import uuid
from datetime import datetime
import src.utils as utils


class MonitoredValue:

    def __init__(self, timestamp, value, deviceId, deviceMeasurementId):
        self.id = uuid.uuid4().bytes
        self.timestamp = timestamp
        self.value = value
        self.deviceId = deviceId
        self.deviceMeasurementId = deviceMeasurementId

    @staticmethod
    def get_value():
        return MonitoredValue.value

    @staticmethod
    def get_timestamp(self):
        return MonitoredValue.timestamp

    @staticmethod
    def get_device_id():
        return MonitoredValue.deviceId

    @staticmethod
    def get_device_measurement_id(self):
        return MonitoredValue.deviceMeasurementId

    @staticmethod
    def to_json(self):
        if isinstance(self, MonitoredValue):
            return {
                'id': str(self.id),
                'timestamp': utils.convert_datetime_to_long(self.timestamp),
                'value': self.value,
                'deviceId': str(self.deviceId),
                'deviceMeasurementId': str(self.deviceMeasurementId)
            }
