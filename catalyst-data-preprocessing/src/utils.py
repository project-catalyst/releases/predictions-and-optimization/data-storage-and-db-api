import datetime

import pandas as pd


# function that reads data from a csv file
def extract_energy_values(data_path):
    data = pd.read_csv(data_path, sep=',').values
    return data


def convert_to_dict(obj):
    """
 A function takes in a custom object and returns a dictionary representation of the object.
    This dict representation includes meta data such as the object's module and class names.
    """
    obj_dict = {
    }

    #  Populate the dictionary with object properties
    obj_dict.update(obj.__dict__)

    return obj_dict


def convert_datetime_to_long(dt):
    return (dt - datetime.datetime.utcfromtimestamp(0)).total_seconds() * 1000.0
