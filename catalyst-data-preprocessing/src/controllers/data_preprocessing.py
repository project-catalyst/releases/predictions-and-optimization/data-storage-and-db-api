import json
import pandas as pd
from flask import Blueprint, request, Response
import src.models.MonitoredValue as MonitoredValue
import src.services.data_imputation as data_imputation
import src.services.data_resampling as data_resampling
import src.repository.monitored_value_repository as monitored_value_repository
from src.extensions import mysql
import logging

logging.basicConfig(level=logging.INFO)

data_preprocessing = Blueprint('preprocessing', __name__)


@data_preprocessing.route('/data_preprocess/<granularity>', methods=['POST'])
def impute_and_resample_data(granularity):
    logging.info("Received new chunk of data for preprocessing...")
    logging.info("Start data imputation and resampling...")
    logging.info("Granularity: " + str(granularity))
    content = request.json
    logging.info("Data to be aggregated..." + json.dumps(content))
    data = pd.DataFrame(content)
    response = []
    # check if data frame not empty
    if data.empty:
        logging.info("Data frame is empty, skip insert...") 
    else:
        if data.shape[0]>1:
            logging.info("Data frame is not empty...")
            # impute missing data
            data['timestamp'] = pd.to_datetime(data['timestamp'], unit='ms')
            data['monitoredValue'] = data_imputation.fill_missing_data(data['monitoredValue'])
            # resample data to a desired frequency (independent of the sampling irregular timestamps)
            resampled_data = data_resampling.resample_data(data, granularity)
            logging.info("Finished data imputation and resampling...")
            # insert preprocessed data in mysql
            if resampled_data.shape[0] == 1: # if data frame has only one row insert directly that row
                logging.info("Start inserting single data instance into MySQL database...")
                m = MonitoredValue.MonitoredValue(resampled_data.index[0],
                                                resampled_data['monitoredValue'][0],
                                                data['deviceId'][0],
                                                data['deviceMeasurementId'][0]
                                                )
                monitored_value_repository.insert_single_preprocessed_data(m)
                response.append(m.to_json(m))
                logging.info("Finish inserting single instance into MySQL database...")
            else:
                monitored_values_list = []
                for row in resampled_data.index:
                   monitored_values_list.append(MonitoredValue.MonitoredValue(row,
                                                                       resampled_data['monitoredValue'][row],
                                                                       data['deviceId'][1],
                                                                       data['deviceMeasurementId'][1]
                                                                       ))
                   logging.info("Start inserting preprocessed data chunk into MySQL database...")
                   monitored_value_repository.insert_preprocessed_data(monitored_values_list)
                   logging.info("Finish inserting preprocessed data chunk into MySQL database...")
                   for elem in monitored_values_list:
                       response.append(elem.to_json(elem))
        else:
            # data has only one element in the list => no need to impute and resample
            logging.info("Data frame is not empty, but has single instance for aggregation...")
            # insert single instance in mysql
            data['timestamp'] = pd.to_datetime(data['timestamp'], unit='ms')
            m = MonitoredValue.MonitoredValue(data['timestamp'][0],
                                                  data['monitoredValue'][0],
                                                  data['deviceId'][0],
                                                  data['deviceMeasurementId'][0]
                                                  )
            monitored_value_repository.insert_single_preprocessed_data(m)
            response.append(m.to_json(m))
            logging.info("Finish inserting single instance into MySQL database...")
    json_response = json.dumps(response)
    logging.info("Response..." + json_response)
    return Response(response=json.dumps(response), status=200, mimetype='application/json')

