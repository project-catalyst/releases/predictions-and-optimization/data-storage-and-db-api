import numpy as np
import pandas as pd

NO_NEIGHBORS = 4


def fill_missing_data(data):
    if data.shape[0] > 1:  # data frame has at least 2 rows
        if data.isnull().values.any():
            data = interpolate_mean_n_nearest_neighbors(data,
                    NO_NEIGHBORS)
        if data.isnull().values.any():
            # do a linear interpolation of what remains
            interpolate_linear(data)
        if data.isnull().values.any():
            # replace nan values by a forward fill
            data = data.fillna(method='ffill')
        if data.isnull().values.any():
            # replace remaining nan values by a backward fill
            data = data.fillna(method='bfill')
        return data
    else:
        # do nothing and return data
        return data


def interpolate_mean_n_nearest_neighbors(data, n):
    columns = list(data)
    result = pd.DataFrame(data[columns[0]])
    for k in columns[1:]:
        ts = data[k].values
        out = np.copy(ts)
        for (i, val) in enumerate(ts):
            if np.isnan(val):
                n_by_2 = np.ceil(n / 2)
                lower = np.max([0, int(i - n_by_2)])
                upper = np.min([len(ts) + 1, int(i + n_by_2)])
                ts_near = np.concatenate([ts[lower:i], ts[i:upper]])
                out[i] = np.nanmean(ts_near)
        result = pd.concat([result, pd.DataFrame(out)], axis=1)
    return result


def interpolate_linear(data):
    data.interpolate(method='linear', axis=0, limit_area='inside',
                     inplace=True)
    return data
