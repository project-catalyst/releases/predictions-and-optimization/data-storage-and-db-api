import pandas as pd

import src.services.data_imputation as data_imputation


def resample_data(data, granularity):
    if data.shape[0] > 1: # data frame has at least 2 rows
        data.set_index('timestamp', inplace=True)
        resampled_data = pd.Series.resample(data, granularity, label='left', closed='left').mean()
        data_imputation.interpolate_linear(resampled_data)
        return resampled_data
    else:
        return data
