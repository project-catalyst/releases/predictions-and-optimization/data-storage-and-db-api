package ro.tuc.dsrl.geyser.datamodel.other;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 6, 2015
 * @Description:
 *
 */
public class EnergyValue {

	private double value;
	private EnergyMeasurementUnit measurementUnit;

	public EnergyValue() {
	}

	public EnergyValue(double value, EnergyMeasurementUnit measurementUnit) {
		this.value = value;
		this.measurementUnit = measurementUnit;
	}

	public double getValueInKwh() {
		return value * measurementUnit.value();
	}

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * @return the measurementUnit
	 */
	public EnergyMeasurementUnit getMeasurementUnit() {
		return measurementUnit;
	}

	/**
	 * @param measurementUnit
	 *            the measurementUnit to set
	 */
	public void setMeasurementUnit(EnergyMeasurementUnit measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

}
