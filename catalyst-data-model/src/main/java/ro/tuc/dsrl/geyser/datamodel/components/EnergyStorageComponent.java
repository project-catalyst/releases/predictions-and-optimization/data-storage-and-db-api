package ro.tuc.dsrl.geyser.datamodel.components;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.components.production.Battery;
import ro.tuc.dsrl.geyser.datamodel.components.production.FuelCell;
import ro.tuc.dsrl.geyser.datamodel.components.production.ThermalEnergyStorage;

import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = Battery.class), @JsonSubTypes.Type(value = FuelCell.class),
		@JsonSubTypes.Type(value = ThermalEnergyStorage.class) })
public class EnergyStorageComponent extends Component {

	private static final long serialVersionUID = 1L;
	@DynamicField
	protected double actualLoadedCapacity;
	protected double maxDischargeRate;
	protected double maxChargeRate;
	protected double energyLossRate;
	protected double chargeLossRate;
	protected double dischargeLossRate;
	protected double maximumCapacity;

	public EnergyStorageComponent() {
	}


	public EnergyStorageComponent(UUID id, String deviceLabel, double actualLoadedCapacity, double maxDischargeRate, double maxChargeRate,
			double energyLossRate, double chargeLossRate, double dischargeLossRate, double maximumCapacity) {
		super(id, deviceLabel);
		this.actualLoadedCapacity = actualLoadedCapacity;
		this.maxDischargeRate = maxDischargeRate;
		this.maxChargeRate = maxChargeRate;
		this.energyLossRate = energyLossRate;
		this.chargeLossRate = chargeLossRate;
		this.dischargeLossRate = dischargeLossRate;
		this.maximumCapacity = maximumCapacity;
	}

	/**
	 * @return the actualLoadedCapacity
	 */
	public double getActualLoadedCapacity() {
		return actualLoadedCapacity;
	}

	/**
	 * @param actualLoadedCapacity
	 *            the actualLoadedCapacity to set
	 */
	public void setActualLoadedCapacity(double actualLoadedCapacity) {
		this.actualLoadedCapacity = actualLoadedCapacity;
	}

	/**
	 * @return the maxDischargeRate
	 */
	public double getMaxDischargeRate() {
		return maxDischargeRate;
	}

	/**
	 * @param maxDischargeRate
	 *            the maxDischargeRate to set
	 */
	public void setMaxDischargeRate(double maxDischargeRate) {
		this.maxDischargeRate = maxDischargeRate;
	}

	/**
	 * @return the maxChargeRate
	 */
	public double getMaxChargeRate() {
		return maxChargeRate;
	}

	/**
	 * @param maxChargeRate
	 *            the maxChargeRate to set
	 */
	public void setMaxChargeRate(double maxChargeRate) {
		this.maxChargeRate = maxChargeRate;
	}

	/**
	 * @return the energyLossRate
	 */
	public double getEnergyLossRate() {
		return energyLossRate;
	}

	/**
	 * @param energyLossRate
	 *            the energyLossRate to set
	 */
	public void setEnergyLossRate(double energyLossRate) {
		this.energyLossRate = energyLossRate;
	}

	/**
	 * @return the chargeLossRate
	 */
	public double getChargeLossRate() {
		return chargeLossRate;
	}

	/**
	 * @param chargeLossRate
	 *            the chargeLossRate to set
	 */
	public void setChargeLossRate(double chargeLossRate) {
		this.chargeLossRate = chargeLossRate;
	}

	/**
	 * @return the dischargeLossRate
	 */
	public double getDischargeLossRate() {
		return dischargeLossRate;
	}

	/**
	 * @param dischargeLossRate
	 *            the dischargeLossRate to set
	 */
	public void setDischargeLossRate(double dischargeLossRate) {
		this.dischargeLossRate = dischargeLossRate;
	}

	/**
	 * @return the maximumCapacity
	 */
	public double getMaximumCapacity() {
		return maximumCapacity;
	}

	/**
	 * @param maximumCapacity
	 *            the maximumCapacity to set
	 */
	public void setMaximumCapacity(double maximumCapacity) {
		this.maximumCapacity = maximumCapacity;
	}

}
