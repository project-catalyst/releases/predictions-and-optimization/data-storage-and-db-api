package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class DynamicAdjustmentOfCoolingIntensity extends NonITComponentsOptimization {

	private static final long serialVersionUID = 5362220613822024889L;

	private double coolingIntensityLevelAdjust; // percentage
	
	public static final String ACTION_TYPE = "Dynamic Adjustment of Cooling Intensity (KWh)";

	public DynamicAdjustmentOfCoolingIntensity() {
		super(ACTION_TYPE);
	}
	
	public DynamicAdjustmentOfCoolingIntensity(long id, double estimatedEnergySaving, Date startTime, Date endTime,
			double coolingIntensityLevelAdjust) {
		super(id,ACTION_TYPE,  estimatedEnergySaving, startTime, endTime);
		this.coolingIntensityLevelAdjust = coolingIntensityLevelAdjust;
	}

	/**
	 * @return the coolingIntensityLevelAdjust
	 */
	public double getCoolingIntensityLevelAdjust() {
		return coolingIntensityLevelAdjust;
	}

	/**
	 * @param coolingIntensityLevelAdjust
	 *            the coolingIntensityLevelAdjust to set
	 */
	public void setCoolingIntensityLevelAdjust(double coolingIntensityLevelAdjust) {
		this.coolingIntensityLevelAdjust = coolingIntensityLevelAdjust;
	}

}
