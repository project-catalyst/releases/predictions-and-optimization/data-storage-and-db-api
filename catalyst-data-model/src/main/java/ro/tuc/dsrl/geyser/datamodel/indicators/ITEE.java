package ro.tuc.dsrl.geyser.datamodel.indicators;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ITEE extends EnergyConsumption {

	public ITEE() {
		super();
	}
	
	public ITEE(long id, double indicatorValue, double optimalValue, double threshold) {
		super(id, indicatorValue, optimalValue, threshold);
	}
	
	public static ITEE createDynamicInstance(long id, double indicatorValue) {
		return new ITEE(id, indicatorValue, -1, -1);
	}
}
