package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class TransformerSystem extends Facility {

	private static final long serialVersionUID = 1L;
	private double inputPower;
	private double outputPower;
	private double lossConversion;

	public TransformerSystem() {
	}

	public TransformerSystem(String deviceLabel, double inputPower, double outputPower, double lossConversion,
			double energyConsumption) {
		super(deviceLabel, energyConsumption);
		this.inputPower = inputPower;
		this.outputPower = outputPower;
		this.lossConversion = lossConversion;
	}

	/**
	 * @return the inputPower
	 */
	public double getInputPower() {
		return inputPower;
	}

	/**
	 * @param inputPower
	 *            the inputPower to set
	 */
	public void setInputPower(double inputPower) {
		this.inputPower = inputPower;
	}

	/**
	 * @return the outputPower
	 */
	public double getOutputPower() {
		return outputPower;
	}

	/**
	 * @param outputPower
	 *            the outputPower to set
	 */
	public void setOutputPower(double outputPower) {
		this.outputPower = outputPower;
	}

	/**
	 * @return the lossConversion
	 */
	public double getLossConversion() {
		return lossConversion;
	}

	/**
	 * @param lossConversion
	 *            the lossConversion to set
	 */
	public void setLossConversion(double lossConversion) {
		this.lossConversion = lossConversion;
	}
}
