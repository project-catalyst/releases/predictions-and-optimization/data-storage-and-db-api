package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;

import java.util.Objects;
import java.util.UUID;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 * Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({@JsonSubTypes.Type(value = LiquidCooling.class), @JsonSubTypes.Type(value = AirCooling.class)})
public class CoolingSystem extends Facility {
    private static final long serialVersionUID = 1L;
    protected double coolingCapacityKWh;
    protected double minCoolingLoadKWh;
    protected double maxCoolingLoadKWh;
    @DynamicField
    protected double inputAirTemperatureCS;
    @DynamicField
    protected double outputAirTemperatureCS;
    protected double copC;
    protected double copH;

    @DynamicField
    protected double flowCS;
    protected double specificHeatCS;

    public CoolingSystem() {
    }


    public CoolingSystem(String deviceLabel, double coolingCapacityKWh, double minCoolingLoadKWh, double maxCoolingLoadKWh,
                         double inputAirTemperatureCS, double outputAirTemperatureCS, double copC, double copH, double flow, double specificHeat, double energyConsumption) {
        super(deviceLabel, energyConsumption);
        this.coolingCapacityKWh = coolingCapacityKWh;
        this.minCoolingLoadKWh = minCoolingLoadKWh;
        this.maxCoolingLoadKWh = maxCoolingLoadKWh;
        this.copC = copC;
        this.copH = copH;
        this.flowCS = flow;
        this.specificHeatCS = specificHeat;
        this.inputAirTemperatureCS = inputAirTemperatureCS;
        this.outputAirTemperatureCS = outputAirTemperatureCS;
    }

    public CoolingSystem(String deviceLabel, double energyConsumption, double maxCoolingLoadKWh, double copC, double copH, double coolingCapacityKWh) {
        super(deviceLabel, energyConsumption);
        this.coolingCapacityKWh = coolingCapacityKWh;
        this.maxCoolingLoadKWh = maxCoolingLoadKWh;
        this.copC = copC;
        this.copH = copH;
    }

    public CoolingSystem(UUID deviceId, String deviceLabel, double energyConsumption, double maxCoolingLoadKWh, double copC, double copH, double coolingCapacityKWh) {
        super(deviceId, deviceLabel, energyConsumption);
        this.maxCoolingLoadKWh = maxCoolingLoadKWh;
        this.copC = copC;
        this.copH = copH;
        this.coolingCapacityKWh = coolingCapacityKWh;
    }

    public double getCoolingCapacityKWh() {
        return coolingCapacityKWh;
    }

    public void setCoolingCapacityKWh(double coolingCapacityKWh) {
        this.coolingCapacityKWh = coolingCapacityKWh;
    }

    public double getMinCoolingLoadKWh() {
        return minCoolingLoadKWh;
    }

    public void setMinCoolingLoadKWh(double minCoolingLoadKWh) {
        this.minCoolingLoadKWh = minCoolingLoadKWh;
    }

    public double getMaxCoolingLoadKWh() {
        return maxCoolingLoadKWh;
    }

    public void setMaxCoolingLoadKWh(double maxCoolingLoadKWh) {
        this.maxCoolingLoadKWh = maxCoolingLoadKWh;
    }

    public double getInputAirTemperatureCS() {
        return inputAirTemperatureCS;
    }

    public void setInputAirTemperatureCS(double inputAirTemperatureCS) {
        this.inputAirTemperatureCS = inputAirTemperatureCS;
    }

    public double getOutputAirTemperatureCS() {
        return outputAirTemperatureCS;
    }

    public void setOutputAirTemperatureCS(double outputAirTemperatureCS) {
        this.outputAirTemperatureCS = outputAirTemperatureCS;
    }

    public double getFlowCS() {
        return flowCS;
    }

    public void setFlowCS(double flow) {
        this.flowCS = flow;
    }

    public double getSpecificHeatCS() {
        return specificHeatCS;
    }

    public void setSpecificHeatCS(double specificHeat) {
        this.specificHeatCS = specificHeat;
    }

    public double getCopC() {
        return copC;
    }

    public void setCopC(double copC) {
        this.copC = copC;
    }

    public double getCopH() {
        return copH;
    }

    public void setCopH(double copH) {
        this.copH = copH;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoolingSystem that = (CoolingSystem) o;
        return  that.deviceLabel.equals(deviceLabel) &&
                Double.compare(that.coolingCapacityKWh, coolingCapacityKWh) == 0 &&
                Double.compare(that.energyConsumption, energyConsumption) == 0 &&
                Double.compare(that.maxCoolingLoadKWh, maxCoolingLoadKWh) == 0 &&
                Double.compare(that.copC, copC) == 0 &&
                Double.compare(that.copH, copH) == 0 ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coolingCapacityKWh, minCoolingLoadKWh, maxCoolingLoadKWh, inputAirTemperatureCS, outputAirTemperatureCS, copC, copH, flowCS, specificHeatCS);
    }

    @Override
    public String toString() {
        return "CoolingSystem{" +
                "coolingCapacityKWh=" + coolingCapacityKWh +
                ", minCoolingLoadKWh=" + minCoolingLoadKWh +
                ", maxCoolingLoadKWh=" + maxCoolingLoadKWh +
                ", inputAirTemperatureCS=" + inputAirTemperatureCS +
                ", outputAirTemperatureCS=" + outputAirTemperatureCS +
                ", copC=" + copC +
                ", copH=" + copH +
                ", flowCS=" + flowCS +
                ", specificHeatCS=" + specificHeatCS +
                '}';
    }
}
