package ro.tuc.dsrl.geyser.datamodel.components.production;


import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class Wind extends GreenEnergySource {
	private static final long serialVersionUID = 1L;
	private double bladeLength;
	@DynamicField
	private double windSpeed;
	private double airDensity;
	private double powerCoefficient;

	public Wind() {
	}
	@Override
	public double estimateEnergyProduction() {
		// P= 1/2*(p* A * v^3 * Cp )
		double area = Math.PI * Math.pow(bladeLength, 2);
		return 0.5 * (airDensity * area * Math.pow(windSpeed, 3) * powerCoefficient);
	}

	/**
	 * @return the bladeLength
	 */
	public double getBladeLength() {
		return bladeLength;
	}

	/**
	 * @param bladeLength
	 *            the bladeLength to set
	 */
	public void setBladeLength(double bladeLength) {
		this.bladeLength = bladeLength;
	}

	/**
	 * @return the windSpeed
	 */
	public double getWindSpeed() {
		return windSpeed;
	}

	/**
	 * @param windSpeed
	 *            the windSpeed to set
	 */
	public void setWindSpeed(double windSpeed) {
		this.windSpeed = windSpeed;
	}

	/**
	 * @return the airDensity
	 */
	public double getAirDensity() {
		return airDensity;
	}

	/**
	 * @param airDensity
	 *            the airDensity to set
	 */
	public void setAirDensity(double airDensity) {
		this.airDensity = airDensity;
	}

	/**
	 * @return the powerCoefficient
	 */
	public double getPowerCoefficient() {
		return powerCoefficient;
	}

	/**
	 * @param powerCoefficient
	 *            the powerCoefficient to set
	 */
	public void setPowerCoefficient(double powerCoefficient) {
		this.powerCoefficient = powerCoefficient;
	}
}
