package ro.tuc.dsrl.geyser.datamodel.other;

import ro.tuc.dsrl.geyser.datamodel.actions.EnergyEfficiencyOptimizationAction;

import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class OptimizerPlan {

	private long id;
	private List<EnergyEfficiencyOptimizationAction> actions;
	private double confidenceLevel;
	private double WE;
	private double WL;
	private double WF;
	private double WT;
	private double Wren;
	private boolean reallocActive;

	public OptimizerPlan() {
	}

	public OptimizerPlan(long id, List<EnergyEfficiencyOptimizationAction> actions) {
		this.id = id;
		this.actions = actions;
	}

	public OptimizerPlan(long id, List<EnergyEfficiencyOptimizationAction> actions, double confidenceLevel) {
		this.id = id;
		this.actions = actions;
		this.confidenceLevel = confidenceLevel;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the actions
	 */
	public List<EnergyEfficiencyOptimizationAction> getActions() {
		return actions;
	}

	/**
	 * @param actions
	 *            the actions to set
	 */
	public void setActions(List<EnergyEfficiencyOptimizationAction> actions) {
		this.actions = actions;
	}

	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	public double getWE() {
		return WE;
	}

	public void setWE(double WE) {
		this.WE = WE;
	}

	public double getWL() {
		return WL;
	}

	public void setWL(double WL) {
		this.WL = WL;
	}

	public double getWF() {
		return WF;
	}

	public void setWF(double WF) {
		this.WF = WF;
	}

	public double getWT() {
		return WT;
	}

	public void setWT(double WT) {
		this.WT = WT;
	}

	public double getWren() {
		return Wren;
	}

	public void setWren(double Wren) {
		this.Wren = Wren;
	}

	public boolean isReallocActive() {
		return reallocActive;
	}

	public void setReallocActive(boolean reallocActive) {
		this.reallocActive = reallocActive;
	}
}
