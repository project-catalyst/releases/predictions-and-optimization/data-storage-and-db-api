package ro.tuc.dsrl.geyser.datamodel.indicators;


import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.annotations.InstanceId;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class EnergyEfficiencyIndicators {
	@InstanceId
	private long id;
	@DynamicField
	private double indicatorValue;
	private double optimalValue;
	private double threshold;

	public EnergyEfficiencyIndicators() {
		super();
	}

	public EnergyEfficiencyIndicators(long id, double indicatorValue, double optimalValue, double threshold) {
		this.id = id;
		this.indicatorValue = indicatorValue;
		this.optimalValue = optimalValue;
		this.threshold = threshold;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the indicatorValue
	 */
	public double getIndicatorValue() {
		return indicatorValue;
	}

	/**
	 * @param indicatorValue
	 *            the indicatorValue to set
	 */
	public void setIndicatorValue(double indicatorValue) {
		this.indicatorValue = indicatorValue;
	}

	/**
	 * @return the optimalValue
	 */
	public double getOptimalValue() {
		return optimalValue;
	}

	/**
	 * @param optimalValue
	 *            the optimalValue to set
	 */
	public void setOptimalValue(double optimalValue) {
		this.optimalValue = optimalValue;
	}

	/**
	 * @return the threshold
	 */
	public double getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold
	 *            the threshold to set
	 */
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

}
