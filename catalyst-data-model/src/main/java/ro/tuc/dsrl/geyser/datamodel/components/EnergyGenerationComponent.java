package ro.tuc.dsrl.geyser.datamodel.components;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.components.production.BrownEnergySource;
import ro.tuc.dsrl.geyser.datamodel.components.production.GreenEnergySource;

;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = GreenEnergySource.class),
		@JsonSubTypes.Type(value = BrownEnergySource.class) })
public class EnergyGenerationComponent extends Component {
	private static final long serialVersionUID = 1L;

	protected double maxCapacity;
	@DynamicField
	protected double energyProduction;
	protected double efficiency;

	public EnergyGenerationComponent() {
	}

	public EnergyGenerationComponent(double maxCapacity, double energyProduction, double efficiency) {
		this.maxCapacity = maxCapacity;
		this.energyProduction = energyProduction;
		this.efficiency = efficiency;
	}


	public double estimateEnergyProduction() {
		return -1;
	}

	/**
	 * @return the maxCapacity
	 */
	public double getMaxCapacity() {
		return maxCapacity;
	}

	/**
	 * @param maxCapacity the maxCapacity to set
	 */
	public void setMaxCapacity(double maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	/**
	 * @return the energyProduction
	 */
	public double getEnergyProduction() {
		return energyProduction;
	}

	/**
	 * @param energyProduction the energyProduction to set
	 */
	public void setEnergyProduction(double energyProduction) {
		this.energyProduction = energyProduction;
	}

	/**
	 * @return the efficiency
	 */
	public double getEfficiency() {
		return efficiency;
	}

	/**
	 * @param efficiency the efficiency to set
	 */
	public void setEfficiency(double efficiency) {
		this.efficiency = efficiency;
	}
}
