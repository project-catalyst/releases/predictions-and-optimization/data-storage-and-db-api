package ro.tuc.dsrl.geyser.datamodel.actions;


import ro.tuc.dsrl.geyser.datamodel.components.production.Generator;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class RunDieselGeneratorsForPeriodicMaintenance extends NonITComponentsOptimization {

	private static final long serialVersionUID = -7389458093925177376L;
	private Generator generator;
	
	
	public static final String ACTION_TYPE = "Diesel (KWh)";

	public RunDieselGeneratorsForPeriodicMaintenance() {
		super(ACTION_TYPE);
	}
	
	public RunDieselGeneratorsForPeriodicMaintenance(long id, double estimatedEnergySaving, Date startTime,
			Date endTime, Generator generator) {
		super(id,ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
		this.generator = generator;
	}

	/**
	 * @return the generator
	 */
	public Generator getGenerator() {
		return generator;
	}

	/**
	 * @param generator
	 *            the generator to set
	 */
	public void setGenerator(Generator generator) {
		this.generator = generator;
	}

}
