package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.annotations.ObjectProperty;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class HDD extends IndividualComponent {

	private static final long serialVersionUID = -7356800606432470011L;
	private double totalCapacityHDD;
	@DynamicField
	private double usedCapacityHDD;
	private double speedRPM;
	private double transferRate;
	@DynamicField
	private double readSpeed;
	@DynamicField
	private double writeSpeed;
	@JsonIgnore
	@ObjectProperty(value = "components")
	private Server server;

	public HDD() {
	}

	public HDD(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double totalCapacity, double usedCapacity,
			double speedRPM, double transferRate, double readSpeed, double writeSpeed, double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				energyConsumption);
		this.totalCapacityHDD = totalCapacity;
		this.usedCapacityHDD = usedCapacity;
		this.speedRPM = speedRPM;
		this.transferRate = transferRate;
		this.readSpeed = readSpeed;
		this.writeSpeed = writeSpeed;
	}

	public static HDD createDynamicInstance(String deviceLabel, Double usedCapacity, Double readSpeed, Double writeSpeed) {
		return new HDD(deviceLabel, -1, -1, -1, -1, -1, -1, -1, usedCapacity, -1, -1, readSpeed, writeSpeed, 0);
	}

	/**
	 * @return the totalCapacityHDD
	 */
	public double getTotalCapacityHDD() {
		return totalCapacityHDD;
	}

	/**
	 * @param totalCapacityHDD the totalCapacityHDD to set
	 */
	public void setTotalCapacityHDD(double totalCapacityHDD) {
		this.totalCapacityHDD = totalCapacityHDD;
	}

	/**
	 * @return the usedCapacityHDD
	 */
	public double getUsedCapacityHDD() {
		return usedCapacityHDD;
	}

	/**
	 * @param usedCapacityHDD the usedCapacityHDD to set
	 */
	public void setUsedCapacityHDD(double usedCapacityHDD) {
		this.usedCapacityHDD = usedCapacityHDD;
	}

	/**
	 * @return the speedRPM
	 */
	public double getSpeedRPM() {
		return speedRPM;
	}

	/**
	 * @param speedRPM the speedRPM to set
	 */
	public void setSpeedRPM(double speedRPM) {
		this.speedRPM = speedRPM;
	}

	/**
	 * @return the transferRate
	 */
	public double getTransferRate() {
		return transferRate;
	}

	/**
	 * @param transferRate the transferRate to set
	 */
	public void setTransferRate(double transferRate) {
		this.transferRate = transferRate;
	}

	/**
	 * @return the readSpeed
	 */
	public double getReadSpeed() {
		return readSpeed;
	}

	/**
	 * @param readSpeed the readSpeed to set
	 */
	public void setReadSpeed(double readSpeed) {
		this.readSpeed = readSpeed;
	}

	/**
	 * @return the writeSpeed
	 */
	public double getWriteSpeed() {
		return writeSpeed;
	}

	/**
	 * @param writeSpeed the writeSpeed to set
	 */
	public void setWriteSpeed(double writeSpeed) {
		this.writeSpeed = writeSpeed;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

}
