package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class DynamicallyUsingNonElectricalCooling extends NonITComponentsOptimization {

	private static final long serialVersionUID = -2197622726463315319L;

	private double nonElectricalCoolingIntensity; // percent
	
	
	public static final String ACTION_TYPE = "Dynamically Using Non Electrical Cooling(KWh)";

	public DynamicallyUsingNonElectricalCooling() {
		super(ACTION_TYPE);
	}
	
	public DynamicallyUsingNonElectricalCooling(long id, double estimatedEnergySaving, Date startTime, Date endTime,
			double nonElectricalCoolingIntensity) {
		super(id,ACTION_TYPE,  estimatedEnergySaving, startTime, endTime);
		this.nonElectricalCoolingIntensity = nonElectricalCoolingIntensity;
	}

	/**
	 * @return the nonElectricalCoolingIntensity
	 */
	public double getNonElectricalCoolingIntensity() {
		return nonElectricalCoolingIntensity;
	}

	/**
	 * @param nonElectricalCoolingIntensity
	 *            the nonElectricalCoolingIntensity to set
	 */
	public void setNonElectricalCoolingIntensity(double nonElectricalCoolingIntensity) {
		this.nonElectricalCoolingIntensity = nonElectricalCoolingIntensity;
	}

}
