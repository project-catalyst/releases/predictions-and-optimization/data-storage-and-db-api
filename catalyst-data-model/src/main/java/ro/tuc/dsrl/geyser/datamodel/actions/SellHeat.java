package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

public class SellHeat extends MarketplaceAction {

    private static final long serialVersionUID = 7531642642978732379L;
    public static final String ACTION_TYPE = "Sell Heat (KWh)";

    public SellHeat() {
        super(ACTION_TYPE);
    }

    public SellHeat(long id, Date startTime, Date endTime, double energyAmount, double price) {
        super(id,ACTION_TYPE, startTime, endTime, energyAmount, price);
    }

    @Override
    public String toString() {
        String toString = "SellHeat id : " + getId() + "\n";
        toString += super.toString();
        return toString;
    }

}