package ro.tuc.dsrl.geyser.datamodel.indicators;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class FEW extends LoadShifting {

	public FEW() {
		super();
	}
	
	public FEW(long id, double indicatorValue, double optimalValue, double threshold) {
		super(id, indicatorValue, optimalValue, threshold);
	}
	
	public static FEW createDynamicInstance(long id, double indicatorValue) {
		return new FEW(id, indicatorValue, -1, -1);
	}
}
