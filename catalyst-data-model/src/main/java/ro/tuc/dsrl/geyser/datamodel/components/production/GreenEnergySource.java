package ro.tuc.dsrl.geyser.datamodel.components.production;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyGenerationComponent;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = Wind.class), @JsonSubTypes.Type(value = Photovoltaic.class),
		@JsonSubTypes.Type(value = Geothermal.class) })
public class GreenEnergySource extends EnergyGenerationComponent {
	private static final long serialVersionUID = -2346472734540974952L;

	public GreenEnergySource() {

	}

}
