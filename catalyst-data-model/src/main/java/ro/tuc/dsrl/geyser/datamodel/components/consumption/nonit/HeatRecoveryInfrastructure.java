package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

import java.util.Objects;
import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class HeatRecoveryInfrastructure extends Facility {
	private static final long serialVersionUID = 1L;
	private double heatAbsortion;
	private double copH;
	private double maxHeatLoadKWh;
	private double heatCapacityKWh;

	public HeatRecoveryInfrastructure(){}


	public HeatRecoveryInfrastructure( String deviceLabel, double energyConsumption, double copH, double maxHeatLoadKWh, double heatCapacityKWh) {
		super(null,deviceLabel, energyConsumption);
		this.copH = copH;
		this.maxHeatLoadKWh = maxHeatLoadKWh;
		this.heatCapacityKWh = heatCapacityKWh;
	}

	public HeatRecoveryInfrastructure(UUID deviceId, String deviceLabel, double energyConsumption, double copH, double maxHeatLoadKWh, double heatCapacityKWh) {
		super(deviceId,deviceLabel, energyConsumption);
		this.copH = copH;
		this.maxHeatLoadKWh = maxHeatLoadKWh;
		this.heatCapacityKWh = heatCapacityKWh;
	}

	/**
	 * @return the heatAbsortion
	 */
	public double getHeatAbsortion() {
		return heatAbsortion;
	}

	/**
	 * @param heatAbsortion the heatAbsortion to set
	 */
	public void setHeatAbsortion(double heatAbsortion) {
		this.heatAbsortion = heatAbsortion;
	}


	public double getCopH() {
		return copH;
	}

	public void setCopH(double copH) {
		this.copH = copH;
	}

	public double getMaxHeatLoadKWh() {
		return maxHeatLoadKWh;
	}

	public void setMaxHeatLoadKWh(double maxHeatLoadKWh) {
		this.maxHeatLoadKWh = maxHeatLoadKWh;
	}

	public double getHeatCapacityKWh() {
		return heatCapacityKWh;
	}

	public void setHeatCapacityKWh(double heatCapacityKWh) {
		this.heatCapacityKWh = heatCapacityKWh;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		HeatRecoveryInfrastructure that = (HeatRecoveryInfrastructure) o;
		return  that.deviceLabel.equals(deviceLabel) &&
				Double.compare(that.energyConsumption, energyConsumption) == 0 &&
				Double.compare(that.copH, copH) == 0 &&
				Double.compare(that.maxHeatLoadKWh, maxHeatLoadKWh) == 0 &&
				Double.compare(that.heatCapacityKWh, heatCapacityKWh) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(heatAbsortion, copH, maxHeatLoadKWh, heatCapacityKWh);
	}

	@Override
	public String toString() {
		return "HeatRecoveryInfrastructure{" +
				"heatAbsortion=" + heatAbsortion +
				", copH=" + copH +
				", maxHeatLoadKWh=" + maxHeatLoadKWh +
				", heatCapacityKWh=" + heatCapacityKWh +
				'}';
	}
}
