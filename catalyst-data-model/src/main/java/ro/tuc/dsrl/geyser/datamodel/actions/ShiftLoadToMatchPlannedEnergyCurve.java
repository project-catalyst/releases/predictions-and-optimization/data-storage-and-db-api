package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;
import java.util.List;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ShiftLoadToMatchPlannedEnergyCurve extends SmartGridInteractionOptimization {

	private static final long serialVersionUID = 4785744352888431830L;
	private List<DateTimeValue> plannedEneregyCurve;

	private static final String ACTION_TYPE = "Shift Load To Match Planned Energy Curve";

	public ShiftLoadToMatchPlannedEnergyCurve() {
		super(ACTION_TYPE);
	}

	public ShiftLoadToMatchPlannedEnergyCurve(long id, List<EnergyConsumptionOptimization> actions,
			double estimatedLoadShifted, Date start, Date end) {
		super(id, ACTION_TYPE, actions, estimatedLoadShifted,start,end);
	}

	/**
	 * @return the plannedEneregyCurve
	 */
	public List<DateTimeValue> getPlannedEneregyCurve() {
		return plannedEneregyCurve;
	}

	/**
	 * @param plannedEneregyCurve
	 *            the plannedEneregyCurve to set
	 */
	public void setPlannedEneregyCurve(List<DateTimeValue> plannedEneregyCurve) {
		this.plannedEneregyCurve = plannedEneregyCurve;
	}

}
