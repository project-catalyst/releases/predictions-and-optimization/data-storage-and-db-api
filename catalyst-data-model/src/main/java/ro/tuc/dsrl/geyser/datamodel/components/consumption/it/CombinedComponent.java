package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import java.util.List;
import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = Server.class), @JsonSubTypes.Type(value = ServerRack.class),
		@JsonSubTypes.Type(value = StorageServer.class) })
public class CombinedComponent extends ITComponent {

	private static final long serialVersionUID = 6446562246707634556L;
	protected List<IndividualComponent> components;

	public CombinedComponent() {
	}

	public CombinedComponent(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double inputAirTemperatureCC,
			double outputAirTemperatureCC, double airFlowCC, double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				energyConsumption);
	}

	public CombinedComponent(UUID deviceId, String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
							 double highMoistureLimit, double pIdle, double pMax, double inputAirTemperatureCC,
							 double outputAirTemperatureCC, double airFlowCC, double energyConsumption) {
		super(deviceId, deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				energyConsumption);
	}

	/**
	 * @return the components
	 */
	public List<IndividualComponent> getComponents() {
		return components;
	}

	/**
	 * @param components the components to set
	 */
	public void setComponents(List<IndividualComponent> components) {
		this.components = components;
	}
}
