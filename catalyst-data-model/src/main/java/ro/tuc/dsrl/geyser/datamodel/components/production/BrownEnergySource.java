package ro.tuc.dsrl.geyser.datamodel.components.production;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyGenerationComponent;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = Generator.class), @JsonSubTypes.Type(value = CHP.class) })
public class BrownEnergySource extends EnergyGenerationComponent {

	private static final long serialVersionUID = 1L;
	@DynamicField
	protected double emissionsCO2;
	protected double sourcePriority;

	public BrownEnergySource() {

	}

	public BrownEnergySource(double maxCapacity, double energyProduction, double efficiency, double emissionsCO2,
			double sourcePriority) {
		super(maxCapacity, energyProduction, efficiency);
		this.emissionsCO2 = emissionsCO2;
		this.sourcePriority = sourcePriority;
	}


	/**
	 * @return the emissionsCO2
	 */
	public double getEmissionsCO2() {
		return emissionsCO2;
	}

	/**
	 * @param emissionsCO2
	 *            the emissionsCO2 to set
	 */
	public void setEmissionsCO2(double emissionsCO2) {
		this.emissionsCO2 = emissionsCO2;
	}

	/**
	 * @return the sourcePriority
	 */
	public double getSourcePriority() {
		return sourcePriority;
	}

	/**
	 * @param sourcePriority
	 *            the sourcePriority to set
	 */
	public void setSourcePriority(double sourcePriority) {
		this.sourcePriority = sourcePriority;
	}

}
