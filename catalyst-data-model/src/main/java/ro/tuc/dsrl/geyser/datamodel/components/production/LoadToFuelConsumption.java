package ro.tuc.dsrl.geyser.datamodel.components.production;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.annotations.InstanceId;
import ro.tuc.dsrl.geyser.datamodel.annotations.ObjectProperty;

import java.io.Serializable;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class LoadToFuelConsumption implements Serializable {

	private static final long serialVersionUID = 1L;
	//TODO: loadValue &fuelValue are static fields... but there are no equivalent static values in the db...yet
	@DynamicField
	private double loadValue;
	@DynamicField
	private double fuelValue;
	@JsonIgnore
	@ObjectProperty(value="loadToFuelConsumption")
	private Generator generator;
	@InstanceId
	private long id;

	public LoadToFuelConsumption() {
	}

	public LoadToFuelConsumption(double loadValue, double fuelValue) {
		super();
		id = System.nanoTime();
		this.loadValue = loadValue;
		this.fuelValue = fuelValue;
	}

	public LoadToFuelConsumption(double loadValue, double fuelValue, Generator generator) {
		super();
		id = System.nanoTime();
		this.loadValue = loadValue;
		this.fuelValue = fuelValue;
		this.generator = generator;
	}

	public LoadToFuelConsumption(long id, double loadValue, double fuelValue, Generator generator) {
		super();
		this.id = id;
		this.loadValue = loadValue;
		this.fuelValue = fuelValue;
		this.generator = generator;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Generator getGenerator() {
		return generator;
	}

	public void setGenerator(Generator generator) {
		this.generator = generator;
	}

	/**
	 * @return the loadValue
	 */
	public double getLoadValue() {
		return loadValue;
	}

	/**
	 * @param loadValue
	 *            the loadValue to set
	 */
	public void setLoadValue(double loadValue) {
		this.loadValue = loadValue;
	}

	/**
	 * @return the fuelValue
	 */
	public double getFuelValue() {
		return fuelValue;
	}

	/**
	 * @param fuelValue
	 *            the fuelValue to set
	 */
	public void setFuelValue(double fuelValue) {
		this.fuelValue = fuelValue;
	}
}
