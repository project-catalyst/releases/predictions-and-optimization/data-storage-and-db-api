//package ro.tuc.dsrl.geyser.datamodel.other;
//
//
//import java.time.LocalDateTime;
//
//public enum Scenarios {
//	CATALYST_0("none","scenario0",  LocalDateTime.of(2019, 10, 10, 0, 0,0)),
//	AS_CATALYST_POZNAN("catalyst/realtime_workload-catalyst-poznan.xls","",  LocalDateTime.of(2019, 9, 9, 0, 0,0)),
//	AS_CATALYST("catalyst/realtime_workload-catalyst-v2.xls","",  LocalDateTime.of(2018, 3, 3, 0, 0,0));
//
//	private LocalDateTime time;
//	private String fileAlgorithm;
//	private String fileInputData;
//
//	private Scenarios(String fileAlgorithm, String fileInputData, LocalDateTime time) {
//		this.fileAlgorithm = fileAlgorithm;
//		this.fileInputData = fileInputData;
//		this.time = time;
//	}
//
//	public String fileAlgorithm() {
//		return "/src/main/resources/scenarios/" + fileAlgorithm;
//	}
//
//	public String getFileInputData() {
//		return "/data/" + fileInputData;
//	}
//
//	public String getFileName() {
//		return  fileInputData;
//	}
//
//	public LocalDateTime time() {
//		return time;
//	}
//}
