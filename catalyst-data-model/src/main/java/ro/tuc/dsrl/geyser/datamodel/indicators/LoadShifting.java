package ro.tuc.dsrl.geyser.datamodel.indicators;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class LoadShifting extends EnergyEfficiencyIndicators {

	public LoadShifting() {
		super();
	}
	
	public LoadShifting(long id, double indicatorValue, double optimalValue, double threshold) {
		super(id, indicatorValue, optimalValue, threshold);
	}
}
