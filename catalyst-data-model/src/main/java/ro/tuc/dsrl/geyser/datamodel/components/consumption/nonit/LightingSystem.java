package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class LightingSystem extends Facility {
	private static final long serialVersionUID = 1L;
	private double illuminance;
	private double luminousEfficacy;
	private double surfaceArea;

	public LightingSystem(String deviceLabel, double illuminance, double luminousEfficacy, double surfaceArea,
			double energyConsumption) {
		super(deviceLabel, energyConsumption);
		this.illuminance = illuminance;
		this.luminousEfficacy = luminousEfficacy;
		this.surfaceArea = surfaceArea;
	}

	/**
	 * @return the illuminance
	 */
	public double getIlluminance() {
		return illuminance;
	}

	/**
	 * @param illuminance the illuminance to set
	 */
	public void setIlluminance(double illuminance) {
		this.illuminance = illuminance;
	}

	/**
	 * @return the luminousEfficacy
	 */
	public double getLuminousEfficacy() {
		return luminousEfficacy;
	}

	/**
	 * @param luminousEfficacy the luminousEfficacy to set
	 */
	public void setLuminousEfficacy(double luminousEfficacy) {
		this.luminousEfficacy = luminousEfficacy;
	}

	/**
	 * @return the surfaceArea
	 */
	public double getSurfaceArea() {
		return surfaceArea;
	}

	/**
	 * @param surfaceArea the surfaceArea to set
	 */
	public void setSurfaceArea(double surfaceArea) {
		this.surfaceArea = surfaceArea;
	}

}
