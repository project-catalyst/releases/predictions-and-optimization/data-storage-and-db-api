package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class BuyEnergy extends MarketplaceAction {

	private static final long serialVersionUID = 7531642642978732379L;
	public static final String ACTION_TYPE = "Buy Energy (KWh)";
	
	public BuyEnergy() {
		super(ACTION_TYPE);
	}

	public BuyEnergy(long id, Date startTime, Date endTime, double energyAmount, double price) {
		super(id,ACTION_TYPE, startTime, endTime, energyAmount, price);
	}

	@Override
	public String toString() {
		String toString = "BuyEnergy id : " + getId() + "\n";
		toString += super.toString();
		return toString;
	}

}
