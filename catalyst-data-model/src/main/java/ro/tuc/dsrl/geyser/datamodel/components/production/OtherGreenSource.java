package ro.tuc.dsrl.geyser.datamodel.components.production;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class OtherGreenSource extends GreenEnergySource {
	private static final long serialVersionUID = -1675863503438296757L;

}
