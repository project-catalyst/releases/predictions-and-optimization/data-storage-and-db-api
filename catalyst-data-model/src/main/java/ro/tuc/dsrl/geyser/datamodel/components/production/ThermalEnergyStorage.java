package ro.tuc.dsrl.geyser.datamodel.components.production;

import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;

import java.util.Objects;
import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 * Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 */
public class ThermalEnergyStorage extends EnergyStorageComponent {

    private static final long serialVersionUID = 1L;
    private double specificHeat;
    private double temperatureDifference;
    private double tesFactor;

    public ThermalEnergyStorage() {
    }

    public ThermalEnergyStorage( String deviceLabel, double actualLoadedCapacity, double factor, double maxDischargeRate, double maxChargeRate,double chargeLossRate, double dischargeLossRate, double maximumCapacity) {
        super(null, deviceLabel, actualLoadedCapacity, maxDischargeRate, maxChargeRate, chargeLossRate, chargeLossRate,
                dischargeLossRate, maximumCapacity);
        this.deviceLabel = deviceLabel;
        this.tesFactor = factor;
    }

    public ThermalEnergyStorage(UUID deviceId, String deviceLabel, double actualLoadedCapacity, double factor, double maxDischargeRate, double maxChargeRate,double chargeLossRate, double dischargeLossRate, double maximumCapacity) {
        super(deviceId, deviceLabel, actualLoadedCapacity, maxDischargeRate, maxChargeRate, chargeLossRate, chargeLossRate,
                dischargeLossRate, maximumCapacity);
        this.deviceLabel = deviceLabel;
        this.tesFactor = factor;
    }

    public ThermalEnergyStorage(UUID id, String deviceLabel, double actualLoadedCapacity, double maxDischargeRate, double maxChargeRate, double energyLossRate,
                                double chargeLossRate, double dischargeLossRate, double maximumCapacity, double specificHeat,
                                double temperatureDifference) {
        super(id, deviceLabel, actualLoadedCapacity, maxDischargeRate, maxChargeRate,
                energyLossRate, chargeLossRate, dischargeLossRate, maximumCapacity);
        this.specificHeat = specificHeat;
        this.temperatureDifference = temperatureDifference;

    }

    public static ThermalEnergyStorage createDynamicInstance(UUID id, String deviceLabel, double value) {
        return new ThermalEnergyStorage(id, deviceLabel, value, -1, -1, -1, -1, -1, -1, -1, -1);
    }

    /**
     * @return the specificHeat
     */
    public double getSpecificHeat() {
        return specificHeat;
    }

    /**
     * @param specificHeat the specificHeat to set
     */
    public void setSpecificHeat(double specificHeat) {
        this.specificHeat = specificHeat;
    }

    /**
     * @return the temperatureDifference
     */
    public double getTemperatureDifference() {
        return temperatureDifference;
    }

    /**
     * @param temperatureDifference the temperatureDifference to set
     */
    public void setTemperatureDifference(double temperatureDifference) {
        this.temperatureDifference = temperatureDifference;
    }

    public double getTesFactor() {
        return tesFactor;
    }

    public void setTesFactor(double tesFactor) {
        this.tesFactor = tesFactor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThermalEnergyStorage that = (ThermalEnergyStorage) o;
        return Double.compare(that.specificHeat, specificHeat) == 0 &&
                Double.compare(that.temperatureDifference, temperatureDifference) == 0 &&
                Double.compare(that.tesFactor, tesFactor) == 0&&
                Double.compare(that.chargeLossRate, chargeLossRate) == 0 &&
                Double.compare(that.dischargeLossRate, dischargeLossRate) == 0 &&
                Double.compare(that.maxChargeRate, maxChargeRate) == 0 &&
                Double.compare(that.maxDischargeRate, maxDischargeRate) == 0 &&
                Double.compare(that.maximumCapacity, maximumCapacity) == 0 &&
                Double.compare(that.actualLoadedCapacity, actualLoadedCapacity) == 0 ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(specificHeat, temperatureDifference, tesFactor);
    }

    @Override
    public String toString() {
        return "ThermalEnergyStorage{" +
                "tesFactor=" + tesFactor +
                ", actualLoadedCapacity=" + actualLoadedCapacity +
                ", maxDischargeRate=" + maxDischargeRate +
                ", maxChargeRate=" + maxChargeRate +
                ", energyLossRate=" + energyLossRate +
                ", chargeLossRate=" + chargeLossRate +
                ", dischargeLossRate=" + dischargeLossRate +
                ", maximumCapacity=" + maximumCapacity +
                ", deviceLabel='" + deviceLabel + '\'' +
                ", deviceId=" + deviceId +
                '}';
    }
}
