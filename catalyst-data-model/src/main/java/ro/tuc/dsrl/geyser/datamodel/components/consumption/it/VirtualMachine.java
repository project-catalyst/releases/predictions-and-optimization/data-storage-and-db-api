package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;



import ro.tuc.dsrl.geyser.datamodel.annotations.InstanceId;
import ro.tuc.dsrl.geyser.datamodel.annotations.ObjectProperty;

import java.io.Serializable;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class VirtualMachine implements Serializable {
	private static final long serialVersionUID = -6651117768601104230L;

	@InstanceId
	private long id;
	private String uuid;

	private long requestArrivalTime;
	private long requestDeadline;
	private double requestCPU;
	private double requestRAM;
	private double requestHDD;

	private double usedCapacityCPU;
	private double usedCapacityRAM;
	private double usedCapacityHDD;
	private double energyConsumption;

	@ObjectProperty(value = "virtualMachines")
	private Server server;

	public VirtualMachine() {
	}

	public VirtualMachine(String deviceLabel, String uuid, long requestArrivalTime, long requestDeadline, double requestCPU,
			double requestRAM, double requestHDD, double usedCapacityCPU, double usedCapacityRAM,
			double usedCapacityHDD, double energyConsumption) {
		this.uuid = uuid;
		this.requestArrivalTime = requestArrivalTime;
		this.requestDeadline = requestDeadline;
		this.requestCPU = requestCPU;
		this.requestRAM = requestRAM;
		this.requestHDD = requestHDD;

		this.usedCapacityCPU = usedCapacityCPU;
		this.usedCapacityRAM = usedCapacityRAM;
		this.usedCapacityHDD = usedCapacityHDD;
		this.energyConsumption = energyConsumption;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the requestArrivalTime
	 */
	public long getRequestArrivalTime() {
		return requestArrivalTime;
	}

	/**
	 * @param requestArrivalTime the requestArrivalTime to set
	 */
	public void setRequestArrivalTime(long requestArrivalTime) {
		this.requestArrivalTime = requestArrivalTime;
	}

	/**
	 * @return the requestDeadline
	 */
	public long getRequestDeadline() {
		return requestDeadline;
	}

	/**
	 * @param requestDeadline the requestDeadline to set
	 */
	public void setRequestDeadline(long requestDeadline) {
		this.requestDeadline = requestDeadline;
	}

	/**
	 * @return the requestCPU
	 */
	public double getRequestCPU() {
		return requestCPU;
	}

	/**
	 * @param requestCPU the requestCPU to set
	 */
	public void setRequestCPU(double requestCPU) {
		this.requestCPU = requestCPU;
	}

	/**
	 * @return the requestRAM
	 */
	public double getRequestRAM() {
		return requestRAM;
	}

	/**
	 * @param requestRAM the requestRAM to set
	 */
	public void setRequestRAM(double requestRAM) {
		this.requestRAM = requestRAM;
	}

	/**
	 * @return the requestHDD
	 */
	public double getRequestHDD() {
		return requestHDD;
	}

	/**
	 * @param requestHDD the requestHDD to set
	 */
	public void setRequestHDD(double requestHDD) {
		this.requestHDD = requestHDD;
	}

	/**
	 * @return the usedCapacityCPU
	 */
	public double getUsedCapacityCPU() {
		return usedCapacityCPU;
	}

	/**
	 * @param usedCapacityCPU the usedCapacityCPU to set
	 */
	public void setUsedCapacityCPU(double usedCapacityCPU) {
		this.usedCapacityCPU = usedCapacityCPU;
	}

	/**
	 * @return the usedCapacityRAM
	 */
	public double getUsedCapacityRAM() {
		return usedCapacityRAM;
	}

	/**
	 * @param usedCapacityRAM the usedCapacityRAM to set
	 */
	public void setUsedCapacityRAM(double usedCapacityRAM) {
		this.usedCapacityRAM = usedCapacityRAM;
	}

	/**
	 * @return the usedCapacityHDD
	 */
	public double getUsedCapacityHDD() {
		return usedCapacityHDD;
	}

	/**
	 * @param usedCapacityHDD the usedCapacityHDD to set
	 */
	public void setUsedCapacityHDD(double usedCapacityHDD) {
		this.usedCapacityHDD = usedCapacityHDD;
	}

	/**
	 * @return the energyConsumption
	 */
	public double getEnergyConsumption() {
		return energyConsumption;
	}

	/**
	 * @param energyConsumption the energyConsumption to set
	 */
	public void setEnergyConsumption(double energyConsumption) {
		this.energyConsumption = energyConsumption;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

}
