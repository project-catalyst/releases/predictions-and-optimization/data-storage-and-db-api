package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

public class ThermalSelfHeat extends ITComponentsOptimization {

    private static final long serialVersionUID = 4063489509599708640L;
    private double thermalSelfHeatAmount;
    private String destinationDC;
    public static final String ACTION_TYPE = "Thermal self heat (KWh)";

    public ThermalSelfHeat() {
        super(ACTION_TYPE);
    }

    public ThermalSelfHeat(long id, double estimatedEnergySaving, Date startTime, Date endTime, double thermalSelfHeatAmount, String destinationDC) {
        super(id, ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
        this.thermalSelfHeatAmount = thermalSelfHeatAmount;
        this.destinationDC = destinationDC;
    }

    public double getThermalSelfHeatAmount() {
        return this.thermalSelfHeatAmount;
    }

    public void seThermalSelfHeatAmount(double thermalSelfHeatAmount) {
        this.thermalSelfHeatAmount = thermalSelfHeatAmount;
    }

    public String getDestinationDC() {
        return this.destinationDC;
    }

    public void setDestinationDC(String destinationDC) {
        this.destinationDC = destinationDC;
    }
}
