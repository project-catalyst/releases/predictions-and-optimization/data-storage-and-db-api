package ro.tuc.dsrl.geyser.datamodel.components.production;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class Geothermal extends GreenEnergySource {
	private static final long serialVersionUID = -6488159474338961661L;
	private double fluidSpecificHeat;
	private double flowRate;
	private double sensibleHeat;
	private double parasiticLosses;


	@Override
	public double estimateEnergyProduction() {
		// MW ~= Cp * F * dT * n - P
		return fluidSpecificHeat * flowRate * sensibleHeat * super.getEfficiency() - parasiticLosses;
	}

	/**
	 * @return the fluidSpecificHeat
	 */
	public double getFluidSpecificHeat() {
		return fluidSpecificHeat;
	}

	/**
	 * @param fluidSpecificHeat
	 *            the fluidSpecificHeat to set
	 */
	public void setFluidSpecificHeat(double fluidSpecificHeat) {
		this.fluidSpecificHeat = fluidSpecificHeat;
	}

	/**
	 * @return the flowRate
	 */
	public double getFlowRate() {
		return flowRate;
	}

	/**
	 * @param flowRate
	 *            the flowRate to set
	 */
	public void setFlowRate(double flowRate) {
		this.flowRate = flowRate;
	}

	/**
	 * @return the sensibleHeat
	 */
	public double getSensibleHeat() {
		return sensibleHeat;
	}

	/**
	 * @param sensibleHeat
	 *            the sensibleHeat to set
	 */
	public void setSensibleHeat(double sensibleHeat) {
		this.sensibleHeat = sensibleHeat;
	}

	/**
	 * @return the parasiticLosses
	 */
	public double getParasiticLosses() {
		return parasiticLosses;
	}

	/**
	 * @param parasiticLosses
	 *            the parasiticLosses to set
	 */
	public void setParasiticLosses(double parasiticLosses) {
		this.parasiticLosses = parasiticLosses;
	}
}
