package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ComputedHeat {
	private Date dateCH;
	private double valueCH;

	public ComputedHeat(Date dateCH, double valueCH) {
		this.dateCH = dateCH;
		this.valueCH = valueCH;
	}

	/**
	 * @return the dateCH
	 */
	public Date getDateCH() {
		return dateCH;
	}

	/**
	 * @param dateCH the dateCH to set
	 */
	public void setDateCH(Date dateCH) {
		this.dateCH = dateCH;
	}

	/**
	 * @return the valueCH
	 */
	public double getValueCH() {
		return valueCH;
	}

	/**
	 * @param valueCH the valueCH to set
	 */
	public void setValueCH(double valueCH) {
		this.valueCH = valueCH;
	}
}
