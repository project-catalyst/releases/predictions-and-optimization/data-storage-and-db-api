package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class SLAnegotiations extends ITComponentsOptimization {

	private static final long serialVersionUID = -8860131954591850149L;
	private double adjustementPercent;
	private Date endTimeSLAN;
	private double cost;
	
	private static final String ACTION_TYPE = "SLA negotiation";

	public SLAnegotiations() {
		super(ACTION_TYPE);
	}
	
	public SLAnegotiations(long id, double estimatedEnergySaving, Date startTime, Date endTime,
			double adjustementPercent, Date endTimeSLAN, double cost) {
		super(id,ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
		this.adjustementPercent = adjustementPercent;
		this.endTimeSLAN = endTimeSLAN;
		this.cost = cost;
	}

	/**
	 * @return the adjustementPercent
	 */
	public double getAdjustementPercent() {
		return adjustementPercent;
	}

	/**
	 * @param adjustementPercent
	 *            the adjustementPercent to set
	 */
	public void setAdjustementPercent(double adjustementPercent) {
		this.adjustementPercent = adjustementPercent;
	}

	public Date getEndTimeSLAN() {
		return endTimeSLAN;
	}

	public void setEndTimeSLAN(Date endTimeSLAN) {
		this.endTimeSLAN = endTimeSLAN;
	}

	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}

}
