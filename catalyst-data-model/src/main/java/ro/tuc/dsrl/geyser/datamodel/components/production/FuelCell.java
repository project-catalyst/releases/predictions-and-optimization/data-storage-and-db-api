package ro.tuc.dsrl.geyser.datamodel.components.production;


import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;

import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class FuelCell extends EnergyStorageComponent {
	private static final long serialVersionUID = 1249170696208682921L;

	public FuelCell(UUID id, String deviceLabel, double actualLoadedCapacity, double maxDischargeRate, double maxChargeRate,
					double energyLossRate, double chargeLossRate, double dischargeLossRate, double maximumCapacity) {
		super(id, deviceLabel, actualLoadedCapacity, maxDischargeRate, maxChargeRate, energyLossRate, chargeLossRate,
				dischargeLossRate, maximumCapacity);
	}
}
