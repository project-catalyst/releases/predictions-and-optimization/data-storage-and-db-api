package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ShiftDelayTolerantWorkload extends ITComponentsOptimization {

	private static final long serialVersionUID = -716772054685393741L;
	private int delayOffset;
	private Date fromTime;
	private Date toTime;
	private double percentOfWorkloadDelayed;
	
	public static final String ACTION_TYPE = "Shift delay-tolerant workload (KWh)";

	public ShiftDelayTolerantWorkload() {
		super(ACTION_TYPE);
	}

	public ShiftDelayTolerantWorkload(long id, double estimatedEnergySaving, Date startTime, Date endTime,
			int delayOffset, Date fromTime, Date toTime, double percentOfWorkloadDelayed) {
		super(id, ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
		this.delayOffset = delayOffset;
		this.fromTime = fromTime;
		this.toTime = toTime;
		this.percentOfWorkloadDelayed = percentOfWorkloadDelayed;
	}

	@Override
	public String toString() {
		String toString = "Shift Delay Tolerant Workload" + getId() + "\n";
		toString += super.toString();
		toString += "Percent Of Workload Delayed : " + percentOfWorkloadDelayed + "\n";
		toString += "FromTime : " + fromTime + "\n";
		toString += "ToTime : " + toTime;
		return toString;
	}

	/**
	 * @return the delayOffset
	 */
	public int getDelayOffset() {
		return delayOffset;
	}

	/**
	 * @param delayOffset
	 *            the delayOffset to set
	 */
	public void setDelayOffset(int delayOffset) {
		this.delayOffset = delayOffset;
	}

	/**
	 * @return the fromTime
	 */
	public Date getFromTime() {
		return fromTime;
	}

	/**
	 * @param fromTime
	 *            the fromTime to set
	 */
	public void setFromTime(Date fromTime) {
		this.fromTime = fromTime;
	}

	/**
	 * @return the toTime
	 */
	public Date getToTime() {
		return toTime;
	}

	/**
	 * @param toTime
	 *            the toTime to set
	 */
	public void setToTime(Date toTime) {
		this.toTime = toTime;
	}

	/**
	 * @return the percentOfWorkloadDelayed
	 */
	public double getPercentOfWorkloadDelayed() {
		return percentOfWorkloadDelayed;
	}

	/**
	 * @param percentOfWorkloadDelayed
	 *            the percentOfWorkloadDelayed to set
	 */
	public void setPercentOfWorkloadDelayed(double percentOfWorkloadDelayed) {
		this.percentOfWorkloadDelayed = percentOfWorkloadDelayed;
	}
}
