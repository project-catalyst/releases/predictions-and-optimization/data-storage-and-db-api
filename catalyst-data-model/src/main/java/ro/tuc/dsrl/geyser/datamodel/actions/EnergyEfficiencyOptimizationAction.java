package ro.tuc.dsrl.geyser.datamodel.actions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.annotations.InstanceId;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = MarketplaceAction.class),
		@JsonSubTypes.Type(value = EnergyConsumptionOptimization.class) })
public class EnergyEfficiencyOptimizationAction implements Serializable {

	private static final long serialVersionUID = -1600543676128369550L;
	@InstanceId
	private long id;
	private Date startTime;
	private Date endTime;
	private double amountOfEnergy;
	private double amountThermal;

	private String description;

	public EnergyEfficiencyOptimizationAction() {
	}

	public EnergyEfficiencyOptimizationAction(String description) {
		this.description = description;
	}

	public EnergyEfficiencyOptimizationAction(long id, String description, Date startTime, Date endTime,
			double amountOfEnergy) {
		this.description = description;
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.amountOfEnergy = amountOfEnergy;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		String toString = "StartTime: " + startTime + "\n";
		toString += "EndTime: " + endTime + "\n";
		return toString;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public double getAmountOfEnergy() {
		return amountOfEnergy;
	}

	public void setAmountOfEnergy(double amountOfEnergy) {
		this.amountOfEnergy = amountOfEnergy;
	}

	public double getAmountThermal() {
		return amountThermal;
	}

	public void setAmountThermal(double amountThermal) {
		this.amountThermal = amountThermal;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EnergyEfficiencyOptimizationAction that = (EnergyEfficiencyOptimizationAction) o;
		return Double.compare(that.amountOfEnergy, amountOfEnergy) == 0 &&
				startTime.equals(that.startTime) &&
				endTime.equals(that.endTime) &&
				description.equals(that.description);
	}

	@Override
	public int hashCode() {
		return Objects.hash(startTime, endTime, amountOfEnergy, description);
	}
}
