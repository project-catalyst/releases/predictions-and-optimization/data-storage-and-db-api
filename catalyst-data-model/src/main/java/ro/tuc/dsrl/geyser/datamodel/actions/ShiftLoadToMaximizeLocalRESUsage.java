package ro.tuc.dsrl.geyser.datamodel.actions;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ShiftLoadToMaximizeLocalRESUsage extends RESUsageOptimization {

	private static final long serialVersionUID = -2483686951785734051L;
	private DateTime shiftingTimeInterval;
	private List<DateTimeValue> predicatedResAmount;

	private static final String ACTION_TYPE = "Shift load to use local RES";

	public ShiftLoadToMaximizeLocalRESUsage() {
		super(ACTION_TYPE);
	}

	public ShiftLoadToMaximizeLocalRESUsage(long id, List<EnergyConsumptionOptimization> actions,
			double estimatedLoadShifted, DateTime shiftingTimeInterval, Date start, Date end) {
		super(id, ACTION_TYPE, actions, estimatedLoadShifted, start, end);
		this.shiftingTimeInterval = shiftingTimeInterval;
	}

	public DateTime getShiftingTimeInterval() {
		return shiftingTimeInterval;
	}

	public void setShiftingTimeInterval(DateTime shiftingTimeInterval) {
		this.shiftingTimeInterval = shiftingTimeInterval;
	}

	/**
	 * @return the predicatedResAmount
	 */
	public List<DateTimeValue> getPredicatedResAmount() {
		return predicatedResAmount;
	}

	/**
	 * @param predicatedResAmount
	 *            the predicatedResAmount to set
	 */
	public void setPredicatedResAmount(List<DateTimeValue> predicatedResAmount) {
		this.predicatedResAmount = predicatedResAmount;
	}

}
