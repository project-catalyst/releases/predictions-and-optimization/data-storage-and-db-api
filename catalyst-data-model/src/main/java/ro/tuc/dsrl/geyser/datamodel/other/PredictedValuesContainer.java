package ro.tuc.dsrl.geyser.datamodel.other;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PredictedValuesContainer {

	private String component;
	private String kind;
	private String timeframe;
	private Date start;
	private Date end;
	private List<EnergyPointValue> energyValues;
	private double confidenceLevel;

	public PredictedValuesContainer() {
		energyValues = new ArrayList<EnergyPointValue>();
	}
	public PredictedValuesContainer(String component, String type,
			String timeframe, Date start, Date end,
			List<EnergyPointValue> values) {
		this.component = component;
		this.kind = type;
		this.timeframe = timeframe;
		this.start = start;
		this.end = end;
		this.energyValues = values;
	}

	public PredictedValuesContainer(String component, String type,
                                    String timeframe, Date start, Date end,
                                    List<EnergyPointValue> values, double confidenceLevel) {
		this.component = component;
		this.kind = type;
		this.timeframe = timeframe;
		this.start = start;
		this.end = end;
		this.energyValues = values;
		this.confidenceLevel = confidenceLevel;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getTimeframe() {
		return timeframe;
	}

	public void setTimeframe(String timeframe) {
		this.timeframe = timeframe;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public List<EnergyPointValue> getEnergyValues() {
		return energyValues;
	}

	public void setEnergyValues(List<EnergyPointValue> values) {
		this.energyValues = values;
	}

	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}


}
