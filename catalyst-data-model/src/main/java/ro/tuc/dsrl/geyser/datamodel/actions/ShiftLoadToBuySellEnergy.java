package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ShiftLoadToBuySellEnergy extends SmartGridInteractionOptimization {

	private static final long serialVersionUID = 6476041560087107513L;
	private List<DateTimeValue> predictedEnergyPrice;

	private static final String ACTION_TYPE = "shift load to trade energy";

	public ShiftLoadToBuySellEnergy() {
		super(ACTION_TYPE);
	}

	public ShiftLoadToBuySellEnergy(long id, List<EnergyConsumptionOptimization> actions, double estimatedLoadShifted,
                                    Date start, Date end) {
		super(id,ACTION_TYPE, actions, estimatedLoadShifted, start, end);
	}

	/**
	 * @return the predictedEnergyPrice
	 */
	public List<DateTimeValue> getPredictedEnergyPrice() {
		return predictedEnergyPrice;
	}

	/**
	 * @param predictedEnergyPrice
	 *            the predictedEnergyPrice to set
	 */
	public void setPredictedEnergyPrice(List<DateTimeValue> predictedEnergyPrice) {
		this.predictedEnergyPrice = predictedEnergyPrice;
	}

}
