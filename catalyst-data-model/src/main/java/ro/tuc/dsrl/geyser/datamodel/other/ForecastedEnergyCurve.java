package ro.tuc.dsrl.geyser.datamodel.other;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ForecastedEnergyCurve extends EnergyCurve {
	private double confidenceLevel;

	public ForecastedEnergyCurve() {
	}

	public ForecastedEnergyCurve(double confidenceLevel, List<EnergyPoint> curve) {
		super(curve);
		this.confidenceLevel = confidenceLevel;
	}

	/**
	 * @return the confidenceLevel
	 */
	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	/**
	 * @param confidenceLevel
	 *            the confidenceLevel to set
	 */
	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}
}
