package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ShiftLoadToMaximizeUsageOfSmartGridRES extends SmartGridInteractionOptimization {

	private static final long serialVersionUID = 6551723221828164795L;
	private List<DateTimeValue> predictedResAmountGrid;

	private static final String ACTION_TYPE = "Shift load to use smart grid RES";

	public ShiftLoadToMaximizeUsageOfSmartGridRES() {
		super(ACTION_TYPE);
	}

	public ShiftLoadToMaximizeUsageOfSmartGridRES(long id, List<EnergyConsumptionOptimization> actions,
			double estimatedLoadShifted, Date start, Date end) {
		super(id,ACTION_TYPE, actions, estimatedLoadShifted,start,end);
	}

	/**
	 * @return the predictedResAmountGrid
	 */
	public List<DateTimeValue> getPredictedResAmountGrid() {
		return predictedResAmountGrid;
	}

	/**
	 * @param predictedResAmountGrid
	 *            the predictedResAmountGrid to set
	 */
	public void setPredictedResAmountGrid(List<DateTimeValue> predictedResAmountGrid) {
		this.predictedResAmountGrid = predictedResAmountGrid;
	}

}
