package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class SmartGridInteractionOptimization extends EnergyEfficiencyOptimizationAction {

	private static final long serialVersionUID = -5291971648360202520L;
	private List<EnergyConsumptionOptimization> actions;

	public SmartGridInteractionOptimization(String description) {
		super(description);
	}

	public SmartGridInteractionOptimization(long id, String description, List<EnergyConsumptionOptimization> actions,
			double estimatedLoadShifted, Date start, Date end) {
		super(id,description, start, end, estimatedLoadShifted);
		this.actions = actions;
	}

	public List<EnergyConsumptionOptimization> getActions() {
		return actions;
	}

	public void setActions(List<EnergyConsumptionOptimization> actions) {
		this.actions = actions;
	}

}
