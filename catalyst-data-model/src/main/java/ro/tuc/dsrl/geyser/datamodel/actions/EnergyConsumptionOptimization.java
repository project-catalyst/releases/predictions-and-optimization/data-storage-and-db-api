package ro.tuc.dsrl.geyser.datamodel.actions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = EnergyStorageOptimization.class),
		@JsonSubTypes.Type(value = ITComponentsOptimization.class),
		@JsonSubTypes.Type(value = NonITComponentsOptimization.class) })
public class EnergyConsumptionOptimization extends EnergyEfficiencyOptimizationAction {

	private static final long serialVersionUID = -7789372105371215944L;

	public EnergyConsumptionOptimization() {
	}

	public EnergyConsumptionOptimization(String description) {
		super(description);
	}

	public EnergyConsumptionOptimization(long id, String description, double estimatedEnergySaving, Date startTime,
			Date endTime) {
		super(id, description, startTime, endTime, estimatedEnergySaving);
	}

	@Override
	public String toString() {
		String toString = super.toString();
		toString += "Estimated Energy Saving: " + super.getAmountOfEnergy() + "\n";
		return toString;
	}

}
