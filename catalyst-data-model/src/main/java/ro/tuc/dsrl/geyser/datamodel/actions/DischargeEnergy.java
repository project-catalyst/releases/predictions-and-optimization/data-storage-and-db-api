package ro.tuc.dsrl.geyser.datamodel.actions;



import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class DischargeEnergy extends EnergyStorageOptimization {

	private static final long serialVersionUID = -4234761016344417264L;
	private static final String ACTION_TYPE = "Discharge energy (KWh)";
	
	public DischargeEnergy() {
		super(ACTION_TYPE);
	}

	public DischargeEnergy(long id, double estimatedEnergySaving, Date startTime, Date endTime, double amountOfEnergy,
			EnergyStorageComponent energyStorageComponent) {
		super(id, ACTION_TYPE, estimatedEnergySaving, startTime, endTime, amountOfEnergy, energyStorageComponent);
	}

	@Override
	public String toString() {
		String toString = "Discharge Energy id: " + getId() + "\n";
		toString += super.toString();
		return toString;
	}
}
