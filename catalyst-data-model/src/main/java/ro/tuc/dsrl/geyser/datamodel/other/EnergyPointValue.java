package ro.tuc.dsrl.geyser.datamodel.other;

import java.util.Date;

public class EnergyPointValue {
	private double energyValue;
	private Date timeStamp;

	public EnergyPointValue() {
	}

	public EnergyPointValue(double value, Date timeStamp) {
		this.energyValue = value;
		this.timeStamp = timeStamp;
	}

	public double getEnergyValue() {
		return energyValue;
	}

	public void setEnergyValue(double energyValue) {
		this.energyValue = energyValue;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

}
