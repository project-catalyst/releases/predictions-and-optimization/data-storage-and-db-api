package ro.tuc.dsrl.geyser.datamodel.components;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.annotations.InstanceId;

import java.io.Serializable;
import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = EnergyGenerationComponent.class),
		@JsonSubTypes.Type(value = EnergyConsumptionComponent.class),
		@JsonSubTypes.Type(value = EnergyStorageComponent.class) })
public class Component implements Serializable {

	private static final long serialVersionUID = 1L;

	protected String deviceLabel;
	protected UUID deviceId;

//	@InstanceId
//	protected long id;

	public Component() {
	}

	public Component(String deviceLabel) {
		this.deviceLabel = deviceLabel;
	}


	public Component(UUID deviceId, String deviceLabel) {
		this.deviceId = deviceId;
		this.deviceLabel = deviceLabel;
	}

//	public Component(long id) {
//		this.id = id;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}

	public String getDeviceLabel() {
		return deviceLabel;
	}

	public void setDeviceLabel(String deviceLabel) {
		this.deviceLabel = deviceLabel;
	}

	public UUID getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(UUID deviceId) {
		this.deviceId = deviceId;
	}
}
