package ro.tuc.dsrl.geyser.datamodel.other;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OptimizationPlansContainer {
	private List<OptimizerPlan> optimizations;
	private String timeframe;
	private Date startDate;
	private Date endDate;
	private String dataCenterName;


	public OptimizationPlansContainer() {
		optimizations = new ArrayList<OptimizerPlan>();
	}

	public void addOptimizationDecisions(OptimizerPlan optimizationDecisions) {
		optimizations.add(optimizationDecisions);
	}

	public List<OptimizerPlan> getOptimizations() {
		return optimizations;
	}

	public void setOptimizations(List<OptimizerPlan> optimizations) {
		this.optimizations = optimizations;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getTimeframe() {
		return timeframe;
	}

	public void setTimeframe(String timeframe) {
		this.timeframe = timeframe;
	}

	public String getDataCenterName() {
		return dataCenterName;
	}

	public void setDataCenterName(String dataCenterName) {
		this.dataCenterName = dataCenterName;
	}
}
