package ro.tuc.dsrl.geyser.datamodel.components.production;


import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class Generator extends BrownEnergySource {
	private static final long serialVersionUID = 1L;
	@DynamicField
	private String mode;
	private List<LoadToFuelConsumption> loadToFuelConsumption;

	public Generator() {
		loadToFuelConsumption = new ArrayList<LoadToFuelConsumption>();
	}

	public Generator(double maxCapacity, double energyProduction, double efficiency, double emissionsCO2,
			double sourcePriority, String mode) {
		super(maxCapacity, energyProduction, efficiency, emissionsCO2, sourcePriority);
		this.mode = mode;
		loadToFuelConsumption = new ArrayList<LoadToFuelConsumption>();
	}


	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return the loadToFuelConsumption
	 */
	public List<LoadToFuelConsumption> getLoadToFuelConsumption() {
		return loadToFuelConsumption;
	}

	/**
	 * @param loadToFuelConsumption
	 *            the loadToFuelConsumption to set
	 */
	public void setLoadToFuelConsumption(List<LoadToFuelConsumption> loadToFuelConsumption) {
		this.loadToFuelConsumption = loadToFuelConsumption;
	}

	public void addLoadToFuelConsumption(LoadToFuelConsumption cons) {
		loadToFuelConsumption.add(cons);
	}
}
