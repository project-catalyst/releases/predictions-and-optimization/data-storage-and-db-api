package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = CPU.class), @JsonSubTypes.Type(value = HDD.class),
		@JsonSubTypes.Type(value = RAM.class), @JsonSubTypes.Type(value = NetworkActiveEquipment.class) })
public class IndividualComponent extends ITComponent {

	private static final long serialVersionUID = 5229713029780800990L;

	public IndividualComponent() {
	}

	public IndividualComponent(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit,
			double lowMoistureLimit, double highMoistureLimit, double pIdle, double pMax,
			double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				energyConsumption);

	}
}
