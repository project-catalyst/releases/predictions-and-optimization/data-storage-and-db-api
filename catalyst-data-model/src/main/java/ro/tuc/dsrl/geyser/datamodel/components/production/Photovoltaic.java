package ro.tuc.dsrl.geyser.datamodel.components.production;


import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class Photovoltaic extends GreenEnergySource {
	private static final long serialVersionUID = 374412469925880785L;
	private double totalSolarPanelArea;
	private double solarPanelYield;
	@DynamicField
	private double averageIrradiationOnTiltedPanels;
	private double performanceRatio;

	public Photovoltaic() {
		
	}


	@Override
	public double estimateEnergyProduction() {
		// E = A * r * H * PR
		return totalSolarPanelArea * solarPanelYield * averageIrradiationOnTiltedPanels * performanceRatio;
	}

	/**
	 * @return the totalSolarPanelArea
	 */
	public double getTotalSolarPanelArea() {
		return totalSolarPanelArea;
	}

	/**
	 * @param totalSolarPanelArea
	 *            the totalSolarPanelArea to set
	 */
	public void setTotalSolarPanelArea(double totalSolarPanelArea) {
		this.totalSolarPanelArea = totalSolarPanelArea;
	}

	/**
	 * @return the solarPanelYield
	 */
	public double getSolarPanelYield() {
		return solarPanelYield;
	}

	/**
	 * @param solarPanelYield
	 *            the solarPanelYield to set
	 */
	public void setSolarPanelYield(double solarPanelYield) {
		this.solarPanelYield = solarPanelYield;
	}

	/**
	 * @return the averageIrradiationOnTiltedPanels
	 */
	public double getAverageIrradiationOnTiltedPanels() {
		return averageIrradiationOnTiltedPanels;
	}

	/**
	 * @param averageIrradiationOnTiltedPanels
	 *            the averageIrradiationOnTiltedPanels to set
	 */
	public void setAverageIrradiationOnTiltedPanels(double averageIrradiationOnTiltedPanels) {
		this.averageIrradiationOnTiltedPanels = averageIrradiationOnTiltedPanels;
	}

	/**
	 * @return the performanceRatio
	 */
	public double getPerformanceRatio() {
		return performanceRatio;
	}

	/**
	 * @param performanceRatio
	 *            the performanceRatio to set
	 */
	public void setPerformanceRatio(double performanceRatio) {
		this.performanceRatio = performanceRatio;
	}
}
