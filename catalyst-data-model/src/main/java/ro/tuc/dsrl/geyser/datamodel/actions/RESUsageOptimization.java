package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;
import java.util.List;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class RESUsageOptimization extends EnergyEfficiencyOptimizationAction {

	private static final long serialVersionUID = 6860300089929456127L;
	private List<EnergyConsumptionOptimization> energyConsumptionOptimizationActionsRESUO;


	public RESUsageOptimization(String description) {
		super(description);
	}

	public RESUsageOptimization(long id, String description, List<EnergyConsumptionOptimization> actions, double estimatedLoadShifted, Date start, Date end) {
		super(id,description, start, end, estimatedLoadShifted);
		this.energyConsumptionOptimizationActionsRESUO = actions;
	}

	/**
	 * @return the energyConsumptionOptimizationActionsRESUO
	 */
	public List<EnergyConsumptionOptimization> getEnergyConsumptionOptimizationActionsRESUO() {
		return energyConsumptionOptimizationActionsRESUO;
	}

	/**
	 * @param energyConsumptionOptimizationActionsRESUO
	 *            the energyConsumptionOptimizationActionsRESUO to set
	 */
	public void setEnergyConsumptionOptimizationActionsRESUO(
			List<EnergyConsumptionOptimization> energyConsumptionOptimizationActionsRESUO) {
		this.energyConsumptionOptimizationActionsRESUO = energyConsumptionOptimizationActionsRESUO;
	}

}
