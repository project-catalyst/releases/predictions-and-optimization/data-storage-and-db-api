package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class OfficeBuilding extends NonITComponent {

	private static final long serialVersionUID = 1L;
	private int personnelNumber;
	private double totalSurface;
	private double ambientalTemperature;
	private double ambientalLinghting;
	private List<ComputedHeat> computedHeat;

	public OfficeBuilding(String deviceLabel, int personnelNumber, double totalSurface, double ambientalTemperature,
			double ambientalLinghting, double energyConsumption) {
		super(deviceLabel, energyConsumption);
		this.personnelNumber = personnelNumber;
		this.totalSurface = totalSurface;
		this.ambientalTemperature = ambientalTemperature;
		this.ambientalLinghting = ambientalLinghting;
	}

	/**
	 * @return the personnelNumber
	 */
	public int getPersonnelNumber() {
		return personnelNumber;
	}

	/**
	 * @param personnelNumber
	 *            the personnelNumber to set
	 */
	public void setPersonnelNumber(int personnelNumber) {
		this.personnelNumber = personnelNumber;
	}

	/**
	 * @return the totalSurface
	 */
	public double getTotalSurface() {
		return totalSurface;
	}

	/**
	 * @param totalSurface
	 *            the totalSurface to set
	 */
	public void setTotalSurface(double totalSurface) {
		this.totalSurface = totalSurface;
	}

	/**
	 * @return the ambientalTemperature
	 */
	public double getAmbientalTemperature() {
		return ambientalTemperature;
	}

	/**
	 * @param ambientalTemperature
	 *            the ambientalTemperature to set
	 */
	public void setAmbientalTemperature(double ambientalTemperature) {
		this.ambientalTemperature = ambientalTemperature;
	}

	/**
	 * @return the ambientalLinghting
	 */
	public double getAmbientalLinghting() {
		return ambientalLinghting;
	}

	/**
	 * @param ambientalLinghting
	 *            the ambientalLinghting to set
	 */
	public void setAmbientalLinghting(double ambientalLinghting) {
		this.ambientalLinghting = ambientalLinghting;
	}

	/**
	 * @return the computedHeat
	 */
	public List<ComputedHeat> getComputedHeat() {
		return computedHeat;
	}

	/**
	 * @param computedHeat
	 *            the computedHeat to set
	 */
	public void setComputedHeat(List<ComputedHeat> computedHeat) {
		this.computedHeat = computedHeat;
	}
}
