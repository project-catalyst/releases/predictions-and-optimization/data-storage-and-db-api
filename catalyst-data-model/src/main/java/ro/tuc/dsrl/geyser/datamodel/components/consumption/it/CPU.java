package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.annotations.ObjectProperty;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class CPU extends IndividualComponent {

	private static final long serialVersionUID = -7055523882696759941L;
	private double clockSpeedGHZ;
	private int noCores;
	@DynamicField
	private double utilization;
	@JsonIgnore
	@ObjectProperty(value = "components")
	private Server server;

	public CPU() {
	}

	public CPU(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double clockSpeedGHZ, int noCores, double utilization,
			double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				energyConsumption);
		this.clockSpeedGHZ = clockSpeedGHZ;
		this.noCores = noCores;
		this.utilization = utilization;

	}

	/**
	 * @param deviceLabel
	 * @param utilization
	 * @return
	 */
	public static CPU createDynamicInstance(String deviceLabel, double utilization) {
		return new CPU(deviceLabel, -1, -1, -1, -1, -1, -1, -1, -1, utilization, 0);
	}

	/**
	 * @return the clockSpeedGHZ
	 */
	public double getClockSpeedGHZ() {
		return clockSpeedGHZ;
	}

	/**
	 * @param clockSpeedGHZ
	 *            the clockSpeedGHZ to set
	 */
	public void setClockSpeedGHZ(double clockSpeedGHZ) {
		this.clockSpeedGHZ = clockSpeedGHZ;
	}

	/**
	 * @return the noCores
	 */
	public int getNoCores() {
		return noCores;
	}

	/**
	 * @param noCores
	 *            the noCores to set
	 */
	public void setNoCores(int noCores) {
		this.noCores = noCores;
	}

	/**
	 * @return the utilization
	 */
	public double getUtilization() {
		return utilization;
	}

	/**
	 * @param utilization the utilization to set
	 */
	public void setUtilization(double utilization) {
		this.utilization = utilization;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

}
