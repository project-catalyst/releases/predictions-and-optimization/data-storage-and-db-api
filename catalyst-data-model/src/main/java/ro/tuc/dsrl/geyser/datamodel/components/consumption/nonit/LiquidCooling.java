package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class LiquidCooling extends CoolingSystem {

	private static final long serialVersionUID = 1L;

	public LiquidCooling() {
	}

	public LiquidCooling(String deviceLabel, double coolingCapacityKWh, double minCoolingLoadKWh, double maxCoolingLoadKWh,
			double inputAirTemperatureCS, double outputAirTemperatureCS, double copCoeff1, double copCoeff2,
			double flow, double specificHeat, double energyConsumption) {
		super(deviceLabel, coolingCapacityKWh, minCoolingLoadKWh, maxCoolingLoadKWh, inputAirTemperatureCS,
				outputAirTemperatureCS, copCoeff1, copCoeff2, flow, specificHeat, energyConsumption);
	}

	public static LiquidCooling createDynamicInstance(String deviceLabel, Double flow, Double inputAirTemperature,
                                                      Double outputAirTemperature, Double energyConsumption) {
		return new LiquidCooling(deviceLabel, -1, -1, -1, inputAirTemperature, outputAirTemperature, -1, -1,  flow, -1,
				energyConsumption);
	}

}
