package ro.tuc.dsrl.geyser.datamodel.indicators;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class APCren extends LoadShifting {
	
	public APCren() {
		super();
	}
	
	public APCren(long id, double indicatorValue, double optimalValue, double threshold) {
		super(id, indicatorValue, optimalValue, threshold);
	}
	
	public static APCren createDynamicInstance(long id, double indicatorValue) {
		return new APCren(id, indicatorValue, -1, -1);
	}
}
