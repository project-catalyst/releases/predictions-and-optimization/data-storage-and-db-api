package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;


import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class AirCooling extends CoolingSystem {

	private static final long serialVersionUID = 1L;

	@DynamicField
	private double fanSpeedRate;
	@DynamicField
	private double damperOpenRate;
	@DynamicField
	private double crahExitTemperature;

	public AirCooling() {
	}

	public AirCooling(String deviceLabel, double coolingCapacityKWh, double minCoolingLoadKWh, double maxCoolingLoadKWh,
			double inputAirTemperatureCS, double outputAirTemperatureCS, double copCoeff1, double copCoeff2
			, double flow, double specificHeat, double fanSpeedRate, double damperOpenRate,
			double crahExitTemperature, double energyConsumption) {
		super(deviceLabel, coolingCapacityKWh, minCoolingLoadKWh, maxCoolingLoadKWh, inputAirTemperatureCS,
				outputAirTemperatureCS, copCoeff1, copCoeff2, flow, specificHeat, energyConsumption);
		this.fanSpeedRate = fanSpeedRate;
		this.damperOpenRate = damperOpenRate;
		this.crahExitTemperature = crahExitTemperature;
	}

	public static AirCooling createDynamicInstance(String deviceLabel, Double flow, Double inputAirTemperature,
                                                   Double outputAirTemperature, Double energyConsumption, Double fanSpeedRate, Double damperOpenRate,
                                                   Double crahExitTemperature) {
		return new AirCooling(deviceLabel, -1, -1, -1, inputAirTemperature, outputAirTemperature, -1, -1,  flow, -1,
				fanSpeedRate, damperOpenRate, crahExitTemperature, energyConsumption);
	}

	/**
	 * @return the fanSpeedRate
	 */
	public double getFanSpeedRate() {
		return fanSpeedRate;
	}

	/**
	 * @param fanSpeedRate the fanSpeedRate to set
	 */
	public void setFanSpeedRate(double fanSpeedRate) {
		this.fanSpeedRate = fanSpeedRate;
	}

	/**
	 * @return the damperOpenRate
	 */
	public double getDamperOpenRate() {
		return damperOpenRate;
	}

	/**
	 * @param damperOpenRate the damperOpenRate to set
	 */
	public void setDamperOpenRate(double damperOpenRate) {
		this.damperOpenRate = damperOpenRate;
	}

	/**
	 * @return the crahExitTemperature
	 */
	public double getCrahExitTemperature() {
		return crahExitTemperature;
	}

	/**
	 * @param crahExitTemperature the crahExitTemperature to set
	 */
	public void setCrahExitTemperature(double crahExitTemperature) {
		this.crahExitTemperature = crahExitTemperature;
	}

}
