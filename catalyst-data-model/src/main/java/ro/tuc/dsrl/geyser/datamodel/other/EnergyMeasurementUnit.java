package ro.tuc.dsrl.geyser.datamodel.other;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 6, 2015
 * @Description:
 *
 */
public enum EnergyMeasurementUnit {
	KWH(1.0),
	KW30MIN(0.5), 
    KW5MIN(1.0 / 12.0);

	private double value;

	private EnergyMeasurementUnit(double energyMeasurementUnit) {
		this.value = energyMeasurementUnit;
	}

	public double value() {
		return value;
	}
}
