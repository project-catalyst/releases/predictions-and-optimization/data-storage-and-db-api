package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ServerRack extends CombinedComponent {
	private static final long serialVersionUID = -4754201927290913004L;
	private double iTRackDensityKWPerRack;
	private int noServers;
	private List<CombinedComponent> combinedComponents;

	public ServerRack() {
	}

	public ServerRack(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double iTRackDensityKWPerRack, int noServers,
			double inputAirTemperatureCC, double outputAirTemperatureCC, double airFlowCC, double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				inputAirTemperatureCC, outputAirTemperatureCC, airFlowCC, energyConsumption);
		this.iTRackDensityKWPerRack = iTRackDensityKWPerRack;
		this.noServers = noServers;
	}

	/**
	 * @return the iTRackDensityKWPerRack
	 */
	public double getiTRackDensityKWPerRack() {
		return iTRackDensityKWPerRack;
	}

	/**
	 * @param iTRackDensityKWPerRack
	 *            the iTRackDensityKWPerRack to set
	 */
	public void setiTRackDensityKWPerRack(double iTRackDensityKWPerRack) {
		this.iTRackDensityKWPerRack = iTRackDensityKWPerRack;
	}

	/**
	 * @return the noServers
	 */
	public int getNoServers() {
		return noServers;
	}

	/**
	 * @param noServers
	 *            the noServers to set
	 */
	public void setNoServers(int noServers) {
		this.noServers = noServers;
	}

	/**
	 * @return the combComponents
	 */
	public List<CombinedComponent> getCombComponents() {
		return combinedComponents;
	}

	/**
	 * @param combComponents
	 *            the combComponents to set
	 */
	public void setCombComponents(List<CombinedComponent> combComponents) {
		this.combinedComponents = combComponents;
	}

}
