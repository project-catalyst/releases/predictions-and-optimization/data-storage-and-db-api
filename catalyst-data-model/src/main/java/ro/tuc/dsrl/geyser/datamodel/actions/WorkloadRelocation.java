package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class WorkloadRelocation extends ITComponentsOptimization {

	private static final long serialVersionUID = 4063489509599708640L;
	private double workloadAmount;
	private double percentOfWorkloadReallocated;
	private String destinationDC;
	
	public static final String ACTION_TYPE = "Relocate Load (KWh)";

	public WorkloadRelocation() {
		super(ACTION_TYPE);
	}
	
	public WorkloadRelocation(long id, double estimatedEnergySaving, Date startTime, Date endTime,double percentOfWorkloadReallocated,
			double workloadAmount, String destinationDC) {
		super(id, ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
		this.percentOfWorkloadReallocated = percentOfWorkloadReallocated;
		this.workloadAmount = workloadAmount;
		this.destinationDC = destinationDC;
	}

	public double getWorkloadAmount() {
		return workloadAmount;
	}

	public void setWorkloadAmount(double workloadAmount) {
		this.workloadAmount = workloadAmount;
	}

	public String getDestinationDC() {
		return destinationDC;
	}

	public void setDestinationDC(String destinationDC) {
		this.destinationDC = destinationDC;
	}

	public double getPercentOfWorkloadReallocated() {
		return percentOfWorkloadReallocated;
	}

	public void setPercentOfWorkloadReallocated(double percentOfWorkloadReallocated) {
		this.percentOfWorkloadReallocated = percentOfWorkloadReallocated;
	}

}
