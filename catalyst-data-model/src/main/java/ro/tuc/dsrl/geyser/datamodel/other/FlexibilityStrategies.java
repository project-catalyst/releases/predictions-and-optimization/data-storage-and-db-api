//package ro.tuc.dsrl.geyser.datamodel.other;
//
///**
// * @Author: Technical University of Cluj-Napoca, Romania
// *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
// * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
// * @Module: data-model
// * @Since: Feb 26, 2015
// * @Description:
// *
// */
//public class FlexibilityStrategies {
//	private double we;
//	private double wl;
//	private double wf;
//	private double wt;
//	private boolean reallocActive;
//
//
//	public FlexibilityStrategies(){}
//
//	public FlexibilityStrategies(double we, double wl, double wf, double wt, boolean reallocActive) {
//		this.we = we;
//		this.wl = wl;
//		this.wf = wf;
//		this.wt = wt;
//		this.reallocActive = reallocActive;
//	}
//
//	public double getWe() {
//		return we;
//	}
//
//	public void setWe(double we) {
//		this.we = we;
//	}
//
//	public double getWl() {
//		return wl;
//	}
//
//	public void setWl(double wl) {
//		this.wl = wl;
//	}
//
//	public double getWf() {
//		return wf;
//	}
//
//	public void setWf(double wf) {
//		this.wf = wf;
//	}
//
//	public double getWt() {
//		return wt;
//	}
//
//	public void setWt(double wt) {
//		this.wt = wt;
//	}
//
//	public boolean isReallocActive() {
//		return reallocActive;
//	}
//
//	public void setReallocActive(boolean reallocActive) {
//		this.reallocActive = reallocActive;
//	}
//}
