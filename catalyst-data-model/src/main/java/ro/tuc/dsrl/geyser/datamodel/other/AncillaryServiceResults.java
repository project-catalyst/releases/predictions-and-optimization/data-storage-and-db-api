package ro.tuc.dsrl.geyser.datamodel.other;

/**
 * Created by doru on 12/11/2015.
 */
public class AncillaryServiceResults {
    private double[] drSignal;
    private double[] dcBaseline;
    private double[] adaptedProfile;

    public AncillaryServiceResults() {
    }

    public AncillaryServiceResults(double[] drSignal, double[] dcBaseline, double[] adaptedProfile) {
        this.drSignal = drSignal;
        this.dcBaseline = dcBaseline;
        this.adaptedProfile = adaptedProfile;
    }

    public double[] getDrSignal() {
        return drSignal;
    }

    public void setDrSignal(double[] drSignal) {
        this.drSignal = drSignal;
    }

    public double[] getDcBaseline() {
        return dcBaseline;
    }

    public void setDcBaseline(double[] dcBaseline) {
        this.dcBaseline = dcBaseline;
    }

    public double[] getAdaptedProfile() {
        return adaptedProfile;
    }

    public void setAdaptedProfile(double[] adaptedProfile) {
        this.adaptedProfile = adaptedProfile;
    }
}
