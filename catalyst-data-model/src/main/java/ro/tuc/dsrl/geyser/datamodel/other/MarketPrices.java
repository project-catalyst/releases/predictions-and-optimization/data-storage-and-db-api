package ro.tuc.dsrl.geyser.datamodel.other;

import ro.tuc.dsrl.catalyst.model.dto.ForecastDayAheadDTO;


public class MarketPrices {
    private ForecastDayAheadDTO energyPrices;
    private ForecastDayAheadDTO workloadPrices;
    private ForecastDayAheadDTO thermalPrices;
    private ForecastDayAheadDTO drIncentives;

    public MarketPrices(){}

    public MarketPrices(ForecastDayAheadDTO energyPrices, ForecastDayAheadDTO workloadPrices, ForecastDayAheadDTO thermalPrices, ForecastDayAheadDTO drIncentives) {
        this.energyPrices = energyPrices;
        this.workloadPrices = workloadPrices;
        this.thermalPrices = thermalPrices;
        this.drIncentives = drIncentives;
    }

    public ForecastDayAheadDTO getEnergyPrices() {
        return energyPrices;
    }

    public void setEnergyPrices(ForecastDayAheadDTO energyPrices) {
        this.energyPrices = energyPrices;
    }

    public ForecastDayAheadDTO getWorkloadPrices() {
        return workloadPrices;
    }

    public void setWorkloadPrices(ForecastDayAheadDTO workloadPrices) {
        this.workloadPrices = workloadPrices;
    }

    public ForecastDayAheadDTO getThermalPrices() {
        return thermalPrices;
    }

    public void setThermalPrices(ForecastDayAheadDTO thermalPrices) {
        this.thermalPrices = thermalPrices;
    }

    public ForecastDayAheadDTO getDrIncentives() {
        return drIncentives;
    }

    public void setDrIncentives(ForecastDayAheadDTO drIncentives) {
        this.drIncentives = drIncentives;
    }
}
