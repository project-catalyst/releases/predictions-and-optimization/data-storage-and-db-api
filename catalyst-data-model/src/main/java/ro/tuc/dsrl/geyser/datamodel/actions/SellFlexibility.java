package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

public class SellFlexibility extends MarketplaceAction {
    private static final long serialVersionUID = 7531642642978732379L;
    public static final String ACTION_TYPE = "Sell Flexibility (KWh)";

    public SellFlexibility() {
        super(ACTION_TYPE);
    }

    public SellFlexibility(long id, Date startTime, Date endTime, double energyAmount, double price) {
        super(id,ACTION_TYPE, startTime, endTime, energyAmount, price);
    }

    @Override
    public String toString() {
        String toString = "SellFlexibility id : " + getId() + "\n";
        toString += super.toString();
        return toString;
    }

}
