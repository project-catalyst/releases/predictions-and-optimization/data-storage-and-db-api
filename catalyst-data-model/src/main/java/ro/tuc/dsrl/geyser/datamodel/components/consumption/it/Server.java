package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class Server extends CombinedComponent {

	private static final long serialVersionUID = 5883122870286286283L;
	private String serialNumber;
	private List<VirtualMachine> virtualMachines;

	public Server() {
	}

	public Server(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double inputAirTemperatureCC,
			double outputAirTemperatureCC, double airFlowCC, String serialNumber, double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				inputAirTemperatureCC, outputAirTemperatureCC, airFlowCC, energyConsumption);
		this.serialNumber = serialNumber;
	}

	/**
	 * @param deviceLabel
	 * @param energyConsumption
	 * @return
	 */
	public static Server createDynamicInstance(String deviceLabel, double energyConsumption) {
		return new Server(deviceLabel, -1, -1, -1, -1, -1, -1, -1, -1, -1, "", energyConsumption);
	}

	/**
	 * @return the virtualMachines
	 */
	public List<VirtualMachine> getVirtualMachines() {
		return virtualMachines;
	}

	/**
	 * @param virtualMachines
	 *            the virtualMachines to set
	 */
	public void setVirtualMachines(List<VirtualMachine> virtualMachines) {
		this.virtualMachines = virtualMachines;
	}

	/**
	 * @return the serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * @param serialNumber
	 *            the serialNumber to set
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

}
