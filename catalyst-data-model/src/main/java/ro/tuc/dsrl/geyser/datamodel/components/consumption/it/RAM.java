package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.annotations.ObjectProperty;


/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class RAM extends IndividualComponent {

	private static final long serialVersionUID = 3471281377043871325L;
	private double totalCapacityRAM;
	private double speedMHZ;
	@DynamicField
	private double usedCapacityRAM;
	@JsonIgnore
	@ObjectProperty(value = "components")
	private Server server;

	public RAM() {
	}

	public RAM(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double totalCapacity, double speedMHZ,
			double usedCapacity, double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				energyConsumption);
		this.totalCapacityRAM = totalCapacity;
		this.speedMHZ = speedMHZ;
		this.usedCapacityRAM = usedCapacity;
	}

	public static RAM createDynamicInstance(String deviceLabel, double usedCapacity) {
		return new RAM(deviceLabel, -1, -1, -1, -1, -1, -1, -1, -1, usedCapacity, 0);
	}

	/**
	 * @return the totalCapacity
	 */
	public double getTotalCapacity() {
		return totalCapacityRAM;
	}

	/**
	 * @param totalCapacity
	 *            the totalCapacity to set
	 */
	public void setTotalCapacity(double totalCapacity) {
		this.totalCapacityRAM = totalCapacity;
	}

	/**
	 * @return the speedMHZ
	 */
	public double getSpeedMHZ() {
		return speedMHZ;
	}

	/**
	 * @param speedMHZ
	 *            the speedMHZ to set
	 */
	public void setSpeedMHZ(double speedMHZ) {
		this.speedMHZ = speedMHZ;
	}

	/**
	 * @return the usedCapacity
	 */
	public double getUsedCapacity() {
		return usedCapacityRAM;
	}

	/**
	 * @param usedCapacity
	 *            the usedCapacity to set
	 */
	public void setUsedCapacity(double usedCapacity) {
		this.usedCapacityRAM = usedCapacity;
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

}
