package ro.tuc.dsrl.geyser.datamodel.components;

import ro.tuc.dsrl.geyser.datamodel.actions.EnergyConsumptionOptimization;
import ro.tuc.dsrl.geyser.datamodel.indicators.EnergyEfficiencyIndicators;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class DataCentre {
	private UUID idDataCentre;
	private String label;
	private int designITLoadDensityWm2;
	private float floorArea;
	private double energyConsumption;
	private List<Component> components;
	private List<EnergyConsumptionOptimization> actions;
	private List<EnergyEfficiencyIndicators> indicators;
	public DataCentre(){}

	public DataCentre(String label, double energyConsumption) {
		this.label = label;
		this.energyConsumption = energyConsumption;
		this.designITLoadDensityWm2 = -1;
		this.floorArea = -1;
		components = new ArrayList<>();
		actions = new ArrayList<>();
		indicators = new ArrayList<>();
	}

	public DataCentre(UUID id, String label, double energyConsumption) {
		this.idDataCentre = id;
		this.label = label;
		this.energyConsumption = energyConsumption;
		this.designITLoadDensityWm2 = -1;
		this.floorArea = -1;
		components = new ArrayList<>();
		actions = new ArrayList<>();
		indicators = new ArrayList<>();
	}
	/**
	 * @return the idDataCentre
	 */
	public UUID getIdDataCentre() {
		return idDataCentre;
	}

	/**
	 * @param idDataCentre the idDataCentre to set
	 */
	public void setIdDataCentre(UUID idDataCentre) {
		this.idDataCentre = idDataCentre;
	}

	/**
	 * @return the designITLoadDensityWm2
	 */
	public int getDesignITLoadDensityWm2() {
		return designITLoadDensityWm2;
	}

	/**
	 * @param designITLoadDensityWm2 the designITLoadDensityWm2 to set
	 */
	public void setDesignITLoadDensityWm2(int designITLoadDensityWm2) {
		this.designITLoadDensityWm2 = designITLoadDensityWm2;
	}

	/**
	 * @return the floorArea
	 */
	public float getFloorArea() {
		return floorArea;
	}

	/**
	 * @param floorArea the floorArea to set
	 */
	public void setFloorArea(float floorArea) {
		this.floorArea = floorArea;
	}

	/**
	 * @return the components
	 */
	public List<Component> getComponents() {
		return components;
	}

	/**
	 * @param components the components to set
	 */
	public void setComponents(List<Component> components) {
		this.components = components;
	}

	/**
	 * @return the actions
	 */
	public List<EnergyConsumptionOptimization> getActions() {
		return actions;
	}

	/**
	 * @param actions the actions to set
	 */
	public void setActions(List<EnergyConsumptionOptimization> actions) {
		this.actions = actions;
	}

	/**
	 * @return the indicators
	 */
	public List<EnergyEfficiencyIndicators> getIndicators() {
		return indicators;
	}

	/**
	 * @param indicators the indicators to set
	 */
	public void setIndicators(List<EnergyEfficiencyIndicators> indicators) {
		this.indicators = indicators;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getEnergyConsumption() {
		return energyConsumption;
	}

	public void setEnergyConsumption(double energyConsumption) {
		this.energyConsumption = energyConsumption;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		DataCentre that = (DataCentre) o;
		return designITLoadDensityWm2 == that.designITLoadDensityWm2 &&
				Float.compare(that.floorArea, floorArea) == 0 &&
				Double.compare(that.energyConsumption, energyConsumption) == 0 &&
				Objects.equals(idDataCentre, that.idDataCentre) &&
				Objects.equals(label, that.label) &&
				Objects.equals(components, that.components) &&
				Objects.equals(actions, that.actions) &&
				Objects.equals(indicators, that.indicators);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idDataCentre, label, designITLoadDensityWm2, floorArea, energyConsumption, components, actions, indicators);
	}

	@Override
	public String toString() {
		return "DataCentre{" +
				"idDataCentre=" + idDataCentre +
				", label='" + label + '\'' +
				", designITLoadDensityWm2=" + designITLoadDensityWm2 +
				", floorArea=" + floorArea +
				", energyConsumption=" + energyConsumption +
				", components=" + components +
				", actions=" + actions +
				", indicators=" + indicators +
				'}';
	}
}
