package ro.tuc.dsrl.geyser.datamodel.components.production;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;

import java.util.Objects;
import java.util.UUID;


/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class Battery extends EnergyStorageComponent {

	private static final long serialVersionUID = 1L;

	@DynamicField
	private String modeB;
	@DynamicField
	private String batteryRuntime;
	@DynamicField
	private double upsOutputFreq;
	@DynamicField
	private double bypassFreq;
	@DynamicField
	private double batteryVoltage;
	@DynamicField
	private double chargeCurrent;
	@DynamicField
	private double dischargeCurrent;
	@DynamicField
	private double rectifierVoltage;
	@DynamicField
	private double bypassVoltage;
	@DynamicField
	private double outputVoltage;
	@DynamicField
	private double outputCurrent;
	@DynamicField
	private double activeOutputPower;
	@DynamicField
	private double reactiveOutputPower;
	@DynamicField
	private double apparentOutputPower;
	@DynamicField
	private double outputPowerB;
	@DynamicField
	private double batteryTemperature;
	@DynamicField
	private double boosterTemperature;
	@DynamicField
	private double inverterTemperature;

	private double dod;

	private double esdFactor;

	public Battery() {
	}

	public Battery(UUID deviceId, String deviceLabel, double actualLoadedCapacity, double factor, double maxDischargeRate,double maxChargeRate, double chargeLossRate, double dischargeLossRate, double dod, double maximumCapacity){
		super(deviceId, deviceLabel, actualLoadedCapacity, maxDischargeRate, maxChargeRate, chargeLossRate, chargeLossRate,
				dischargeLossRate, maximumCapacity);
		this.deviceLabel = deviceLabel;
		this.dod = dod;
		this.esdFactor = factor;
	}
	public Battery(String deviceLabel, double actualLoadedCapacity, double factor, double maxDischargeRate,double maxChargeRate, double chargeLossRate, double dischargeLossRate, double dod, double maximumCapacity){
		super(null, deviceLabel, actualLoadedCapacity, maxDischargeRate, maxChargeRate, chargeLossRate, chargeLossRate,
				dischargeLossRate, maximumCapacity);
		this.deviceLabel = deviceLabel;
		this.dod = dod;
		this.esdFactor = factor;
	}

	public Battery(UUID id, String deviceLabel, double actualLoadedCapacity, double maxDischargeRate, double maxChargeRate,
			double energyLossRate, double chargeLossRate, double dischargeLossRate, double maximumCapacity,
			String modeB, String batteryRuntime, double upsOutputFreq, double bypassFreq, double batteryVoltage,
			double chargeCurrent, double dischargeCurrent, double rectifierVoltage, double bypassVoltage,
			double outputVoltage, double outputCurrent, double activeOutputPower, double reactiveOutputPower,
			double apparentOutputPower, double outputPowerB, double batteryTemperature, double boosterTemperature,
			double inverterTemperature) {
		super(id, deviceLabel, actualLoadedCapacity, maxDischargeRate, maxChargeRate, energyLossRate, chargeLossRate,
				dischargeLossRate, maximumCapacity);
		this.modeB = modeB;
		this.batteryRuntime = batteryRuntime;
		this.upsOutputFreq = upsOutputFreq;
		this.bypassFreq = bypassFreq;
		this.batteryVoltage = batteryVoltage;
		this.chargeCurrent = chargeCurrent;
		this.dischargeCurrent = dischargeCurrent;
		this.rectifierVoltage = rectifierVoltage;
		this.bypassVoltage = bypassVoltage;
		this.outputVoltage = outputVoltage;
		this.outputCurrent = outputCurrent;
		this.activeOutputPower = activeOutputPower;
		this.reactiveOutputPower = reactiveOutputPower;
		this.apparentOutputPower = apparentOutputPower;
		this.outputPowerB = outputPowerB;
		this.batteryTemperature = batteryTemperature;
		this.boosterTemperature = boosterTemperature;
		this.inverterTemperature = inverterTemperature;
	}

	public String getDeviceLabel() {
		return deviceLabel;
	}

	public void setDeviceLabel(String deviceLabel) {
		this.deviceLabel = deviceLabel;
	}

	public static Battery createDynamicInstance(UUID id, String deviceLabel, double value, String mode, String batteryRuntime,
												double upsOutputFreq, double bypassFreq, double batteryVoltage, double chargeCurrent,
												double dischargeCurrent, double rectifierVoltage, double bypassVoltage, double outputVoltage,
												double outputCurrent, double activeOutputPower, double reactiveOutputPower, double apparentOutputPower,
												double outputPowerB, double batteryTemperature, double boosterTemperature, double inverterTemperature) {
		return new Battery(id, deviceLabel, value, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, mode, batteryRuntime, upsOutputFreq,
				bypassFreq, batteryVoltage, chargeCurrent, dischargeCurrent, rectifierVoltage, bypassVoltage,
				outputVoltage, outputCurrent, activeOutputPower, reactiveOutputPower, apparentOutputPower,
				outputPowerB, batteryTemperature, boosterTemperature, inverterTemperature);
	}

	public double getDod() {
		return dod;
	}

	public void setDod(double dod) {
		this.dod = dod;
	}

	public double getEsdFactor() {
		return esdFactor;
	}

	public void setEsdFactor(double esdFactor) {
		this.esdFactor = esdFactor;
	}

	public String getModeB() {
		return modeB;
	}

	public void setModeB(String modeB) {
		this.modeB = modeB;
	}

	public String getBatteryRuntime() {
		return batteryRuntime;
	}

	public void setBatteryRuntime(String batteryRuntime) {
		this.batteryRuntime = batteryRuntime;
	}

	public double getUpsOutputFreq() {
		return upsOutputFreq;
	}

	public void setUpsOutputFreq(double upsOutputFreq) {
		this.upsOutputFreq = upsOutputFreq;
	}

	public double getBypassFreq() {
		return bypassFreq;
	}

	public void setBypassFreq(double bypassFreq) {
		this.bypassFreq = bypassFreq;
	}

	public double getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(double batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public double getChargeCurrent() {
		return chargeCurrent;
	}

	public void setChargeCurrent(double chargeCurrent) {
		this.chargeCurrent = chargeCurrent;
	}

	public double getDischargeCurrent() {
		return dischargeCurrent;
	}

	public void setDischargeCurrent(double dischargeCurrent) {
		this.dischargeCurrent = dischargeCurrent;
	}

	public double getRectifierVoltage() {
		return rectifierVoltage;
	}

	public void setRectifierVoltage(double rectifierVoltage) {
		this.rectifierVoltage = rectifierVoltage;
	}

	public double getBypassVoltage() {
		return bypassVoltage;
	}

	public void setBypassVoltage(double bypassVoltage) {
		this.bypassVoltage = bypassVoltage;
	}

	public double getOutputVoltage() {
		return outputVoltage;
	}

	public void setOutputVoltage(double outputVoltage) {
		this.outputVoltage = outputVoltage;
	}

	public double getOutputCurrent() {
		return outputCurrent;
	}

	public void setOutputCurrent(double outputCurrent) {
		this.outputCurrent = outputCurrent;
	}

	public double getActiveOutputPower() {
		return activeOutputPower;
	}

	public void setActiveOutputPower(double activeOutputPower) {
		this.activeOutputPower = activeOutputPower;
	}

	public double getReactiveOutputPower() {
		return reactiveOutputPower;
	}

	public void setReactiveOutputPower(double reactiveOutputPower) {
		this.reactiveOutputPower = reactiveOutputPower;
	}

	public double getApparentOutputPower() {
		return apparentOutputPower;
	}

	public void setApparentOutputPower(double apparentOutputPower) {
		this.apparentOutputPower = apparentOutputPower;
	}

	public double getOutputPowerB() {
		return outputPowerB;
	}

	public void setOutputPowerB(double outputPowerB) {
		this.outputPowerB = outputPowerB;
	}

	public double getBatteryTemperature() {
		return batteryTemperature;
	}

	public void setBatteryTemperature(double batteryTemperature) {
		this.batteryTemperature = batteryTemperature;
	}

	public double getBoosterTemperature() {
		return boosterTemperature;
	}

	public void setBoosterTemperature(double boosterTemperature) {
		this.boosterTemperature = boosterTemperature;
	}

	public double getInverterTemperature() {
		return inverterTemperature;
	}

	public void setInverterTemperature(double inverterTemperature) {
		this.inverterTemperature = inverterTemperature;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Battery battery = (Battery) o;
		return
				battery.deviceLabel.equals(deviceLabel) &&
				Double.compare(battery.bypassFreq, bypassFreq) == 0 &&
				Double.compare(battery.batteryVoltage, batteryVoltage) == 0 &&
				Double.compare(battery.chargeCurrent, chargeCurrent) == 0 &&
				Double.compare(battery.dischargeCurrent, dischargeCurrent) == 0 &&
				Double.compare(battery.rectifierVoltage, rectifierVoltage) == 0 &&
				Double.compare(battery.bypassVoltage, bypassVoltage) == 0 &&
				Double.compare(battery.outputVoltage, outputVoltage) == 0 &&
				Double.compare(battery.outputCurrent, outputCurrent) == 0 &&
				Double.compare(battery.activeOutputPower, activeOutputPower) == 0 &&
				Double.compare(battery.reactiveOutputPower, reactiveOutputPower) == 0 &&
				Double.compare(battery.apparentOutputPower, apparentOutputPower) == 0 &&
				Double.compare(battery.outputPowerB, outputPowerB) == 0 &&
				Double.compare(battery.batteryTemperature, batteryTemperature) == 0 &&
				Double.compare(battery.boosterTemperature, boosterTemperature) == 0 &&
				Double.compare(battery.inverterTemperature, inverterTemperature) == 0 &&
				Double.compare(battery.dod, dod) == 0 &&
				Double.compare(battery.esdFactor, esdFactor) == 0 &&
				Double.compare(battery.chargeLossRate, chargeLossRate) == 0 &&
				Double.compare(battery.dischargeLossRate, dischargeLossRate) == 0 &&
				Double.compare(battery.maxChargeRate, maxChargeRate) == 0 &&
				Double.compare(battery.maxDischargeRate, maxDischargeRate) == 0 &&
				Double.compare(battery.maximumCapacity, maximumCapacity) == 0 &&
				Double.compare(battery.actualLoadedCapacity, actualLoadedCapacity) == 0 &&
				Objects.equals(modeB, battery.modeB) &&
				Objects.equals(batteryRuntime, battery.batteryRuntime);
	}

	@Override
	public int hashCode() {
		return Objects.hash(modeB, batteryRuntime, upsOutputFreq, bypassFreq, batteryVoltage, chargeCurrent, dischargeCurrent, rectifierVoltage, bypassVoltage, outputVoltage, outputCurrent, activeOutputPower, reactiveOutputPower, apparentOutputPower, outputPowerB, batteryTemperature, boosterTemperature, inverterTemperature, dod, esdFactor);
	}

	@Override
	public String toString() {
		return "Battery{" +
				"dod=" + dod +
				", esdFactor=" + esdFactor +
				", actualLoadedCapacity=" + actualLoadedCapacity +
				", maxDischargeRate=" + maxDischargeRate +
				", maxChargeRate=" + maxChargeRate +
				", energyLossRate=" + energyLossRate +
				", chargeLossRate=" + chargeLossRate +
				", dischargeLossRate=" + dischargeLossRate +
				", maximumCapacity=" + maximumCapacity +
				", deviceLabel='" + deviceLabel + '\'' +
				", deviceId=" + deviceId +
				'}';
	}
}
