package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

public class WorkloadHosting extends ITComponentsOptimization{

        private static final long serialVersionUID = 4063489509599708640L;
        private double workloadAmount;
        private double percentOfWorkloadHosting;
        private String destinationDC;
        public static final String ACTION_TYPE = "Host Load (KWh)";

        public WorkloadHosting() {
            super(ACTION_TYPE);
        }

        public WorkloadHosting(long id, double estimatedEnergySaving, Date startTime, Date endTime, double percentOfWorkloadHosting, double workloadAmount, String destinationDC) {
            super(id, ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
            this.percentOfWorkloadHosting = percentOfWorkloadHosting;
            this.workloadAmount = workloadAmount;
            this.destinationDC = destinationDC;
        }

        public double getWorkloadAmount() {
            return this.workloadAmount;
        }

        public void setWorkloadAmount(double workloadAmount) {
            this.workloadAmount = workloadAmount;
        }

        public String getDestinationDC() {
            return this.destinationDC;
        }

        public void setDestinationDC(String destinationDC) {
            this.destinationDC = destinationDC;
        }

        public double getpercentOfWorkloadHosting() {
            return this.percentOfWorkloadHosting;
        }

        public void setpercentOfWorkloadHosting(double percentOfWorkloadReallocated) {
            this.percentOfWorkloadHosting = percentOfWorkloadReallocated;
        }
}
