package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import java.util.UUID;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = CoolingSystem.class), @JsonSubTypes.Type(value = LightingSystem.class),
		@JsonSubTypes.Type(value = HeatRecoveryInfrastructure.class),
		@JsonSubTypes.Type(value = TransformerSystem.class) })
public class Facility extends NonITComponent {

	private static final long serialVersionUID = 1L;

	public Facility() {

	}

	/**
	 * @param deviceLabel
	 * @param energyConsumption
	 */
	public Facility(String  deviceLabel, double energyConsumption) {
		super(deviceLabel, energyConsumption);
	}


	public Facility(UUID deviceId, String  deviceLabel, double energyConsumption) {
		super(deviceId, deviceLabel, energyConsumption);
	}
}
