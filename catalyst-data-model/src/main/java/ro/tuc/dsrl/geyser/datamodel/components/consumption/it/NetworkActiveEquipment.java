package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.annotations.ObjectProperty;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class NetworkActiveEquipment extends IndividualComponent {

	private static final long serialVersionUID = 6976944878556270416L;
	@DynamicField
	private double throughput;
	private int interfaces;
	@DynamicField
	private double networkUtilization;
	@JsonIgnore
	@ObjectProperty(value = "components")
	private Server server;

	public NetworkActiveEquipment() {
	}

	public NetworkActiveEquipment(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit,
			double lowMoistureLimit, double highMoistureLimit, double pIdle, double pMax, double throughput,
			int interfaces, double networkUtilization, double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				energyConsumption);
		this.throughput = throughput;
		this.interfaces = interfaces;
		this.networkUtilization = networkUtilization;
	}

	public static NetworkActiveEquipment createDynamicInstance(String deviceLabel, Double networkUtilization, Double throughput) {
		return new NetworkActiveEquipment(deviceLabel, -1, -1, -1, -1, -1, -1, throughput, -1, networkUtilization, 0);
	}

	/**
	 * @return the throughtput
	 */
	public double getThroughput() {
		return throughput;
	}

	/**
	 * @param throughput
	 *            the throughtput to set
	 */
	public void setThroughput(double throughput) {
		this.throughput = throughput;
	}

	/**
	 * @return the interfaces
	 */
	public int getInterfaces() {
		return interfaces;
	}

	/**
	 * @param interfaces
	 *            the interfaces to set
	 */
	public void setInterfaces(int interfaces) {
		this.interfaces = interfaces;
	}

	/**
	 * @return the networkUtilization
	 */
	public double getNetworkUtilization() {
		return networkUtilization;
	}

	/**
	 * @param networkUtilization
	 *            the networkUtilization to set
	 */
	public void setNetworkUtilization(double networkUtilization) {
		this.networkUtilization = networkUtilization;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

}
