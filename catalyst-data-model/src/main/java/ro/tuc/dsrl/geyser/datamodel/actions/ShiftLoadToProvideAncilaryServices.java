package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class ShiftLoadToProvideAncilaryServices extends SmartGridInteractionOptimization {

	private static final long serialVersionUID = 2067715923193272502L;
	private String ancilaryServiceType;
	
	private static final String ACTION_TYPE = "Shift load for ancillary services";

	public ShiftLoadToProvideAncilaryServices() {
		super(ACTION_TYPE);
	}
	
	public ShiftLoadToProvideAncilaryServices(long id, List<EnergyConsumptionOptimization> actions,
			double estimatedLoadShifted, String ancilaryServiceType, Date start, Date end) {
		super(id,ACTION_TYPE, actions, estimatedLoadShifted,start,end);
		this.ancilaryServiceType = ancilaryServiceType;
	}

	/**
	 * @return the ancilaryServiceType
	 */
	public String getAncilaryServiceType() {
		return ancilaryServiceType;
	}

	/**
	 * @param ancilaryServiceType
	 *            the ancilaryServiceType to set
	 */
	public void setAncilaryServiceType(String ancilaryServiceType) {
		this.ancilaryServiceType = ancilaryServiceType;
	}

}
