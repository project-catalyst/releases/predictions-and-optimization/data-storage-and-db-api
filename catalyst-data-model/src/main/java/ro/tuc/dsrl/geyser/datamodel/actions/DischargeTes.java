package ro.tuc.dsrl.geyser.datamodel.actions;



import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model-v2
 * @Since: Feb 5, 2015
 * @Description:
 *
 */
public class DischargeTes extends EnergyStorageOptimization {

	private static final long serialVersionUID = -5976653181049169741L;
	public static final String ACTION_TYPE = "Discharge TES (KWh)";
	
	public DischargeTes() {
		super(ACTION_TYPE);
	}

	public DischargeTes(long id, double estimatedEnergySaving, Date startTime, Date endTime, double amountOfEnergy) {
		super(id,ACTION_TYPE, estimatedEnergySaving, startTime, endTime, amountOfEnergy, new EnergyStorageComponent());
	}

	@Override
	public String toString() {
		String toString = "DischargeTes id : " + getId() + "\n";
		toString += super.toString();
		return toString;
	}
}