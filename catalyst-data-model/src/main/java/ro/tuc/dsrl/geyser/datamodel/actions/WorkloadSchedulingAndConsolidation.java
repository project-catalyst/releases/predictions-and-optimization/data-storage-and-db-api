package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class WorkloadSchedulingAndConsolidation extends ITComponentsOptimization {

	private static final long serialVersionUID = 4497442140914511560L;
	private int consolitdationType;
	
	private static final String ACTION_TYPE = "Load scheduling and consolidation";

	public WorkloadSchedulingAndConsolidation() {
		super(ACTION_TYPE);
	}
	
	public WorkloadSchedulingAndConsolidation(long id, double estimatedEnergySaving, Date startTime, Date endTime,
			int consolitdationType) {
		super(id, ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
		this.consolitdationType = consolitdationType;
	}

	public int getConsolitdationType() {
		return consolitdationType;
	}

	public void setConsolitdationType(int consolitdationType) {
		this.consolitdationType = consolitdationType;
	}

}
