package ro.tuc.dsrl.geyser.datamodel.indicators;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class PUE extends EnergyConsumption {

	public PUE() {
		super();
	}
	
	public PUE(long id, double indicatorValue, double optimalValue, double threshold) {
		super(id, indicatorValue, optimalValue, threshold);
	}
	
	public static PUE createDynamicInstance(long id, double indicatorValue) {
		return new PUE(id, indicatorValue, -1, -1);
	}
}
