package ro.tuc.dsrl.geyser.datamodel.other;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = ForecastedEnergyCurve.class) })
public class EnergyCurve {

	private List<EnergyPoint> curve;

	public EnergyCurve() {
		curve = new ArrayList<EnergyPoint>();
	}

	public EnergyCurve(List<EnergyPoint> curve) {
		this.curve = curve;
	}

	/**
	 * @return the curve
	 */
	public List<EnergyPoint> getCurve() {
		return curve;
	}

	/**
	 * @param curve the curve to set
	 */
	public void setCurve(List<EnergyPoint> curve) {
		this.curve = curve;
	}

}
