package ro.tuc.dsrl.geyser.datamodel.other;

import java.io.Serializable;
import java.util.List;

public class DSOSignalStrategy implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8793979614643780327L;
	private int dayAheadIntervals;
	private int intradayIntervals;
	private int realTimeIntervals;
	private int closestHourInterval;
	private int closestHalfHourInterval;
	private int asSignalStartInterval;
	private int startResponseTime; 
	private int endResponseTime; 
	private List<Double> scheduledTesChargeActions;
	private List<Double> schedluedTesDischargeActions;
	private List<Double> baseline;
	private List<Double> drSignal;
	private int dieselGeneratorsActive;
	private int enablePFOptimization;
	
	public DSOSignalStrategy(){
		
	}

	public DSOSignalStrategy(int dayAheadIntervals, int intradayIntervals,
			int realTimeIntervals, int closestHourInterval,
			int closestHalfHourInterval, int asSignalStartInterval,
			int startResponseTime, int endResponseTime,
			List<Double> scheduledTesChargeActions,
			List<Double> schedluedTesDischargeActions, List<Double> baseline,List<Double> drSignal,
			int dieselGeneratorsActive, int enablePFOptimization) {
		super();
		this.dayAheadIntervals = dayAheadIntervals;
		this.intradayIntervals = intradayIntervals;
		this.realTimeIntervals = realTimeIntervals;
		this.closestHourInterval = closestHourInterval;
		this.closestHalfHourInterval = closestHalfHourInterval;
		this.asSignalStartInterval = asSignalStartInterval;
		this.startResponseTime = startResponseTime;
		this.endResponseTime = endResponseTime;
		this.scheduledTesChargeActions = scheduledTesChargeActions;
		this.schedluedTesDischargeActions = schedluedTesDischargeActions;
		this.baseline = baseline;
		this.dieselGeneratorsActive = dieselGeneratorsActive;
		this.enablePFOptimization = enablePFOptimization;
		this.drSignal = drSignal;
	}

	public int getDayAheadIntervals() {
		return dayAheadIntervals;
	}

	public void setDayAheadIntervals(int dayAheadIntervals) {
		this.dayAheadIntervals = dayAheadIntervals;
	}

	public int getIntradayIntervals() {
		return intradayIntervals;
	}

	public void setIntradayIntervals(int intradayIntervals) {
		this.intradayIntervals = intradayIntervals;
	}

	public int getRealTimeIntervals() {
		return realTimeIntervals;
	}

	public void setRealTimeIntervals(int realTimeIntervals) {
		this.realTimeIntervals = realTimeIntervals;
	}

	public int getClosestHourInterval() {
		return closestHourInterval;
	}

	public void setClosestHourInterval(int closestHourInterval) {
		this.closestHourInterval = closestHourInterval;
	}

	public int getClosestHalfHourInterval() {
		return closestHalfHourInterval;
	}

	public void setClosestHalfHourInterval(int closestHalfHourInterval) {
		this.closestHalfHourInterval = closestHalfHourInterval;
	}

	public int getAsSignalStartInterval() {
		return asSignalStartInterval;
	}

	public void setAsSignalStartInterval(int asSignalStartInterval) {
		this.asSignalStartInterval = asSignalStartInterval;
	}

	public int getStartResponseTime() {
		return startResponseTime;
	}

	public void setStartResponseTime(int startResponseTime) {
		this.startResponseTime = startResponseTime;
	}

	public int getEndResponseTime() {
		return endResponseTime;
	}

	public void setEndResponseTime(int endResponseTime) {
		this.endResponseTime = endResponseTime;
	}

	public List<Double> getScheduledTesChargeActions() {
		return scheduledTesChargeActions;
	}

	public void setScheduledTesChargeActions(List<Double> scheduledTesChargeActions) {
		this.scheduledTesChargeActions = scheduledTesChargeActions;
	}

	public List<Double> getSchedluedTesDischargeActions() {
		return schedluedTesDischargeActions;
	}

	public void setSchedluedTesDischargeActions(
			List<Double> schedluedTesDischargeActions) {
		this.schedluedTesDischargeActions = schedluedTesDischargeActions;
	}

	public List<Double> getBaseline() {
		return baseline;
	}

	public void setBaseline(List<Double> baseline) {
		this.baseline = baseline;
	}

	public int getDieselGeneratorsActive() {
		return dieselGeneratorsActive;
	}

	public void setDieselGeneratorsActive(int dieselGeneratorsActive) {
		this.dieselGeneratorsActive = dieselGeneratorsActive;
	}

	public int getEnablePFOptimization() {
		return enablePFOptimization;
	}

	public void setEnablePFOptimization(int enablePFOptimization) {
		this.enablePFOptimization = enablePFOptimization;
	}

	public List<Double> getDrSignal() {
		return drSignal;
	}

	public void setDrSignal(List<Double> drSignal) {
		this.drSignal = drSignal;
	}

	@Override
	public String toString() {
		return "DSOSignalStrategy [dayAheadIntervals=" + dayAheadIntervals + ", intradayIntervals=" + intradayIntervals
				+ ", realTimeIntervals=" + realTimeIntervals + ", closestHourInterval=" + closestHourInterval
				+ ", closestHalfHourInterval=" + closestHalfHourInterval + ", asSignalStartInterval="
				+ asSignalStartInterval + ", startResponseTime=" + startResponseTime + ", endResponseTime="
				+ endResponseTime + ", baseline=" + baseline + ", drSignal=" + drSignal + ", dieselGeneratorsActive="
				+ dieselGeneratorsActive + ", enablePFOptimization=" + enablePFOptimization + "]";
	}
	
	

}
