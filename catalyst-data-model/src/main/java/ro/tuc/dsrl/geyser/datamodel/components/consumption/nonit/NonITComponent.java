package ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyConsumptionComponent;

import java.util.UUID;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = Facility.class), @JsonSubTypes.Type(value = OfficeBuilding.class) })
public class NonITComponent extends EnergyConsumptionComponent {

	private static final long serialVersionUID = 1L;

	public NonITComponent() {
	}

	/**
	 * @param deviceLabel
	 * @param energyConsumption
	 */
	public NonITComponent(String deviceLabel, double energyConsumption) {
		super(deviceLabel, energyConsumption);
	}

	public NonITComponent(UUID deviceId, String deviceLabel, double energyConsumption) {
		super(deviceId, deviceLabel, energyConsumption);
	}
}
