package ro.tuc.dsrl.geyser.datamodel.other;

public enum AncillaryServiceType {
	REGULATION, RESERVE, SCHEDULING, REACTIVE, CATALYST
}
