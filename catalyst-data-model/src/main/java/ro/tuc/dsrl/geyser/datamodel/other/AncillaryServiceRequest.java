package ro.tuc.dsrl.geyser.datamodel.other;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

public class AncillaryServiceRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1118994794519917911L;
	
	private DateTime startResponseTime;
	private DateTime endResponseTime;

	private List<Double> drSignal;

	private AncillaryServiceType ancillaryServiceType;

	public AncillaryServiceRequest(DateTime startResponseTime,
			DateTime endResponseTime, List<Double> drSignal,
			AncillaryServiceType ancillaryServiceType) {
		super();
		this.startResponseTime = startResponseTime;
		this.endResponseTime = endResponseTime;
		this.drSignal = drSignal;
		this.ancillaryServiceType = ancillaryServiceType;
	}

	public AncillaryServiceRequest()
	{

	}

	public DateTime getStartResponseTime() {
		return startResponseTime;
	}

	public void setStartResponseTime(DateTime startResponseTime) {
		this.startResponseTime = startResponseTime;
	}

	public DateTime getEndResponseTime() {
		return endResponseTime;
	}

	public void setEndResponseTime(DateTime endResponseTime) {
		this.endResponseTime = endResponseTime;
	}

	public List<Double> getDrSignal() {
		return drSignal;
	}

	public void setDrSignal(List<Double> drSignal) {
		this.drSignal = drSignal;
	}

	public AncillaryServiceType getAncillaryServiceType() {
		return ancillaryServiceType;
	}

	public void setAncillaryServiceType(AncillaryServiceType ancillaryServiceType) {
		this.ancillaryServiceType = ancillaryServiceType;
	}
	
	
	
}
