package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyConsumptionComponent;

import java.util.UUID;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = IndividualComponent.class),
		@JsonSubTypes.Type(value = CombinedComponent.class) })
public class ITComponent extends EnergyConsumptionComponent {

	private static final long serialVersionUID = -2821449872233515867L;
	private double lowTemperatureLimit;
	private double highTemperatureLimit;
	private double lowMoistureLimit;
	private double highMoistureLimit;
	private double pIdle;
	private double pMax;

	public ITComponent() {
	}

	public ITComponent(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double energyConsumption) {
		super(deviceLabel, energyConsumption);
		this.lowTemperatureLimit = lowTemperatureLimit;
		this.highTemperatureLimit = highTemperatureLimit;
		this.lowMoistureLimit = lowMoistureLimit;
		this.highMoistureLimit = highMoistureLimit;
		this.pIdle = pIdle;
		this.pMax = pMax;
	}

	public ITComponent(UUID deviceId, String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
					   double highMoistureLimit, double pIdle, double pMax, double energyConsumption) {
		super(deviceId, deviceLabel, energyConsumption);
		this.lowTemperatureLimit = lowTemperatureLimit;
		this.highTemperatureLimit = highTemperatureLimit;
		this.lowMoistureLimit = lowMoistureLimit;
		this.highMoistureLimit = highMoistureLimit;
		this.pIdle = pIdle;
		this.pMax = pMax;
	}

	/**
	 * @return the lowTemperatureLimit
	 */
	public double getLowTemperatureLimit() {
		return lowTemperatureLimit;
	}

	/**
	 * @param lowTemperatureLimit
	 *            the lowTemperatureLimit to set
	 */
	public void setLowTemperatureLimit(double lowTemperatureLimit) {
		this.lowTemperatureLimit = lowTemperatureLimit;
	}

	/**
	 * @return the highTemperatureLimit
	 */
	public double getHighTemperatureLimit() {
		return highTemperatureLimit;
	}

	/**
	 * @param highTemperatureLimit
	 *            the highTemperatureLimit to set
	 */
	public void setHighTemperatureLimit(double highTemperatureLimit) {
		this.highTemperatureLimit = highTemperatureLimit;
	}

	/**
	 * @return the lowMoistureLimit
	 */
	public double getLowMoistureLimit() {
		return lowMoistureLimit;
	}

	/**
	 * @param lowMoistureLimit
	 *            the lowMoistureLimit to set
	 */
	public void setLowMoistureLimit(double lowMoistureLimit) {
		this.lowMoistureLimit = lowMoistureLimit;
	}

	/**
	 * @return the highMoistureLimit
	 */
	public double getHighMoistureLimit() {
		return highMoistureLimit;
	}

	/**
	 * @param highMoistureLimit
	 *            the highMoistureLimit to set
	 */
	public void setHighMoistureLimit(double highMoistureLimit) {
		this.highMoistureLimit = highMoistureLimit;
	}

	/**
	 * @return the pIdle
	 */
	public double getpIdle() {
		return pIdle;
	}

	/**
	 * @param pIdle
	 *            the pIdle to set
	 */
	public void setpIdle(double pIdle) {
		this.pIdle = pIdle;
	}

	/**
	 * @return the pMax
	 */
	public double getpMax() {
		return pMax;
	}

	/**
	 * @param pMax
	 *            the pMax to set
	 */
	public void setpMax(double pMax) {
		this.pMax = pMax;
	}

}
