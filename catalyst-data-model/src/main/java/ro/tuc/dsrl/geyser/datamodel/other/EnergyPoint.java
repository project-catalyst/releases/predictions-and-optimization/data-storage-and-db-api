package ro.tuc.dsrl.geyser.datamodel.other;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 2, 2015
 * @Description:
 *
 */
public class EnergyPoint {

	private EnergyValue energyValue;
	private Date timeStamp;

	public EnergyPoint() {
	}

	public EnergyPoint(EnergyValue energyValue, Date timeStamp) {
		this.energyValue = energyValue;
		this.timeStamp = timeStamp;
	}

	public EnergyPoint(double value, EnergyMeasurementUnit measurementUnit, Date timeStamp) {
		this.energyValue = new EnergyValue(value, measurementUnit);
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the timeStamp
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the energyValue
	 */
	public EnergyValue getEnergyValue() {
		return energyValue;
	}

	/**
	 * @param energyValue
	 *            the energyValue to set
	 */
	public void setEnergyValue(EnergyValue energyValue) {
		this.energyValue = energyValue;
	}
}
