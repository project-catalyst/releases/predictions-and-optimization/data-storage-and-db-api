package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 26, 2015
 * @Description:
 *
 */
public class SellEnergy extends MarketplaceAction {
	private static final long serialVersionUID = 6967014324827605792L;

	public static final String ACTION_TYPE = "Sell Energy (KWh)";

	public SellEnergy() {
		super(ACTION_TYPE);
	}

	public SellEnergy(long id, Date startTime, Date endTime, double energyAmount, double price) {
		super(id, ACTION_TYPE, startTime, endTime, energyAmount, price);
	}

	@Override
	public String toString() {
		String toString = "SellEnergy id : " + getId() + "\n";
		toString += super.toString();
		return toString;
	}
}