package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class StorageServer extends CombinedComponent {
	private static final long serialVersionUID = -3267992493944585092L;

	public StorageServer() {
	}

	public StorageServer(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
			double highMoistureLimit, double pIdle, double pMax, double inputAirTemperatureCC,
			double outputAirTemperatureCC, double airFlowCC, double energyConsumption) {
		super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
				inputAirTemperatureCC, outputAirTemperatureCC, airFlowCC, energyConsumption);
	}
}
