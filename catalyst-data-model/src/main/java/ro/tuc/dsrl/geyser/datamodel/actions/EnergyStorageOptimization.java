package ro.tuc.dsrl.geyser.datamodel.actions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.annotations.ObjectProperty;
import ro.tuc.dsrl.geyser.datamodel.components.EnergyStorageComponent;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = DischargeEnergy.class), @JsonSubTypes.Type(value = ChargeEnergy.class) })
public class EnergyStorageOptimization extends EnergyConsumptionOptimization {

	private static final long serialVersionUID = 3338149669090853603L;
	//computed using the lossfactor* amountOfEnergy
	private double estimatedEnergySaving;

	@ObjectProperty(value = "optimizationActions")
	private EnergyStorageComponent energyStorageComponent;

	public EnergyStorageOptimization(String description) {
		super(description);
	}

	public EnergyStorageOptimization(long id,String description, double estimatedEnergySaving, Date startTime, Date endTime,
			double amountOfEnergy, EnergyStorageComponent energyStorageComponent) {
		super(id, description, amountOfEnergy, startTime, endTime);
		this.estimatedEnergySaving = estimatedEnergySaving;
		this.energyStorageComponent = energyStorageComponent;
	}

	public double getEstimatedEnergySaving() {
		return estimatedEnergySaving;
	}

	public void setEstimatedEnergySaving(double estimatedEnergySaving) {
		this.estimatedEnergySaving = estimatedEnergySaving;
	}

	/**
	 * @return
	 */
	public EnergyStorageComponent getEnergyStorageComponent() {
		return energyStorageComponent;
	}

	/**
	 * @param energyStorageComponent
	 */
	public void setEnergyStorageComponent(EnergyStorageComponent energyStorageComponent) {
		this.energyStorageComponent = energyStorageComponent;
	}

}
