package ro.tuc.dsrl.geyser.datamodel.components.consumption.it;


import java.util.Objects;
import java.util.UUID;

public class ServerRoom extends CombinedComponent {
    private static final long serialVersionUID = -4754201927290913004L;
    private double maxEnergyConsumption;
    private double edPercentage;
    private double  maxHostLoad;
    private double maxReallocLoad;
    private double itDT;
    private double itRT;
   // private List<CombinedComponent> combinedComponents;

    public ServerRoom() {
    }

    public ServerRoom(String deviceLabel, double lowTemperatureLimit, double highTemperatureLimit, double lowMoistureLimit,
                      double highMoistureLimit, double pIdle, double pMax, double inputAirTemperatureCC,
                      double outputAirTemperatureCC, double airFlowCC, double energyConsumption) {
        super(deviceLabel, lowTemperatureLimit, highTemperatureLimit, lowMoistureLimit, highMoistureLimit, pIdle, pMax,
                inputAirTemperatureCC, outputAirTemperatureCC, airFlowCC, energyConsumption);
        this.maxEnergyConsumption = pMax;

    }

    public ServerRoom(String deviceLabel, double energyConsumption, double pMax) {
        super(deviceLabel, -1, -1, -1, -1, -1, pMax,
                -1, -1, -1, energyConsumption);
        this.maxEnergyConsumption = pMax;
    }

    public ServerRoom(String deviceLabel, double energyConsumption, double pMax, double edPercentage, double maxHostLoad, double maxReallocLoad) {
        super(deviceLabel, -1, -1, -1, -1, -1, pMax,
                -1, -1, -1, energyConsumption);
        this.maxEnergyConsumption = pMax;
        this.edPercentage = edPercentage;
        this.maxHostLoad = maxHostLoad;
        this.maxReallocLoad = maxReallocLoad;
    }

    public ServerRoom(UUID deviceId, String deviceLabel, double energyConsumption, double pMax, double edPercentage, double maxHostLoad, double maxReallocLoad, double rt, double dt) {
        super(deviceId, deviceLabel, -1, -1, -1, -1, -1, pMax,
                -1, -1, -1, energyConsumption);
        this.maxEnergyConsumption = pMax;
        this.edPercentage = edPercentage;
        this.maxHostLoad = maxHostLoad;
        this.maxReallocLoad = maxReallocLoad;
        this.itDT = dt;
        this.itRT = rt;
    }

    public double getItDT() {
        return itDT;
    }

    public void setItDT(double itDT) {
        this.itDT = itDT;
    }

    public double getItRT() {
        return itRT;
    }

    public void setItRT(double itRT) {
        this.itRT = itRT;
    }

    public double getMaxEnergyConsumption() {
        return maxEnergyConsumption;
    }

    public void setMaxEnergyConsumption(double maxEnergyConsumption) {
        this.maxEnergyConsumption = maxEnergyConsumption;
    }

    public double getEdPercentage() {
        return edPercentage;
    }

    public void setEdPercentage(double edPercentage) {
        this.edPercentage = edPercentage;
    }

    public double getMaxHostLoad() {
        return maxHostLoad;
    }

    public void setMaxHostLoad(double maxHostLoad) {
        this.maxHostLoad = maxHostLoad;
    }

    public double getMaxReallocLoad() {
        return maxReallocLoad;
    }

    public void setMaxReallocLoad(double maxReallocLoad) {
        this.maxReallocLoad = maxReallocLoad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerRoom that = (ServerRoom) o;
        return that.deviceLabel.equals(deviceLabel) &&
                Double.compare(that.maxEnergyConsumption, maxEnergyConsumption) == 0 &&
                Double.compare(that.edPercentage, edPercentage) == 0 &&
                Double.compare(that.maxHostLoad, maxHostLoad) == 0 &&
                Double.compare(that.maxReallocLoad, maxReallocLoad) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxEnergyConsumption, edPercentage, maxHostLoad, maxReallocLoad);
    }

    @Override
    public String toString() {
        return "ServerRoom{" +
                "maxEnergyConsumption=" + maxEnergyConsumption +
                ", edPercentage=" + edPercentage +
                ", maxHostLoad=" + maxHostLoad +
                ", maxReallocLoad=" + maxReallocLoad +
                '}';
    }
}
