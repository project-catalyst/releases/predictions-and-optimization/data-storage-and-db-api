package ro.tuc.dsrl.geyser.datamodel.components;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import ro.tuc.dsrl.geyser.datamodel.annotations.DynamicField;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ITComponent;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.nonit.NonITComponent;

import java.util.UUID;


/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = ITComponent.class), @JsonSubTypes.Type(value = NonITComponent.class) })
public class EnergyConsumptionComponent extends Component {

	private static final long serialVersionUID = 1L;
	@DynamicField
	protected double energyConsumption;

	public EnergyConsumptionComponent() {

	}

	public EnergyConsumptionComponent(String deviceLabel, double energyConsumption) {
		super(deviceLabel);
		this.energyConsumption = energyConsumption;
	}

	public EnergyConsumptionComponent(UUID deviceId, String deviceLabel, double energyConsumption) {
		super(deviceId, deviceLabel);
		this.energyConsumption = energyConsumption;
	}

	public double estimateEnergyConsumption() {
		return 0;
	}

	public double getEnergyConsumption() {
		return energyConsumption;
	}

	public void setEnergyConsumption(double energyConsumption) {
		this.energyConsumption = energyConsumption;
	}

}
