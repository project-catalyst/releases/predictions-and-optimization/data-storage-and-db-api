package ro.tuc.dsrl.geyser.datamodel.actions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import java.util.Date;
import java.util.Objects;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Feb 26, 2015
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = BuyEnergy.class),
		@JsonSubTypes.Type(value = SellEnergy.class) })
public class MarketplaceAction extends EnergyEfficiencyOptimizationAction {

	private static final long serialVersionUID = 6891102921146256939L;
	private double price;

	public MarketplaceAction(String description) {
		super(description);
	}

	public MarketplaceAction(long id,String description, Date startTime, Date endTime,
			double amountOfEnergy, double priceValue) {
		super(id, description, startTime, endTime, amountOfEnergy);
		this.price = priceValue;
	}

	@Override
	public String toString() {
		String toString = "Amount of Energy : " + super.getAmountOfEnergy()
				+ "\n";
		toString += super.toString();
		return toString;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		MarketplaceAction that = (MarketplaceAction) o;
		return Double.compare(that.price, price) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), price);
	}
}
