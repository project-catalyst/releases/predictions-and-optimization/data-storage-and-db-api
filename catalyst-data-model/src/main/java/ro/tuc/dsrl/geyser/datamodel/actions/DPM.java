package ro.tuc.dsrl.geyser.datamodel.actions;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
public class DPM extends ITComponentsOptimization {

	private static final long serialVersionUID = 934060969125374076L;

	private double powerStateBefore;
	private double powerStateAfter;
	private static final String ACTION_TYPE = "DPM";

	public DPM() {
		super(ACTION_TYPE);
	}
	
	public DPM(long id, double estimatedEnergySaving, Date startTime, Date endTime, double powerStateBefore,
			double powerStateAfter) {
		super(id, ACTION_TYPE, estimatedEnergySaving, startTime, endTime);
		this.powerStateBefore = powerStateBefore;
		this.powerStateAfter = powerStateAfter;
	}

	public double getPowerStateBefore() {
		return powerStateBefore;
	}

	public void setPowerStateBefore(double powerStateBefore) {
		this.powerStateBefore = powerStateBefore;
	}

	public double getPowerStateAfter() {
		return powerStateAfter;
	}

	public void setPowerStateAfter(double powerStateAfter) {
		this.powerStateAfter = powerStateAfter;
	}

}
