package ro.tuc.dsrl.geyser.datamodel.actions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Project: FP7 GEYSER Project, http://www.geyser-project.eu/
 * @Module: data-model
 * @Since: Dec 16, 2014
 * @Description:
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = ShiftDelayTolerantWorkload.class) })
public class ITComponentsOptimization extends EnergyConsumptionOptimization {

	private static final long serialVersionUID = -1533707454419764113L;

	public ITComponentsOptimization() {
	}

	public ITComponentsOptimization(String description) {
		super(description);
	}

	public ITComponentsOptimization(long id, String description, double estimatedEnergySaving, Date startTime,
			Date endTime) {
		super(id, description, estimatedEnergySaving, startTime, endTime);
	}
}
