package ro.tuc.dsrl.catalyst.model.dto;

import java.util.ArrayList;
import java.util.List;

public class ProfileDTO {

    private List<Double> values;

    public ProfileDTO(){

    }

    public ProfileDTO(String s){
        values = new ArrayList<>();
        String[] parts = s.split(",");
        for(int i =0; i<parts.length; i++){
            double d = 0;
            try{
                d = Double.parseDouble(parts[i]);
            }catch (NumberFormatException e){
            }
            values.add(d);
        }

    }

    public ProfileDTO(List<Double> values) {
        this.values = values;
    }

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }
}
