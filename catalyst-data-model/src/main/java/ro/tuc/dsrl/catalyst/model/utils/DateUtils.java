package ro.tuc.dsrl.catalyst.model.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

    private static final int INTRA_DAY_PERIOD = 4;
    private static final int DAY_AHEAD_PERIOD = 24;

    private DateUtils() {
    }

    public static Date dateFromLocalDateTime(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime localDateTimeFromDate(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    //TODO: remove redundant methods...
    //TODO: BUG ALERT localDateTimeToLong returns number of SECONDS
    @Deprecated
    public static Long localDateTimeToLong(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
    }

    public static Long localDateTimeToLongInMillis(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static Long datetoLong(Date date) {
        return date.getTime();
    }

    public static Date convertToDateViaInstant(LocalDateTime dateToConvert) {
        return java.util.Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date longToDate(long value) {
        return new Date(value);
    }

    public static Long dateTimeToMillis(LocalDateTime localDateTime) {
        return localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    public static LocalDateTime millisToLocalDateTime(Long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault());
    }

    public static LocalDateTime millisToUTCLocalDateTime(Long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), TimeZone.getTimeZone("UTC").toZoneId());
    }

    public static LocalDateTime isoStringToLocalDateTime(String dateTimeAsString){

        DateTimeFormatter formatter = null;

        if (dateTimeAsString.contains(".")) {
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                    .withZone(ZoneId.of("UTC"));
        }
        else {
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
                    .withZone(ZoneId.of("UTC"));
        }

        LocalDateTime dateTime = LocalDateTime.parse(dateTimeAsString, formatter);
        return  dateTime;
    }

    public static LocalDateTime computeCurrentIntraDayStart(LocalDateTime current){

        int currentHour = current.getHour();
        int hour = currentHour - (currentHour % INTRA_DAY_PERIOD);

        return current.withHour(hour).withMinute(0).withSecond(0).withNano(0);
    }

    public static LocalDateTime computeCurrentIntraDayEnd(LocalDateTime current){

        int currentHour = current.getHour();
        int hour = currentHour - (currentHour % INTRA_DAY_PERIOD);
        hour = hour + INTRA_DAY_PERIOD;

        if(hour != DAY_AHEAD_PERIOD){
            return current.withHour(hour).withMinute(0).withSecond(0).withNano(0);
        } else {
            return computeCurrentDayEnd(current);
        }

    }

    public static LocalDateTime computeCurrentDayStart(LocalDateTime current){
        return current.withHour(0).withMinute(0).withSecond(0).withNano(0);
    }

    public static LocalDateTime computeCurrentDayEnd(LocalDateTime current){
        return current.plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
    }

}