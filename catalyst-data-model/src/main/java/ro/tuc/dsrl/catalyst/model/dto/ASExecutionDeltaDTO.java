//package ro.tuc.dsrl.catalyst.model.dto;
//
//import java.util.List;
//
//import static org.junit.Assert.assertTrue;
//
//public class ASExecutionDeltaDTO {
//
//    private int startHour;
//
//    private List<Double> deltaDiff;
//
//    private int length = 0;
//
//    public ASExecutionDeltaDTO(int length) {
//        this.length = length;
//    }
//
//    public List<Double> getDeltaDiff() {
//        return deltaDiff;
//    }
//
//    public void setDeltaDiff(List<Double> deltaDiff) {
//        assertTrue("deltaDiff " + deltaDiff.size(), deltaDiff.size() == length);
//        this.deltaDiff = deltaDiff;
//    }
//
//    public int getStartHour() {
//        return startHour;
//    }
//
//    public void setStartHour(int startHour) {
//        this.startHour = startHour;
//    }
//
//    public int getLength() {
//        return length;
//    }
//
//    public void setLength(int length) {
//        this.length = length;
//    }
//}
