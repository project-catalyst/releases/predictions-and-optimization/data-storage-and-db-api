package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class MonitoredValueDTO implements Serializable {

    private UUID id;
    private LocalDateTime timestamp;
    private double value;
    private UUID deviceId;
    private UUID measurementId;

    public MonitoredValueDTO(){

    }

    public MonitoredValueDTO(UUID id,
                             LocalDateTime timestamp,
                             double value,
                             UUID deviceId,
                             UUID measurementId) {
        this.id = id;
        this.timestamp = timestamp;
        this.value = value;
        this.deviceId = deviceId;
        this.measurementId = measurementId;
    }

    public MonitoredValueDTO(LocalDateTime timestamp, double value, UUID deviceId, UUID measurementId) {
        this.timestamp = timestamp;
        this.value = value;
        this.deviceId = deviceId;
        this.measurementId = measurementId;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public double getValue() {
        return value;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public UUID getMeasurementId() {
        return measurementId;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public void setMeasurementId(UUID measurementId) {
        this.measurementId = measurementId;
    }

    @Override
    public String toString() {
        return "MonitoredValueDTO{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", value=" + value +
                ", deviceId=" + deviceId +
                ", measurementId=" + measurementId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MonitoredValueDTO)) return false;
        MonitoredValueDTO that = (MonitoredValueDTO) o;
        return Double.compare(that.value, value) == 0 &&
                Objects.equals(id, that.id) &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(measurementId, that.measurementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, value, deviceId, measurementId);
    }
}
