package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class OptimizationPlanDTO {
    private UUID id;

    private String timeframe;

    private LocalDateTime start;

    private LocalDateTime end;

    private Boolean selected;

    private Double confidenceLevel;

    private Double costSavings;

    private Double carbonSavings;

    private Double energySavings;

    private Double WE;

    private Double WL;

    private Double WF;

    private Double WT;

    private Double Wren;

    private Boolean reallocActive;

    private UUID dataCenterId;

    public OptimizationPlanDTO() {
    }

    public OptimizationPlanDTO(UUID id, String timeframe, LocalDateTime start, LocalDateTime end, Boolean selected, Double confidenceLevel, Double costSavings, Double carbonSavings, Double energySavings) {
        this.id = id;
        this.timeframe = timeframe;
        this.start = start;
        this.end = end;
        this.selected = selected;
        this.confidenceLevel = confidenceLevel;
        this.costSavings = costSavings;
        this.carbonSavings = carbonSavings;
        this.energySavings = energySavings;
    }

    public OptimizationPlanDTO(UUID id, String timeframe, LocalDateTime start, LocalDateTime end, Boolean selected, Double confidenceLevel, Double costSavings, Double carbonSavings, Double energySavings, Double WE, Double WL, Double WF, Double WT, Double Wren, Boolean reallocActive, UUID dataCenterId) {
        this.id = id;
        this.timeframe = timeframe;
        this.start = start;
        this.end = end;
        this.selected = selected;
        this.confidenceLevel = confidenceLevel;
        this.costSavings = costSavings;
        this.carbonSavings = carbonSavings;
        this.energySavings = energySavings;
        this.WE = WE;
        this.WL = WL;
        this.WF = WF;
        this.WT = WT;
        this.Wren = Wren;
        this.reallocActive = reallocActive;
        this.dataCenterId = dataCenterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OptimizationPlanDTO that = (OptimizationPlanDTO) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Double getConfidenceLevel() {
        return confidenceLevel;
    }

    public void setConfidenceLevel(Double confidenceLevel) {
        this.confidenceLevel = confidenceLevel;
    }

    public Double getCostSavings() {
        return costSavings;
    }

    public void setCostSavings(Double costSavings) {
        this.costSavings = costSavings;
    }

    public Double getCarbonSavings() {
        return carbonSavings;
    }

    public void setCarbonSavings(Double carbonSavings) {
        this.carbonSavings = carbonSavings;
    }

    public Double getEnergySavings() {
        return energySavings;
    }

    public void setEnergySavings(Double energySavings) {
        this.energySavings = energySavings;
    }

    public Double getWE() {
        return WE;
    }

    public void setWE(Double WE) {
        this.WE = WE;
    }

    public Double getWL() {
        return WL;
    }

    public void setWL(Double WL) {
        this.WL = WL;
    }

    public Double getWF() {
        return WF;
    }

    public void setWF(Double WF) {
        this.WF = WF;
    }

    public Double getWT() {
        return WT;
    }

    public void setWT(Double WT) {
        this.WT = WT;
    }

    public Double getWren() {
        return Wren;
    }

    public void setWren(Double Wren) {
        this.Wren = Wren;
    }

    public Boolean getReallocActive() {
        return reallocActive;
    }

    public void setReallocActive(Boolean reallocActive) {
        this.reallocActive = reallocActive;
    }

    public UUID getDataCenterId() {
        return dataCenterId;
    }

    public void setDataCenterId(UUID dataCenterId) {
        this.dataCenterId = dataCenterId;
    }
}
