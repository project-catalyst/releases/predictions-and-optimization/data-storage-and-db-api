package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;

public class MetricsDTO implements Serializable {
    private String id;
    private long time;
    private String datacenterName;
    private String metric;
    private String timeframe;
    private double value;


    public MetricsDTO(){}

    public MetricsDTO(String id, long time, String datacenterName, String metric, double value, String timeframe) {
        this.id = id;
        this.time = time;
        this.datacenterName = datacenterName;
        this.metric = metric;
        this.value = value;
        this.timeframe = timeframe;
    }

    public MetricsDTO(long time, String datacenterName, String metric, double value, String timeframe) {
        this.time = time;
        this.datacenterName = datacenterName;
        this.metric = metric;
        this.value = value;
        this.timeframe = timeframe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDatacenterName() {
        return datacenterName;
    }

    public void setDatacenterName(String datacenterName) {
        this.datacenterName = datacenterName;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }
}
