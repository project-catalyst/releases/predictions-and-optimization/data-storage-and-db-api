package ro.tuc.dsrl.catalyst.model.enums;

import ro.tuc.dsrl.catalyst.model.utils.DateUtils;

import java.time.LocalDateTime;

public enum Scenario {
    AS_2018(1,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2018-11-25T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2019-09-06T14:10:51Z"),
            "DATACENTER_POZNAN", "",  ""),
    AS_CATALYST(0,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2019-06-29T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2019-06-29T00:00:00Z"),
            "DATACENTER_POZNAN",
            "",
            ""),
    AS_2019(2,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-05-06T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-05-06T00:00:00Z"),
            "DATACENTER_POZNAN", "none",
            "scenario0"),
    SCENARIO_1(3,
                    "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-06-01T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-06-01T00:00:00Z"),
                    "PILOT_DATACENTER", "none",
                    "scenario1"),
    SCENARIO_2(4,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-06-03T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-06-03T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario2"),
    SCENARIO_3(5,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-06-05T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-06-05T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario3"),
    SCENARIO_4(6,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-06-07T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-06-07T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario4"),
    SCENARIO_5(7,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-06-09T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-06-09T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario5"),
    SCENARIO_6(8,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-06-13T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-06-13T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario6"),
    SCENARIO_7(9,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-06-17T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-06-17T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario7"),
    SCENARIO_8(10,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-07-06T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-06T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario8"),
    SCENARIO_9(11,
                       "catalyst/realtime_workload-catalyst-v2.xls",
               DateUtils.isoStringToLocalDateTime("2020-07-07T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-07T00:00:00Z"),
                    "PILOT_DATACENTER", "none",
                    "scenario8"),
    SCENARIO_10(12,
                       "catalyst/realtime_workload-catalyst-v2.xls",
               DateUtils.isoStringToLocalDateTime("2020-07-08T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-08T00:00:00Z"),
                    "PILOT_DATACENTER", "none",
                    "scenario8"),
    SCENARIO_11(13,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-07-09T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-09T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario8"),
    SCENARIO_12(14,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-07-10T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-10T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario8"),
    SCENARIO_13(15,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-07-11T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-11T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario9-psm-flex"),
    SCENARIO_14(16,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-07-12T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-12T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario10-psm-thermal"),
    SCENARIO_15(17,
                       "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-07-13T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-07-13T00:00:00Z"),
                    "PILOT_DATACENTER", "none",
                    "scenario11-psm-flex-thermal"),
    SCENARIO_PSM(18,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-09-15T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-09-15T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario9-psm-flex"),
    SCENARIO_PSM_2(19,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-09-16T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-09-16T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario9-psm-flex"),
    SCENARIO_REN(20,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-08-08T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-08-08T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario12-ren"),
    SCENARIO_QARNOT(21,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-08-03T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-08-03T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario-qarnot-flex"),
    SCENARIO_PSNC(22,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-12-11T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-12-11T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario-psnc-flex"),
    SCENARIO_SBP(23,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-10-21T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-10-21T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario-sbp-flex"),
    SCENARIO_PSM_REVIEW(24,
            "catalyst/realtime_workload-catalyst-v2.xls",
            DateUtils.isoStringToLocalDateTime("2020-10-10T00:00:00Z"),
            DateUtils.isoStringToLocalDateTime("2020-10-10T00:00:00Z"),
            "PILOT_DATACENTER", "none",
            "scenario-psm-flex-review");

    private Integer index;
    private String title;
    private LocalDateTime time;
    private LocalDateTime predictionJobTime;
    private String dataCenterName;

    private String fileAlgorithm;
	private String fileInputData;

    Scenario(Integer index, String title, LocalDateTime time, LocalDateTime predictionJobTime, String dataCenterName, String fileAlgorithm, String fileInputData) {
        this.index = index;
        this.title = title;
        this.time = time;
        this.predictionJobTime = predictionJobTime;
        this.dataCenterName = dataCenterName;
        this.fileAlgorithm = fileAlgorithm;
        this.fileInputData = fileInputData;
    }

    public static Scenario getByIndex(Integer index){
        for (Scenario scenario : Scenario.values()) {
            if(scenario.index.equals(index)){
                return scenario;
            }
        }
        return null;
    }

    public static Scenario getByPredictionJobTime(LocalDateTime predictionJobTime){
        for (Scenario scenario : Scenario.values()) {
            if(scenario.predictionJobTime.equals(predictionJobTime)){
                return scenario;
            }
        }
        return null;
    }

    public static Scenario getByTitle(String title){
        for (Scenario scenario : Scenario.values()) {
            if(scenario.title.equals(title)){
                return scenario;
            }
        }
        return null;
    }

    public static Scenario getByTime(LocalDateTime time){
        for (Scenario scenario : Scenario.values()) {
            if(scenario.time.equals(time)){
                return scenario;
            }
        }
        return null;
    }

    public LocalDateTime getPredictionJobTime() {
        return predictionJobTime;
    }

    public Integer getIndex() {
        return index;
    }

    public String getTitle() {
        return title;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getDataCenterName() {
        return dataCenterName;
    }

	public String fileAlgorithm() {
		return "/src/main/resources/scenarios/" + fileAlgorithm;
	}

	public String getFileInputData() {
		return "/data/" + fileInputData;
	}

	public String getFileName() {
		return  fileInputData;
	}

}
