package ro.tuc.dsrl.catalyst.model.utils;

import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class EnergyArraysUtility {

    private static final int INTRA_DAY_PERIOD = 4;
    public static final double DEFAULT_UNMONIIORED_VALUE = -0.00001;

    private EnergyArraysUtility() {

    }

//    public static List<Double> populateIntraDayArray(Object[][] values, int startHour) {
//        List<Double> valuesToReturn = new ArrayList<Double>(Collections.nCopies(PredictionGranularity.INTRADAY.getNoOutputs(), DEFAULT_UNMONIIORED_VALUE));
//        for (Object[] row : values) {
//            int index = 2 * ((Integer) row[4] - startHour) + (Integer) row[5];
//            valuesToReturn.set(index, (Double) row[0]);
//        }
//
//        return valuesToReturn;
//    }
//
//    public static List<Double> populateIntraDayArrayFromDayAyead(Object[][] values) {
//        List<Double> valuesToReturn = new ArrayList<Double>(Collections.nCopies(PredictionGranularity.INTRADAY.getNoOutputs(), DEFAULT_UNMONIIORED_VALUE));
//        for (Object[] row : values) {
//            int index = ((Integer) row[4]) % INTRA_DAY_PERIOD;
//            valuesToReturn.set(2 * index, (Double) row[0]);
//            valuesToReturn.set(1 + 2 * index, (Double) row[0]);
//        }
//
//        return valuesToReturn;
//    }
//
//    public static List<Double> populateDayAheadArray(Object[][] values) {
//        List<Double> valuesToReturn = new ArrayList<Double>(Collections.nCopies(PredictionGranularity.DAYAHEAD.getNoOutputs(), DEFAULT_UNMONIIORED_VALUE));
//        for (Object[] row : values) {
//            valuesToReturn.set((Integer) row[4], (Double) row[0]);
//        }
//        return valuesToReturn;
//    }
//
//    public static List<Double> populateRealtimeArray(Object[][] values, int startHour, int noOutputs) {
//        List<Double> valuesToReturn = new ArrayList<Double>(Collections.nCopies(noOutputs, DEFAULT_UNMONIIORED_VALUE));
//        for (Object[] row : values) {
//            int index = 12 * ((Integer) row[4] - startHour) + (Integer) row[5];
//            valuesToReturn.set(index, (Double) row[0]);
//        }
//        return valuesToReturn;
//    }


    //   public static List<Double> populateIntraDayArray(List<FlexibilityOrderAndDCResponseDTO> values, int startHour) {
//        List<Double> valuesToReturn = new ArrayList<>(Collections.nCopies(FlexibilityOrderAndDCResponse.INTRA_DAY_PERIOD_SLOTS.getRate(), 0.0));
//        for (FlexibilityOrderAndDCResponseDTO row : values) {
//            int index = 2 * (row.getHour() - startHour) + Integer.parseInt(row.getMinute().toString());
//            valuesToReturn.set(index, (Double) row.getAmount());
//        }
//
//        return valuesToReturn;
//    }
//
//    public static List<Double> populateUsingGranularity(Integer granularity, List<FlexibilityOrderAndDCResponseDTO> values, int startHour) {
//        List<Double> valuesToReturn = new ArrayList<>(Collections.nCopies(granularity, DEFAULT_UNMONIIORED_VALUE));
//
//        int gran = granularity == 24 ? 1 : 2;
//
//        for (FlexibilityOrderAndDCResponseDTO row : values) {
//            int index = gran * (row.getHour() - startHour) + Integer.parseInt(row.getMinute().toString());
//            valuesToReturn.set(index, (Double) row.getAmount());
//            if(gran == 2)
//                valuesToReturn.set(index + 1, row.getAmount());
//        }
//
//        return valuesToReturn;
//    }
//    public static List<Double> populateIntraDayArrayFromDayAyead(List<FlexibilityOrderAndDCResponseDTO> values) {
//        List<Double> valuesToReturn = new ArrayList(Collections.nCopies(FlexibilityOrderAndDCResponse.INTRA_DAY_PERIOD_SLOTS.getRate(), 0.0));
//        for (FlexibilityOrderAndDCResponseDTO row : values) {
//            int index = ((Integer) row.getHour()) % INTRA_DAY_PERIOD;
//            valuesToReturn.set(2 * index, (Double) row.getAmount());
//            valuesToReturn.set(1 + 2 * index, (Double) row.getAmount());
//        }
//
//        return valuesToReturn;
//    }
//
//    public static List<Double> populateArray(List<FlexibilityOrderAndDCResponseDTO> values, int startHour, Integer rate) {
//        List<Double> valuesToReturn = new ArrayList<>(Collections.nCopies(rate, 0.0));
//        for (FlexibilityOrderAndDCResponseDTO row : values) {
//            int index = 2 * (row.getHour() - startHour) + Integer.parseInt(row.getMinute().toString());
//            valuesToReturn.set(index, (Double) row.getAmount());
//        }
//
//        return valuesToReturn;
//    }
//
//    public static List<Double> populateArrayFromDayAhead(List<FlexibilityOrderAndDCResponseDTO> values, int startHour, Integer rate, Timeframe type) {
//        if(type == Timeframe.INTRA_DAY)
//            return populateIntraDayArrayFromDayAyead(values);
//        else
//            return populateArray(values, startHour, rate);
//    }

    public static List<Double> computeLostFactorBatteryCharge(List<Double> chargeBattery, double factor) {
        List<Double> valuesToReturn = new ArrayList<>();
        for (Double charge : chargeBattery) {
            double value = charge * factor;
            valuesToReturn.add(value);
        }
        return valuesToReturn;
    }

    public static List<Double> computeLostFactorBatteryDischarge(List<Double> dischargeBattery, double factor) {
        List<Double> valuesToReturn = new ArrayList<>();
        for (Double charge : dischargeBattery) {
            double value = charge * factor;
            valuesToReturn.add(value);
        }
        return valuesToReturn;
    }

    public static List<Double> computeLostFactorTesCharge(List<Double> chargeTes, double factor) {
        List<Double> valuesToReturn = new ArrayList<>();
        for (Double charge : chargeTes) {
            double value = charge * factor;
            valuesToReturn.add(value);
        }
        return valuesToReturn;
    }

    public static List<Double> computeLostFactorTesDischarge(List<Double> dischargeTes, double factor) {
        List<Double> valuesToReturn = new ArrayList<>();
        for (Double charge : dischargeTes) {
            double value = charge * factor;
            valuesToReturn.add(value);
        }
        return valuesToReturn;
    }

    public static List<Double> duplicateToRealtimeValues(List<Double> values, PredictionGranularity granularity) {
        int duplicateSlots = granularity.getNoOutputs() / values.size();
        List<Double> toReturn = new ArrayList<>();
        for (Double value : values) {
            for (int i = 0; i <= duplicateSlots; i++) {
                toReturn.add(value);
            }
        }
        return toReturn;
    }

    public static List<Double> revertCoolingValuesFromActions(List<Double> cooling, List<Double> chargeTes,
                                                              List<Double> dischargeTes) {
        assertTrue("cooling arrays " + cooling.size(), (chargeTes.size() == cooling.size()));
        assertTrue("cooling arrays " + chargeTes.size(), (chargeTes.size() == dischargeTes.size()));
        List<Double> valuesToReturn = new ArrayList<>();
        for (int i = 0; i < cooling.size(); i++) {
            valuesToReturn.add(cooling.get(i) - chargeTes.get(i) + Math.abs(dischargeTes.get(i)));
        }
        return valuesToReturn;
    }

    public static List<Double> mergeDRSignalWithBaseline(List<Double> drSignal, List<Double> baseline) {
        assertTrue("add intraday arrays " + drSignal.size(), (drSignal.size() == baseline.size()));
        List<Double> values = new ArrayList<>();
        for (int i = 0; i < drSignal.size(); i++) {
            double dr = drSignal.get(i);
            if (dr > 1) {
                values.add(dr);
            } else {
                values.add(baseline.get(i));
            }
        }
        return values;
    }

    public static List<Double> computeDeliveredFlexibility(List<Double> baseline, List<Double> dcOptimized){
        assertTrue("computeDeliveredFlexibility " + baseline.size(), (baseline.size() == dcOptimized.size()));
        List<Double> values = new ArrayList<>();
        for (int i = 0; i < baseline.size(); i++) {
            values.add(baseline.get(i) - dcOptimized.get(i));
        }
        return values;
    }

//    public static List<Double> retrieveHalfHourMean(List<Double> values) {
//        List<Double> valuesToReturn = new ArrayList<>();
//        double halfHourSum = values.get(0);
//        for (int i = 1; i < values.size(); i++) {
//            halfHourSum += values.get(i);
//            if ((i + 1) % 6 == 0) {
//                valuesToReturn.add(halfHourSum / 6.0);
//                halfHourSum = 0.0;
//            }
//
//        }
//        return valuesToReturn;
//    }
//
//    public static List<Double> retrieveHourMean(List<Double> values) {
//        List<Double> valuesToReturn = new ArrayList<>();
//
//        double sum = values.get(0);
//        for (int i = 1; i < values.size(); i++) {
//            if (i % 2 == 1) {
//                valuesToReturn.add(sum / 2.0);
//                sum = 0.0;
//            }
//        }
//
//        return valuesToReturn;
//    }
}
