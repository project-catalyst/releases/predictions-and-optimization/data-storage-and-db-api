package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.util.UUID;

public class ValueTypeDTO implements Serializable {

    private UUID id;
    private String valueType;

    public ValueTypeDTO(){

    }

    public ValueTypeDTO(UUID id,
                        String valueType) {
        this.id = id;
        this.valueType = valueType;
    }

    public UUID getId() {
        return id;
    }

    public String getValueType() {
        return valueType;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }
}
