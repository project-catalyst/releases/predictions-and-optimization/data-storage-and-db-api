package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

public class TrainedModelDTO implements Serializable {

    private UUID id;
    private String scenario;
    private String predictionType;
    private String algorithmType;
    private String componentType;
    private String pathOnDisk;
    private String status;
    private LocalDateTime startTime;

    public TrainedModelDTO(){

    }

    public TrainedModelDTO(UUID id,
                           String scenario,
                           String predictionType,
                           String algorithmType,
                           String componentType,
                           String pathOnDisk,
                           String status,
                           LocalDateTime startTime) {
        this.id = id;
        this.scenario = scenario;
        this.predictionType = predictionType;
        this.algorithmType = algorithmType;
        this.componentType = componentType;
        this.pathOnDisk = pathOnDisk;
        this.status = status;
        this.startTime = startTime;
    }

    public TrainedModelDTO(String scenario, String predictionType, String algorithmType, String componentType, String pathOnDisk, String status, LocalDateTime startTime) {
        this.scenario = scenario;
        this.predictionType = predictionType;
        this.algorithmType = algorithmType;
        this.componentType = componentType;
        this.pathOnDisk = pathOnDisk;
        this.status = status;
        this.startTime = startTime;
    }

    public UUID getId() {
        return id;
    }

    public String getScenario() {
        return scenario;
    }

    public String getPredictionType() {
        return predictionType;
    }

    public String getAlgorithmType() {
        return algorithmType;
    }

    public String getComponentType() {
        return componentType;
    }

    public String getPathOnDisk() {
        return pathOnDisk;
    }

    public String getStatus() {
        return status;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public void setPredictionType(String predictionType) {
        this.predictionType = predictionType;
    }

    public void setAlgorithmType(String algorithmType) {
        this.algorithmType = algorithmType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public void setPathOnDisk(String pathOnDisk) {
        this.pathOnDisk = pathOnDisk;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TrainedModelDTO{" +
                "id=" + id +
                ", scenario='" + scenario + '\'' +
                ", predictionType='" + predictionType + '\'' +
                ", algorithmType='" + algorithmType + '\'' +
                ", componentType='" + componentType + '\'' +
                ", pathOnDisk='" + pathOnDisk + '\'' +
                ", status='" + status + '\'' +
                ", startTime=" + startTime +
                '}';
    }
}
