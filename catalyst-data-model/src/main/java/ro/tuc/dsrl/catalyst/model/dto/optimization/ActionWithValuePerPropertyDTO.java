//package ro.tuc.dsrl.catalyst.model.dto.optimization;
//
//import java.time.LocalDateTime;
//import java.util.UUID;
//
//public class ActionWithValuePerPropertyDTO {
//
//    private UUID actionInstanceId;
//    private LocalDateTime startTime;
//    private LocalDateTime endTime;
//    private String actionType;
//    private String actionProperty;
//    private Double value;
//
//    public ActionWithValuePerPropertyDTO() {
//    }
//
//    public ActionWithValuePerPropertyDTO(UUID actionInstanceId, LocalDateTime startTime, LocalDateTime endTime, String actionType, String actionProperty, Double value) {
//        this.actionInstanceId = actionInstanceId;
//        this.startTime = startTime;
//        this.endTime = endTime;
//        this.actionType = actionType;
//        this.actionProperty = actionProperty;
//        this.value = value;
//    }
//
//    public UUID getActionInstanceId() {
//        return actionInstanceId;
//    }
//
//    public void setActionInstanceId(UUID actionInstanceId) {
//        this.actionInstanceId = actionInstanceId;
//    }
//
//    public LocalDateTime getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(LocalDateTime startTime) {
//        this.startTime = startTime;
//    }
//
//    public LocalDateTime getEndTime() {
//        return endTime;
//    }
//
//    public void setEndTime(LocalDateTime endTime) {
//        this.endTime = endTime;
//    }
//
//    public String getActionType() {
//        return actionType;
//    }
//
//    public void setActionType(String actionType) {
//        this.actionType = actionType;
//    }
//
//    public String getActionProperty() {
//        return actionProperty;
//    }
//
//    public void setActionProperty(String actionProperty) {
//        this.actionProperty = actionProperty;
//    }
//
//    public Double getValue() {
//        return value;
//    }
//
//    public void setValue(Double value) {
//        this.value = value;
//    }
//}
