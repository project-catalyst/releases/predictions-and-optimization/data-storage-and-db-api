package ro.tuc.dsrl.catalyst.model.dto;

import java.io.Serializable;

public class ThermalDTO  implements Serializable {

    private long startTime;
    private double temperatureRack1;
    private double temperatureRack2;
    private double temperatureInputAir;
    private double airFlow;
    private double temperatureRoomInitial;

    private ThermalDTO()
    {

    }

    public ThermalDTO(double temperatureRack1, double temperatureRack2, double temperatureInputeAir, double airFlow, double temperatureRoomInitial) {
        this.temperatureRack1 = temperatureRack1;
        this.temperatureRack2 = temperatureRack2;
        this.temperatureInputAir = temperatureInputeAir;
        this.airFlow = airFlow;
        this.temperatureRoomInitial = temperatureRoomInitial;
    }

    public ThermalDTO(long startTime, double temperatureRack1, double temperatureRack2, double temperatureInputAir, double airFlow, double temperatureRoomInitial) {
        this.startTime = startTime;
        this.temperatureRack1 = temperatureRack1;
        this.temperatureRack2 = temperatureRack2;
        this.temperatureInputAir = temperatureInputAir;
        this.airFlow = airFlow;
        this.temperatureRoomInitial = temperatureRoomInitial;
    }

    public double getTemperatureRack1() {
        return temperatureRack1;
    }

    public void setTemperatureRack1(double temperatureRack1) {
        this.temperatureRack1 = temperatureRack1;
    }

    public double getTemperatureRack2() {
        return temperatureRack2;
    }

    public void setTemperatureRack2(double temperatureRack2) {
        this.temperatureRack2 = temperatureRack2;
    }

    public double getTemperatureInputAir() {
        return temperatureInputAir;
    }

    public void setTemperatureInputAir(double temperatureInputAir) {
        this.temperatureInputAir = temperatureInputAir;
    }

    public double getAirFlow() {
        return airFlow;
    }

    public void setAirFlow(double airFlow) {
        this.airFlow = airFlow;
    }

    public double getTemperatureRoomInitial() {
        return temperatureRoomInitial;
    }

    public void setTemperatureRoomInitial(double temperatureRoomInitial) {
        this.temperatureRoomInitial = temperatureRoomInitial;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
