package ro.tuc.dsrl.catalyst.model.enums;

public enum DataScenario {
    PAPER("PAPER"),
    POZNAN("POZNAN"),
    PILOT("PILOT");

    private final String scenario;


    public String getScenario() {
        return scenario;
    }

    DataScenario(String scenario) {
        this.scenario = scenario;
    }

    public static DataScenario getDataScenario(String scenario) {
        for (DataScenario dS : values()) {
            if (dS.scenario.equalsIgnoreCase(scenario)) {
                return dS;
            }
        }

        return null;
    }
}
