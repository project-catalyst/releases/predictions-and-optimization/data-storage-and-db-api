package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.time.LocalDateTime;
import java.util.UUID;

public class OptimizationActionDTO {

    private UUID id;
    private String type;
    private long startTime;
    private long endTime;
    private Double amount;
    private Double thermalAmount;
    private long moveFromDate;
    private Double movePercentage;
    private boolean active;

    public OptimizationActionDTO(){

    }
    public OptimizationActionDTO(UUID id, String type, long startTime, long endTime, Double amount, Double thermalAmount, long moveToDate, Double movePercentage) {
        this.id = id;
        this.type = type;
        this.startTime = startTime;
        this.endTime = endTime;
        this.amount = amount;
        this.thermalAmount =thermalAmount;
        this.moveFromDate = moveToDate;
        this.movePercentage = movePercentage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public long getMoveFromDate() {
        return moveFromDate;
    }

    public void setMoveFromDate(long moveFromDate) {
        this.moveFromDate = moveFromDate;
    }

    public Double getMovePercentage() {
        return movePercentage;
    }

    public void setMovePercentage(Double movePercentage) {
        this.movePercentage = movePercentage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Double getThermalAmount() {
        return thermalAmount;
    }

    public void setThermalAmount(Double thermalAmount) {
        this.thermalAmount = thermalAmount;
    }

    @Override
    public String toString() {
        return "Action{" +
                "type='" + type + '\'' +
                ", startTime=" + startTime +
                ", amount=" + amount + '}';
    }
}
