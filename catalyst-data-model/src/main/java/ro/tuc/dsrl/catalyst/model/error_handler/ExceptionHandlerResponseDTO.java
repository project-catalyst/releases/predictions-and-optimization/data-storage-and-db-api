package ro.tuc.dsrl.catalyst.model.error_handler;

import java.util.Date;
import java.util.List;

public class ExceptionHandlerResponseDTO {

    private Date timestamp;
    private int status;
    private String error;
    private String message;
    private String path;
    private String resource;
    private List<String> details;

    public ExceptionHandlerResponseDTO(String resource,
                                       String error,
                                       int status,
                                       String message,
                                       List<String> details,
                                       String path) {
        this.timestamp = new Date();
        this.resource = resource;
        this.error = error;
        this.status = status;
        this.message = message;
        this.details = details;
        this.path = path;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String error) {
        this.resource = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
