package ro.tuc.dsrl.catalyst.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

public class AncillaryServiceResultsDTO implements Serializable {

    private UUID id;
    private LocalDateTime recordTime;
    private Double adaptedProfile;

    public AncillaryServiceResultsDTO(UUID id, LocalDateTime recordTime, Double adaptedProfile) {
        this.id = id;
        this.recordTime = recordTime;
        this.adaptedProfile = adaptedProfile;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(LocalDateTime recordTime) {
        this.recordTime = recordTime;
    }

    public Double getAdaptedProfile() {
        return adaptedProfile;
    }

    public void setAdaptedProfile(Double adaptedProfile) {
        this.adaptedProfile = adaptedProfile;
    }
}
