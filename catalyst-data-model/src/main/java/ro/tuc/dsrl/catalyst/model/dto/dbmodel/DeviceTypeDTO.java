package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.util.UUID;

public class DeviceTypeDTO implements Serializable {

    private UUID id;
    private String typeOfDevice;

    public DeviceTypeDTO(){

    }

    public DeviceTypeDTO(UUID id,
                         String typeOfDevice) {
        this.id = id;
        this.typeOfDevice = typeOfDevice;
    }

    public UUID getId() {
        return id;
    }

    public String getType() {
        return typeOfDevice;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setType(String type) {
        this.typeOfDevice = type;
    }
}
