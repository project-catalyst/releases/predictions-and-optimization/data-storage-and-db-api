//package ro.tuc.dsrl.catalyst.model.enums;
//
//public enum ActionPropertyName {
//
//    DISCHARGE_BATTERY_AMOUNT("Discharge Battery Amount", "dicharge_battery_amount"),
//    CHARGE_BATTERY_AMOUNT("Charge Battery Amount", "charge_battery_amount"),
//    DISCHARGE_TES_AMOUNT("Discharge TES Amount", "dicharge_TES_amount"),
//    CHARGE_TES_AMOUNT("Charge TES Amount", "charge_TES_amount"),
//    COOLING_AMOUNT("Cooling amount", "cooling_amount"),
//    SDTW_AMOUNT("SDTW Amount", "sdtw_amount"),
//    SDTW_START("SDTW Start Time", "sdtw_start_time"),
//    SDTW_END("SDTW End Time", "sdtw_end_time"),
//    SDTW_PERCENTAGE("SDTW Percentage", "sdtw_percentage"),
//    RELOCATE_AMOUNT("Relocate amount", "relocate_amount"),
//    RELOCATE_PERCENTAGE("Relocate Percentage", "reallocation_percentage"),
//    THERMAL_SELF_HEAT_AMOUNT("Thermal self heat amount", "thermal_self_heat_amount"),
//    HOST_WORKLOAD_AMOUNT("Host workload amount", "host_workload_amount"),
//    BUY_ENERGY_AMOUNT("Buy Energy (KWh)",  "buy_energy"),
//    SELL_ENERGY_AMOUNT("Sell Energy (KWh)",  "sell_energy"),
//    BUY_HEAT_AMOUNT("Buy Heat (KWh)", "buy_heat"),
//    SELL_HEAT_AMOUNT("Sell Heat (KWh)", "sell_heat"),
//    SELL_FLEXIBILITY_AMOUNT("Sell Flexibility (KWh)", "sell_flexibility");
//
//    private String name;
//    private String shortName;
//
//    public static ActionPropertyName getByName(String name){
//        for (ActionPropertyName actionPropertyName : ActionPropertyName.values()) {
//            if(actionPropertyName.name.equals(name)){
//                return actionPropertyName;
//            }
//        }
//        return null;
//    }
//
//    public static ActionPropertyName getByShortName(String shortName){
//        for (ActionPropertyName actionPropertyName : ActionPropertyName.values()) {
//            if(actionPropertyName.shortName.equals(shortName)){
//                return actionPropertyName;
//            }
//        }
//        return null;
//    }
//
//    ActionPropertyName(String name, String shortName){
//        this.name = name;
//        this.shortName = shortName;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getShortName() {
//        return shortName;
//    }
//}
