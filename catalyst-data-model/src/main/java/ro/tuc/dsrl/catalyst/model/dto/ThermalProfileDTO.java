package ro.tuc.dsrl.catalyst.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ThermalProfileDTO implements Serializable {

    private List<EnergySampleDTO> temperature;

    public ThermalProfileDTO()
    {
        temperature = new ArrayList<EnergySampleDTO>();
    }

    public ThermalProfileDTO(List<EnergySampleDTO> list){
        this.temperature = list;
    }

    public List<EnergySampleDTO> getTemperature() {
        return temperature;
    }

    public void setTemperature(List<EnergySampleDTO> temperature) {
        this.temperature = temperature;
    }
}
