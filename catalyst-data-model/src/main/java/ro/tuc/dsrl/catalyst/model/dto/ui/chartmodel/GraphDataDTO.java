//package ro.tuc.dsrl.catalyst.model.dto.ui.chartmodel;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//
//
//
//public class GraphDataDTO implements Serializable {
//
//    private int size;
//    private List<LabeledValues> data;
//
//    public GraphDataDTO(int size) {
//        this.size = size;
//        this.data = new ArrayList<>();
//    }
//
//    public int getSize() {
//        return size;
//    }
//
//    public void setSize(int size) {
//        this.size = size;
//    }
//
//    public List<LabeledValues> getData() {
//        return data;
//    }
//
//    public void setData(List<LabeledValues> data) {
//        this.data = data;
//    }
//
//    public void addLabeledValues(LabeledValues values){
//        //assertEquals(values.getLabel() + values.getValues().size(), values.getValues().size(), size);
//        data.add(values);
//    }
//}