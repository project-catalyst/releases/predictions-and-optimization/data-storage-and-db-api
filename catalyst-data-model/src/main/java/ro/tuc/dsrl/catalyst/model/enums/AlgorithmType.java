package ro.tuc.dsrl.catalyst.model.enums;

public enum AlgorithmType {

    MLP("Multi Layer Perceptron", "MLP"),
    LSTM("Long Short-Term Memory", "LSTM"),
    MARKET("MARKET_CLEARING", "MARKET"),
    ENSEMBLE("ENSEMBLE", "ENSEMBLE"),
    OPTIMIZATION("OPTIMIZATION" , "OPTIMIZATION");

    private final String name;
    private final String shortName;

    AlgorithmType(String name, String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }
}
