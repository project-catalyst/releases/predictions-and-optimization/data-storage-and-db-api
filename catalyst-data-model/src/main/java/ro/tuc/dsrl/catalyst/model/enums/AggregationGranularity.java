package ro.tuc.dsrl.catalyst.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum AggregationGranularity {

    MINUTES_30(30),
    HOUR(60);

    private final int granularity; // in minutes
    private static Map map = new HashMap<>();

    AggregationGranularity(int granularity)
    {
        this.granularity = granularity;
    }

    public int getGranularity() {
        return granularity;
    }

    public static AggregationGranularity setGranularity(int granularity) {

        if (granularity == 30) {
            return AggregationGranularity.MINUTES_30;
        } else if (granularity == 60) {
            return AggregationGranularity.HOUR;
        } else {
            return null;
        }
    }

    public long numberOfEntriesFor(AggregationGranularity smUnit) {
        if (this.granularity >= smUnit.granularity) {
            return this.granularity / smUnit.granularity;
        }
        return -1;
    }

    static {
        for (AggregationGranularity aggregationGranularity : AggregationGranularity.values()) {
            map.put(aggregationGranularity.granularity, aggregationGranularity);
        }
    }

    public static AggregationGranularity fromInt(int granularity) {
        return (AggregationGranularity) map.get(granularity);
    }
}
