package ro.tuc.dsrl.catalyst.model.enums;

public enum FlexibilityOrderAndDCResponse {

    BATTERY_CHARGE_RATE("BATTERY_CHARGE_RATE", 1),
    BATTERY_DISCHARGE_RATE("BATTERY_DISCHARGE_RATE", 1),
    TES_CHARGE_RATE("TES_CHARGE_RATE", 1),
    TES_DISCHARGE_RATE("TES_DISCHARGE_RATE", 1),
    INTRA_DAY_PERIOD_SLOTS("INTRA_DAY_PERIOD_SLOTS", 8),
    REALTIME_PERIOD_SLOTS("REALTIME_PERIOD_SLOTS", 48),
    DAY_AHEAD_PERIOD_SLOTS("DAY_AHEAD_PERIOD_SLOTS", 24),
    INTRA_DAY_SAMPLE("INTRA_DAY_SAMPLE", 30),
    DAY_AHEAD_SAMPLE("DAY_AHEAD_SAMPLE", 60);
    //    RT("real_time_workload"),
    //    DT("delay_tolerant_workload"),
    //    DAY_AHEAD_TIMEFRAME("day_ahead"),
    //    AS_TIMEFRAME("as");
    private final String name;
    private final Integer rate;

    FlexibilityOrderAndDCResponse(String name, Integer rate) {
        this.name = name;
        this.rate = rate;
    }

    public Integer getRate() {
        return rate;
    }

    public static FlexibilityOrderAndDCResponse getByName(String name) {
        for(FlexibilityOrderAndDCResponse chargeDischargeRate: FlexibilityOrderAndDCResponse.values()){
            if(chargeDischargeRate.name.equals(name)){
                return chargeDischargeRate;
            }
        }
        return null;
    }
}
