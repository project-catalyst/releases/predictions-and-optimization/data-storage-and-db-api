//package ro.tuc.dsrl.catalyst.model.dto;
//
//import com.fasterxml.jackson.annotation.JsonSubTypes;
//import com.fasterxml.jackson.annotation.JsonTypeInfo;
//
//import java.util.UUID;
//@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
//@JsonSubTypes({ @JsonSubTypes.Type(value = RelocateActionDTO.class),
//        @JsonSubTypes.Type(value = ShiftDelayTolerantActionDTO.class) })
//public class ActionDTO extends BaseActionDTO {
//
//    private String destinationDC;
//
//    public ActionDTO() {
//    }
//
//    public ActionDTO(UUID id, long start, long end, String name, Double value, boolean active, String destinationDC) {
//        super(id, start, end, name, value, active);
//        this.destinationDC = destinationDC;
//    }
//
//    public String getDestinationDC() {
//        return destinationDC;
//    }
//
//    public void setDestinationDC(String destinationDC) {
//        this.destinationDC = destinationDC;
//    }
//}
