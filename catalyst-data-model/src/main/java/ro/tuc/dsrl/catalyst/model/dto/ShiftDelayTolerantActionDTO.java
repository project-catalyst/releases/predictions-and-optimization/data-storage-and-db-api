//package ro.tuc.dsrl.catalyst.model.dto;
//
//
//import java.util.UUID;
//
//public class ShiftDelayTolerantActionDTO extends ActionDTO {
//
//    private long sdtwStartTime;
//    private long sdtwEndTime;
//    private double sdtwPercentage;
//
//    public ShiftDelayTolerantActionDTO() {
//        super();
//    }
//
//    public ShiftDelayTolerantActionDTO(UUID id, long startTime, long endTime, String name, boolean active, String destinationDC){
//        this.setId(id);
//        this.setStart(startTime);
//        this.setEnd(endTime);
//        this.setName(name);
//        this.setActive(active);
//        this.setDestinationDC(destinationDC);
//
//    }
//
//    public ShiftDelayTolerantActionDTO(UUID id, long start, long end, String name, Double value, boolean active, String destinationDC, long sdtwStartTime, long sdtwEndTime, double sdtwPercentage) {
//        super(id, start, end, name, value, active, destinationDC);
//        this.sdtwStartTime = sdtwStartTime;
//        this.sdtwEndTime = sdtwEndTime;
//        this.sdtwPercentage = sdtwPercentage;
//    }
//
//    public long getSdtwStartTime() {
//        return sdtwStartTime;
//    }
//
//    public void setSdtwStartTime(long sdtwStartTime) {
//        this.sdtwStartTime = sdtwStartTime;
//    }
//
//    public long getSdtwEndTime() {
//        return sdtwEndTime;
//    }
//
//    public void setSdtwEndTime(long sdtwEndTime) {
//        this.sdtwEndTime = sdtwEndTime;
//    }
//
//    public double getSdtwPercentage() {
//        return sdtwPercentage;
//    }
//
//    public void setSdtwPercentage(double sdtwPercentage) {
//        this.sdtwPercentage = sdtwPercentage;
//    }
//}
