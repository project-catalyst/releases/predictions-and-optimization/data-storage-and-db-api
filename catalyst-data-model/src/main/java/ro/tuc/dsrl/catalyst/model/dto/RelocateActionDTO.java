//package ro.tuc.dsrl.catalyst.model.dto;
//
//import java.util.UUID;
//
//public class RelocateActionDTO extends ActionDTO {
//
//    private Double relocatePercentage;
//
//    public RelocateActionDTO() {
//    }
//
//    public RelocateActionDTO(UUID id, long start, long end, String name, boolean active, String destination) {
//        this.setId(id);
//        this.setStart(start);
//        this.setEnd(end);
//        this.setName(name);
//        this.setActive(active);
//        this.setDestinationDC(destination);
//    }
//
//    public RelocateActionDTO(UUID id, long start, long end, String name, Double value, boolean active, Double relocatePercentage, String destination) {
//        super(id, start, end, name, value, active, destination);
//        this.relocatePercentage = relocatePercentage;
//    }
//
//    public RelocateActionDTO(Double relocatePercentage) {
//        this.relocatePercentage = relocatePercentage;
//    }
//
//    public Double getRelocatePercentage() {
//        return relocatePercentage;
//    }
//
//    public void setRelocatePercentage(Double relocatePercentage) {
//        this.relocatePercentage = relocatePercentage;
//    }
//}
