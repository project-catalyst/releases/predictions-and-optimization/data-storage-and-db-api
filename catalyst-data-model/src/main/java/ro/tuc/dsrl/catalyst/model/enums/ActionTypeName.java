package ro.tuc.dsrl.catalyst.model.enums;

import ro.tuc.dsrl.geyser.datamodel.actions.*;

public enum ActionTypeName {

    DISCHARGE_TES(DischargeTes.ACTION_TYPE, "dischargeTes"),
    CHARGE_TES(ChargeTes.ACTION_TYPE, "chargeTes"),
    DISCHARGE_BATTERY(DischargeBattery.ACTION_TYPE, "dischargeBattery"),
    CHARGE_BATTERY(ChargeBattery.ACTION_TYPE, "chargeBattery"),
    DYNAMIC_ADJUSTMENT_OF_COOLING_INTENSITY(DynamicAdjustmentOfCoolingIntensity.ACTION_TYPE, "dynamicAdjustmentCoolingIntensity"),
    SHIFT_DELAY_TOLERANT_WORKLOAD(ShiftDelayTolerantWorkload.ACTION_TYPE, "shiftDelayTolerantWorkload"),
    RELOCATE_WORKLOAD(WorkloadRelocation.ACTION_TYPE, "relocateWorkload"),
   // THERMAL_SELF_HEAT("Thermal self heat (KWh)", "thermalSelfHeat", "thermal_self_heat_amount"),
    HOST_WORKLOAD(WorkloadHosting.ACTION_TYPE, "hostWorkload"),
    BUY_ENERGY(BuyEnergy.ACTION_TYPE, "buyEnergy"),
    SELL_ENERGY(SellEnergy.ACTION_TYPE, "sellEnergy"),
    BUY_HEAT(BuyHeat.ACTION_TYPE,"buyHeat"),
    SELL_HEAT(SellHeat.ACTION_TYPE, "sellHeat"),
    SELL_FLEXIBILITY(SellFlexibility.ACTION_TYPE, "sellFlexibility"),
    NONE("NONE", "-");


    private String name;
    private String shortName;

    ActionTypeName(String name, String shortName){
        this.name = name;
        this.shortName = shortName;
    }

    public static ActionTypeName getByShortName(String shortName){

        for (ActionTypeName actionTypeName : ActionTypeName.values()) {
            if(actionTypeName.shortName.equals(shortName)){
                return actionTypeName;
            }
        }
        return NONE;
    }

    public String getName() {
        return name;
    }
}
