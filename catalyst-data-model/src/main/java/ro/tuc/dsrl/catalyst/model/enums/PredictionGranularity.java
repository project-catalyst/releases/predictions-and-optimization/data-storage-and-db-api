package ro.tuc.dsrl.catalyst.model.enums;

public enum PredictionGranularity {

    DAYAHEAD("DAYAHEAD", 24, 60, 24, 24, Timeframe.DAY_AHEAD),
    INTRADAY("INTRADAY", 4, 30, 8, 8, Timeframe.INTRA_DAY),
    REALTIME_INTRADAY_FRAME("REALTIME_INTRADAY_FRAME", 4,5, 48, 48, Timeframe.REAL_TIME),
    NEAR_REAL_TIME("NEAR_REAL_TIME", 24, 60, 24, 1, Timeframe.NONE),
    REALTIME_DAYAHEAD_FRAME("REALTIME_DAYAHEAD_FRAME", 24, 5, 288, 288, Timeframe.REAL_TIME),
    NONE("", 0, 0, 0, 0, Timeframe.NONE);

    private final String granularity;
    private final Integer noHours;
    private final Integer sampleFrequencyMin;
    private final Integer noInputs;
    private final Integer noOutputs;
    private final Timeframe optPlanTimeframe;

    PredictionGranularity(String granularity,
                          Integer noHours,
                          Integer sampleFrequencyMin,
                          Integer noInputs,
                          Integer noOutputs,
                          Timeframe optPlanTimeframe) {

        this.granularity = granularity;
        this.noHours = noHours;
        this.sampleFrequencyMin = sampleFrequencyMin;
        this.noInputs = noInputs;
        this.noOutputs = noOutputs;
        this.optPlanTimeframe = optPlanTimeframe;
    }

    public String getTimeframeLabel(){
        return this.optPlanTimeframe.getName();
    }
    public Timeframe getTimeframe(){
        return this.optPlanTimeframe;
    }

    public static PredictionGranularity setGranularity(String granularityPrediction) {
        switch (granularityPrediction) {
            case "dayahead":
                return PredictionGranularity.DAYAHEAD;
            case "intraday":
                return PredictionGranularity.INTRADAY;
            case "NEAR_REAL_TIME":
                return PredictionGranularity.NEAR_REAL_TIME;
            case "realtime-id":
                return PredictionGranularity.REALTIME_INTRADAY_FRAME;
            case "realtime-da":
                return PredictionGranularity.REALTIME_DAYAHEAD_FRAME;
            default:
                return PredictionGranularity.NONE;
        }
    }

    public static String setType(PredictionGranularity granularityPrediction) {
        switch (granularityPrediction) {
            case DAYAHEAD:
                return "day_ahead";
            case INTRADAY:
            case REALTIME_INTRADAY_FRAME:
            case NEAR_REAL_TIME:
            case REALTIME_DAYAHEAD_FRAME:
                return "intra day";
            default:
                return "";
        }
    }


    public String getGranularity(){
        return granularity;
    }

    public Integer getNoHours(){
        return noHours;
    }

    public Integer getSampleFrequencyMin() {
        return sampleFrequencyMin;
    }

    public Integer getNoInputs() {
        return noInputs;
    }

    public Integer getNoOutputs() {
        return noOutputs;
    }
}
