package ro.tuc.dsrl.catalyst.model.dto;

import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;

import java.util.ArrayList;
import java.util.List;

public class EnergySamplesForMeasurementDTO {

    private MeasurementType measurementType;

    private List<Double> values;

    public EnergySamplesForMeasurementDTO() {
        values = new ArrayList<>();
    }

    public EnergySamplesForMeasurementDTO(MeasurementType measurementType, List<Double> values) {
        this.measurementType = measurementType;
        this.values = values;
    }

    public MeasurementType getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(MeasurementType measurementType) {
        this.measurementType = measurementType;
    }

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "EnergySamplesForMeasurementDTO{" +
                "measurementType=" + measurementType +
                ", values=" + values +
                '}';
    }
}
