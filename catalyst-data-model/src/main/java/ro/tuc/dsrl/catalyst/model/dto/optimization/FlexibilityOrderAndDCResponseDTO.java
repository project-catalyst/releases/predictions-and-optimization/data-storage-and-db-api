//package ro.tuc.dsrl.catalyst.model.dto.optimization;
//
//import ro.tuc.dsrl.catalyst.model.enums.FlexibilityOrderAndDCResponse;
//
//import java.time.LocalDateTime;
//
//public class FlexibilityOrderAndDCResponseDTO {
//
//    private Integer hour;
//    private Integer minute;
//    private Double amount;
//
//    public FlexibilityOrderAndDCResponseDTO(LocalDateTime time, Integer sample, Double amount) {
//        this.hour = time.getHour();
//        this.minute = time.getMinute() / sample;
//        this.amount = amount;
//    }
//
//    public FlexibilityOrderAndDCResponseDTO(Integer hour, Integer minute, Double amount)
//    {
//        this.hour = hour;
//        this.minute = minute;
//        this.amount = amount;
//    }
//
//    public FlexibilityOrderAndDCResponseDTO() {
//    }
//
//    public Integer getHour() {
//        return hour;
//    }
//
//    public void setHour(Integer hour) {
//        this.hour = hour;
//    }
//
//    public Integer getMinute() {
//        return minute;
//    }
//
//    public void setMinute(Integer minute) {
//        this.minute = minute;
//    }
//
//    public Double getAmount() {
//        return amount;
//    }
//
//    public void setAmount(Double amount) {
//        this.amount = amount;
//    }
//}
