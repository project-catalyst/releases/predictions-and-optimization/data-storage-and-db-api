package ro.tuc.dsrl.catalyst.model.enums;

public enum FlexibilityType {

    UPPER("UPPER"),
    LOWER("LOWER"),
    NONE("NONE");

    private final String type;

    FlexibilityType(String type){
        this.type = type;
    }

    public String getType(){
        return type;
    }

    public static FlexibilityType setType(String flexibilityType){
        if (flexibilityType.equals("UPPER")) {
            return FlexibilityType.UPPER;
        } else if(flexibilityType.equals("LOWER")){
            return FlexibilityType.LOWER;
        } else if(flexibilityType.equals("NONE")) {
            return FlexibilityType.NONE;
        }else {
            return null;
        }
    }
}
