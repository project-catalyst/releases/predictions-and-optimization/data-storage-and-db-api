package ro.tuc.dsrl.catalyst.model.dto;

public class ServerRoomDTO {

    private double maxEnergyConsumption;
    private double edPercentage;
    private double maxHostLoad;
    private double maxReallocLoad;
    private double itDT;
    private double itRT;

    public ServerRoomDTO() {
    }

    public ServerRoomDTO(double maxEnergyConsumption, double edPercentage, double maxHostLoad, double maxReallocLoad, double itDT, double itRT) {
        this.maxEnergyConsumption = maxEnergyConsumption;
        this.edPercentage = edPercentage;
        this.maxHostLoad = maxHostLoad;
        this.maxReallocLoad = maxReallocLoad;
        this.itDT = itDT;
        this.itRT = itRT;
    }

    public double getMaxEnergyConsumption() {
        return maxEnergyConsumption;
    }

    public double getEdPercentage() {
        return edPercentage;
    }

    public double getMaxHostLoad() {
        return maxHostLoad;
    }

    public double getMaxReallocLoad() {
        return maxReallocLoad;
    }

    public double getItDT() {
        return itDT;
    }

    public double getItRT() {
        return itRT;
    }

    public void setMaxEnergyConsumption(double maxEnergyConsumption) {
        this.maxEnergyConsumption = maxEnergyConsumption;
    }

    public void setEdPercentage(double edPercentage) {
        this.edPercentage = edPercentage;
    }

    public void setMaxHostLoad(double maxHostLoad) {
        this.maxHostLoad = maxHostLoad;
    }

    public void setMaxReallocLoad(double maxReallocLoad) {
        this.maxReallocLoad = maxReallocLoad;
    }

    public void setItDT(double itDT) {
        this.itDT = itDT;
    }

    public void setItRT(double itRT) {
        this.itRT = itRT;
    }
}

