package ro.tuc.dsrl.catalyst.model.dto;

import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.EnergyType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class EnergyProfileDTO implements Serializable {

    private List<EnergySampleDTO> curve;
    private UUID deviceId;
    private UUID measurementId;
    private AggregationGranularity aggregationGranularity;
    private PredictionGranularity predictionGranularity;
    private PredictionType predictionType;
    private EnergyType energyType;

    public EnergyProfileDTO() {
        this.curve = new ArrayList<>();
    }

    public static EnergyProfileDTO getDefaultInstance(LocalDateTime startTime,
                            PredictionGranularity predictionGranularity) {
        EnergyProfileDTO defaultDTO = new EnergyProfileDTO();
        for (int i = 0; i < predictionGranularity.getNoOutputs(); i++) {
            defaultDTO.curve.add(new EnergySampleDTO(0.0, startTime));
            startTime = startTime.plus(predictionGranularity.getSampleFrequencyMin(), ChronoUnit.MINUTES);
        }
        return  defaultDTO;
    }

    public static EnergyProfileDTO getDefaultInstanceExpanded(LocalDateTime startTime,
                                                              PredictionGranularity predictionGranularity,
                                                              UUID deviceId,
                                                              UUID measurementId,
                                                              String energyType,
                                                              Integer aggregationGranularity) {
        EnergyProfileDTO defaultDTO = new EnergyProfileDTO();
        for (int i = 0; i < predictionGranularity.getNoOutputs(); i++) {
            defaultDTO.curve.add(new EnergySampleDTO(0.0, startTime));
            startTime = startTime.plus(predictionGranularity.getSampleFrequencyMin(), ChronoUnit.MINUTES);
        }
        defaultDTO.setDeviceId(deviceId);
        defaultDTO.setMeasurementId(measurementId);
        defaultDTO.setPredictionGranularity(predictionGranularity);
        defaultDTO.setEnergyType(EnergyType.setType(energyType));
        defaultDTO.setAggregationGranularity(AggregationGranularity.setGranularity(aggregationGranularity));

        return defaultDTO;
    }

    public static EnergyProfileDTO getDefaultInstanceExpanded(LocalDateTime startTime, PredictionGranularity predictionGranularity, UUID deviceId, UUID measurementId, String energyType) {
        EnergyProfileDTO defaultDTO = new EnergyProfileDTO();

        for(int i = 0; i < predictionGranularity.getNoOutputs(); ++i) {
            defaultDTO.curve.add(new EnergySampleDTO(0.0D, startTime));
            startTime = startTime.plus((long)predictionGranularity.getSampleFrequencyMin(), ChronoUnit.MINUTES);
        }

        defaultDTO.setDeviceId(deviceId);
        defaultDTO.setMeasurementId(measurementId);
        defaultDTO.setPredictionGranularity(predictionGranularity);
        defaultDTO.setEnergyType(EnergyType.setType(energyType));
        return defaultDTO;
    }


    public EnergyProfileDTO(List<EnergySampleDTO> curve,
                            UUID deviceId,
                            UUID measurementId,
                            AggregationGranularity aggregationGranularity,
                            PredictionGranularity predictionGranularity,
                            PredictionType predictionType,
                            EnergyType energyType) {
        this.curve = curve;
        this.deviceId = deviceId;
        this.measurementId = measurementId;
        this.aggregationGranularity = aggregationGranularity;
        this.predictionGranularity = predictionGranularity;
        this.predictionType = predictionType;
        this.energyType = energyType;
    }

    public EnergyProfileDTO(List<EnergySampleDTO> curve,
                            UUID deviceId,
                            UUID measurementId,
                            AggregationGranularity aggregationGranularity) {
        this.curve = curve;
        this.deviceId = deviceId;
        this.measurementId = measurementId;
        this.aggregationGranularity = aggregationGranularity;
    }

    public List<EnergySampleDTO> getCurve() {
        return curve;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public UUID getMeasurementId() {
        return measurementId;
    }

    public AggregationGranularity getAggregationGranularity() {
        return aggregationGranularity;
    }

    public PredictionGranularity getPredictionGranularity() {
        return predictionGranularity;
    }

    public PredictionType getPredictionType() {
        return predictionType;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public void setCurve(List<EnergySampleDTO> curve) {
        this.curve = curve;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public void setMeasurementId(UUID measurementId) {
        this.measurementId = measurementId;
    }

    public void setAggregationGranularity(AggregationGranularity aggregationGranularity) {
        this.aggregationGranularity = aggregationGranularity;
    }

    public void setPredictionGranularity(PredictionGranularity predictionGranularity) {
        this.predictionGranularity = predictionGranularity;
    }

    public void setPredictionType(PredictionType predictionType) {
        this.predictionType = predictionType;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public List<Double> getEnergyValues() {
        if (curve == null) return new ArrayList<>();

        return curve.stream()
                .map(EnergySampleDTO::getEnergyValue)
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EnergyProfileDTO)) return false;
        EnergyProfileDTO that = (EnergyProfileDTO) o;
        return Objects.equals(curve, that.curve) &&
                Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(measurementId, that.measurementId) &&
                aggregationGranularity == that.aggregationGranularity &&
                predictionGranularity == that.predictionGranularity &&
                predictionType == that.predictionType &&
                energyType == that.energyType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(curve, deviceId, measurementId, aggregationGranularity, predictionGranularity, predictionType, energyType);
    }

    @Override
    public String toString() {
        return "EnergyProfileDTO{" +
                "curve=" + curve +
                ", deviceId=" + deviceId +
                ", measurementId=" + measurementId +
                ", aggregationGranularity=" + aggregationGranularity +
                ", predictionGranularity=" + predictionGranularity +
                ", predictionType=" + predictionType +
                ", energyType=" + energyType +
                '}';
    }
}
