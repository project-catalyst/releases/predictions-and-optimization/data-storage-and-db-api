//package ro.tuc.dsrl.catalyst.model.dto;
//
//import java.io.Serializable;
//import java.util.UUID;
//
//public class ActionPropertyDTO implements Serializable {
//
//    private UUID id;
//    private String property;
//    private String observation;
//    private UUID measureUnitId;
//    private UUID valueTypeId;
//    private UUID actionTypeId;
//
//    public ActionPropertyDTO() {
//
//    }
//
//    public ActionPropertyDTO(UUID id,
//                             String property,
//                             String observation,
//                             UUID measureUnitId,
//                             UUID valueTypeId,
//                             UUID actionTypeId) {
//        this.id = id;
//        this.property = property;
//        this.observation = observation;
//        this.measureUnitId = measureUnitId;
//        this.valueTypeId = valueTypeId;
//        this.actionTypeId = actionTypeId;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public String getProperty() {
//        return property;
//    }
//
//    public String getObservation() {
//        return observation;
//    }
//
//    public UUID getMeasureUnitId() {
//        return measureUnitId;
//    }
//
//    public UUID getValueTypeId() {
//        return valueTypeId;
//    }
//
//    public UUID getActionTypeId() {
//        return actionTypeId;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public void setProperty(String property) {
//        this.property = property;
//    }
//
//    public void setObservation(String observation) {
//        this.observation = observation;
//    }
//
//    public void setMeasureUnitId(UUID measureUnitId) {
//        this.measureUnitId = measureUnitId;
//    }
//
//    public void setValueTypeId(UUID valueTypeId) {
//        this.valueTypeId = valueTypeId;
//    }
//
//    public void setActionTypeId(UUID actionTypeId) {
//        this.actionTypeId = actionTypeId;
//    }
//}
