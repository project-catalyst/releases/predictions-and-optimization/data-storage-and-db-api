package ro.tuc.dsrl.catalyst.model.enums;

public class OptimizerConfig {

    public static final boolean ID_ACTIVE = false;
    public static final int MONITORING_RATE_IN_MINUTES = 5;
}
