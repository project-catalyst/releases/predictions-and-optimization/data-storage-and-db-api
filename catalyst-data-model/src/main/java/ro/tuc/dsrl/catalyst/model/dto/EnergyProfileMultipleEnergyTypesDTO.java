package ro.tuc.dsrl.catalyst.model.dto;

import ro.tuc.dsrl.catalyst.model.enums.AggregationGranularity;
import ro.tuc.dsrl.catalyst.model.enums.AlgorithmType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;

import java.util.List;

public class EnergyProfileMultipleEnergyTypesDTO {

    private String deviceName;

    private AggregationGranularity aggregationGranularity;
    private PredictionGranularity predictionGranularity;
    private PredictionType predictionType;
    private AlgorithmType algorithmType;

    private Long startTime;
    private List<EnergySamplesForMeasurementDTO> samples;

    public EnergyProfileMultipleEnergyTypesDTO() {
    }

    public EnergyProfileMultipleEnergyTypesDTO(String deviceName, AggregationGranularity aggregationGranularity, PredictionGranularity predictionGranularity, PredictionType predictionType, AlgorithmType algorithmType, Long startTime, List<EnergySamplesForMeasurementDTO> samples) {
        this.deviceName = deviceName;
        this.aggregationGranularity = aggregationGranularity;
        this.predictionGranularity = predictionGranularity;
        this.predictionType = predictionType;
        this.algorithmType = algorithmType;
        this.startTime = startTime;
        this.samples = samples;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public AggregationGranularity getAggregationGranularity() {
        return aggregationGranularity;
    }

    public void setAggregationGranularity(AggregationGranularity aggregationGranularity) {
        this.aggregationGranularity = aggregationGranularity;
    }

    public PredictionGranularity getPredictionGranularity() {
        return predictionGranularity;
    }

    public void setPredictionGranularity(PredictionGranularity predictionGranularity) {
        this.predictionGranularity = predictionGranularity;
    }

    public PredictionType getPredictionType() {
        return predictionType;
    }

    public void setPredictionType(PredictionType predictionType) {
        this.predictionType = predictionType;
    }

    public AlgorithmType getAlgorithmType() {
        return algorithmType;
    }

    public void setAlgorithmType(AlgorithmType algorithmType) {
        this.algorithmType = algorithmType;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public List<EnergySamplesForMeasurementDTO> getSamples() {
        return samples;
    }

    public void setSamples(List<EnergySamplesForMeasurementDTO> samples) {
        this.samples = samples;
    }

    @Override
    public String toString() {
        return "EnergyProfileMultipleEnergyTypesDTO{" +
                "deviceName='" + deviceName + '\'' +
                ", aggregationGranularity=" + aggregationGranularity +
                ", predictionGranularity=" + predictionGranularity +
                ", predictionType=" + predictionType +
                ", algorithmType=" + algorithmType +
                ", startTime=" + startTime +
                ", samples=" + samples +
                '}';
    }
}
