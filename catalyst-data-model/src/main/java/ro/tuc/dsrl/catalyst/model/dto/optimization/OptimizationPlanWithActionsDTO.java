//package ro.tuc.dsrl.catalyst.model.dto.optimization;
//
//import ro.tuc.dsrl.catalyst.model.dto.ShiftDelayTolerantActionDTO;
//
//import java.util.List;
//
//public class OptimizationPlanWithActionsDTO {
//
//    private Long startTimePlan;
//    private Long endTimePlan;
//    private String timeframe;
//
//    private Double carbonSavings;
//    private Double confidenceLevel;
//    private Double costSavings;
//    private Double energySavings;
//
//    private Boolean selected;
//
//    private List<ShiftDelayTolerantActionDTO> actions;
//
//    public OptimizationPlanWithActionsDTO(Long startTimePlan, Long endTimePlan, String timeframe, Double carbonSavings, Double confidenceLevel, Double costSavings, Double energySavings, Boolean selected, List<ShiftDelayTolerantActionDTO> actions) {
//        this.startTimePlan = startTimePlan;
//        this.endTimePlan = endTimePlan;
//        this.timeframe = timeframe;
//        this.carbonSavings = carbonSavings;
//        this.confidenceLevel = confidenceLevel;
//        this.costSavings = costSavings;
//        this.energySavings = energySavings;
//        this.selected = selected;
//        this.actions = actions;
//    }
//
//    public OptimizationPlanWithActionsDTO() {
//    }
//
//    public Long getStartTimePlan() {
//        return startTimePlan;
//    }
//
//    public void setStartTimePlan(Long startTimePlan) {
//        this.startTimePlan = startTimePlan;
//    }
//
//    public Long getEndTimePlan() {
//        return endTimePlan;
//    }
//
//    public void setEndTimePlan(Long endTimePlan) {
//        this.endTimePlan = endTimePlan;
//    }
//
//    public String getTimeframe() {
//        return timeframe;
//    }
//
//    public void setTimeframe(String timeframe) {
//        this.timeframe = timeframe;
//    }
//
//    public Double getCarbonSavings() {
//        return carbonSavings;
//    }
//
//    public void setCarbonSavings(Double carbonSavings) {
//        this.carbonSavings = carbonSavings;
//    }
//
//    public Double getConfidenceLevel() {
//        return confidenceLevel;
//    }
//
//    public void setConfidenceLevel(Double confidenceLevel) {
//        this.confidenceLevel = confidenceLevel;
//    }
//
//    public Double getCostSavings() {
//        return costSavings;
//    }
//
//    public void setCostSavings(Double costSavings) {
//        this.costSavings = costSavings;
//    }
//
//    public Double getEnergySavings() {
//        return energySavings;
//    }
//
//    public void setEnergySavings(Double energySavings) {
//        this.energySavings = energySavings;
//    }
//
//    public Boolean getSelected() {
//        return selected;
//    }
//
//    public void setSelected(Boolean selected) {
//        this.selected = selected;
//    }
//
//    public List<ShiftDelayTolerantActionDTO> getActions() {
//        return actions;
//    }
//
//    public void setActions(List<ShiftDelayTolerantActionDTO> actions) {
//        this.actions = actions;
//    }
//}
