package ro.tuc.dsrl.catalyst.model.dto;

import java.io.Serializable;
import java.util.UUID;

public class PropertySourceDTO implements Serializable {

    private UUID id;
    private String propertySource;

    public PropertySourceDTO(){

    }

    public PropertySourceDTO(UUID id,
                             String propertySource) {
        this.id = id;
        this.propertySource = propertySource;
    }

    public UUID getId() {
        return id;
    }

    public String getPropertySource() {
        return propertySource;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setPropertySource(String propertySource) {
        this.propertySource = propertySource;
    }
}
