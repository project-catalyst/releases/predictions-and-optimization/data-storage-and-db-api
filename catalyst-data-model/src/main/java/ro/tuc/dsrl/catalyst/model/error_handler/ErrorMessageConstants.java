package ro.tuc.dsrl.catalyst.model.error_handler;

public class ErrorMessageConstants {

    private ErrorMessageConstants() {

    }

    public static final String GRANULARITY_NOT_NULL = "Granularity must not be null.";
    public static final String ENERGY_TYPE_NOT_NULL = "Energy type must not be null.";
    public static final String COMPONENT_ID_NOT_NULL = "Component Name must not be null";
    public static final String START_TIME_NOT_NULL = "Start time must not be null";

}
