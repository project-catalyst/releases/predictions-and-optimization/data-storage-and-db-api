package ro.tuc.dsrl.catalyst.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum EnergyProfileLabel {

    COOLING_SYSTEM("COOLING_SYSTEM"),
    IT_COMPONENT_DT("SERVER_ROOM_DT"),
    IT_COMPONENT_RT("SERVER_ROOM_RT");

    private final String label;
    private static Map map = new HashMap<>();

    EnergyProfileLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static EnergyProfileLabel setType(String label){
        switch (label) {
            case "COOLING_SYSTEM":
                return EnergyProfileLabel.COOLING_SYSTEM;
            case "IT_COMPONENT_DT":
                return EnergyProfileLabel.IT_COMPONENT_DT;
            case "IT_COMPONENT_RT":
                return EnergyProfileLabel.IT_COMPONENT_RT;
            default:
                return null;
        }
    }
}
