package ro.tuc.dsrl.catalyst.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MarketDataDTO implements Serializable  {
    private LocalDateTime recordTime;
    private List<Double> drSignal;
    private List<Double> drIncentive;
    private List<Double> energyPrice;
    private List<Double> thermalPrice;
    private List<Double> itLoadPrice;

    public MarketDataDTO() {
        this.recordTime = LocalDateTime.now();
        this.drSignal = new ArrayList<>();
        this.drIncentive = new ArrayList<>();
        this.energyPrice = new ArrayList<>();
        this.thermalPrice = new ArrayList<>();
        this.itLoadPrice = new ArrayList<>();
    }

    public MarketDataDTO(LocalDateTime recordTime, List<Double> drSignal, List<Double> drIncentive, List<Double> energyPrice, List<Double> thermalPrice, List<Double> itLoadPrice) {
        this.recordTime = recordTime;
        this.drSignal = drSignal;
        this.drIncentive = drIncentive;
        this.energyPrice = energyPrice;
        this.thermalPrice = thermalPrice;
        this.itLoadPrice = itLoadPrice;
    }

    public LocalDateTime getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(LocalDateTime recordTime) {
        this.recordTime = recordTime;
    }

    public List<Double> getDrSignal() {
        return drSignal;
    }

    public void setDrSignal(List<Double> drSignal) {
        this.drSignal = drSignal;
    }

    public List<Double> getDrIncentive() {
        return drIncentive;
    }

    public void setDrIncentive(List<Double> drIncentive) {
        this.drIncentive = drIncentive;
    }

    public List<Double> getEnergyPrice() {
        return energyPrice;
    }

    public void setEnergyPrice(List<Double> energyPrice) {
        this.energyPrice = energyPrice;
    }

    public List<Double> getThermalPrice() {
        return thermalPrice;
    }

    public void setThermalPrice(List<Double> thermalPrice) {
        this.thermalPrice = thermalPrice;
    }

    public List<Double> getItLoadPrice() {
        return itLoadPrice;
    }

    public void setItLoadPrice(List<Double> itLoadPrice) {
        this.itLoadPrice = itLoadPrice;
    }
}
