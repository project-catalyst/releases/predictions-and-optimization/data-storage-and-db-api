package ro.tuc.dsrl.catalyst.model.enums;

public enum PropertySource {

    MANUAL("STATIC"),
    MEASURED("MEASURED");

    private final String type;

    PropertySource(String type){
        this.type = type;
    }

    public String getType(){
        return type;
    }

    public static PropertySource setType(String propertySource){
        if (propertySource.equals("STATIC")) {
            return PropertySource.MANUAL;
        } else if(propertySource.equals("MEASURED")){
            return PropertySource.MEASURED;
        } else {
            return null;
        }
    }
}
