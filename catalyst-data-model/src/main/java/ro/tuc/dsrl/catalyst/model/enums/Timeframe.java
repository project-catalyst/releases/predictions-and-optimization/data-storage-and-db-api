package ro.tuc.dsrl.catalyst.model.enums;

public enum Timeframe {

    DAY_AHEAD("day_ahead"),
    INTRA_DAY("intra_day"),
    REAL_TIME("real_time"),
    NONE(""),
    AS_TIMEFRAME("as");

    private String name;

    Timeframe(String name){
        this.name = name;
    }

    public static Timeframe getFromName(String name){

        for (Timeframe tf : Timeframe.values()) {
            if(tf.name.equals(name)){
                return tf;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }
}
