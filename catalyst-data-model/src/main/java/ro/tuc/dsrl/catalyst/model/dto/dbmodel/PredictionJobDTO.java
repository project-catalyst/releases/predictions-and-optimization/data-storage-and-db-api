package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class PredictionJobDTO implements Serializable {

    private UUID id;
    private LocalDateTime startTime;
    private String granularity;
    private String algoType;
    private String flexibilityType;

    public PredictionJobDTO(){

    }

    public PredictionJobDTO(UUID id, LocalDateTime startTime, String granularity, String algoType, String flexibilityType) {
        this.id = id;
        this.startTime = startTime;
        this.granularity = granularity;
        this.algoType = algoType;
        this.flexibilityType = flexibilityType;
    }

    public PredictionJobDTO(LocalDateTime startTime, String granularity, String algoType, String flexibilityType) {
        this.startTime = startTime;
        this.granularity = granularity;
        this.algoType = algoType;
        this.flexibilityType = flexibilityType;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public String getGranularity() {
        return granularity;
    }

    public String getAlgoType() {
        return algoType;
    }

    public String getFlexibilityType() {
        return flexibilityType;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void setGranularity(String granularity) {
        this.granularity = granularity;
    }

    public void setAlgoType(String algoType) {
        this.algoType = algoType;
    }

    public void setFlexibilityType(String flexibilityType) {
        this.flexibilityType = flexibilityType;
    }

    @Override
    public String toString() {
        return "PredictionJobDTO{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", granularity='" + granularity + '\'' +
                ", algoType='" + algoType + '\'' +
                ", flexibilityType='" + flexibilityType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PredictionJobDTO)) return false;
        PredictionJobDTO that = (PredictionJobDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(granularity, that.granularity) &&
                Objects.equals(algoType, that.algoType) &&
                Objects.equals(flexibilityType, that.flexibilityType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startTime, granularity, algoType, flexibilityType);
    }
}
