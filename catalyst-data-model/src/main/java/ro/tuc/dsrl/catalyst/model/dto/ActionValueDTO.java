//package ro.tuc.dsrl.catalyst.model.dto;
//
//import java.io.Serializable;
//import java.util.UUID;
//
//public class ActionValueDTO implements Serializable {
//
//    private UUID id;
//    private Double value;
//    private UUID actionInstanceId;
//    private UUID actionPropertyId;
//
//    public ActionValueDTO(){
//
//    }
//
//    public ActionValueDTO(UUID id,
//                          Double value,
//                          UUID actionInstanceId,
//                          UUID actionPropertyId) {
//        this.id = id;
//        this.value = value;
//        this.actionInstanceId = actionInstanceId;
//        this.actionPropertyId = actionPropertyId;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public Double getValue() {
//        return value;
//    }
//
//    public UUID getActionInstanceId() {
//        return actionInstanceId;
//    }
//
//    public UUID getActionPropertyId() {
//        return actionPropertyId;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public void setValue(Double value) {
//        this.value = value;
//    }
//
//    public void setActionInstanceId(UUID actionInstanceId) {
//        this.actionInstanceId = actionInstanceId;
//    }
//
//    public void setActionPropertyId(UUID actionPropertyId) {
//        this.actionPropertyId = actionPropertyId;
//    }
//}
