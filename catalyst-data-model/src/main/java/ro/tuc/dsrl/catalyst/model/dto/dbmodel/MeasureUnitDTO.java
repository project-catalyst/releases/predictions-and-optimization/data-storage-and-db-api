package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.util.UUID;

public class MeasureUnitDTO  implements Serializable {

    private UUID id;
    private String unit;

    public MeasureUnitDTO(){

    }

    public MeasureUnitDTO(UUID id,
                          String unit) {
        this.id = id;
        this.unit = unit;
    }

    public UUID getId() {
        return id;
    }

    public String getUnit() {
        return unit;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
