package ro.tuc.dsrl.catalyst.model.dto;

public class EnergyPointValueDTO {
    private double energyValue;
    private long timeStamp;

    public EnergyPointValueDTO() {
    }

    public EnergyPointValueDTO(double value, long timeStamp) {
        this.energyValue = value;
        this.timeStamp = timeStamp;
    }

    public double getEnergyValue() {
        return energyValue;
    }

    public void setEnergyValue(double energyValue) {
        this.energyValue = energyValue;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

}
