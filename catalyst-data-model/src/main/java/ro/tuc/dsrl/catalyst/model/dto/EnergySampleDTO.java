package ro.tuc.dsrl.catalyst.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class EnergySampleDTO implements Serializable, Comparable<EnergySampleDTO> {

    private Double energyValue;

    private LocalDateTime timestamp;

    public EnergySampleDTO() {

    }

    public EnergySampleDTO(Double energyValue, LocalDateTime timestamp) {
        this.energyValue = energyValue;
        this.timestamp = timestamp;
    }

    public Double getEnergyValue() {
        return energyValue;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setEnergyValue(Double energyValue) {
        this.energyValue = energyValue;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EnergySampleDTO)) return false;
        EnergySampleDTO that = (EnergySampleDTO) o;
        return Objects.equals(energyValue, that.energyValue);
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 31 * result + energyValue.intValue();
        result = 31 * result + timestamp.hashCode();

        return result;
    }

    @Override
    public int compareTo(EnergySampleDTO o) {
        return o.getTimestamp().compareTo(this.getTimestamp());}

    @Override
    public String toString() {
        return "EnergySampleDTO{" +
                "energyValue=" + energyValue +
                ", timestamp=" + timestamp +
                '}';
    }
}
