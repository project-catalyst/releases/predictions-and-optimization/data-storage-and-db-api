package ro.tuc.dsrl.catalyst.model.dto;

import java.io.Serializable;
import java.util.Map;

public class ThermalSimulationDTO implements Serializable {

    private Map<String, ThermalProfileDTO> thermalSimulations;

    public ThermalSimulationDTO(){}

    public ThermalSimulationDTO(Map<String, ThermalProfileDTO> thermalSimulations) {
        this.thermalSimulations = thermalSimulations;
    }

    public Map<String, ThermalProfileDTO> getThermalSimulations() {
        return thermalSimulations;
    }

    public void setThermalSimulations(Map<String, ThermalProfileDTO> thermalSimulations) {
        this.thermalSimulations = thermalSimulations;
    }
}
