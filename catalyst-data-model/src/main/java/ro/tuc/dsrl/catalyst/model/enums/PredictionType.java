package ro.tuc.dsrl.catalyst.model.enums;

public enum PredictionType {

    ENERGY_PRODUCTION("PRODUCTION"),
    ENERGY_CONSUMPTION("CONSUMPTION"),
    ENERGY_BASELINE("BASELINE"),
    ENERGY_PRICE("MARKET_PRICE"),
    OPTIMIZED_PROFILE("OPTIMIZED_PROFILE");

    private final String type;

    PredictionType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static PredictionType setType(String property) {
        switch (property) {
            case "ENERGY_PRODUCTION":
                return PredictionType.ENERGY_PRODUCTION;
            case "ENERGY_CONSUMPTION":
                return PredictionType.ENERGY_CONSUMPTION;
            case "ENERGY_BASELINE":
                return PredictionType.ENERGY_BASELINE;
            case "MARKET_PRICE":
                return PredictionType.ENERGY_PRICE;
            case "OPTIMIZED_PROFILE":
                return PredictionType.OPTIMIZED_PROFILE;
            default:
                return null;
        }
    }
}
