package ro.tuc.dsrl.catalyst.model.utils;

public class Constants {
    private Constants() {
    }

    public static final String SDTW_AMOUNT = "sdtw_amount";
    public static final String SDTW_PERCENTAGE = "sdtw_percentage";
    public static final String SDTW_START_TIME = "sdtw_start_time";
    public static final String SDTW_END_TIME = "sdtw_end_time";

    public static final String REALLOCATION_PERCENTAGE = "reallocation_percentage";
    public static final String RELOCATE_AMOUNT = "relocate_amount";

    public static final String CURRENT_DC = "current DC";
    public static final String OTHER_DC = "other DC";
    public static final String EMPTY_LABEL = "EMPTY_LABEL";
    public static final boolean ACTIVE = true;
    public static final boolean INACTIVE = false;

    public static final double MEASURE_UNIT_CONVERSION = 0.001;

}
