package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.util.UUID;

public class DeviceDTO implements Serializable {

    private UUID id;
    private String label;
    private UUID deviceTypeId;
    private UUID parent;

    public DeviceDTO(){

    }

    public DeviceDTO(UUID id,
                     String label,
                     UUID deviceTypeId,
                     UUID parent) {
        this.id = id;
        this.label = label;
        this.deviceTypeId = deviceTypeId;
        this.parent = parent;
    }

    public UUID getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public UUID getDeviceTypeId() {
        return deviceTypeId;
    }

    public UUID getParent() {
        return parent;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDeviceTypeId(UUID deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public void setParent(UUID parent) {
        this.parent = parent;
    }
}
