package ro.tuc.dsrl.catalyst.model.dto;

import java.util.UUID;

public class FlexibilityStrategyDTO {

    private double We;

   // private double Wl;

    private double Wf;

    private double Wt;

    private double REN;

    private boolean reallocActive;

    private String datacenter;

    public FlexibilityStrategyDTO() {
    }

    public FlexibilityStrategyDTO(double we, /*double wl,*/ double wf, double wt, double ren, boolean reallocActive, String datacenter) {
        We = we;
     //   Wl = wl;
        Wf = wf;
        Wt = wt;
        this.REN = ren;
        this.reallocActive = reallocActive;
        this.datacenter = datacenter;
    }

    public double getWe() {
        return We;
    }

    public void setWe(double we) {
        We = we;
    }

    /*
    public double getWl() {
        return Wl;
    }

    public void setWl(double wl) {
        Wl = wl;
    }
*/
    public double getWf() {
        return Wf;
    }

    public void setWf(double wf) {
        Wf = wf;
    }

    public double getWt() {
        return Wt;
    }

    public void setWt(double wt) {
        Wt = wt;
    }

    public boolean isReallocActive() {
        return reallocActive;
    }

    public void setReallocActive(boolean reallocActive) {
        this.reallocActive = reallocActive;
    }

    public String getDatacenter() {
        return datacenter;
    }

    public void setDatacenter(String datacenter) {
        this.datacenter = datacenter;
    }

    public double getREN() {
        return REN;
    }

    public void setREN(double REN) {
        this.REN = REN;
    }
}
