package ro.tuc.dsrl.catalyst.model.enums;

public enum EnergyType {

    ELECTRICAL("ELECTRICAL"),
    THERMAL("THERMAL");

    private final String type;

    EnergyType(String type){
        this.type = type;
    }

    public String getType(){
        return type;
    }

    public static EnergyType setType(String energyType){
        if (energyType.equals("ELECTRICAL")) {
            return EnergyType.ELECTRICAL;
        } else if(energyType.equals("THERMAL")){
            return EnergyType.THERMAL;
        } else {
            return null;
        }
    }
}
