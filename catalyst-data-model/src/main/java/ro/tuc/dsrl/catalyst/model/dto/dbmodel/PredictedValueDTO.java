package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class PredictedValueDTO implements Serializable {

    private UUID id;
    private LocalDateTime timestamp;
    private double value;
    private UUID measurementId;
    private UUID deviceId;
    private UUID predictionJobId;

    public PredictedValueDTO(){

    }

    public PredictedValueDTO(UUID id,
                             LocalDateTime timestamp,
                             double value,
                             UUID measurementId,
                             UUID deviceId,
                             UUID predictionJobId) {
        this.id = id;
        this.timestamp = timestamp;
        this.value = value;
        this.measurementId = measurementId;
        this.deviceId = deviceId;
        this.predictionJobId = predictionJobId;
    }
	
	public PredictedValueDTO(LocalDateTime timestamp, 
							double value,
							UUID measurementId,
							UUID deviceId, 
							UUID predictionJobId) {
        this.timestamp = timestamp;
        this.value = value;
        this.measurementId = measurementId;
        this.deviceId = deviceId;
        this.predictionJobId = predictionJobId;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public double getValue() {
        return value;
    }

    public UUID getMeasurementId() {
        return measurementId;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public UUID getPredictionJobId() {
        return predictionJobId;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setMeasurementId(UUID measurementId) {
        this.measurementId = measurementId;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public void setPredictionJobId(UUID predictionJobId) {
        this.predictionJobId = predictionJobId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PredictedValueDTO)) return false;
        PredictedValueDTO that = (PredictedValueDTO) o;
        return Double.compare(that.value, value) == 0 &&
                Objects.equals(id, that.id) &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(measurementId, that.measurementId) &&
                Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(predictionJobId, that.predictionJobId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, value, measurementId, deviceId, predictionJobId);
    }

    @Override
    public String toString() {
        return "PredictedValueDTO{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", value=" + value +
                ", measurementId=" + measurementId +
                ", deviceId=" + deviceId +
                ", predictionJobId=" + predictionJobId +
                '}';
    }
}
