//package ro.tuc.dsrl.catalyst.model.dto;
//
//import com.fasterxml.jackson.annotation.JsonSubTypes;
//import com.fasterxml.jackson.annotation.JsonTypeInfo;
//
//import java.util.Objects;
//import java.util.UUID;
//
//@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
//@JsonSubTypes({ @JsonSubTypes.Type(value = ActionDTO.class) })
//public class BaseActionDTO {
//    private UUID id;
//    private long start;
//    private long end;
//    private String name;
//    private Double value;
//    private boolean active;
//
//    public BaseActionDTO() {
//    }
//
//    public BaseActionDTO(UUID id, long start, long end, String name, Double value, boolean active) {
//        this.id = id;
//        this.start = start;
//        this.end = end;
//        this.name = name;
//        this.value = value;
//        this.active = active;
//    }
//
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        BaseActionDTO actionDTO = (BaseActionDTO) o;
//        return id.equals(actionDTO.id);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id);
//    }
//
//
//    public UUID getId() {
//        return id;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public long getStart() {
//        return start;
//    }
//
//    public void setStart(long start) {
//        this.start = start;
//    }
//
//    public long getEnd() {
//        return end;
//    }
//
//    public void setEnd(long end) {
//        this.end = end;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Double getValue() {
//        return value;
//    }
//
//    public void setValue(Double value) {
//        this.value = value;
//    }
//
//    public boolean isActive() {
//        return active;
//    }
//
//    public void setActive(boolean active) {
//        this.active = active;
//    }
//}
