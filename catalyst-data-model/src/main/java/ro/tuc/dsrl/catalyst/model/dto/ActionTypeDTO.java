//package ro.tuc.dsrl.catalyst.model.dto;
//
//import java.io.Serializable;
//import java.util.UUID;
//
//public class ActionTypeDTO implements Serializable {
//
//    private UUID id;
//    private String typeOfActivity;
//
//    public ActionTypeDTO(){
//
//    }
//
//    public ActionTypeDTO(UUID id,
//                         String typeOfActivity) {
//        this.id = id;
//        this.typeOfActivity = typeOfActivity;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public String getTypeOfActivity() {
//        return typeOfActivity;
//    }
//
//    public void setTypeOfActivity(String typeOfActivity) {
//        this.typeOfActivity = typeOfActivity;
//    }
//}
