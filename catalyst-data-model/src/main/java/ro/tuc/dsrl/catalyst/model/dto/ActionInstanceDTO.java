//package ro.tuc.dsrl.catalyst.model.dto;
//
//import java.io.Serializable;
//import java.time.LocalDateTime;
//import java.util.UUID;
//
//public class ActionInstanceDTO  implements Serializable {
//
//    private UUID id;
//    private String label;
//    private LocalDateTime startTime;
//    private LocalDateTime endTime;
//    private UUID actionTypeId;
//    private UUID optimizationPlanId;
//
//    public ActionInstanceDTO(){
//
//    }
//
//    public UUID getOptimizationPlanId() {
//        return optimizationPlanId;
//    }
//
//    public void setOptimizationPlanId(UUID optimizationPlanId) {
//        this.optimizationPlanId = optimizationPlanId;
//    }
//
//    public ActionInstanceDTO(UUID id,
//                             String label,
//                             LocalDateTime startTime,
//                             LocalDateTime endTime,
//                             UUID actionTypeId,
//                             UUID optimizationPlanId) {
//        this.id = id;
//        this.label = label;
//        this.startTime = startTime;
//        this.endTime = endTime;
//        this.actionTypeId = actionTypeId;
//        this.optimizationPlanId = optimizationPlanId;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public String getLabel() {
//        return label;
//    }
//
//    public LocalDateTime getStartTime() {
//        return startTime;
//    }
//
//    public LocalDateTime getEndTime() {
//        return endTime;
//    }
//
//    public UUID getActionTypeId() {
//        return actionTypeId;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public void setLabel(String label) {
//        this.label = label;
//    }
//
//    public void setStartTime(LocalDateTime startTime) {
//        this.startTime = startTime;
//    }
//
//    public void setEndTime(LocalDateTime endTime) {
//        this.endTime = endTime;
//    }
//
//    public void setActionTypeId(UUID actionTypeId) {
//        this.actionTypeId = actionTypeId;
//    }
//}
