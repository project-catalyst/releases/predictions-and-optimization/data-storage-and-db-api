package ro.tuc.dsrl.catalyst.model.dto.optimization;

import java.io.Serializable;
import java.util.UUID;

public class DatacenterStateDTO implements Serializable {

    private UUID id;

    private long recordTime;

//    private double greenGeneration;
//
//    private double brownGeneration;

    private double facilityPowerConsumption;

//    private double officesPowerConsumption;

    private double itPowerConsumptionRt;

    private double itPowerConsumptionDt;

    private double powerToSmartGrid;

    private double powerFromSmartGrid;

    private double esdCurrent;

//    private double esdCharge;
//
//    private double esdDischarge;

    private double tesCurrent;
//
//    private double tesCharge;
//
//    private double tesDischarge;

    private double realocEnergy;

    private double selfThermalHeat;

    private double hostWorkload;

    private double planConfidence;

    public DatacenterStateDTO() {

    }

    public DatacenterStateDTO(UUID id, long recordTime, double greenGeneration, double brownGeneration, double facilityPowerConsumption, double officesPowerConsumption, double itPowerConsumptionRt, double itPowerConsumptionDt, double powerToSmartGrid, double powerFromSmartGrid, double esdCurrent, double esdCharge, double esdDischarge, double tesCurrent, double tesCharge, double tesDischarge, double realocEnergy, double selfThermalHeat, double planConfidence, double hostWorkload) {
        this.id = id;
        this.recordTime = recordTime;
      //  this.greenGeneration = greenGeneration;
      //  this.brownGeneration = brownGeneration;
        this.facilityPowerConsumption = facilityPowerConsumption;
     //   this.officesPowerConsumption = officesPowerConsumption;
        this.itPowerConsumptionRt = itPowerConsumptionRt;
        this.itPowerConsumptionDt = itPowerConsumptionDt;
        this.powerToSmartGrid = powerToSmartGrid;
        this.powerFromSmartGrid = powerFromSmartGrid;
        this.esdCurrent = esdCurrent;
      //  this.esdCharge = esdCharge;
      //  this.esdDischarge = esdDischarge;
        this.tesCurrent = tesCurrent;
      //  this.tesCharge = tesCharge;
      //  this.tesDischarge = tesDischarge;
        this.realocEnergy = realocEnergy;
        this.selfThermalHeat = selfThermalHeat;
        this.planConfidence = planConfidence;
        this.hostWorkload = hostWorkload;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public long getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(long recordTime) {
        this.recordTime = recordTime;
    }
//
//    public double getGreenGeneration() {
//        return greenGeneration;
//    }
//
//    public void setGreenGeneration(double greenGeneration) {
//        this.greenGeneration = greenGeneration;
//    }
//
//    public double getBrownGeneration() {
//        return brownGeneration;
//    }
//
//    public void setBrownGeneration(double brownGeneration) {
//        this.brownGeneration = brownGeneration;
//    }

    public double getFacilityPowerConsumption() {
        return facilityPowerConsumption;
    }

    public void setFacilityPowerConsumption(double facilityPowerConsumption) {
        this.facilityPowerConsumption = facilityPowerConsumption;
    }
//
//    public double getOfficesPowerConsumption() {
//        return officesPowerConsumption;
//    }
//
//    public void setOfficesPowerConsumption(double officesPowerConsumption) {
//        this.officesPowerConsumption = officesPowerConsumption;
//    }

    public double getItPowerConsumptionRt() {
        return itPowerConsumptionRt;
    }

    public void setItPowerConsumptionRt(double itPowerConsumptionRt) {
        this.itPowerConsumptionRt = itPowerConsumptionRt;
    }

    public double getItPowerConsumptionDt() {
        return itPowerConsumptionDt;
    }

    public void setItPowerConsumptionDt(double itPowerConsumptionDt) {
        this.itPowerConsumptionDt = itPowerConsumptionDt;
    }

    public double getPowerToSmartGrid() {
        return powerToSmartGrid;
    }

    public void setPowerToSmartGrid(double powerToSmartGrid) {
        this.powerToSmartGrid = powerToSmartGrid;
    }

    public double getPowerFromSmartGrid() {
        return powerFromSmartGrid;
    }

    public void setPowerFromSmartGrid(double powerFromSmartGrid) {
        this.powerFromSmartGrid = powerFromSmartGrid;
    }

    public double getEsdCurrent() {
        return esdCurrent;
    }

    public void setEsdCurrent(double esdCurrent) {
        this.esdCurrent = esdCurrent;
    }

//    public double getEsdCharge() {
//        return esdCharge;
//    }
//
//    public void setEsdCharge(double esdCharge) {
//        this.esdCharge = esdCharge;
//    }
//
//    public double getEsdDischarge() {
//        return esdDischarge;
//    }
//
//    public void setEsdDischarge(double esdDischarge) {
//        this.esdDischarge = esdDischarge;
//    }

    public double getTesCurrent() {
        return tesCurrent;
    }

    public void setTesCurrent(double tesCurrent) {
        this.tesCurrent = tesCurrent;
    }

//    public double getTesCharge() {
//        return tesCharge;
//    }
//
//    public void setTesCharge(double tesCharge) {
//        this.tesCharge = tesCharge;
//    }
//
//    public double getTesDischarge() {
//        return tesDischarge;
//    }
//
//    public void setTesDischarge(double tesDischarge) {
//        this.tesDischarge = tesDischarge;
//    }

    public double getRealocEnergy() {
        return realocEnergy;
    }

    public void setRealocEnergy(double realocEnergy) {
        this.realocEnergy = realocEnergy;
    }

    public double getSelfThermalHeat() {
        return selfThermalHeat;
    }

    public void setSelfThermalHeat(double selfThermalHeat) {
        this.selfThermalHeat = selfThermalHeat;
    }

    public double getPlanConfidence() {
        return planConfidence;
    }

    public void setPlanConfidence(double planConfidence) {
        this.planConfidence = planConfidence;
    }

    public double getHostWorkload() {
        return hostWorkload;
    }

    public void setHostWorkload(double hostWorkload) {
        this.hostWorkload = hostWorkload;
    }
}
