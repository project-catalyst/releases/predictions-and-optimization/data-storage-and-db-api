package ro.tuc.dsrl.catalyst.model.error_handler;

public class ResourceNotFoundException extends RuntimeException {

    private static final String MESSAGE = "Resource not found!";
    private final String resource;

    public ResourceNotFoundException(String resource) {
        super(MESSAGE);
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }
}
