//package ro.tuc.dsrl.catalyst.model.dto;
//
//import java.util.UUID;
//
//public class TotalPowerDTO {
//
//    private UUID id;
//
//    private long recordTime;
//
//    private double greenGeneration;
//
//    private double brownGeneration;
//
//    private double facilityPowerConsumption;
//
//    private double officesPowerConsumption;
//
//    private double itPowerConsumptionRt;
//
//    private double itPowerConsumptionDt;
//
//    private double powerToSmartGrid;
//
//    private double powerFromSmartGrid;
//
//    private double asSignal;
//
//    private double incomingRealloc;
//
//    public TotalPowerDTO() {
//    }
//
//    public TotalPowerDTO(UUID id, long recordTime, double greenGeneration, double brownGeneration, double facilityPowerConsumption, double officesPowerConsumption, double itPowerConsumptionRt, double itPowerConsumptionDt, double powerToSmartGrid, double powerFromSmartGrid, double asSignal, double incomingRealloc) {
//        this.id = id;
//        this.recordTime = recordTime;
//        this.greenGeneration = greenGeneration;
//        this.brownGeneration = brownGeneration;
//        this.facilityPowerConsumption = facilityPowerConsumption;
//        this.officesPowerConsumption = officesPowerConsumption;
//        this.itPowerConsumptionRt = itPowerConsumptionRt;
//        this.itPowerConsumptionDt = itPowerConsumptionDt;
//        this.powerToSmartGrid = powerToSmartGrid;
//        this.powerFromSmartGrid = powerFromSmartGrid;
//        this.asSignal = asSignal;
//        this.incomingRealloc = incomingRealloc;
//    }
//
//    public UUID getId() {
//        return id;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }
//
//    public long getRecordTime() {
//        return recordTime;
//    }
//
//    public void setRecordTime(long recordTime) {
//        this.recordTime = recordTime;
//    }
//
//    public double getGreenGeneration() {
//        return greenGeneration;
//    }
//
//    public void setGreenGeneration(double greenGeneration) {
//        this.greenGeneration = greenGeneration;
//    }
//
//    public double getBrownGeneration() {
//        return brownGeneration;
//    }
//
//    public void setBrownGeneration(double brownGeneration) {
//        this.brownGeneration = brownGeneration;
//    }
//
//    public double getFacilityPowerConsumption() {
//        return facilityPowerConsumption;
//    }
//
//    public void setFacilityPowerConsumption(double facilityPowerConsumption) {
//        this.facilityPowerConsumption = facilityPowerConsumption;
//    }
//
//    public double getOfficesPowerConsumption() {
//        return officesPowerConsumption;
//    }
//
//    public void setOfficesPowerConsumption(double officesPowerConsumption) {
//        this.officesPowerConsumption = officesPowerConsumption;
//    }
//
//    public double getItPowerConsumptionRt() {
//        return itPowerConsumptionRt;
//    }
//
//    public void setItPowerConsumptionRt(double itPowerConsumptionRt) {
//        this.itPowerConsumptionRt = itPowerConsumptionRt;
//    }
//
//    public double getItPowerConsumptionDt() {
//        return itPowerConsumptionDt;
//    }
//
//    public void setItPowerConsumptionDt(double itPowerConsumptionDt) {
//        this.itPowerConsumptionDt = itPowerConsumptionDt;
//    }
//
//    public double getPowerToSmartGrid() {
//        return powerToSmartGrid;
//    }
//
//    public void setPowerToSmartGrid(double powerToSmartGrid) {
//        this.powerToSmartGrid = powerToSmartGrid;
//    }
//
//    public double getPowerFromSmartGrid() {
//        return powerFromSmartGrid;
//    }
//
//    public void setPowerFromSmartGrid(double powerFromSmartGrid) {
//        this.powerFromSmartGrid = powerFromSmartGrid;
//    }
//
//    public double getAsSignal() {
//        return asSignal;
//    }
//
//    public void setAsSignal(double asSignal) {
//        this.asSignal = asSignal;
//    }
//
//    public double getIncomingRealloc() {
//        return incomingRealloc;
//    }
//
//    public void setIncomingRealloc(double incomingRealloc) {
//        this.incomingRealloc = incomingRealloc;
//    }
//}
