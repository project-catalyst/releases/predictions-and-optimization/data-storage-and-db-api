package ro.tuc.dsrl.catalyst.model.dto;

import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

//TODO: rename class - it is used for different granularities
public class ForecastDayAheadDTO {
    List<EnergyPointValueDTO> energyPointValueDTOS;

    public ForecastDayAheadDTO() {
        this.energyPointValueDTOS = new ArrayList<>();
    }

    public static  ForecastDayAheadDTO getDefaultInstance(LocalDateTime startTime,
                               PredictionGranularity predictionGranularity) {
        ForecastDayAheadDTO defaultDTO = new ForecastDayAheadDTO();
        defaultDTO.energyPointValueDTOS = new ArrayList<>();
        for(int i = 0; i< predictionGranularity.getNoInputs(); i++){
            defaultDTO.energyPointValueDTOS.add(new EnergyPointValueDTO(0.0, DateUtils.dateTimeToMillis(startTime)));
            startTime = startTime.plus(predictionGranularity.getSampleFrequencyMin(), ChronoUnit.MINUTES);
        }
        return defaultDTO;
    }


    public ForecastDayAheadDTO(List<EnergyPointValueDTO> energyPointValueDTOS) {
        this.energyPointValueDTOS = energyPointValueDTOS;
    }

    public List<EnergyPointValueDTO> getEnergyPointValueDTOS() {
        return energyPointValueDTOS;
    }

    public void setEnergyPointValueDTOS(List<EnergyPointValueDTO> energyPointValueDTOS) {
        this.energyPointValueDTOS = energyPointValueDTOS;
    }
}
