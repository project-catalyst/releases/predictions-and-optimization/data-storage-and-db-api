package ro.tuc.dsrl.catalyst.model.enums;

import ro.tuc.dsrl.catalyst.model.error_handler.ResourceNotFoundException;

public enum DeviceTypeEnum {
    BATTERY("BATTERY"),
    TES("THERMAL ENERGY STORAGE"),
    RACK("RACK"),
    SERVER_ROOM("SERVER_ROOM"),
    SERVER("SERVER"),
    DATACENTER("DATACENTER"),
    MARKETPLACE("MARKETPLACE"),
    COOLING_SYSTEM("COOLING_SYSTEM"),
    HEAT_RECOVERY_SYSTEM("HEAT_RECOVERY_SYSTEM"),
    NONE("NONE");

    private  String type;


    public String type() {
        return type;
    }

    DeviceTypeEnum(String type) {
        this.type = type;
    }

    public static DeviceTypeEnum setType(String deviceType) {
        switch (deviceType) {
            case "BATTERY":
                return DeviceTypeEnum.BATTERY;
            case "TES":
                return DeviceTypeEnum.TES;
            case "RACK":
                return DeviceTypeEnum.RACK;
            case "SERVER_ROOM":
                return DeviceTypeEnum.SERVER_ROOM;
            case "SERVER":
                return DeviceTypeEnum.SERVER;
            case "DATACENTER":
                return DeviceTypeEnum.DATACENTER;
            case "MARKETPLACE":
                return DeviceTypeEnum.MARKETPLACE;
            case "COOLING_SYSTEM":
                return DeviceTypeEnum.COOLING_SYSTEM;
            case "HEAT_RECOVERY_SYSTEM":
                return DeviceTypeEnum.HEAT_RECOVERY_SYSTEM;
            default:
                throw new ResourceNotFoundException("Device type " + deviceType);
        }
    }
}
