package ro.tuc.dsrl.catalyst.model.dto.dbmodel;

import java.io.Serializable;
import java.util.UUID;

public class MeasurementDTO implements Serializable {

    private UUID id;
    private String property;
    private String observation;
    private UUID deviceTypeId;
    private UUID measureUnitId;
    private UUID propertySourceId;
    private UUID valueTypeId;

    public MeasurementDTO(){

    }

    public MeasurementDTO(UUID id,
                          String property,
                          String observation,
                          UUID deviceTypeId,
                          UUID measureUnitId,
                          UUID propertySourceId,
                          UUID valueTypeId) {
        this.id = id;
        this.property = property;
        this.observation = observation;
        this.deviceTypeId = deviceTypeId;
        this.measureUnitId = measureUnitId;
        this.propertySourceId = propertySourceId;
        this.valueTypeId = valueTypeId;
    }

    public UUID getId() {
        return id;
    }

    public String getProperty() {
        return property;
    }

    public String getObservation() {
        return observation;
    }

    public UUID getDeviceTypeId() {
        return deviceTypeId;
    }

    public UUID getMeasureUnitId() {
        return measureUnitId;
    }

    public UUID getPropertySourceId() {
        return propertySourceId;
    }

    public UUID getValueTypeId() {
        return valueTypeId;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public void setDeviceTypeId(UUID deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public void setMeasureUnitId(UUID measureUnitId) {
        this.measureUnitId = measureUnitId;
    }

    public void setPropertySourceId(UUID propertySourceId) {
        this.propertySourceId = propertySourceId;
    }

    public void setValueTypeId(UUID valueTypeId) {
        this.valueTypeId = valueTypeId;
    }
}
